#ifndef ICLIENTVGUI_H
#define ICLIENTVGUI_H
#ifdef _WIN32
#pragma once
#endif

#include <vgui/VGUI.h>
#include "interface.h"

//-----------------------------------------------------------------------------
// Purpose: Client VGUI2 interface. 
//			Enables the client library to use VGUI2.
//-----------------------------------------------------------------------------
class IClientVGUI : public IBaseInterface
{
public:
	virtual void Initialize( CreateInterfaceFn *factories, int count ) = 0;
	virtual void Start() = 0;

	virtual void SetParent( vgui::VPANEL parent ) = 0;
	virtual bool UseVGUI1() = 0;

	virtual void HideScoreBoard() = 0;
	virtual void HideAllVGUIMenu() = 0;

	virtual void ActivateClientUI() = 0;
	virtual void HideClientUI() = 0;

	// Note: only called for CS & CZero, do not use! - Solokiller
	virtual void Shutdown() = 0;
};

#define VCLIENTVGUI_VERSION "VClientVGUI001"

#endif //ICLIENTVGUI_H