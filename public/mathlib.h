/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
// mathlib.h

#ifndef MATHLIB_H
#define MATHLIB_H

#include <cmath>
#include "in_defs.h" // PITCH YAW ROLL
#include <float.h>	// Needed for FLT_EPSILON

// disable 'possible loss of data converting float to int' warning message
#pragma warning( disable: 4244 )
// disable 'truncation from 'const double' to 'float' warning message
#pragma warning( disable: 4305 )

typedef float vec_t;

#include "vector.h"

typedef vec_t vec4_t[4];	// x,y,z,w
typedef vec_t vec5_t[5];

typedef short vec_s_t;
typedef vec_s_t vec3s_t[3];
typedef vec_s_t vec4s_t[4];	// x,y,z,w
typedef vec_s_t vec5s_t[5];

typedef	int	fixed4_t;
typedef	int	fixed8_t;
typedef	int	fixed16_t;

typedef struct mplane_s mplane_t;

#ifndef M_PI
#define M_PI		3.14159265358979323846	// matches value in gcc v2 math.h
#endif

#define M_PI_F		((float)(M_PI))	// Shouldn't collide with anything.

#ifndef RAD2DEG
#define RAD2DEG( x  )  ( (float)(x) * (float)(180.f / M_PI_F) )
#endif

#ifndef DEG2RAD
#define DEG2RAD( x  )  ( (float)(x) * (float)(M_PI_F / 180.f) )
#endif

extern  const Vector vec3_origin;
extern	int nanmask;

#define	IS_NAN(x) (((*(int *)&x)&nanmask)==nanmask)

//vec_t DotProduct(const vec_t *v1, const vec_t *v2);

#define VectorCopy(a,b) {(b)[0]=(a)[0];(b)[1]=(a)[1];(b)[2]=(a)[2];}
#define VectorSubtract(a,b,c) {(c)[0]=(a)[0]-(b)[0];(c)[1]=(a)[1]-(b)[1];(c)[2]=(a)[2]-(b)[2];}
#define VectorAdd(a,b,c) {(c)[0]=(a)[0]+(b)[0];(c)[1]=(a)[1]+(b)[1];(c)[2]=(a)[2]+(b)[2];}

inline void VectorClear( float *a ) { a[ 0 ] = 0.0; a[ 1 ] = 0.0; a[ 2 ] = 0.0; }

void VectorMA ( const float *start, float scale, const float *direction, float *dest );

int VectorCompare (const float* v1, const float* v2);
float Length (const float* v);
void CrossProduct (const float* v1, const float* v2, float* cross);
float VectorNormalize (float* v);		// returns vector length
void VectorInverse (float* v);
void VectorScale (const float* in, float scale, float* out);
int Q_log2(int val);

// Math routines done in optimized assembly math package routines
void inline SinCos( float radians, float *sine, float *cosine )
{
#ifdef _WIN32
	_asm
	{
		fld		DWORD PTR [radians]
		fsincos

		mov edx, DWORD PTR [cosine]
		mov eax, DWORD PTR [sine]

		fstp DWORD PTR [edx]
		fstp DWORD PTR [eax]
	}
#elif _LINUX
	register double __cosr, __sinr;
 	__asm __volatile__
    		("fsincos"
     	: "=t" (__cosr), "=u" (__sinr) : "0" (radians));

  	*sine = __sinr;
  	*cosine = __cosr;
#endif
}

// Math routines for optimizing division
void FloorDivMod (double numer, double denom, int *quotient, int *rem);
fixed16_t Invert24To16(fixed16_t val);
int GreatestCommonDivisor (int i1, int i2);

void MatrixAngles( const float matrix[3][4], float *angles );
void VectorTransform (const float* in1, const float in2[3][4], float* out);
void VectorITransform (const float *in1, const float in2[3][4], float *out);

void MatrixCopy( const float in[3][4], float out[3][4] );
int InvertMatrix( const float *m, float *out );

void MatrixGetColumn( const float in[3][4], int column, float *out );
void MatrixSetColumn( const float *in, int column, float out[3][4] );

void ConcatRotations (float in1[3][3], float in2[3][3], float out[3][3]);
void ConcatTransforms ( const float in1[3][4], const float in2[3][4], float out[3][4]);

void VectorIRotate( const float *in1, const float in2[3][4], float *out );
void VectorRotate( const float *in1, const float in2[3][4], float *out );

void QuaternionSlerp( vec4_t p, vec4_t q, float t, vec4_t qt );
void QuaternionMatrix( vec4_t quaternion, float (*matrix)[4] );
void AngleQuaternion( float *angles, vec4_t quaternion );

int BoxOnPlaneSide (const float *emins, const float *emaxs, const mplane_t *p);

inline float anglemod(float a)
{
	a = (360.f/65536) * ((int)(a*(65536.f/360.0f)) & 65535);
	return a;
}

// Remap a value in the range [A,B] to [C,D].
inline float RemapVal( float val, float A, float B, float C, float D)
{
	return C + (D - C) * (val - A) / (B - A);
}

//
// Returns a clamped value in the range [min, max].
//
//#define clamp( val, min, max ) ( ((val) > (max)) ? (max) : ( ((val) < (min)) ? (min) : (val) ) )

#define bound( min, num, max )	((num) >= (min) ? ((num) < (max) ? (num) : (max)) : (min))

#define BOX_ON_PLANE_SIDE(emins, emaxs, p)	\
	(((p)->type < 3)?						\
	(										\
		((p)->dist <= (emins)[(p)->type])?	\
			1								\
		:									\
		(									\
			((p)->dist >= (emaxs)[(p)->type])?\
				2							\
			:								\
				3							\
		)									\
	)										\
	:										\
		BoxOnPlaneSide( (emins), (emaxs), (p)))

extern int SignbitsForPlane( mplane_t *out );

void AngleVectors (const Vector &angles, Vector *forward);
void AngleVectors (const Vector &angles, Vector *forward, Vector *right, Vector *up);
void AngleVectorsTranspose (const Vector &angles, Vector *forward, Vector *right, Vector *up);
void AngleMatrix (const float* angles, float (*matrix)[4] );
void AngleIMatrix (const Vector &angles, float (*matrix)[4] );
void VectorAngles( const float *forward, float *angles );
void VectorMatrix( const Vector &forward, float matrix[3][4] );
void VectorVectors( const Vector &forward, Vector &right, Vector &up);
void SetIdentityMatrix( float mat[3][4] );

float AngleBetweenVectors( const Vector &v1, const Vector &v2 );
void InterpolateAngles( float *start, float *end, float *output, float frac );
void NormalizeAngles( float *angles );

// hermite basis function for smooth interpolation
// Similar to Gain() above, but very cheap to call
// value should be between 0 & 1 inclusive
inline float SimpleSpline( float value )
{
	float valueSquared = value * value;

	// Nice little ease-in, ease-out spline-like curve
	return (3 * valueSquared - 2 * valueSquared * value);
}

// Fast, accurate ftol:
inline int Float2Int( float a )
{
   int CtrlwdHolder;
   int CtrlwdSetter;
   int RetVal;

#ifdef _WIN32
   __asm 
   {
      fld    a					// push 'a' onto the FP stack
      fnstcw CtrlwdHolder		// store FPU control word
      movzx  eax, CtrlwdHolder	// move and zero extend word into eax
      and    eax, 0xFFFFF3FF	// set all bits except rounding bits to 1
      or     eax, 0x00000C00	// set rounding mode bits to round down
      mov    CtrlwdSetter, eax	// Prepare to set the rounding mode -- prepare to enter plaid!
      fldcw  CtrlwdSetter		// Entering plaid!
      fistp  RetVal				// Store and converted (to int) result
      fldcw  CtrlwdHolder		// Restore control word
   }
#elif _LINUX
	RetVal = static_cast<int>( a );
#endif

	return RetVal;
}

// Over 15x faster than: (int)floor(value)
inline int Floor2Int( float a )
{
   int CtrlwdHolder;
   int CtrlwdSetter;
   int RetVal;

#ifdef _WIN32
   __asm 
   {
      fld    a					// push 'a' onto the FP stack
      fnstcw CtrlwdHolder		// store FPU control word
      movzx  eax, CtrlwdHolder	// move and zero extend word into eax
      and    eax, 0xFFFFF3FF	// set all bits except rounding bits to 1
      or     eax, 0x00000400	// set rounding mode bits to round down
      mov    CtrlwdSetter, eax	// Prepare to set the rounding mode -- prepare to enter plaid!
      fldcw  CtrlwdSetter		// Entering plaid!
      fistp  RetVal				// Store floored and converted (to int) result
      fldcw  CtrlwdHolder		// Restore control word
   }
#elif _LINUX
	RetVal = static_cast<int>( floor(a) );
#endif

	return RetVal;
}

// Get the minimum distance from vOrigin to the bounding box defined by [mins,maxs]
// using voronoi regions.
// 0 is returned if the origin is inside the box.
void CalcClosestPointOnAABB( const Vector &mins, const Vector &maxs, const Vector &point, Vector &closestOut );

// Get the closest point from P to the (infinite) line through vLineA and vLineB and
// calculate the shortest distance from P to the line.
// If you pass in a value for t, it will tell you the t for (A + (B-A)t) to get the closest point.
// If the closest point lies on the segment between A and B, then 0 <= t <= 1.
float CalcDistanceToLine( const Vector &P, const Vector &vLineA, const Vector &vLineB, float *t=0 );

float Approach( float target, float value, float speed );
float ApproachAngle( float target, float value, float speed );
float AngleDiff( float destAngle, float srcAngle );
float AngleDistance( float next, float cur );
float SplineFraction( float value, float scale );

#endif	// MATHLIB_H