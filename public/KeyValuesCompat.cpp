#include "IKeyValues.h"
#include "vstdlib/IKeyValuesSystem.h"
#include "KeyValues.h"
#include "KeyValuesCompat.h"

IKeyValues* g_pKeyValuesInterface = NULL;

class CKeyValuesWrapper : public IKeyValuesSystem
{
public:
	void RegisterSizeofKeyValues( int size ) override
	{
		return g_pKeyValuesInterface->RegisterSizeofKeyValues( size );
	}

	void *AllocKeyValuesMemory( int size ) override
	{
		return g_pKeyValuesInterface->AllocKeyValuesMemory( size );
	}

	void FreeKeyValuesMemory( void *pMem ) override
	{
		g_pKeyValuesInterface->FreeKeyValuesMemory( pMem );
	}

	HKeySymbol GetSymbolForString( const char *name ) override
	{
		return g_pKeyValuesInterface->GetSymbolForString( name );
	}

	const char *GetStringForSymbol( HKeySymbol symbol ) override
	{
		return g_pKeyValuesInterface->GetStringForSymbol( symbol );
	}

	void AddKeyValuesToMemoryLeakList( void *pMem, HKeySymbol name ) override
	{
		g_pKeyValuesInterface->AddKeyValuesToMemoryLeakList( pMem, name );
	}

	void RemoveKeyValuesFromMemoryLeakList( void *pMem ) override
	{
		g_pKeyValuesInterface->RemoveKeyValuesFromMemoryLeakList( pMem );
	}
};

CKeyValuesWrapper g_KeyValuesSystem;

IKeyValuesSystem *keyvalues()
{
	return &g_KeyValuesSystem;
}

bool KV_InitKeyValuesSystem( CreateInterfaceFn* pFactories, int iNumFactories )
{
	g_pKeyValuesInterface = ( IKeyValues* ) InitializeInterface( KEYVALUES_INTERFACE_VERSION, pFactories, iNumFactories );

	if( !g_pKeyValuesInterface )
		return false;

	g_pKeyValuesInterface->RegisterSizeofKeyValues( sizeof( KeyValues ) );

	return true;
}
