/***
*
*	Copyright (c) 1999, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
//  cdll_int.h
//
// 4-23-98  
// JOHN:  client dll interface declarations
//

#ifndef CDLL_INT_H
#define CDLL_INT_H

#ifdef _WIN32
#pragma once
#endif

#include "const.h"
#include "steam/steamtypes.h"
#include "ref_params.h"
#include "r_efx.h"
#include "studio_event.h"
#include "netadr.h"
#include "Sequence.h"
#include "enums.h"
#include "cmd_function.h"

// this file is included by both the engine and the client-dll,
// so make sure engine declarations aren't done twice

typedef int HSPRITE;	// handle to a graphic

#define SCRINFO_SCREENFLASH 1
#define SCRINFO_STRETCHED	2

typedef struct SCREENINFO_s
{
	int		iSize;
	int		iWidth;
	int		iHeight;
	int		iFlags;
	int		iCharHeight;
	short	charWidths[256];
} SCREENINFO;


typedef struct client_data_s
{
	// fields that cannot be modified  (ie. have no effect if changed)
	Vector origin;

	// fields that can be changed by the cldll
	Vector viewangles;
	int		iWeaponBits;
//	int		iAccessoryBits;
	float	fov;	// field of view
} client_data_t;

typedef struct client_sprite_s
{
	char szName[64];
	char szSprite[64];
	int hspr;
	int iRes;
	wrect_t rc;
} client_sprite_t;



typedef struct hud_player_info_s
{
	char *name;
	short ping;
	byte thisplayer;  // TRUE if this is the calling player

	byte spectator;
	byte packetloss;

	char *model;
	short topcolor;
	short bottomcolor;

	uint64 m_nSteamID;
} hud_player_info_t;

#define	MAX_ALIAS_NAME	32

typedef struct cmdalias_s
{
	struct cmdalias_s	*next;
	char	name[MAX_ALIAS_NAME];
	char	*value;
} cmdalias_t;

//Now defined here. Who ever thought defining this somewhere else was a good idea? - Solokiller
typedef int (*pfnUserMsgHook)(const char *pszName, int iSize, void *pbuf);

// Functions exported by the engine
typedef struct cl_enginefuncs_s
{
	// sprite handlers
	HSPRITE						( *pfnSPR_Load )			( const char *szPicName );
	int							( *pfnSPR_Frames )			( HSPRITE hPic );
	int							( *pfnSPR_Height )			( HSPRITE hPic, int frame );
	int							( *pfnSPR_Width )			( HSPRITE hPic, int frame );
	void						( *pfnSPR_Set )				( HSPRITE hPic, int r, int g, int b );
	void						( *pfnSPR_Draw )			( int frame, int x, int y, const wrect_t *prc );
	void						( *pfnSPR_DrawHoles )		( int frame, int x, int y, const wrect_t *prc );
	void						( *pfnSPR_DrawAdditive )	( int frame, int x, int y, const wrect_t *prc );
	void						( *pfnSPR_EnableScissor )	( int x, int y, int width, int height );
	void						( *pfnSPR_DisableScissor )	( void );
	client_sprite_t				*( *pfnSPR_GetList )		( char *psz, int *piCount );

	// screen handlers
	void						( *pfnFillRGBA )			( int x, int y, int width, int height, int r, int g, int b, int a );
	int							( *pfnGetScreenInfo ) 		( SCREENINFO *pscrinfo );
	void						( *pfnSetCrosshair )		( HSPRITE hspr, wrect_t rc, int r, int g, int b );

	// cvar handlers
	struct cvar_s				*( *pfnRegisterVariable )	( char *szName, char *szValue, int flags );
	float						( *pfnGetCvarFloat )		( char *szName );
	char*						( *pfnGetCvarString )		( char *szName );

	// command handlers
	int							( *pfnAddCommand )			( char *cmd_name, xcommand_t function );
	int							( *pfnHookUserMsg )			( char *szMsgName, pfnUserMsgHook pfn );
	int							( *pfnServerCmd )			( char *szCmdString );
	int							( *pfnClientCmd )			( char *szCmdString );

	void						( *pfnGetPlayerInfo )		( int ent_num, hud_player_info_t *pinfo );

	// sound handlers
	void						( *pfnPlaySoundByName )		( const char *szSound, float volume );
	void						( *pfnPlaySoundByIndex )	( int iSound, float volume );

	// vector helpers
	void						( *pfnAngleVectors )		( const float * vecAngles, float * forward, float * right, float * up );

	// text message system

	// Retrieves text message system information for the specified message by name
	client_textmessage_t		*( *pfnTextMessageGet )		( const char *pName );
	int							( *pfnDrawCharacter )		( int x, int y, int number, int r, int g, int b );
	int							( *pfnDrawConsoleString )	( int x, int y, char *string );
	void						( *pfnDrawSetTextColor )	( float r, float g, float b );
	void						( *pfnDrawConsoleStringLen )(  const char *string, int *length, int *height );

	void						( *pfnConsolePrint )		( const char *string );
	void						( *pfnCenterPrint )			( const char *string );


// Added for user input processing
	int							( *GetWindowCenterX )		( void );
	int							( *GetWindowCenterY )		( void );
	void						( *GetViewAngles )			( float * );
	void						( *SetViewAngles )			( float * );

	// Retrieve the current game's maxclients setting
	int							( *GetMaxClients )			( void );
	void						( *Cvar_SetValue )			( char *cvar, float value );

	int       					(*Cmd_Argc)					(void);	
	char						*( *Cmd_Argv )				( int arg );
	void						( *Con_Printf )				( char *fmt, ... );
	void						( *Con_DPrintf )			( char *fmt, ... );

	// Prints the formatted string to the notification area of the screen ( down the right hand edge
	//  numbered lines starting at position 0
	void						( *Con_NPrintf )			( int pos, char *fmt, ... );
	// Similar to Con_NPrintf, but allows specifying custom text color and duration information
	void						( *Con_NXPrintf )			( struct con_nprint_s *info, char *fmt, ... );

	const char					*( *PhysInfo_ValueForKey )	( const char *key );
	const char					*( *ServerInfo_ValueForKey )( const char *key );
	float						( *GetClientMaxspeed )		( void );
	int							( *CheckParm )				( char *parm, char **ppnext );
	void						( *Key_Event )				( int key, int down );
	void						( *GetMousePosition )		( int *mx, int *my );
	int							( *IsNoClipping )			( void );

	struct cl_entity_s			*( *GetLocalPlayer )		( void );
	struct cl_entity_s			*( *GetViewModel )			( void );
	struct cl_entity_s			*( *GetEntityByIndex )		( int idx );

	float						( *GetClientTime )			( void );
	void						( *V_CalcShake )			( void );
	void						( *V_ApplyShake )			( float *origin, float *angles, float factor );

	int							( *PM_PointContents )		( float *point, int *truecontents );
	int							( *PM_WaterEntity )			( float *p );
	struct pmtrace_s			*( *PM_TraceLine )			( float *start, float *end, int flags, int usehull, int ignore_pe );

	// Client DLL is hooking a model, loads the model into memory and returns pointer to the model_t
	struct model_s				*( *CL_LoadModel )			( const char *modelname, int *index );
	int							( *CL_CreateVisibleEntity )	( int type, struct cl_entity_s *ent );

	const struct model_s *		( *GetSpritePointer )		( HSPRITE hSprite );
	void						( *pfnPlaySoundByNameAtLocation )	( char *szSound, float volume, float *origin );

	unsigned short				( *pfnPrecacheEvent )		( int type, const char* psz );
	void						( *pfnPlaybackEvent )		( int flags, const struct edict_s *pInvoker, unsigned short eventindex, float delay, float *origin, float *angles, float fparam1, float fparam2, int iparam1, int iparam2, int bparam1, int bparam2 );
	void						( *pfnWeaponAnim )			( int iAnim, int body );
	float						( *pfnRandomFloat )			( float flLow, float flHigh );
	long						( *pfnRandomLong )			( long lLow, long lHigh );
	void						( *pfnHookEvent )			( char *name, void ( *pfnEvent )( struct event_args_s *args ) );

	// returns 1 if the console is visible
	int							(*Con_IsVisible)			( void );

	// Get the current game directory
	const char					*( *pfnGetGameDirectory )	( void );
	struct cvar_s				*( *pfnGetCvarPointer )		( const char *szName );

	// Given the string pBinding which may be bound to a key, 
	//  returns the string name of the key to which this string is bound. Returns NULL if no such binding exists
	const char					*( *Key_LookupBinding )		( const char *pBinding );

	// Get the name of the current map
	const char					*( *pfnGetLevelName )		( void );

	void						( *pfnGetScreenFade )		( struct screenfade_s *fade );
	void						( *pfnSetScreenFade )		( struct screenfade_s *fade );
	void                        *( *VGui_GetPanel )         ( );
	void                         ( *VGui_ViewportPaintBackground ) (int extents[4]);

	byte*						(*COM_LoadFile)				( char *path, int usehunk, int *pLength );
	char*						(*COM_ParseFile)			( char *data, char *token );
	void						(*COM_FreeFile)				( void *buffer );
		
	struct triangleapi_s		*pTriAPI;
	struct efx_api_s			*pEfxAPI;
	struct event_api_s			*pEventAPI;
	struct demo_api_s			*pDemoAPI;
	struct net_api_s			*pNetAPI;

	// Obtain access to the voice tweaking API
	struct IVoiceTweak_s		*pVoiceTweak;

	// returns 1 if the client is a spectator only (connected to a proxy), 0 otherwise or 2 if in dev_overview mode
	int							( *IsSpectateOnly ) ( void );
	struct model_s				*( *LoadMapSprite )			( const char *filename );

	// file search functions
	void						( *COM_AddAppDirectoryToSearchPath ) ( const char *pszBaseDir, const char *appName );
	int							( *COM_ExpandFilename)				 ( const char *fileName, char *nameOutBuffer, int nameOutBufferSize );

	// User info
	// playerNum is in the range (1, MaxClients)
	// returns NULL if player doesn't exit
	// returns "" if no value is set
	const char					*( *PlayerInfo_ValueForKey )( int playerNum, const char *key );
	void						( *PlayerInfo_SetValueForKey )( const char *key, const char *value );

	// Gets a unique ID for the specified player. This is the same even if you see the player on a different server.
	// iPlayer is an entity index, so client 0 would use iPlayer=1.
	// Returns false if there is no player on the server in the specified slot.
	qboolean					(*GetPlayerUniqueID)(int iPlayer, char playerID[16]);

	// TrackerID access
	int							(*GetTrackerIDForPlayer)(int playerSlot);
	int							(*GetPlayerForTrackerID)(int trackerID);

	// Same as pfnServerCmd, but the message goes in the unreliable stream so it can't clog the net stream
	// (but it might not get there).
	int							( *pfnServerCmdUnreliable )( char *szCmdString );

	void						( *pfnGetMousePos )( struct tagPOINT *ppt );
	void						( *pfnSetMousePos )( int x, int y );
	void						( *pfnSetMouseEnable )( qboolean fEnable );

	struct cvar_s			   *( *GetFirstCvarPtr ) ( void );
	struct cmd_function_s	   *( *GetFirstCmdFunctionHandle ) ( void );
	struct cmd_function_s	   *( *GetNextCmdFunctionHandle ) ( struct cmd_function_s *cmdhandle );
	const char				   *( *GetCmdFunctionName ) ( struct cmd_function_s *cmdhandle );

	// Gets the old client time
	float						( *hudGetClientOldTime ) ( void );
	float						( *hudGetServerGravityValue ) ( void );

	// Retrieve indexed model from client side model precache list
	struct model_s	*			( *hudGetModelByIndex ) ( int index );

	void						( *pfnSetFilterMode ) ( int mode );
	void						( *pfnSetFilterColor ) ( float r, float g, float b );
	void						( *pfnSetFilterBrightness ) ( float brightness );

	sequenceEntry_s*			( *pfnSequenceGet ) ( const char *fileName, const char* entryName );
	void						( *pfnSPR_DrawGeneric ) ( int frame, int x, int y, const struct rect_s *prc, int src, int dest, int w, int h );
	sentenceEntry_s*			( *pfnSequencePickSentence ) ( const char *sentenceName, int pickMethod, int* entryPicked );

	// draw a complete string
	int							( *pfnDrawString ) ( int x, int y, const char *str, int r, int g, int b );
	int							( *pfnDrawStringReverse )		( int x, int y, const char *str, int r, int g, int b );
	const char *				( *LocalPlayerInfo_ValueForKey ) ( const char *key );
	int							( *pfnVGUI2DrawCharacter )		( int x, int y, int ch, unsigned int font );
	int							( *pfnVGUI2DrawCharacterAdd )	( int x, int y, int ch, int r, int g, int b, unsigned int font);
	unsigned int				( *COM_GetApproxWavePlayLength ) ( const char * filename );
	void *						( *pfnGetCareerUI ) ( void );
	void						( *Cvar_Set )			( char *cvar, char *value );
	int							( *pfnIsCareerMatch ) ( void );
	double						( *GetAbsoluteTime ) ( void );

	void						( *pfnProcessTutorMessageDecayBuffer ) ( int *buffer, int bufferLength );
	void						( *pfnConstructTutorMessageDecayBuffer ) ( int *buffer, int bufferLength );
	void						( *pfnResetTutorMessageDecayData ) ( void );

	void						( *pfnFillRGBABlend )	( int x, int y, int width, int height, int r, int g, int b, int a );
	int							( *pfnGetAppID ) ( void );
	cmdalias_t*					( *pfnGetAliasList ) ( void );
	void						( *pfnVguiWrap2_GetMouseDelta ) ( int *x, int *y );
	int							( *pfnFilteredClientCmd )	( const char *szCmdString );
} cl_enginefunc_t;

#ifndef IN_BUTTONS_H
#include "in_buttons.h"
#endif

#define CLDLL_INTERFACE_VERSION		7

// Functions exported by the client .dll
typedef struct
{
	int				( *pInitFunc ) ( struct cl_enginefuncs_s *pEnginefuncs, int version );
	void			( *pHudInitFunc ) ( void );
	int				( *pHudVidInitFunc ) ( void );
	int				( *pHudRedrawFunc )	( float time, int intermission );
	int				( *pHudUpdateClientDataFunc ) ( struct client_data_s*, float time );
	void			( *pHudResetFunc ) ( void );

	void			( *pClientMove ) ( struct playermove_s *ppmove, qboolean server );
	void			( *pClientMoveInit ) ( struct playermove_s *ppmove );
	char			( *pClientTextureType ) ( char *name );

	void			( *pIN_ActivateMouse ) ( void );
	void			( *pIN_DeactivateMouse ) ( void );
	void			( *pIN_MouseEvent ) ( int mstate );
	void			( *pIN_ClearStates ) ( void );
	void			( *pIN_Accumulate ) ( void );
	void			( *pCL_CreateMove )	( float frametime, struct usercmd_s *cmd, int active );
	int				( *pCL_IsThirdPerson ) ( void );
	void			( *pCL_GetCameraOffsets ) ( float *ofs );
	struct kbutton_s * ( *pFindKey ) ( const char *name );
	void			( *pCamThink ) ( void );
	void			( *pCalcRefdef ) ( struct ref_params_s *pparams );
	
	int				( *pAddEntity ) ( int type, struct cl_entity_s *ent, const char *modelname );
	void			( *pCreateEntities ) ( void );

	void			( *pDrawNormalTriangles ) ( void );
	void			( *pDrawTransparentTriangles ) ( void );
	
	void			( *pStudioEvent ) ( const struct mstudioevent_t *event, const struct cl_entity_s *entity );
	void			( *pPostRunCmd ) ( struct local_state_s *from, struct local_state_s *to, struct usercmd_s *cmd, int runfuncs, double time, unsigned int random_seed );
	void			( *pShutdown ) ( void );

	void			( *pTxferLocalOverrides ) ( struct entity_state_s *state, const struct clientdata_s *client );
	void			( *pProcessPlayerState ) ( struct entity_state_s *dst, const struct entity_state_s *src );
	void			( *pTxferPredictionData ) ( struct entity_state_s *ps, const struct entity_state_s *pps, struct clientdata_s *pcd, const struct clientdata_s *ppcd, struct weapon_data_s *wd, const struct weapon_data_s *pwd );
	
	void			( *pReadDemoBuffer ) ( int size, unsigned char *buffer );
	int				( *pConnectionlessPacket ) ( const struct netadr_s *net_from, const char *args, char *response_buffer, int *response_buffer_size );

	int				( *pGetHullBounds ) ( int hullnumber, float *mins, float *maxs );
	void			( *pHudFrame )		( double frametime );
	int				( *pKeyEvent ) ( int eventcode, int keynum, const char *pszCurrentBinding );
	void			( *pTempEntUpdate ) ( double frametime, double client_time, double cl_gravity, struct tempent_s **ppTempEntFree, struct tempent_s **ppTempEntActive, int ( *Callback_AddVisibleEntity )( struct cl_entity_s *pEntity ),	void ( *Callback_TempEntPlaySound )( struct tempent_s *pTemp, float damp ) );

	struct cl_entity_s *( *pGetUserEntity ) ( int index );

	// Possibly null on old client dlls.
	void			( *pVoiceStatus ) ( int entindex, qboolean bTalking );
	void			( *pDirectorMessage ) ( int iSize, void *pbuf );
	
	// Not used by all clients
	int				( *pStudioInterface) ( int version, struct r_studio_interface_s **ppinterface, struct engine_studio_api_s *pstudio );
	void			( *pChatInputPosition ) ( int *x, int *y );
	int				( *pGetPlayerTeam ) ( int iplayer );

	// this should be CreateInterfaceFn but that means including interface.h
	// which is a C++ file and some of the client files a C only... 
	// so we return a void * which we then do a typecast on later.
	void *			( *pClientFactory ) ( void );
} cldll_func_t;

#endif // CDLL_INT_H
	