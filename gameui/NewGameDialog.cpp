//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// Vasia, 5-15-2022: Moved this from client dll to GameUI
//=============================================================================//

#include "NewGameDialog.h"

#include "EngineInterface.h"
#include <vgui_controls/Button.h>
#include <vgui_controls/CheckButton.h>
#include <KeyValues.h>
#include "vgui/ISurface.h"
#include "vgui/IInput.h"
#include "vgui/ILocalize.h"
#include <vgui/ISystem.h>
#include "vgui_controls/RadioButton.h"
#include "vgui_controls/ComboBox.h"
#include "vgui_controls/ImagePanel.h"
#include "vgui_controls/Frame.h"
#include "FileSystem.h"
#include "ModInfo.h"
#include "GameUI_Interface.h"
#include "taskframe.h"
#include <vgui_controls/Label.h>
#include <vgui/ISurface.h>
#include <vgui_controls/RadioButton.h>

#include <stdio.h>

// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

extern cvar_t *sv_unlockedchapters;

using namespace vgui;

// sort function used in displaying chapter list
struct chapter_t
{
	char filename[32];
};

static int __cdecl ChapterSortFunc(const void *elem1, const void *elem2)
{
	chapter_t *c1 = (chapter_t *)elem1;
	chapter_t *c2 = (chapter_t *)elem2;

	// compare chapter number first
	static int chapterlen = strlen("chapter");
	if (atoi(c1->filename + chapterlen) > atoi(c2->filename + chapterlen))
		return 1;
	else if (atoi(c1->filename + chapterlen) < atoi(c2->filename + chapterlen))
		return -1;

	// compare length second (longer string show up later in the list, eg. chapter9 before chapter9a)
	if (strlen(c1->filename) > strlen(c2->filename))
		return 1;
	else if (strlen(c1->filename) < strlen(c2->filename))
		return -1;

	// compare strings third
	return strcmp(c1->filename, c2->filename);
}

//-----------------------------------------------------------------------------
// Purpose: invisible panel used for selecting a chapter panel
//-----------------------------------------------------------------------------
class CSelectionOverlayPanel : public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CSelectionOverlayPanel, Panel );

	int m_iChapterIndex;
	CNewGameDialog *m_pSelectionTarget;

public:
	CSelectionOverlayPanel( Panel *parent, CNewGameDialog *selectionTarget, int chapterIndex ) : BaseClass( parent, NULL )
	{
		m_iChapterIndex = chapterIndex;
		m_pSelectionTarget = selectionTarget;
		SetPaintEnabled(false);
		SetPaintBackgroundEnabled(false);
	}

	virtual void OnMousePressed( vgui::MouseCode code )
	{
		if (GetParent()->IsEnabled())
		{
			m_pSelectionTarget->SetSelectedChapterIndex( m_iChapterIndex );
		}
	}

	virtual void OnMouseDoublePressed( vgui::MouseCode code )
	{
		// call the panel
		OnMousePressed( code );
		if (GetParent()->IsEnabled())
		{
			PostMessage( m_pSelectionTarget, new KeyValues("Command", "command", "Play") );
		}
	}
};

//-----------------------------------------------------------------------------
// Purpose: selectable item with screenshot for an individual chapter in the dialog
//-----------------------------------------------------------------------------
class CGameChapterPanel : public vgui::EditablePanel
{
	DECLARE_CLASS_SIMPLE( CGameChapterPanel, EditablePanel );

	ImagePanel *m_pLevelPicBorder;
	ImagePanel *m_pLevelPic;
	Label *m_pChapterLabel;
	Label *m_pChapterNameLabel;

	Color m_TextColor;
	Color m_DisabledColor;
	Color m_SelectedColor;
	Color m_FillColor;

	char m_szConfigFile[_MAX_PATH];
	char m_szChapter[32];

	bool m_bTeaserChapter;

	bool m_bIsSelected;

public:
	CGameChapterPanel( CNewGameDialog *parent, const char *name, const char *chapterName, int chapterIndex, const char *chapterNumber, const char *chapterConfigFile ) : BaseClass( parent, name )
	{
		Q_strncpy( m_szConfigFile, chapterConfigFile, sizeof(m_szConfigFile) );
		Q_strncpy( m_szChapter, chapterNumber, sizeof(m_szChapter) );

		m_pLevelPicBorder = SETUP_PANEL( new ImagePanel( this, "LevelPicBorder" ) );
		m_pLevelPic = SETUP_PANEL( new ImagePanel( this, "LevelPic" ) );
		m_bIsSelected = false;

		wchar_t text[32];
		wchar_t num[32];
		wchar_t *chapter = vgui::localize()->Find("#GameUI_Chapter");
		vgui::localize()->ConvertANSIToUnicode( chapterNumber, num, sizeof(num) );
		_snwprintf( text, sizeof(text), L"%s %s", chapter ? chapter : L"CHAPTER", num );

		m_pChapterLabel = new Label( this, "ChapterLabel", text );
		m_pChapterNameLabel = new Label( this, "ChapterNameLabel", chapterName );

		SetPaintBackgroundEnabled( false );

		// the image has the same name as the config file
		char szMaterial[ MAX_PATH ];
		Q_snprintf( szMaterial, sizeof(szMaterial), "gfx/vgui/chapters/%s", chapterConfigFile );
		char *ext = strstr( szMaterial, "." );
		if ( ext )
		{
			*ext = 0;
		}
		m_pLevelPic->SetImage( szMaterial );

		LoadControlSettings( "resource/NewGameChapterPanel.res" );

		int px, py;
		m_pLevelPicBorder->GetPos( px, py );
		SetSize( m_pLevelPicBorder->GetWide(), py + m_pLevelPicBorder->GetTall() );

		// create a selection panel the size of the page
		CSelectionOverlayPanel *overlay = new CSelectionOverlayPanel( this, parent, chapterIndex );
		overlay->SetBounds(0, 0, GetWide(), GetTall());
		overlay->MoveToFront();

		// HACK: Detect new episode teasers by the "Coming Soon" text
		wchar_t w_szStrTemp[256];
		m_pChapterNameLabel->GetText( w_szStrTemp, sizeof(w_szStrTemp)  );
		m_bTeaserChapter = !wcscmp(w_szStrTemp, L"Coming Soon");
	}

	virtual void ApplySchemeSettings( IScheme *pScheme )
	{
		m_TextColor = pScheme->GetColor( "ControlText", Color(255, 255, 255, 255) );
		m_FillColor = pScheme->GetColor( "SelectionBG2", Color(255, 255, 255, 255) );
		m_DisabledColor = pScheme->GetColor( "DisabledText1", Color(255, 255, 255, 255) );
		m_SelectedColor = pScheme->GetColor( "SelectionBG", Color(255, 255, 255, 255) );

		BaseClass::ApplySchemeSettings( pScheme );

		// Hide chapter numbers for new episode teasers
		if ( m_bTeaserChapter )
		{
			m_pChapterLabel->SetVisible( false );
		}
	}

	bool IsSelected( void ) const { return m_bIsSelected; }

	void SetSelected( bool state )
	{
		m_bIsSelected = state;

		// update the text/border colors
		if ( !IsEnabled() )
		{
			m_pChapterLabel->SetFgColor( m_DisabledColor );
			//m_pChapterNameLabel->SetFgColor( Color(0, 0, 0, 0) ); // Doesn't properly work in software mode
			m_pLevelPicBorder->SetFillColor( m_DisabledColor );

			// Vasia: Fade the chapter image
			m_pLevelPic->SetDrawColor( Color( 255, 255, 255, 128 ));
			m_pChapterNameLabel->SetPaintEnabled( false );
			return;
		}

		if ( state )
		{
			m_pChapterLabel->SetFgColor( m_SelectedColor );
			m_pChapterNameLabel->SetFgColor( m_SelectedColor );
			m_pLevelPicBorder->SetFillColor( m_SelectedColor );
		}
		else
		{
			m_pChapterLabel->SetFgColor( m_TextColor );
			m_pChapterNameLabel->SetFgColor( m_TextColor );
			m_pLevelPicBorder->SetFillColor( m_FillColor );
		}

		// Vasia: Reset to normal
		m_pLevelPic->SetDrawColor( Color( 255, 255, 255, 255 ));
		m_pChapterNameLabel->SetPaintEnabled( true );
	}

	const char *GetConfigFile()
	{
		return m_szConfigFile;
	}

	const char *GetChapter()
	{
		return m_szChapter;
	}

	bool IsTeaserChapter()
	{
		return m_bTeaserChapter;
	}

};

//-----------------------------------------------------------------------------
// Purpose: Basic help dialog
//-----------------------------------------------------------------------------
CNewGameDialog::CNewGameDialog(vgui::Panel *parent) : BaseClass(parent, "NewGameDialog")
{
	g_pTaskbar->AddTask(GetVPanel());

	SetDeleteSelfOnClose( true );
	SetBounds( 0, 0, 372, 160 );
	SetSizeable( false );
	m_iSelectedChapter = -1;
	m_ActiveTitleIdx = 0;

	m_bScrolling = false;
	m_ScrollCt = 0;
	m_ButtonPressed = SCROLL_NONE;
	m_ScrollDirection = SCROLL_NONE;
	m_bMapStarting = false;

	SetTitle("#GameUI_NewGame", true);

	m_pNextButton = new vgui::Button( this, "Next", "#Gameui_Next" );
	m_pPrevButton = new vgui::Button( this, "Prev", "#Gameui_Prev" );
	m_pPlayButton = new vgui::Button( this, "Play", "#GameUI_Play" );
	m_pPlayButton->SetCommand( "Play" );

	vgui::Button *cancel = new vgui::Button( this, "Cancel", "#GameUI_Cancel" );
	cancel->SetCommand( "Close" );

	// parse out the chapters off disk
	static const int MAX_CHAPTERS = 32;
	chapter_t chapters[MAX_CHAPTERS];

	const char *pGameDir = engine->pfnGetGameDirectory();

	char szFullFileName[MAX_PATH];
	int chapterIndex = 0;

	FileFindHandle_t findHandle = FILESYSTEM_INVALID_FIND_HANDLE;
	const char *fileName = "scripts/chapter*.cfg";
	fileName = vgui::filesystem()->FindFirst( fileName, &findHandle, "GAME" );
	while ( fileName && chapterIndex < MAX_CHAPTERS )
	{
		// Only load chapter configs from the current mod's dir
		// or else chapters appear that we don't want!
		Q_snprintf( szFullFileName, sizeof(szFullFileName), "scripts/%s", fileName );
		FileHandle_t f = vgui::filesystem()->Open( szFullFileName, "rb", "GAME" );
		if ( f )
		{	
			// don't load chapter files that are empty, used in the demo
			if ( vgui::filesystem()->Size(f) > 0 )
			{
				Q_strncpy(chapters[chapterIndex].filename, fileName, sizeof(chapters[chapterIndex].filename));
				++chapterIndex;
			}
			vgui::filesystem()->Close( f );
		}
		fileName = vgui::filesystem()->FindNext(findHandle);
	}

	// sort the chapters
	qsort(chapters, chapterIndex, sizeof(chapter_t), &ChapterSortFunc);

	const char *unlockedChapter = ( sv_unlockedchapters != NULL ) ? sv_unlockedchapters->string : "1";
	int iUnlockedChapter = atoi(unlockedChapter);

	// add chapters to combobox
	for (int i = 0; i < chapterIndex; i++)
	{
		const char *fileName = chapters[i].filename;
		char chapterID[32] = { 0 };
		sscanf(fileName, "chapter%s", chapterID);
		// strip the extension
		char *ext = V_stristr(chapterID, ".cfg");
		if (ext)
		{
			*ext = 0;
		}

		char chapterName[64];
		Q_snprintf(chapterName, sizeof(chapterName), "#%s_Chapter%s_Title", pGameDir, chapterID);

		Q_snprintf( szFullFileName, sizeof( szFullFileName ), "%s", fileName );
		CGameChapterPanel *chapterPanel = SETUP_PANEL( new CGameChapterPanel( this, NULL, chapterName, i, chapterID, szFullFileName ) );
		chapterPanel->SetVisible( false );

		UpdatePanelLockedStatus( iUnlockedChapter, i + 1, chapterPanel );

		m_ChapterPanels.AddToTail( chapterPanel );
	}

	LoadControlSettings( "Resource/NewGameDialog.res" );

	// Reset all properties
	for ( int i = 0; i < NUM_SLOTS; ++i )
	{
		m_PanelIndex[i] = INVALID_INDEX;
	}

	if ( !m_ChapterPanels.Count() )
	{
		UpdateMenuComponents( SCROLL_NONE);
		return;
	}

	// Layout panel positions relative to the dialog center.
	int panelWidth = m_ChapterPanels[0]->GetWide() + 16;
	int dialogWidth = GetWide();
	m_PanelXPos[2] = ( dialogWidth - panelWidth ) / 2 + 8;
	
	if (m_ChapterPanels.Count() > 1)
	{
		m_PanelXPos[1] = m_PanelXPos[2] - panelWidth;
		m_PanelXPos[0] = m_PanelXPos[1];
		m_PanelXPos[3] = m_PanelXPos[2] + panelWidth;
		m_PanelXPos[4] = m_PanelXPos[3];
	}
	else
	{
		m_PanelXPos[0] = m_PanelXPos[1] = m_PanelXPos[3] =
		m_PanelXPos[4] = m_PanelXPos[2];
	}

	m_PanelAlpha[0] = 0;
	m_PanelAlpha[1] = 255;
	m_PanelAlpha[2] = 255;
	m_PanelAlpha[3] = 255;
	m_PanelAlpha[4] = 0;

	// start the first item selected
	SetSelectedChapterIndex( 0 );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CNewGameDialog::~CNewGameDialog()
{
}

void CNewGameDialog::Activate( void )
{
	m_bMapStarting = false;

	// work out which chapters are unlocked
	const char *unlockedChapter = (sv_unlockedchapters != NULL) ? sv_unlockedchapters->string : "1";
	int iUnlockedChapter = atoi(unlockedChapter);

	for ( int i = 0; i < m_ChapterPanels.Count(); i++)
	{
		CGameChapterPanel *pChapterPanel = m_ChapterPanels[ i ];

		if ( pChapterPanel )
		{
			UpdatePanelLockedStatus( iUnlockedChapter, i + 1, pChapterPanel );
		}
	}

	BaseClass::Activate();
}

//-----------------------------------------------------------------------------
// Purpose: Apply special properties of the menu
//-----------------------------------------------------------------------------
void CNewGameDialog::ApplySettings( KeyValues *inResourceData )
{
	BaseClass::ApplySettings( inResourceData );

	int ypos = inResourceData->GetInt( "chapterypos", 40 );
	for ( int i = 0; i < NUM_SLOTS; ++i )
	{
		m_PanelYPos[i] = ypos;
	}

}

void CNewGameDialog::ApplySchemeSettings( vgui::IScheme *pScheme )
{
	BaseClass::ApplySchemeSettings( pScheme );

	UpdateMenuComponents( SCROLL_NONE );
}


//-----------------------------------------------------------------------------
// Purpose: sets the correct properties for visible components
//-----------------------------------------------------------------------------
void CNewGameDialog::UpdateMenuComponents( EScrollDirection dir )
{
	// This is called prior to any scrolling, 
	// so we need to look ahead to the post-scroll state
	int centerIdx = SLOT_CENTER;
	if ( dir == SCROLL_LEFT )
	{
		++centerIdx;
	}
	else if ( dir == SCROLL_RIGHT )
	{
		--centerIdx;
	}
	int leftIdx = centerIdx - 1;
	int rightIdx = centerIdx + 1;

	if ( m_PanelIndex[leftIdx] == INVALID_INDEX || m_PanelIndex[leftIdx] == 0 )
	{
		m_pPrevButton->SetVisible( false );
		m_pPrevButton->SetEnabled( false );
	}
	else
	{
		m_pPrevButton->SetVisible( true );
		m_pPrevButton->SetEnabled( true );
	}

	if ( m_ChapterPanels.Count() < 4 ) // if there are less than 4 chapters show the next button but disabled
	{
		m_pNextButton->SetVisible( true );
		m_pNextButton->SetEnabled( false );
	}
	else if ( m_PanelIndex[rightIdx] == INVALID_INDEX || m_PanelIndex[rightIdx] == m_ChapterPanels.Count()-1 )
	{
		m_pNextButton->SetVisible( false );
		m_pNextButton->SetEnabled( false );
	}
	else
	{
		m_pNextButton->SetVisible( true );
		m_pNextButton->SetEnabled( true );
	}
}

//-----------------------------------------------------------------------------
// Purpose: sets a chapter as selected
//-----------------------------------------------------------------------------
void CNewGameDialog::SetSelectedChapterIndex( int index )
{
	m_iSelectedChapter = index;

	for (int i = 0; i < m_ChapterPanels.Count(); i++)
	{
		if ( i == index )
		{
			m_ChapterPanels[i]->SetSelected( true );
		}
		else
		{
			m_ChapterPanels[i]->SetSelected( false );
		}
	}

	if ( m_pPlayButton )
	{
		m_pPlayButton->SetEnabled( true );
	}

	// Setup panels to the left of the selected panel
	int selectedSlot = index % 3 + 1;
	int currIdx = index;
	for ( int i = selectedSlot; i >= 0 && currIdx >= 0; --i )
	{
		m_PanelIndex[i] = currIdx;
		--currIdx;
		InitPanelIndexForDisplay( i );
	}

	// Setup panels to the right of the selected panel
	currIdx = index + 1;
	for ( int i = selectedSlot + 1; i < NUM_SLOTS && currIdx < m_ChapterPanels.Count(); ++i )
	{
		m_PanelIndex[i] = currIdx;
		++currIdx;
		InitPanelIndexForDisplay( i );
	}

	UpdateMenuComponents( SCROLL_NONE );
}

//-----------------------------------------------------------------------------
// Purpose: sets a chapter as selected
//-----------------------------------------------------------------------------
void CNewGameDialog::SetSelectedChapter( const char *chapter )
{
	Assert( chapter );
	for (int i = 0; i < m_ChapterPanels.Count(); i++)
	{
		if ( chapter && !Q_stricmp(m_ChapterPanels[i]->GetChapter(), chapter) )
		{
			m_iSelectedChapter = i;
			m_ChapterPanels[m_iSelectedChapter]->SetSelected( true );
		}
		else
		{
			m_ChapterPanels[i]->SetSelected( false );
		}
	}

	if ( m_pPlayButton )
	{
		m_pPlayButton->SetEnabled( true );
	}
}


//-----------------------------------------------------------------------------
// iUnlockedChapter - the value of sv_unlockedchapters, 1-based. A value of 0
//		is treated as a 1, since at least one chapter must be unlocked.
//
// i - the 1-based index of the chapter we're considering.
//-----------------------------------------------------------------------------
void CNewGameDialog::UpdatePanelLockedStatus( int iUnlockedChapter, int i, CGameChapterPanel *pChapterPanel )
{
	if ( iUnlockedChapter <= 0 )
	{
		iUnlockedChapter = 1;
	}

	// Commentary mode requires chapters to be finished before they can be chosen
	bool bLocked = false;

	if ( iUnlockedChapter < i )
	{
		// Never lock the first chapter
		bLocked = ( i != 0 );
	}

	pChapterPanel->SetEnabled( !bLocked );
}


//-----------------------------------------------------------------------------
// Purpose: After a scroll, each panel slot holds the index of a panel that has 
//			scrolled to an adjacent slot. This function updates each slot so
//			it holds the index of the panel that is actually in that slot's position.
//-----------------------------------------------------------------------------
void CNewGameDialog::ShiftPanelIndices( int offset )
{
	// Shift all the elements over one slot, then calculate what the last slot's index should be.
	int lastSlot = NUM_SLOTS - 1;
	if ( offset > 0 )
	{
		// Hide the panel that's dropping out of the slots
		if ( IsValidPanel( m_PanelIndex[0] ) )
		{
			m_ChapterPanels[ m_PanelIndex[0] ]->SetVisible( false );
		}

		// Scrolled panels to the right, so shift the indices one slot to the left
		Q_memmove( &m_PanelIndex[0], &m_PanelIndex[1], lastSlot * sizeof( m_PanelIndex[0] ) );
		if ( m_PanelIndex[lastSlot] != INVALID_INDEX )
		{
			int num = m_PanelIndex[ lastSlot ] + 1;
			if ( IsValidPanel( num ) )
			{
				m_PanelIndex[lastSlot] = num;
				InitPanelIndexForDisplay( lastSlot );
			}
			else
			{
				m_PanelIndex[lastSlot] = INVALID_INDEX;
			}
		}
	}
	else
	{
		// Hide the panel that's dropping out of the slots
		if ( IsValidPanel( m_PanelIndex[lastSlot] ) )
		{
			m_ChapterPanels[ m_PanelIndex[lastSlot] ]->SetVisible( false );
		}

		// Scrolled panels to the left, so shift the indices one slot to the right
		Q_memmove( &m_PanelIndex[1], &m_PanelIndex[0], lastSlot * sizeof( m_PanelIndex[0] ) );
		if ( m_PanelIndex[0] != INVALID_INDEX )
		{
			int num = m_PanelIndex[0] - 1;
			if ( IsValidPanel( num ) )
			{
				m_PanelIndex[0] = num;
				InitPanelIndexForDisplay( 0 );
			}
			else
			{
				m_PanelIndex[0] = INVALID_INDEX;
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: Validates an index into the selection panels vector
//-----------------------------------------------------------------------------
bool CNewGameDialog::IsValidPanel( const int idx )
{
	if ( idx < 0 || idx >= m_ChapterPanels.Count() )
		return false;
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Sets up a panel's properties before it is displayed
//-----------------------------------------------------------------------------
void CNewGameDialog::InitPanelIndexForDisplay( const int idx )
{
	CGameChapterPanel *panel = m_ChapterPanels[ m_PanelIndex[idx] ];
	if ( panel )
	{
		panel->SetPos( m_PanelXPos[idx], m_PanelYPos[idx] );
		panel->SetVisible( true );
		if ( m_PanelAlpha[idx] )
		{
			panel->SetZPos( 50 );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: Called when a scroll distance of one slot has been completed
//-----------------------------------------------------------------------------
void CNewGameDialog::FinishScroll( void )
{
	ShiftPanelIndices( m_ScrollDirection );
	m_bScrolling = false;
	m_ScrollCt = 0;
	
	// End of scroll step
	PostScroll( m_ScrollDirection );

	if ( m_PanelIndex[SLOT_CENTER-1] % 3 )
	{
		ScrollSelectionPanels( m_ScrollDirection );
	}
}

//-----------------------------------------------------------------------------
// Purpose: starts the game at the specified skill level
//-----------------------------------------------------------------------------
void CNewGameDialog::StartGame( void )
{
	if ( m_ChapterPanels.IsValidIndex( m_iSelectedChapter ) )
	{
		char mapcommand[512];
		mapcommand[0] = 0;
		Q_snprintf( mapcommand, sizeof( mapcommand ), "disconnect\nmaxplayers 1\ndeathmatch 0\nexec scripts/%s\n", m_ChapterPanels[m_iSelectedChapter]->GetConfigFile() );

		// start map
		engine->pfnClientCmd( mapcommand );

		OnClose();
	}
}

//-----------------------------------------------------------------------------
// Purpose: handles button commands
//-----------------------------------------------------------------------------
void CNewGameDialog::OnCommand( const char *command )
{
	if ( !Q_stricmp( command, "Play" ) )
	{
		if ( m_bMapStarting )
			return;

		StartGame();
	}
	else if ( !stricmp( command, "Next" ) )
	{
		if ( m_bMapStarting )
			return;

		ScrollSelectionPanels( SCROLL_LEFT );
	}
	else if ( !stricmp( command, "Prev" ) )
	{
		if ( m_bMapStarting )
			return;

		ScrollSelectionPanels( SCROLL_RIGHT );
	}
	else
	{
		BaseClass::OnCommand( command );
	}
}

void CNewGameDialog::OnKeyCodePressed( vgui::KeyCode code )
{
	switch ( code )
	{
	case KEY_LEFT:
		if ( !m_bScrolling )
		{
			for ( int i = 0; i < m_ChapterPanels.Count(); ++i )
			{
				if ( m_ChapterPanels[ i ]->IsSelected() )
				{
					int nNewChapter = i - 1;
					if ( nNewChapter >= 0 )
					{
						if ( nNewChapter < m_PanelIndex[ SLOT_LEFT ] && m_PanelIndex[ SLOT_LEFT ] != -1 )
						{
							ScrollSelectionPanels( SCROLL_RIGHT );
						}
						else if ( m_ChapterPanels[ nNewChapter ]->IsEnabled() )
						{
							SetSelectedChapterIndex( nNewChapter );
						}
					}
					break;
				}
			}
		}
		return;

	case KEY_RIGHT:
		if ( !m_bScrolling )
		{
			for ( int i = 0; i < m_ChapterPanels.Count(); ++i )
			{
				if ( m_ChapterPanels[ i ]->IsSelected() )
				{
					int nNewChapter = i + 1;
					if ( nNewChapter < m_ChapterPanels.Count() )
					{
						if ( nNewChapter > m_PanelIndex[ SLOT_RIGHT ] && m_PanelIndex[ SLOT_RIGHT ] != -1 )
						{
							ScrollSelectionPanels( SCROLL_LEFT );
						}
						else if ( m_ChapterPanels[ nNewChapter ]->IsEnabled() )
						{
							SetSelectedChapterIndex( nNewChapter );
						}
					}
					break;
				}
			}
		}
		return;
	}

	BaseClass::OnKeyCodePressed( code );
}

//-----------------------------------------------------------------------------
// Purpose: Called before a panel scroll starts.
//-----------------------------------------------------------------------------
void CNewGameDialog::PreScroll( EScrollDirection dir )
{
	int hideIdx = INVALID_INDEX;
	if ( dir == SCROLL_LEFT )
	{
		hideIdx = m_PanelIndex[SLOT_LEFT];
	}
	else if ( dir == SCROLL_RIGHT )
	{
		hideIdx = m_PanelIndex[SLOT_RIGHT];
	}
	if ( hideIdx != INVALID_INDEX )
	{
		// Push back the panel that's about to be hidden
		// so the next panel scrolls over the top of it.
		m_ChapterPanels[hideIdx]->SetZPos( 0 );
	}

	// Flip the active title label prior to the crossfade
	m_ActiveTitleIdx ^= 0x01;
}

//-----------------------------------------------------------------------------
// Purpose: Called after a panel scroll finishes.
//-----------------------------------------------------------------------------
void CNewGameDialog::PostScroll( EScrollDirection dir )
{
	int index = INVALID_INDEX;
	if ( dir == SCROLL_LEFT )
	{
		index = m_PanelIndex[SLOT_RIGHT];
	}
	else if ( dir == SCROLL_RIGHT )
	{
		index = m_PanelIndex[SLOT_LEFT];
	}

	// Fade in the revealed panel
	if ( index != INVALID_INDEX )
	{
		CGameChapterPanel *panel = m_ChapterPanels[index];
		panel->SetZPos( 50 );
	}

}

//-----------------------------------------------------------------------------
// Purpose: Initiates the scripted scroll and fade effects of all five slotted panels 
//-----------------------------------------------------------------------------
void CNewGameDialog::AnimateSelectionPanels( void )
{
	int idxOffset = 0;
	int startIdx = SLOT_LEFT;
	int endIdx = SLOT_RIGHT;

	// Don't scroll outside the bounds of the panel list
	if ( m_ScrollCt >= SCROLL_LEFT  )
	{
		idxOffset = -1;
		endIdx = SLOT_OFFRIGHT;
		m_ScrollDirection = SCROLL_LEFT;
	}
	else if ( m_ScrollCt <= SCROLL_RIGHT )
	{
		idxOffset = 1;
		startIdx = SLOT_OFFLEFT;
		m_ScrollDirection = SCROLL_RIGHT;
	}

	if ( 0 == idxOffset )
	{
		// Kill the scroll, it's outside the bounds
		m_ScrollCt = 0;
		m_bScrolling = false;
		m_ScrollDirection = SCROLL_NONE;
		vgui::surface()->PlaySound( "common/wpn_denyselect.wav" );
		return;
	}

	// Should never happen
	if ( startIdx > endIdx )
		return;

	for ( int i = startIdx; i <= endIdx; ++i )
	{
		if ( m_PanelIndex[i] != INVALID_INDEX )
		{
			int nextIdx = i + idxOffset;
			CGameChapterPanel *panel = m_ChapterPanels[ m_PanelIndex[i] ];
			panel->SetPos( m_PanelXPos[nextIdx], m_PanelYPos[nextIdx]);
		}
	}

	PostMessage( this, new KeyValues( "FinishScroll" ) );
}

//-----------------------------------------------------------------------------
// Purpose: Initiates a panel scroll and starts the animation.
//-----------------------------------------------------------------------------
void CNewGameDialog::ScrollSelectionPanels( EScrollDirection dir )
{
	// Only initiate a scroll if panels aren't currently scrolling
	if ( !m_bScrolling )
	{
		// Handle any pre-scroll setup
		PreScroll( dir );

		if ( dir == SCROLL_LEFT)
		{
			m_ScrollCt += SCROLL_LEFT;
		}
		else if ( dir == SCROLL_RIGHT && m_PanelIndex[SLOT_CENTER] != 0 )
		{
			m_ScrollCt += SCROLL_RIGHT;
		}

		m_bScrolling = true;
		AnimateSelectionPanels();

		// Update the arrow colors, help text, and buttons. Doing it here looks better than having
		// the components change after the entire scroll animation has finished.
		UpdateMenuComponents( m_ScrollDirection );
	}
}

