//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include <stdio.h>

#include "BasePanel.h"
#include "Taskbar.h"
#include "EngineInterface.h"

#include <vgui/IPanel.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui/IVGui.h>
#include <FileSystem.h>

#include "GameConsole.h"
#include "igameuifuncs.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CBasePanel::CBasePanel() : Panel(NULL, "BasePanel")
{
	m_eBackgroundState = BACKGROUND_NONE;
}

//-----------------------------------------------------------------------------
// Purpose: determines which is the topmost panel under the coordinates (x, y)
//-----------------------------------------------------------------------------
VPANEL CBasePanel::IsWithinTraverse( int x, int y, bool traversePopups)
{
	if ( IsVisible() )
		return BaseClass::IsWithinTraverse( x, y, false );

	return NULL;
}

//-----------------------------------------------------------------------------
// Purpose: Notifies the task bar that a new top-level panel has been created
//-----------------------------------------------------------------------------
void CBasePanel::OnChildAdded(VPANEL child)
{
	if (g_pTaskbar)
	{
		g_pTaskbar->AddTask(child);
	}
}

//-----------------------------------------------------------------------------
// Purpose: paints the main background image
//-----------------------------------------------------------------------------
void CBasePanel::PaintBackground()
{
	const char *levelName = engine->pfnGetLevelName();
	if (levelName && levelName[0])
	{
		// render filled background in game
		int swide, stall;
		surface()->GetScreenSize(swide, stall);
		surface()->DrawSetColor(0, 0, 0, 128);
		surface()->DrawFilledRect(0, 0, swide, stall);		
		return;	
	}

	switch (m_eBackgroundState)
	{
	case BACKGROUND_BLACK:
		{
			// if the loading dialog is visible, draw the background black
			int swide, stall;
			surface()->GetScreenSize(swide, stall);
			surface()->DrawSetColor(0, 0, 0, 255);
			surface()->DrawFilledRect(0, 0, swide, stall);		
		}
		break;

	case BACKGROUND_LOADING:
		DrawBackgroundImage();
		break;

	case BACKGROUND_DESKTOPIMAGE:
		DrawBackgroundImage();
		break;
		
	case BACKGROUND_LOADINGTRANSITION:
		{
		}
		break;

	case BACKGROUND_NONE:
	default:
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: loads background texture
//-----------------------------------------------------------------------------
void CBasePanel::ApplySchemeSettings(IScheme *pScheme)
{
	BaseClass::ApplySchemeSettings(pScheme);

	FileHandle_t file = filesystem()->Open( "resource/BackgroundLayout.txt", "rt" );
	if ( file == FILESYSTEM_INVALID_HANDLE )
		return;

	int fileSize = filesystem()->Size( file );

	char *buffer = (char *)_alloca( fileSize + 1 );

	filesystem()->Read( buffer, fileSize, file );

	filesystem()->Close( file );
	buffer[ fileSize ] = 0;

	int vid_level;
	gameuifuncs->GetCurrentRenderer( NULL, NULL, NULL, NULL, NULL, &vid_level );

	while ( buffer && *buffer )
	{
		char token[512];
		buffer = filesystem()->ParseFile( buffer, token, false );

		if ( !buffer || !token[0] )
			break;

		if ( stricmp( token, "resolution") )
		{
			bimage_t *bimage = &m_ImageID[ m_ImageID.AddToTail() ];
			bimage->imageID = surface()->CreateNewTextureID();

			// remove the text '.tga' from the file name to get the image name
			char *str = strstr( token, ".tga" );
			if (str)
			{
				*str = 0;
			}

			// turn on hardware filtering if we're scaling the images
			surface()->DrawSetTextureFile( bimage->imageID, token, vid_level == 0, false);
			surface()->DrawGetTextureSize( bimage->imageID, bimage->width, bimage->height );

			buffer = filesystem()->ParseFile( buffer, token, false );
			bimage->scaled = !stricmp( token, "scaled" );

			buffer = filesystem()->ParseFile( buffer, token, false );
			bimage->x = atoi( token );

			buffer = filesystem()->ParseFile( buffer, token, false );
			bimage->y = atoi( token );
		}
		else
		{
			buffer = filesystem()->ParseFile( buffer, token, false );
			m_iBaseResX = atoi( token );

			buffer = filesystem()->ParseFile( buffer, token, false );
			m_iBaseResY = atoi( token );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: sets how the game background should render
//-----------------------------------------------------------------------------
void CBasePanel::SetBackgroundRenderState(EBackgroundState state)
{
	m_eBackgroundState = state;
}

//-----------------------------------------------------------------------------
// Purpose: draws the background desktop image
//-----------------------------------------------------------------------------
void CBasePanel::DrawBackgroundImage()
{
	int wide, tall;
	GetSize(wide, tall);

	// work out scaling factors
	int swide, stall;
	surface()->GetScreenSize(swide, stall);
	float xScale, yScale;
	xScale = swide / (float)m_iBaseResX;
	yScale = stall / (float)m_iBaseResY;
	
	// iterate and draw all the background pieces
	for ( int x = 0; x < m_ImageID.Count(); x++ )
	{
		bimage_t *bimage = &m_ImageID[x];

		int dx = 0;
		if ( bimage->x )
		{
			dx = (int)ceil(bimage->x * xScale);
		}

		int dy = 0;
		if ( bimage->y )
		{
			dy = (int)ceil(bimage->y * yScale);
		}

		int dw, dt;
		if ( bimage->scaled )
		{
			dw = (int)ceil((bimage->x + bimage->width) * xScale);
			dt = (int)ceil((bimage->y + bimage->height) * yScale);
		}
		else
		{
			dw = dx + bimage->width;
			dt = dy + bimage->height;
		}

		// draw the color image only if the mono image isn't yet fully opaque
		surface()->DrawSetColor(255, 255, 255, 255);
		surface()->DrawSetTexture(bimage->imageID);
		surface()->DrawTexturedRect(dx, dy, dw, dt);
	}
}
