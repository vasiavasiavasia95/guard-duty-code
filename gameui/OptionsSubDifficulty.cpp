//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "OptionsSubDifficulty.h"
#include "EngineInterface.h"
#include <KeyValues.h>

#include "vgui_controls/RadioButton.h"

using namespace vgui;


//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
COptionsSubDifficulty::COptionsSubDifficulty(vgui::Panel *parent) : BaseClass(parent, NULL)
{
	m_pEasyRadio = new RadioButton(this, "Skill1Radio", "#GameUI_SkillEasy");
	m_pNormalRadio = new RadioButton(this, "Skill2Radio", "#GameUI_SkillNormal");
	m_pHardRadio = new RadioButton(this, "Skill3Radio", "#GameUI_SkillHard");

	LoadControlSettings("Resource/OptionsSubDifficulty.res");
}

//-----------------------------------------------------------------------------
// Purpose: resets controls
//-----------------------------------------------------------------------------
void COptionsSubDifficulty::OnResetData()
{
	int value = (int)engine->pfnGetCvarFloat( "skill" );

	if ( value == 1)
	{
		m_pEasyRadio->SetSelected(true);
	}
	else if ( value == 3)
	{
		m_pHardRadio->SetSelected(true);
	}
	else
	{
		m_pNormalRadio->SetSelected(true);
	}
}

//-----------------------------------------------------------------------------
// Purpose: sets data based on control settings
//-----------------------------------------------------------------------------
void COptionsSubDifficulty::OnApplyChanges()
{
	int value;

	if ( m_pEasyRadio->IsSelected() )
	{
		value = 1;
	}
	else if ( m_pHardRadio->IsSelected() )
	{
		value = 3;
	}
	else
	{
		value = 2;
	}

	engine->Cvar_SetValue( "skill", value );
}


//-----------------------------------------------------------------------------
// Purpose: enables apply button on radio buttons being pressed
//-----------------------------------------------------------------------------
void COptionsSubDifficulty::OnRadioButtonChecked()
{
	PostActionSignal(new KeyValues("ApplyButtonEnable"));
}
