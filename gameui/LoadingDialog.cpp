//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "LoadingDialog.h"
//#include "GameUISurface.h"
#include "EngineInterface.h"
#include "igameuifuncs.h"

#include <vgui/ISurface.h>
#include <vgui/ILocalize.h>
#include <vgui/ISystem.h>
#include <vgui_controls/ProgressBar.h>
#include <vgui_controls/Label.h>
#include <vgui_controls/Button.h>
#include <vgui_controls/HTML.h>

#include "GameUI_Interface.h"
#include "ModInfo.h"

// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CLoadingDialog::CLoadingDialog(vgui::Panel *parent) : Frame(parent, "LoadingDialog")
{
	SetSize(416, 100);
	SetTitle("#GameUI_Loading", true);
	m_bShowingSecondaryProgress = false;
	m_flSecondaryProgress = 0.0f;
	m_flLastSecondaryProgressUpdateTime = 0.0f;
	m_flSecondaryProgressStartTime = 0.0f;
	m_switchTime = 0.0f;
	m_flProgress = 0.0f;
	m_isHttpDownloadDialog = 0;
	m_bShowingVACInfo = 0;
	m_bShowingVACError = 0;
	m_szUserName[0] = 0;

	m_pProgress = new ProgressBar(this, "Progress");
	m_pProgress2 = new ProgressBar(this, "Progress2");
	m_pInfoLabel = new Label(this, "InfoLabel", "");
	m_pCancelButton = new Button(this, "CancelButton", "Cancel");
	m_pTimeRemainingLabel = new Label(this, "TimeRemainingLabel", "");
	m_pCancelButton->SetCommand("Cancel");

	m_pInfoLabel->SetBounds(20, 32, 392, 24);
	m_pProgress->SetBounds(20, 64, 300, 24); 
	m_pCancelButton->SetBounds(330, 64, 72, 24);

	m_pInfoLabel->SetTextColorState(Label::CS_DULL);

	SetMinimizeButtonVisible(false);
	SetMaximizeButtonVisible(false);
	SetCloseButtonVisible(false);
	SetSizeable(false);
	SetMoveable(false);
	m_pProgress2->SetVisible(false);

	/*if ( engine->CheckParm("-steam", NULL ) || engine->CheckParm("-showplatform", NULL))
	{
		if ( gameuifuncs->IsConnectedToVACSecureServer() )
		{
			LoadControlSettings("Resource/LoadingDialogVAC.res");
			m_bShowingVACInfo = true;
		}
		else
		{
			LoadControlSettings("Resource/LoadingDialog.res");
		}
	}
	else if ( ModInfo().BShowSimpleLoadingDialog() )
	{
		LoadControlSettings("Resource/LoadingDialogNoBannerSingle.res");
	}
	else
	{
		LoadControlSettings("Resource/LoadingDialogNoBanner.res");
	}*/

	// Vasia: Only use singleplayer version of the dialog
	LoadControlSettings("Resource/LoadingDialogNoBannerSingle.res");
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CLoadingDialog::~CLoadingDialog()
{
}

//-----------------------------------------------------------------------------
// Purpose: Activates the loading screen, initializing and making it visible
//-----------------------------------------------------------------------------
void CLoadingDialog::DisplayProgressBar(const char *resourceType, const char *resourceName)
{
	SetTitle("#GameUI_Loading", true);
	vgui::surface()->RestrictPaintToSinglePanel(GetVPanel());
	vgui::surface()->SetModalPanel(GetVPanel());
	BaseClass::Activate();

	m_pProgress->SetVisible(true);
	if ( !ModInfo().BShowSimpleLoadingDialog() )
	{
		m_pInfoLabel->SetVisible(true);
	}
	m_pInfoLabel->SetText("");
	
	m_pCancelButton->SetText("Cancel");
	m_pCancelButton->SetCommand("Cancel");
}

//-----------------------------------------------------------------------------
// Purpose: error display file
//-----------------------------------------------------------------------------
void CLoadingDialog::SetupControlSettingsForErrorDisplay( const char *settingsFile )
{
	SetTitle("#GameUI_Disconnected", true);
	m_pInfoLabel->SetText("");
	LoadControlSettings( settingsFile );
	vgui::surface()->RestrictPaintToSinglePanel(GetVPanel());
	vgui::surface()->SetModalPanel(GetVPanel());

	BaseClass::Activate();
	
	m_pProgress->SetVisible(false);

	m_pInfoLabel->SetVisible(true);
	m_pCancelButton->SetText("#GameUI_Close");
	m_pCancelButton->SetCommand("Close");
	m_pInfoLabel->InvalidateLayout();
}

//-----------------------------------------------------------------------------
// Purpose: Turns dialog into error display
//-----------------------------------------------------------------------------
void CLoadingDialog::DisplayGenericError(const char *failureReason, const char *extendedReason)
{
	if ( m_bShowingVACError )
	{
		m_bShowingVACError = false;
		return;
	}

	SetupControlSettingsForErrorDisplay("Resource/LoadingDialogError.res");

	if ( extendedReason && strlen( extendedReason ) > 0 ) 
	{
		wchar_t compositeReason[256], finalMsg[512], formatStr[256];
		if ( extendedReason[0] == '#' )
		{
			wcsncpy(compositeReason, localize()->Find(extendedReason), sizeof( compositeReason ) / sizeof( wchar_t ) );
		}
		else
		{
			vgui::localize()->ConvertANSIToUnicode(extendedReason, compositeReason, sizeof( compositeReason ));
		}

		if ( failureReason[0] == '#' )
		{
			wcsncpy(formatStr, localize()->Find(failureReason), sizeof( formatStr ) / sizeof( wchar_t ) );
		}
		else
		{
			vgui::localize()->ConvertANSIToUnicode(failureReason, formatStr, sizeof( formatStr ));
		}

		vgui::localize()->ConstructString(finalMsg, sizeof( finalMsg ), formatStr, 1, compositeReason);
		m_pInfoLabel->SetText(finalMsg);
	}
	else
	{
		m_pInfoLabel->SetText(failureReason);
	}
}

//-----------------------------------------------------------------------------
// Purpose: explain to the user they can't join secure servers due to a VAC ban
//-----------------------------------------------------------------------------
void CLoadingDialog::DisplayVACBannedError()
{
	SetupControlSettingsForErrorDisplay("Resource/LoadingDialogErrorVACBanned.res");
	SetTitle("#VAC_ConnectionRefusedTitle", true);
	m_bShowingVACError = true;
}

//-----------------------------------------------------------------------------
// Purpose: explain to the user they can't connect to public servers due to 
//			not having a valid connection to Steam
//			this should only happen if they are a pirate
//-----------------------------------------------------------------------------
void CLoadingDialog::DisplayNoSteamConnectionError()
{
	SetupControlSettingsForErrorDisplay("Resource/LoadingDialogErrorNoSteamConnection.res");
	m_bShowingVACError = true;
}

void CLoadingDialog::DisplaySteamConnectionError(const char *username, ESteamLoginFailure steamLoginFailure)
{
	switch (steamLoginFailure)
	{
	case STEAMLOGINFAILURE_CONNECTIONLOST:
		SetupControlSettingsForErrorDisplay("Resource/LoadingDialogConnectionLost.res");
		break;
	case STEAMLOGINFAILURE_NOCONNECTION:
		SetupControlSettingsForErrorDisplay("Resource/LoadingDialogErrorNoSteamConnection.res");
		break;
	case STEAMLOGINFAILURE_LOGGED_IN_ELSEWHERE:
		SetupControlSettingsForErrorDisplay("Resource/LoadingDialogErrorLoggedInElsewhere.res");
		break;
	}

	m_bShowingVACError = true;
	m_pCancelButton->SetText("#GameUI_Close");
	m_pCancelButton->SetCommand("Close");
	strncpy( m_szUserName, username, sizeof(m_szUserName) );
	m_szUserName[63] = 0;
}

//-----------------------------------------------------------------------------
// Purpose: sets status info text
//-----------------------------------------------------------------------------
void CLoadingDialog::SetStatusText(const char *statusText)
{
	m_pInfoLabel->SetText(statusText);
}

//-----------------------------------------------------------------------------
// Purpose: updates time remaining
//-----------------------------------------------------------------------------
void CLoadingDialog::OnThink()
{
	BaseClass::OnThink();
	if (m_bShowingSecondaryProgress)
	{
		// calculate the time remaining string
		wchar_t unicode[512];
		if (m_flSecondaryProgress >= 1.0f)
		{
			m_pTimeRemainingLabel->SetText("complete");
		}
		else if (ProgressBar::ConstructTimeRemainingString(unicode, sizeof(unicode), m_flSecondaryProgressStartTime, (float)system()->GetFrameTime(), m_flSecondaryProgress, m_flLastSecondaryProgressUpdateTime, true))
		{
			m_pTimeRemainingLabel->SetText(unicode);
		}
		else
		{
			m_pTimeRemainingLabel->SetText("");
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLoadingDialog::PerformLayout()
{
	MoveToCenterOfScreen();
	BaseClass::PerformLayout();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLoadingDialog::SetProgressPoint(int progressPoint)
{
	m_pProgress->SetProgress((float)progressPoint / (m_iRangeMax - m_iRangeMin));
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLoadingDialog::SetProgressRange(int min, int max)
{
	m_iRangeMin = min;
	m_iRangeMax = max;
}

//-----------------------------------------------------------------------------
// Purpose: sets and shows the secondary progress bar
//-----------------------------------------------------------------------------
void CLoadingDialog::SetSecondaryProgress(float progress)
{
	// don't show the progress if we've jumped right to completion
	if (!m_bShowingSecondaryProgress && progress > 0.99f)
		return;

	// if we haven't yet shown secondary progress then reconfigure the dialog
	if (!m_bShowingSecondaryProgress)
	{
		if ( m_bShowingVACInfo || gameuifuncs->IsConnectedToVACSecureServer() )
		{
			LoadControlSettings("Resource/LoadingDialogDualProgressVAC.res");
			m_bShowingVACInfo = true;
		}
		else
		{
			LoadControlSettings("Resource/LoadingDialogDualProgress.res");
		}

		m_bShowingSecondaryProgress = true;
		m_pProgress2->SetVisible(true);
		m_flSecondaryProgressStartTime = (float)system()->GetFrameTime();
	}

	// if progress has increased then update the progress counters
	if (progress > m_flSecondaryProgress)
	{
		m_pProgress2->SetProgress(progress);
		m_flSecondaryProgress = progress;
		m_flLastSecondaryProgressUpdateTime = (float)system()->GetFrameTime();
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLoadingDialog::SetSecondaryProgressText(const char *statusText)
{
	SetControlString("SecondaryProgressLabel", statusText);
}

void CLoadingDialog::SwitchToHttpDownloadDialog(const char *url)
{
	m_isHttpDownloadDialog = true;
	LoadControlSettings("Resource/LoadingDialog.res");
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLoadingDialog::OnClose()
{
	// remove any rendering restrictions
	vgui::surface()->RestrictPaintToSinglePanel(NULL);
	vgui::surface()->SetModalPanel(NULL);

	BaseClass::OnClose();

	// delete ourselves
	MarkForDeletion();
}

//-----------------------------------------------------------------------------
// Purpose: command handler
//-----------------------------------------------------------------------------
void CLoadingDialog::OnCommand(const char *command)
{
	if (!stricmp(command, "Cancel"))
	{
		if ( m_isHttpDownloadDialog )
		{
			// cancel any download
			engine->pfnClientCmd("httpstop\n");
		}
		else
		{ 
			// disconnect from the server
			engine->pfnClientCmd("disconnect\n");
		}

		// close
		Close();
	}
	else
	{
		BaseClass::OnCommand(command);
	}
}

//-----------------------------------------------------------------------------
// Purpose: Maps ESC to quiting loading
//-----------------------------------------------------------------------------
void CLoadingDialog::OnKeyCodePressed(KeyCode code)
{
	if (code == KEY_ESCAPE)
	{
		OnCommand("Cancel");
	}
	else
	{
		BaseClass::OnKeyCodePressed(code);
	}
}

//-----------------------------------------------------------------------------
// Purpose: Singleton accessor
//-----------------------------------------------------------------------------
extern vgui::DHANDLE<CLoadingDialog> g_hLoadingDialog;
vgui::Panel *LoadingDialog()
{
	return g_hLoadingDialog.Get();
}