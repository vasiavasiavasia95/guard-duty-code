//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef BASEPANEL_H
#define BASEPANEL_H
#ifdef _WIN32
#pragma once
#endif

#include <vgui_controls/Panel.h>

//-----------------------------------------------------------------------------
// Purpose: The panel at the top of the vgui panel hierarchy
//-----------------------------------------------------------------------------
class CBasePanel : public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CBasePanel, vgui::Panel );

public:
	CBasePanel();

	virtual vgui::VPANEL IsWithinTraverse(int x, int y, bool traversePopups);
	virtual void OnChildAdded(vgui::VPANEL child);
	virtual void PaintBackground();
	virtual void ApplySchemeSettings(vgui::IScheme *pScheme);

	enum EBackgroundState
	{
		BACKGROUND_NONE,
		BACKGROUND_BLACK,
		BACKGROUND_DESKTOPIMAGE,
		BACKGROUND_LOADING,
		BACKGROUND_LOADINGTRANSITION,
	};

	void SetBackgroundRenderState(EBackgroundState state);

private:
	void DrawBackgroundImage();

	EBackgroundState m_eBackgroundState;
	
	struct bimage_t
	{
		int imageID;
		int x, y;
		int width, height;
		bool scaled;
	};
	CUtlVector<bimage_t> m_ImageID;

	int m_iBaseResX;
	int m_iBaseResY;
};


#endif // BASEPANEL_H
