/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef ENGINECALLBACK_H
#define ENGINECALLBACK_H
#pragma once

#ifndef EIFACE_H
#include "eiface.h"
#endif

#include "event_flags.h"

typedef struct enginefuncs_s enginefuncs_t;					// include eiface.h
typedef struct server_studio_api_s server_studio_api_t;		// include r_studioint.h
class IFileSystem;											// include filesystem.h

extern enginefuncs_t			*engine;
extern server_studio_api_t		IEngineStudio;
extern IFileSystem				*g_pFileSystem;

// The actual engine callbacks
#define GETPLAYERUSERID (*engine->pfnGetPlayerUserId)
#define PRECACHE_MODEL	(*engine->pfnPrecacheModel)
#define PRECACHE_SOUND	(*engine->pfnPrecacheSound)
#define PRECACHE_GENERIC	(*engine->pfnPrecacheGeneric)
#define SET_MODEL		(*engine->pfnSetModel)
#define MODEL_INDEX		(*engine->pfnModelIndex)
#define MODEL_FRAMES	(*engine->pfnModelFrames)
#define SET_SIZE		(*engine->pfnSetSize)
#define CHANGE_LEVEL	(*engine->pfnChangeLevel)
#define GET_SPAWN_PARMS	(*engine->pfnGetSpawnParms)
#define SAVE_SPAWN_PARMS (*engine->pfnSaveSpawnParms)
#define VEC_TO_YAW		(*engine->pfnVecToYaw)
#define VEC_TO_ANGLES	(*engine->pfnVecToAngles)
#define MOVE_TO_ORIGIN  (*engine->pfnMoveToOrigin)
#define oldCHANGE_YAW		(*engine->pfnChangeYaw)
#define CHANGE_PITCH	(*engine->pfnChangePitch)
#define MAKE_VECTORS	(*engine->pfnMakeVectors)
#define CREATE_ENTITY	(*engine->pfnCreateEntity)
#define REMOVE_ENTITY	(*engine->pfnRemoveEntity)
#define CREATE_NAMED_ENTITY		(*engine->pfnCreateNamedEntity)
#define MAKE_STATIC		(*engine->pfnMakeStatic)
#define ENT_IS_ON_FLOOR	(*engine->pfnEntIsOnFloor)
#define DROP_TO_FLOOR	(*engine->pfnDropToFloor)
#define WALK_MOVE		(*engine->pfnWalkMove)
#define SET_ORIGIN		(*engine->pfnSetOrigin)
#define EMIT_SOUND_DYN2 (*engine->pfnEmitSound)
#define BUILD_SOUND_MSG (*engine->pfnBuildSoundMsg)
#define TRACE_LINE		(*engine->pfnTraceLine)
#define TRACE_TOSS		(*engine->pfnTraceToss)
#define TRACE_MONSTER_HULL		(*engine->pfnTraceMonsterHull)
#define TRACE_HULL		(*engine->pfnTraceHull)
#define GET_AIM_VECTOR	(*engine->pfnGetAimVector)
#define SERVER_COMMAND	(*engine->pfnServerCommand)
#define SERVER_EXECUTE	(*engine->pfnServerExecute)
#define CLIENT_COMMAND	(*engine->pfnClientCommand)
#define PARTICLE_EFFECT	(*engine->pfnParticleEffect)
#define LIGHT_STYLE		(*engine->pfnLightStyle)
#define DECAL_INDEX		(*engine->pfnDecalIndex)
#define POINT_CONTENTS	(*engine->pfnPointContents)
#define CRC32_INIT           (*engine->pfnCRC32_Init)
#define CRC32_PROCESS_BUFFER (*engine->pfnCRC32_ProcessBuffer)
#define CRC32_PROCESS_BYTE   (*engine->pfnCRC32_ProcessByte)
#define CRC32_FINAL          (*engine->pfnCRC32_Final)
#define RANDOM_LONG		(*engine->pfnRandomLong)
#define RANDOM_FLOAT	(*engine->pfnRandomFloat)
#define GETPLAYERAUTHID	(*engine->pfnGetPlayerAuthId)

inline void MESSAGE_BEGIN( int msg_dest, int msg_type, const float *pOrigin = NULL, edict_t *ed = NULL ) {
	(*engine->pfnMessageBegin)(msg_dest, msg_type, pOrigin, ed);
}
#define MESSAGE_END		(*engine->pfnMessageEnd)
#define WRITE_BYTE		(*engine->pfnWriteByte)
#define WRITE_CHAR		(*engine->pfnWriteChar)
#define WRITE_SHORT		(*engine->pfnWriteShort)
#define WRITE_LONG		(*engine->pfnWriteLong)
#define WRITE_ANGLE		(*engine->pfnWriteAngle)
#define WRITE_COORD		(*engine->pfnWriteCoord)
#define WRITE_STRING	(*engine->pfnWriteString)
#define WRITE_ENTITY	(*engine->pfnWriteEntity)
#define CVAR_REGISTER	(*engine->pfnCVarRegister)
#define CVAR_GET_FLOAT	(*engine->pfnCVarGetFloat)
#define CVAR_GET_STRING	(*engine->pfnCVarGetString)
#define CVAR_SET_FLOAT	(*engine->pfnCVarSetFloat)
#define CVAR_SET_STRING	(*engine->pfnCVarSetString)
#define CVAR_GET_POINTER (*engine->pfnCVarGetPointer)
#define ALERT			(*engine->pfnAlertMessage)
#define ENGINE_FPRINTF	(*engine->pfnEngineFprintf)
#define ALLOC_PRIVATE	(*engine->pfnPvAllocEntPrivateData)
#define FREE_PRIVATE	(*engine->pfnFreeEntPrivateData)
//#define STRING			(*engine->pfnSzFromIndex)
#define ALLOC_STRING	(*engine->pfnAllocString)
#define FIND_ENTITY_BY_STRING	(*engine->pfnFindEntityByString)
#define GETENTITYILLUM	(*engine->pfnGetEntityIllum)
#define FIND_ENTITY_IN_SPHERE		(*engine->pfnFindEntityInSphere)
#define FIND_CLIENT_IN_PVS			(*engine->pfnFindClientInPVS)
#define EMIT_AMBIENT_SOUND			(*engine->pfnEmitAmbientSound)
#define GET_MODEL_PTR				(*engine->pfnGetModelPtr)
#define REG_USER_MSG				(*engine->pfnRegUserMsg)
#define GET_BONE_POSITION			(*engine->pfnGetBonePosition)
#define FUNCTION_FROM_NAME			(*engine->pfnFunctionFromName)
#define NAME_FOR_FUNCTION			(*engine->pfnNameForFunction)
#define TRACE_TEXTURE				(*engine->pfnTraceTexture)
#define CLIENT_PRINTF				(*engine->pfnClientPrintf)
#define CMD_ARGS					(*engine->pfnCmd_Args)
#define CMD_ARGC					(*engine->pfnCmd_Argc)
#define CMD_ARGV					(*engine->pfnCmd_Argv)
#define GET_ATTACHMENT			(*engine->pfnGetAttachment)
#define SET_VIEW				(*engine->pfnSetView)
#define SET_CROSSHAIRANGLE		(*engine->pfnCrosshairAngle)
#define LOAD_FILE_FOR_ME		(*engine->pfnLoadFileForMe)
#define FREE_FILE				(*engine->pfnFreeFile)
#define COMPARE_FILE_TIME		(*engine->pfnCompareFileTime)
#define GET_GAME_DIR			(*engine->pfnGetGameDir)
#define IS_MAP_VALID			(*engine->pfnIsMapValid)
#define NUMBER_OF_ENTITIES		(*engine->pfnNumberOfEntities)
#define IS_DEDICATED_SERVER		(*engine->pfnIsDedicatedServer)

#define PRECACHE_EVENT			(*engine->pfnPrecacheEvent)
#define PLAYBACK_EVENT_FULL		(*engine->pfnPlaybackEvent)

#define ENGINE_SET_PVS			(*engine->pfnSetFatPVS)
#define ENGINE_SET_PAS			(*engine->pfnSetFatPAS)

#define ENGINE_CHECK_VISIBILITY (*engine->pfnCheckVisibility)

#define DELTA_SET				( *engine->pfnDeltaSetField )
#define DELTA_UNSET				( *engine->pfnDeltaUnsetField )
#define DELTA_ADDENCODER		( *engine->pfnDeltaAddEncoder )
#define ENGINE_CURRENT_PLAYER   ( *engine->pfnGetCurrentPlayer )

#define	ENGINE_CANSKIP			( *engine->pfnCanSkipPlayer )

#define DELTA_FINDFIELD			( *engine->pfnDeltaFindField )
#define DELTA_SETBYINDEX		( *engine->pfnDeltaSetFieldByIndex )
#define DELTA_UNSETBYINDEX		( *engine->pfnDeltaUnsetFieldByIndex )

#define ENGINE_GETPHYSINFO		( *engine->pfnGetPhysicsInfoString )

#define ENGINE_SETGROUPMASK		( *engine->pfnSetGroupMask )

#define ENGINE_INSTANCE_BASELINE ( *engine->pfnCreateInstancedBaseline )

#define ENGINE_FORCE_UNMODIFIED	( *engine->pfnForceUnmodified )

#define PLAYER_CNX_STATS		( *engine->pfnGetPlayerStats )

#endif		//ENGINECALLBACK_H
