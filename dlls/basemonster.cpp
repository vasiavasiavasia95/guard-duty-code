//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "basemonster.h"
#include "nodes.h"
#include "monsters.h"
#include "animation.h"
#include "bitvec.h"
#include "ai_default.h"
#include "ai_schedule.h"
#include "ai_task.h"
#include "ai_senses.h"
#include "ai_squadslot.h"
#include "ai_memory.h"
#include "ai_squad.h"
#include "saverestore.h"
#include "saverestore_utlvector.h"
#include "weapons.h"
#include "scripted.h"
#include "squadmonster.h"
#include "decals.h"
#include "soundent.h"
#include "gamerules.h"
#include "player.h"

#define MONSTER_CUT_CORNER_DIST		8 // 8 means the monster's bounding box is contained without the box of the node in WC

Vector VecBModelOrigin( entvars_t* pevBModel );

extern DLL_GLOBAL	bool	g_fDrawLines;
extern DLL_GLOBAL	short	g_sModelIndexLaser;// holds the index for the laser beam
extern DLL_GLOBAL	short	g_sModelIndexLaserDot;// holds the index for the laser beam dot

extern CGraph WorldGraph;// the world node graph

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CAI_Manager g_AI_Manager;

//-------------------------------------

CAI_Manager::CAI_Manager()
{
	m_AIs.EnsureCapacity( MAX_AIS );
}

//-------------------------------------

CBaseMonster **CAI_Manager::AccessAIs()
{
	if (m_AIs.Count())
		return &m_AIs[0];
	return NULL;
}

//-------------------------------------

int CAI_Manager::NumAIs()
{
	return m_AIs.Count();
}

//-------------------------------------

void CAI_Manager::AddAI( CBaseMonster *pAI )
{
	m_AIs.AddToTail( pAI );
}

//-------------------------------------

void CAI_Manager::RemoveAI( CBaseMonster *pAI )
{
	int i = m_AIs.Find( pAI );

	Assert( i != -1 );

	if ( i != -1 )
		m_AIs.FastRemove( i );
}

//-----------------------------------------------------------------------------

// ================================================================
//  Init static data
// ================================================================
int					CBaseMonster::m_nDebugBits = 0;
CBaseMonster*		CBaseMonster::m_pDebugMonster = NULL;
int					CBaseMonster::m_nDebugPauseIndex = -1;

CAI_ClassScheduleIdSpace	CBaseMonster::gm_ClassScheduleIdSpace( true );
CAI_GlobalScheduleNamespace CBaseMonster::gm_SchedulingSymbols;
CAI_LocalIdSpace			CBaseMonster::gm_SquadSlotIdSpace( true );

// ================================================================
//  Class Methods
// ================================================================

//-----------------------------------------------------------------------------
// Purpose: Static debug function to clear schedules for all monsters
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CBaseMonster::ClearAllSchedules( void )
{
	for ( int i = 0; i < g_AI_Manager.NumAIs(); i++ )
	{
		CBaseMonster *pMonster = (g_AI_Manager.AccessAIs())[i];
		if ( pMonster )
		{
			pMonster->ClearSchedule();
			pMonster->RouteClear();
		}
	}
}

// Global Savedata for monster
//
// This should be an exact copy of the var's in the header.  Fields
// that aren't save/restored are commented out

BEGIN_DATADESC( CBaseMonster )

	//								m_pSchedule  (reacquired on restore)
	DEFINE_EMBEDDED( CBaseMonster,	m_ScheduleState ),
	DEFINE_FIELD( CBaseMonster, m_failSchedule,				FIELD_INTEGER ),
	//								m_bDoPostRestoreRefindPath (not saved)
	//								m_Conditions (custom save)
	//								m_CustomInterruptConditions (custom save)
	//								m_ConditionsPreIgnore (custom save)
	//								m_InverseIgnoreConditions (custom save)
	DEFINE_FIELD( CBaseMonster, m_bForceConditionsGather,	FIELD_BOOLEAN ),
	DEFINE_FIELD( CBaseMonster,	m_bConditionsGathered,		FIELD_BOOLEAN ),
	DEFINE_FIELD( CBaseMonster, m_bSkippedChooseEnemy,		FIELD_BOOLEAN ),
	DEFINE_FIELD( CBaseMonster, m_MonsterState,				FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_IdealMonsterState,		FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_Activity,					FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_IdealActivity,			FIELD_INTEGER ),
	DEFINE_EMBEDDEDBYREF( CBaseMonster,	m_pSenses ),
	DEFINE_FIELD( CBaseMonster, m_hEnemy,					FIELD_EHANDLE ),
	DEFINE_FIELD( CBaseMonster, m_hTargetEnt,				FIELD_EHANDLE ),
	DEFINE_SIMTIMER( CBaseMonster,	m_GiveUpOnDeadEnemyTimer ),
	DEFINE_FIELD( CBaseMonster, m_EnemiesSerialNumber,		FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster,	m_flAcceptableTimeSeenEnemy, FIELD_TIME ),
	DEFINE_FIELD( CBaseMonster, m_flFieldOfView,			FIELD_FLOAT ),
	DEFINE_FIELD( CBaseMonster, m_flWaitFinished,			FIELD_TIME ),
	DEFINE_FIELD( CBaseMonster, m_afCapability,				FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster,	m_flSoundWaitTime,			FIELD_TIME ),
	DEFINE_FIELD( CBaseMonster, m_flMoveWaitFinished,		FIELD_TIME ),
	DEFINE_UTLVECTOR( CBaseMonster, m_UnreachableEnts,		FIELD_EMBEDDED ),
	DEFINE_FIELD( CBaseMonster, m_scriptState,				FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_pCine,					FIELD_CLASSPTR ),
	DEFINE_FIELD( CBaseMonster, m_afMemory,					FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster,	m_hEnemyOccluder,			FIELD_EHANDLE ),
	// 							m_pSquad					Saved specially in ai_saverestore.cpp
	DEFINE_FIELD( CBaseMonster,	m_iMySquadSlot,				FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_vecLastPosition,			FIELD_POSITION_VECTOR ),
	DEFINE_FIELD( CBaseMonster, m_iHintNode,				FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_cAmmoLoaded,				FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_flDistTooFar,				FIELD_FLOAT ),
	DEFINE_FIELD( CBaseMonster,	m_pGoalEnt,					FIELD_CLASSPTR ),
	
	// Since schedules are now save/restored we need to save this too
	DEFINE_FIELD( CBaseMonster, m_movementGoal,				FIELD_INTEGER ),
	//DEFINE_FIELD( CBaseMonster, m_iRouteIndex,			FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_moveWaitTime,				FIELD_FLOAT ),

	DEFINE_FIELD( CBaseMonster, m_vecMoveGoal,				FIELD_POSITION_VECTOR ),
	DEFINE_FIELD( CBaseMonster, m_movementActivity,			FIELD_INTEGER ),

	DEFINE_FIELD( CBaseMonster, m_flHungryTime,				FIELD_TIME ),
	
	DEFINE_FIELD( CBaseMonster, m_iTriggerCondition,		FIELD_INTEGER ),
	DEFINE_FIELD( CBaseMonster, m_iszTriggerTarget,			FIELD_STRING ),
	// 							m_nDebugCurIndex			DEBUG

END_DATADESC()

BEGIN_SIMPLE_DATADESC( AIScheduleState_t )
	DEFINE_FIELD( AIScheduleState_t,	iCurTask,				FIELD_INTEGER ),
	DEFINE_FIELD( AIScheduleState_t,	fTaskStatus,			FIELD_INTEGER ),
	DEFINE_FIELD( AIScheduleState_t,	timeStarted,			FIELD_TIME ),
	DEFINE_FIELD( AIScheduleState_t,	timeCurTaskStarted,		FIELD_TIME ),
	DEFINE_FIELD( AIScheduleState_t,	bScheduleWasInterrupted, FIELD_BOOLEAN ),
END_DATADESC()

//-------------------------------------

BEGIN_SIMPLE_DATADESC( UnreachableEnt_t )
	DEFINE_FIELD( UnreachableEnt_t,		hUnreachableEnt,			FIELD_EHANDLE	),
	DEFINE_FIELD( UnreachableEnt_t,		fExpireTime,				FIELD_TIME		),
	DEFINE_FIELD( UnreachableEnt_t,		vLocationWhenUnreachable,	FIELD_POSITION_VECTOR	),
END_DATADESC()

// Vasia: This seems to be a really ancient piece of code!!!
// Monsters can use new schedules without calling this
void CBaseMonster::Precache( void )
{
	// Make sure schedules are loaded for this monster type
	if ( !LoadedSchedules() )
	{
		ALERT( at_error, "Rejecting spawn of %s as error in monster's schedules.\n", GetDebugName() );
		UTIL_Remove( this );
		return;
	}
}

//-----------------------------------------------------------------------------

const short AI_EXTENDED_SAVE_HEADER_VERSION = 3;

struct AIExtendedSaveHeader_t
{
	AIExtendedSaveHeader_t()
	:	version(AI_EXTENDED_SAVE_HEADER_VERSION), 
		flags(0),
		scheduleCrc(0)
	{
		szSchedule[0] = 0;
		szSequence[0] = 0;
	}

	short version;
	unsigned flags;
	char szSchedule[128];
	CRC32_t scheduleCrc;
	char szSequence[128];

	DECLARE_SIMPLE_DATADESC();
};

enum AIExtendedSaveHeaderFlags_t
{
	AIESH_HAD_ENEMY		= 0x01,
	AIESH_HAD_TARGET	= 0x02,
	AIESH_HAD_NAVGOAL	= 0x04,
};

//-------------------------------------

BEGIN_SIMPLE_DATADESC( AIExtendedSaveHeader_t )
	DEFINE_FIELD( AIExtendedSaveHeader_t, version, FIELD_SHORT ),
	DEFINE_FIELD( AIExtendedSaveHeader_t, flags, FIELD_INTEGER ),
	DEFINE_AUTO_ARRAY( AIExtendedSaveHeader_t, szSchedule, FIELD_CHARACTER ),
	DEFINE_FIELD( AIExtendedSaveHeader_t, scheduleCrc, FIELD_INTEGER ),
	DEFINE_AUTO_ARRAY( AIExtendedSaveHeader_t, szSequence, FIELD_CHARACTER ),
END_DATADESC()

//-------------------------------------

int CBaseMonster::Save( CSave &save )
{
	if ( !BaseClass::Save(save) )
		return 0;

	AIExtendedSaveHeader_t saveHeader;
	
	if ( GetEnemy() )
		saveHeader.flags |= AIESH_HAD_ENEMY;
	if ( m_hTargetEnt )
		saveHeader.flags |= AIESH_HAD_TARGET;
	if ( !FRouteClear() )
		saveHeader.flags |= AIESH_HAD_NAVGOAL;
	
	if ( m_pSchedule )
	{
		const char *pszSchedule = m_pSchedule->GetName();

		Assert( strlen( pszSchedule ) < sizeof( saveHeader.szSchedule ) - 1 );
		strncpy( saveHeader.szSchedule, pszSchedule, sizeof( saveHeader.szSchedule ) - 1 );
		saveHeader.szSchedule[sizeof(saveHeader.szSchedule) - 1] = 0;

		CRC32_INIT( &saveHeader.scheduleCrc );
		CRC32_PROCESS_BUFFER( &saveHeader.scheduleCrc, (void *)m_pSchedule->GetTaskList(), m_pSchedule->NumTasks() * sizeof(Task_t) );
		CRC32_FINAL( saveHeader.scheduleCrc );
	}
	else
	{
		saveHeader.szSchedule[0] = 0;
		saveHeader.scheduleCrc = 0;
	}

	if ( pev->sequence != ACT_INVALID && GetModelPtr() )
	{
		const char *pszSequenceName = GetSequenceName( pev->sequence );
		if ( pszSequenceName && *pszSequenceName )
		{
			Assert( strlen( pszSequenceName ) < sizeof( saveHeader.szSequence ) - 1 );
			strncpy( saveHeader.szSequence, pszSequenceName, sizeof(saveHeader.szSequence) );
		}
	}

	save.WriteAll( &saveHeader );

	// save conditions
	save.StartBlock();
		SaveConditions( save, m_Conditions );
		SaveConditions( save, m_CustomInterruptConditions );
		SaveConditions( save, m_ConditionsPreIgnore );
		CAI_ScheduleBits ignoreConditions;
		m_InverseIgnoreConditions.Not( &ignoreConditions );
		SaveConditions( save, ignoreConditions );
	save.EndBlock();

	// success
	return 1;
}

//-------------------------------------

void CBaseMonster::DiscardScheduleState()
{
	// We don't save/restore routes yet
	RouteClear();

	// We don't save/restore schedules yet
	ClearSchedule();

	// Reset animation
	m_Activity = ACT_RESET;

	// If we don't have an enemy, clear conditions like see enemy, etc.
	if ( GetEnemy() == NULL )
	{
		m_Conditions.ClearAll();
	}

	// went across a transition and lost my m_pCine
	bool bLostScript = ( m_MonsterState == MONSTERSTATE_SCRIPT && m_pCine == NULL );
	if ( bLostScript )
	{
		// UNDONE: Do something better here?
		// for now, just go back to idle and let the AI figure out what to do.
		SetState( MONSTERSTATE_IDLE );
		m_IdealMonsterState = MONSTERSTATE_IDLE;

		ALERT ( at_aiconsole, "Scripted Sequence stripped on level transition for %s\n", GetDebugName() );
	}
}

//-------------------------------------

int CBaseMonster::Restore( CRestore &restore )
{
	// do a normal restore
	int status = BaseClass::Restore(restore);
	if ( !status )
		return 0;

	AIExtendedSaveHeader_t saveHeader;
	restore.ReadAll( &saveHeader );

	// restore conditions
	restore.StartBlock();
		RestoreConditions( restore, &m_Conditions );
		RestoreConditions( restore, &m_CustomInterruptConditions );
		RestoreConditions( restore, &m_ConditionsPreIgnore );
		CAI_ScheduleBits ignoreConditions;
		RestoreConditions( restore, &ignoreConditions );
		ignoreConditions.Not( &m_InverseIgnoreConditions );
	restore.EndBlock();

	bool bLostSequence = false;
	if ( saveHeader.szSequence[0] && GetModelPtr() )
	{
		pev->sequence = LookupSequence( saveHeader.szSequence );
		if ( pev->sequence == ACT_INVALID )
		{
			ALERT( at_console, "Discarding missing sequence %s on load.\n", saveHeader.szSequence );
			pev->sequence = 0;
			bLostSequence = true;
		}
	}

	bool bLostScript = ( m_MonsterState == MONSTERSTATE_SCRIPT && m_pCine == NULL );
	bool bDiscardScheduleState = ( bLostScript || 
								   bLostSequence ||
								   saveHeader.szSchedule[0] == 0 ||
								   saveHeader.version != AI_EXTENDED_SAVE_HEADER_VERSION ||
								   ( (saveHeader.flags & AIESH_HAD_ENEMY) && !GetEnemy() ) ||
								   ( (saveHeader.flags & AIESH_HAD_TARGET) && !m_hTargetEnt ) );

	if ( !bDiscardScheduleState )
	{
		m_pSchedule = g_AI_SchedulesManager.GetScheduleByName( saveHeader.szSchedule );
		if ( m_pSchedule )
		{
			CRC32_t scheduleCrc;
			CRC32_INIT( &scheduleCrc );
			CRC32_PROCESS_BUFFER( &scheduleCrc, (void *)m_pSchedule->GetTaskList(), m_pSchedule->NumTasks() * sizeof(Task_t) );
			CRC32_FINAL( scheduleCrc );

			if ( scheduleCrc != saveHeader.scheduleCrc )
			{
				m_pSchedule = NULL;
			}
		}
	}

	if ( !m_pSchedule )
		bDiscardScheduleState = true;

	if ( !bDiscardScheduleState )
		m_bDoPostRestoreRefindPath = ( ( saveHeader.flags & AIESH_HAD_NAVGOAL) != 0 );
	else 
	{
		m_bDoPostRestoreRefindPath = false;
		DiscardScheduleState();
	}

	return status;
}

void CBaseMonster::OnRestore( void )
{
	if ( m_bDoPostRestoreRefindPath && WorldGraph.m_fGraphPointersSet )
	{
		if ( !FRefreshRoute() )
			DiscardScheduleState();
	}
	else
		RouteClear();
}

//-------------------------------------

void CBaseMonster::SaveConditions( CSave &save, const CAI_ScheduleBits &conditions )
{
	for ( int i = 0; i < MAX_CONDITIONS; i++ )
	{
		if (conditions.IsBitSet(i))
		{
			const char *pszConditionName = ConditionName(AI_RemapToGlobal(i));
			if ( !pszConditionName )
				break;
			save.WriteString( pszConditionName );
		}
	}
	save.WriteString( "" );
}

//-------------------------------------

void CBaseMonster::RestoreConditions( CRestore &restore, CAI_ScheduleBits *pConditions )
{
	pConditions->ClearAll();
	char szCondition[256];
	for (;;)
	{
		restore.ReadString( szCondition, sizeof(szCondition), 0 );
		if ( !szCondition[0] )
			break;
		int iCondition = GetSchedulingSymbols()->ConditionSymbolToId( szCondition );
		if ( iCondition != -1 )
			pConditions->Set( AI_RemapFromGlobal( iCondition ) );
	}
}

//-----------------------------------------------------------------------------
// Purpose: Written by subclasses macro to load schedules
// Input  :
// Output :
//-----------------------------------------------------------------------------
bool CBaseMonster::LoadSchedules( void )
{
	return true;
}

//-----------------------------------------------------------------------------

bool CBaseMonster::LoadedSchedules( void )
{
	return true;
}

CBaseMonster::CBaseMonster( void )
:	m_UnreachableEnts( 0, 4 )
{
	CreateComponents();

	m_pSchedule = NULL;

	m_afCapability				= 0;		// Make sure this is cleared in the base class

	m_iMySquadSlot				= SQUAD_SLOT_NONE;
	m_flSoundWaitTime			= 0;
	m_pEnemies					= new CAI_Enemies;
	m_hTargetEnt				= NULL;

	m_pSquad					= NULL;

	m_flMoveWaitFinished		= 0;

	// ----------------------------
	//  Debugging fields
	// ----------------------------
	m_nDebugPauseIndex = 0;

	g_AI_Manager.AddAI( this );
}

CBaseMonster::~CBaseMonster( void )
{
	g_AI_Manager.RemoveAI( this );

	RemoveMemory();

	delete m_pSenses;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CBaseMonster::UpdateOnRemove(void)
{
	// Vacate any strategy slot I might have
	VacateStrategySlot();

	if (m_pSquad)
	{
		// Remove from squad if not already removed
		if (m_pSquad->SquadIsMember(this))
		{
			m_pSquad->RemoveFromSquad(this);
		}
	}

	// Chain at end to mimic destructor unwind order
	BaseClass::UpdateOnRemove();
}

//-----------------------------------------------------------------------------

bool CBaseMonster::CreateComponents()
{
	m_pSenses = CreateSenses();
	if ( !m_pSenses )
		return false;

	return true;
}

//-----------------------------------------------------------------------------

CAI_Senses *CBaseMonster::CreateSenses()
{
	CAI_Senses *pSenses = new CAI_Senses;
	pSenses->SetOuter( this );
	return pSenses;
}

//-----------------------------------------------------------------------------


//=========================================================
// Eat - makes a monster full for a little while.
//=========================================================
void CBaseMonster::Eat( float flFullDuration )
{
	m_flHungryTime = gpGlobals->time + flFullDuration;
}

//=========================================================
// FShouldEat - returns true if a monster is hungry.
//=========================================================
bool CBaseMonster::FShouldEat( void )
{
	if ( m_flHungryTime > gpGlobals->time )
	{
		return false;
	}

	return true;
}

//=========================================================
// BarnacleVictimBitten - called
// by Barnacle victims when the barnacle pulls their head
// into its mouth
//=========================================================
void CBaseMonster::BarnacleVictimBitten ( CBaseEntity *pBarnacle )
{
	CAI_Schedule *pNewSchedule;

	pNewSchedule = GetScheduleOfType( SCHED_BARNACLE_VICTIM_CHOMP );

	if ( pNewSchedule )
	{
		SetSchedule( pNewSchedule );
	}
}

//=========================================================
// BarnacleVictimReleased - called by barnacle victims when
// the host barnacle is killed.
//=========================================================
void CBaseMonster :: BarnacleVictimReleased ( void )
{
	m_IdealMonsterState = MONSTERSTATE_IDLE;

	pev->velocity = vec3_origin;
	pev->movetype = MOVETYPE_STEP;
}

//---------------------------------------------------------
//---------------------------------------------------------
#define InterruptFromCondition( iCondition ) \
	AI_RemapFromGlobal( ( AI_IdIsLocal( iCondition ) ? GetClassScheduleIdSpace()->ConditionLocalToGlobal( iCondition ) : iCondition ) )
	
void CBaseMonster::SetCondition( int iCondition )
{
	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return;
	}
	
	m_Conditions.Set( interrupt );
}

//---------------------------------------------------------
//---------------------------------------------------------
bool CBaseMonster::HasCondition( int iCondition )
{
	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return false;
	}
	
	bool bReturn = m_Conditions.IsBitSet(interrupt);
	return (bReturn);
}

//---------------------------------------------------------
//---------------------------------------------------------
bool CBaseMonster::HasCondition( int iCondition, bool bUseIgnoreConditions )
{
	if ( bUseIgnoreConditions )
		return HasCondition( iCondition );
	
	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return false;
	}
	
	bool bReturn = m_ConditionsPreIgnore.IsBitSet(interrupt);
	return (bReturn);
}

//---------------------------------------------------------
//---------------------------------------------------------
void CBaseMonster::ClearCondition( int iCondition )
{
	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return;
	}
	
	m_Conditions.Clear(interrupt);
}

//---------------------------------------------------------
//---------------------------------------------------------
void CBaseMonster::ClearConditions( int *pConditions, int nConditions )
{
	for ( int i = 0; i < nConditions; ++i )
	{
		int iCondition = pConditions[i];
		int interrupt = InterruptFromCondition( iCondition );
		
		if ( interrupt == -1 )
		{
			Assert(0);
			continue;
		}
		
		m_Conditions.Clear(interrupt);
	}
}

//---------------------------------------------------------
//---------------------------------------------------------
void CBaseMonster::SetIgnoreConditions( int *pConditions, int nConditions )
{
	for ( int i = 0; i < nConditions; ++i )
	{
		int iCondition = pConditions[i];
		int interrupt = InterruptFromCondition( iCondition );
		
		if ( interrupt == -1 )
		{
			Assert(0);
			continue;
		}
		
		m_InverseIgnoreConditions.Clear(interrupt); // clear means ignore
	}
}

void CBaseMonster::ClearIgnoreConditions( int *pConditions, int nConditions )
{
	for ( int i = 0; i < nConditions; ++i )
	{
		int iCondition = pConditions[i];
		int interrupt = InterruptFromCondition( iCondition );
		
		if ( interrupt == -1 )
		{
			Assert(0);
			continue;
		}
		
		m_InverseIgnoreConditions.Set( interrupt ); // set means don't ignore
	}
}

//---------------------------------------------------------
//---------------------------------------------------------
bool CBaseMonster::HasInterruptCondition( int iCondition )
{
	if( !GetCurSchedule() )
	{
		return false;
	}

	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return false;
	}
	return ( m_Conditions.IsBitSet( interrupt ) && GetCurSchedule()->HasInterrupt( interrupt ) );
}

//---------------------------------------------------------
//---------------------------------------------------------
bool CBaseMonster::ConditionInterruptsCurSchedule( int iCondition )
{
	if( !GetCurSchedule() )
	{
		return false;
	}

	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return false;
	}
	return ( GetCurSchedule()->HasInterrupt( interrupt ) );
}

//---------------------------------------------------------
//---------------------------------------------------------
bool CBaseMonster::ConditionInterruptsSchedule( int localScheduleID, int iCondition )
{
	CAI_Schedule *pSchedule = GetSchedule( localScheduleID );
	if ( !pSchedule )
		return false;

	int interrupt = InterruptFromCondition( iCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return false;
	}
	return ( pSchedule->HasInterrupt( interrupt ) );
}

//-----------------------------------------------------------------------------
// Returns whether we currently have any interrupt conditions that would
// interrupt the given schedule.
//-----------------------------------------------------------------------------
bool CBaseMonster::HasConditionsToInterruptSchedule( int nLocalScheduleID )
{
	CAI_Schedule *pSchedule = GetSchedule( nLocalScheduleID );
	if ( !pSchedule )
		return false;

	CAI_ScheduleBits bitsMask;
	pSchedule->GetInterruptMask( &bitsMask );

	CAI_ScheduleBits bitsOut;
	AccessConditionBits().And( bitsMask, &bitsOut );
	
	return !bitsOut.IsAllClear();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
bool CBaseMonster::IsCustomInterruptConditionSet( int nCondition )
{
	int interrupt = InterruptFromCondition( nCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return false;
	}
	
	return m_CustomInterruptConditions.IsBitSet( interrupt );
}

//-----------------------------------------------------------------------------
// Purpose: Sets a flag in the custom interrupt flags, translating the condition
//			to the proper global space, if necessary
//-----------------------------------------------------------------------------
void CBaseMonster::SetCustomInterruptCondition( int nCondition )
{
	int interrupt = InterruptFromCondition( nCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return;
	}
	
	m_CustomInterruptConditions.Set( interrupt );
}

//-----------------------------------------------------------------------------
// Purpose: Clears a flag in the custom interrupt flags, translating the condition
//			to the proper global space, if necessary
//-----------------------------------------------------------------------------
void CBaseMonster::ClearCustomInterruptCondition( int nCondition )
{
	int interrupt = InterruptFromCondition( nCondition );
	
	if ( interrupt == -1 )
	{
		Assert(0);
		return;
	}
	
	m_CustomInterruptConditions.Clear( interrupt );
}

//-----------------------------------------------------------------------------
// Purpose: Clears all the custom interrupt flags.
//-----------------------------------------------------------------------------
void CBaseMonster::ClearCustomInterruptConditions()
{
	m_CustomInterruptConditions.ClearAll();
}

void CBaseMonster::SetDistLook( float flDistLook )
{
	GetSenses()->SetDistLook( flDistLook );
}

//-----------------------------------------------------------------------------

void CBaseMonster::OnLooked( void )
{
	// DON'T let visibility information from last frame sit around!
	static int conditionsToClear[] =
	{
		COND_SEE_HATE,
		COND_SEE_DISLIKE,
		COND_SEE_ENEMY,
		COND_SEE_FEAR,
		COND_SEE_NEMESIS,
		COND_SEE_PLAYER,
	};

	ClearConditions( conditionsToClear, ARRAYSIZE( conditionsToClear ) );

	AISightIter_t iter;
	CBaseEntity *pSightEnt;

	pSightEnt = GetSenses()->GetFirstSeenEntity( &iter );// the current visible entity that we're dealing with

	while( pSightEnt )
	{
		if ( pSightEnt->IsPlayer() )
		{
			// if we see a client, remember that (mostly for scripted AI)
			SetCondition( COND_SEE_PLAYER );
		}

		Disposition_t relation = IRelationType( pSightEnt );

		// the looker will want to consider this entity
		// don't check anything else about an entity that can't be seen, or an entity that you don't care about.
		if ( relation != D_NU )
		{
			if ( pSightEnt == GetEnemy() )
			{
				// we know this ent is visible, so if it also happens to be our enemy, store that now.
				SetCondition( COND_SEE_ENEMY );
			}

			// don't add the Enemy's relationship to the conditions. We only want to worry about conditions when
			// we see monsters other than the Enemy.
			switch ( relation )
			{
			case D_HT:
				{
					int priority = IRelationPriority( pSightEnt );
					if (priority < 0)
					{
						SetCondition( COND_SEE_DISLIKE );
					}
					else if (priority > 10)
					{
						SetCondition( COND_SEE_NEMESIS );
					}
					else
					{
						SetCondition( COND_SEE_HATE );
					}
					UpdateEnemyMemory( pSightEnt, pSightEnt->pev->origin );
					break;
				}
			case D_FR:
				UpdateEnemyMemory( pSightEnt, pSightEnt->pev->origin );
				SetCondition( COND_SEE_FEAR );
				break;
			case D_LI:
			case D_NU:
				break;
			default:
				ALERT ( at_aiconsole, "%s can't assess %s\n", STRING(pev->classname), STRING(pSightEnt->pev->classname ) );
				break;
			}
		}

		pSightEnt = GetSenses()->GetNextSeenEntity( &iter );
	}
}

void CBaseMonster::OnListened( void )
{
	AISoundIter_t iter;
	CSound	*pCurrentSound;

	static int conditionsToClear[] =
	{
		COND_HEAR_DANGER,
		COND_HEAR_COMBAT,
		COND_HEAR_WORLD,
		COND_HEAR_PLAYER,
		COND_SMELL,
		COND_SMELL_FOOD,
	};

	ClearConditions( conditionsToClear, ARRAYSIZE( conditionsToClear ) );

	pCurrentSound = GetSenses()->GetFirstHeardSound( &iter );

	while ( pCurrentSound )
	{
		if ( pCurrentSound->FIsSound() )
		{
			// this is an audible sound.
			if ( pCurrentSound->m_iType & bits_SOUND_DANGER )
			{
				SetCondition( COND_HEAR_DANGER );
			}

			if ( pCurrentSound->m_iType & bits_SOUND_COMBAT )
			{
				SetCondition( COND_HEAR_COMBAT );
			}

			if ( pCurrentSound->m_iType & bits_SOUND_WORLD )
			{
				SetCondition( COND_HEAR_WORLD );
			}

			if ( pCurrentSound->m_iType & bits_SOUND_PLAYER )
			{
				SetCondition( COND_HEAR_PLAYER );
			}
		}
		else
		{
			// if not a sound, must be a smell - determine if it's just a scent, or if it's a food scent
			if ( pCurrentSound->m_iType & ( bits_SOUND_MEAT | bits_SOUND_CARCASS ) )
			{
				// the detected scent is a food item, so set both conditions.
				// !!!BUGBUG - maybe a virtual function to determine whether or not the scent is food?
				SetCondition( COND_SMELL_FOOD );
				SetCondition( COND_SMELL );
			}
			else
			{
				// just a normal scent. 
				SetCondition( COND_SMELL );
			}
		}

		pCurrentSound = GetSenses()->GetNextHeardSound( &iter );
	}
}

//=========================================================
// FLSoundVolume - subtracts the volume of the given sound
// from the distance the sound source is from the caller, 
// and returns that value, which is considered to be the 'local' 
// volume of the sound. 
//=========================================================
float CBaseMonster :: FLSoundVolume ( CSound *pSound )
{
	return ( pSound->m_iVolume - ( ( pSound->m_vecOrigin - pev->origin ).Length() ) );
}

//=========================================================
// GetSoundInterests - returns a bit mask indicating which types
// of sounds this monster regards. In the base class implementation,
// monsters care about all sounds, but no scents.
//=========================================================
int CBaseMonster::GetSoundInterests( void )
{
	return	bits_SOUND_WORLD	|
			bits_SOUND_COMBAT	|
			bits_SOUND_PLAYER;
}

//---------------------------------------------------------
// Return the loudest sound of this type in the sound list
//---------------------------------------------------------
CSound *CBaseMonster::GetLoudestSoundOfType( int iType )
{
	CSound *pLoudestSound = NULL;

	int iThisSound; 
	int	iBestSound = SOUNDLIST_EMPTY;
	float flBestDist = 8192 * 8192;// so first nearby sound will become best so far.
	float flDist;
	CSound *pSound;

	iThisSound = CSoundEnt::ActiveList();

	while ( iThisSound != SOUNDLIST_EMPTY )
	{
		pSound = CSoundEnt::SoundPointerForIndex( iThisSound );

		if ( pSound && pSound->m_iType == iType )
		{
			flDist = ( pSound->m_vecOrigin - EarPosition() ).Length();

			if ( flDist <= pSound->m_iVolume && flDist < flBestDist )
			{
				pLoudestSound = pSound;

				iBestSound = iThisSound;
				flBestDist = flDist;
			}
		}

		iThisSound = pSound->m_iNext;
	}

	return pLoudestSound;
}

//=========================================================
// GetBestSound - returns a pointer to the sound the monster 
// should react to. Right now responds only to nearest sound.
//=========================================================
CSound *CBaseMonster::GetBestSound( void )
{	
	CSound *pResult = GetSenses()->GetClosestSound();
	if ( pResult == NULL)
		ALERT( at_error, "NULL Return from GetBestSound\n" ); // condition previously set now no longer true. Have seen this when play too many sounds...
	return pResult;
}

//=========================================================
// GetBestScent - returns a pointer to the scent the monster 
// should react to. Right now responds only to nearest scent
//=========================================================
CSound *CBaseMonster::GetBestScent( void )
{	
	CSound *pResult = GetSenses()->GetClosestSound( true );
	if ( pResult == NULL)
		ALERT( at_error, "NULL Return from GetBestScent\n" );
	return pResult;
}

//=========================================================
// FValidateHintType - tells use whether or not the monster cares
// about the type of Hint Node given
//=========================================================
bool CBaseMonster::FValidateHintType( short sHint )
{
	return false;
}

//-----------------------------------------------------------------------------
float g_AINextDisabledMessageTime;

bool CBaseMonster::PreThink( void )
{
	// ----------------------------------------------------------
	// Skip AI if its been disabled and put a warning message on the screen
	// ----------------------------------------------------------
	if ( CBaseMonster::m_nDebugBits & bits_debugDisableAI )
	{
		if ( gpGlobals->time >= g_AINextDisabledMessageTime )
		{
			g_AINextDisabledMessageTime = gpGlobals->time + 5.0f;

			hudtextparms_s tTextParam;
			tTextParam.x			= 0.7;
			tTextParam.y			= 0.65;
			tTextParam.effect		= 0;
			tTextParam.r1			= 255;
			tTextParam.g1			= 255;
			tTextParam.b1			= 255;
			tTextParam.a1			= 255;
			tTextParam.r2			= 255;
			tTextParam.g2			= 255;
			tTextParam.b2			= 255;
			tTextParam.a2			= 255;
			tTextParam.fadeinTime	= 0;
			tTextParam.fadeoutTime	= 0;
			tTextParam.holdTime		= 5.1;
			tTextParam.fxTime		= 0;
			tTextParam.channel		= 1;
			UTIL_HudMessageAll( tTextParam, "A.I. Disabled...\n" );
		}
		SetActivity( ACT_IDLE );
		return false;
	}

	// --------------------------------------------------------
	//	If debug stepping
	// --------------------------------------------------------
	if (CBaseMonster::m_nDebugBits & bits_debugStepAI)
	{
		if (m_nDebugCurIndex >= CBaseMonster::m_nDebugPauseIndex)
		{
			if ( FRouteClear() )
			{
				pev->framerate = 0;
			}
			return false;
		}
		else
		{
			pev->framerate = 1;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

void CBaseMonster::RunAnimation( void )
{
	StudioFrameAdvance( ); // animate

	// start or end a fidget
	// This needs a better home -- switching animations over time should be encapsulated on a per-activity basis
	// perhaps MaintainActivity() or a ShiftAnimationOverTime() or something.
	if ( m_MonsterState != MONSTERSTATE_SCRIPT && m_MonsterState != MONSTERSTATE_DEAD && m_Activity == ACT_IDLE && IsSequenceFinished() )
	{
		int iSequence;

		if ( SequenceLoops() )
		{
			// animation does loop, which means we're playing subtle idle. Might need to 
			// fidget.
			iSequence = LookupActivity ( m_Activity );
		}
		else
		{
			// animation that just ended doesn't loop! That means we just finished a fidget
			// and should return to our heaviest weighted idle (the subtle one)
			iSequence = LookupActivityHeaviest ( m_Activity );
		}
		if ( iSequence != ACTIVITY_NOT_AVAILABLE )
		{
			pev->sequence = iSequence;	// Set to new anim (if it's there)
			ResetSequenceInfo( );
		}
	}

	DispatchAnimEvents();
}

//-----------------------------------------------------------------------------

bool CBaseMonster::ShouldAlwaysThink()
{
	// @TODO (toml 07-08-03): This needs to be beefed up. 
	// There are simple cases already seen where an AI can briefly leave
	// the PVS while navigating to the player. Perhaps should incorporate a heuristic taking into
	// account mode, enemy, last time saw player, player range etc. For example, if enemy is player,
	// and player is within 100 feet, and saw the player within the past 15 seconds, keep running...
	return false;
}

//=========================================================
// Monster Think - calls out to core AI functions and handles this
// monster's specific animation events
//=========================================================
void CBaseMonster::MonsterThink ( void )
{
	pev->nextthink = gpGlobals->time + 0.1f;// keep monster thinking.

	if ( !PreThink() )
		return;

	float flInterval = GetAnimTimeInterval();

	RunAI();

	RunAnimation();

	Move( flInterval );
}

//=========================================================
// CBaseMonster - USE - will make a monster angry at whomever
// activated it.
//=========================================================
void CBaseMonster :: MonsterUse ( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	m_IdealMonsterState = MONSTERSTATE_ALERT;
}

//-----------------------------------------------------------------------------
// Purpose: Virtual function that allows us to have any monster ignore a set of
// shared conditions.
//
//-----------------------------------------------------------------------------
void CBaseMonster::RemoveIgnoredConditions( void )
{
	m_ConditionsPreIgnore = m_Conditions;
	m_Conditions.And( m_InverseIgnoreConditions, &m_Conditions );

	if ( !FShouldEat() )
	{
		// not hungry? Ignore food smell.
		ClearCondition( COND_SMELL_FOOD );
	}

	if ( m_MonsterState == MONSTERSTATE_SCRIPT && m_pCine )
		m_pCine->RemoveIgnoredConditions();

}

//=========================================================
// 	RouteClear - zeroes out the monster's route array and goal
//=========================================================
void CBaseMonster :: RouteClear ( void )
{
	RouteNew();
	m_movementGoal = MOVEGOAL_NONE;
	m_movementActivity = ACT_IDLE;
	Forget( bits_MEMORY_MOVE_FAILED );
}

//=========================================================
// Route New - clears out a route to be changed, but keeps
//				goal intact.
//=========================================================
void CBaseMonster :: RouteNew ( void )
{
	m_Route[ 0 ].iType		= 0;
	m_iRouteIndex			= 0;
}

//=========================================================
// FRouteClear - returns TRUE is the Route is cleared out
// ( invalid )
//=========================================================
bool CBaseMonster :: FRouteClear ( void )
{
	if ( m_Route[ m_iRouteIndex ].iType == 0 || m_movementGoal == MOVEGOAL_NONE )
		return TRUE;

	return FALSE;
}

//=========================================================
// FRefreshRoute - after calculating a path to the monster's
// target, this function copies as many waypoints as possible
// from that path to the monster's Route array
//=========================================================
bool CBaseMonster :: FRefreshRoute ( void )
{
	CBaseEntity	*pPathCorner;
	int			i;
	bool		returnCode;

	RouteNew();

	returnCode = FALSE;

	switch( m_movementGoal )
	{
		case MOVEGOAL_PATHCORNER:
			{
				// monster is on a path_corner loop
				pPathCorner = m_pGoalEnt;
				i = 0;

				while ( pPathCorner && i < ROUTE_SIZE )
				{
					m_Route[ i ].iType = bits_MF_TO_PATHCORNER;
					m_Route[ i ].vecLocation = pPathCorner->pev->origin;

					pPathCorner = pPathCorner->GetNextTarget();

					// Last path_corner in list?
					if ( !pPathCorner )
						m_Route[i].iType |= bits_MF_IS_GOAL;
					
					i++;
				}
			}
			returnCode = TRUE;
			break;

		case MOVEGOAL_ENEMY:
			returnCode = BuildRoute( GetEnemyLKP(), bits_MF_TO_ENEMY, m_hEnemy );
			break;

		case MOVEGOAL_LOCATION:
			returnCode = BuildRoute( m_vecMoveGoal, bits_MF_TO_LOCATION, NULL );
			break;

		case MOVEGOAL_TARGETENT:
			if (m_hTargetEnt != NULL)
			{
				returnCode = BuildRoute( m_hTargetEnt->pev->origin, bits_MF_TO_TARGETENT, m_hTargetEnt );
			}
			break;

		case MOVEGOAL_NODE:
			returnCode = FGetNodeRoute( m_vecMoveGoal );
//			if ( returnCode )
//				RouteSimplify( NULL );
			break;
	}

	return returnCode;
}


bool CBaseMonster::MoveToEnemy( Activity movementAct, float waitTime )
{
	m_movementActivity = movementAct;
	m_moveWaitTime = waitTime;
	
	m_movementGoal = MOVEGOAL_ENEMY;
	return FRefreshRoute();
}


bool CBaseMonster::MoveToLocation( Activity movementAct, float waitTime, const Vector &goal )
{
	m_movementActivity = movementAct;
	m_moveWaitTime = waitTime;
	
	m_movementGoal = MOVEGOAL_LOCATION;
	m_vecMoveGoal = goal;
	return FRefreshRoute();
}


bool CBaseMonster::MoveToTarget( Activity movementAct, float waitTime )
{
	m_movementActivity = movementAct;
	m_moveWaitTime = waitTime;
	
	m_movementGoal = MOVEGOAL_TARGETENT;
	return FRefreshRoute();
}


bool CBaseMonster::MoveToNode( Activity movementAct, float waitTime, const Vector &goal )
{
	m_movementActivity = movementAct;
	m_moveWaitTime = waitTime;

	m_movementGoal = MOVEGOAL_NODE;
	m_vecMoveGoal = goal;
	return FRefreshRoute();
}

bool CBaseMonster::MoveToWanderGoal( float minRadius, float maxRadius )
{
	// @Note (toml 11-07-02): this is a bogus placeholder implementation!!!
	for ( int i = 0; i < 5; i++ )
	{
		float dist = RANDOM_FLOAT( minRadius, maxRadius );
		Vector dir = pev->origin + UTIL_YawToVector( RANDOM_FLOAT( 0, 359.99 ) ) * dist;
		
		if ( MoveToLocation( ACT_WALK, 0, dir ) )
			return true;
	}
	
	return BuildRandomRoute();
}

#ifdef _DEBUG
void DrawRoute( entvars_t *pev, WayPoint_t *m_Route, int m_iRouteIndex, int r, int g, int b )
{
	int			i;

	if ( m_Route[m_iRouteIndex].iType == 0 )
	{
		ALERT( at_aiconsole, "Can't draw route!\n" );
		return;
	}

//	UTIL_ParticleEffect ( m_Route[ m_iRouteIndex ].vecLocation, vec3_origin, 255, 25 );

	MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
		WRITE_BYTE( TE_BEAMPOINTS);
		WRITE_COORD( pev->origin.x );
		WRITE_COORD( pev->origin.y );
		WRITE_COORD( pev->origin.z );
		WRITE_COORD( m_Route[ m_iRouteIndex ].vecLocation.x );
		WRITE_COORD( m_Route[ m_iRouteIndex ].vecLocation.y );
		WRITE_COORD( m_Route[ m_iRouteIndex ].vecLocation.z );

		WRITE_SHORT( g_sModelIndexLaser );
		WRITE_BYTE( 0 ); // frame start
		WRITE_BYTE( 10 ); // framerate
		WRITE_BYTE( 1 ); // life
		WRITE_BYTE( 16 );  // width
		WRITE_BYTE( 0 );   // noise
		WRITE_BYTE( r );   // r, g, b
		WRITE_BYTE( g );   // r, g, b
		WRITE_BYTE( b );   // r, g, b
		WRITE_BYTE( 255 );	// brightness
		WRITE_BYTE( 10 );		// speed
	MESSAGE_END();

	for ( i = m_iRouteIndex ; i < ROUTE_SIZE - 1; i++ )
	{
		if ( (m_Route[ i ].iType & bits_MF_IS_GOAL) || (m_Route[ i+1 ].iType == 0) )
			break;

		
		MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
			WRITE_BYTE( TE_BEAMPOINTS );
			WRITE_COORD( m_Route[ i ].vecLocation.x );
			WRITE_COORD( m_Route[ i ].vecLocation.y );
			WRITE_COORD( m_Route[ i ].vecLocation.z );
			WRITE_COORD( m_Route[ i + 1 ].vecLocation.x );
			WRITE_COORD( m_Route[ i + 1 ].vecLocation.y );
			WRITE_COORD( m_Route[ i + 1 ].vecLocation.z );
			WRITE_SHORT( g_sModelIndexLaser );
			WRITE_BYTE( 0 ); // frame start
			WRITE_BYTE( 10 ); // framerate
			WRITE_BYTE( 1 ); // life
			WRITE_BYTE( 8 );  // width
			WRITE_BYTE( 0 );   // noise
			WRITE_BYTE( r );   // r, g, b
			WRITE_BYTE( g );   // r, g, b
			WRITE_BYTE( b );   // r, g, b
			WRITE_BYTE( 255 );	// brightness
			WRITE_BYTE( 10 );		// speed
		MESSAGE_END();

//		UTIL_ParticleEffect ( m_Route[ i ].vecLocation, vec3_origin, 255, 25 );
	}
}
#endif


int ShouldSimplify( int routeType )
{
	routeType &= ~bits_MF_IS_GOAL;

	if ( (routeType == bits_MF_TO_PATHCORNER) || (routeType & bits_MF_DONT_SIMPLIFY) )
		return FALSE;
	return TRUE;
}

//=========================================================
// RouteSimplify
//
// Attempts to make the route more direct by cutting out
// unnecessary nodes & cutting corners.
//
//=========================================================
void CBaseMonster :: RouteSimplify( CBaseEntity *pTargetEnt )
{
	// BUGBUG: this doesn't work 100% yet
	int			i, count, outCount;
	Vector		vecStart;
	WayPoint_t	outRoute[ ROUTE_SIZE * 2 ];	// Any points except the ends can turn into 2 points in the simplified route

	count = 0;

	for ( i = m_iRouteIndex; i < ROUTE_SIZE; i++ )
	{
		if ( !m_Route[i].iType )
			break;
		else
			count++;
		if ( m_Route[i].iType & bits_MF_IS_GOAL )
			break;
	}
	// Can't simplify a direct route!
	if ( count < 2 )
	{
//		DrawRoute( pev, m_Route, m_iRouteIndex, 0, 0, 255 );
		return;
	}

	outCount = 0;
	vecStart = pev->origin;
	for ( i = 0; i < count-1; i++ )
	{
		// Don't eliminate path_corners
		if ( !ShouldSimplify( m_Route[m_iRouteIndex+i].iType ) )
		{
			outRoute[outCount] = m_Route[ m_iRouteIndex + i ];
			outCount++;
		}
		else if ( CheckLocalMove ( vecStart, m_Route[m_iRouteIndex+i+1].vecLocation, pTargetEnt, NULL ) == LOCALMOVE_VALID )
		{
			// Skip vert
			continue;
		}
		else
		{
			Vector vecTest, vecSplit;

			// Halfway between this and next
			vecTest = (m_Route[m_iRouteIndex+i+1].vecLocation + m_Route[m_iRouteIndex+i].vecLocation) * 0.5;

			// Halfway between this and previous
			vecSplit = (m_Route[m_iRouteIndex+i].vecLocation + vecStart) * 0.5;

			int iType = (m_Route[m_iRouteIndex+i].iType | bits_MF_TO_DETOUR) & ~bits_MF_NOT_TO_MASK;
			if ( CheckLocalMove ( vecStart, vecTest, pTargetEnt, NULL ) == LOCALMOVE_VALID )
			{
				outRoute[outCount].iType = iType;
				outRoute[outCount].vecLocation = vecTest;
			}
			else if ( CheckLocalMove ( vecSplit, vecTest, pTargetEnt, NULL ) == LOCALMOVE_VALID )
			{
				outRoute[outCount].iType = iType;
				outRoute[outCount].vecLocation = vecSplit;
				outRoute[outCount+1].iType = iType;
				outRoute[outCount+1].vecLocation = vecTest;
				outCount++; // Adding an extra point
			}
			else
			{
				outRoute[outCount] = m_Route[ m_iRouteIndex + i ];
			}
		}
		// Get last point
		vecStart = outRoute[ outCount ].vecLocation;
		outCount++;
	}
	ASSERT( i < count );
	outRoute[outCount] = m_Route[ m_iRouteIndex + i ];
	outCount++;
	
	// Terminate
	outRoute[outCount].iType = 0;
	ASSERT( outCount < (ROUTE_SIZE*2) );

// Copy the simplified route, disable for testing
	m_iRouteIndex = 0;
	for ( i = 0; i < ROUTE_SIZE && i < outCount; i++ )
	{
		m_Route[i] = outRoute[i];
	}

	// Terminate route
	if ( i < ROUTE_SIZE )
		m_Route[i].iType = 0;

// Debug, test movement code
#if 0
//	if ( CVAR_GET_FLOAT( "simplify" ) != 0 )
		DrawRoute( pev, outRoute, 0, 255, 0, 0 );
//	else
		DrawRoute( pev, m_Route, m_iRouteIndex, 0, 255, 0 );
#endif
}

//=========================================================
// FBecomeProne - tries to send a monster into PRONE state.
// right now only used when a barnacle snatches someone, so 
// may have some special case stuff for that.
//=========================================================
bool CBaseMonster::FBecomeProne ( void )
{
	if ( FBitSet ( pev->flags, FL_ONGROUND ) )
	{
		pev->flags &= ~FL_ONGROUND;
	}

	m_IdealMonsterState = MONSTERSTATE_PRONE;
	return true;
}

//=========================================================
// CheckRangeAttack1
//=========================================================
bool CBaseMonster::CheckRangeAttack1( float flDot, float flDist )
{
	if ( flDist > 64 && flDist <= 784 && flDot >= 0.5 )
	{
		return true;
	}
	return false;
}

//=========================================================
// CheckRangeAttack2
//=========================================================
bool CBaseMonster::CheckRangeAttack2( float flDot, float flDist )
{
	if ( flDist > 64 && flDist <= 512 && flDot >= 0.5 )
	{
		return true;
	}
	return false;
}

//=========================================================
// CheckMeleeAttack1
//=========================================================
bool CBaseMonster::CheckMeleeAttack1( float flDot, float flDist )
{
	// Decent fix to keep folks from kicking/punching hornets and snarks is to check the onground flag(sjb)
	if ( flDist <= 64 && flDot >= 0.7 && m_hEnemy != NULL && FBitSet ( m_hEnemy->pev->flags, FL_ONGROUND ) )
	{
		return true;
	}
	return false;
}

//=========================================================
// CheckMeleeAttack2
//=========================================================
bool CBaseMonster::CheckMeleeAttack2( float flDot, float flDist )
{
	if ( flDist <= 64 && flDot >= 0.7 )
	{
		return true;
	}
	return false;
}

// Get capability mask
int CBaseMonster::CapabilitiesGet( void )
{
	return m_afCapability;
}

// Set capability mask
int CBaseMonster::CapabilitiesAdd( int capability )
{
	m_afCapability |= capability;

	return m_afCapability;
}

// Set capability mask
int CBaseMonster::CapabilitiesRemove( int capability )
{
	m_afCapability &= ~capability;

	return m_afCapability;
}

// Clear capability mask
void CBaseMonster::CapabilitiesClear( void )
{
	m_afCapability = 0;
}

//=========================================================
// ClearAttackConditions - clear out all attack conditions
//=========================================================
void CBaseMonster::ClearAttackConditions( )
{
	// Clear all attack conditions
	ClearCondition( COND_CAN_RANGE_ATTACK1 );
	ClearCondition( COND_CAN_RANGE_ATTACK2 );
	ClearCondition( COND_CAN_MELEE_ATTACK1 );
	ClearCondition( COND_CAN_MELEE_ATTACK2 );
}

//=========================================================
// GatherAttackConditions - sets all of the bits for attacks that the
// monster is capable of carrying out on the passed entity.
//=========================================================
void CBaseMonster::GatherAttackConditions( CBaseEntity *pTarget, float flDist )
{
	Vector vecLOS = ( pTarget->pev->origin - pev->origin );
	vecLOS.z = 0;
	VectorNormalize( vecLOS );

	Vector vBodyDir = BodyDirection2D( );
	float  flDot	= DotProduct( vecLOS, vBodyDir );

	// we know the enemy is in front now. We'll find which attacks the monster is capable of by
	// checking for corresponding Activities in the model file, then do the simple checks to validate
	// those attack types.
	
	// Clear all attack conditions
	ClearAttackConditions( );

	if ( CapabilitiesGet() & bits_CAP_RANGE_ATTACK1 )
	{
		if ( CheckRangeAttack1 ( flDot, flDist ) )
			SetCondition( COND_CAN_RANGE_ATTACK1 );
	}
	if ( CapabilitiesGet() & bits_CAP_RANGE_ATTACK2 )
	{
		if ( CheckRangeAttack2 ( flDot, flDist ) )
			SetCondition( COND_CAN_RANGE_ATTACK2 );
	}
	if ( CapabilitiesGet() & bits_CAP_MELEE_ATTACK1 )
	{
		if ( CheckMeleeAttack1 ( flDot, flDist ) )
			SetCondition( COND_CAN_MELEE_ATTACK1 );
	}
	if ( CapabilitiesGet() & bits_CAP_MELEE_ATTACK2 )
	{
		if ( CheckMeleeAttack2 ( flDot, flDist ) )
			SetCondition( COND_CAN_MELEE_ATTACK2 );
	}
}

//=========================================================
// SetState
//=========================================================
void CBaseMonster::SetState ( MONSTERSTATE State )
{
/*
	if ( State != m_MonsterState )
	{
		ALERT ( at_aiconsole, "State Changed to %d\n", State );
	}
*/

	MONSTERSTATE OldState;

	OldState = m_MonsterState;
	
	switch( State )
	{
	
	// Drop enemy pointers when going to idle
	case MONSTERSTATE_IDLE:

		if ( m_hEnemy != NULL )
		{
			SetEnemy( NULL ); // not allowed to have an enemy anymore.
			ALERT ( at_aiconsole, "Stripped\n" );
		}
		break;
	}

	bool fNotifyChange = false;

	if( m_MonsterState != State )
	{
		// Don't notify if we're changing to a state we're already in!
		fNotifyChange = true;
	}

	m_MonsterState = State;
	m_IdealMonsterState = State;

	// Notify the monster that its state has changed.
	if( fNotifyChange )
	{
		OnStateChange( OldState, m_MonsterState );
	}
}

//-----------------------------------------------------------------------------
// Sets all sensing-related conditions
//-----------------------------------------------------------------------------
void CBaseMonster::PerformSensing( void )
{
	GetSenses()->PerformSensing();
}

//-----------------------------------------------------------------------------

void CBaseMonster::ClearSenseConditions( void )
{
	static int conditionsToClear[] =
	{
		COND_SEE_HATE,
		COND_SEE_DISLIKE,
		COND_SEE_ENEMY,
		COND_SEE_FEAR,
		COND_SEE_NEMESIS,
		COND_SEE_PLAYER,
		COND_HEAR_DANGER,
		COND_HEAR_COMBAT,
		COND_HEAR_WORLD,
		COND_HEAR_PLAYER,
		COND_SMELL,
		COND_SMELL_FOOD,
	};

	ClearConditions( conditionsToClear, ARRAYSIZE( conditionsToClear ) );
}

//-----------------------------------------------------------------------------

void CBaseMonster::GatherConditions( void )
{
	m_bConditionsGathered = true;

	if ( m_MonsterState != MONSTERSTATE_NONE	&& 
		 m_MonsterState != MONSTERSTATE_PRONE   && 
		 m_MonsterState != MONSTERSTATE_DEAD )// don't bother with this crap if monster is prone. 
	{
		bool bForcedGather = m_bForceConditionsGather;
		m_bForceConditionsGather = false;

		// Sample the environment. Do this unconditionally if there is a player in this
		// monsters's PVS. Monsters in COMBAT state are allowed to simulate when there is no player in
		// the same PVS. This is so that any fights in progress will continue even if the player leaves the PVS.
		if ( bForcedGather || ShouldAlwaysThink() || !FNullEnt( FIND_CLIENT_IN_PVS( edict() ) ) || ( m_MonsterState == MONSTERSTATE_COMBAT ) )
		{
			if ( ShouldPlayIdleSound() )
			{
				IdleSound();
			}

			PerformSensing();

			GetEnemies()->RefreshMemories();
			ChooseEnemy();

			if ( GetCurSchedule() &&
				( m_MonsterState == MONSTERSTATE_IDLE || m_MonsterState == MONSTERSTATE_ALERT) &&
				 GetEnemy() &&
				 !HasCondition( COND_NEW_ENEMY ) && 
				 GetCurSchedule()->HasInterrupt( COND_NEW_ENEMY ) )
			{
				// @Note (toml 05-05-04): There seems to be a case where a monster can not respond
				//						  to COND_NEW_ENEMY. Only evidence right now is save
				//						  games after the fact, so for now, just patching it up
				ALERT( at_aiconsole, "Had to force COND_NEW_ENEMY\n" );
				SetCondition( COND_NEW_ENEMY );
			}
		}
		else
		{
			// if not done, can have problems if leave PVS in same frame heard/saw things, 
			// since only PerformSensing clears conditions
			ClearSenseConditions();
		}

		// do these calculations if monster has an enemy.
		if ( GetEnemy() != NULL )
		{
			CheckEnemy( GetEnemy() );
		}

		CheckAmmo();
	}
}

//=========================================================
// RunAI - main entry point for processing AI
//=========================================================
void CBaseMonster :: RunAI ( void )
{
	// to test model's eye height
	//UTIL_ParticleEffect ( EyePosition(), vec3_origin, 255, 10 );

	// to test monster's position in a squad
	/*if ( GetSquad() )
	{
		if ( ( GetSquad()->IsLeader( this ) || GetSquad()->NumMembers() == 1 ) )
			pev->effects = EF_BRIGHTFIELD;
		else
			pev->effects = EF_LIGHT;
	}*/

	m_bConditionsGathered = false;
	m_bSkippedChooseEnemy = false;

	GatherConditions();
	RemoveIgnoredConditions();

	if ( !m_bConditionsGathered )
		m_bConditionsGathered = true; // derived class didn't call to base

	FCheckAITrigger();

	PrescheduleThink();

	MaintainSchedule();

	PostscheduleThink();

	ClearTransientConditions();
}

//-----------------------------------------------------------------------------
void CBaseMonster::ClearTransientConditions()
{
	// if the monster didn't use these conditions during the above call to MaintainSchedule() or CheckAITrigger()
	// we throw them out cause we don't want them sitting around through the lifespan of a schedule
	// that doesn't use them. 
	ClearCondition( COND_LIGHT_DAMAGE  );
	ClearCondition( COND_HEAVY_DAMAGE );
}


//-----------------------------------------------------------------------------
// Purpose: Surveys the Conditions information available and finds the best
// new state for a monster.
//
// NOTICE the CBaseMonster implementation of this function does not care about
// private conditions!
//
// Output : MONSTERSTATE - the suggested ideal state based on current conditions.
//-----------------------------------------------------------------------------
MONSTERSTATE CBaseMonster::SelectIdealState( void )
{
	// If no schedule conditions, the new ideal state is probably the reason we're in here.

	// ---------------------------
	// Do some squad stuff first
	// ---------------------------
	if ( m_pSquad )
	{
		switch( m_MonsterState )
		{
		case MONSTERSTATE_IDLE:
		case MONSTERSTATE_ALERT:
			if ( HasInterruptCondition( COND_NEW_ENEMY ) )
			{
				m_pSquad->SquadNewEnemy( GetEnemy() );
			}
			break;
		}
	}

	switch ( m_MonsterState )
	{
	case MONSTERSTATE_IDLE:
		
		/*
		IDLE goes to ALERT upon hearing a sound
		-IDLE goes to ALERT upon being injured
		IDLE goes to ALERT upon seeing food
		-IDLE goes to COMBAT upon sighting an enemy
		IDLE goes to HUNT upon smelling food
		*/
		{
			if ( HasInterruptCondition( COND_NEW_ENEMY ) || HasInterruptCondition( COND_SEE_ENEMY ) )
			{
				// new enemy! This means an idle monster has seen someone it dislikes, or 
				// that a monster in combat has found a more suitable target to attack
				return MONSTERSTATE_COMBAT;
			}
			else if ( HasInterruptCondition( COND_LIGHT_DAMAGE ) || HasInterruptCondition( COND_HEAVY_DAMAGE ) )
			{
				if ( GetEnemy() )
				{
					Vector vecEnemyLKP = GetEnemyLKP();
					MakeIdealYaw( vecEnemyLKP );
				}
				else
				{
					// Don't have an enemy, so face the direction the last attack came
					// from. Don't face north.
					MakeIdealYaw( Center() + g_vecAttackDir * 128 );
				}

				return MONSTERSTATE_ALERT;
			}
			else if ( HasInterruptCondition(COND_HEAR_DANGER)  ||
					  HasInterruptCondition(COND_HEAR_COMBAT)  ||
					  HasInterruptCondition(COND_HEAR_WORLD)   ||
					  HasInterruptCondition(COND_HEAR_PLAYER)  )
			{
				// Interrupted by a sound. So make our ideal yaw the
				// source of the sound!
				CSound *pSound;
				
				pSound = GetBestSound();
				ASSERT( pSound != NULL );
				if ( pSound )
				{
					// Build a yaw to the sound or source of the sound,
					// whichever is appropriate
					MakeIdealYaw ( pSound->m_vecOrigin );

					if ( pSound->m_iType & (bits_SOUND_COMBAT|bits_SOUND_DANGER) )
						return MONSTERSTATE_ALERT;
				}
			}
			else if ( HasInterruptCondition( COND_SMELL ) || HasInterruptCondition( COND_SMELL_FOOD ) )
			{
				return MONSTERSTATE_ALERT;
			}

			break;
		}
	case MONSTERSTATE_ALERT:
		/*
		ALERT goes to IDLE upon becoming bored
		-ALERT goes to COMBAT upon sighting an enemy
		ALERT goes to HUNT upon hearing a noise
		*/
		{
			if ( HasInterruptCondition(COND_NEW_ENEMY) ||
				 HasInterruptCondition(COND_SEE_ENEMY) ||
				GetEnemy() != NULL )		
			{
				// see an enemy we MUST attack
				return MONSTERSTATE_COMBAT;
			}
			else if ( HasInterruptCondition( COND_HEAR_DANGER )  ||
					  HasInterruptCondition( COND_HEAR_COMBAT )  ||
					  HasInterruptCondition( COND_HEAR_WORLD )   ||
					  HasInterruptCondition( COND_HEAR_PLAYER ) )
			{
				CSound *pSound = GetBestSound();
				ASSERT( pSound != NULL );
				if ( pSound )
					MakeIdealYaw ( pSound->m_vecOrigin );

				return MONSTERSTATE_ALERT;
			}
			break;
		}
	case MONSTERSTATE_COMBAT:
		/*
		COMBAT goes to HUNT upon losing sight of enemy
		COMBAT goes to ALERT upon death of enemy
		*/
		{
			if ( GetEnemy() == NULL )
			{
				// pev->effects = EF_BRIGHTFIELD;
				ALERT ( at_aiconsole, "***Combat state with no enemy!\n" );
				return MONSTERSTATE_ALERT;
			}
			break;
		}
	case MONSTERSTATE_HUNT:
		/*
		HUNT goes to ALERT upon seeing food
		HUNT goes to ALERT upon being injured
		HUNT goes to IDLE if goal touched
		HUNT goes to COMBAT upon seeing enemy
		*/
		{
			break;
		}
	case MONSTERSTATE_SCRIPT:
		if ( HasInterruptCondition(COND_TASK_FAILED)  ||
			 HasInterruptCondition(COND_LIGHT_DAMAGE) ||
			 HasInterruptCondition(COND_HEAVY_DAMAGE))
		{
			ExitScriptedSequence();	// This will set the ideal state
		}
		break;

	case MONSTERSTATE_DEAD:
		return MONSTERSTATE_DEAD;
		break;
	}

	return m_IdealMonsterState;
}

//=========================================================
// CanCheckAttacks - prequalifies a monster to do more fine
// checking of potential attacks. 
//=========================================================
bool CBaseMonster :: FCanCheckAttacks ( void )
{
	if ( HasCondition( COND_SEE_ENEMY ) && !HasCondition( COND_ENEMY_TOO_FAR) )
	{
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: Return dist. to enemy (closest of origin/head/feet)
// Input  :
// Output :
//-----------------------------------------------------------------------------
float CBaseMonster::EnemyDistance( CBaseEntity *pEnemy )
{
	Vector enemyDelta = pEnemy->Center() - Center();
	
	float enemyHeight = pEnemy->pev->size.z;
	float myHeight = pev->size.z;
	
	// max distance our centers can be apart with the boxes still overlapping
	float flMaxZDist = ( enemyHeight + myHeight ) * 0.5f;

	// see if the enemy is closer to my head, feet or in between
	if ( enemyDelta.z > flMaxZDist )
	{
		// enemy feet above my head, compute distance from my head to his feet
		enemyDelta.z -= flMaxZDist;
	}
	else if ( enemyDelta.z < -flMaxZDist )
	{
		// enemy head below my feet, return distance between my feet and his head
		enemyDelta.z += flMaxZDist;
	}
	else
	{
		// boxes overlap in Z, no delta
		enemyDelta.z = 0;
	}

	return enemyDelta.Length();
}

//-----------------------------------------------------------------------------
// Purpose: Update information on my enemy
// Input  :
// Output : Returns true is new enemy, false is known enemy
//-----------------------------------------------------------------------------
bool CBaseMonster::UpdateEnemyMemory( CBaseEntity *pEnemy, const Vector &position, bool firstHand )
{
	if ( GetEnemies() )
	{
		// If the was eluding me and allow the monster to play a sound
		if (GetEnemies()->HasEludedMe(pEnemy))
		{
			FoundEnemySound();
		}
		bool result = GetEnemies()->UpdateMemory( pEnemy, position );
		if ( firstHand && pEnemy && m_pSquad )
		{
			m_pSquad->UpdateEnemyMemory( this, pEnemy, position );
		}
		return result;
	}
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Remembers the thing my enemy is hiding behind. Called when
//			COND_ENEMY_OCCLUDED is set.
//-----------------------------------------------------------------------------
void CBaseMonster::SetEnemyOccluder(CBaseEntity *pBlocker)
{
	m_hEnemyOccluder = pBlocker;
}

//-----------------------------------------------------------------------------
// Purpose: Gets the thing my enemy is hiding behind (assuming they are hiding).
//-----------------------------------------------------------------------------
CBaseEntity *CBaseMonster::GetEnemyOccluder(void)
{
	return m_hEnemyOccluder;
}

//=========================================================
// CheckEnemy - part of the Condition collection process,
// gets and stores data and conditions pertaining to a monster's
// enemy. Returns TRUE if Enemy LKP was updated.
//=========================================================
void CBaseMonster::CheckEnemy( CBaseEntity *pEnemy )
{
	ClearCondition( COND_ENEMY_FACING_ME );
	
	// ---------------------------
	//  Set visibility conditions
	// --------------------------
	if ( HasCondition( COND_NEW_ENEMY ) || GetSenses()->GetTimeLastUpdate( GetEnemy() ) == gpGlobals->time )
	{
		ClearCondition( COND_ENEMY_OCCLUDED );

		CBaseEntity *pBlocker = NULL;
		SetEnemyOccluder(NULL);

		bool bSensesDidSee = GetSenses()->DidSeeEntity( pEnemy );

		if ( !bSensesDidSee && ( ( EnemyDistance( pEnemy ) >= GetSenses()->GetDistLook() ) || !FVisible( pEnemy, &pBlocker ) ) )
		{
			// No LOS to enemy
			SetEnemyOccluder(pBlocker);
			SetCondition( COND_ENEMY_OCCLUDED );
			ClearCondition( COND_SEE_ENEMY );
		}
		else
		{
			if ( bSensesDidSee )
			{
				// Have LOS and in view cone
				SetCondition( COND_SEE_ENEMY );
			}
			else
			{
				ClearCondition( COND_SEE_ENEMY );
			}
		}
	}

	// -------------------
  	// If enemy is dead
  	// -------------------
	if ( !pEnemy->IsAlive() )
	{
		SetCondition( COND_ENEMY_DEAD );
		ClearCondition( COND_SEE_ENEMY );
		ClearCondition( COND_ENEMY_OCCLUDED );
		return;
	}

	float flDistToEnemy = EnemyDistance( pEnemy );

	if ( HasCondition( COND_SEE_ENEMY ) )
	{
		// Trail the enemy a bit if he's moving
		if (pEnemy->pev->velocity != vec3_origin)
		{
			Vector vTrailPos = pEnemy->pev->origin - pEnemy->pev->velocity * RANDOM_FLOAT( -0.05, 0 );
			UpdateEnemyMemory(pEnemy,vTrailPos);
		}
		else
		{
			UpdateEnemyMemory( pEnemy, pEnemy->pev->origin );
		}

		// If it's not a monster, assume it can't see me
		if ( pEnemy->MyCombatCharacterPointer() && pEnemy->MyCombatCharacterPointer()->FInViewCone ( this ) )
		{
			SetCondition( COND_ENEMY_FACING_ME );
		}
		else
		{
			ClearCondition( COND_ENEMY_FACING_ME );
		}
	}
	else if ( (!HasCondition(COND_ENEMY_OCCLUDED) && !HasCondition(COND_SEE_ENEMY)) && ( flDistToEnemy <= 256 ) )
	{
		// if the enemy is not occluded, and unseen, that means it is behind or beside the monster.
		// if the enemy is near enough the monster, we go ahead and let the monster know where the
		// enemy is. 
		UpdateEnemyMemory( pEnemy, pEnemy->pev->origin );
	}

	if ( flDistToEnemy >= m_flDistTooFar )
	{
		// enemy is very far away from monster
		SetCondition( COND_ENEMY_TOO_FAR );
	}
	else
		ClearCondition( COND_ENEMY_TOO_FAR );

	if ( FCanCheckAttacks() )	
	{
		GatherAttackConditions ( GetEnemy(), flDistToEnemy );
	}
	else
	{
		ClearAttackConditions();
	}

	// If my enemy has moved significantly, or if the enemy has changed update my path
	if ( m_movementGoal == MOVEGOAL_ENEMY )
	{
		for ( int i = m_iRouteIndex; i < ROUTE_SIZE; i++ )
		{
			if ( m_Route[ i ].iType == (bits_MF_IS_GOAL|bits_MF_TO_ENEMY) )
			{
				// UNDONE: Should we allow monsters to override this distance (80?)
				Vector vecEnemyLKP = GetEnemyLKP();
				if ( (m_Route[ i ].vecLocation - vecEnemyLKP).Length() > 80 )
				{
					// Refresh
					FRefreshRoute();
				}
			}
		}
	}

	// Note that unreachablity times out
	if ( IsUnreachable( GetEnemy() ) )
	{
		SetCondition( COND_ENEMY_UNREACHABLE );
	}

	//-----------------------------------------------------------------------
	// If I haven't seen the enemy in a while he may have eluded me
	//-----------------------------------------------------------------------
	if ( gpGlobals->time - GetEnemyLastTimeSeen() > 8 )
	{
		//-----------------------------------------------------------------------
		// I'm at last known position at enemy isn't in sight then has eluded me
		// ----------------------------------------------------------------------
		Vector vecEnemyLKP = GetEnemyLKP();
		if ( ((vecEnemyLKP - pev->origin).Length2D() < 48) && !HasCondition(COND_SEE_ENEMY) )
		{
			MarkEnemyAsEluded();
		}

		//-------------------------------------------------------------------
		// If enemy isn't reachable, I can see last known position and enemy
		// isn't there, then he has eluded me
		// ------------------------------------------------------------------
		if ( !HasCondition(COND_SEE_ENEMY) && HasCondition(COND_ENEMY_UNREACHABLE) )
		{
			if ( !FVisible( vecEnemyLKP ) )
			{
				MarkEnemyAsEluded();
			}
		}
	}
}

//=========================================================
// SetActivity 
//=========================================================
void CBaseMonster::SetActivity( Activity NewActivity )
{
	int	iSequence;

	// If I'm already doing the NewActivity I can bail.
	if ( m_Activity == NewActivity )
	{
		return;
	}

	iSequence = TranslateSequence( NewActivity );

	// Set to the desired anim, or default anim if the desired is not present
	if ( iSequence > ACTIVITY_NOT_AVAILABLE )
	{
		if ( pev->sequence != iSequence || !m_fSequenceLoops )
		{
			// Don't reset frame between movement phased animations
			if ( !IsActivityMovementPhased( m_Activity ) || !IsActivityMovementPhased( NewActivity ) )
				pev->frame = 0;
		}

		pev->sequence		= iSequence;	// Set to the reset anim (if it's there)
		ResetSequenceInfo( );
	}
	else
	{
		// Not available try to get default anim
		ALERT ( at_aiconsole, "%s has no sequence for act:%d\n", STRING(pev->classname), NewActivity );
		pev->sequence		= 0;	// Set to the reset anim (if it's there)
	}

	if (m_Activity != NewActivity)
	{
		OnChangeActivity(NewActivity);
	}

	m_Activity = NewActivity; // Go ahead and set this so it doesn't keep trying when the anim is not present

	// Vasia: this cannot be called until m_Activity stores NewActivity!
	SetYawSpeed();
	
	// In case someone calls this with something other than the ideal activity
	m_IdealActivity = m_Activity;
}

//=========================================================
// MaintainActivity
// Tries to achieve our ideal animation state.
//=========================================================
void CBaseMonster::MaintainActivity( void )
{
	if ( pev->deadflag == DEAD_DEAD )
	{
		// Don't maintain activities if we're dead.
		// Blame Speyrer
		return;
	}

	// Our animation state is being controlled by a script.
	//if ( m_MonsterState == MONSTERSTATE_SCRIPT )
		//return;

	// We may have work to do if we aren't playing our ideal activity
	if ( GetActivity() != m_IdealActivity )
	{
		SetActivity ( m_IdealActivity );
	}

}

//=========================================================
// IsActivityMovementPhased
// Checks to see if the activity is one of the standard phase - matched movement activities
//=========================================================
bool CBaseMonster :: IsActivityMovementPhased( Activity activity )
{
	switch( activity )
	{
	case ACT_WALK:
	case ACT_RUN:
	case ACT_WALK_SCARED:
	case ACT_RUN_SCARED:
	case ACT_WALK_HURT:
	case ACT_RUN_HURT:
		return true;
	}

	return false;
}

//=========================================================
// SetSequenceByName
//=========================================================
void CBaseMonster :: SetSequenceByName ( const char *szSequence )
{
	int	iSequence;

	iSequence = LookupSequence ( szSequence );

	// Set to the desired anim, or default anim if the desired is not present
	if ( iSequence > ACTIVITY_NOT_AVAILABLE )
	{
		if ( pev->sequence != iSequence || !m_fSequenceLoops )
		{
			pev->frame = 0;
		}

		pev->sequence		= iSequence;	// Set to the reset anim (if it's there)
		ResetSequenceInfo( );
		SetYawSpeed(); // Vasia: Won't work properly, check SetActivity for why.
	}
	else
	{
		// Not available try to get default anim
		ALERT ( at_aiconsole, "%s has no sequence named:%f\n", STRING(pev->classname), szSequence );
		pev->sequence		= 0;	// Set to the reset anim (if it's there)
	}
}

//=========================================================
// CheckLocalMove - returns TRUE if the caller can walk a 
// straight line from its current origin to the given 
// location. If so, don't use the node graph!
//
// if a valid pointer to a int is passed, the function
// will fill that int with the distance that the check 
// reached before hitting something. THIS ONLY HAPPENS
// IF THE LOCAL MOVE CHECK FAILS!
//
// !!!PERFORMANCE - should we try to load balance this?
// DON"T USE SETORIGIN! 
//=========================================================
#define	LOCAL_STEP_SIZE	16
int CBaseMonster :: CheckLocalMove ( const Vector &vecStart, const Vector &vecEnd, CBaseEntity *pTarget, float *pflDist )
{
	Vector	vecStartPos;// record monster's position before trying the move
	float	flYaw;
	float	flDist;
	float	flStep, stepSize;
	int		iReturn;

	vecStartPos = pev->origin;
	
	
	flYaw = UTIL_VecToYaw ( vecEnd - vecStart );// build a yaw that points to the goal.
	flDist = ( vecEnd - vecStart ).Length2D();// get the distance.
	iReturn = LOCALMOVE_VALID;// assume everything will be ok.

	// move the monster to the start of the local move that's to be checked.
	UTIL_SetOrigin( pev, vecStart );// !!!BUGBUG - won't this fire triggers? - nope, SetOrigin doesn't fire

	if ( !(pev->flags & (FL_FLY|FL_SWIM)) )
	{
		UTIL_DropToFloor( this );//make sure monster is on the floor!
	}

	//pev->origin.z = vecStartPos.z;//!!!HACKHACK

//	pev->origin = vecStart;

/*
	if ( flDist > 1024 )
	{
		// !!!PERFORMANCE - this operation may be too CPU intensive to try checks this large.
		// We don't lose much here, because a distance this great is very likely
		// to have something in the way.

		// since we've actually moved the monster during the check, undo the move.
		pev->origin = vecStartPos;
		return FALSE;
	}
*/
	// this loop takes single steps to the goal.
	for ( flStep = 0 ; flStep < flDist ; flStep += LOCAL_STEP_SIZE )
	{
		stepSize = LOCAL_STEP_SIZE;

		if ( (flStep + LOCAL_STEP_SIZE) >= (flDist-1) )
			stepSize = (flDist - flStep) - 1;
		
//		UTIL_ParticleEffect ( pev->origin, vec3_origin, 255, 25 );

		if ( !WALK_MOVE( ENT(pev), flYaw, stepSize, WALKMOVE_CHECKONLY ) )
		{// can't take the next step, fail!

			if ( pflDist != NULL )
			{
				*pflDist = flStep;
			}
			if ( pTarget && pTarget->edict() == gpGlobals->trace_ent )
			{
				// if this step hits target ent, the move is legal.
				iReturn = LOCALMOVE_VALID;
				break;
			}
			else
			{
				// If we're going toward an entity, and we're almost getting there, it's OK.
//				if ( pTarget && fabs( flDist - iStep ) < LOCAL_STEP_SIZE )
//					fReturn = TRUE;
//				else
				iReturn = LOCALMOVE_INVALID;
				break;
			}

		}
	}

	if ( iReturn == LOCALMOVE_VALID && 	!(pev->flags & (FL_FLY|FL_SWIM) ) && (!pTarget || (pTarget->pev->flags & FL_ONGROUND)) )
	{
		// The monster can move to a spot UNDER the target, but not to it. Don't try to triangulate, go directly to the node graph.
		// UNDONE: Magic # 64 -- this used to be pev->size.z but that won't work for small creatures like the headcrab
		if ( fabs(vecEnd.z - pev->origin.z) > 64 )
		{
			iReturn = LOCALMOVE_INVALID_DONT_TRIANGULATE;
		}
	}
	/*
	// uncommenting this block will draw a line representing the nearest legal move.
	WRITE_BYTE(MSG_BROADCAST, SVC_TEMPENTITY);
	WRITE_BYTE(MSG_BROADCAST, TE_SHOWLINE);
	WRITE_COORD(MSG_BROADCAST, pev->origin.x);
	WRITE_COORD(MSG_BROADCAST, pev->origin.y);
	WRITE_COORD(MSG_BROADCAST, pev->origin.z);
	WRITE_COORD(MSG_BROADCAST, vecStart.x);
	WRITE_COORD(MSG_BROADCAST, vecStart.y);
	WRITE_COORD(MSG_BROADCAST, vecStart.z);
	*/

	// since we've actually moved the monster during the check, undo the move.
	UTIL_SetOrigin( pev, vecStartPos );

	return iReturn;
}


float CBaseMonster :: OpenDoorAndWait( entvars_t *pevDoor )
{
	float flTravelTime = 0;

	//ALERT(at_aiconsole, "A door. ");
	CBaseEntity *pcbeDoor = CBaseEntity::Instance(pevDoor);
	if (pcbeDoor && !pcbeDoor->IsLockedByMaster())
	{
		//ALERT(at_aiconsole, "unlocked! ");
		pcbeDoor->Use(this, this, USE_ON, 0.0);
		//ALERT(at_aiconsole, "pevDoor->nextthink = %d ms\n", (int)(1000*pevDoor->nextthink));
		//ALERT(at_aiconsole, "pevDoor->ltime = %d ms\n", (int)(1000*pevDoor->ltime));
		//ALERT(at_aiconsole, "pev-> nextthink = %d ms\n", (int)(1000*pev->nextthink));
		//ALERT(at_aiconsole, "pev->ltime = %d ms\n", (int)(1000*pev->ltime));
		flTravelTime = pevDoor->nextthink - pevDoor->ltime;
		//ALERT(at_aiconsole, "Waiting %d ms\n", (int)(1000*flTravelTime));
		if ( pcbeDoor->pev->targetname )
		{
			edict_t *pentTarget = NULL;
			for (;;)
			{
				pentTarget = FIND_ENTITY_BY_TARGETNAME( pentTarget, STRING(pcbeDoor->pev->targetname));

				if ( VARS( pentTarget ) != pcbeDoor->pev )
				{
					if (FNullEnt(pentTarget))
						break;

					if ( FClassnameIs ( pentTarget, STRING(pcbeDoor->pev->classname) ) )
					{
						CBaseEntity *pDoor = Instance(pentTarget);
						if ( pDoor )
							pDoor->Use(this, this, USE_ON, 0.0);
					}
				}
			}
		}
	}

	return gpGlobals->time + flTravelTime;
}

bool CBaseMonster::ShouldMoveWait()
{
	return (m_flMoveWaitFinished > gpGlobals->time);
}


//=========================================================
// AdvanceRoute - poorly named function that advances the 
// m_iRouteIndex. If it goes beyond ROUTE_SIZE, the route 
// is refreshed. 
//=========================================================
void CBaseMonster :: AdvanceRoute ( float distance )
{

	if ( m_iRouteIndex == ROUTE_SIZE - 1 )
	{
		// time to refresh the route.
		if ( !FRefreshRoute() )
		{
			ALERT ( at_aiconsole, "Can't Refresh Route!!\n" );
		}
	}
	else
	{
		if ( ! (m_Route[ m_iRouteIndex ].iType & bits_MF_IS_GOAL) )
		{
			// If we've just passed a path_corner, advance m_pGoalEnt
			if ( (m_Route[ m_iRouteIndex ].iType & ~bits_MF_NOT_TO_MASK) == bits_MF_TO_PATHCORNER )
				m_pGoalEnt = m_pGoalEnt->GetNextTarget();

			// IF both waypoints are nodes, then check for a link for a door and operate it.
			//
			if (  (m_Route[m_iRouteIndex].iType   & bits_MF_TO_NODE) == bits_MF_TO_NODE
			   && (m_Route[m_iRouteIndex+1].iType & bits_MF_TO_NODE) == bits_MF_TO_NODE)
			{
				//ALERT(at_aiconsole, "SVD: Two nodes. ");

				int iSrcNode  = WorldGraph.FindNearestNode(m_Route[m_iRouteIndex].vecLocation, this );
				int iDestNode = WorldGraph.FindNearestNode(m_Route[m_iRouteIndex+1].vecLocation, this );

				int iLink;
				WorldGraph.HashSearch(iSrcNode, iDestNode, iLink);

				if ( iLink >= 0 && WorldGraph.m_pLinkPool[iLink].m_pLinkEnt != NULL )
				{
					//ALERT(at_aiconsole, "A link. ");
					if ( WorldGraph.HandleLinkEnt ( iSrcNode, WorldGraph.m_pLinkPool[iLink].m_pLinkEnt, m_afCapability, CGraph::NODEGRAPH_DYNAMIC ) )
					{
						//ALERT(at_aiconsole, "usable.");
						entvars_t *pevDoor = WorldGraph.m_pLinkPool[iLink].m_pLinkEnt;
						if (pevDoor)
						{
							m_flMoveWaitFinished = OpenDoorAndWait( pevDoor );
//							ALERT( at_aiconsole, "Wating for door %.2f\n", m_flMoveWaitFinished-gpGlobals->time );
						}
					}
				}
				//ALERT(at_aiconsole, "\n");
			}
			m_iRouteIndex++;
		}
		else	// At goal!!!
		{
			if ( distance < m_flGroundSpeed * 0.2 /* FIX */ )
			{
				TaskMovementComplete();
			}
		}
	}
}


int CBaseMonster :: RouteClassify( int iMoveFlag )
{
	int movementGoal;

	movementGoal = MOVEGOAL_NONE;

	if ( iMoveFlag & bits_MF_TO_TARGETENT )
		movementGoal = MOVEGOAL_TARGETENT;
	else if ( iMoveFlag & bits_MF_TO_ENEMY )
		movementGoal = MOVEGOAL_ENEMY;
	else if ( iMoveFlag & bits_MF_TO_PATHCORNER )
		movementGoal = MOVEGOAL_PATHCORNER;
	else if ( iMoveFlag & bits_MF_TO_NODE )
		movementGoal = MOVEGOAL_NODE;
	else if ( iMoveFlag & bits_MF_TO_LOCATION )
		movementGoal = MOVEGOAL_LOCATION;

	return movementGoal;
}

//=========================================================
// BuildRoute
//=========================================================
bool CBaseMonster :: BuildRoute ( const Vector &vecGoal, int iMoveFlag, CBaseEntity *pTarget )
{
	float	flDist;
	Vector	vecApex;
	int		iLocalMove;

	RouteNew();
	m_movementGoal = RouteClassify( iMoveFlag );

// so we don't end up with no moveflags
	m_Route[ 0 ].vecLocation = vecGoal;
	m_Route[ 0 ].iType = iMoveFlag | bits_MF_IS_GOAL;

// check simple local move
	iLocalMove = CheckLocalMove( pev->origin, vecGoal, pTarget, &flDist );

	if ( iLocalMove == LOCALMOVE_VALID )
	{
		// monster can walk straight there!
		return TRUE;
	}
// try to triangulate around any obstacles.
	else if ( iLocalMove != LOCALMOVE_INVALID_DONT_TRIANGULATE && FTriangulate( pev->origin, vecGoal, flDist, pTarget, &vecApex ) )
	{
		// there is a slightly more complicated path that allows the monster to reach vecGoal
		m_Route[ 0 ].vecLocation = vecApex;
		m_Route[ 0 ].iType = (iMoveFlag | bits_MF_TO_DETOUR);

		m_Route[ 1 ].vecLocation = vecGoal;
		m_Route[ 1 ].iType = iMoveFlag | bits_MF_IS_GOAL;

			/*
			WRITE_BYTE(MSG_BROADCAST, SVC_TEMPENTITY);
			WRITE_BYTE(MSG_BROADCAST, TE_SHOWLINE);
			WRITE_COORD(MSG_BROADCAST, vecApex.x );
			WRITE_COORD(MSG_BROADCAST, vecApex.y );
			WRITE_COORD(MSG_BROADCAST, vecApex.z );
			WRITE_COORD(MSG_BROADCAST, vecApex.x );
			WRITE_COORD(MSG_BROADCAST, vecApex.y );
			WRITE_COORD(MSG_BROADCAST, vecApex.z + 128 );
			*/

		RouteSimplify( pTarget );
		return TRUE;
	}

// last ditch, try nodes
	if ( FGetNodeRoute( vecGoal ) )
	{
//		ALERT ( at_console, "Can get there on nodes\n" );
		m_vecMoveGoal = vecGoal;
		RouteSimplify( pTarget );
		return TRUE;
	}

	// b0rk
	return FALSE;
}


//=========================================================
// InsertWaypoint - Rebuilds the existing route so that the
// supplied vector and moveflags are the first waypoint in
// the route, and fills the rest of the route with as much
// of the pre-existing route as possible
//=========================================================
void CBaseMonster :: InsertWaypoint ( Vector vecLocation, int afMoveFlags )
{
	int			i, type;

	
	// we have to save some Index and Type information from the real
	// path_corner or node waypoint that the monster was trying to reach. This makes sure that data necessary 
	// to refresh the original path exists even in the new waypoints that don't correspond directy to a path_corner
	// or node. 
	type = afMoveFlags | (m_Route[ m_iRouteIndex ].iType & ~bits_MF_NOT_TO_MASK);

	for ( i = ROUTE_SIZE-1; i > 0; i-- )
		m_Route[i] = m_Route[i-1];

	m_Route[ m_iRouteIndex ].vecLocation = vecLocation;
	m_Route[ m_iRouteIndex ].iType = type;
}

//=========================================================
// FTriangulate - tries to overcome local obstacles by 
// triangulating a path around them.
//
// iApexDist is how far the obstruction that we are trying
// to triangulate around is from the monster.
//=========================================================
bool CBaseMonster :: FTriangulate ( const Vector &vecStart , const Vector &vecEnd, float flDist, CBaseEntity *pTargetEnt, Vector *pApex )
{
	Vector		vecDir;
	Vector		vecForward;
	Vector		vecLeft;// the spot we'll try to triangulate to on the left
	Vector		vecRight;// the spot we'll try to triangulate to on the right
	Vector		vecTop;// the spot we'll try to triangulate to on the top
	Vector		vecBottom;// the spot we'll try to triangulate to on the bottom
	Vector		vecFarSide;// the spot that we'll move to after hitting the triangulated point, before moving on to our normal goal.
	int			i;
	float		sizeX, sizeZ;

	// If the hull width is less than 24, use 24 because CheckLocalMove uses a min of
	// 24.
	sizeX = pev->size.x;
	if (sizeX < 24.0)
		sizeX = 24.0;
	else if (sizeX > 48.0)
		sizeX = 48.0;
	sizeZ = pev->size.z;
	//if (sizeZ < 24.0)
	//	sizeZ = 24.0;

	vecForward = ( vecEnd - vecStart ).Normalize();

	Vector vecDirUp(0,0,1);
	vecDir = CrossProduct ( vecForward, vecDirUp);

	// start checking right about where the object is, picking two equidistant starting points, one on
	// the left, one on the right. As we progress through the loop, we'll push these away from the obstacle, 
	// hoping to find a way around on either side. pev->size.x is added to the ApexDist in order to help select
	// an apex point that insures that the monster is sufficiently past the obstacle before trying to turn back
	// onto its original course.

	vecLeft = pev->origin + ( vecForward * ( flDist + sizeX ) ) - vecDir * ( sizeX * 3 );
	vecRight = pev->origin + ( vecForward * ( flDist + sizeX ) ) + vecDir * ( sizeX * 3 );
	if (pev->movetype == MOVETYPE_FLY)
	{
		vecTop = pev->origin + (vecForward * flDist) + (vecDirUp * sizeZ * 3);
		vecBottom = pev->origin + (vecForward * flDist) - (vecDirUp *  sizeZ * 3);
	}

	vecFarSide = m_Route[ m_iRouteIndex ].vecLocation;
	
	vecDir = vecDir * sizeX * 2;
	if (pev->movetype == MOVETYPE_FLY)
		vecDirUp = vecDirUp * sizeZ * 2;

	for ( i = 0 ; i < 8; i++ )
	{
// Debug, Draw the triangulation
#if 0
		MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
			WRITE_BYTE( TE_SHOWLINE);
			WRITE_COORD( pev->origin.x );
			WRITE_COORD( pev->origin.y );
			WRITE_COORD( pev->origin.z );
			WRITE_COORD( vecRight.x );
			WRITE_COORD( vecRight.y );
			WRITE_COORD( vecRight.z );
		MESSAGE_END();

		MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
			WRITE_BYTE( TE_SHOWLINE );
			WRITE_COORD( pev->origin.x );
			WRITE_COORD( pev->origin.y );
			WRITE_COORD( pev->origin.z );
			WRITE_COORD( vecLeft.x );
			WRITE_COORD( vecLeft.y );
			WRITE_COORD( vecLeft.z );
		MESSAGE_END();
#endif

#if 0
		if (pev->movetype == MOVETYPE_FLY)
		{
			MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
				WRITE_BYTE( TE_SHOWLINE );
				WRITE_COORD( pev->origin.x );
				WRITE_COORD( pev->origin.y );
				WRITE_COORD( pev->origin.z );
				WRITE_COORD( vecTop.x );
				WRITE_COORD( vecTop.y );
				WRITE_COORD( vecTop.z );
			MESSAGE_END();

			MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
				WRITE_BYTE( TE_SHOWLINE );
				WRITE_COORD( pev->origin.x );
				WRITE_COORD( pev->origin.y );
				WRITE_COORD( pev->origin.z );
				WRITE_COORD( vecBottom.x );
				WRITE_COORD( vecBottom.y );
				WRITE_COORD( vecBottom.z );
			MESSAGE_END();
		}
#endif

		if ( CheckLocalMove( pev->origin, vecRight, pTargetEnt, NULL ) == LOCALMOVE_VALID )
		{
			if ( CheckLocalMove ( vecRight, vecFarSide, pTargetEnt, NULL ) == LOCALMOVE_VALID )
			{
				if ( pApex )
				{
					*pApex = vecRight;
				}

				return TRUE;
			}
		}
		if ( CheckLocalMove( pev->origin, vecLeft, pTargetEnt, NULL ) == LOCALMOVE_VALID )
		{
			if ( CheckLocalMove ( vecLeft, vecFarSide, pTargetEnt, NULL ) == LOCALMOVE_VALID )
			{
				if ( pApex )
				{
					*pApex = vecLeft;
				}

				return TRUE;
			}
		}

		if (pev->movetype == MOVETYPE_FLY)
		{
			if ( CheckLocalMove( pev->origin, vecTop, pTargetEnt, NULL ) == LOCALMOVE_VALID)
			{
				if ( CheckLocalMove ( vecTop, vecFarSide, pTargetEnt, NULL ) == LOCALMOVE_VALID )
				{
					if ( pApex )
					{
						*pApex = vecTop;
						//ALERT(at_aiconsole, "triangulate over\n");
					}

					return TRUE;
				}
			}
#if 1
			if ( CheckLocalMove( pev->origin, vecBottom, pTargetEnt, NULL ) == LOCALMOVE_VALID )
			{
				if ( CheckLocalMove ( vecBottom, vecFarSide, pTargetEnt, NULL ) == LOCALMOVE_VALID )
				{
					if ( pApex )
					{
						*pApex = vecBottom;
						//ALERT(at_aiconsole, "triangulate under\n");
					}

					return TRUE;
				}
			}
#endif
		}

		vecRight = vecRight + vecDir;
		vecLeft = vecLeft - vecDir;
		if (pev->movetype == MOVETYPE_FLY)
		{
			vecTop = vecTop + vecDirUp;
			vecBottom = vecBottom - vecDirUp;
		}
	}

	return FALSE;
}

//=========================================================
// Move - take a single step towards the next ROUTE location
//=========================================================
#define DIST_TO_CHECK	200

void CBaseMonster::Move( float flInterval ) 
{
	float		flWaypointDist;
	float		flCheckDist;
	float		flDist;// how far the lookahead check got before hitting an object.
	Vector		vecDir;
	Vector		vecApex;
	CBaseEntity	*pTargetEnt;

	if (flInterval > 1.0)
	{
		// Bound interval so we don't get ludicrous motion when debugging
		// or when framerate drops catostrophically
		flInterval = 1.0;
	}

	if ( !OverrideMove( flInterval ) )
	{
		// UNDONE: Figure out how much of the timestep was consumed by movement
		// this frame and restart the movement/schedule engine if necessary

		bool fShouldMove = ( m_movementGoal != MOVEGOAL_NONE && m_movementActivity > ACT_IDLE );

		if ( fShouldMove )
		{

			// Don't move if no valid route
			if ( FRouteClear() )
			{
				// If we still have a movement goal, then this is probably a route truncated by SimplifyRoute()
				// so refresh it.
				if ( m_movementGoal == MOVEGOAL_NONE || !FRefreshRoute() )
				{
					ALERT( at_aiconsole, "Tried to move with no route!\n" );
					TaskFail();
					return;
				}
			}
	
			// If I've been asked to wait, let's wait
			if ( m_flMoveWaitFinished > gpGlobals->time )
				return;

// Debug, test movement code
#if 0
//		if ( CVAR_GET_FLOAT("stopmove" ) != 0 )
		{
			if ( m_movementGoal == MOVEGOAL_ENEMY )
				RouteSimplify( m_hEnemy );
			else
				RouteSimplify( m_hTargetEnt );
			FRefreshRoute();
			return;
		}
#else
//	Debug, draw the route
//		DrawRoute( pev, m_Route, m_iRouteIndex, 0, 200, 0 );
#endif

			// if the monster is moving directly towards an entity (enemy for instance), we'll set this pointer
			// to that entity for the CheckLocalMove and Triangulate functions.
			pTargetEnt = NULL;

			// local move to waypoint.
			vecDir = ( m_Route[ m_iRouteIndex ].vecLocation - pev->origin ).Normalize();
			flWaypointDist = ( m_Route[ m_iRouteIndex ].vecLocation - pev->origin ).Length2D();

			//GRUNT STRAFE:: This must go away!!!
			if ( m_movementActivity == ACT_STRAFE_RIGHT || m_movementActivity == ACT_STRAFE_LEFT )
			{ 
			}
			else
			{
				// turn in the direction of movement
				MakeIdealYaw ( m_Route[ m_iRouteIndex ].vecLocation );
	
				ChangeYaw(pev->yaw_speed);
			}

			// if the waypoint is closer than CheckDist, CheckDist is the dist to waypoint
			if ( flWaypointDist < DIST_TO_CHECK )
			{
				flCheckDist = flWaypointDist;
			}
			else
			{
				flCheckDist = DIST_TO_CHECK;
			}
	
			if ( (m_Route[ m_iRouteIndex ].iType & (~bits_MF_NOT_TO_MASK)) == bits_MF_TO_ENEMY )
			{
				// only on a PURE move to enemy ( i.e., ONLY MF_TO_ENEMY set, not MF_TO_ENEMY and DETOUR )
				pTargetEnt = m_hEnemy;
			}
			else if ( (m_Route[ m_iRouteIndex ].iType & ~bits_MF_NOT_TO_MASK) == bits_MF_TO_TARGETENT )
			{
				pTargetEnt = m_hTargetEnt;
			}

			// !!!BUGBUG - CheckDist should be derived from ground speed.
			// If this fails, it should be because of some dynamic entity blocking this guy.
			// We've already checked this path, so we should wait and time out if the entity doesn't move
			flDist = 0;
			if ( CheckLocalMove ( pev->origin, pev->origin + vecDir * flCheckDist, pTargetEnt, &flDist ) != LOCALMOVE_VALID )
			{
				CBaseEntity *pBlocker;

				// Can't move, stop
				Stop();
				// Blocking entity is in global trace_ent
				pBlocker = CBaseEntity::Instance( gpGlobals->trace_ent );
				if (pBlocker)
				{
					DispatchBlocked( edict(), pBlocker->edict() );
				}

				if ( pBlocker && m_moveWaitTime > 0 && pBlocker->IsMoving() && !pBlocker->IsPlayer() && (gpGlobals->time-m_flMoveWaitFinished) > 3.0 )
				{
					// Can we still move toward our target?
					if ( flDist < m_flGroundSpeed )
					{
						// No, Wait for a second
						m_flMoveWaitFinished = gpGlobals->time + m_moveWaitTime;
						return;
					}	
					// Ok, still enough room to take a step
				}
				else 
				{
					// try to triangulate around whatever is in the way.
					if ( FTriangulate( pev->origin, m_Route[ m_iRouteIndex ].vecLocation, flDist, pTargetEnt, &vecApex ) )
					{
						InsertWaypoint( vecApex, bits_MF_TO_DETOUR );
						RouteSimplify( pTargetEnt );
					}
					else
					{
//						ALERT ( at_aiconsole, "Couldn't Triangulate\n" );
						Stop();
						// Only do this once until your route is cleared
						if ( m_moveWaitTime > 0 && !(m_afMemory & bits_MEMORY_MOVE_FAILED) )
						{
							FRefreshRoute();
							if ( FRouteClear() )
							{
								TaskFail();
							}
							else
							{
								// Don't get stuck
								if ( (gpGlobals->time - m_flMoveWaitFinished) < 0.2 )
									Remember( bits_MEMORY_MOVE_FAILED );

								m_flMoveWaitFinished = gpGlobals->time + 0.1;
							}
						}
						else
						{
							TaskFail();
							ALERT( at_aiconsole, "%s Failed to move (%d)!\n", STRING(pev->classname), HasMemory( bits_MEMORY_MOVE_FAILED ) );
							//ALERT( at_aiconsole, "%f, %f, %f\n", pev->origin.z, (pev->origin + (vecDir * flCheckDist)).z, m_Route[m_iRouteIndex].vecLocation.z );
						}	
						return;
					}
				}	
			}

			// close enough to the target, now advance to the next target. This is done before actually reaching
			// the target so that we get a nice natural turn while moving.
			if ( ShouldAdvanceRoute( flWaypointDist ) )///!!!BUGBUG- magic number
			{
				AdvanceRoute( flWaypointDist );
			}

			// Might be waiting for a door
			if ( m_flMoveWaitFinished > gpGlobals->time )
			{
				Stop();
				return;
			}

			// UNDONE: this is a hack to quit moving farther than it has looked ahead.
			if (flCheckDist < m_flGroundSpeed * flInterval)
			{
				flInterval = flCheckDist / m_flGroundSpeed;
				// ALERT( at_console, "%.02f\n", flInterval );
			}

			MoveExecute( pTargetEnt, vecDir, flInterval );

			if ( MovementIsComplete() )
			{
				Stop();
				RouteClear();
			}

		}
#if _DEBUG	
		else 
		{
			if ( !TaskIsRunning() && !TaskIsComplete() )
				ALERT( at_error, "Schedule stalled!!\n" );
		}
#endif
	}
}


bool CBaseMonster:: ShouldAdvanceRoute( float flWaypointDist )
{
	if ( flWaypointDist <= MONSTER_CUT_CORNER_DIST )
	{
		// ALERT( at_console, "cut %f\n", flWaypointDist );
		return TRUE;
	}

	return FALSE;
}


void CBaseMonster::MoveExecute( CBaseEntity *pTargetEnt, const Vector &vecDir, float flInterval )
{
//	float flYaw = UTIL_VecToYaw ( m_Route[ m_iRouteIndex ].vecLocation - pev->origin );// build a yaw that points to the goal.
//	WALK_MOVE( ENT(pev), flYaw, m_flGroundSpeed * flInterval, WALKMOVE_NORMAL );
	if ( m_IdealActivity != m_movementActivity )
		m_IdealActivity = m_movementActivity;

	int iMovementFlag = MOVE_NORMAL;

	if ( m_movementActivity == ACT_STRAFE_LEFT || m_movementActivity == ACT_STRAFE_RIGHT )
		iMovementFlag = MOVE_STRAFE;

	float flTotal = m_flGroundSpeed * pev->framerate * flInterval;
	float flStep;
	while (flTotal > 0.001)
	{
		// don't walk more than 16 units or stairs stop working
		flStep = V_min( 16.0, flTotal );
		UTIL_MoveToOrigin ( ENT(pev), m_Route[ m_iRouteIndex ].vecLocation, flStep, iMovementFlag );
		flTotal -= flStep;
	}
	// ALERT( at_console, "dist %f\n", m_flGroundSpeed * pev->framerate * flInterval );
}


//=========================================================
// MonsterInit - after a monster is spawned, it needs to 
// be dropped into the world, checked for mobility problems,
// and put on the proper path, if any. This function does
// all of those things after the monster spawns. Any
// initialization that should take place for all monsters
// goes here.
//=========================================================
void CBaseMonster :: MonsterInit ( void )
{
	if (!g_pGameRules->FAllowMonsters())
	{
		UTIL_Remove( this );			// Post this because some monster code modifies class data after calling this function
		return;
	}

	// Set fields common to all monsters
	SetBits (pev->flags, FL_MONSTER);
	if ( pev->spawnflags & SF_MONSTER_HITMONSTERCLIP )
		pev->flags |= FL_MONSTERCLIP;

	pev->effects		= 0;
	pev->takedamage		= DAMAGE_AIM;
	pev->ideal_yaw		= pev->angles.y;
	pev->max_health		= pev->health;
	pev->deadflag		= DEAD_NO;
	m_IdealMonsterState	= MONSTERSTATE_IDLE;// Assume monster will be idle, until proven otherwise
	m_IdealActivity = ACT_IDLE;

	ClearSchedule();
	RouteClear();
	InitBoneControllers( ); // FIX: should be done in Spawn

	m_iHintNode			= NO_NODE;

	m_afMemory			= MEMORY_CLEAR;

	SetEnemy( NULL );

	m_flDistTooFar		= 1024.0;
	SetDistLook( 2048.0 );

	// Clear conditions
	m_Conditions.ClearAll();

	// set eye position
	SetEyePosition();

	SetUse ( &CBaseMonster::MonsterUse );

	// NOTE: Can't call Monster Init Think directly... logic changed about
	// what time it is when worldspawn happens..

	// We're at worldspawn, so we must put off the rest of our initialization
	// until we're sure everything else has had a chance to spawn. Otherwise
	// we may try to reference entities that haven't spawned yet.(sjb)
	SetThink( &CBaseMonster::MonsterInitThink );
	pev->nextthink = gpGlobals->time + 0.01f;

	ForceGatherConditions();

	m_GiveUpOnDeadEnemyTimer.Set( 0.75, 2.0 );

	m_EnemiesSerialNumber = -1;
}

//=========================================================
// MonsterInitThink - Calls StartMonster. Startmonster is 
// virtual, but this function cannot be 
//=========================================================
void CBaseMonster :: MonsterInitThink ( void )
{
	StartMonster();
}

//=========================================================
// StartMonster - final bit of initization before a monster 
// is turned over to the AI. 
//=========================================================
void CBaseMonster :: StartMonster ( void )
{
	// update capabilities
	if ( LookupActivity ( ACT_RANGE_ATTACK1 ) != ACTIVITY_NOT_AVAILABLE )
	{
		m_afCapability |= bits_CAP_RANGE_ATTACK1;
	}
	if ( LookupActivity ( ACT_RANGE_ATTACK2 ) != ACTIVITY_NOT_AVAILABLE )
	{
		m_afCapability |= bits_CAP_RANGE_ATTACK2;
	}
	if ( LookupActivity ( ACT_MELEE_ATTACK1 ) != ACTIVITY_NOT_AVAILABLE )
	{
		m_afCapability |= bits_CAP_MELEE_ATTACK1;
	}
	if ( LookupActivity ( ACT_MELEE_ATTACK2 ) != ACTIVITY_NOT_AVAILABLE )
	{
		m_afCapability |= bits_CAP_MELEE_ATTACK2;
	}

	// Raise monster off the floor one unit, then drop to floor
	if ( pev->movetype != MOVETYPE_FLY && !FBitSet( pev->spawnflags, SF_MONSTER_FALL_TO_GROUND ) )
	{
		pev->origin.z += 1;
		UTIL_DropToFloor( this );
		// Try to move the monster to make sure it's not stuck in a brush.
		if (!WALK_MOVE ( ENT(pev), 0, 0, WALKMOVE_NORMAL ) )
		{
			ALERT(at_error, "Monster %s stuck in wall--level design error\n", STRING(pev->classname));
			pev->effects = EF_BRIGHTFIELD;
		}
	}
	else 
	{
		pev->flags &= ~FL_ONGROUND;
	}
	
	if ( !FStringNull(pev->target) )// this monster has a target
	{
		// Find the monster's initial target entity, stash it
		m_pGoalEnt = CBaseEntity::Instance( FIND_ENTITY_BY_TARGETNAME ( NULL, STRING( pev->target ) ) );

		if ( !m_pGoalEnt )
		{
			ALERT(at_error, "ReadyMonster()--%s couldn't find target %s\n", STRING(pev->classname), STRING(pev->target));
		}
		else
		{
			// Monster will start turning towards his destination
			MakeIdealYaw ( m_pGoalEnt->pev->origin );

			// JAY: How important is this error message?  Big Momma doesn't obey this rule, so I took it out.
#if 0
			// At this point, we expect only a path_corner as initial goal
			if (!FClassnameIs( m_pGoalEnt->pev, "path_corner"))
			{
				ALERT(at_warning, "ReadyMonster--monster's initial goal '%s' is not a path_corner", STRING(pev->target));
			}
#endif

			// set the monster up to walk a path corner path. 
			// !!!BUGBUG - this is a minor bit of a hack.
			// JAYJAY
			m_movementGoal = MOVEGOAL_PATHCORNER;
			
			if ( pev->movetype == MOVETYPE_FLY )
				m_movementActivity = ACT_FLY;
			else
				m_movementActivity = ACT_WALK;

			if ( !FRefreshRoute() )
			{
				ALERT ( at_aiconsole, "Can't Create Route!\n" );
			}
			SetState( MONSTERSTATE_IDLE );
			SetSchedule( SCHED_IDLE_WALK );
		}
	}
	
	//SetState ( m_IdealMonsterState );
	//SetActivity ( m_IdealActivity );

	InitSquad();

	if ( gpGlobals->time > 2.0 )
	{
		// If this monster is spawning late in the game, just push through the rest of the initialization
		// start thinking right now.
		SetThink ( &CBaseMonster::CallMonsterThink );
		pev->nextthink = gpGlobals->time;
	}
	else
	{ 
		// Delay drop to floor to make sure each door in the level has had its chance to spawn
		// Spread think times so that they don't all happen at the same time (Carmack)
		SetThink ( &CBaseMonster::CallMonsterThink );
		pev->nextthink += RANDOM_FLOAT(0.1, 0.4); // spread think times.
	}
	
	if ( FBitSet( pev->spawnflags, SF_MONSTER_WAIT_FOR_SCRIPT ) )// wait until triggered
	{
		SetState( MONSTERSTATE_IDLE );
		// UNDONE: Some scripted sequence monsters don't have an idle?
		SetActivity( ACT_IDLE );
		SetSchedule( SCHED_WAIT_TRIGGER );
	}
}

//-----------------------------------------------------------------------------
// Purpose: Connect my memory to the squad's
//-----------------------------------------------------------------------------
bool CBaseMonster::InitSquad( void )
{
	// -------------------------------------------------------
	//  If I form squads add me to a squad
	// -------------------------------------------------------
	if ( !m_pSquad && ( CapabilitiesGet() & bits_CAP_SQUAD ) )
	{
		if ( !FStringNull( pev->netname ) )
		{
			// if I have a groupname, I can only recruit if I'm flagged as leader
			if ( !HasSpawnFlags( SF_MONSTER_SQUADLEADER ) )
			{
				return false;
			}
		}

		// try to form squads now.
		int iSquadSize = SquadRecruit( 1024 );

		if ( iSquadSize )
		{
		  ALERT ( at_aiconsole, "Squad of %d %s formed\n", iSquadSize, STRING( pev->classname ) );
		}
	}

	return ( m_pSquad != NULL );
}


//-----------------------------------------------------------------------------
// Purpose: Get the memory for this monster
//-----------------------------------------------------------------------------
CAI_Enemies *CBaseMonster::GetEnemies( void )
{
	return m_pEnemies;
}

//-----------------------------------------------------------------------------
// Purpose: Remove this monster's memory
//-----------------------------------------------------------------------------
void CBaseMonster::RemoveMemory( void )
{
	delete m_pEnemies;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CBaseMonster::TaskComplete( bool fIgnoreSetFailedCondition )
{
	// Handy thing to use for debugging
	//if (IsCurSchedule(SCHED_PUT_HERE) &&
	//	GetTask()->iTask == TASK_PUT_HERE)
	//{
	//	int put_breakpoint_here = 5;
	//}

	if ( fIgnoreSetFailedCondition || !HasCondition( COND_TASK_FAILED ) )
	{
		SetTaskStatus( TASKSTATUS_COMPLETE );
	}
}

void CBaseMonster::TaskMovementComplete( void )
{ 
	switch( GetTaskStatus() )
	{
	case TASKSTATUS_NEW:
	case TASKSTATUS_RUN_MOVE_AND_TASK:
		SetTaskStatus( TASKSTATUS_RUN_TASK );
		break;

	case TASKSTATUS_RUN_MOVE:
		TaskComplete();
		break;
	
	case TASKSTATUS_RUN_TASK:
		ALERT( at_error, "Movement completed twice!\n" );
		break;

	case TASKSTATUS_COMPLETE:		
		break;
	}

	// Now clear the path, it's done.
	m_movementGoal = MOVEGOAL_NONE;
}


int CBaseMonster::TaskIsRunning( void )
{
	if ( GetTaskStatus() != TASKSTATUS_COMPLETE &&
		 GetTaskStatus() != TASKSTATUS_RUN_MOVE )
		 return 1;

	return 0;
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CBaseMonster::TaskFail( void )
{
	// Handy tool for debugging
	//if (IsCurSchedule(SCHED_PUT_NAME_HERE))
	//{
	//	int put_breakpoint_here = 5;
	//}

	SetCondition( COND_TASK_FAILED );
}

//=========================================================
// FindCover - tries to find a nearby node that will hide
// the caller from its enemy. 
//
// If supplied, search will return a node at least as far
// away as MinDist, but no farther than MaxDist. 
// if MaxDist isn't supplied, it defaults to a reasonable 
// value
//=========================================================
// UNDONE: Should this find the nearest node?

//float CGraph::PathLength( int iStart, int iDest, int iHull, int afCapMask )

bool CBaseMonster :: FindCover ( Vector vecThreat, Vector vecViewOffset, float flMinDist, float flMaxDist )
{
	int i;
	int iMyHullIndex;
	int iMyNode;
	int iThreatNode;
	float flDist;
	Vector	vecLookersOffset;
	TraceResult tr;

	if ( !flMaxDist )
	{
		// user didn't supply a MaxDist, so work up a crazy one.
		flMaxDist = 784;
	}

	if ( flMinDist > 0.5 * flMaxDist)
	{
#if _DEBUG
		ALERT ( at_console, "FindCover MinDist (%.0f) too close to MaxDist (%.0f)\n", flMinDist, flMaxDist );
#endif
		flMinDist = 0.5 * flMaxDist;
	}

	if ( !WorldGraph.m_fGraphPresent || !WorldGraph.m_fGraphPointersSet )
	{
		ALERT ( at_aiconsole, "Graph not ready for findcover!\n" );
		return FALSE;
	}

	iMyNode = WorldGraph.FindNearestNode( pev->origin, this );
	iThreatNode = WorldGraph.FindNearestNode ( vecThreat, this );
	iMyHullIndex = WorldGraph.HullIndex( this );

	if ( iMyNode == NO_NODE )
	{
		ALERT ( at_aiconsole, "FindCover() - %s has no nearest node!\n", STRING(pev->classname));
		return FALSE;
	}
	if ( iThreatNode == NO_NODE )
	{
		// ALERT ( at_aiconsole, "FindCover() - Threat has no nearest node!\n" );
		iThreatNode = iMyNode;
		// return FALSE;
	}

	vecLookersOffset = vecThreat + vecViewOffset;// calculate location of enemy's eyes

	// we'll do a rough sample to find nodes that are relatively nearby
	for ( i = 0 ; i < WorldGraph.m_cNodes ; i++ )
	{
		int nodeNumber = (i + WorldGraph.m_iLastCoverSearch) % WorldGraph.m_cNodes;

		CNode &node = WorldGraph.Node( nodeNumber );
		WorldGraph.m_iLastCoverSearch = nodeNumber + 1; // next monster that searches for cover node will start where we left off here.

		// could use an optimization here!!
		flDist = ( pev->origin - node.m_vecOrigin ).Length();

		// DON'T do the trace check on a node that is farther away than a node that we've already found to 
		// provide cover! Also make sure the node is within the mins/maxs of the search.
		if ( flDist >= flMinDist && flDist < flMaxDist )
		{
			UTIL_TraceLine ( node.m_vecOrigin + vecViewOffset, vecLookersOffset, ignore_monsters, ignore_glass,  ENT(pev), &tr );

			// if this node will block the threat's line of sight to me...
			if ( tr.flFraction != 1.0 )
			{
				// ..and is also closer to me than the threat, or the same distance from myself and the threat the node is good.
				if ( ( iMyNode == iThreatNode ) || WorldGraph.PathLength( iMyNode, nodeNumber, iMyHullIndex, m_afCapability ) <= WorldGraph.PathLength( iThreatNode, nodeNumber, iMyHullIndex, m_afCapability ) )
				{
					if ( FValidateCover ( node.m_vecOrigin ) && MoveToLocation( ACT_RUN, 0, node.m_vecOrigin ) )
					{
						/*
						MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
							WRITE_BYTE( TE_SHOWLINE);
							
							WRITE_COORD( node.m_vecOrigin.x );
							WRITE_COORD( node.m_vecOrigin.y );
							WRITE_COORD( node.m_vecOrigin.z );

							WRITE_COORD( vecLookersOffset.x );
							WRITE_COORD( vecLookersOffset.y );
							WRITE_COORD( vecLookersOffset.z );
						MESSAGE_END();
						*/

						return TRUE;
					}
				}
			}
		}
	}
	return FALSE;
}


//=========================================================
// BuildNearestRoute - tries to build a route as close to the target
// as possible, even if there isn't a path to the final point.
//
// If supplied, search will return a node at least as far
// away as MinDist from vecThreat, but no farther than MaxDist. 
// if MaxDist isn't supplied, it defaults to a reasonable 
// value
//=========================================================
bool CBaseMonster :: BuildNearestRoute ( Vector vecThreat, Vector vecViewOffset, float flMinDist, float flMaxDist )
{
	int i;
	int iMyHullIndex;
	int iMyNode;
	float flDist;
	Vector	vecLookersOffset;
	TraceResult tr;

	if ( !flMaxDist )
	{
		// user didn't supply a MaxDist, so work up a crazy one.
		flMaxDist = 784;
	}

	if ( flMinDist > 0.5 * flMaxDist)
	{
#if _DEBUG
		ALERT ( at_console, "FindCover MinDist (%.0f) too close to MaxDist (%.0f)\n", flMinDist, flMaxDist );
#endif
		flMinDist = 0.5 * flMaxDist;
	}

	if ( !WorldGraph.m_fGraphPresent || !WorldGraph.m_fGraphPointersSet )
	{
		ALERT ( at_aiconsole, "Graph not ready for BuildNearestRoute!\n" );
		return FALSE;
	}

	iMyNode = WorldGraph.FindNearestNode( pev->origin, this );
	iMyHullIndex = WorldGraph.HullIndex( this );

	if ( iMyNode == NO_NODE )
	{
		ALERT ( at_aiconsole, "BuildNearestRoute() - %s has no nearest node!\n", STRING(pev->classname));
		return FALSE;
	}

	vecLookersOffset = vecThreat + vecViewOffset;// calculate location of enemy's eyes

	// we'll do a rough sample to find nodes that are relatively nearby
	for ( i = 0 ; i < WorldGraph.m_cNodes ; i++ )
	{
		int nodeNumber = (i + WorldGraph.m_iLastCoverSearch) % WorldGraph.m_cNodes;

		CNode &node = WorldGraph.Node( nodeNumber );
		WorldGraph.m_iLastCoverSearch = nodeNumber + 1; // next monster that searches for cover node will start where we left off here.

		// can I get there?
		if (WorldGraph.NextNodeInRoute( iMyNode, nodeNumber, iMyHullIndex, 0 ) != iMyNode)
		{
			flDist = ( vecThreat - node.m_vecOrigin ).Length();

			// is it close?
			if ( flDist > flMinDist && flDist < flMaxDist)
			{
				// can I see where I want to be from there?
				UTIL_TraceLine( node.m_vecOrigin + pev->view_ofs, vecLookersOffset, ignore_monsters, edict(), &tr );

				if (tr.flFraction == 1.0)
				{
					// try to actually get there
					if ( BuildRoute ( node.m_vecOrigin, bits_MF_TO_LOCATION, NULL ) )
					{
						flMaxDist = flDist;
						m_vecMoveGoal = node.m_vecOrigin;
						return TRUE; // UNDONE: keep looking for something closer!
					}
				}
			}
		}
	}

	return FALSE;
}

bool CBaseMonster::BuildRandomRoute( void )
{
	int i;
	int iMyHullIndex;
	int iMyNode;

	if ( !WorldGraph.m_fGraphPresent || !WorldGraph.m_fGraphPointersSet )
	{
		ALERT ( at_aiconsole, "Graph not ready for BuildRandomRoute!\n" );
		return false;
	}

	iMyNode = WorldGraph.FindNearestNode( pev->origin, this );
	iMyHullIndex = WorldGraph.HullIndex( this );

	if ( iMyNode == NO_NODE )
	{
		ALERT ( at_aiconsole, "BuildRandomRoute() - %s has no nearest node!\n", STRING( pev->classname ) );
		return false;
	}

	// we'll do a rough sample to find nodes that are relatively nearby
	for ( i = 0 ; i < WorldGraph.m_cNodes ; i++ )
	{
		int nodeNumber = (i + WorldGraph.m_iLastWanderSearch) % WorldGraph.m_cNodes;

		CNode &node = WorldGraph.Node( nodeNumber );
		WorldGraph.m_iLastWanderSearch = nodeNumber + 1; // next monster that searches for cover node will start where we left off here.

		// can I get there?
		if (WorldGraph.NextNodeInRoute( iMyNode, nodeNumber, iMyHullIndex, 0 ) != iMyNode)
		{
			if ( BuildRoute ( node.m_vecOrigin, bits_MF_TO_LOCATION, NULL ) )
			{
				m_vecMoveGoal = node.m_vecOrigin;
				return true; // UNDONE: keep looking for something closer!
			}
		}
	}

	return false;
}

//------------------------------------------------------------------------------
// Purpose : Remember that this entity wasn't reachable
// Input   :
// Output  :
//------------------------------------------------------------------------------
void CBaseMonster::RememberUnreachable(CBaseEntity *pEntity)
{
	if ( pEntity == GetEnemy() )
	{
		ForceChooseNewEnemy();
	}

	const float NPC_UNREACHABLE_TIMEOUT = 3;
	// Only add to list if not already on it
	for (int i=m_UnreachableEnts.Size()-1;i>=0;i--)
	{
		// If record already exists just update mark time
		if (pEntity == m_UnreachableEnts[i].hUnreachableEnt)
		{
			m_UnreachableEnts[i].fExpireTime	 = gpGlobals->time + NPC_UNREACHABLE_TIMEOUT;
			m_UnreachableEnts[i].vLocationWhenUnreachable = pEntity->pev->origin;
			return;
		}
	}

	// Add new unreachabe entity to list
	int nNewIndex = m_UnreachableEnts.AddToTail();
	m_UnreachableEnts[nNewIndex].hUnreachableEnt = pEntity;
	m_UnreachableEnts[nNewIndex].fExpireTime	 = gpGlobals->time + NPC_UNREACHABLE_TIMEOUT;
	m_UnreachableEnts[nNewIndex].vLocationWhenUnreachable = pEntity->pev->origin;
}

//------------------------------------------------------------------------------
// Purpose : Returns true is entity was remembered as unreachable.
//			 After a time delay reachability is checked
// Input   :
// Output  :
//------------------------------------------------------------------------------
bool CBaseMonster::IsUnreachable(CBaseEntity *pEntity)
{
	float UNREACHABLE_DIST_TOLERANCE_SQ = (120*120);

	// Note that it's ok to remove elements while I'm iterating
	// as long as I iterate backwards and remove them using FastRemove
	for (int i=m_UnreachableEnts.Size()-1;i>=0;i--)
	{
		// Remove any dead elements
		if (m_UnreachableEnts[i].hUnreachableEnt == NULL)
		{
			m_UnreachableEnts.FastRemove(i);
		}
		else if (pEntity == m_UnreachableEnts[i].hUnreachableEnt)
		{
			// Test for reachablility on any elements that have timed out
			if ( gpGlobals->time > m_UnreachableEnts[i].fExpireTime ||
				  pEntity->pev->origin.DistToSqr(m_UnreachableEnts[i].vLocationWhenUnreachable) > UNREACHABLE_DIST_TOLERANCE_SQ)
			{
				m_UnreachableEnts.FastRemove(i);
				return false;
			}
			return true;
		}
	}
	return false;
}

bool CBaseMonster::IsValidEnemy( CBaseEntity *pEnemy )
{
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Picks best enemy from my list of entities
//			Prefers reachable enemies over enemies that are unreachable,
//			regardless of priority.  For enemies that are both reachable or
//			unreachable picks by priority.  If priority is the same, picks
//			by distance.
// Input  :
// Output :
//-----------------------------------------------------------------------------
CBaseEntity *CBaseMonster::BestEnemy( void )
{
	// TODO - may want to consider distance, attack types, back turned, etc.

	CBaseEntity*	pBestEnemy			= NULL;
	int				iBestDistSq			= 4096 * 4096;// so first visible entity will become the closest.
	int				iBestPriority		= -1000;
	bool			bBestUnreachable	= true;			  // Forces initial check
	ThreeState_t	fBestSeen			= TRS_NONE;
	ThreeState_t	fBestVisible		= TRS_NONE;
	int				iDistSq;
	bool			bUnreachable		= false;

	AIEnemiesIter_t iter;
	for( AI_EnemyInfo_t *pEMemory = GetEnemies()->GetFirst(&iter); pEMemory != NULL; pEMemory = GetEnemies()->GetNext(&iter) )
	{
		CBaseEntity *pNextEnt = pEMemory->hEnemy;

		if ( !pNextEnt || !pNextEnt->IsAlive() )
			continue;
		
		// UNDONE: Move relationship checks into IsValidEnemy?
		if ( (pNextEnt->pev->flags & FL_NOTARGET) ||
			(IRelationType( pNextEnt ) != D_HT && IRelationType( pNextEnt ) != D_FR) ||
			!IsValidEnemy( pNextEnt ) )
		{
			continue;
		}

		if ( m_flAcceptableTimeSeenEnemy > 0.0 && pEMemory->flLastTimeSeen < m_flAcceptableTimeSeenEnemy )
			continue;

		// Skip enemies that have eluded me to prevent infinite loops
		if ( pEMemory->bEludedMe )
			continue;

		// establish the reachability of this enemy
		bUnreachable = IsUnreachable( pNextEnt );

		// If best is reachable and current is unreachable, skip the unreachable enemy regardless of priority
		if (!bBestUnreachable && bUnreachable)
			continue;

		//  If best is unreachable and current is reachable, always pick the current regardless of priority
		if (bBestUnreachable && !bUnreachable)
		{
			iBestPriority	 = IRelationPriority ( pNextEnt );
			iBestDistSq		 = (pNextEnt->pev->origin - pev->origin ).LengthSqr();
			pBestEnemy		 = pNextEnt;
			bBestUnreachable = bUnreachable;
			fBestSeen		 = TRS_NONE;
			fBestVisible	 = TRS_NONE;
		}
		// If both are unreachable or both are reachable, chose enemy based on priority and distance
		else if ( IRelationPriority( pNextEnt ) > iBestPriority )
		{
			// this entity is disliked MORE than the entity that we
			// currently think is the best visible enemy. No need to do
			// a distance check, just get mad at this one for now.
			iBestPriority	 = IRelationPriority ( pNextEnt );
			iBestDistSq		 = (pNextEnt->pev->origin - pev->origin ).LengthSqr();
			pBestEnemy		 = pNextEnt;
			bBestUnreachable = bUnreachable;
			fBestSeen		 = TRS_NONE;
			fBestVisible	 = TRS_NONE;
		}
		else if ( IRelationPriority( pNextEnt ) == iBestPriority )
		{
			// this entity is disliked just as much as the entity that
			// we currently think is the best visible enemy, so we only
			// get mad at it if it is closer.
			iDistSq	= ( pNextEnt->pev->origin - pev->origin ).LengthSqr();

			bool bAcceptCurrent = false;
			bool bCloser = ( iDistSq < iBestDistSq );
			ThreeState_t fCurSeen	 = TRS_NONE;
			ThreeState_t fCurVisible = TRS_NONE;

			// The following code is constructed in such a verbose manner to 
			// ensure the expensive calls only occur if absolutely needed

			// If current is farther, and best has previously been confirmed as seen or visible, move on
			if ( !bCloser )
			{
				if ( fBestSeen == TRS_TRUE || fBestVisible == TRS_TRUE )
					continue;
			}

			// If current is closer, and best has previously been confirmed as not seen and not visible, take it
			if ( bCloser )
			{
				if ( fBestSeen == TRS_FALSE && fBestVisible == TRS_FALSE )
				{
					bAcceptCurrent = true;
				}
			}

			if ( !bAcceptCurrent )
			{
				// If current is closer, and seen, take it
				if ( bCloser )
				{
					fCurSeen = ( GetSenses()->DidSeeEntity( pNextEnt ) ) ? TRS_TRUE : TRS_FALSE;

					bAcceptCurrent = ( fCurSeen == TRS_TRUE );
				}
			}

			if ( !bAcceptCurrent )
			{
				// If current is farther, and best is seen, move on
				if ( !bCloser )
				{
					if ( fBestSeen == TRS_NONE )
					{
						fBestSeen = ( GetSenses()->DidSeeEntity( pBestEnemy ) ) ? TRS_TRUE : TRS_FALSE;
					}

					if ( fBestSeen == TRS_TRUE )
						continue;
				}

				// At this point, need to start performing expensive tests
				if ( bCloser && fBestVisible == TRS_NONE )
				{
					// Perform shortest FVisible
					fCurVisible = ( ( EnemyDistance( pNextEnt ) < GetSenses()->GetDistLook() ) && FVisible( pNextEnt ) ) ? TRS_TRUE : TRS_FALSE;

					bAcceptCurrent = ( fCurVisible == TRS_TRUE );
				}

				// Alas, must do the most expensive comparison
				if ( !bAcceptCurrent )
				{
					if ( fBestSeen == TRS_NONE )
					{
						fBestSeen = ( GetSenses()->DidSeeEntity( pBestEnemy ) ) ? TRS_TRUE : TRS_FALSE;
					}

					if ( fBestVisible == TRS_NONE )
					{
						fBestVisible = ( ( EnemyDistance( pBestEnemy ) < GetSenses()->GetDistLook() ) && FVisible( pBestEnemy ) ) ? TRS_TRUE : TRS_FALSE;
					}

					if ( fCurSeen == TRS_NONE )
					{
						fCurSeen = ( GetSenses()->DidSeeEntity( pNextEnt ) ) ? TRS_TRUE : TRS_FALSE;
					}

					if ( fCurVisible == TRS_NONE )
					{
						fCurVisible = ( ( EnemyDistance( pNextEnt ) < GetSenses()->GetDistLook() ) && FVisible( pNextEnt ) ) ? TRS_TRUE : TRS_FALSE;
					}

					bool bBestSeenOrVisible = ( fBestSeen == TRS_TRUE || fBestVisible == TRS_TRUE );
					bool bCurSeenOrVisible = ( fCurSeen == TRS_TRUE || fCurVisible == TRS_TRUE );

					if ( !bCloser)
					{
						if ( bBestSeenOrVisible )
							continue;
						else if ( !bCurSeenOrVisible )
							continue;
					}
					else // Closer
					{
						if ( !bCurSeenOrVisible && bBestSeenOrVisible )
							continue;
					}
				}
			}

			fBestSeen		 = fCurSeen;
			fBestVisible	 = fCurVisible;
			iBestDistSq		 = iDistSq;
			iBestPriority	 = IRelationPriority ( pNextEnt );
			pBestEnemy		 = pNextEnt;
			bBestUnreachable = bUnreachable;
		}
	}

	return pBestEnemy;
}


//=========================================================
// MakeIdealYaw - gets a yaw value for the caller that would
// face the supplied vector. Value is stuffed into the monster's
// ideal_yaw
//=========================================================
void CBaseMonster :: MakeIdealYaw( Vector vecTarget )
{
	Vector	vecProjection;
	
	// strafing monster needs to face 90 degrees away from its goal
	if ( m_movementActivity == ACT_STRAFE_LEFT )
	{
		vecProjection.x = -vecTarget.y;
		vecProjection.y = vecTarget.x;

		pev->ideal_yaw = UTIL_VecToYaw( vecProjection - pev->origin );
	}
	else if ( m_movementActivity == ACT_STRAFE_RIGHT )
	{
		vecProjection.x = vecTarget.y;
		vecProjection.y = vecTarget.x;

		pev->ideal_yaw = UTIL_VecToYaw( vecProjection - pev->origin );
	}
	else
	{
		pev->ideal_yaw = UTIL_VecToYaw ( vecTarget - pev->origin );
	}
}

//=========================================================
// FlYawDiff - returns the difference ( in degrees ) between
// monster's current yaw and ideal_yaw
//
// Positive result is left turn, negative is right turn
//=========================================================
float CBaseMonster::FlYawDiff ( void )
{
	float	flCurrentYaw;

	flCurrentYaw = UTIL_AngleMod( pev->angles.y );

	if ( flCurrentYaw == pev->ideal_yaw )
	{
		return 0;
	}

	return UTIL_AngleDiff( pev->ideal_yaw, flCurrentYaw );
}

//-----------------------------------------------------------------------------

float AI_ClampYaw( float yawSpeedPerSec, float current, float target, float time )
{
	if (current != target)
	{
		float speed = yawSpeedPerSec * time;
		float move = target - current;

		if (target > current)
		{
			if (move >= 180)
				move = move - 360;
		}
		else
		{
			if (move <= -180)
				move = move + 360;
		}

		if (move > 0)
		{// turning to the monster's left
			if (move > speed)
				move = speed;
		}
		else
		{// turning to the monster's right
			if (move < -speed)
				move = -speed;
		}
		
		return UTIL_AngleMod(current + move);
	}
	
	return target;
}

//=========================================================
// Changeyaw - turns a monster towards its ideal_yaw
//=========================================================
void CBaseMonster::ChangeYaw( int yawSpeed )
{
	float ideal, current, newYaw;

	// NOTE: ideal_yaw will never exactly be reached because UTIL_AngleMod
	// also truncates the angle to 16 bits of resolution. So lets truncate it here.
	current = UTIL_AngleMod( pev->angles.y );
	ideal = UTIL_AngleMod( pev->ideal_yaw );

	// FIXME: this needs a proper interval
	float dt = V_min( 0.2, gpGlobals->frametime );

	newYaw = AI_ClampYaw( (float)yawSpeed * 10.0, current, ideal, dt );

	if ( newYaw != current )
	{
		pev->angles.y = newYaw;

		// turn head in desired direction only if they have a turnable head
		if (m_afCapability & bits_CAP_TURN_HEAD)
		{
			float yaw = pev->ideal_yaw - pev->angles.y;
			if (yaw > 180) yaw -= 360;
			if (yaw < -180) yaw += 360;
			// yaw *= 0.8;
			SetBoneController( 0, yaw );
		}
	}
}

bool CBaseMonster::OverrideMove( float flInterval )
{
	return false;
}

//=========================================================
// VecToYaw - turns a directional vector into a yaw value
// that points down that vector.
//=========================================================
float CBaseMonster::VecToYaw ( const Vector &vecDir )
{
	if (vecDir.x == 0 && vecDir.y == 0 && vecDir.z == 0)
		return pev->angles.y;

	return UTIL_VecToYaw( vecDir );
}


//=========================================================
// SetEyePosition
//
// queries the monster's model for $eyeposition and copies
// that vector to the monster's view_ofs
//
//=========================================================
void CBaseMonster :: SetEyePosition ( void )
{
	Vector  vecEyePosition;

	GetEyePosition( GetModelPtr(), vecEyePosition );

	pev->view_ofs = vecEyePosition;

	if ( pev->view_ofs == vec3_origin )
	{
		ALERT ( at_aiconsole, "%s has no view_ofs!\n", STRING ( pev->classname ) );
	}
}

void CBaseMonster :: HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case SCRIPT_EVENT_DEAD:
		if ( m_MonsterState == MONSTERSTATE_SCRIPT )
		{
			pev->deadflag = DEAD_DYING;
			// Kill me now! (and fade out when CineCleanup() is called)
#if _DEBUG
			ALERT( at_aiconsole, "Death event: %s\n", STRING(pev->classname) );
#endif
			pev->health = 0;
		}
#if _DEBUG
		else
			ALERT( at_aiconsole, "INVALID death event:%s\n", STRING(pev->classname) );
#endif
		break;
	case SCRIPT_EVENT_NOT_DEAD:
		if ( m_MonsterState == MONSTERSTATE_SCRIPT )
		{
			pev->deadflag = DEAD_NO;
			// This is for life/death sequences where the player can determine whether a character is dead or alive after the script 
			pev->health = pev->max_health;
		}
		break;

	case SCRIPT_EVENT_SOUND:			// Play a named wave file
		EMIT_SOUND( edict(), CHAN_BODY, pEvent->options, 1.0, ATTN_IDLE );
		break;

	case SCRIPT_EVENT_SOUND_VOICE:
		EMIT_SOUND( edict(), CHAN_VOICE, pEvent->options, 1.0, ATTN_IDLE );
		break;

	case SCRIPT_EVENT_SENTENCE_RND1:		// Play a named sentence group 33% of the time
		if (RANDOM_LONG(0,2) == 0)
			break;
		// fall through...
	case SCRIPT_EVENT_SENTENCE:			// Play a named sentence group
		SENTENCEG_PlayRndSz( edict(), pEvent->options, 1.0, ATTN_IDLE, 0, 100 );
		break;

	case SCRIPT_EVENT_FIREEVENT:		// Fire a trigger
		FireTargets( pEvent->options, this, this, USE_TOGGLE, 0 );
		break;

	case SCRIPT_EVENT_NOINTERRUPT:		// Can't be interrupted from now on
		if ( m_pCine )
			m_pCine->AllowInterrupt( FALSE );
		break;

	case SCRIPT_EVENT_CANINTERRUPT:		// OK to interrupt now
		if ( m_pCine )
			m_pCine->AllowInterrupt( TRUE );
		break;

#if 0
	case SCRIPT_EVENT_INAIR:			// Don't DROP_TO_FLOOR()
	case SCRIPT_EVENT_ENDANIMATION:		// Set ending animation sequence to
		break;
#endif

	case MONSTER_EVENT_BODYDROP_HEAVY:
		if ( pev->flags & FL_ONGROUND )
		{
			if ( RANDOM_LONG( 0, 1 ) == 0 )
			{
				EMIT_SOUND_DYN( ENT(pev), CHAN_BODY, "common/bodydrop3.wav", 1, ATTN_NORM, 0, 90 );
			}
			else
			{
				EMIT_SOUND_DYN( ENT(pev), CHAN_BODY, "common/bodydrop4.wav", 1, ATTN_NORM, 0, 90 );
			}
		}
		break;

	case MONSTER_EVENT_BODYDROP_LIGHT:
		if ( pev->flags & FL_ONGROUND )
		{
			if ( RANDOM_LONG( 0, 1 ) == 0 )
			{
				EMIT_SOUND( ENT(pev), CHAN_BODY, "common/bodydrop3.wav", 1, ATTN_NORM );
			}
			else
			{
				EMIT_SOUND( ENT(pev), CHAN_BODY, "common/bodydrop4.wav", 1, ATTN_NORM );
			}
		}
		break;

	case MONSTER_EVENT_SWISHSOUND:
		{
			// NO MONSTER may use this anim event unless that monster's precache precaches this sound!!!
			EMIT_SOUND( ENT(pev), CHAN_BODY, "zombie/claw_miss2.wav", 1, ATTN_NORM );
			break;
		}

	default:
		ALERT( at_aiconsole, "Unhandled animation event %d for %s\n", pEvent->event, STRING(pev->classname) );
		break;

	}
}

//=========================================================
// NODE GRAPH
//=========================================================

//=========================================================
// FGetNodeRoute - tries to build an entire node path from
// the callers origin to the passed vector. If this is 
// possible, ROUTE_SIZE waypoints will be copied into the
// callers m_Route. TRUE is returned if the operation 
// succeeds (path is valid) or FALSE if failed (no path 
// exists )
//=========================================================
bool CBaseMonster :: FGetNodeRoute ( Vector vecDest )
{
	int iPath[ MAX_PATH_SIZE ];
	int iSrcNode, iDestNode;
	int iResult;
	int i;
	int iNumToCopy;

	iSrcNode = WorldGraph.FindNearestNode ( pev->origin, this );
	iDestNode = WorldGraph.FindNearestNode ( vecDest, this );

	if ( iSrcNode == -1 )
	{
		// no node nearest self
//		ALERT ( at_aiconsole, "FGetNodeRoute: No valid node near self!\n" );
		return FALSE;
	}
	else if ( iDestNode == -1 )
	{
		// no node nearest target
//		ALERT ( at_aiconsole, "FGetNodeRoute: No valid node near target!\n" );
		return FALSE;
	}

	// valid src and dest nodes were found, so it's safe to proceed with
	// find shortest path
	int iNodeHull = WorldGraph.HullIndex( this ); // make this a monster virtual function
	iResult = WorldGraph.FindShortestPath ( iPath, iSrcNode, iDestNode, iNodeHull, m_afCapability );

	if ( !iResult )
	{
#if 1
		ALERT ( at_aiconsole, "No Path from %d to %d!\n", iSrcNode, iDestNode );
		return FALSE;
#else
		bool bRoutingSave = WorldGraph.m_fRoutingComplete;
		WorldGraph.m_fRoutingComplete = FALSE;
		iResult = WorldGraph.FindShortestPath(iPath, iSrcNode, iDestNode, iNodeHull, m_afCapability);
		WorldGraph.m_fRoutingComplete = bRoutingSave;
		if ( !iResult )
		{
			ALERT ( at_aiconsole, "No Path from %d to %d!\n", iSrcNode, iDestNode );
			return FALSE;
		}
		else
		{
			ALERT ( at_aiconsole, "Routing is inconsistent!" );
		}
#endif
	}

	// there's a valid path within iPath now, so now we will fill the route array
	// up with as many of the waypoints as it will hold.
	
	// don't copy ROUTE_SIZE entries if the path returned is shorter
	// than ROUTE_SIZE!!!
	if ( iResult < ROUTE_SIZE )
	{
		iNumToCopy = iResult;
	}
	else
	{
		iNumToCopy = ROUTE_SIZE;
	}
	
	for ( i = 0 ; i < iNumToCopy; i++ )
	{
		m_Route[ i ].vecLocation = WorldGraph.m_pNodes[ iPath[ i ] ].m_vecOrigin;
		m_Route[ i ].iType = bits_MF_TO_NODE;
	}
	
	if ( iNumToCopy < ROUTE_SIZE )
	{
		m_Route[ iNumToCopy ].vecLocation = vecDest;
		m_Route[ iNumToCopy ].iType |= bits_MF_IS_GOAL;
	}

	return TRUE;
}

//=========================================================
// FindHintNode
//=========================================================
int CBaseMonster :: FindHintNode ( void )
{
	int i;
	TraceResult tr;

	if ( !WorldGraph.m_fGraphPresent )
	{
		ALERT ( at_aiconsole, "find_hintnode: graph not ready!\n" );
		return NO_NODE;
	}

	if ( WorldGraph.m_iLastActiveIdleSearch >= WorldGraph.m_cNodes )
	{
		WorldGraph.m_iLastActiveIdleSearch = 0;
	}

	for ( i = 0; i < WorldGraph.m_cNodes ; i++ )
	{
		int nodeNumber = (i + WorldGraph.m_iLastActiveIdleSearch) % WorldGraph.m_cNodes;
		CNode &node = WorldGraph.Node( nodeNumber );

		if ( node.m_sHintType )
		{
			// this node has a hint. Take it if it is visible, the monster likes it, and the monster has an animation to match the hint's activity.
			if ( FValidateHintType ( node.m_sHintType ) )
			{
				if ( !node.m_sHintActivity || LookupActivity ( node.m_sHintActivity ) != ACTIVITY_NOT_AVAILABLE )
				{
					UTIL_TraceLine ( pev->origin + pev->view_ofs, node.m_vecOrigin + pev->view_ofs, ignore_monsters, ENT(pev), &tr );

					if ( tr.flFraction == 1.0 )
					{
						WorldGraph.m_iLastActiveIdleSearch = nodeNumber + 1; // next monster that searches for hint nodes will start where we left off.
						return nodeNumber;// take it!
					}
				}
			}
		}
	}

	WorldGraph.m_iLastActiveIdleSearch = 0;// start at the top of the list for the next search.

	return NO_NODE;
}
			
//-----------------------------------------------------------------------------
// Purpose: Displays information in the console about the state of this monster.
//-----------------------------------------------------------------------------
void CBaseMonster::ReportAIState( void )
{
	ALERT_TYPE level = at_console;

	static const char *pStateNames[] = { "None", "Idle", "Combat", "Alert", "Hunt", "Prone", "Scripted", "PlayDead", "Dead" };

	ALERT( level, "%s: ", STRING(pev->classname) );
	if ( (int)m_MonsterState < ARRAYSIZE(pStateNames) )
		ALERT( level, "State: %s, ", pStateNames[m_MonsterState] );
	int i = 0;
	while ( activity_map[i].type != 0 )
	{
		if ( activity_map[i].type == (int)m_Activity )
		{
			ALERT( level, "Activity %s, ", activity_map[i].name );
			break;
		}
		i++;
	}

	if ( GetCurSchedule() )
	{
		const char *pName = NULL;
		pName = GetCurSchedule()->GetName();
		if ( !pName )
			pName = "Unknown";
		ALERT( level, "Schedule %s, ", pName );
		const Task_t *pTask = GetTask();
		if ( pTask )
			ALERT( level, "Task %d (#%d), ", pTask->iTask, GetScheduleCurTaskIndex() );
	}
	else
		ALERT( level, "No Schedule, " );

	if ( m_hEnemy != NULL )
		ALERT( level, "\nEnemy is %s", STRING(m_hEnemy->pev->classname) );
	else
		ALERT( level, "No enemy" );

	if ( IsMoving() )
	{
		ALERT( level, " Moving " );
		if ( m_flMoveWaitFinished > gpGlobals->time )
			ALERT( level, ": Stopped for %.2f. ", m_flMoveWaitFinished - gpGlobals->time );
		else if ( m_IdealActivity == GetStoppedActivity() )
			ALERT( level, ": In stopped anim. " );
	}

	if ( !m_pSquad )
	{
		ALERT ( level, "not " );
	}

	ALERT ( level, "In Squad, " );

	if ( m_pSquad && !m_pSquad->IsLeader( this ) )
	{
		ALERT ( level, "not " );
	}

	ALERT ( level, "Leader." );

	ALERT( level, "\n" );
	ALERT( level, "Yaw speed:%3.1f,Health: %3.1f\n", pev->yaw_speed, pev->health );
	if ( pev->spawnflags & SF_MONSTER_PRISONER )
		ALERT( level, " PRISONER! " );
	if ( pev->spawnflags & SF_MONSTER_PREDISASTER )
		ALERT( level, " Pre-Disaster! " );
	ALERT( level, "\n" );

	if ( pev->groundentity )
	{
		ALERT( level, "Groundent:%s\n\n", STRING( pev->groundentity->v.classname ) );
	}
	else
	{
		ALERT( level, "Groundent: NULL\n\n" );
	}
}

//=========================================================
// KeyValue
//
// !!! netname entvar field is used in squadmonster for groupname!!!
//=========================================================
void CBaseMonster :: KeyValue( KeyValueData *pkvd )
{
	if (FStrEq(pkvd->szKeyName, "TriggerTarget"))
	{
		m_iszTriggerTarget = ALLOC_STRING( pkvd->szValue );
		pkvd->fHandled = true;
	}
	else if (FStrEq(pkvd->szKeyName, "TriggerCondition") )
	{
		m_iTriggerCondition = atoi( pkvd->szValue );
		pkvd->fHandled = true;
	}
	else
	{
		BaseClass::KeyValue( pkvd );
	}
}

//=========================================================
// FCheckAITrigger - checks the monster's AI Trigger Conditions,
// if there is a condition, then checks to see if condition is 
// met. If yes, the monster's TriggerTarget is fired.
//
// Returns true if the target is fired.
//=========================================================
bool CBaseMonster::FCheckAITrigger ( void )
{
	bool fFireTarget;

	if ( m_iTriggerCondition == AITRIGGER_NONE )
	{
		// no conditions, so this trigger is never fired.
		return false; 
	}

	fFireTarget = false;

	switch ( m_iTriggerCondition )
	{
	case AITRIGGER_SEEPLAYER_ANGRY_AT_PLAYER:
		if ( m_hEnemy != NULL && m_hEnemy->IsPlayer() && HasCondition( COND_SEE_ENEMY ) )
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_SEEPLAYER_UNCONDITIONAL:
		if ( HasCondition ( COND_SEE_PLAYER ) )
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_SEEPLAYER_NOT_IN_COMBAT:
		if ( HasCondition( COND_SEE_PLAYER ) && 
			 m_MonsterState != MONSTERSTATE_COMBAT	&& 
			 m_MonsterState != MONSTERSTATE_PRONE	&& 
			 m_MonsterState != MONSTERSTATE_SCRIPT)
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_TAKEDAMAGE:
		if ( HasCondition( COND_LIGHT_DAMAGE ) || HasCondition( COND_HEAVY_DAMAGE ) )
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_DEATH:
		if ( pev->deadflag != DEAD_NO )
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_HALFHEALTH:
		if ( IsAlive() && pev->health <= ( pev->max_health / 2 ) )
		{
			fFireTarget = true;
		}
		break;
/*

  // !!!UNDONE - no persistant game state that allows us to track these two. 

	case AITRIGGER_SQUADMEMBERDIE:
		break;
	case AITRIGGER_SQUADLEADERDIE:
		break;
*/
	case AITRIGGER_HEARWORLD:
		if ( HasCondition( COND_HEAR_WORLD ) )
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_HEARPLAYER:
		if ( HasCondition( COND_HEAR_PLAYER ) )
		{
			fFireTarget = true;
		}
		break;
	case AITRIGGER_HEARCOMBAT:
		if ( HasCondition( COND_HEAR_COMBAT ) )
		{
			fFireTarget = true;
		}
		break;
	}

	if ( fFireTarget )
	{
		// fire the target, then set the trigger conditions to NONE so we don't fire again
		ALERT ( at_aiconsole, "AI Trigger Fire Target\n" );
		FireTargets( STRING( m_iszTriggerTarget ), this, this, USE_TOGGLE, 0 );
		m_iTriggerCondition = AITRIGGER_NONE;
		return true;
	}

	return false;
}

//=========================================================	
// CanPlaySequence - determines whether or not the monster
// can play the scripted sequence or AI sequence that is 
// trying to possess it. If DisregardState is set, the monster
// will be sucked into the script no matter what state it is
// in. ONLY Scripted AI ents should allow this.
//=========================================================	
int CBaseMonster :: CanPlaySequence( bool fDisregardMonsterState, int interruptLevel )
{
	if ( m_pCine || !IsAlive() || m_MonsterState == MONSTERSTATE_PRONE )
	{
		// monster is already running a scripted sequence or dead!
		return FALSE;
	}
	
	if ( fDisregardMonsterState )
	{
		// ok to go, no matter what the monster state. (scripted AI)
		return TRUE;
	}

	if ( m_MonsterState == MONSTERSTATE_NONE || m_MonsterState == MONSTERSTATE_IDLE || m_IdealMonsterState == MONSTERSTATE_IDLE )
	{
		// ok to go, but only in these states
		return TRUE;
	}
	
	if ( m_MonsterState == MONSTERSTATE_ALERT && interruptLevel >= SS_INTERRUPT_BY_NAME )
		return TRUE;

	// unknown situation
	return FALSE;
}

//-------------------------------------
// Checks lateral cover
//-------------------------------------
bool CBaseMonster::TestLateralCover( const Vector &vecCheckStart, const Vector &vecCheckEnd )
{
	TraceResult	tr;

	// it's faster to check the SightEnt's visibility to the potential spot than to check the local move, so we do that first.
	UTIL_TraceLine( vecCheckStart, vecCheckEnd + pev->view_ofs, ignore_monsters, ignore_glass, edict(), &tr );

	if ( tr.flFraction != 1.0 )
	{
		if ( FValidateCover ( vecCheckEnd ) && CheckLocalMove( pev->origin, vecCheckEnd, NULL, NULL ) == LOCALMOVE_VALID )
		{
			if ( MoveToLocation( ACT_RUN, 0,  vecCheckEnd ) )
			{
				return true;
			}
		}
	}

	return false;
}

//=========================================================
// FindLateralCover - attempts to locate a spot in the world
// directly to the left or right of the caller that will
// conceal them from view of pSightEnt
//=========================================================
#define	COVER_CHECKS	5// how many checks are made
#define COVER_DELTA		48// distance between checks

bool CBaseMonster::FindLateralCover( const Vector &vecThreat )
{
	Vector	vecLeftTest;
	Vector	vecRightTest;
	Vector	vecStepRight;
	Vector  vecCheckStart;
	int		i;

	if ( TestLateralCover( vecThreat, pev->origin ) )
	{
		return true;
	}

	Vector right;
	AngleVectors( pev->angles, NULL, &right, NULL );
	vecStepRight = right * COVER_DELTA;
	vecStepRight.z = 0; 
	
	vecLeftTest = vecRightTest = pev->origin;
	vecCheckStart = vecThreat;

	for ( i = 0 ; i < COVER_CHECKS ; i++ )
	{
		vecLeftTest = vecLeftTest - vecStepRight;
		vecRightTest = vecRightTest + vecStepRight;

		if ( TestLateralCover( vecCheckStart, vecLeftTest ) )
		{
			return true;
		}

		if ( TestLateralCover( vecCheckStart, vecRightTest ) )
		{
			return true;
		}
	}

	return false;
}

Vector CBaseMonster::GetShootEnemyDir( const Vector &shootOrigin )
{
	CBaseEntity *pEnemy = GetEnemy();

	if ( pEnemy )
	{
		Vector vecEnemyLKP = GetEnemyLKP();

		Vector retval = (pEnemy->BodyTarget( shootOrigin ) - pEnemy->pev->origin) + vecEnemyLKP - shootOrigin;
		VectorNormalize( retval );
		return retval;
	}
	else
	{
		Vector forward;
		AngleVectors( pev->angles, &forward );
		return forward;
	}
}

//-----------------------------------------------------------------------------

Vector CBaseMonster::BodyTarget( const Vector &posSrc, bool bNoisy )
{ 
	Vector low = Center() - ( Center() - pev->origin ) * .25;
	Vector high = EyePosition();
	Vector delta = high - low;
	Vector result;
	if ( bNoisy )
	{
		// bell curve
		float rand1 = RANDOM_FLOAT( 0.0, 0.5 );
		float rand2 = RANDOM_FLOAT( 0.0, 0.5 );
		result = low + delta * rand1 + delta * rand2;
	}
	else
		result = low + delta * 0.5; 

	return result;
}

//-----------------------------------------------------------------------------

//=========================================================
// FacingIdeal - tells us if a monster is facing its ideal
// yaw. Created this function because many spots in the 
// code were checking the yawdiff against this magic
// number. Nicer to have it in one place if we're gonna
// be stuck with it.
//=========================================================
bool CBaseMonster :: FacingIdeal( void )
{
	if ( fabs( FlYawDiff() ) <= 0.006 )//!!!BUGBUG - no magic numbers!!!
	{
		return true;
	}

	return false;
}

//=========================================================
// FCanActiveIdle
//=========================================================
bool CBaseMonster::FCanActiveIdle( void )
{
	/*
	if ( m_MonsterState == MONSTERSTATE_IDLE && m_IdealMonsterState == MONSTERSTATE_IDLE && !IsMoving() )
	{
		return TRUE;
	}
	*/
	return false;
}


void CBaseMonster::PlaySentence( const char *pszSentence, float duration, float volume, float attenuation )
{
	if ( pszSentence && IsAlive() )
	{
		if ( pszSentence[0] == '!' )
			EMIT_SOUND_DYN( edict(), CHAN_VOICE, pszSentence, volume, attenuation, 0, PITCH_NORM );
		else
			SENTENCEG_PlayRndSz( edict(), pszSentence, volume, attenuation, 0, PITCH_NORM );
	}
}


void CBaseMonster::PlayScriptedSentence( const char *pszSentence, float duration, float volume, float attenuation, bool bConcurrent, CBaseEntity *pListener )
{ 
	PlaySentence( pszSentence, duration, volume, attenuation );
}


void CBaseMonster::SentenceStop( void )
{
	EMIT_SOUND( edict(), CHAN_VOICE, "common/null.wav", VOL_NORM, ATTN_IDLE );
}


void CBaseMonster::CorpseFallThink( void )
{
	if ( pev->flags & FL_ONGROUND )
	{
		SetThink ( NULL );

		SetSequenceBox( );
		UTIL_SetOrigin( pev, pev->origin );// link into world.
	}
	else
		pev->nextthink = gpGlobals->time + 0.1;
}

// Call after animation/pose is set up
void CBaseMonster :: MonsterInitDead( void )
{
	InitBoneControllers();

	pev->solid			= SOLID_BBOX;
	pev->movetype		= MOVETYPE_TOSS;// so he'll fall to ground

	pev->frame = 0;
	ResetSequenceInfo( );
	pev->framerate = 0;
	
	// Copy health
	pev->max_health		= pev->health;
	pev->deadflag		= DEAD_DEAD;
	
	UTIL_SetSize(pev, vec3_origin, vec3_origin );
	UTIL_SetOrigin( pev, pev->origin );

	// Setup health counters, etc.
	BecomeDead();
	SetThink( &CBaseMonster::CorpseFallThink );
	pev->nextthink = gpGlobals->time + 0.5;
}

//=========================================================
// BBoxIsFlat - check to see if the monster's bounding box
// is lying flat on a surface (traces from all four corners
// are same length.)
//=========================================================
bool CBaseMonster :: BBoxFlat ( void )
{
	TraceResult	tr;
	Vector		vecPoint;
	float		flXSize, flYSize;
	float		flLength;
	float		flLength2;

	flXSize = pev->size.x / 2;
	flYSize = pev->size.y / 2;

	vecPoint.x = pev->origin.x + flXSize;
	vecPoint.y = pev->origin.y + flYSize;
	vecPoint.z = pev->origin.z;

	UTIL_TraceLine ( vecPoint, vecPoint - Vector ( 0, 0, 100 ), ignore_monsters, ENT(pev), &tr );
	flLength = (vecPoint - tr.vecEndPos).Length();

	vecPoint.x = pev->origin.x - flXSize;
	vecPoint.y = pev->origin.y - flYSize;

	UTIL_TraceLine ( vecPoint, vecPoint - Vector ( 0, 0, 100 ), ignore_monsters, ENT(pev), &tr );
	flLength2 = (vecPoint - tr.vecEndPos).Length();
	if ( flLength2 > flLength )
	{
		return false;
	}
	flLength = flLength2;

	vecPoint.x = pev->origin.x - flXSize;
	vecPoint.y = pev->origin.y + flYSize;
	UTIL_TraceLine ( vecPoint, vecPoint - Vector ( 0, 0, 100 ), ignore_monsters, ENT(pev), &tr );
	flLength2 = (vecPoint - tr.vecEndPos).Length();
	if ( flLength2 > flLength )
	{
		return false;
	}
	flLength = flLength2;

	vecPoint.x = pev->origin.x + flXSize;
	vecPoint.y = pev->origin.y - flYSize;
	UTIL_TraceLine ( vecPoint, vecPoint - Vector ( 0, 0, 100 ), ignore_monsters, ENT(pev), &tr );
	flLength2 = (vecPoint - tr.vecEndPos).Length();
	if ( flLength2 > flLength )
	{
		return false;
	}
	flLength = flLength2;

	return true;
}

void CBaseMonster::SetEnemy( CBaseEntity *pEnemy, bool bSetCondNewEnemy )
{
	if ( m_hEnemy != pEnemy)
	{
		ClearAttackConditions( );
		VacateStrategySlot();
		m_GiveUpOnDeadEnemyTimer.Stop();

		// If we've just found a new enemy, set the condition
		if ( pEnemy && bSetCondNewEnemy )
		{
			SetCondition( COND_NEW_ENEMY );
		}
	}

	// Assert( (pEnemy == NULL) || (m_MonsterState == MONSTERSTATE_COMBAT) );

	m_hEnemy = pEnemy;

	if ( !pEnemy )
		ClearCondition( COND_NEW_ENEMY );
}

const Vector &CBaseMonster::GetEnemyLKP() const
{
	return (const_cast<CBaseMonster *>(this))->GetEnemies()->LastKnownPosition( GetEnemy() );
}

float CBaseMonster::GetEnemyLastTimeSeen() const
{
	return (const_cast<CBaseMonster *>(this))->GetEnemies()->LastTimeSeen( GetEnemy() );
}

void CBaseMonster::MarkEnemyAsEluded()
{
	GetEnemies()->MarkAsEluded( GetEnemy() );
}

void CBaseMonster::ClearEnemyMemory()
{
	GetEnemies()->ClearMemory( GetEnemy());
}

bool CBaseMonster::EnemyHasEludedMe() const
{
	return (const_cast<CBaseMonster *>(this))->GetEnemies()->HasEludedMe( GetEnemy() );
}

bool CBaseMonster::ShouldChooseNewEnemy()
{
	CBaseEntity *pEnemy = GetEnemy();
	if ( pEnemy )
	{
		if ( GetEnemies()->GetSerialNumber() != m_EnemiesSerialNumber )
		{
			return true;
		}

		m_EnemiesSerialNumber = GetEnemies()->GetSerialNumber();

		if ( EnemyHasEludedMe() || (IRelationType( pEnemy ) != D_HT && IRelationType( pEnemy ) != D_FR) || !IsValidEnemy( pEnemy ) )
			return true;

		if ( HasCondition(COND_SEE_HATE) || HasCondition(COND_SEE_DISLIKE) || HasCondition(COND_SEE_NEMESIS) )
			return true;

		if ( !pEnemy->IsAlive() )
		{
			if ( m_GiveUpOnDeadEnemyTimer.IsRunning() )
			{
				if ( m_GiveUpOnDeadEnemyTimer.Expired() )
					return true;
			}
			else
				m_GiveUpOnDeadEnemyTimer.Start();
		}

		if( GetEnemies()->HasMemory( pEnemy ) )
			return false;
	}

	m_EnemiesSerialNumber = GetEnemies()->GetSerialNumber();

	return true;
}

//=========================================================
// Choose Enemy - tries to find the best suitable enemy for the monster.
//=========================================================
bool CBaseMonster::ChooseEnemy( void )
{
	//---------------------------------
	//
	// Gather initial conditions
	//

	CBaseEntity *pInitialEnemy = GetEnemy();
	CBaseEntity *pChosenEnemy  = pInitialEnemy;

	// Use memory bits in case enemy pointer altered outside this function, (e.g., ehandle goes NULL)
	bool fHadEnemy  	 = ( HasMemory( bits_MEMORY_HAD_ENEMY | bits_MEMORY_HAD_PLAYER ) );
	bool fEnemyWasPlayer = HasMemory( bits_MEMORY_HAD_PLAYER );
	bool fEnemyWentNull  = ( fHadEnemy && !pInitialEnemy );
	bool fEnemyEluded	 = ( fEnemyWentNull || ( pInitialEnemy && GetEnemies()->HasEludedMe( pInitialEnemy ) ) );

	//---------------------------------
	//
	// Establish suitability of choosing a new enemy
	//

	bool fHaveCondNewEnemy;
	bool fHaveCondLostEnemy;

	if ( !m_ScheduleState.bScheduleWasInterrupted && GetCurSchedule() && !FScheduleDone() )
	{
		Assert( InterruptFromCondition( COND_NEW_ENEMY ) == COND_NEW_ENEMY && InterruptFromCondition( COND_LOST_ENEMY ) == COND_LOST_ENEMY );
		fHaveCondNewEnemy  = GetCurSchedule()->HasInterrupt( COND_NEW_ENEMY );
		fHaveCondLostEnemy = GetCurSchedule()->HasInterrupt( COND_LOST_ENEMY );

		// See if they've been added as a custom interrupt
		if ( !fHaveCondNewEnemy )
		{
			fHaveCondNewEnemy = IsCustomInterruptConditionSet( COND_NEW_ENEMY );
		}
		if ( !fHaveCondLostEnemy )
		{
			fHaveCondLostEnemy = IsCustomInterruptConditionSet( COND_LOST_ENEMY );
		}
	}
	else
	{
		fHaveCondNewEnemy  = true; // not having a schedule is the same as being interruptable by any condition
		fHaveCondLostEnemy = true;
	}

	if ( !fEnemyWentNull )
	{
		if ( !fHaveCondNewEnemy && !( fHaveCondLostEnemy && fEnemyEluded ) )
		{
			// DO NOT mess with the monster's enemy pointer unless the schedule the monster is currently
			// running will be interrupted by COND_NEW_ENEMY or COND_LOST_ENEMY. This will
			// eliminate the problem of monsters getting a new enemy while they are in a schedule
			// that doesn't care, and then not realizing it by the time they get to a schedule
			// that does. I don't feel this is a good permanent fix.
			m_bSkippedChooseEnemy = true;

			return ( pChosenEnemy != NULL );
		}
	}
	else if ( !fHaveCondNewEnemy && !fHaveCondLostEnemy && GetCurSchedule() )
	{
		ALERT( at_aiconsole, "WARNING: AI enemy went NULL but schedule (%s) is not interested\n", GetCurSchedule()->GetName() );
	}

	m_bSkippedChooseEnemy = false;

	//---------------------------------
	//
	// Select a target
	//

	if ( ShouldChooseNewEnemy()	)
	{
		pChosenEnemy = BestEnemy();
	}

	//---------------------------------
	//
	// React to result of selection
	//

	bool fChangingEnemy = ( pChosenEnemy != pInitialEnemy );

	if ( fChangingEnemy || fEnemyWentNull )
	{
		Forget( bits_MEMORY_HAD_ENEMY | bits_MEMORY_HAD_PLAYER );

		// Did our old enemy snuff it?
		if ( pInitialEnemy && !pInitialEnemy->IsAlive() )
		{
			SetCondition( COND_ENEMY_DEAD );
		}

		SetEnemy( pChosenEnemy );

		if ( fHadEnemy )
		{
			// Vacate any strategy slot on old enemy
			VacateStrategySlot();
		}

		if ( !pChosenEnemy )
		{
			if ( fEnemyEluded )
			{
				SetCondition( COND_LOST_ENEMY );
				LostEnemySound();
			}
		}
		else
		{
			Remember( ( pChosenEnemy->IsPlayer() ) ? bits_MEMORY_HAD_PLAYER : bits_MEMORY_HAD_ENEMY );
		}
	}

	//---------------------------------

	return ( pChosenEnemy != NULL );// monster has an enemy.
}


//=========================================================
// DropItem - dead monster drops named item 
//=========================================================
CBaseEntity *CBaseMonster::DropItem ( const char *pszItemName, const Vector &vecPos, const Vector &vecAng )
{
	if ( !pszItemName )
	{
		ALERT ( at_console, "DropItem() - No item name!\n" );
		return NULL;
	}

	CBaseEntity *pItem = CBaseEntity::Create( pszItemName, vecPos, vecAng, edict() );

	if ( pItem )
	{
		// do we want this behavior to be default?! (sjb)
		pItem->pev->velocity = pev->velocity;
		pItem->pev->avelocity = Vector ( 0, RANDOM_FLOAT( 0, 100 ), 0 );
		return pItem;
	}
	else
	{
		ALERT ( at_console, "DropItem() - Didn't create!\n" );
		return NULL;
	}

}


bool CBaseMonster::ShouldFadeOnDeath( void )
{
	// if flagged to fade out or I have an owner (I came from a monster spawner)
	if ( (pev->spawnflags & SF_MONSTER_FADECORPSE) || !FNullEnt( pev->owner ) )
		return true;

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: Indicates whether or not this monster should play an idle sound now.
//
//
// Output : Returns true if yes, false if no.
//-----------------------------------------------------------------------------
bool CBaseMonster::ShouldPlayIdleSound( void )
{
	// IDLE sound permitted in ALERT state is because monsters were silent in ALERT state. Only play IDLE sound in IDLE state
	// once we have sounds for that state.
	if ( ( m_MonsterState == MONSTERSTATE_IDLE || m_MonsterState == MONSTERSTATE_ALERT ) && 
		RANDOM_LONG(0,99) == 0 && !HasSpawnFlags(SF_MONSTER_GAG) )
	{
		return true;
	}
		
	return false;
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  :
// Output :
//-----------------------------------------------------------------------------
bool CBaseMonster::FOkToMakeSound( void )
{
	// Am I making uninterruptable sound
	if (gpGlobals->time <= m_flSoundWaitTime)
	{
		return false;
	}

	// Check other's in squad to see if they making uninterrupable sounds
	if (m_pSquad)
	{
		if (gpGlobals->time <= m_pSquad->GetSquadSoundWaitTime())
		{
			return false;
		}
	}

	if ( HasSpawnFlags( SF_MONSTER_GAG ) )
	{
		if ( m_MonsterState != MONSTERSTATE_COMBAT )
		{
			// no talking outside of combat if gagged.
			return false;
		}
	}
	
	return true;
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CBaseMonster::JustMadeSound( void )
{
	m_flSoundWaitTime = gpGlobals->time + RANDOM_FLOAT(1.5, 2.0);

	if (m_pSquad)
	{
		m_pSquad->SetSquadSoundWaitTime( gpGlobals->time + RANDOM_FLOAT(1.5, 2.0) );
	}
}

//=========================================================
//=========================================================
void CBaseMonster::OnScheduleChange( void )
{
	m_flMoveWaitFinished = 0;

	VacateStrategySlot();

	// If I still have have a route, clear it
	RouteClear();
}

CBaseCombatCharacter* CBaseMonster::GetEnemyCombatCharacterPointer()
{
	if ( GetEnemy() == NULL )
		return NULL;

	return GetEnemy()->MyCombatCharacterPointer();
}

//=========================================================
//=========================================================
class CAI_Relationship : public CPointEntity
{
	DECLARE_CLASS( CAI_Relationship, CPointEntity );

public:
	CAI_Relationship() { m_iPreviousDisposition = -1; }

	void Spawn();
	void KeyValue( KeyValueData *pkvd );
	void Activate();

	void Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );

	void ApplyRelationship( void );
	void ChangeRelationships( int disposition, bool fReverting );

	DECLARE_DATADESC();

private:

	void EXPORT	ApplyRelationshipThink( void );

	string_t	m_iszSubject;
	int			m_iDisposition;
	int			m_iRank;
	bool		m_fStartActive;
	bool		m_bIsActive;
	int			m_iPreviousDisposition;
};

LINK_ENTITY_TO_CLASS( ai_relationship, CAI_Relationship );

BEGIN_DATADESC( CAI_Relationship )

	DEFINE_FIELD( CAI_Relationship, m_iszSubject, FIELD_STRING ),
	DEFINE_FIELD( CAI_Relationship, m_iDisposition, FIELD_INTEGER ),
	DEFINE_FIELD( CAI_Relationship, m_iRank, FIELD_INTEGER ),
	DEFINE_FIELD( CAI_Relationship, m_fStartActive, FIELD_BOOLEAN ),
	DEFINE_FIELD( CAI_Relationship, m_bIsActive, FIELD_BOOLEAN ),
	DEFINE_FIELD( CAI_Relationship, m_iPreviousDisposition, FIELD_INTEGER ),

END_DATADESC()

void CAI_Relationship::KeyValue( KeyValueData *pkvd )
{
	if ( FStrEq( pkvd->szKeyName, "subject" ) )
	{
		// Sent over net now.
		m_iszSubject = ALLOC_STRING( pkvd->szValue );
		pkvd->fHandled = true;
	}
	else if ( FStrEq(pkvd->szKeyName, "rank") )
	{
		m_iRank = atoi( pkvd->szValue );
		pkvd->fHandled = true;
	}
	else if ( FStrEq(pkvd->szKeyName, "disposition") )
	{
		m_iDisposition = atoi( pkvd->szValue );
		pkvd->fHandled = true;
	}
	else if ( FStrEq(pkvd->szKeyName, "StartActive") )
	{
		m_fStartActive = atoi( pkvd->szValue );
		pkvd->fHandled = true;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CAI_Relationship::Spawn()
{
	m_bIsActive = false;

	if ( FStringNull( m_iszSubject ) )
	{
		ALERT( at_warning, "ai_relationship '%s' with no subject specified, removing.\n", GetDebugName() );
		UTIL_Remove( this );
	}
	else if ( FStringNull( pev->target ) )
	{
		ALERT( at_warning, "ai_relationship '%s' with no target specified, removing.\n", GetDebugName() );
		UTIL_Remove( this );
	}
}

//---------------------------------------------------------
//---------------------------------------------------------
void CAI_Relationship::Activate()
{
	if ( m_fStartActive )
	{
		ApplyRelationship();

		// Clear this flag so that nothing happens when the level is loaded (which calls activate again)
		m_fStartActive = false;
	}
}

//-----------------------------------------------------------------------------
// Purpose: This think function is used to wait until the player has properly
//			spawned, after all the NPCs have spawned.  Once that occurs, this
//			function terminates.
//-----------------------------------------------------------------------------
void CAI_Relationship::ApplyRelationshipThink( void )
{
	// Call down to the base until the player has properly spawned
	ApplyRelationship();
}

//---------------------------------------------------------
// Purpose: Applies the desired relationships to an entity
//---------------------------------------------------------
void CAI_Relationship::ApplyRelationship( void )
{
	// The player spawns slightly after the NPCs, meaning that if we don't wait, the
	// player will miss any relationships placed on them.
	if ( !g_pGameRules->IsMultiplayer() && !UTIL_PlayerByIndex( 1 ) )
	{
		SetThink( &CAI_Relationship::ApplyRelationshipThink );
		pev->nextthink = gpGlobals->time;
	}

	if ( !m_bIsActive )
	{
		m_bIsActive = true;
	}

	ChangeRelationships( m_iDisposition, false );
}

//---------------------------------------------------------
//---------------------------------------------------------
void CAI_Relationship::ChangeRelationships( int disposition, bool fReverting )
{
	if( fReverting && m_iPreviousDisposition == -1 )
	{
		// Trying to revert without having ever set the relationships!
		ALERT( at_error, "ai_relationship cannot revert changes before they are applied!\n");
		return;
	}

	// Assume the same thing for the target.
	CBaseEntity *pTarget;
	pTarget = UTIL_FindEntityByTargetname( NULL, STRING( m_iszSubject ) );

	CBaseEntity *pSubjectList[ 100 ];
	CBaseEntity *pTargetList[ 100 ];
	int numSubjects = 0;
	int numTargets = 0;

	// Populate the subject list. Try targetname first.
	CBaseEntity *pSearch = NULL;
	do
	{
		pSearch = UTIL_FindEntityByTargetname( pSearch, STRING( m_iszSubject ) );
		if( pSearch )
		{
			pSubjectList[ numSubjects ] = pSearch;
			numSubjects++;
		}
	} while( pSearch );

	if( numSubjects == 0 )
	{
		// No subjects found by targetname! Assume classname.
		do
		{
			pSearch = UTIL_FindEntityByClassname( pSearch, STRING( m_iszSubject ) );
			if( pSearch )
			{
				pSubjectList[ numSubjects ] = pSearch;
				numSubjects++;
			}
		} while( pSearch );
	}
	// If the subject list is still empty, we have an error!
	if( numSubjects == 0 )
	{
		ALERT( at_error, "ai_relationship finds no subject(s) called: %s\n", STRING( m_iszSubject ) );
		return;
	}

	// Now populate the target list.
	bool fTargetIsClass = false;
	do
	{
		pSearch = UTIL_FindEntityByTargetname( pSearch, STRING( pev->target ) );
		if( pSearch )
		{
			pTargetList[ numTargets ] = pSearch;
			numTargets++;
		}
	} while( pSearch );

	if( numTargets == 0 )
	{
		// No subjects found by targetname! Assume classname.
		fTargetIsClass = true;
		do
		{
			pSearch = UTIL_FindEntityByClassname( pSearch, STRING( pev->target ) );
			if( pSearch )
			{
				pTargetList[ numTargets ] = pSearch;
				numTargets++;
			}
		} while( pSearch );
	}
	// If the subject list is still empty, we have an error!
	if( numTargets == 0 )
	{
		ALERT( at_error, "ai_relationship finds no target(s) called: %s\n", STRING( pev->target ) );
		return;
	}

	// Ok, lists are populated. Apply all relationships.
	if( fTargetIsClass )
	{
		int i;
		for( i = 0 ; i < numSubjects ; i++ )
		{
			CBaseMonster *pSubject = pSubjectList[ i ]->MyMonsterPointer();
			if( !pSubject )
			{
				ALERT( at_console, "Skipping non-monster Subject\n" );
				continue;
			}

			// Since the target list is populated by indentical creatures, just use
			// the first one to get the class's AI classification for all subjects.
			CBaseMonster *pTarget = pTargetList[ 0 ]->MyMonsterPointer();
			if( !pTarget )
			{
				ALERT( at_console, "Skipping non-monster Target\n" );
				continue;
			}

			if( m_iPreviousDisposition == -1 )
			{
				// Set previous disposition.
				m_iPreviousDisposition = pSubject->IRelationType( pTarget );
			}

			pSubject->AddClassRelationship( pTarget->Classify(), (Disposition_t)disposition, m_iRank );
		}
	}
	else
	{
		int i,j;
		for( i = 0 ; i < numSubjects ; i++ )
		{
			CBaseMonster *pSubject = pSubjectList[ i ]->MyMonsterPointer();

			if( !pSubject )
			{
				ALERT( at_console, "Skipping non-monster Subject\n" );
				continue;
			}

			for( j = 0 ; j < numTargets ; j++ )
			{
				CBaseMonster *pTarget = pTargetList[ j ]->MyMonsterPointer();

				if( pTarget )
				{
					if( m_iPreviousDisposition == -1 )
					{
						// Set previous disposition.
						m_iPreviousDisposition = pSubject->IRelationType( pTarget );
					}

					pSubject->AddEntityRelationship( pTarget, (Disposition_t)disposition, m_iRank );
				}
				else
				{
					// Not a monster. Is it a player?
					if( pTargetList[ j ]->IsPlayer() )
					{
						if( m_iPreviousDisposition == -1 )
						{
							// Set previous disposition.
							m_iPreviousDisposition = pSubject->IRelationType( pTargetList[ j ] );
						}

						pSubject->AddEntityRelationship( pTargetList[ j ], (Disposition_t)disposition, m_iRank );
					}
					else
					{
						ALERT( at_console, "Skipping non-monster Target\n" );
						continue;
					}
				}
			}
		}
	}
}

void CAI_Relationship::Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	if ( !ShouldToggle( useType, m_bIsActive ) )
		return;

	if ( m_bIsActive )
	{
		ChangeRelationships( m_iPreviousDisposition, true );
		m_bIsActive = false;
	}
	else
	{
		ChangeRelationships( m_iDisposition, false );
		m_bIsActive = true;
	}
}