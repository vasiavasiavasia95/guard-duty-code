/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// Generic Monster - purely for scripted sequence work.
//=========================================================

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"
#include	"soundent.h"

// For holograms, make them not solid so the player can walk through them
#define	SF_GENERICMONSTER_NOTSOLID					4 
#define	SF_HEAD_CONTROLLER							8 

//=========================================================
// Monster's Anim Events Go Here
//=========================================================

class CGenericMonster : public CBaseMonster
{
public:
	DECLARE_CLASS( CGenericMonster, CBaseMonster );

	void Spawn( void );
	void Precache( void );
	void SetYawSpeed( void );
	Class_T Classify ( void );
	void HandleAnimEvent( animevent_t *pEvent );
	int GetSoundInterests ( void );

	// Blue Shift stuff

	DECLARE_DATADESC();

	void MonsterThink( void );
	void PlayScriptedSentence( const char *pszSentence, float duration, float volume, float attenuation, bool bConcurrent, CBaseEntity *pListener );
	void IdleHeadTurn( Vector &vecFriend );

private:

	float		m_talkTime;
	float		m_flIdealYaw;
	float		m_flCurrentYaw;

	EHANDLE		m_hTalkTarget;
};
LINK_ENTITY_TO_CLASS( monster_generic, CGenericMonster );

BEGIN_DATADESC(	CGenericMonster )

	// Vasia: BUG! Original implementation had this saved as a float
	//DEFINE_FIELD( CGenericMonster, m_talkTime, FIELD_FLOAT ), 
	DEFINE_FIELD( CGenericMonster, m_talkTime, FIELD_TIME ), 
	DEFINE_FIELD( CGenericMonster, m_hTalkTarget, FIELD_EHANDLE ),
	
	DEFINE_FIELD( CGenericMonster, m_flIdealYaw, FIELD_FLOAT ),
	DEFINE_FIELD( CGenericMonster, m_flCurrentYaw, FIELD_FLOAT ),

END_DATADESC()

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CGenericMonster::Classify( void )
{
	return CLASS_PLAYER_ALLY;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CGenericMonster::SetYawSpeed( void )
{
	int ys;

	switch ( m_Activity )
	{
	case ACT_IDLE:
	default:
		ys = 90;
	}

	pev->yaw_speed = ys;
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CGenericMonster::HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case 0:
	default:
		BaseClass::HandleAnimEvent( pEvent );
		break;
	}
}

//=========================================================
// GetSoundInterests - generic monster can't hear.
//=========================================================
int CGenericMonster::GetSoundInterests( void )
{
	return bits_SOUND_NONE;
}

//=========================================================
// Spawn
//=========================================================
void CGenericMonster::Spawn( void )
{
	Precache();

	SET_MODEL( ENT(pev), STRING(pev->model) );

/*
	if ( FStrEq( STRING(pev->model), "models/player.mdl" ) )
		UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);
	else
		UTIL_SetSize(pev, VEC_HULL_MIN, VEC_HULL_MAX);
*/

	UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_RED;
	pev->health			= 8;
	m_flFieldOfView		= 0.5;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;

	MonsterInit();

	if ( pev->spawnflags & SF_GENERICMONSTER_NOTSOLID )
	{
		pev->solid = SOLID_NOT;
		pev->takedamage = DAMAGE_NO;
	}

	if ( pev->spawnflags & SF_HEAD_CONTROLLER )
	{
		CapabilitiesClear();
		CapabilitiesAdd( bits_CAP_TURN_HEAD );
	}

	m_flCurrentYaw = 0.0;
	m_flIdealYaw = 0.0;
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CGenericMonster::Precache( void )
{
	PRECACHE_MODEL( (char *)STRING(pev->model) );
}	

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

void CGenericMonster::MonsterThink( void )
{
	// Vasia BS FEATURE: look at who I'm talking to, if SF_HEAD_CONTROLLER is set.
	if ( CapabilitiesGet() & bits_CAP_TURN_HEAD )
	{
		if ( m_hTalkTarget )
		{
			if (gpGlobals->time > m_talkTime)
			{
				m_flIdealYaw = 0;
				m_hTalkTarget = NULL;
			}
			else
			{
				IdleHeadTurn(m_hTalkTarget->pev->origin);
			}
		}

		if ( m_flCurrentYaw != m_flIdealYaw)
		{
			if (m_flCurrentYaw <= m_flIdealYaw)
			{
				m_flCurrentYaw += V_min(20.0, m_flIdealYaw - m_flCurrentYaw);
			}
			else
			{
				m_flCurrentYaw -= V_min(20.0, m_flCurrentYaw - m_flIdealYaw);
			}

			SetBoneController(0, m_flCurrentYaw);
		}
	}

	BaseClass::MonsterThink();
}

void CGenericMonster::PlayScriptedSentence( const char *pszSentence, float duration, float volume, float attenuation, bool bConcurrent, CBaseEntity *pListener )
{
	BaseClass::PlayScriptedSentence( pszSentence, duration, volume, attenuation, bConcurrent, pListener );

	m_talkTime = gpGlobals->time + duration;
	m_hTalkTarget = pListener;
}

// turn head towards supplied origin
void CGenericMonster::IdleHeadTurn( Vector &vecFriend )
{
	 // turn head in desired direction only if ent has a turnable head
	if ( CapabilitiesGet() & bits_CAP_TURN_HEAD)
	{
		float yaw = VecToYaw(vecFriend - pev->origin) - pev->angles.y;

		if (yaw > 180) yaw -= 360;
		if (yaw < -180) yaw += 360;

		// turn towards vector
		m_flIdealYaw = yaw;
	}
}