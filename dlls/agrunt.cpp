/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// Agrunt - Dominant, warlike alien grunt monster
//=========================================================

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"
#include	"ai_squadslot.h"
#include	"weapons.h"
#include	"soundent.h"
#include	"hornet.h"

extern cvar_t sk_agrunt_health;
extern cvar_t sk_agrunt_dmg_punch;

//=========================================================
// monster-specific squad slots
//=========================================================
enum AGruntSquadSlot_T
{	
	AGRUNT_SQUAD_SLOT_HORNET1 = LAST_SHARED_SQUADSLOT,
	AGRUNT_SQUAD_SLOT_HORNET2,
	AGRUNT_SQUAD_SLOT_CHASE,
};

//=========================================================
// monster-specific schedule types
//=========================================================
enum
{
	SCHED_AGRUNT_FAIL = LAST_SHARED_SCHEDULE,
	SCHED_AGRUNT_COMBAT_FAIL,
	SCHED_AGRUNT_STANDOFF,
	SCHED_AGRUNT_SUPPRESS,
	SCHED_AGRUNT_RANGE_ATTACK,
	SCHED_AGRUNT_HIDDEN_RANGE_ATTACK,
	SCHED_AGRUNT_TAKE_COVER_FROM_ENEMY,
	SCHED_AGRUNT_VICTORY_DANCE,
	SCHED_AGRUNT_THREAT_DISPLAY,
};

//=========================================================
// monster-specific tasks
//=========================================================
enum 
{
	TASK_AGRUNT_SETUP_HIDE_ATTACK = LAST_SHARED_TASK,
	TASK_AGRUNT_GET_PATH_TO_ENEMY_CORPSE,
};

int iAgruntMuzzleFlash;

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		AGRUNT_AE_HORNET1	( 1 )
#define		AGRUNT_AE_HORNET2	( 2 )
#define		AGRUNT_AE_HORNET3	( 3 )
#define		AGRUNT_AE_HORNET4	( 4 )
#define		AGRUNT_AE_HORNET5	( 5 )
// some events are set up in the QC file that aren't recognized by the code yet.
#define		AGRUNT_AE_PUNCH		( 6 )
#define		AGRUNT_AE_BITE		( 7 )

#define		AGRUNT_AE_LEFT_FOOT	 ( 10 )
#define		AGRUNT_AE_RIGHT_FOOT ( 11 )

#define		AGRUNT_AE_LEFT_PUNCH ( 12 )
#define		AGRUNT_AE_RIGHT_PUNCH ( 13 )

#define		AGRUNT_MELEE_DIST	100

class CAGrunt : public CBaseMonster
{
	DECLARE_CLASS( CAGrunt, CBaseMonster );
public:
	void Spawn( void );
	void Precache( void );
	void SetYawSpeed( void );
	Class_T Classify( void );
	int  GetSoundInterests( void );
	void HandleAnimEvent( animevent_t *pEvent );
	void SetObjectCollisionBox( void )
	{
		pev->absmin = pev->origin + Vector( -32, -32, 0 );
		pev->absmax = pev->origin + Vector( 32, 32, 85 );
	}

	int SelectSchedule( void );
	int TranslateSchedule( int scheduleType );

	bool FCanCheckAttacks ( void );
	bool CheckMeleeAttack1 ( float flDot, float flDist );
	bool CheckRangeAttack1 ( float flDot, float flDist );
	void StartTask ( const Task_t *pTask );
	void AlertSound( void );
	void DeathSound ( void );
	void PainSound ( void );
	void AttackSound ( void );
	void PrescheduleThink ( void );
	void TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, TraceResult *ptr );
	int	 IRelationPriority( CBaseEntity *pTarget );
	void StopTalking ( void );
	bool ShouldSpeak( void );

	DECLARE_DATADESC();

	static const char *pAttackHitSounds[];
	static const char *pAttackMissSounds[];
	static const char *pAttackSounds[];
	static const char *pDieSounds[];
	static const char *pPainSounds[];
	static const char *pIdleSounds[];
	static const char *pAlertSounds[];

	DEFINE_CUSTOM_AI;

	bool	m_fCanHornetAttack;
	float	m_flNextHornetAttackCheck;

	float m_flNextPainTime;

	// three hacky fields for speech stuff. These don't really need to be saved.
	float	m_flNextSpeakTime;
	float	m_flNextWordTime;
	int		m_iLastWord;
};
LINK_ENTITY_TO_CLASS( monster_alien_grunt, CAGrunt );

BEGIN_DATADESC( CAGrunt )
	DEFINE_FIELD( CAGrunt, m_fCanHornetAttack, FIELD_BOOLEAN ),
	DEFINE_FIELD( CAGrunt, m_flNextHornetAttackCheck, FIELD_TIME ),
	DEFINE_FIELD( CAGrunt, m_flNextPainTime, FIELD_TIME ),
	DEFINE_FIELD( CAGrunt, m_flNextSpeakTime, FIELD_TIME ),
	DEFINE_FIELD( CAGrunt, m_flNextWordTime, FIELD_TIME ),
	DEFINE_FIELD( CAGrunt, m_iLastWord, FIELD_INTEGER ),
END_DATADESC()

const char *CAGrunt::pAttackHitSounds[] = 
{
	"zombie/claw_strike1.wav",
	"zombie/claw_strike2.wav",
	"zombie/claw_strike3.wav",
};

const char *CAGrunt::pAttackMissSounds[] = 
{
	"zombie/claw_miss1.wav",
	"zombie/claw_miss2.wav",
};

const char *CAGrunt::pAttackSounds[] =
{
	"agrunt/ag_attack1.wav",
	"agrunt/ag_attack2.wav",
	"agrunt/ag_attack3.wav",
};

const char *CAGrunt::pDieSounds[] =
{
	"agrunt/ag_die1.wav",
	"agrunt/ag_die4.wav",
	"agrunt/ag_die5.wav",
};

const char *CAGrunt::pPainSounds[] =
{
	"agrunt/ag_pain1.wav",
	"agrunt/ag_pain2.wav",
	"agrunt/ag_pain3.wav",
	"agrunt/ag_pain4.wav",
	"agrunt/ag_pain5.wav",
};

const char *CAGrunt::pIdleSounds[] =
{
	"agrunt/ag_idle1.wav",
	"agrunt/ag_idle2.wav",
	"agrunt/ag_idle3.wav",
	"agrunt/ag_idle4.wav",
};

const char *CAGrunt::pAlertSounds[] =
{
	"agrunt/ag_alert1.wav",
	"agrunt/ag_alert3.wav",
	"agrunt/ag_alert4.wav",
	"agrunt/ag_alert5.wav",
};

//=========================================================
// IRelationship - overridden because Human Grunts are 
// Alien Grunt's nemesis.
//=========================================================
int CAGrunt::IRelationPriority( CBaseEntity *pTarget )
{
	if ( FClassnameIs( pTarget->pev, "monster_human_grunt" ) )
	{
		return ( BaseClass::IRelationPriority ( pTarget ) + 1 );
	}

	return BaseClass::IRelationPriority( pTarget );
}

//=========================================================
// GetSoundInterests 
//=========================================================
int CAGrunt :: GetSoundInterests ( void )
{
	return	bits_SOUND_WORLD	|
			bits_SOUND_COMBAT	|
			bits_SOUND_PLAYER	|
			bits_SOUND_DANGER;
}

//=========================================================
// TraceAttack
//=========================================================
void CAGrunt :: TraceAttack( const CTakeDamageInfo &inputinfo, const Vector &vecDir, TraceResult *ptr )
{
	CTakeDamageInfo info = inputinfo;
	
	float flDamage = info.GetDamage();

	if ( ptr->iHitgroup == 10 && (info.GetDamageType() & (DMG_BULLET | DMG_SLASH | DMG_CLUB)))
	{
		// hit armor
		if ( pev->dmgtime != gpGlobals->time || (RANDOM_LONG(0,10) < 1) )
		{
			UTIL_Ricochet( ptr->vecEndPos, RANDOM_FLOAT( 1, 2) );
			pev->dmgtime = gpGlobals->time;
		}

		if ( RANDOM_LONG( 0, 1 ) == 0 )
		{
			Vector vecTracerDir = vecDir;

			vecTracerDir.x += RANDOM_FLOAT( -0.3, 0.3 );
			vecTracerDir.y += RANDOM_FLOAT( -0.3, 0.3 );
			vecTracerDir.z += RANDOM_FLOAT( -0.3, 0.3 );

			vecTracerDir = vecTracerDir * -512;

			MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, ptr->vecEndPos );
			WRITE_BYTE( TE_TRACER );
				WRITE_COORD( ptr->vecEndPos.x );
				WRITE_COORD( ptr->vecEndPos.y );
				WRITE_COORD( ptr->vecEndPos.z );

				WRITE_COORD( vecTracerDir.x );
				WRITE_COORD( vecTracerDir.y );
				WRITE_COORD( vecTracerDir.z );
			MESSAGE_END();
		}

		flDamage -= 20;
		if (flDamage <= 0)
			flDamage = 0.1;// don't hurt the monster much, but allow bits_COND_LIGHT_DAMAGE to be generated
	}
	else
	{
		SpawnBlood(ptr->vecEndPos, BloodColor(), flDamage);// a little surface blood.
		TraceBleed( flDamage, vecDir, ptr, info.GetDamageType() );
	}

	AddMultiDamage( info, this );
}

//=========================================================
// StopTalking - won't speak again for 10-20 seconds.
//=========================================================
void CAGrunt::StopTalking( void )
{
	m_flNextWordTime = m_flNextSpeakTime = gpGlobals->time + 10 + RANDOM_LONG(0, 10);
}

//=========================================================
// ShouldSpeak - Should this agrunt be talking?
//=========================================================
bool CAGrunt::ShouldSpeak( void )
{
	if ( m_flNextSpeakTime > gpGlobals->time )
	{
		// my time to talk is still in the future.
		return false;
	}

	if ( HasSpawnFlags( SF_MONSTER_GAG ) )
	{
		if ( m_MonsterState != MONSTERSTATE_COMBAT )
		{
			// if gagged, don't talk outside of combat.
			// if not going to talk because of this, put the talk time 
			// into the future a bit, so we don't talk immediately after 
			// going into combat
			m_flNextSpeakTime = gpGlobals->time + 3;
			return false;
		}
	}

	return true;
}

//=========================================================
// PrescheduleThink 
//=========================================================
void CAGrunt :: PrescheduleThink ( void )
{
	if ( ShouldSpeak() )
	{
		if ( m_flNextWordTime < gpGlobals->time )
		{
			int num = -1;

			do
			{
				num = RANDOM_LONG(0,ARRAYSIZE(pIdleSounds)-1);
			} while( num == m_iLastWord );

			m_iLastWord = num;

			// play a new sound
			EMIT_SOUND ( ENT(pev), CHAN_VOICE, pIdleSounds[ num ], 1.0, ATTN_NORM );

			// is this word our last?
			if ( RANDOM_LONG( 1, 10 ) <= 1 )
			{
				// stop talking.
				StopTalking();
			}
			else
			{
				m_flNextWordTime = gpGlobals->time + RANDOM_FLOAT( 0.5, 1 );
			}
		}
	}
}

//=========================================================
// DieSound
//=========================================================
void CAGrunt :: DeathSound ( void )
{
	StopTalking();

	EMIT_SOUND ( ENT(pev), CHAN_VOICE, pDieSounds[RANDOM_LONG(0,ARRAYSIZE(pDieSounds)-1)], 1.0, ATTN_NORM );
}

//=========================================================
// AlertSound
//=========================================================
void CAGrunt :: AlertSound ( void )
{
	StopTalking();

	EMIT_SOUND ( ENT(pev), CHAN_VOICE, pAlertSounds[RANDOM_LONG(0,ARRAYSIZE(pAlertSounds)-1)], 1.0, ATTN_NORM );
}

//=========================================================
// AttackSound
//=========================================================
void CAGrunt :: AttackSound ( void )
{
	StopTalking();

	EMIT_SOUND ( ENT(pev), CHAN_VOICE, pAttackSounds[RANDOM_LONG(0,ARRAYSIZE(pAttackSounds)-1)], 1.0, ATTN_NORM );
}

//=========================================================
// PainSound
//=========================================================
void CAGrunt :: PainSound ( void )
{
	if ( m_flNextPainTime > gpGlobals->time )
	{
		return;
	}

	m_flNextPainTime = gpGlobals->time + 0.6;

	StopTalking();

	EMIT_SOUND ( ENT(pev), CHAN_VOICE, pPainSounds[RANDOM_LONG(0,ARRAYSIZE(pPainSounds)-1)], 1.0, ATTN_NORM );
}

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CAGrunt::Classify( void )
{
	return CLASS_ALIEN_MILITARY;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CAGrunt :: SetYawSpeed ( void )
{
	int ys;

	switch ( GetActivity() )
	{
	case ACT_TURN_LEFT:
	case ACT_TURN_RIGHT:
		ys = 110;
		break;
	default:			ys = 100;
	}

	pev->yaw_speed = ys;
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//
// Returns number of events handled, 0 if none.
//=========================================================
void CAGrunt :: HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case AGRUNT_AE_HORNET1:
	case AGRUNT_AE_HORNET2:
	case AGRUNT_AE_HORNET3:
	case AGRUNT_AE_HORNET4:
	case AGRUNT_AE_HORNET5:
		{
			// m_vecEnemyLKP should be center of enemy body
			Vector vecArmPos, vecArmDir;
			Vector vecDirToEnemy;
			Vector angDir;

			if ( HasCondition( COND_SEE_ENEMY ) )
			{
				Vector vecEnemyLKP = GetEnemyLKP();

				vecDirToEnemy = ( ( vecEnemyLKP ) - pev->origin );
				angDir = UTIL_VecToAngles( vecDirToEnemy );
				vecDirToEnemy = vecDirToEnemy.Normalize();
			}
			else
			{
				angDir = pev->angles;
				UTIL_MakeAimVectors( angDir );
				vecDirToEnemy = gpGlobals->v_forward;
			}

			pev->effects = EF_MUZZLEFLASH;

			// make angles +-180
			if (angDir.x > 180)
			{
				angDir.x = angDir.x - 360;
			}

			SetBlending( 0, -angDir.x );
			GetAttachment( 0, vecArmPos, vecArmDir );

			vecArmPos = vecArmPos + vecDirToEnemy * 32;
			MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, vecArmPos );
				WRITE_BYTE( TE_SPRITE );
				WRITE_COORD( vecArmPos.x );	// pos
				WRITE_COORD( vecArmPos.y );	
				WRITE_COORD( vecArmPos.z );	
				WRITE_SHORT( iAgruntMuzzleFlash );		// model
				WRITE_BYTE( 6 );				// size * 10
				WRITE_BYTE( 128 );			// brightness
			MESSAGE_END();

			CBaseEntity *pHornet = CBaseEntity::Create( "hornet", vecArmPos, UTIL_VecToAngles( vecDirToEnemy ), edict() );
			UTIL_MakeVectors ( pHornet->pev->angles );
			pHornet->pev->velocity = gpGlobals->v_forward * 300;
			
			CBaseMonster *pHornetMonster = pHornet->MyMonsterPointer();

			if ( pHornetMonster )
			{
				pHornetMonster->SetEnemy(m_hEnemy);
			}
		}
		break;

	case AGRUNT_AE_LEFT_FOOT:
		switch (RANDOM_LONG(0,1))
		{
		// left foot
		case 0:	EMIT_SOUND_DYN ( ENT(pev), CHAN_BODY, "player/pl_ladder2.wav", 1, ATTN_NORM, 0, 70 );	break;
		case 1:	EMIT_SOUND_DYN ( ENT(pev), CHAN_BODY, "player/pl_ladder4.wav", 1, ATTN_NORM, 0, 70 );	break;
		}
		break;
	case AGRUNT_AE_RIGHT_FOOT:
		// right foot
		switch (RANDOM_LONG(0,1))
		{
		case 0:	EMIT_SOUND_DYN ( ENT(pev), CHAN_BODY, "player/pl_ladder1.wav", 1, ATTN_NORM, 0, 70 );	break;
		case 1:	EMIT_SOUND_DYN ( ENT(pev), CHAN_BODY, "player/pl_ladder3.wav", 1, ATTN_NORM, 0 ,70);	break;
		}
		break;

	case AGRUNT_AE_LEFT_PUNCH:
		{
			CBaseEntity *pHurt = CheckTraceHullAttack( AGRUNT_MELEE_DIST, sk_agrunt_dmg_punch.value, DMG_CLUB );
			
			if ( pHurt )
			{
				pHurt->pev->punchangle.y = -25;
				pHurt->pev->punchangle.x = 8;

				// OK to use gpGlobals without calling MakeVectors, cause CheckTraceHullAttack called it above.
				if ( pHurt->IsPlayer() )
				{
					// this is a player. Knock him around.
					pHurt->pev->velocity = pHurt->pev->velocity + gpGlobals->v_right * 250;
				}

				EMIT_SOUND_DYN ( ENT(pev), CHAN_WEAPON, pAttackHitSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackHitSounds)-1) ], 1.0, ATTN_NORM, 0, 100 + RANDOM_LONG(-5,5) );

				Vector vecArmPos, vecArmAng;
				GetAttachment( 0, vecArmPos, vecArmAng );
				SpawnBlood(vecArmPos, pHurt->BloodColor(), 25);// a little surface blood.
			}
			else
			{
				// Play a random attack miss sound
				EMIT_SOUND_DYN ( ENT(pev), CHAN_WEAPON, pAttackMissSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackMissSounds)-1) ], 1.0, ATTN_NORM, 0, 100 + RANDOM_LONG(-5,5) );
			}
		}
		break;

	case AGRUNT_AE_RIGHT_PUNCH:
		{
			CBaseEntity *pHurt = CheckTraceHullAttack( AGRUNT_MELEE_DIST, sk_agrunt_dmg_punch.value, DMG_CLUB );

			if ( pHurt )
			{
				pHurt->pev->punchangle.y = 25;
				pHurt->pev->punchangle.x = 8;

				// OK to use gpGlobals without calling MakeVectors, cause CheckTraceHullAttack called it above.
				if ( pHurt->IsPlayer() )
				{
					// this is a player. Knock him around.
					pHurt->pev->velocity = pHurt->pev->velocity + gpGlobals->v_right * -250;
				}

				EMIT_SOUND_DYN ( ENT(pev), CHAN_WEAPON, pAttackHitSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackHitSounds)-1) ], 1.0, ATTN_NORM, 0, 100 + RANDOM_LONG(-5,5) );

				Vector vecArmPos, vecArmAng;
				GetAttachment( 0, vecArmPos, vecArmAng );
				SpawnBlood(vecArmPos, pHurt->BloodColor(), 25);// a little surface blood.
			}
			else
			{
				// Play a random attack miss sound
				EMIT_SOUND_DYN ( ENT(pev), CHAN_WEAPON, pAttackMissSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackMissSounds)-1) ], 1.0, ATTN_NORM, 0, 100 + RANDOM_LONG(-5,5) );
			}
		}
		break;

	default:
		BaseClass::HandleAnimEvent( pEvent );
		break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CAGrunt :: Spawn()
{
	Precache( );

	SET_MODEL(ENT(pev), "models/agrunt.mdl");
	UTIL_SetSize(pev, Vector(-32, -32, 0), Vector(32, 32, 64));

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_GREEN;
	pev->effects		= 0;
	pev->health			= sk_agrunt_health.value;
	m_flFieldOfView		= 0.2;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;
	
	CapabilitiesClear();
	CapabilitiesAdd ( bits_CAP_SQUAD );

	m_HackedGunPos		= Vector( 24, 64, 48 );

	m_flNextSpeakTime	= m_flNextWordTime = gpGlobals->time + 10 + RANDOM_LONG(0, 10);

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CAGrunt :: Precache()
{
	PRECACHE_MODEL("models/agrunt.mdl");

	PRECACHE_SOUND_ARRAY(pAttackHitSounds);
	PRECACHE_SOUND_ARRAY(pAttackMissSounds);
	PRECACHE_SOUND_ARRAY(pIdleSounds);
	PRECACHE_SOUND_ARRAY(pDieSounds);
	PRECACHE_SOUND_ARRAY(pPainSounds);
	PRECACHE_SOUND_ARRAY(pAttackSounds);
	PRECACHE_SOUND_ARRAY(pAlertSounds);

	PRECACHE_SOUND( "hassault/hw_shoot1.wav" );

	iAgruntMuzzleFlash = PRECACHE_MODEL( "sprites/muz4.spr" );

	UTIL_PrecacheOther( "hornet" );
}	
	
//=========================================================
// FCanCheckAttacks - this is overridden for alien grunts
// because they can use their smart weapons against unseen
// enemies. Base class doesn't attack anyone it can't see.
//=========================================================
bool CAGrunt :: FCanCheckAttacks ( void )
{
	if ( !HasCondition( COND_ENEMY_TOO_FAR ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

//=========================================================
// CheckMeleeAttack1 - alien grunts zap the crap out of 
// any enemy that gets too close. 
//=========================================================
bool CAGrunt::CheckMeleeAttack1( float flDot, float flDist )
{
	if ( HasCondition( COND_SEE_ENEMY ) && flDist <= AGRUNT_MELEE_DIST && flDot >= 0.6 && m_hEnemy != NULL )
	{
		return true;
	}
	return false;
}

//=========================================================
// CheckRangeAttack1 
//
// !!!LATER - we may want to load balance this. Several
// tracelines are done, so we may not want to do this every
// server frame. Definitely not while firing. 
//=========================================================
bool CAGrunt::CheckRangeAttack1( float flDot, float flDist )
{
	if ( gpGlobals->time < m_flNextHornetAttackCheck )
	{
		return m_fCanHornetAttack;
	}

	if ( HasCondition( COND_SEE_ENEMY ) && flDist >= AGRUNT_MELEE_DIST && flDist <= 1024 && flDot >= 0.5 && NoFriendlyFire() )
	{
		TraceResult	tr;
		Vector	vecArmPos, vecArmDir;

		// verify that a shot fired from the gun will hit the enemy before the world.
		// !!!LATER - we may wish to do something different for projectile weapons as opposed to instant-hit
		UTIL_MakeVectors( pev->angles );
		GetAttachment( 0, vecArmPos, vecArmDir );
//		UTIL_TraceLine( vecArmPos, vecArmPos + gpGlobals->v_forward * 256, ignore_monsters, ENT(pev), &tr);
		UTIL_TraceLine( vecArmPos, m_hEnemy->BodyTarget(vecArmPos), dont_ignore_monsters, ENT(pev), &tr);

		if ( tr.flFraction == 1.0 || tr.pHit == m_hEnemy->edict() )
		{
			m_flNextHornetAttackCheck = gpGlobals->time + RANDOM_FLOAT( 2, 5 );
			m_fCanHornetAttack = true;
			return m_fCanHornetAttack;
		}
	}
	
	m_flNextHornetAttackCheck = gpGlobals->time + 0.2;// don't check for half second if this check wasn't successful
	m_fCanHornetAttack = false;
	return m_fCanHornetAttack;
}

//=========================================================
// StartTask
//=========================================================
void CAGrunt :: StartTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_AGRUNT_GET_PATH_TO_ENEMY_CORPSE:
		{
			Vector forward;
			AngleVectors( pev->angles, &forward );
			Vector vecEnemyLKP = GetEnemyLKP();

			if ( BuildRoute ( vecEnemyLKP - forward * 50, bits_MF_TO_LOCATION, NULL ) )
			{
				TaskComplete();
			}
			else
			{
				ALERT ( at_aiconsole, "AGruntGetPathToEnemyCorpse failed!!\n" );
				TaskFail();
			}
		}
		break;

	case TASK_AGRUNT_SETUP_HIDE_ATTACK:
		// alien grunt shoots hornets back out into the open from a concealed location. 
		// try to find a spot to throw that gives the smart weapon a good chance of finding the enemy.
		// ideally, this spot is along a line that is perpendicular to a line drawn from the agrunt to the enemy.

		if ( GetEnemyCombatCharacterPointer() )
		{
			Vector		vecCenter, vForward, vRight, vecEnemyLKP, vecAngTemp;
			TraceResult	tr;
			bool		fSkip;

			fSkip = false;
			vecCenter = Center();
			vecEnemyLKP = GetEnemyLKP();

			VectorAngles( vecEnemyLKP - pev->origin, vecAngTemp );
			AngleVectors( vecAngTemp, &vForward, &vRight, NULL );

			UTIL_TraceLine( Center() + vForward * 128, vecEnemyLKP, ignore_monsters, ENT(pev), &tr);
			if ( tr.flFraction == 1.0 )
			{
				MakeIdealYaw ( pev->origin + vRight * 128 );
				fSkip = true;
				TaskComplete();
			}
			
			if ( !fSkip )
			{
				UTIL_TraceLine( Center() - vForward * 128, vecEnemyLKP, ignore_monsters, ENT(pev), &tr);
				if ( tr.flFraction == 1.0 )
				{
					MakeIdealYaw ( pev->origin - vRight * 128 );
					fSkip = true;
					TaskComplete();
				}
			}
			
			if ( !fSkip )
			{
				UTIL_TraceLine( Center() + vForward * 256, vecEnemyLKP, ignore_monsters, ENT(pev), &tr);
				if ( tr.flFraction == 1.0 )
				{
					MakeIdealYaw ( pev->origin + vRight * 256 );
					fSkip = true;
					TaskComplete();
				}
			}
			
			if ( !fSkip )
			{
				UTIL_TraceLine( Center() - vForward * 256, vecEnemyLKP, ignore_monsters, ENT(pev), &tr);
				if ( tr.flFraction == 1.0 )
				{
					MakeIdealYaw ( pev->origin - vRight * 256 );
					fSkip = true;
					TaskComplete();
				}
			}
			
			if ( !fSkip )
			{
				TaskFail();
			}
		}
		else
		{
			ALERT ( at_aiconsole, "AGRunt - no enemy monster ptr!!!\n" );
			TaskFail();
		}
		break;

	default:
		BaseClass::StartTask ( pTask );
		break;
	}
}

//=========================================================
// GetSchedule - Decides which type of schedule best suits
// the monster's current state and conditions. Then calls
// monster's member function to get a pointer to a schedule
// of the proper type.
//=========================================================
int CAGrunt :: SelectSchedule ( void )
{
	if ( HasCondition( COND_HEAR_DANGER ) )
	{
		// dangerous sound nearby!
		return SCHED_TAKE_COVER_FROM_BEST_SOUND;
	}

	switch	( m_MonsterState )
	{
	case MONSTERSTATE_COMBAT:
		{
// dead enemy
			if ( HasCondition( COND_ENEMY_DEAD ) )
			{
				// call base class, all code to handle dead enemies is centralized there.
				return CBaseMonster :: SelectSchedule();
			}

			if ( HasCondition( COND_NEW_ENEMY ) )
			{
				return SCHED_WAKE_ANGRY;
			}

	// zap player!
			if ( HasCondition( COND_CAN_MELEE_ATTACK1 ) )
			{
				AttackSound();// this is a total hack. Should be parto f the schedule
				return SCHED_MELEE_ATTACK1;
			}

			if ( HasCondition( COND_HEAVY_DAMAGE ) )
			{
				return SCHED_BIG_FLINCH;
			}

	// can attack
			if ( HasCondition( COND_CAN_RANGE_ATTACK1 ) && OccupyStrategySlotRange( AGRUNT_SQUAD_SLOT_HORNET1, AGRUNT_SQUAD_SLOT_HORNET2 ) )
			{
				return SCHED_RANGE_ATTACK1;
			}

			if ( OccupyStrategySlot ( AGRUNT_SQUAD_SLOT_CHASE ) )
			{
				return SCHED_CHASE_ENEMY;
			}

			return  SCHED_STANDOFF;
		}
	}

	return BaseClass::SelectSchedule();
}

//=========================================================
//=========================================================
int CAGrunt::TranslateSchedule( int scheduleType )
{
	switch	( scheduleType )
	{
	case SCHED_TAKE_COVER_FROM_ENEMY:
		return SCHED_AGRUNT_TAKE_COVER_FROM_ENEMY;
		break;
	
	case SCHED_RANGE_ATTACK1:
		if ( HasCondition( COND_SEE_ENEMY ) )
		{
			//normal attack
			return SCHED_AGRUNT_RANGE_ATTACK;
		}
		else
		{
			// attack an unseen enemy
			//return SCHED_AGRUNT_HIDDEN_RANGE_ATTACK
			return SCHED_AGRUNT_RANGE_ATTACK;
		}
		break;

	case SCHED_STANDOFF:
		return SCHED_AGRUNT_STANDOFF;
		break;

	case SCHED_VICTORY_DANCE:
		return SCHED_AGRUNT_VICTORY_DANCE;
		break;

	case SCHED_FAIL:
		// no fail schedule specified, so pick a good generic one.
		{
			if ( m_hEnemy != NULL )
			{
				// I have an enemy
				// !!!LATER - what if this enemy is really far away and i'm chasing him?
				// this schedule will make me stop, face his last known position for 2 
				// seconds, and then try to move again
				return SCHED_AGRUNT_COMBAT_FAIL;
			}

			return SCHED_AGRUNT_FAIL;
		}
		break;

	}

	return BaseClass::TranslateSchedule( scheduleType );
}

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

AI_BEGIN_CUSTOM_NPC( monster_alien_grunt, CAGrunt )

	DECLARE_TASK ( TASK_AGRUNT_SETUP_HIDE_ATTACK )
	DECLARE_TASK ( TASK_AGRUNT_GET_PATH_TO_ENEMY_CORPSE )

	DECLARE_SQUADSLOT( AGRUNT_SQUAD_SLOT_HORNET1 )
	DECLARE_SQUADSLOT( AGRUNT_SQUAD_SLOT_HORNET2 )
	DECLARE_SQUADSLOT( AGRUNT_SQUAD_SLOT_CHASE )

	//=========================================================
	// Fail Schedule
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_FAIL,
			"	Tasks"
			"	TASK_STOP_MOVING			0"
			"	TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
			"	TASK_WAIT					2"
			"	TASK_WAIT_PVS				0"
			""
			"	Interrupts"
			"	COND_CAN_RANGE_ATTACK1"
			"	COND_CAN_MELEE_ATTACK1"
	)

	//=========================================================
	// Combat Fail Schedule
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_COMBAT_FAIL,
			"	Tasks"
			"	TASK_STOP_MOVING			0"
			"	TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
			"	TASK_WAIT_FACE_ENEMY		2"
			"	TASK_WAIT_PVS				0"
			"	"
			"	Interrupts"
			"	COND_CAN_RANGE_ATTACK1"
			"	COND_CAN_MELEE_ATTACK1"
	)

	//=========================================================
	// Standoff schedule. Used in combat when a monster is 
	// hiding in cover or the enemy has moved out of sight. 
	// Should we look around in this schedule?
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_STANDOFF,
		
			"	Tasks"
			"		TASK_STOP_MOVING			0"
			"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
			"		TASK_WAIT_FACE_ENEMY		2"
			"	"
			"	Interrupts"
			"		COND_CAN_RANGE_ATTACK1"
			"		COND_CAN_MELEE_ATTACK1"
			"		COND_SEE_ENEMY"
			"		COND_NEW_ENEMY"
			"		COND_HEAR_DANGER"
	)

	//=========================================================
	// Suppress
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_SUPPRESS,
			"	Tasks"
			"	TASK_STOP_MOVING			0"
			"	TASK_RANGE_ATTACK1			0"
	)

	//=========================================================
	// primary range attacks
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_RANGE_ATTACK,
			"	Tasks"
			"	TASK_STOP_MOVING			0"
			"	TASK_FACE_ENEMY				0"
			"	TASK_RANGE_ATTACK1			0"
			"	"
			"	Interrupts"
			"	COND_NEW_ENEMY"
			"	COND_ENEMY_DEAD"
			"	COND_HEAVY_DAMAGE"
	)

	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_HIDDEN_RANGE_ATTACK,
			"	Tasks"
			"	TASK_SET_FAIL_SCHEDULE				SCHEDULE:SCHED_AGRUNT_STANDOFF"
			"	TASK_AGRUNT_SETUP_HIDE_ATTACK		0"
			"	TASK_STOP_MOVING					0"
			"	TASK_FACE_IDEAL						0"
			"	TASK_RANGE_ATTACK1_NOTURN			0"
			"	"
			"	Interrupts"
			"	COND_NEW_ENEMY"
			"	COND_HEAVY_DAMAGE"
			"	COND_HEAR_DANGER"
	)

	//=========================================================
	// Take cover from enemy! Tries lateral cover before node 
	// cover! 
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_TAKE_COVER_FROM_ENEMY,
			"	Tasks"
			"	TASK_STOP_MOVING				0"
			"	TASK_WAIT						0.2"
			"	TASK_FIND_COVER_FROM_ENEMY		0"
			"	TASK_RUN_PATH					0"
			"	TASK_WAIT_FOR_MOVEMENT			0"
			"	TASK_REMEMBER					MEMORY:INCOVER"
			"	TASK_FACE_ENEMY					0"	
			"	"
			"	Interrupts"
			"	COND_NEW_ENEMY"
	)

	//=========================================================
	// Victory dance!
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_VICTORY_DANCE,
			"	Tasks"
			"	TASK_STOP_MOVING							0"
			"	TASK_SET_FAIL_SCHEDULE						SCHEDULE:SCHED_AGRUNT_THREAT_DISPLAY"
			"	TASK_WAIT									0.2"
			"	TASK_AGRUNT_GET_PATH_TO_ENEMY_CORPSE		0"
			"	TASK_WALK_PATH								0"
			"	TASK_WAIT_FOR_MOVEMENT						0"
			"	TASK_FACE_ENEMY								0"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_CROUCH"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_STAND"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_THREAT_DISPLAY"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_CROUCH"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_VICTORY_DANCE"
			"	TASK_PLAY_SEQUENCE							ACTIVITY:ACT_STAND"
			"	"
			"	Interrupts"
			"	COND_NEW_ENEMY"
			"	COND_LIGHT_DAMAGE"
			"	COND_HEAVY_DAMAGE"
	)

	//=========================================================
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_AGRUNT_THREAT_DISPLAY,
			"	Tasks"
			"	TASK_STOP_MOVING		0"
			"	TASK_FACE_ENEMY			0"
			"	TASK_PLAY_SEQUENCE		ACTIVITY:ACT_THREAT_DISPLAY"
			"	"
			"	Interrupts"
			"	COND_NEW_ENEMY"
			"	COND_LIGHT_DAMAGE"
			"	COND_HEAVY_DAMAGE"
			"	COND_HEAR_PLAYER"
			"	COND_HEAR_COMBAT"
			"	COND_HEAR_WORLD"
	)

AI_END_CUSTOM_NPC()
