//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: The various ammo types for HL1
//
//=============================================================================//

#include "cbase.h"
#include "player.h"
#include "weapons.h"
#include "gamerules.h"
#include "items.h"

// ========================================================================
//	>> Crossbow bolts
// ========================================================================
#define AMMO_CROSSBOWCLIP_GIVE	CROSSBOW_MAX_CLIP

class CCrossbowAmmo : public CItem
{
public:
	DECLARE_CLASS( CCrossbowAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_crossbow_clip.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_crossbow_clip.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		if (pPlayer->GiveAmmo(AMMO_CROSSBOWCLIP_GIVE, "bolts", BOLT_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_crossbow, CCrossbowAmmo);
PRECACHE_REGISTER(ammo_crossbow);

// ========================================================================
//	>> Egon ammo
// ========================================================================
#define AMMO_URANIUMBOX_GIVE	20

class CEgonAmmo : public CItem
{
public:
	DECLARE_CLASS( CEgonAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_chainammo.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_chainammo.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		if (pPlayer->GiveAmmo(AMMO_URANIUMBOX_GIVE, "uranium", URANIUM_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_egonclip, CEgonAmmo);

// ========================================================================
//	>> Gauss ammo
// ========================================================================
class CGaussAmmo : public CItem
{
public:
	DECLARE_CLASS( CGaussAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_gaussammo.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_gaussammo.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		if (pPlayer->GiveAmmo(AMMO_URANIUMBOX_GIVE, "uranium", URANIUM_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_gaussclip, CGaussAmmo);
PRECACHE_REGISTER( ammo_gaussclip );

// ========================================================================
//	>> Glock ammo
// ========================================================================
#define AMMO_GLOCKCLIP_GIVE		GLOCK_MAX_CLIP

class CGlockAmmo : public CItem
{
public:
	DECLARE_CLASS( CGlockAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_9mmclip.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_9mmclip.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		if (pPlayer->GiveAmmo(AMMO_GLOCKCLIP_GIVE, "9mm", _9MM_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_glockclip, CGlockAmmo);
LINK_ENTITY_TO_CLASS(ammo_9mmclip, CGlockAmmo);
PRECACHE_REGISTER(ammo_9mmclip);

// ========================================================================
//	>> MP5 ammo
// ========================================================================
#define AMMO_MP5CLIP_GIVE		MP5_MAX_CLIP

class CMP5AmmoClip : public CItem
{
public:
	DECLARE_CLASS( CMP5AmmoClip, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_9mmARclip.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_9mmARclip.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		int bResult = (pPlayer->GiveAmmo(AMMO_MP5CLIP_GIVE, "9mm", _9MM_MAX_CARRY) != -1);
		if (bResult)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
		}
		return bResult;
	}
};
LINK_ENTITY_TO_CLASS(ammo_mp5clip, CMP5AmmoClip);
LINK_ENTITY_TO_CLASS(ammo_9mmAR, CMP5AmmoClip);
PRECACHE_REGISTER(ammo_9mmAR);

// ========================================================================
//	>> MP5Chain (?) ammo
// ========================================================================
#define AMMO_CHAINBOX_GIVE		200

class CMP5Chainammo : public CItem
{
public:
	DECLARE_CLASS( CMP5Chainammo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_chainammo.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_chainammo.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		int bResult = (pPlayer->GiveAmmo(AMMO_CHAINBOX_GIVE, "9mm", _9MM_MAX_CARRY) != -1);
		if (bResult)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
		}
		return bResult;
	}
};
LINK_ENTITY_TO_CLASS(ammo_9mmbox, CMP5Chainammo);
PRECACHE_REGISTER(ammo_9mmbox);

// ========================================================================
//	>> MP5 grenades
// ========================================================================
#define AMMO_MP5GRENADE_GIVE	2

class CMP5AmmoGrenade : public CItem
{
public:
	DECLARE_CLASS( CMP5AmmoGrenade, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_ARgrenade.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_ARgrenade.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		int bResult = (pPlayer->GiveAmmo(AMMO_MP5GRENADE_GIVE, "ARgrenades", M203_GRENADE_MAX_CARRY) != -1);

		if (bResult)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
		}
		return bResult;
	}
};
LINK_ENTITY_TO_CLASS(ammo_mp5grenades, CMP5AmmoGrenade);
LINK_ENTITY_TO_CLASS(ammo_ARgrenades, CMP5AmmoGrenade);
PRECACHE_REGISTER(ammo_ARgrenades);

// ========================================================================
//	>> 357 ammo
// ========================================================================
#define AMMO_357BOX_GIVE		PYTHON_MAX_CLIP

class CPythonAmmo : public CItem
{
public:
	DECLARE_CLASS( CPythonAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_357ammobox.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_357ammobox.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		if (pPlayer->GiveAmmo(AMMO_357BOX_GIVE, "357", _357_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_357, CPythonAmmo);
PRECACHE_REGISTER(ammo_357);

// ========================================================================
//	>> RPG rockets
// ========================================================================
#define AMMO_RPGCLIP_GIVE		RPG_MAX_CLIP

class CRPGAmmo : public CItem
{
public:
	DECLARE_CLASS( CRPGAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_rpgammo.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_rpgammo.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		int iGive;

		if (g_pGameRules->IsMultiplayer())
		{
			// hand out more ammo per rocket in multiplayer.
			iGive = AMMO_RPGCLIP_GIVE * 2;
		}
		else
		{
			iGive = AMMO_RPGCLIP_GIVE;
		}

		if (pPlayer->GiveAmmo(iGive, "rockets", ROCKET_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_rpgclip, CRPGAmmo);
PRECACHE_REGISTER(ammo_rpgclip);

// ========================================================================
//	>> Shotgun ammo
// ========================================================================
#define AMMO_BUCKSHOTBOX_GIVE	12

class CShotgunAmmo : public CItem
{
public:
	DECLARE_CLASS( CShotgunAmmo, CItem );

	void Spawn(void)
	{
		Precache();
		SET_MODEL(ENT(pev), "models/w_shotbox.mdl");
		BaseClass::Spawn();
	}
	void Precache(void)
	{
		PRECACHE_MODEL("models/w_shotbox.mdl");
		PRECACHE_SOUND("items/9mmclip1.wav");
	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		if (pPlayer->GiveAmmo(AMMO_BUCKSHOTBOX_GIVE, "buckshot", BUCKSHOT_MAX_CARRY) != -1)
		{
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);
			return true;
		}
		return false;
	}
};
LINK_ENTITY_TO_CLASS(ammo_buckshot, CShotgunAmmo);
PRECACHE_REGISTER(ammo_buckshot);