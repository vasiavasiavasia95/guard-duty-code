//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:	Squad classes
//
//=============================================================================//

#ifndef AI_SQUAD_H
#define AI_SQUAD_H

#include "ai_memory.h"
#include "ai_squadslot.h"
#include "bitvec.h"

class CAI_Squad;

#define PER_ENEMY_SQUADSLOTS 1


//-----------------------------------------------------------------------------

DECLARE_POINTER_HANDLE(AISquadsIter_t);
DECLARE_POINTER_HANDLE(AISquadIter_t);

#define	MAX_SQUAD_MEMBERS	5

//-----------------------------------------------------------------------------
// CAI_SquadManager
//
// Purpose: Manages all the squads in the system
//
//-----------------------------------------------------------------------------

class CAI_SquadManager
{
public:
	CAI_SquadManager()
	{
		m_pSquads = NULL;
	}

	CAI_Squad *		GetFirstSquad( AISquadsIter_t *pIter );
	CAI_Squad *		GetNextSquad( AISquadsIter_t *pIter );
	int				NumSquads();

	CAI_Squad *		FindSquad( string_t squadName );	// Returns squad of the given name
	CAI_Squad *		CreateSquad( string_t squadName );	// Returns squad of the given name
	CAI_Squad *		FindCreateSquad( CBaseMonster *pMonster, string_t squadName );	// Returns squad of the given name

	void			DeleteSquad( CAI_Squad *pSquad );
	void			DeleteAllSquads(void);

private:

	CAI_Squad *		m_pSquads;										// A linked list of all squads

};

//-------------------------------------

extern CAI_SquadManager g_AI_SquadManager;

//-----------------------------------------------------------------------------

#ifdef PER_ENEMY_SQUADSLOTS

struct AISquadEnemyInfo_t
{
	EHANDLE 						hEnemy;
	CBitVec<MAX_SQUADSLOTS>	slots;									// What squad slots are filled?

	DECLARE_SIMPLE_DATADESC();
};

#endif

//-----------------------------------------------------------------------------
// CAI_Squad
//
// Purpose: Tracks enemies, squad slots, squad members
//
//-----------------------------------------------------------------------------

class CAI_Squad
{
public:

	const char *			GetName() const	{ return STRING(m_Name); }

	void					RemoveFromSquad( CBaseMonster *pMonster );

	CBaseMonster *			GetFirstMember( AISquadIter_t *pIter );
	CBaseMonster *			GetNextMember( AISquadIter_t *pIter );
	CBaseMonster *			GetAnyMember();
	int						NumMembers( void );

	void					SquadNewEnemy ( CBaseEntity *pEnemy );
	void					UpdateEnemyMemory( CBaseMonster *pUpdater, CBaseEntity *pEnemy, const Vector &position );

	bool 					OccupyStrategySlotRange( CBaseEntity *pEnemy, int slotIDStart, int slotIDEnd, int *pSlot );
	void 					VacateStrategySlot( CBaseEntity *pEnemy, int slot);
	
	CBaseMonster	*			SquadMemberInRange( const Vector &vecLocation, float flDist );
	CBaseMonster *			NearestSquadMember( CBaseMonster *pMember );
	int						GetVisibleSquadMembers( CBaseMonster *pMember );
	bool					SquadIsMember( CBaseEntity *pMember );
	bool					IsLeader( CBaseMonster *pLeader );
	CBaseMonster				*GetLeader( void );

	void					AddToSquad(CBaseMonster *pMonster);
	float					GetSquadSoundWaitTime() const		{ return m_flSquadSoundWaitTime; }
	void					SetSquadSoundWaitTime( float time ) { m_flSquadSoundWaitTime = time; }

private:
	void OccupySlot( CBaseEntity *pEnemy, int i );
	void VacateSlot( CBaseEntity *pEnemy, int i );
	bool IsSlotOccupied( CBaseEntity *pEnemy, int i ) const;

private:
	friend class CAI_SaveRestoreHandler;
	friend class CAI_SquadManager;

	CAI_Squad();
	CAI_Squad(string_t squadName);
	~CAI_Squad(void);

	CAI_Squad*				GetNext() { return m_pNextSquad; }

	void Init( string_t squadName );

	CAI_Squad *	m_pNextSquad;								// The next squad is list of all squads

	string_t	m_Name;
	EHANDLE		m_hSquadMember[MAX_SQUAD_MEMBERS];
	int			m_nNumMembers;

	float		m_flSquadSoundWaitTime;					// Time when I'm allowed to make another sound

	
#ifdef PER_ENEMY_SQUADSLOTS

	AISquadEnemyInfo_t *FindEnemyInfo( CBaseEntity *pEnemy );
	const AISquadEnemyInfo_t *FindEnemyInfo( CBaseEntity *pEnemy ) const	{ return const_cast<CAI_Squad *>(this)->FindEnemyInfo( pEnemy ); }

	AISquadEnemyInfo_t *			m_pLastFoundEnemyInfo; // Occupy/Vacate need to be reworked to not want this
	
	CUtlVector<AISquadEnemyInfo_t>	m_EnemyInfos;
	float							m_flEnemyInfoCleanupTime;

#else
	
	CBitString	m_squadSlotsUsed;							// What squad slots are filled?

#endif

	//---------------------------------
public:
	DECLARE_SIMPLE_DATADESC();
};

//-----------------------------------------------------------------------------
//
// Purpose: CAI_SquadManager inline functions
//
//-----------------------------------------------------------------------------

inline CAI_Squad *CAI_SquadManager::GetFirstSquad( AISquadsIter_t *pIter )
{
	*pIter = (AISquadsIter_t)m_pSquads;
	return m_pSquads;
}

//-------------------------------------

inline CAI_Squad *CAI_SquadManager::GetNextSquad( AISquadsIter_t *pIter )
{
	CAI_Squad *pSquad = (CAI_Squad *)*pIter;
	if ( pSquad )
		pSquad = pSquad->m_pNextSquad;
	*pIter = (AISquadsIter_t)pSquad;
	return pSquad;
}

//-------------------------------------
// Purpose: Returns squad of the given name or creates a new squad with the
//			given name if none exists and add pMonster to the list of members
//-------------------------------------

inline CAI_Squad *CAI_SquadManager::FindCreateSquad(CBaseMonster *pMonster, string_t squadName)
{
	CAI_Squad* pSquad = FindSquad( squadName );
	
	if ( !pSquad )
		pSquad = CreateSquad( squadName );
	
	pSquad->AddToSquad( pMonster );

	return pSquad;
}

//-----------------------------------------------------------------------------

inline CBaseMonster *CAI_Squad::GetFirstMember( AISquadIter_t *pIter )
{
	*pIter = (AISquadIter_t)0;
	if ( !m_nNumMembers )
		return NULL;
	return (CBaseMonster *)((CBaseEntity *)m_hSquadMember[0]);
}

//-------------------------------------

inline CBaseMonster *CAI_Squad::GetNextMember( AISquadIter_t *pIter )
{
	int &i = (int &)*pIter;
	if ( ++i >= m_nNumMembers )
		return NULL;
	return (CBaseMonster *)((CBaseEntity *)m_hSquadMember[i]);
}

//-------------------------------------

inline CBaseMonster *CAI_Squad::GetAnyMember()
{
	if ( m_nNumMembers )
		return ((CBaseMonster *)(CBaseEntity *)m_hSquadMember[RANDOM_LONG( 0, m_nNumMembers-1 )]);
	return NULL;
}

//-----------------------------------------------------------------------------

#endif // AI_SQUAD_H
