//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Base combat character with no AI
//
// $NoKeywords: $
//=============================================================================//

#ifndef BASECOMBATCHARACTER_H
#define BASECOMBATCHARACTER_H
#ifdef _WIN32
#pragma once
#endif

// ---------------------------
//  Hit Group standards
// ---------------------------
#define	HITGROUP_GENERIC	0
#define	HITGROUP_HEAD		1
#define	HITGROUP_CHEST		2
#define	HITGROUP_STOMACH	3
#define HITGROUP_LEFTARM	4	
#define HITGROUP_RIGHTARM	5
#define HITGROUP_LEFTLEG	6
#define HITGROUP_RIGHTLEG	7

// -------------------------------------
//  Capability Bits
// -------------------------------------

enum Capability_t 
{
	bits_CAP_DUCK					= 0x00000001, // crouch
	bits_CAP_JUMP					= 0x00000002, // jump/leap
	bits_CAP_STRAFE					= 0x00000004, // strafe ( walk/run sideways)
	bits_CAP_SQUAD					= 0x00000008, // can form squads
	bits_CAP_SWIM					= 0x00000010, // proficiently navigate in water
	bits_CAP_CLIMB					= 0x00000020, // climb ladders/ropes
	bits_CAP_USE					= 0x00000040, // open doors/push buttons/pull levers
	bits_CAP_HEAR					= 0x00000080, // can hear forced sounds
	bits_CAP_AUTO_DOORS				= 0x00000100, // can trigger auto doors
	bits_CAP_OPEN_DOORS				= 0x00000200, // can open manual doors
	bits_CAP_TURN_HEAD				= 0x00000400, // can turn head, always bone controller 0
	bits_CAP_RANGE_ATTACK1			= 0x00000800, // can do a range attack 1
	bits_CAP_RANGE_ATTACK2			= 0x00001000, // can do a range attack 2
	bits_CAP_MELEE_ATTACK1			= 0x00002000, // can do a melee attack 1
	bits_CAP_MELEE_ATTACK2			= 0x00004000, // can do a melee attack 2
	bits_CAP_FLY					= 0x00008000, // can fly, move all around
};

#define bits_CAP_DOORS_GROUP    (bits_CAP_USE | bits_CAP_AUTO_DOORS | bits_CAP_OPEN_DOORS)
#define bits_CAP_RANGE_ATTACK_GROUP	(bits_CAP_RANGE_ATTACK1 | bits_CAP_RANGE_ATTACK2)
#define bits_CAP_MELEE_ATTACK_GROUP	(bits_CAP_MELEE_ATTACK1 | bits_CAP_MELEE_ATTACK2)

// monster to monster relationship types
enum Disposition_t 
{
	D_ER,		// Undefined - error
	D_HT,		// Hate
	D_FR,		// Fear
	D_LI,		// Like
	D_NU		// Neutral
};

const int DEF_RELATIONSHIP_PRIORITY = INT_MIN;

struct Relationship_t
{
	EHANDLE			entity;			// Relationship to a particular entity
	Class_T			classType;		// Relationship to a class  CLASS_NONE = not class based (Def. in baseentity.h)
	Disposition_t	disposition;	// D_HT (Hate), D_FR (Fear), D_LI (Like), D_NT (Neutral)
	int				priority;		// Relative importance of this relationship (higher numbers mean more important)

	DECLARE_SIMPLE_DATADESC();
};

// people gib if their health is <= this at the time of death
#define	GIB_HEALTH_VALUE	-30

//-----------------------------------------------------------------------------
// Purpose: This should contain all of the combat entry points / functionality 
// that are common between monsters and players
//-----------------------------------------------------------------------------
class CBaseCombatCharacter : public CBaseAnimating
{
	DECLARE_CLASS( CBaseCombatCharacter, CBaseAnimating );

public:
	CBaseCombatCharacter( void );
	~CBaseCombatCharacter( void );

	DECLARE_DATADESC();

public:

	virtual int			TakeHealth( float flHealth, int bitsDamageType );

	virtual	bool		FVisible( CBaseEntity *pEntity, CBaseEntity **ppBlocker = NULL );
	virtual bool		FVisible( const Vector &vecTarget, CBaseEntity **ppBlocker = NULL )	{ return BaseClass::FVisible( vecTarget, ppBlocker ); }
	static void			ResetVisibilityCache( CBaseCombatCharacter *pBCC = NULL );

	virtual bool		FInViewCone( CBaseEntity *pEntity );	// see if pEntity is in character's view cone
	virtual bool		FInViewCone( const Vector &vecSpot );	// see if given location is in character's view cone

	virtual bool		FInAimCone( CBaseEntity *pEntity );
	virtual bool		FInAimCone( const Vector &vecSpot );

	virtual Vector		BodyAngles();
	virtual Vector		BodyDirection2D( void );
	virtual Vector		BodyDirection3D( void );
	virtual Vector		HeadDirection2D( void )	{ return BodyDirection2D( ); }; // No head motion so just return body dir
	virtual Vector		HeadDirection3D( void )	{ return BodyDirection2D( ); }; // No head motion so just return body dir

	virtual bool		FBecomeProne( void )	{ return false; };
	virtual void		BarnacleVictimBitten( CBaseEntity *pBarnacle )	{};
	virtual void		BarnacleVictimReleased( void )	{};

	virtual	Vector		GetGunPosition( void );		// gun position at current position/orientation

	virtual bool		AddPlayerItem( CBasePlayerWeapon *pItem ) { return false; }
	virtual bool		RemovePlayerItem( CBasePlayerWeapon *pItem ) { return false; }

	// -----------------------
	// Damage
	// -----------------------
	// Don't override this for characters, override the per-life-state versions below
	virtual int				TakeDamage( const CTakeDamageInfo &info );

	// Override these to control how your character takes damage in different states
	virtual int				TakeDamage_Alive( const CTakeDamageInfo &info );
	virtual int				TakeDamage_Dying( const CTakeDamageInfo &info );
	virtual int				TakeDamage_Dead( const CTakeDamageInfo &info );

	virtual int				BloodColor( void ) { return m_bloodColor; }
	virtual Activity		GetDeathActivity( void );

	virtual void			BecomeDead(void);
	virtual bool			CorpseGib( const CTakeDamageInfo &info );
	virtual void			CorpseFade( void );	// Called instead of GibMonster() when gibs are disabled
	virtual bool			HasHumanGibs( void );
	virtual bool			HasAlienGibs( void );
	virtual bool			ShouldGib( const CTakeDamageInfo &info );

	// Character killed (only fired once)
	virtual void			Event_Killed( const CTakeDamageInfo &info );

	// Exactly one of these happens immediately after killed (gibbed may happen later when the corpse gibs)
	// Character gibbed or faded out (violence controls) (only fired once)
	// returns true if gibs were spawned
	virtual bool			Event_Gibbed( const CTakeDamageInfo &info );

	// Character entered the dying state without being gibbed (only fired once)
	virtual void			Event_Dying( void );

	CBaseEntity*			CheckTraceHullAttack( float flDist, int iDamage, int iDmgType );
	CBaseEntity*			CheckTraceHullAttack( const Vector &vStart, const Vector &vEnd, int iDamage, int iDmgType );

	virtual CBaseCombatCharacter *MyCombatCharacterPointer( void ) { return this; }

	virtual Disposition_t	IRelationType( CBaseEntity *pTarget );
	virtual int				IRelationPriority( CBaseEntity *pTarget );

protected:
	Relationship_t			*FindEntityRelationship( CBaseEntity *pTarget );

public:

	// Relationships
	static void			AllocateDefaultRelationships( );
	static void			SetDefaultRelationship( Class_T nClass, Class_T nClassTarget,  Disposition_t nDisposition, int nPriority );
	void				AddEntityRelationship( CBaseEntity *pEntity, Disposition_t nDisposition, int nPriority );
	void				AddClassRelationship  ( Class_T nClass, Disposition_t nDisposition, int nPriority );


public:

	float				GetNextAttack() const { return m_flNextAttack; }
	void				SetNextAttack( float flWait ) { m_flNextAttack = flWait; }

protected:
	// returns the last body region that took damage
	int	LastHitGroup() const				{ return m_LastHitGroup; }
	void SetLastHitGroup( int nHitGroup )	{ m_LastHitGroup = nHitGroup; }

protected:
	int			m_bloodColor;			// color of blood particles

	// -------------------
	// combat ability data
	// -------------------
	float		m_flFieldOfView;		// cosine of field of view for this character
	Vector		m_HackedGunPos;			// HACK until we can query end of gun
	float		m_flNextAttack;			// cannot attack again until this time

private:

	// ---------------
	//  Relationships
	// ---------------
	static Relationship_t**			m_DefaultRelationship;
	CUtlVector<Relationship_t>		m_Relationship;						// Array of relationships

	// attack/damage
	int					m_LastHitGroup;		// the last body region that took damage

	friend class CCleanupDefaultRelationShips;
};

#endif // BASECOMBATCHARACTER_H