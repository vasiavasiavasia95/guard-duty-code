//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: The base class from which all game entities are derived.
//
//===========================================================================//
#ifndef BASEENTITY_H
#define BASEENTITY_H
#ifdef _WIN32
#pragma once
#endif

// entity capabilities
// These are caps bits to indicate what an object's capabilities (currently used for save/restore and level transitions)
#define		FCAP_MUST_SPAWN				0x00000001		// Spawn after restore
#define		FCAP_ACROSS_TRANSITION		0x00000002		// should transfer between transitions
// UNDONE: This will ignore transition volumes (trigger_transition), but not the PVS!!!
#define		FCAP_FORCE_TRANSITION		0x00000004		// ALWAYS goes across transitions

#define		FCAP_IMPULSE_USE			0x00000008		// can be used by the player
#define		FCAP_CONTINUOUS_USE			0x00000010		// can be used by the player
#define		FCAP_ONOFF_USE				0x00000020		// can be used by the player
#define		FCAP_DIRECTIONAL_USE		0x00000040		// Player sends +/- 1 when using (currently only tracktrains)
// NOTE: Normally +USE only works in direct line of sight.  Add these caps for additional searches
#define		FCAP_USE_ONGROUND			0x00000080
#define		FCAP_USE_IN_RADIUS			0x00000100

#define		FCAP_MASTER					0x10000000		// Can be used to "master" other entities (like multisource)
#define		FCAP_DONT_SAVE				0x80000000		// Don't save this

// For CLASSIFY
enum Class_T
{
	CLASS_NONE,
	CLASS_MACHINE,
	CLASS_PLAYER,
	CLASS_HUMAN_PASSIVE,
	CLASS_HUMAN_MILITARY,
	CLASS_ALIEN_MILITARY,
	CLASS_ALIEN_PASSIVE,
	CLASS_ALIEN_MONSTER,
	CLASS_ALIEN_PREY,
	CLASS_ALIEN_PREDATOR,
	CLASS_INSECT,
	CLASS_PLAYER_ALLY,
	CLASS_PLAYER_BIOWEAPON,		// hornets and snarks.launched by players
	CLASS_ALIEN_BIOWEAPON,		// hornets and snarks.launched by the alien menace
	CLASS_BARNACLE,				// special because no one pays attention to it, and it eats a wide cross-section of creatures.

	NUM_AI_CLASSES
};

// All monsters need this data
#define	DONT_BLEED				-1
#define	BLOOD_COLOR_RED			70
#define	BLOOD_COLOR_YELLOW		195
#define	BLOOD_COLOR_GREEN		54	// Vasia: Referenced from AZ. Also matches decompiled alpha code
#define	BLOOD_COLOR_PANTHER		145	// Blood color used for panther in the alpha

class CSave;
class CRestore;
class CBaseEntity;
class CBasePlayerWeapon;
class CBaseCombatCharacter;
class Vector;
class CBaseAnimating;
class CBasePlayer;
class CBaseMonster;

//-----------------------------------------------------------------------------

inline int InternalCheckDeclareClass( const char *pClassName, const char *pClassNameMatch, void *pTestPtr, void *pBasePtr )
{
	// This makes sure that casting from ThisClass to BaseClass works right. You'll get a compiler error if it doesn't
	// work at all, and you'll get a runtime error if you use multiple inheritance.
	Assert( pTestPtr == pBasePtr );
	
	// This is triggered by IMPLEMENT_SERVER_CLASS. It does DLLClassName::CheckDeclareClass( #DLLClassName ).
	// If they didn't do a DECLARE_CLASS in DLLClassName, then it'll be calling its base class's version
	// and the class names won't match.
	Assert( (void*)pClassName == (void*)pClassNameMatch );
	return 0;
}

template <typename T> 
inline int CheckDeclareClass_Access( T *, const char *pShouldBe )
{
	return T::CheckDeclareClass( pShouldBe );
}

#ifdef _DEBUG

	#define DECLARE_CLASS( className, baseClassName ) \
		typedef baseClassName BaseClass; \
		typedef className ThisClass; \
		template <typename T> friend int CheckDeclareClass_Access(T *, const char *pShouldBe); \
		static int CheckDeclareClass( const char *pShouldBe ) \
		{ \
			InternalCheckDeclareClass( pShouldBe, #className, (ThisClass*)0xFFFFF, (BaseClass*)(ThisClass*)0xFFFFF ); \
			return CheckDeclareClass_Access( (BaseClass *)NULL, #baseClassName ); \
		}

	#define DECLARE_CLASS_NOBASE( className ) \
		typedef className ThisClass; \
		template <typename T> friend int CheckDeclareClass_Access(T *, const char *pShouldBe); \
		static int CheckDeclareClass( const char *pShouldBe ) \
		{ \
			return InternalCheckDeclareClass( pShouldBe, #className, 0, 0 ); \
		}

#else

	#define DECLARE_CLASS( className, baseClassName ) \
		typedef baseClassName BaseClass; \
		typedef className ThisClass;

	#define DECLARE_CLASS_NOBASE( className )					typedef className ThisClass;

#endif

//-----------------------------------------------------------------------------

typedef enum { USE_OFF = 0, USE_ON = 1, USE_SET = 2, USE_TOGGLE = 3 } USE_TYPE;

typedef void (CBaseEntity::*BASEPTR)(void);
typedef void (CBaseEntity::*ENTITYFUNCPTR)(CBaseEntity *pOther );
typedef void (CBaseEntity::*USEPTR)( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );

// Things that toggle (buttons/triggers/doors) need this
enum TOGGLE_STATE
{
	TS_AT_TOP,
	TS_AT_BOTTOM,
	TS_GOING_UP,
	TS_GOING_DOWN
};

extern int	DispatchSpawn( edict_t *pent );
extern void DispatchKeyValue( edict_t *pentKeyvalue, KeyValueData *pkvd );
extern void DispatchTouch( edict_t *pentTouched, edict_t *pentOther );
extern void DispatchUse( edict_t *pentUsed, edict_t *pentOther );
extern void DispatchThink( edict_t *pent );
extern void DispatchBlocked( edict_t *pentBlocked, edict_t *pentOther );
extern void	DispatchSave( edict_t *pent, SAVERESTOREDATA *pSaveData );
extern int	DispatchRestore( edict_t *pent, SAVERESTOREDATA *pSaveData, int globalEntity );
extern void	DispatchObjectCollsionBox( edict_t *pent );
extern int	DispatchCollide( edict_t *pentTouched, edict_t *pentOther );
extern void OnFreeContainingEntity( edict_t *pent );

extern void BeginRestoreEntities( void );
extern void EndRestoreEntities( void );

inline void *GET_PRIVATE( edict_t *pent );

//
// Base Entity.  All entity types derive from this
//
class CBaseEntity 
{
public:
	DECLARE_CLASS_NOBASE( CBaseEntity );

	// Constructor.  Set engine to use C/C++ callback functions
	// pointers to engine data
	entvars_t *pev;		// Don't need to save/restore this pointer, the engine resets it

	// Constructor callback
	CBaseEntity( void ) {}

	// Destructor
	virtual ~CBaseEntity( void ) {}

	// data description
	DECLARE_DATADESC();

	// memory handling
    void *operator new( size_t stAllocateBlock, entvars_t *pev );
	void operator delete( void *pMem, entvars_t *pev );

	// path corners
	CBaseEntity			*m_pLink;// used for temporary link-list operations. 

	// non-virtual methods. Don't override these!
public:

	int			GetSpawnFlags( void ) const;
	void		AddSpawnFlags( int nFlags );
	void		RemoveSpawnFlags( int nFlags );
	void		ClearSpawnFlags( void );
	bool		HasSpawnFlags( int nFlags ) const;
	
public:

	// returns a pointer to the entities edict.
	inline edict_t			*edict( void ) { return ENT( pev ); }
	inline const edict_t	*edict( void ) const { return ENT( pev ); }
	inline EOFFSET			eoffset( )	{ return OFFSET( pev ); };
	inline int				entindex( ) { return ENTINDEX( edict() ); };

	// initialization functions
	virtual void	Spawn( void ) { return; }
	virtual void	Precache( void ) { return; }
	virtual void	KeyValue( KeyValueData* pkvd) { pkvd->fHandled = FALSE; }
	virtual int		ObjectCaps( void ) { return FCAP_ACROSS_TRANSITION; }
	virtual void	Activate( void ) {}

	bool					IsTransparent() const;
	
	// Setup the object->object collision box (pev->mins / pev->maxs is the object->world collision box)
	virtual void	SetObjectCollisionBox( void );

	// Debug Overlays
	const char	*GetDebugName( void );

	// Selects a random point in the bounds given the normalized 0-1 bounds 
	void	RandomPointInBounds( const Vector &vecNormalizedMins, const Vector &vecNormalizedMaxs, Vector *pPoint) const;

// Classify - returns the type of group (i.e, "houndeye", or "human military" so that monsters with different classnames
// still realize that they are teammates. (overridden for monsters that form groups)
	virtual Class_T Classify ( void ) { return CLASS_NONE; };
	virtual void DeathNotice ( CBaseEntity *pChild ) {}// monster maker children use this to tell the monster maker that they have died.

	// save/restore
	// only overload these if you have special data to serialize
	virtual int		Save( CSave &save );
	virtual int		Restore( CRestore &restore );

private:
	int SaveDataDescBlock( CSave &save, datamap_t *dmap );
	int RestoreDataDescBlock( CRestore &restore, datamap_t *dmap );
public:

	// handler to reset stuff before you are restored
	// NOTE: Always chain to base class when implementing this!
	virtual void OnSave();

	// handler to reset stuff after you are restored
	// called after all entities have been loaded from all affected levels
	// called before activate
	// NOTE: Always chain to base class when implementing this!
	virtual void OnRestore();

	virtual void	TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, TraceResult *ptr );
	virtual int		TakeDamage( const CTakeDamageInfo &info );
	virtual int		TakeHealth( float flHealth, int bitsDamageType );
	virtual void	Event_Killed( const CTakeDamageInfo &info );
	virtual int		BloodColor( void ) { return DONT_BLEED; }
	virtual void	TraceBleed( float flDamage, Vector vecDir, TraceResult *ptr, int bitsDamageType );
	virtual bool    IsTriggered( CBaseEntity *pActivator )	{ return true; }
	virtual CBaseMonster *MyMonsterPointer( void ) { return NULL;}
	virtual CBaseCombatCharacter *MyCombatCharacterPointer( void ) { return NULL; }
	virtual	int		GetToggleState( void ) { return TS_AT_TOP; }
	virtual void	AddPoints( int score, bool bAllowNegativeScore ) {}
	virtual void	AddPointsToTeam( int score, bool bAllowNegativeScore ) {}
	virtual int 	GiveAmmo( int iAmount, const char *szName, int iMax ) { return -1; };
	virtual float	GetDelay( void ) { return 0; }
	virtual int		IsMoving( void ) { return pev->velocity != vec3_origin; }
	virtual void	OverrideReset( void ) {}
	virtual int		DamageDecal( int bitsDamageType );

	// This is ONLY used by the node graph to test movement through a door
	virtual void	SetToggleState( int state ) {}
	virtual void    StartSneaking( void ) {}
	virtual void    StopSneaking( void ) {}
	virtual bool	OnControls( entvars_t *pev ) { return false; }
	virtual bool    IsSneaking( void ) { return false; }
	virtual bool	IsAlive( void ) { return (pev->deadflag == DEAD_NO) && pev->health > 0; }
	virtual bool	IsBSPModel( void ) { return pev->solid == SOLID_BSP || pev->movetype == MOVETYPE_PUSHSTEP; }
	virtual bool	ReflectGauss( void ) { return ( IsBSPModel() && !pev->takedamage ); }
	virtual bool	HasTarget( string_t targetname ) { return FStrEq(STRING(targetname), STRING(pev->targetname) ); }
	bool			IsInWorld( void ) const;
	virtual	bool	IsPlayer( void ) { return false; }
	virtual bool	IsNetClient( void ) { return false; }
	virtual const char *TeamID( void ) { return ""; }
	virtual bool	ShouldCollide( CBaseEntity *pOther ) { return true; }


//	virtual void	SetActivator( CBaseEntity *pActivator ) {}
	virtual CBaseEntity *GetNextTarget( void );
	
	// fundamental callbacks
	void (CBaseEntity ::*m_pfnThink)(void);
	void (CBaseEntity ::*m_pfnTouch)( CBaseEntity *pOther );
	void (CBaseEntity ::*m_pfnUse)( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	void (CBaseEntity ::*m_pfnBlocked)( CBaseEntity *pOther );

	virtual void Think( void ) { if (m_pfnThink) (this->*m_pfnThink)(); };
	virtual void Touch( CBaseEntity *pOther ) { if (m_pfnTouch) (this->*m_pfnTouch)( pOther ); };
	virtual void Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value ) 
	{ 
		if (m_pfnUse) 
			(this->*m_pfnUse)( pActivator, pCaller, useType, value );
	}
	virtual void Blocked( CBaseEntity *pOther ) { if (m_pfnBlocked) (this->*m_pfnBlocked)( pOther ); };

public:

	virtual void			UpdateOnRemove( void );

	// common member functions
	void EXPORT				SUB_Remove( void );
	void EXPORT				SUB_DoNothing( void );
	void EXPORT				SUB_StartFadeOut ( void );
	void EXPORT				SUB_FadeOut ( void );
	void EXPORT				SUB_Vanish( void );
	void EXPORT				SUB_CallUseToggle( void ) { this->Use( this, this, USE_TOGGLE, 0 ); }

	int						ShouldToggle( USE_TYPE useType, int currentState );

	virtual void FireBullets( int cShots, const Vector &vecSrc, const Vector &vecDirShooting, const Vector &vecSpread, float flDistance, int iBulletType, int iTracerFreq = 4, int iDamage = 0, CBaseEntity *pAttacker = NULL  );

	virtual CBaseEntity *Respawn( void ) { return NULL; }

	void SUB_UseTargets( CBaseEntity *pActivator, USE_TYPE useType, float value );
	// Do the bounding boxes of these two intersect?
	int		Intersects( CBaseEntity *pOther );
	void	MakeDormant( void );
	int		IsDormant( void );
	bool    IsLockedByMaster( void ) { return false; }

	static CBaseEntity *Instance( edict_t *pent )
	{ 
		if ( !pent )
			pent = ENT(0);
		CBaseEntity *pEnt = (CBaseEntity *)GET_PRIVATE(pent); 
		return pEnt; 
	}

	static CBaseEntity *Instance( entvars_t *pev ) { return Instance( ENT( pev ) ); }
	static CBaseEntity *Instance( int eoffset) { return Instance( ENT( eoffset) ); }

	CBaseMonster *GetMonsterPointer( entvars_t *pevMonster ) 
	{ 
		CBaseEntity *pEntity = Instance( pevMonster );
		if ( pEntity )
			return pEntity->MyMonsterPointer();
		return NULL;
	}
	CBaseMonster *GetMonsterPointer( edict_t *pentMonster ) 
	{ 
		CBaseEntity *pEntity = Instance( pentMonster );
		if ( pEntity )
			return pEntity->MyMonsterPointer();
		return NULL;
	}


	// Ugly code to lookup all functions to make sure they are exported when set.
#ifdef _DEBUG
	void FunctionCheck( void *pFunction, char *name );
	
	BASEPTR	ThinkSet( BASEPTR func, char *name ) 
	{ 
		m_pfnThink = func; 
		FunctionCheck( (void *)*((int *)((char *)this + ( offsetof(CBaseEntity,m_pfnThink)))), name ); 
		return func;
	}
	ENTITYFUNCPTR TouchSet( ENTITYFUNCPTR func, char *name ) 
	{ 
		m_pfnTouch = func; 
		FunctionCheck( (void *)*((int *)((char *)this + ( offsetof(CBaseEntity,m_pfnTouch)))), name ); 
		return func;
	}
	USEPTR	UseSet( USEPTR func, char *name ) 
	{ 
		m_pfnUse = func; 
		FunctionCheck( (void *)*((int *)((char *)this + ( offsetof(CBaseEntity,m_pfnUse)))), name ); 
		return func;
	}
	ENTITYFUNCPTR	BlockedSet( ENTITYFUNCPTR func, char *name ) 
	{ 
		m_pfnBlocked = func; 
		FunctionCheck( (void *)*((int *)((char *)this + ( offsetof(CBaseEntity,m_pfnBlocked)))), name ); 
		return func;
	}

#endif

	// virtual functions used by a few classes
	
	// used by monsters that are created by the MonsterMaker
	virtual	void UpdateOwner( void ) { return; };


	//
	static CBaseEntity *Create( const char *szName, const Vector &vecOrigin, const Vector &vecAngles, edict_t *pentOwner = NULL );

	virtual Vector Center( ) { return (pev->absmax + pev->absmin) * 0.5; }; // center point of entity
	virtual Vector EyePosition( ) { return pev->origin + pev->view_ofs; };			// position of eyes
	virtual Vector EarPosition( ) { return pev->origin + pev->view_ofs; };			// position of ears
	virtual Vector BodyTarget( const Vector &posSrc, bool bNoisy = true ) { return Center( ); };		// position to shoot at

	virtual int Illumination( ) { return GETENTITYILLUM( ENT( pev ) ); };

	virtual Vector	GetSmoothedVelocity( void );

	virtual	bool FVisible ( CBaseEntity *pEntity, CBaseEntity **ppBlocker = NULL );
	virtual	bool FVisible ( const Vector &vecOrigin, CBaseEntity **ppBlocker = NULL );

	virtual bool CanBeSeen() { return true; } // allows entities to be 'invisible' to monster senses.

	// overrideable rules if an entity should interpolate
	virtual	bool ShouldInterpolate( void ) { return false; };
};


// Ugly technique to override base member functions
// Normally it's illegal to cast a pointer to a member function of a derived class to a pointer to a 
// member function of a base class.  static_cast is a sleezy way around that problem.

#ifdef _DEBUG

#define SetThink( a ) ThinkSet( static_cast <void (CBaseEntity::*)(void)> (a), #a )
#define SetTouch( a ) TouchSet( static_cast <void (CBaseEntity::*)(CBaseEntity *)> (a), #a )
#define SetUse( a ) UseSet( static_cast <void (CBaseEntity::*)(	CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )> (a), #a )
#define SetBlocked( a ) BlockedSet( static_cast <void (CBaseEntity::*)(CBaseEntity *)> (a), #a )

#else

#define SetThink( a ) m_pfnThink = static_cast <void (CBaseEntity::*)(void)> (a)
#define SetTouch( a ) m_pfnTouch = static_cast <void (CBaseEntity::*)(CBaseEntity *)> (a)
#define SetUse( a ) m_pfnUse = static_cast <void (CBaseEntity::*)( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )> (a)
#define SetBlocked( a ) m_pfnBlocked = static_cast <void (CBaseEntity::*)(CBaseEntity *)> (a)

#endif

#define SetMoveDone( a ) m_pfnCallWhenMoveDone = static_cast <void (CBaseToggle::*)(void)> (a)

// handling entity/edict transforms
inline void *GET_PRIVATE( edict_t *pent )
{
	if ( pent )
		return pent->pvPrivateData;
	return NULL;
}

//-----------------------------------------------------------------------------
// Inline methods
//-----------------------------------------------------------------------------

inline int CBaseEntity::GetSpawnFlags( void ) const
{ 
	return pev->spawnflags; 
}

inline void CBaseEntity::AddSpawnFlags( int nFlags ) 
{ 
	pev->spawnflags |= nFlags;
}
inline void CBaseEntity::RemoveSpawnFlags( int nFlags ) 
{ 
	pev->spawnflags &= ~nFlags;
}

inline void CBaseEntity::ClearSpawnFlags( void ) 
{ 
	pev->spawnflags = 0;
}

inline bool CBaseEntity::HasSpawnFlags( int nFlags ) const
{ 
	return (pev->spawnflags & nFlags) != 0;
}

inline CBaseCombatCharacter *ToBaseCombatCharacter( CBaseEntity *pEntity )
{
	if ( !pEntity )
		return NULL;

	return pEntity->MyCombatCharacterPointer();
}

inline Vector CBaseEntity::GetSmoothedVelocity( void )
{
	return pev->velocity;
}

class CPointEntity : public CBaseEntity
{
	DECLARE_CLASS( CPointEntity, CBaseEntity );
public:
	void	Spawn( void );
	virtual int	ObjectCaps( void ) { return BaseClass::ObjectCaps() & ~FCAP_ACROSS_TRANSITION; }
private:
};

#endif // BASEENTITY_H