//=========================================================
// This is a skeleton file for use when creating a new 
// monster. Copy and rename this file for the new
// monster and add the copy to the build.
//
// Leave this file in the build until we ship! Allowing 
// this file to be rebuilt with the rest of the game ensures
// that it stays up to date with the rest of the monster code.
//
// Replace occurances of CNewMonster with the new monsters's
// classname. Don't forget the lower-case occurance in 
// LINK_ENTITY_TO_CLASS()
//
//
// ASSUMPTIONS MADE:
//
// You're making a character based on CBaseMonster. If this 
// is not true, make sure you replace all occurances
// of 'CBaseMonster' in this file with the appropriate 
// parent class.
//
// You're making a human-sized monster that walks.
//
//=========================================================

#include "cbase.h"
#include "ai_default.h"
#include "ai_task.h"
#include "ai_schedule.h"
#include "basemonster.h"

//=========================================================
// Monster's Anim Events Go Here
//=========================================================

//=========================================================
// Custom schedules
//=========================================================
enum
{
	SCHED_MYCUSTOMSCHEDULE = LAST_SHARED_SCHEDULE,
};

//=========================================================
// Custom tasks
//=========================================================
enum 
{
	TASK_MYCUSTOMTASK = LAST_SHARED_TASK,
};

//=========================================================
// Custom Conditions
//=========================================================
enum 
{
	COND_MYCUSTOMCONDITION = LAST_SHARED_CONDITION,
};

class CNewMonster : public CBaseMonster
{
	DECLARE_CLASS( CNewMonster, CBaseMonster );

public:
	void	Spawn( void );
	void	Precache( void );
	void	SetYawSpeed( void );
	Class_T	Classify ( void );
	void	HandleAnimEvent( animevent_t *pEvent );

	DECLARE_DATADESC();

	// This is a dummy field. In order to provide save/restore
	// code in this file, we must have at least one field
	// for the code to operate on. Delete this field when
	// you are ready to do your own save/restore for this
	// character.
	int		m_iDeleteThisField;

	DEFINE_CUSTOM_AI;
};
LINK_ENTITY_TO_CLASS( monster_newmonster, CNewMonster );
IMPLEMENT_CUSTOM_AI( monster_newmonster, CNewMonster );

//---------------------------------------------------------
// Save/Restore
//---------------------------------------------------------
BEGIN_DATADESC( CNewMonster )

	DEFINE_FIELD( CNewMonster, m_iDeleteThisField, FIELD_INTEGER ),

END_DATADESC()

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CNewMonster::Classify( void )
{
	return CLASS_NONE;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CNewMonster::SetYawSpeed ( void )
{
	int ys;

	switch ( m_Activity )
	{
	case ACT_IDLE:
	default:
		ys = 90;
	}

	pev->yaw_speed = ys;
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CNewMonster::HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case 0:
	default:
		BaseClass::HandleAnimEvent( pEvent );
		break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CNewMonster::Spawn()
{
	Precache( );

	SET_MODEL(ENT(pev), "models/mymodel.mdl");
	UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_RED;
	pev->health			= 20;
	pev->view_ofs		= Vector ( 0, 0, 0 );// position of the eyes relative to monster's origin.
	m_flFieldOfView		= 0.5;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;

	CapabilitiesClear();

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CNewMonster::Precache()
{
	PRECACHE_SOUND("mysound.wav");

	PRECACHE_MODEL("models/mymodel.mdl");
}	

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

//-----------------------------------------------------------------------------
// Purpose: Initialize the custom schedules
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CNewMonster::InitCustomSchedules(void)
{
	INIT_CUSTOM_AI(CNewMonster);

	ADD_CUSTOM_TASK(CNewMonster,		TASK_MYCUSTOMTASK);

	ADD_CUSTOM_SCHEDULE(CNewMonster,	SCHED_MYCUSTOMSCHEDULE);

	ADD_CUSTOM_CONDITION(CNewMonster,	COND_MYCUSTOMCONDITION);
}