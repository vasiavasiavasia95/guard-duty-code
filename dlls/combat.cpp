/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
/*

===== combat.cpp ========================================================

  functions dealing with damage infliction & death

*/

#include "cbase.h"
#include "basemonster.h"
#include "ai_memory.h"
#include "ai_squad.h"
#include "soundent.h"
#include "decals.h"
#include "animation.h"
#include "weapons.h"
#include "gib.h"

extern cvar_t sk_monster_head;
extern cvar_t sk_monster_chest;
extern cvar_t sk_monster_stomach;
extern cvar_t sk_monster_arm;
extern cvar_t sk_monster_leg;

extern cvar_t sk_12mm_bullet;
extern cvar_t sk_9mmAR_bullet;
extern cvar_t sk_9mm_bullet;

extern cvar_t sk_plr_9mm_bullet;
extern cvar_t sk_plr_357_bullet;
extern cvar_t sk_plr_9mmAR_bullet;
extern cvar_t sk_plr_buckshot;

extern DLL_GLOBAL Vector		g_vecAttackDir;
extern DLL_GLOBAL int			g_iSkillLevel;

extern Vector VecBModelOrigin( entvars_t* pevBModel );

#define MIN_CORPSE_FADE_TIME	10.0
#define	MIN_CORPSE_FADE_DIST	256.0
#define	MAX_CORPSE_FADE_DIST	1500.0

bool CBaseMonster::CorpseGib( const CTakeDamageInfo &info )
{
	bool gibbed = BaseClass::CorpseGib( info );

	if ( gibbed )
	{
		// don't remove players!
		UTIL_Remove( this );
	}
	else
	{
		CorpseFade();
	}

	return gibbed;
}

bool CBaseMonster::Event_Gibbed( const CTakeDamageInfo &info )
{
	bool gibbed = BaseClass::Event_Gibbed( info );

	FCheckAITrigger();

	return gibbed;
}

//=========================================================
// GetSmallFlinchActivity - determines the best type of flinch
// anim to play.
//=========================================================
Activity CBaseMonster::GetFlinchActivity ( bool bHeavyDamage )
{
	Activity	flinchActivity;
	
	switch ( LastHitGroup() )
	{
		// pick a region-specific flinch
	case HITGROUP_HEAD:
		flinchActivity = ACT_FLINCH_HEAD;
		break;
	case HITGROUP_STOMACH:
		flinchActivity = ACT_FLINCH_STOMACH;
		break;
	case HITGROUP_LEFTARM:
		flinchActivity = ACT_FLINCH_LEFTARM;
		break;
	case HITGROUP_RIGHTARM:
		flinchActivity = ACT_FLINCH_RIGHTARM;
		break;
	case HITGROUP_LEFTLEG:
		flinchActivity = ACT_FLINCH_LEFTLEG;
		break;
	case HITGROUP_RIGHTLEG:
		flinchActivity = ACT_FLINCH_RIGHTLEG;
		break;
	case HITGROUP_GENERIC:
	default:
		// just get a generic flinch.
		if ( bHeavyDamage )
		{
			flinchActivity = ACT_BIG_FLINCH;
		}
		else
		{
			flinchActivity = ACT_SMALL_FLINCH;
		}
		break;
	}

	// do we have a sequence for the ideal activity?
	if ( LookupActivity ( flinchActivity ) == ACTIVITY_NOT_AVAILABLE )
	{
		if ( bHeavyDamage )
		{
			flinchActivity = ACT_BIG_FLINCH;

			// If we fail at finding a big flinch, resort to a small one
			if ( LookupActivity ( flinchActivity ) == ACTIVITY_NOT_AVAILABLE )
			{
				flinchActivity = ACT_SMALL_FLINCH;
			}
		}
		else
		{
			flinchActivity = ACT_SMALL_FLINCH;
		}
	}

	return flinchActivity;
}

/*
============
Event_Killed
============
*/
void CBaseMonster::Event_Killed( const CTakeDamageInfo &info )
{
	if ( ( pev->flags & FL_MONSTER ) && ( ShouldGib( info ) == false ) )
	{
		SetTouch( NULL );
		BecomeDead();
	}

	BaseClass::Event_Killed( info );

	// Make sure this condition is fired too (TakeDamage breaks out before this happens on death)
	SetCondition( COND_LIGHT_DAMAGE );

	m_IdealMonsterState = MONSTERSTATE_DEAD;

	// Vacate any strategy slot I might have
	VacateStrategySlot();

	// Remove from squad if in one
	if (m_pSquad)
	{
		// If I'm in idle it means that I didn't see who killed me
		// and my squad is still in idle state. Tell squad we have
		// an enemy to wake them up and put the enemy position at
		// my death position
		if ( m_MonsterState == MONSTERSTATE_IDLE && info.GetAttacker() )
		{
			UpdateEnemyMemory( info.GetAttacker(), pev->origin );
		}

		// Remove from squad
		m_pSquad->RemoveFromSquad(this);
		m_pSquad = NULL;
	}
}

//
// fade out - slowly fades a entity out, then removes it.
//
// DON'T USE ME FOR GIBS AND STUFF IN MULTIPLAYER! 
// SET A FUTURE THINK AND A RENDERMODE!!
void CBaseEntity::SUB_StartFadeOut ( void )
{
	if (pev->rendermode == kRenderNormal)
	{
		pev->renderamt = 255;
		pev->rendermode = kRenderTransTexture;
	}

	pev->solid = SOLID_NOT;
	pev->avelocity = vec3_origin;

	pev->nextthink = gpGlobals->time + MIN_CORPSE_FADE_TIME;
	SetThink ( &CBaseEntity::SUB_FadeOut );
}

/*
============
SUB_Vanish

Vanish when players aren't looking
============
*/
void CBaseEntity::SUB_Vanish( void )
{
	//Always think again next frame
	pev->nextthink = gpGlobals->time + 0.1f;

	CBaseEntity *pPlayer;

	//Get all players
	for ( int i = 1; i <= gpGlobals->maxClients; i++ )
	{
		//Get the next client
		if ( ( pPlayer = UTIL_PlayerByIndex( i ) ) != NULL )
		{
			Vector corpseDir = (pev->origin - pPlayer->Center() );

			float flDistSqr = corpseDir.LengthSqr();
			//If the player is close enough, don't fade out
			if ( flDistSqr < (MIN_CORPSE_FADE_DIST*MIN_CORPSE_FADE_DIST) )
				return;

			// If the player's far enough away, we don't care about looking at it
			if ( flDistSqr < (MAX_CORPSE_FADE_DIST*MAX_CORPSE_FADE_DIST) )
			{
				VectorNormalize( corpseDir );

				Vector	plForward;
				AngleVectors( pPlayer->pev->v_angle, &plForward, NULL, NULL );

				float dot =	DotProduct( plForward, corpseDir );

				if ( dot > 0.0f )
					return;
			}
		}
	}

	//If we're here, then we can vanish safely
	SetThink( &CBaseEntity::SUB_Remove );
}

void CBaseEntity::SUB_FadeOut ( void  )
{
	if ( pev->renderamt > 7 )
	{
		pev->renderamt -= 7;
		pev->nextthink = gpGlobals->time + 0.1;
	}
	else 
	{
		pev->renderamt = 0;
		pev->nextthink = gpGlobals->time + 0.2;
		SetThink ( &CBaseEntity::SUB_Remove );
	}
}

int CBaseMonster::TakeDamage_Alive( const CTakeDamageInfo &info )
{
	PainSound();// "Ouch!"
	Forget( bits_MEMORY_INCOVER );

	// HACKHACK Don't kill monsters in a script.  Let them break their scripts first
	if ( m_MonsterState == MONSTERSTATE_SCRIPT )
	{
		SetCondition( COND_LIGHT_DAMAGE );
		return 0;
	}
	
	if ( !BaseClass::TakeDamage_Alive( info ) )
		return 0;

	// react to the damage (get mad)
	if ( (pev->flags & FL_MONSTER) && info.GetAttacker() )
	{
		// ----------------------------------------------------------------
		// If the attacker was a monster or client update my position memory
		// -----------------------------------------------------------------
		if ( info.GetAttacker()->pev->flags & (FL_MONSTER | FL_CLIENT) )
		{
			// ------------------------------------------------------------------
			//				DO NOT CHANGE THIS CODE W/O CONSULTING
			// Only update information about my attacker if I don't see my attacker
			// ------------------------------------------------------------------
			if ( !FInViewCone( info.GetAttacker() ) || !FVisible( info.GetAttacker() ) )
			{
				// -------------------------------------------------------------
				//  If I have an inflictor (enemy / grenade) update memory with
				//  position of inflictor, otherwise update with an position
				//  estimate for where the attack came from
				// ------------------------------------------------------
				Vector vAttackPos;
				if (info.GetInflictor())
				{
					vAttackPos = info.GetInflictor()->pev->origin;
				}
				else
				{
					vAttackPos = (pev->origin + ( g_vecAttackDir * 64 ));
				}

				// ----------------------------------------------------------------
				//  If I already have an enemy, assume that the attack
				//  came from the enemy and update my enemy's position
				//  unless I already know about the attacker or I can see my enemy
				// ----------------------------------------------------------------
				if ( GetEnemy() != NULL							&&
					!GetEnemies()->HasMemory( info.GetAttacker() )			&&
					!HasCondition(COND_SEE_ENEMY)	)
				{
					UpdateEnemyMemory( GetEnemy(), vAttackPos );
				}
				// ----------------------------------------------------------------
				//  If I already know about this enemy, update his position
				// ----------------------------------------------------------------
				else if ( GetEnemies()->HasMemory( info.GetAttacker() ) )
				{
					UpdateEnemyMemory( info.GetAttacker(), vAttackPos );
				}
				// -----------------------------------------------------------------
				//  Otherwise just not the position, but don't add enemy to my list
				// -----------------------------------------------------------------
				else
				{
					UpdateEnemyMemory( NULL, vAttackPos );
				}
			}

			// add pain to the conditions 
			if ( IsLightDamage(info.GetDamage(),info.GetDamageType()) )
			{
				SetCondition( COND_LIGHT_DAMAGE );
			}

			if ( IsHeavyDamage(info.GetDamage(),info.GetDamageType()) )
			{
				SetCondition( COND_HEAVY_DAMAGE );
			}

			ForceGatherConditions();
		}
	}

	return 1;
}

//=========================================================
// TakeDamage_Dead - takedamage function called when a monster's
// corpse is damaged.
//=========================================================
int CBaseMonster::TakeDamage_Dead( const CTakeDamageInfo &info )
{
	Vector			vecDir;

	// grab the vector of the incoming attack. ( pretend that the inflictor is a little lower than it really is, so the body will tend to fly upward a bit).
	vecDir = vec3_origin;
	if ( info.GetInflictor() )
	{
		vecDir = (info.GetInflictor()->Center() - Vector ( 0, 0, 10 ) - Center() ).Normalize();
		vecDir = g_vecAttackDir = vecDir.Normalize();
	}

#if 0// turn this back on when the bounding box issues are resolved.

	pev->flags &= ~FL_ONGROUND;
	pev->origin.z += 1;
	
	// let the damage scoot the corpse around a bit.
	if ( info.GetInflictor() && (info.GetAttacker()->pev->solid != SOLID_TRIGGER) )
	{
		pev->velocity = pev->velocity + vecDir * -DamageForce( flDamage );
	}

#endif

	// kill the corpse if enough damage was done to destroy the corpse and the damage is of a type that is allowed to destroy the corpse.
	if ( info.GetDamageType() & DMG_GIB_CORPSE )
	{
		// Accumulate corpse gibbing damage, so you can gib with multiple hits
		pev->health -= info.GetDamage() * 0.1;
	}
	
	return 1;
}

bool CBaseMonster::IsLightDamage( float flDamage, int bitsDamageType )
{
	// ALL nonzero damage is light damage! Mask off COND_LIGHT_DAMAGE if you want to ignore light damage.
	return flDamage >  0;
}

bool CBaseMonster::IsHeavyDamage( float flDamage, int bitsDamageType )
{
	return (flDamage > 20);
}

//
// RadiusDamage - this entity is exploding, or otherwise needs to inflict damage upon entities within a certain range.
// 
// only damage ents that can clearly be seen by the explosion!

	
void RadiusDamage( const CTakeDamageInfo &info, const Vector &vecSrcIn, float flRadius, int iClassIgnore )
{
	CBaseEntity *pEntity = NULL;
	TraceResult	tr;
	float		flAdjustedDamage, falloff;
	Vector		vecSpot;

	Vector vecSrc = vecSrcIn;

	if ( flRadius )
		falloff = info.GetDamage() / flRadius;
	else
		falloff = 1.0;

	int bInWater = (UTIL_PointContents ( vecSrc ) == CONTENTS_WATER);

	vecSrc.z += 1;// in case grenade is lying on the ground

	//if ( !pevAttacker )
		//pevAttacker = pevInflictor;

	// iterate on all entities in the vicinity.
	while ((pEntity = UTIL_FindEntityInSphere( pEntity, vecSrc, flRadius )) != NULL)
	{
		if ( pEntity->pev->takedamage != DAMAGE_NO )
		{
			// UNDONE: this should check a damage mask, not an ignore
			if ( iClassIgnore != CLASS_NONE && pEntity->Classify() == iClassIgnore )
			{// houndeyes don't hurt other houndeyes with their attack
				continue;
			}

			// blast's don't tavel into or out of water
			if (bInWater && pEntity->pev->waterlevel == 0)
				continue;
			if (!bInWater && pEntity->pev->waterlevel == 3)
				continue;

			vecSpot = pEntity->BodyTarget( vecSrc );
			
			UTIL_TraceLine ( vecSrc, vecSpot, dont_ignore_monsters, info.GetInflictor()->edict(), &tr );

			if ( tr.flFraction == 1.0 || tr.pHit == pEntity->edict() )
			{// the explosion can 'see' this entity, so hurt them!
				if (tr.fStartSolid)
				{
					// if we're stuck inside them, fixup the position and distance
					tr.vecEndPos = vecSrc;
					tr.flFraction = 0.0;
				}
				
				// decrease damage for an ent that's farther from the bomb.
				flAdjustedDamage = ( vecSrc - tr.vecEndPos ).Length() * falloff;
				flAdjustedDamage = info.GetDamage() - flAdjustedDamage;
			
				if ( flAdjustedDamage < 0 )
				{
					flAdjustedDamage = 0;
				}

				if ( flAdjustedDamage > 0 )
				{
					CTakeDamageInfo adjustedInfo = info;
					adjustedInfo.SetDamage( flAdjustedDamage );

					Vector dir = tr.vecEndPos - vecSrc;
					VectorNormalize( dir );

					// ALERT( at_console, "hit %s\n", STRING( pEntity->pev->classname ) );
					if (tr.flFraction != 1.0)
					{
						ClearMultiDamage( );
						pEntity->TraceAttack( adjustedInfo, dir, &tr );
						ApplyMultiDamage();
					}
					else
					{
						pEntity->TakeDamage( adjustedInfo );
					}
				}
			}
		}
	}
}


void CBaseMonster::DoRadiusDamage( const CTakeDamageInfo &info, int iClassIgnore )
{
	::RadiusDamage( info, pev->origin, info.GetDamage() * 2.5, iClassIgnore );
}


void CBaseMonster::DoRadiusDamage( const CTakeDamageInfo &info, const Vector &vecSrc, int iClassIgnore )
{
	::RadiusDamage( info, vecSrc, info.GetDamage() * 2.5, iClassIgnore );
}

/*
================
TraceAttack
================
*/
void CBaseEntity::TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, TraceResult *ptr )
{
	Vector vecOrigin = ptr->vecEndPos - vecDir * 4;

	if ( pev->takedamage )
	{
		AddMultiDamage( info, this );

		int blood = BloodColor();
		
		if ( blood != DONT_BLEED )
		{
			SpawnBlood( vecOrigin, blood, info.GetDamage() );// a little surface blood.
			TraceBleed( info.GetDamage(), vecDir, ptr, info.GetDamageType() );
		}
	}
}


/*
//=========================================================
// TraceAttack
//=========================================================
void CBaseMonster::TraceAttack(entvars_t *pevAttacker, float flDamage, Vector vecDir, TraceResult *ptr, int bitsDamageType)
{
	Vector vecOrigin = ptr->vecEndPos - vecDir * 4;

	ALERT ( at_console, "%d\n", ptr->iHitgroup );


	if ( pev->takedamage )
	{
		AddMultiDamage( pevAttacker, this, flDamage, bitsDamageType );

		int blood = BloodColor();
		
		if ( blood != DONT_BLEED )
		{
			SpawnBlood(vecOrigin, blood, flDamage);// a little surface blood.
		}
	}
}
*/

//=========================================================
// TraceAttack
//=========================================================
void CBaseMonster::TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, TraceResult *ptr )
{
	if ( pev->takedamage )
	{
		CTakeDamageInfo subInfo = info;

		SetLastHitGroup( ptr->iHitgroup );

		switch ( ptr->iHitgroup )
		{
		case HITGROUP_GENERIC:
			break;
		case HITGROUP_HEAD:
			subInfo.ScaleDamage( sk_monster_head.value );
			break;
		case HITGROUP_CHEST:
			subInfo.ScaleDamage( sk_monster_chest.value );
			break;
		case HITGROUP_STOMACH:
			subInfo.ScaleDamage( sk_monster_stomach.value );
			break;
		case HITGROUP_LEFTARM:
		case HITGROUP_RIGHTARM:
			subInfo.ScaleDamage( sk_monster_arm.value );
			break;
		case HITGROUP_LEFTLEG:
		case HITGROUP_RIGHTLEG:
			subInfo.ScaleDamage( sk_monster_leg.value );
			break;
		default:
			break;
		}

		Vector vecOrigin = ptr->vecEndPos - vecDir * 4;

		Vector vecBloodOffset = vecDir * 8.0 + ptr->vecEndPos;

		// Vasia: Alpha calls this before creating the blood effect in order to make sure gMultiDamage.amount
		// is always calculated
		subInfo.SetInflictor( info.GetAttacker() );
		AddMultiDamage( subInfo, this );

		// Vasia: Alpha always spawns blood
		//SpawnBlood( vecOrigin2, BloodColor(), flDamage );// a little surface blood.

		if ( pev->health <= g_MultiDamage.GetDamage() )
		{ 
			// all values are taken from the alpha code
			UTIL_BloodStream( vecBloodOffset, UTIL_RandomBloodVector(), BloodColor(), RANDOM_LONG( 80, 150 ) );
		}
		else
		{
			SpawnBlood( vecOrigin, BloodColor(), subInfo.GetDamage() );// a little surface blood.
		}

		TraceBleed( subInfo.GetDamage(), vecDir, ptr, subInfo.GetDamageType() );
	}
}

//---------------------------------------------------------
// Caches off a shot direction and allows you to perform
// various operations on it without having to recalculate
// vecRight and vecUp each time. 
//---------------------------------------------------------
class CShotManipulator
{
public:
	CShotManipulator( const Vector &vecForward )
	{
		m_vecShotDirection = vecForward;
		VectorVectors( m_vecShotDirection, m_vecRight, m_vecUp );
	};

	const Vector &ApplySpread( const Vector &vecSpread );

	const Vector &GetShotDirection()	{ return m_vecShotDirection; }
	const Vector &GetResult()			{ return m_vecResult; }
	const Vector &GetRightVector()		{ return m_vecRight; }
	const Vector &GetUpVector()			{ return m_vecUp;}

private:
	Vector m_vecShotDirection;
	Vector m_vecRight;
	Vector m_vecUp;
	Vector m_vecResult;
};

//---------------------------------------------------------
// Take a vector (direction) and another vector (spread) 
// and modify the direction to point somewhere within the 
// spread. This used to live inside FireBullets.
//---------------------------------------------------------
const Vector &CShotManipulator::ApplySpread( const Vector &vecSpread )
{
	// get circular gaussian spread
	float x, y, z;

	do
	{
		x = RANDOM_FLOAT(-0.5,0.5) + RANDOM_FLOAT(-0.5,0.5);
		y = RANDOM_FLOAT(-0.5,0.5) + RANDOM_FLOAT(-0.5,0.5);
		z = x*x+y*y;
	} while (z > 1);

	m_vecResult = m_vecShotDirection + x * vecSpread.x * m_vecRight + y * vecSpread.y * m_vecUp;

	return m_vecResult;
}

/*
================
FireBullets

Go to the trouble of combining multiple pellets into a single damage call.
================
*/
void CBaseEntity::FireBullets(int cShots, const Vector &vecSrc, 
	const Vector &vecDirShooting, const Vector &vecSpread, float flDistance, 
	int iBulletType, int iTracerFreq, int iDamage, CBaseEntity *pAttacker)
{
	static int tracerCount;
	bool tracer;
	TraceResult tr;
	Vector vecRight = gpGlobals->v_right;
	Vector vecUp = gpGlobals->v_up;

	if ( pAttacker == NULL )
	{
		pAttacker = this;  // the default attacker is ourselves
	}

	ClearMultiDamage();
	g_MultiDamage.SetDamageType( DMG_BULLET | DMG_NEVERGIB );

	//-----------------------------------------------------
	// Set up our shot manipulator.
	//-----------------------------------------------------
	CShotManipulator Manipulator( vecDirShooting );

	for (int iShot = 1; iShot <= cShots; iShot++)
	{
		Vector vecDir;
		Vector vecEnd;

		vecDir = Manipulator.ApplySpread( vecSpread );

		vecEnd = vecSrc + vecDir * flDistance;

		UTIL_TraceLine(vecSrc, vecEnd, dont_ignore_monsters, ENT(pev)/*pentIgnore*/, &tr);

		tracer = false;
		if (iTracerFreq != 0 && (tracerCount++ % iTracerFreq) == 0)
		{
			Vector vecTracerSrc;

			if (IsPlayer())
			{// adjust tracer position for player
				vecTracerSrc = vecSrc + Vector(0, 0, -4) + gpGlobals->v_right * 2 + gpGlobals->v_forward * 16;
			}
			else
			{
				vecTracerSrc = vecSrc;
			}

			if ( iTracerFreq != 1 )		// guns that always trace also always decal
			{
				tracer = true;
			}

			switch (iBulletType)
			{
			case BULLET_PLAYER_MP5:
			case BULLET_MONSTER_MP5:
			case BULLET_MONSTER_9MM:
			case BULLET_MONSTER_12MM:
			default:
				MESSAGE_BEGIN(MSG_PAS, SVC_TEMPENTITY, vecTracerSrc);
				WRITE_BYTE(TE_TRACER);
				WRITE_COORD(vecTracerSrc.x);
				WRITE_COORD(vecTracerSrc.y);
				WRITE_COORD(vecTracerSrc.z);
				WRITE_COORD(tr.vecEndPos.x);
				WRITE_COORD(tr.vecEndPos.y);
				WRITE_COORD(tr.vecEndPos.z);
				MESSAGE_END();

				break;
			}
		}
		// do damage, paint decals
		if (tr.flFraction != 1.0)
		{
			CBaseEntity *pEntity = CBaseEntity::Instance(tr.pHit);

			if (iDamage)
			{
				// Damage specified by function parameter
				CTakeDamageInfo dmgInfo( this, pAttacker, iDamage, DMG_BULLET | ((iDamage > 16) ? DMG_ALWAYSGIB : DMG_NEVERGIB ) );
				pEntity->TraceAttack( dmgInfo, vecDir, &tr );

				TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
				DecalGunshot(&tr, iBulletType);
			}
			else
			{
				switch (iBulletType)
				{
				default:
				case BULLET_PLAYER_9MM:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_plr_9mm_bullet.value, DMG_BULLET ), vecDir, &tr );
					if (!tracer)
					{
						TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
						DecalGunshot(&tr, iBulletType);
					}
					break;

				case BULLET_PLAYER_MP5:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_plr_9mmAR_bullet.value, DMG_BULLET ), vecDir, &tr );
					if (!tracer)
					{
						TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
						DecalGunshot(&tr, iBulletType);
					}
					break;

				case BULLET_PLAYER_BUCKSHOT:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_plr_buckshot.value, DMG_BULLET ), vecDir, &tr );
					// make distance based!
					if (!tracer)
					{
						// TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
						DecalGunshot(&tr, iBulletType);
					}
					break;

				case BULLET_PLAYER_357:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_plr_357_bullet.value, DMG_BULLET ), vecDir, &tr );
					if (!tracer)
					{
						TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
						DecalGunshot(&tr, iBulletType);
					}
					break;

				case BULLET_MONSTER_9MM:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_9mm_bullet.value, DMG_BULLET ), vecDir, &tr );

					TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
					DecalGunshot(&tr, iBulletType);

					break;

				case BULLET_MONSTER_MP5:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_9mmAR_bullet.value, DMG_BULLET ), vecDir, &tr );

					TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
					DecalGunshot(&tr, iBulletType);

					break;

				case BULLET_MONSTER_12MM:
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, sk_12mm_bullet.value, DMG_BULLET ), vecDir, &tr );
					if (!tracer)
					{
						TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
						DecalGunshot(&tr, iBulletType);
					}
					break;

				case BULLET_NONE: // FIX 
					pEntity->TraceAttack( CTakeDamageInfo( this, pAttacker, 50, DMG_CLUB ), vecDir, &tr );
					TEXTURETYPE_PlaySound(&tr, vecSrc, vecEnd, iBulletType);
					// only decal glass
					if (!FNullEnt(tr.pHit) && VARS(tr.pHit)->rendermode != 0)
					{
						UTIL_DecalTrace(&tr, DECAL_GLASSBREAK1 + RANDOM_LONG(0, 2));
					}

					break;
				}
			}
		}
		// make bullet trails
		UTIL_BubbleTrail(vecSrc, tr.vecEndPos, (flDistance * tr.flFraction) / 64.0);
	}
	
	ApplyMultiDamage();
}

void CBaseEntity::TraceBleed( float flDamage, Vector vecDir, TraceResult *ptr, int bitsDamageType )
{
	if (BloodColor() == DONT_BLEED)
		return;
	
	if ( (int)flDamage == 0 )
		return;

	if (! (bitsDamageType & (DMG_CRUSH | DMG_BULLET | DMG_SLASH | DMG_BLAST | DMG_CLUB | DMG_MORTAR)))
		return;
	
	// make blood decal on the wall! 
	TraceResult Bloodtr;
	Vector vecTraceDir; 
	float flNoise;
	int cCount;
	int i;

/*
	if ( !IsAlive() )
	{
		// dealing with a dead monster. 
		if ( pev->max_health <= 0 )
		{
			// no blood decal for a monster that has already decalled its limit.
			return; 
		}
		else
		{
			pev->max_health--;
		}
	}
*/

	if (flDamage < 10)
	{
		flNoise = 0.1;
		cCount = 1;
	}
	else if (flDamage < 25)
	{
		flNoise = 0.2;
		cCount = 2;
	}
	else
	{
		flNoise = 0.3;
		cCount = 4;
	}

	for ( i = 0 ; i < cCount ; i++ )
	{
		vecTraceDir = vecDir * -1;// trace in the opposite direction the shot came from (the direction the shot is going)

		vecTraceDir.x += RANDOM_FLOAT( -flNoise, flNoise );
		vecTraceDir.y += RANDOM_FLOAT( -flNoise, flNoise );
		vecTraceDir.z += RANDOM_FLOAT( -flNoise, flNoise );

		UTIL_TraceLine( ptr->vecEndPos, ptr->vecEndPos + vecTraceDir * -172, ignore_monsters, ENT(pev), &Bloodtr);

		if ( Bloodtr.flFraction != 1.0 )
		{
			UTIL_BloodDecalTrace( &Bloodtr, BloodColor() );
		}
	}
}

//=========================================================
//=========================================================
void CBaseMonster::MakeDamageBloodDecal ( int cCount, float flNoise, TraceResult *ptr, const Vector &vecDir )
{
	// make blood decal on the wall! 
	TraceResult Bloodtr;
	Vector vecTraceDir; 
	int i;

	if ( !IsAlive() )
	{
		// dealing with a dead monster. 
		if ( pev->max_health <= 0 )
		{
			// no blood decal for a monster that has already decalled its limit.
			return; 
		}
		else
		{
			pev->max_health--;
		}
	}

	for ( i = 0 ; i < cCount ; i++ )
	{
		vecTraceDir = vecDir;

		vecTraceDir.x += RANDOM_FLOAT( -flNoise, flNoise );
		vecTraceDir.y += RANDOM_FLOAT( -flNoise, flNoise );
		vecTraceDir.z += RANDOM_FLOAT( -flNoise, flNoise );

		UTIL_TraceLine( ptr->vecEndPos, ptr->vecEndPos + vecTraceDir * 172, ignore_monsters, ENT(pev), &Bloodtr);

/*
		MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
			WRITE_BYTE( TE_SHOWLINE);
			WRITE_COORD( ptr->vecEndPos.x );
			WRITE_COORD( ptr->vecEndPos.y );
			WRITE_COORD( ptr->vecEndPos.z );
			
			WRITE_COORD( Bloodtr.vecEndPos.x );
			WRITE_COORD( Bloodtr.vecEndPos.y );
			WRITE_COORD( Bloodtr.vecEndPos.z );
		MESSAGE_END();
*/

		if ( Bloodtr.flFraction != 1.0 )
		{
			UTIL_BloodDecalTrace( &Bloodtr, BloodColor() );
		}
	}
}
