//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"

#include "saverestore.h"
#include "saverestore_utlvector.h"
#include "ai_saverestore.h"
#include "basemonster.h"
#include "ai_squad.h"
#include "ai_memory.h"

static short AI_SAVE_RESTORE_VERSION = 2;

//-----------------------------------------------------------------------------

CAI_SaveRestoreHandler g_AI_SaveRestoreHandler;

void CAI_SaveRestoreHandler::Save( CSave &save )
{
	save.WriteShort( &AI_SAVE_RESTORE_VERSION );

	save.StartBlock( "Squads" );
	short nSquads = (short)g_AI_SquadManager.NumSquads();
	save.WriteShort( &nSquads );
		
	AISquadsIter_t iter;
	int squadName;
	CAI_Squad* pSquad = g_AI_SquadManager.GetFirstSquad( &iter );
	while (pSquad)
	{
		squadName = MAKE_STRING( pSquad->GetName() );
		save.WriteString( "", &squadName, 1 );
		save.WriteAll( pSquad );
		pSquad = g_AI_SquadManager.GetNextSquad( &iter );
	}
		
	save.EndBlock();

	save.StartBlock( "Enemies" );
	short nMemories = 0;
		
	CBaseMonster **ppAIs = g_AI_Manager.AccessAIs();
	int i;

	for ( i = 0; i < g_AI_Manager.NumAIs(); i++ )
	{
		if ( ppAIs[i]->GetEnemies() )
			nMemories++;
	}

	save.WriteShort( &nMemories );
		
	for ( i = 0; i < g_AI_Manager.NumAIs(); i++ )
	{
		if ( ppAIs[i]->GetEnemies() )
		{
			CBaseEntity *p = ppAIs[i];
			save.WriteEntityPtr( &p );
			save.WriteAll( ppAIs[i]->GetEnemies() );
		}
	}
	save.EndBlock();
}

void CAI_SaveRestoreHandler::Restore( CRestore &restore )
{
	// Initialize the squads (as there's no spawn)
	CBaseMonster **ppAIs = g_AI_Manager.AccessAIs();
	int i;

	for ( i = 0; i < g_AI_Manager.NumAIs(); i++ )
	{
		ppAIs[i]->InitSquad();
	}

	// No reason why any future version shouldn't try to retain backward compatability. The default here is to not do so.
	short version;
	restore.ReadShort( &version );

	if ( version == AI_SAVE_RESTORE_VERSION )
	{
		restore.StartBlock();
		// Fixup all the squads
		CAI_Squad  ignored;
		CAI_Squad *pSquad;
		string_t   squadName;
		int 	   nSavedSquads = restore.ReadShort();
			
		while ( nSavedSquads-- )
		{
			int sizeData = restore.SkipHeader();
			restore.ReadString( &squadName, 1, sizeData );
			pSquad = g_AI_SquadManager.FindSquad( squadName );
			if ( !pSquad )
				pSquad = &ignored; // if all of the AIs in a squad failed to spawn, there would be no squad
			restore.ReadAll( pSquad );
		}
		restore.EndBlock();

		//---------------------------------
		// load memories for unsquadded monsters
			
		restore.StartBlock();
		CAI_Enemies ignoredMem;
		short nMemories = restore.ReadShort();
			
		CBaseEntity *pAI; 
			
		while ( nMemories-- )
		{
			restore.ReadEntityPtr( &pAI );
				
			if ( pAI )
				restore.ReadAll( ((CBaseMonster *)pAI)->GetEnemies() );
			else
				restore.ReadAll( &ignoredMem ); // AI probably failed to spawn
		}

		restore.EndBlock();
	}
}


