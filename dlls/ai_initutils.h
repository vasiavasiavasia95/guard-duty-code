//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:		AI Utility classes for building the initial AI Networks
//
// $NoKeywords: $
//=============================================================================//

#ifndef AI_INITUTILS_H
#define AI_INITUTILS_H
#ifdef _WIN32
#pragma once
#endif

#include "basemonster.h"

//=========================================================
// Nodes start out as ents in the level. The node graph 
// is built, then these ents are discarded. 
//=========================================================
class CNodeEnt : public CPointEntity
{
	DECLARE_CLASS( CNodeEnt, CPointEntity );
public:

	void Spawn( void );
	void KeyValue( KeyValueData *pkvd );

	short m_sHintType;
	short m_sHintActivity;
};

//=========================================================
// TestHull is a modelless clip hull that verifies reachable
// nodes by walking from every node to each of it's connections
//=========================================================
class CTestHull : public CBaseMonster
{
	DECLARE_CLASS( CTestHull, CBaseMonster );
public:
	virtual void			Precache();
	void Spawn( entvars_t *pevMasterNode );
	virtual int	ObjectCaps( void ) { return BaseClass::ObjectCaps() & ~(FCAP_ACROSS_TRANSITION|FCAP_DONT_SAVE); }
	void EXPORT CallBuildNodeGraph ( void );
	void BuildNodeGraph ( void );
	void EXPORT ShowBadNode ( void );
	void EXPORT DropDelay ( void );
	void EXPORT PathFind ( void );

	Vector	vecBadNodeOrigin;
};

#endif // AI_INITUTILS_H
