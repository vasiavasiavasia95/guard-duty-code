/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef PLAYER_H
#define PLAYER_H
#ifdef _WIN32
#pragma once
#endif

#include "basecombatcharacter.h"

//
// Player PHYSICS FLAGS bits
//
enum PlayerPhysFlag_e
{
	PFLAG_ONLADDER		= ( 1<<0 ),
	PFLAG_ONSWING		= ( 1<<0 ),
	PFLAG_ONTRAIN		= ( 1<<1 ),
	PFLAG_ONBARNACLE	= ( 1<<2 ),
	PFLAG_DUCKING		= ( 1<<3 ),		// In the process of ducking, but totally squatted yet
	PFLAG_USING			= ( 1<<4 ),		// Using a continuous entity
	PFLAG_OBSERVER		= ( 1<<5 ),		// player is locked in stationary cam mode. Spectators can move, observers can't.
};

//
// generic player
//
//-----------------------------------------------------
//This is Half-Life player entity
//-----------------------------------------------------
#define CSUITPLAYLIST	4		// max of 4 suit sentences queued up at any time

#define SUIT_GROUP			TRUE
#define	SUIT_SENTENCE		FALSE

#define	SUIT_REPEAT_OK		0
#define SUIT_NEXT_IN_30SEC	30
#define SUIT_NEXT_IN_1MIN	60
#define SUIT_NEXT_IN_5MIN	300
#define SUIT_NEXT_IN_10MIN	600
#define SUIT_NEXT_IN_30MIN	1800
#define SUIT_NEXT_IN_1HOUR	3600

#define CSUITNOREPEAT		32

#define	SOUND_FLASHLIGHT_ON		"items/flashlight1.wav"
#define	SOUND_FLASHLIGHT_OFF	"items/flashlight1.wav"

#define TEAM_NAME_LENGTH	16

#define AUTOAIM_2DEGREES  0.0348994967025
#define AUTOAIM_5DEGREES  0.08715574274766
#define AUTOAIM_8DEGREES  0.1391731009601
#define AUTOAIM_10DEGREES 0.1736481776669

// useful cosines
#define DOT_1DEGREE   0.9998476951564
#define DOT_2DEGREE   0.9993908270191
#define DOT_3DEGREE   0.9986295347546
#define DOT_4DEGREE   0.9975640502598
#define DOT_5DEGREE   0.9961946980917
#define DOT_6DEGREE   0.9945218953683
#define DOT_7DEGREE   0.9925461516413
#define DOT_8DEGREE   0.9902680687416
#define DOT_9DEGREE   0.9876883405951
#define DOT_10DEGREE  0.9848077530122
#define DOT_15DEGREE  0.9659258262891
#define DOT_20DEGREE  0.9396926207859
#define DOT_25DEGREE  0.9063077870367
#define DOT_30DEGREE  0.866025403784

typedef enum
{
	PLAYER_IDLE,
	PLAYER_WALK,
	PLAYER_JUMP,
	PLAYER_SUPERJUMP,
	PLAYER_DIE,
	PLAYER_ATTACK1,
} PLAYER_ANIM;

#define CHAT_INTERVAL 1.0f

// PlayerUse defines
#define	PLAYER_USE_RADIUS	(float)64

class CBasePlayer : public CBaseCombatCharacter
{
public:
	DECLARE_CLASS( CBasePlayer, CBaseCombatCharacter );

	DECLARE_DATADESC();

	virtual void			Spawn( void );

	void EXPORT				PlayerDeathThink( void );

	virtual void			Pain( void );
	virtual void			Jump( void );
	virtual void			Duck( void );

	virtual void			PreThink( void );
	virtual void			PostThink( void );
	virtual int				TakeHealth( float flHealth, int bitsDamageType );
	virtual void			TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, TraceResult *ptr );
	virtual int				TakeDamage( const CTakeDamageInfo &info );

	const Vector			&EyeAngles( );		// Direction of eyes
	void					EyeVectors( Vector *pForward, Vector *pRight = NULL, Vector *pUp = NULL );

	// Sets the view angles
	void					SnapEyeAngles( const Vector &viewAngles );

	virtual Vector			BodyAngles() { return EyeAngles(); };
	virtual Vector			BodyTarget( const Vector &posSrc, bool bNoisy );
	virtual bool			ShouldFadeOnDeath( void ) { return false; }
	
	int						TakeDamage_Alive( const CTakeDamageInfo &info );
	virtual void			Event_Killed( const CTakeDamageInfo &info );
	void					Event_Dying( void );

	virtual	bool			IsPlayer( void ) { return true; }			// Spectators should return false for this, they aren't "players" as far as game logic is concerned
	virtual bool			IsNetClient( void ) { return true; }		// Bots should return false for this, they can't receive NET messages
																		// Spectators should return true for this

	// Get the client index (entindex-1).
	int						GetClientIndex()	{ return ENTINDEX( edict() ) - 1; }
	
	virtual void			StartSneaking( void ) { m_tSneaking = gpGlobals->time - 1; }
	virtual void			StopSneaking( void ) { m_tSneaking = gpGlobals->time + 30; }
	virtual bool			IsSneaking( void ) { return m_tSneaking <= gpGlobals->time; }
	
	virtual const char		*TeamID( void );

	virtual int				Save( CSave &save );
	virtual int				Restore( CRestore &restore );
	virtual void			OnRestore( void );

	virtual void			RenewItems( void );
	virtual void			PackDeadPlayerItems( void );
	virtual void			RemoveAllItems( bool removeSuit );

	// Weapon stuff
	virtual Vector			GetGunPosition( void );
	virtual bool			SwitchWeapon( CBasePlayerWeapon *pWeapon );
	virtual void			SelectPrevItem( int iItem );
	virtual void			SelectNextItem( int iItem );

	// JOHN:  sends custom messages if player HUD data has changed  (eg health, ammo)
	virtual void			UpdateClientData( void );

	// Player is moved across the transition by other means
	virtual int				ObjectCaps( void ) { return BaseClass::ObjectCaps() & ~FCAP_ACROSS_TRANSITION; }
	virtual void			Precache( void );
	bool					IsOnLadder( void );

	virtual bool			FlashlightIsOn( void );
	virtual void			FlashlightTurnOn( void );
	virtual void			FlashlightTurnOff( void );

	void					UpdatePlayerSound ( void );
	virtual void			DeathSound ( void );

	Class_T					Classify ( void );
	virtual void			SetAnimation( PLAYER_ANIM playerAnim );
	void					SetWeaponAnimType( const char *szExtention );

	// custom player functions
	virtual void			ImpulseCommands( void );
	virtual void			CheatImpulseCommands( int iImpulse );

	virtual void			StartDeathCam( void );
	virtual void			StartObserver( Vector vecPosition, Vector vecViewAngle ); // true, if successful

	void					AddPoints( int score, bool bAllowNegativeScore );
	void					AddPointsToTeam( int score, bool bAllowNegativeScore );
	bool					AddPlayerItem( CBasePlayerWeapon *pItem );
	bool					RemovePlayerItem( CBasePlayerWeapon *pItem );
	void					DropPlayerItem ( char *pszItemName );
	bool					HasPlayerItem( CBasePlayerWeapon *pCheckItem );
	bool					HasNamedPlayerItem( const char *pszItemName );
	bool					HasWeapons( void );// do I have ANY weapons?
	virtual void			SelectLastItem(void);
	virtual void			SelectItem(const char *pstr);
	void					ItemPreFrame( void );
	void					ItemPostFrame( void );
	void					GiveNamedItem( const char *szName );
	void					EnableControl( bool fControl);

	int						GiveAmmo( int iAmount, const char *szName, int iMax );
	void					SendAmmoUpdate(void);

	void					WaterMove( void );
	virtual void			PlayerUse( void );
	CBaseEntity				*FindUseEntity( void );
	virtual bool			IsUseableEntity( CBaseEntity *pEntity, unsigned int requiredCaps );
	bool					ClearUseEntity();

	void					CheckSuitUpdate();
	void					SetSuitUpdate(const char *name, int fgroup, int iNoRepeat);
	void					UpdateGeigerCounter( void );
	void					CheckTimeBasedDamage( void );

	bool					FBecomeProne ( void );
	void					BarnacleVictimBitten ( CBaseEntity *pBarnacle );
	void					BarnacleVictimReleased ( void );

	static int				GetAmmoIndex(const char *psz);
	int						AmmoInventory( int iAmmoIndex );
	int						Illumination( void );

	void					ResetAutoaim( void );
	Vector					GetAutoaimVector( float flDelta  );
	Vector					AutoaimDeflection( Vector &vecSrc, float flDist, float flDelta );
	bool					ShouldAutoaim( void );

	void					SetViewEntity( CBaseEntity *pEntity );
	CBaseEntity				*GetViewEntity( void ) { return m_hViewEntity; }

	virtual void			ForceClientDllUpdate( void );  // Forces all client .dll specific data to be resent to client.

	void					SetCustomDecalFrames( int nFrames );
	int						GetCustomDecalFrames( void );

	void					HandleFuncTrain( void );

public:

	Vector					GetSmoothedVelocity( void );

	void	SetUseEntity( CBaseEntity *pUseEntity );

public:

	Vector						m_vecSmoothedVelocity;

	int					m_iPlayerSound;// the index of the sound list slot reserved for this player
	int					m_iTargetVolume;// ideal sound volume. 
	int					m_iWeaponVolume;// how loud the player's weapon is right now.
	int					m_iExtraSoundTypes;// additional classification for this weapon's sound
	int					m_iWeaponFlash;// brightness of the weapon flash
	float				m_flStopExtraSoundTime;
	
	float				m_flFlashLightTime;	// Time until next battery draw/Recharge
	int					m_iFlashBattery;		// Flashlight Battery Draw

	int					m_afButtonLast;
	int					m_afButtonPressed;
	int					m_afButtonReleased;
	
	edict_t			   *m_pentSndLast;			// last sound entity to modify player room type
	float				m_flSndRoomtype;		// last roomtype set by sound entity
	float				m_flSndRange;			// dist from player to sound entity

	float				m_flFallVelocity;
	
	int					m_rgItems[MAX_ITEMS];
	int					m_fKnownItem;		// True when a new item needs to be added
	int					m_fNewAmmo;			// True when a new item has been added

	unsigned int		m_afPhysicsFlags;	// physics flags - set when 'normal' physics should be revisited or overriden
	float				m_fNextSuicideTime; // the time after which the player can next use the suicide command


// these are time-sensitive things that we keep track of
	float				m_flWallJumpTime;	// how long until next walljump

	float				m_flSuitUpdate;					// when to play next suit update
	int					m_rgSuitPlayList[CSUITPLAYLIST];// next sentencenum to play for suit update
	int					m_iSuitPlayNext;				// next sentence slot for queue storage;
	int					m_rgiSuitNoRepeat[CSUITNOREPEAT];		// suit sentence no repeat list
	float				m_rgflSuitNoRepeatTime[CSUITNOREPEAT];	// how long to wait before allowing repeat
	int					m_lastDamageAmount;		// Last damage taken
	float				m_tbdPrev;				// Time-based damage timer

	float				m_flgeigerRange;		// range to nearest radiation source
	float				m_flgeigerDelay;		// delay per update of range msg to client
	int					m_igeigerRangePrev;

	int					m_idrowndmg;			// track drowning damage taken
	int					m_idrownrestored;		// track drowning damage restored

	int					m_bitsDamageType;		// what types of damage has player taken
	int					m_bitsHUDDamage;		// Damage bits for the current fame. These get sent to 
												// the hude via the DAMAGE message

	bool				m_fInitHUD;				// True when deferred HUD restart msg needs to be sent
	bool				m_fGameHUDInitialized;
	int					m_iTrain;				// Train control position
	bool				m_fWeapon;				// Set this to FALSE to force a reset of the current weapon HUD info

	EHANDLE				m_hUseEntity;			// the player is currently controlling this entity because of +USE latched, NULL if no entity
	float				m_fDeadTime;			// the time at which the player died  (used in PlayerDeathThink())

	bool			m_fNoPlayerSound;	// a debugging feature. Player makes no sound if this is true. 
	bool			m_fLongJump; // does this player have the longjump module?

	float       m_tSneaking;
	int			m_iUpdateTime;		// stores the number of frame ticks before sending HUD update messages
	int			m_iClientHealth;	// the health currently known by the client.  If this changes, send a new
	int			m_iClientBattery;	// the Battery currently known by the client.  If this changes, send a new
	int			m_iHideHUD;		// the players hud weapon info is to be hidden
	int			m_iClientHideHUD;
	int			m_iFOV;			// field of view
	int			m_iClientFOV;	// client's known FOV
	// usable player items 
	CBasePlayerWeapon	*m_rgpPlayerItems[MAX_ITEM_TYPES];
	CBasePlayerWeapon *m_pActiveItem;
	CBasePlayerWeapon *m_pClientActiveItem;  // client version of the active item
	CBasePlayerWeapon *m_pLastItem;
	// shared ammo slots
	int	m_rgAmmo[MAX_AMMO_SLOTS];
	int	m_rgAmmoLast[MAX_AMMO_SLOTS];

	Vector				m_vecAutoAim;
	bool				m_fOnTarget;
	int					m_iDeaths;
	float				m_iRespawnFrames;	// used in PlayerDeathThink() to make sure players can always respawn

	int					m_lastx, m_lasty;  // These are the previous update's crosshair angles, DON"T SAVE/RESTORE

	// NOTE: bits damage type appears to only be used for time-based damage
	BYTE				m_rgbTimeBasedDamage[CDMG_TIMEBASED];

	int m_nCustomSprayFrames;// Custom clan logo frames for this player
	float	m_flNextDecalTime;// next time this player can spray a decal

	char m_szTeamName[TEAM_NAME_LENGTH];

	char m_szAnimExtention[32];

	float m_flNextChatTime;

	void LadderMove_Alpha( void );

	int m_iAlphaLadderFrame;
	float m_iAlphaLadderSpeed;
	bool m_bAlphaLadderPunchSwap;

private:

	Activity				m_Activity;

	EHANDLE					m_hViewEntity;
};

inline void CBasePlayer::SetUseEntity( CBaseEntity *pUseEntity ) 
{ 
	m_hUseEntity = pUseEntity; 
}

inline Vector CBasePlayer::GetSmoothedVelocity( void )
{ 
	return m_vecSmoothedVelocity; 
}

//-----------------------------------------------------------------------------
// Converts an entity to a player
//-----------------------------------------------------------------------------
inline CBasePlayer *ToBasePlayer( CBaseEntity *pEntity )
{
	if ( !pEntity || !pEntity->IsPlayer() )
		return NULL;
#if _DEBUG
	return dynamic_cast<CBasePlayer *>( pEntity );
#else
	return static_cast<CBasePlayer *>( pEntity );
#endif
}

extern int	gmsgHudText;
extern bool gInitHUD;

#endif // PLAYER_H
