/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
#ifndef TALKMONSTER_H
#define TALKMONSTER_H
#ifdef _WIN32
#pragma once
#endif

#include "ai_task.h"
#include "ai_schedule.h"
#include "ai_default.h"
#include "basemonster.h"

//=========================================================
// Talking monster base class
// Used for scientists and barneys
//=========================================================

#define TALKRANGE_MIN 500.0				// don't talk to anyone farther away than this

#define TLK_STARE_DIST	128				// anyone closer than this and looking at me is probably staring at me.

#define bit_saidDamageLight		(1<<0)	// bits so we don't repeat key sentences
#define bit_saidDamageMedium	(1<<1)
#define bit_saidDamageHeavy		(1<<2)
#define bit_saidHelloPlayer		(1<<3)
#define bit_saidWoundLight		(1<<4)
#define bit_saidWoundHeavy		(1<<5)
#define bit_saidHeard			(1<<6)
#define bit_saidSmelled			(1<<7)

#define TLK_CFRIENDS		3

typedef enum
{
	TLK_ANSWER = 0,
	TLK_QUESTION,
	TLK_IDLE,
	TLK_STARE,
	TLK_USE,
	TLK_UNUSE,
	TLK_STOP,
	TLK_NOSHOOT,
	TLK_HELLO,
	TLK_PHELLO,
	TLK_PIDLE,
	TLK_PQUESTION,
	TLK_PLHURT1,
	TLK_PLHURT2,
	TLK_PLHURT3,
	TLK_SMELL,
	TLK_WOUND,
	TLK_MORTAL,

	TLK_CGROUPS,					// MUST be last entry
} TALKGROUPNAMES;

class CTalkMonster : public CBaseMonster
{
	DECLARE_CLASS( CTalkMonster, CBaseMonster );
public:
	void			TalkInit( void );				
	CBaseEntity		*FindNearestFriend( bool fPlayer );
	float			TargetDistance( void );
	void			StopTalking( void ) { SentenceStop(); }
	
	// Base Monster functions
	void			Precache( void );
	int				TakeDamage_Alive( const CTakeDamageInfo &info );
	void			Touch(	CBaseEntity *pOther );
	void			Event_Killed( const CTakeDamageInfo &info );
	Disposition_t	IRelationType( CBaseEntity *pTarget );
	virtual int		CanPlaySentence( bool fDisregardState );
	virtual void	PlaySentence( const char *pszSentence, float duration, float volume, float attenuation );
	void			PlayScriptedSentence( const char *pszSentence, float duration, float volume, float attenuation, bool bConcurrent, CBaseEntity *pListener );
	void			KeyValue( KeyValueData *pkvd );

	// AI functions
	void			SetActivity ( Activity newActivity );
	virtual int		TranslateSchedule( int scheduleType );
	void			StartTask( const Task_t *pTask );
	void			RunTask( const Task_t *pTask );
	void			HandleAnimEvent( animevent_t *pEvent );
	void			PrescheduleThink( void );
	bool			ShouldAlwaysThink();

	//=========================================================
	// TalkMonster schedules
	//=========================================================
	enum
	{
		SCHED_TALKER_IDLE_RESPONSE = BaseClass::NEXT_SCHEDULE,
		SCHED_TALKER_IDLE_SPEAK,
		SCHED_TALKER_IDLE_HELLO,
		SCHED_TALKER_IDLE_SPEAK_WAIT,
		SCHED_TALKER_IDLE_STOP_SHOOTING,
		SCHED_TALKER_MOVE_AWAY,				// Try to get out of the player's way
		SCHED_TALKER_MOVE_AWAY_FOLLOW,		// same, but follow afterward
		SCHED_TALKER_MOVE_AWAY_FAIL,		// Turn back toward player
		SCHED_TALKER_IDLE_WATCH_CLIENT,
		SCHED_TALKER_IDLE_WATCH_CLIENT_STARE,
		SCHED_TALKER_IDLE_EYE_CONTACT,

		// !ALWAYS LAST!
		NEXT_SCHEDULE,	
	};

	//=========================================================
	// TalkMonster tasks
	//=========================================================
	enum
	{
		TASK_CANT_FOLLOW = BaseClass::NEXT_TASK,
		TASK_MOVE_AWAY_PATH,
		TASK_WALK_PATH_FOR_UNITS,

		TASK_TALKER_RESPOND,		// say my response
		TASK_TALKER_SPEAK,			// question or remark
		TASK_TALKER_HELLO,			// Try to say hello to player
		TASK_TALKER_HEADRESET,		// reset head position
		TASK_TALKER_STOPSHOOTING,	// tell player to stop shooting friend
		TASK_TALKER_STARE,			// let the player know I know he's staring at me.
		TASK_TALKER_LOOK_AT_CLIENT,	// faces player if not moving and not talking and in idle.
		TASK_TALKER_CLIENT_STARE,	// same as look at client, but says something if the player stares.
		TASK_TALKER_EYECONTACT,		// maintain eyecontact with person who I'm talking to
		TASK_TALKER_IDEALYAW,		// set ideal yaw to face who I'm talking to
		TASK_FACE_PLAYER,			// Face the player

		// !ALWAYS LAST!
		NEXT_TASK,		
	};

	//=========================================================
	// TalkMonster Conditions
	//=========================================================
	enum 
	{
		// Don't see a client right now.
		COND_TALKER_CLIENT_UNSEEN = BaseClass::NEXT_CONDITION, 	

		// Clients can push talkmonsters out of their way
		COND_TALKER_CLIENT_PUSH, 
		
		// !ALWAYS LAST!
		NEXT_CONDITION
	};
	
	// Conversations / communication
	int				GetVoicePitch( void );
	void			IdleRespond( void );
	int				FIdleSpeak( void );
	int				FIdleStare( void );
	int				FIdleHello( void );
	void			IdleHeadTurn( Vector &vecFriend );
	bool			IsOkToSpeak( void );
	bool			IsOkToCombatSpeak( void );
	void			TrySmellTalk( void );
	CBaseEntity		*EnumFriends( CBaseEntity *pentPrevious, int listNumber, bool bTrace );
	void			AlertFriends( void );
	void			ShutUpFriends( void );
	bool			IsTalking( void );
	void			Talk( float flDuration );	
	// For following
	bool			CanFollow( void );
	bool			IsFollowing( void ) { return m_hTargetEnt != NULL && m_hTargetEnt->IsPlayer(); }
	void			StopFollowing( bool clearSchedule );
	void			StartFollowing( CBaseEntity *pLeader );
	virtual void	DeclineFollowing( void ) {}
	void			LimitFollowers( CBaseEntity *pPlayer, int maxFollowers );

	void EXPORT		FollowerUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	
	virtual void	SetAnswerQuestion( CTalkMonster *pSpeaker );
	virtual int		FriendNumber( int arrayNumber )	{ return arrayNumber; }

	static const char *m_szFriends[TLK_CFRIENDS];		// array of friend names
	static float g_talkWaitTime;
	
	int			m_bitsSaid;						// set bits for sentences we don't want repeated
	int			m_nSpeak;						// number of times initiated talking
	int			m_voicePitch;					// pitch of voice for this head
	const char	*m_szGrp[TLK_CGROUPS];			// sentence group names
	float		m_useTime;						// Don't allow +USE until this time
	int			m_iszUse;						// Custom +USE sentence group (follow)
	int			m_iszUnUse;						// Custom +USE sentence group (stop following)

	float		m_flLastSaidSmelled;// last time we talked about something that stinks
	float		m_flStopTalkTime;// when in the future that I'll be done saying this sentence.

	EHANDLE		m_hTalkTarget;	// who to look at while talking

protected:
	DECLARE_DATADESC();
	DEFINE_CUSTOM_AI;
};

#endif		//TALKMONSTER_H
