/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// hassassin - Human assassin, fast and stealthy
//=========================================================

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"
#include	"weapons.h"
#include	"soundent.h"
#include	"game.h"
#include	"grenade.h"

extern cvar_t sk_hassassin_health;

extern DLL_GLOBAL int  g_iSkillLevel;

//=========================================================
// monster-specific schedule types
//=========================================================
enum
{
	SCHED_ASSASSIN_FAIL = LAST_SHARED_SCHEDULE,
	SCHED_ASSASSIN_EXPOSED,						// cover was blown.
	SCHED_ASSASSIN_TAKECOVER_FROM_ENEMY,
	SCHED_ASSASSIN_TAKECOVER_FROM_ENEMY2,
	SCHED_ASSASSIN_TAKECOVER_FROM_BEST_SOUND,
	SCHED_ASSASSIN_HIDE,
	SCHED_ASSASSIN_HUNT,
	SCHED_ASSASSIN_JUMP,						// fly through the air
	SCHED_ASSASSIN_JUMP_ATTACK,					// fly through the air and shoot
	SCHED_ASSASSIN_JUMP_LAND,					// hit and run away
};

//=========================================================
// monster-specific tasks
//=========================================================
enum
{
	TASK_ASSASSIN_FALL_TO_GROUND = LAST_SHARED_TASK, // falling and waiting to hit ground
};

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		ASSASSIN_AE_SHOOT1	1
#define		ASSASSIN_AE_TOSS1	2
#define		ASSASSIN_AE_JUMP	3

#define bits_MEMORY_BADJUMP		(bits_MEMORY_CUSTOM1)

class CHAssassin : public CBaseMonster
{
	DECLARE_CLASS( CHAssassin, CBaseMonster );

public:
	void Spawn( void );
	void Precache( void );

	void SetYawSpeed ( void );
	Class_T Classify ( void );
	int  GetSoundInterests ( void);
	void Shoot( void );
	void HandleAnimEvent( animevent_t *pEvent );

	int SelectSchedule( void );
	int	TranslateSchedule( int scheduleType );

	bool CheckMeleeAttack1( float flDot, float flDist );	// jump
	// bool CheckMeleeAttack2( float flDot, float flDist );
	bool CheckRangeAttack1( float flDot, float flDist );	// shoot
	bool CheckRangeAttack2( float flDot, float flDist );	// throw grenade
	void StartTask ( const Task_t *pTask );
	void RunAI( void );
	void RunTask ( const Task_t *pTask );

	void DeathSound ( void );
	void IdleSound ( void );

	DEFINE_CUSTOM_AI;
	DECLARE_DATADESC();

private:

	float	m_flLastShot;
	float	m_flDiviation;

	float	m_flNextJump;
	Vector	m_vecJumpVelocity;

	float	m_flNextGrenadeCheck;
	Vector	m_vecTossVelocity;
	bool	m_fThrowGrenade;

	int		m_iTargetRanderamt;

	int		m_iFrustration;

	int		m_iShell;
};
LINK_ENTITY_TO_CLASS( monster_human_assassin, CHAssassin );


BEGIN_DATADESC(	CHAssassin )

	DEFINE_FIELD( CHAssassin, m_flLastShot, FIELD_TIME ),
	DEFINE_FIELD( CHAssassin, m_flDiviation, FIELD_FLOAT ),

	DEFINE_FIELD( CHAssassin, m_flNextJump, FIELD_TIME ),
	DEFINE_FIELD( CHAssassin, m_vecJumpVelocity, FIELD_VECTOR ),

	DEFINE_FIELD( CHAssassin, m_flNextGrenadeCheck, FIELD_TIME ),
	DEFINE_FIELD( CHAssassin, m_vecTossVelocity, FIELD_VECTOR ),
	DEFINE_FIELD( CHAssassin, m_fThrowGrenade, FIELD_BOOLEAN ),

	DEFINE_FIELD( CHAssassin, m_iTargetRanderamt, FIELD_INTEGER ),
	DEFINE_FIELD( CHAssassin, m_iFrustration, FIELD_INTEGER ),

END_DATADESC()


//=========================================================
// DieSound
//=========================================================
void CHAssassin :: DeathSound ( void )
{
}

//=========================================================
// IdleSound
//=========================================================
void CHAssassin :: IdleSound ( void )
{
}

//=========================================================
// GetSoundInterests - returns a bit mask indicating which types
// of sounds this monster regards. 
//=========================================================
int CHAssassin :: GetSoundInterests ( void) 
{
	return	bits_SOUND_WORLD	|
			bits_SOUND_COMBAT	|
			bits_SOUND_DANGER	|
			bits_SOUND_PLAYER;
}


//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CHAssassin::Classify( void )
{
	return CLASS_HUMAN_MILITARY;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CHAssassin :: SetYawSpeed ( void )
{
	int ys;

	switch ( m_Activity )
	{
	case ACT_TURN_LEFT:
	case ACT_TURN_RIGHT:
		ys = 360;
		break;
	default:			
		ys = 360;
		break;
	}

	pev->yaw_speed = ys;
}


//=========================================================
// Shoot
//=========================================================
void CHAssassin :: Shoot ( void )
{
	if (m_hEnemy == NULL)
	{
		return;
	}

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = GetShootEnemyDir( vecShootOrigin );

	if (m_flLastShot + 2 < gpGlobals->time)
	{
		m_flDiviation = 0.10;
	}
	else
	{
		m_flDiviation -= 0.01;
		if (m_flDiviation < 0.02)
			m_flDiviation = 0.02;
	}
	m_flLastShot = gpGlobals->time;

	UTIL_MakeVectors ( pev->angles );

	Vector	vecShellVelocity = gpGlobals->v_right * RANDOM_FLOAT(40,90) + gpGlobals->v_up * RANDOM_FLOAT(75,200) + gpGlobals->v_forward * RANDOM_FLOAT(-40, 40);
	EjectBrass ( pev->origin + gpGlobals->v_up * 32 + gpGlobals->v_forward * 12, vecShellVelocity, pev->angles.y, m_iShell, TE_BOUNCE_SHELL); 
	FireBullets(1, vecShootOrigin, vecShootDir, Vector( m_flDiviation, m_flDiviation, m_flDiviation ), 2048, BULLET_MONSTER_9MM ); // shoot +-8 degrees

	switch(RANDOM_LONG(0,1))
	{
	case 0:
		EMIT_SOUND(ENT(pev), CHAN_WEAPON, "weapons/pl_gun1.wav", RANDOM_FLOAT(0.6, 0.8), ATTN_NORM);
		break;
	case 1:
		EMIT_SOUND(ENT(pev), CHAN_WEAPON, "weapons/pl_gun2.wav", RANDOM_FLOAT(0.6, 0.8), ATTN_NORM);
		break;
	}

	pev->effects |= EF_MUZZLEFLASH;

	Vector angDir = UTIL_VecToAngles( vecShootDir );
	SetBlending( 0, -angDir.x );

	m_cAmmoLoaded--;
}


//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//
// Returns number of events handled, 0 if none.
//=========================================================
void CHAssassin :: HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case ASSASSIN_AE_SHOOT1:
		Shoot( );
		break;
	case ASSASSIN_AE_TOSS1:
		{
			UTIL_MakeVectors( pev->angles );
			CGrenade::ShootTimed( pev, pev->origin + gpGlobals->v_forward * 34 + Vector (0, 0, 32), m_vecTossVelocity, 2.0 );

			m_flNextGrenadeCheck = gpGlobals->time + 6;// wait six seconds before even looking again to see if a grenade can be thrown.
			m_fThrowGrenade = FALSE;
			// !!!LATER - when in a group, only try to throw grenade if ordered.
		}
		break;
	case ASSASSIN_AE_JUMP:
		{
			// ALERT( at_console, "jumping");
			UTIL_MakeAimVectors( pev->angles );
			pev->movetype = MOVETYPE_TOSS;
			pev->flags &= ~FL_ONGROUND;
			pev->velocity = m_vecJumpVelocity;
			m_flNextJump = gpGlobals->time + 3.0;
		}
		return;
	default:
		CBaseMonster::HandleAnimEvent( pEvent );
		break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CHAssassin :: Spawn()
{
	Precache( );

	SET_MODEL(ENT(pev), "models/hassassin.mdl");
	UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_RED;
	pev->effects		= 0;
	pev->health			= sk_hassassin_health.value;
	m_flFieldOfView		= VIEW_FIELD_WIDE; // indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;
	pev->friction		= 1;

	m_HackedGunPos		= Vector( 0, 24, 48 );

	m_iTargetRanderamt	= 20;
	pev->renderamt		= 20;
	pev->rendermode		= kRenderTransTexture;

	CapabilitiesClear();
	CapabilitiesAdd( bits_CAP_MELEE_ATTACK1 | bits_CAP_DOORS_GROUP );

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CHAssassin :: Precache()
{
	PRECACHE_MODEL("models/hassassin.mdl");

	PRECACHE_SOUND("weapons/pl_gun1.wav");
	PRECACHE_SOUND("weapons/pl_gun2.wav");

	PRECACHE_SOUND("debris/beamstart1.wav");

	m_iShell = PRECACHE_MODEL ("models/shell.mdl");// brass shell
}	
	
//=========================================================
// CheckMeleeAttack1 - jump like crazy if the enemy gets too close. 
//=========================================================
bool CHAssassin::CheckMeleeAttack1( float flDot, float flDist )
{
	if ( m_flNextJump < gpGlobals->time && (flDist <= 128 || HasMemory( bits_MEMORY_BADJUMP )) && m_hEnemy != NULL )
	{
		TraceResult	tr;

		Vector vecDest = pev->origin + Vector( RANDOM_FLOAT( -64, 64), RANDOM_FLOAT( -64, 64 ), 160 );

		UTIL_TraceHull( pev->origin + Vector( 0, 0, 36 ), vecDest + Vector( 0, 0, 36 ), dont_ignore_monsters, human_hull, ENT(pev), &tr);

		if ( tr.fStartSolid || tr.flFraction < 1.0)
		{
			return false;
		}

		float flGravity = g_psv_gravity->value;

		float time = sqrt( 160 / (0.5 * flGravity));
		float speed = flGravity * time / 160;
		m_vecJumpVelocity = (vecDest - pev->origin) * speed;

		return true;
	}
	return false;
}

//=========================================================
// CheckRangeAttack1  - drop a cap in their ass
//
//=========================================================
bool CHAssassin::CheckRangeAttack1( float flDot, float flDist )
{
	if ( !HasCondition( COND_ENEMY_OCCLUDED ) && flDist > 64 && flDist <= 2048 /* && flDot >= 0.5 */ /* && NoFriendlyFire() */ )
	{
		TraceResult	tr;

		Vector vecSrc = GetGunPosition();

		// verify that a bullet fired from the gun will hit the enemy before the world.
		UTIL_TraceLine( vecSrc, m_hEnemy->BodyTarget(vecSrc), dont_ignore_monsters, ENT(pev), &tr);

		if ( tr.flFraction == 1 || tr.pHit == m_hEnemy->edict() )
		{
			return true;
		}
	}
	return false;
}

//=========================================================
// CheckRangeAttack2 - toss grenade is enemy gets in the way and is too close. 
//=========================================================
bool CHAssassin::CheckRangeAttack2( float flDot, float flDist )
{
	m_fThrowGrenade = false;
	if ( !FBitSet ( m_hEnemy->pev->flags, FL_ONGROUND ) )
	{
		// don't throw grenades at anything that isn't on the ground!
		return false;
	}

	// don't get grenade happy unless the player starts to piss you off
	if ( m_iFrustration <= 2)
		return false;

	if ( m_flNextGrenadeCheck < gpGlobals->time && !HasCondition( COND_ENEMY_OCCLUDED ) && flDist <= 512 /* && flDot >= 0.5 */ /* && NoFriendlyFire() */ )
	{
		Vector vecToss = VecCheckThrow( this, GetGunPosition( ), m_hEnemy->Center(), flDist, 0.5 ); // use dist as speed to get there in 1 second

		if ( vecToss != vec3_origin )
		{
			m_vecTossVelocity = vecToss;

			// throw a hand grenade
			m_fThrowGrenade = true;

			return true;
		}
	}

	return false;
}


//=========================================================
// RunAI
//=========================================================
void CHAssassin :: RunAI( void )
{
	CBaseMonster :: RunAI();

	// always visible if moving
	// always visible is not on hard
	if (g_iSkillLevel != SKILL_HARD || m_hEnemy == NULL || pev->deadflag != DEAD_NO || m_Activity == ACT_RUN || m_Activity == ACT_WALK || !(pev->flags & FL_ONGROUND))
		m_iTargetRanderamt = 255;
	else
		m_iTargetRanderamt = 20;

	if (pev->renderamt > m_iTargetRanderamt)
	{
		if (pev->renderamt == 255)
		{
			EMIT_SOUND (ENT(pev), CHAN_BODY, "debris/beamstart1.wav", 0.2, ATTN_NORM );
		}

		pev->renderamt = V_max( pev->renderamt - 50, m_iTargetRanderamt );
		pev->rendermode = kRenderTransTexture;
	}
	else if (pev->renderamt < m_iTargetRanderamt)
	{
		pev->renderamt = V_min( pev->renderamt + 50, m_iTargetRanderamt );
		if (pev->renderamt == 255)
			pev->rendermode = kRenderNormal;
	}

	if (m_Activity == ACT_RUN || m_Activity == ACT_WALK)
	{
		static int iStep = 0;
		iStep = ! iStep;
		if (iStep)
		{
			switch( RANDOM_LONG( 0, 3 ) )
			{
			case 0:	EMIT_SOUND( ENT(pev), CHAN_BODY, "player/pl_step1.wav", 0.5, ATTN_NORM);	break;
			case 1:	EMIT_SOUND( ENT(pev), CHAN_BODY, "player/pl_step3.wav", 0.5, ATTN_NORM);	break;
			case 2:	EMIT_SOUND( ENT(pev), CHAN_BODY, "player/pl_step2.wav", 0.5, ATTN_NORM);	break;
			case 3:	EMIT_SOUND( ENT(pev), CHAN_BODY, "player/pl_step4.wav", 0.5, ATTN_NORM);	break;
			}
		}
	}
}


//=========================================================
// StartTask
//=========================================================
void CHAssassin :: StartTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_RANGE_ATTACK2:
		if (!m_fThrowGrenade)
		{
			TaskComplete( );
		}
		else
		{
			CBaseMonster :: StartTask ( pTask );
		}
		break;
	case TASK_ASSASSIN_FALL_TO_GROUND:
		break;
	default:
		CBaseMonster :: StartTask ( pTask );
		break;
	}
}


//=========================================================
// RunTask 
//=========================================================
void CHAssassin :: RunTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_ASSASSIN_FALL_TO_GROUND:
		MakeIdealYaw( GetEnemyLKP() );
		ChangeYaw( pev->yaw_speed );

		if (m_fSequenceFinished)
		{
			if (pev->velocity.z > 0)
			{
				pev->sequence = LookupSequence( "fly_up" );
			}
			else if ( HasCondition( COND_SEE_ENEMY ) )
			{
				pev->sequence = LookupSequence( "fly_attack" );
				pev->frame = 0;
			}
			else
			{
				pev->sequence = LookupSequence( "fly_down" );
				pev->frame = 0;
			}
			
			ResetSequenceInfo( );
			SetYawSpeed();
		}
		if (pev->flags & FL_ONGROUND)
		{
			// ALERT( at_console, "on ground\n");
			TaskComplete( );
		}
		break;
	default: 
		BaseClass::RunTask( pTask );
		break;
	}
}

//=========================================================
// GetSchedule - Decides which type of schedule best suits
// the monster's current state and conditions. Then calls
// monster's member function to get a pointer to a schedule
// of the proper type.
//=========================================================
int CHAssassin :: SelectSchedule ( void )
{
	switch	( m_MonsterState )
	{
	case MONSTERSTATE_IDLE:
	case MONSTERSTATE_ALERT:
		{
			if ( HasCondition ( COND_HEAR_DANGER ) || HasCondition ( COND_HEAR_COMBAT ) )
			{
				if ( HasCondition ( COND_HEAR_DANGER ) )
					 return SCHED_TAKE_COVER_FROM_BEST_SOUND;
				else
				 	 return SCHED_INVESTIGATE_SOUND;
			}
		}
		break;

	case MONSTERSTATE_COMBAT:
		{
// dead enemy
			if ( HasCondition( COND_ENEMY_DEAD ) )
			{
				// call base class, all code to handle dead enemies is centralized there.
				return BaseClass::SelectSchedule();
			}

			// flying?
			if ( pev->movetype == MOVETYPE_TOSS)
			{
				if (pev->flags & FL_ONGROUND)
				{
					// ALERT( at_console, "landed\n");
					// just landed
					pev->movetype = MOVETYPE_STEP;
					return SCHED_ASSASSIN_JUMP_LAND;
				}
				else
				{
					// ALERT( at_console, "jump\n");
					// jump or jump/shoot
					if ( m_MonsterState == MONSTERSTATE_COMBAT )
						return SCHED_ASSASSIN_JUMP;
					else
						return SCHED_ASSASSIN_JUMP_ATTACK;
				}
			}

			if ( HasCondition( COND_HEAR_DANGER ) )
			{
				return SCHED_TAKE_COVER_FROM_BEST_SOUND;
			}

			if ( HasCondition( COND_LIGHT_DAMAGE ) )
			{
				m_iFrustration++;
			}
			if ( HasCondition( COND_HEAVY_DAMAGE ) )
			{
				m_iFrustration++;
			}

		// jump player!
			if ( HasCondition( COND_CAN_MELEE_ATTACK1 ) )
			{
				// ALERT( at_console, "melee attack 1\n");
				return SCHED_MELEE_ATTACK1;
			}

		// throw grenade
			if ( HasCondition( COND_CAN_RANGE_ATTACK2 ) )
			{
				// ALERT( at_console, "range attack 2\n");
				return SCHED_RANGE_ATTACK2;
			}

		// spotted
			if ( HasCondition( COND_SEE_ENEMY ) && HasCondition( COND_ENEMY_FACING_ME ) )
			{
				// ALERT( at_console, "exposed\n");
				m_iFrustration++;
				return SCHED_ASSASSIN_EXPOSED;
			}

		// can attack
			if ( HasCondition( COND_CAN_RANGE_ATTACK1 ) )
			{
				// ALERT( at_console, "range attack 1\n");
				m_iFrustration = 0;
				return SCHED_RANGE_ATTACK1;
			}

			if ( HasCondition( COND_SEE_ENEMY ) )
			{
				// ALERT( at_console, "face\n");
				return SCHED_COMBAT_FACE;
			}

		// new enemy
			if ( HasCondition( COND_NEW_ENEMY ) )
			{
				// ALERT( at_console, "take cover\n");
				return SCHED_TAKE_COVER_FROM_ENEMY;
			}

			// ALERT( at_console, "stand\n");
			return SCHED_ALERT_STAND;
		}
		break;
	}

	return BaseClass::SelectSchedule();
}

//=========================================================
//=========================================================
int CHAssassin::TranslateSchedule( int scheduleType ) 
{
	// ALERT( at_console, "%d\n", m_iFrustration );
	switch	( scheduleType )
	{
	case SCHED_TAKE_COVER_FROM_ENEMY:

		if ( pev->health > 30 )
			return SCHED_ASSASSIN_TAKECOVER_FROM_ENEMY;
		else
			return SCHED_ASSASSIN_TAKECOVER_FROM_ENEMY2;

	case SCHED_TAKE_COVER_FROM_BEST_SOUND:
		return SCHED_ASSASSIN_TAKECOVER_FROM_BEST_SOUND;
	case SCHED_FAIL:
		if (m_MonsterState == MONSTERSTATE_COMBAT)
			return SCHED_ASSASSIN_FAIL;
		break;
	case SCHED_ALERT_STAND:
		if (m_MonsterState == MONSTERSTATE_COMBAT)
			return SCHED_ASSASSIN_HIDE;
		break;
	case SCHED_MELEE_ATTACK1:
		if (pev->flags & FL_ONGROUND)
		{
			if (m_flNextJump > gpGlobals->time)
			{
				// can't jump yet, go ahead and fail
				return SCHED_ASSASSIN_FAIL;
			}
			else
			{
				return SCHED_ASSASSIN_JUMP;
			}
		}
		else
		{
			return SCHED_ASSASSIN_JUMP_ATTACK;
		}
	}

	return BaseClass::TranslateSchedule( scheduleType );
}

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

AI_BEGIN_CUSTOM_NPC( monster_human_assassin, CHAssassin )

	DECLARE_TASK( TASK_ASSASSIN_FALL_TO_GROUND )

	//=========================================================
	// > SCHED_ASSASSIN_FAIL - Fail Schedule
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_FAIL,
	
		"	Tasks"
		"		TASK_STOP_MOVING		0"
		"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
		"		TASK_WAIT_FACE_ENEMY	2"
		"		TASK_SET_SCHEDULE		SCHEDULE:SCHED_CHASE_ENEMY"
		""
		"	Interrupts"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
		"		COND_CAN_RANGE_ATTACK1"
		"		COND_CAN_RANGE_ATTACK2"
		"		COND_CAN_MELEE_ATTACK1"
		"		COND_HEAR_DANGER"
		"		COND_HEAR_PLAYER"
	)

	//=========================================================
	// > SCHED_ASSASSIN_EXPOSED - Enemy exposed assasin's cover
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_EXPOSED,
	
		"	Tasks"
		"		TASK_STOP_MOVING		0"
		"		TASK_RANGE_ATTACK1		0"
		"		TASK_SET_FAIL_SCHEDULE	SCHEDULE:SCHED_ASSASSIN_JUMP"
		"		TASK_SET_SCHEDULE		SCHEDULE:SCHED_TAKE_COVER_FROM_ENEMY"
		""
		"	Interrupts"
		"	COND_CAN_MELEE_ATTACK1"
		
	)

	//=========================================================
	// > SCHED_ASSASSIN_TAKE_COVER_FROM_ENEMY
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_TAKECOVER_FROM_ENEMY,
	
		"	Tasks"
		"		TASK_STOP_MOVING				0"
		"		TASK_WAIT						0.2"
		"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_RANGE_ATTACK1"
		"		TASK_FIND_COVER_FROM_ENEMY		0"
		"		TASK_RUN_PATH					0"
		"		TASK_WAIT_FOR_MOVEMENT			0"
		"		TASK_REMEMBER					MEMORY:INCOVER"
		"		TASK_FACE_ENEMY					0"
		""
		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_CAN_MELEE_ATTACK1"
		"		COND_HEAR_DANGER"
		
	)

	//=========================================================
	// > SCHED_ASSASSIN_TAKE_COVER_FROM_ENEMY2
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_TAKECOVER_FROM_ENEMY2,
	
		"	Tasks"
		"		TASK_STOP_MOVING					0"
		"		TASK_WAIT							0.2"
		"		TASK_FACE_ENEMY						0"
		"		TASK_RANGE_ATTACK1					0"
		"		TASK_SET_FAIL_SCHEDULE				SCHEDULE:SCHED_RANGE_ATTACK2"
		"		TASK_FIND_FAR_NODE_COVER_FROM_ENEMY	384"
		"		TASK_RUN_PATH						0"
		"		TASK_WAIT_FOR_MOVEMENT				0"
		"		TASK_REMEMBER						MEMORY:INCOVER"
		"		TASK_FACE_ENEMY						0"
		""
		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_CAN_MELEE_ATTACK1"
		"		COND_HEAR_DANGER"
		
	)

	//=========================================================
	// > SCHED_ASSASSIN_TAKE_COVER_FROM_BEST_SOUND
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_TAKECOVER_FROM_BEST_SOUND,
	
		"	Tasks"
		"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_MELEE_ATTACK1"
		"		TASK_STOP_MOVING				0"
		"		TASK_FIND_COVER_FROM_BEST_SOUND	0"
		"		TASK_RUN_PATH					0"
		"		TASK_WAIT_FOR_MOVEMENT			0"
		"		TASK_REMEMBER					MEMORY:INCOVER"
		"		TASK_TURN_LEFT					179"
		""
		"	Interrupts"
		"		COND_NEW_ENEMY"
			
	)

	//=========================================================
	// > SCHED_ASSASSIN_HIDE
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_HIDE,
	
		"	Tasks"
		"		TASK_STOP_MOVING		0"
		"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
		"		TASK_WAIT				2"
		"		TASK_SET_SCHEDULE		SCHEDULE:SCHED_CHASE_ENEMY"
	
		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_SEE_ENEMY"
		"		COND_SEE_FEAR"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
		"		COND_PROVOKED"
		"		COND_HEAR_DANGER"
	)

	//=========================================================
	// > SCHED_ASSASSIN_HUNT
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_HUNT,
	
		"	Tasks"
		"		TASK_GET_PATH_TO_ENEMY		0"
		"		TASK_RUN_PATH				0"
		"		TASK_WAIT_FOR_MOVEMENT		0"
		
		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_CAN_RANGE_ATTACK1"
		"		COND_HEAR_DANGER"
	)

	//=========================================================
	// > SCHED_ASSASSIN_JUMP
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_JUMP,
	
		"	Tasks"
		"		TASK_STOP_MOVING		0"
		"		TASK_PLAY_SEQUENCE		ACTIVITY:ACT_HOP"
		"		TASK_SET_SCHEDULE		SCHEDULE:SCHED_ASSASSIN_JUMP_ATTACK"
		""
		"	Interrupts"
	)

	//=========================================================
	// > SCHED_ASSASSIN_JUMP_ATTACK
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_JUMP_ATTACK,
	
		"	Tasks"
		"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_ASSASSIN_JUMP_LAND"
		"		TASK_SET_ACTIVITY				ACTIVITY:ACT_FLY"
		"		TASK_ASSASSIN_FALL_TO_GROUND	0"
		""
		"	Interrupts"
	)

	//=========================================================
	// > SCHED_ASSASSIN_JUMP_LAND
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ASSASSIN_JUMP_LAND,
	
		"	Tasks"
		"		TASK_SET_FAIL_SCHEDULE				SCHEDULE:SCHED_ASSASSIN_EXPOSED"
//		"		TASK_SET_FAIL_SCHEDULE				SCHEDULE:SCHED_MELEE_ATTACK1"
		"		TASK_SET_ACTIVITY					ACTIVITY:ACT_IDLE"
		"		TASK_REMEMBER						MEMORY:CUSTOM1"
		"		TASK_FIND_NODE_COVER_FROM_ENEMY		0"
		"		TASK_RUN_PATH						0"
		"		TASK_FORGET							MEMORY:CUSTOM1"
		"		TASK_WAIT_FOR_MOVEMENT				0"
		"		TASK_REMEMBER						MEMORY:INCOVER"
		"		TASK_FACE_ENEMY						0"
		"		TASK_SET_FAIL_SCHEDULE				SCHEDULE:SCHED_RANGE_ATTACK1"
		""
		"	Interrupts"
	)
		
AI_END_CUSTOM_NPC()
