/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// Squadmonster  functions
//=========================================================

#include "cbase.h"
#include "basemonster.h"
#include "ai_squadslot.h"
#include "ai_squad.h"
#include "plane.h"

int g_iSquadIndex = 0;

//-----------------------------------------------------------------------------
// Purpose: If requested slot is available return true and take the slot
//			Otherwise return false
// Input  :
// Output :
//-----------------------------------------------------------------------------
bool CBaseMonster::OccupyStrategySlot( int squadSlotID )
{
	return OccupyStrategySlotRange( squadSlotID, squadSlotID );
}

//-----------------------------------------------------------------------------

bool CBaseMonster::OccupyStrategySlotRange( int slotIDStart, int slotIDEnd )
{
	// If I'm not in a squad a I don't fill slots
	return ( !m_pSquad || m_pSquad->OccupyStrategySlotRange( GetEnemy(), slotIDStart, slotIDEnd, &m_iMySquadSlot ) );

}

//=========================================================
// HasStrategySlot 
//=========================================================
bool CBaseMonster::HasStrategySlot( int squadSlotID )
{
	// If I wasn't taking up a squad slot I'm done
	return (m_iMySquadSlot == squadSlotID);
}

bool CBaseMonster::HasStrategySlotRange( int slotIDStart, int slotIDEnd )
{
	// If I wasn't taking up a squad slot I'm done
	if (m_iMySquadSlot < slotIDStart || m_iMySquadSlot > slotIDEnd)
	{
		return false;
	}
	return true;
}

//=========================================================
// VacateSlot 
//=========================================================

void CBaseMonster::VacateStrategySlot(void)
{
	if (m_pSquad)
	{
		m_pSquad->VacateStrategySlot(GetEnemy(), m_iMySquadSlot);
		m_iMySquadSlot = SQUAD_SLOT_NONE;
	}
}

//=========================================================
// NoFriendlyFire - checks for possibility of friendly fire
//
// Builds a large box in front of the grunt and checks to see 
// if any squad members are in that box. 
//=========================================================
bool CBaseMonster::NoFriendlyFire( void )
{
	if ( !m_pSquad )
	{
		return true;
	}

	CPlane	backPlane;
	CPlane  leftPlane;
	CPlane	rightPlane;

	Vector	vecLeftSide;
	Vector	vecRightSide;
	Vector	v_left;

	Vector  vForward, vRight, vAngleToEnemy;

	VectorAngles( ( GetEnemy()->Center() - pev->origin ), vAngleToEnemy );

	//!!!BUGBUG - to fix this, the planes must be aligned to where the monster will be firing its gun, not the direction it is facing!!!

	if ( GetEnemy() != NULL )
	{
		AngleVectors( vAngleToEnemy, &vForward, &vRight, NULL );
	}
	else
	{
		// if there's no enemy, pretend there's a friendly in the way, so the grunt won't shoot.
		return false;
	}

	vecLeftSide = pev->origin - ( vRight * ( pev->size.x * 1.5 ) );
	vecRightSide = pev->origin + ( vRight * ( pev->size.x * 1.5 ) );
	v_left = vRight * -1;

	leftPlane.InitializePlane ( vRight, vecLeftSide );
	rightPlane.InitializePlane ( v_left, vecRightSide );
	backPlane.InitializePlane ( vForward, pev->origin );

/*
	ALERT ( at_console, "LeftPlane: %f %f %f : %f\n", leftPlane.m_vecNormal.x, leftPlane.m_vecNormal.y, leftPlane.m_vecNormal.z, leftPlane.m_flDist );
	ALERT ( at_console, "RightPlane: %f %f %f : %f\n", rightPlane.m_vecNormal.x, rightPlane.m_vecNormal.y, rightPlane.m_vecNormal.z, rightPlane.m_flDist );
	ALERT ( at_console, "BackPlane: %f %f %f : %f\n", backPlane.m_vecNormal.x, backPlane.m_vecNormal.y, backPlane.m_vecNormal.z, backPlane.m_flDist );
*/

	AISquadIter_t iter;
	for ( CBaseMonster *pSquadMember = m_pSquad->GetFirstMember( &iter ); pSquadMember; pSquadMember = m_pSquad->GetNextMember( &iter ) )
	{
		if ( pSquadMember == this )
			 continue;

		if ( backPlane.PointInFront  ( pSquadMember->pev->origin ) &&
				leftPlane.PointInFront  ( pSquadMember->pev->origin ) && 
			 rightPlane.PointInFront ( pSquadMember->pev->origin) )
		{
			// this guy is in the check volume! Don't shoot!
			return false;
		}
	}

	return true;
}

//=========================================================
// FValidateCover - determines whether or not the chosen
// cover location is a good one to move to. (currently based
// on proximity to others in the squad)
//=========================================================
bool CBaseMonster::FValidateCover( const Vector &vecCoverLocation )
{
	if ( !m_pSquad )
	{
		return true;
	}

	if ( m_pSquad->SquadMemberInRange( vecCoverLocation, 128 ) )
	{
		// another squad member is too close to this piece of cover.
		return false;
	}

	return true;
}

//=========================================================
//
// SquadRecruit(), get some monsters of my classification and
// link them as a group.  returns the group size
//
//=========================================================
int CBaseMonster::SquadRecruit( int searchRadius )
{
	int squadCount;
	int iMyClass = Classify();// cache this monster's class
	CAI_Squad *pSquad = NULL;

	// Don't recruit if I'm already in a group
	if ( m_pSquad )
		return 0;

	// I am my own leader
	squadCount = 1;

	CBaseEntity *pEntity = NULL;

	if ( !FStringNull( pev->netname ) )
	{
		// we have a squadname?
		pSquad = g_AI_SquadManager.FindCreateSquad( this, pev->netname );

		// I have a netname, so unconditionally recruit everyone else with that name.
		pEntity = UTIL_FindEntityByString( pEntity, "netname", STRING( pev->netname ) );
		while ( pEntity )
		{
			CBaseMonster *pRecruit = pEntity->MyMonsterPointer();

			if ( pRecruit )
			{
				if ( !pRecruit->m_pSquad && pRecruit->Classify() == iMyClass && pRecruit != this )
				{
					// minimum protection here against user error.in worldcraft. 
					pSquad->AddToSquad( pRecruit );
					squadCount++;
				}
			}
	
			pEntity = UTIL_FindEntityByString( pEntity, "netname", STRING( pev->netname ) );
		}
	}
	else 
	{
		char szSquadName[64];
		Q_snprintf( szSquadName, sizeof( szSquadName ), "squad%d", g_iSquadIndex );

		pev->netname = MAKE_STRING( szSquadName );

		while ((pEntity = UTIL_FindEntityInSphere( pEntity, pev->origin, searchRadius )) != NULL)
		{
			CBaseMonster *pRecruit = pEntity->MyMonsterPointer( );

			if ( pRecruit && pRecruit != this && pRecruit->IsAlive() && !pRecruit->m_pCine )
			{
				// Can we recruit this guy?
				if ( !pRecruit->m_pSquad && pRecruit->Classify() == iMyClass &&
				   ( (iMyClass != CLASS_ALIEN_MONSTER) || FStrEq(STRING(pev->classname), STRING(pRecruit->pev->classname))) &&
				    FStringNull( pRecruit->pev->netname ) )
				{
					TraceResult tr;
					UTIL_TraceLine( EyePosition(), pRecruit->EyePosition(), ignore_monsters, pRecruit->edict(), &tr );// try to hit recruit with a traceline.
					if ( tr.flFraction == 1.0 )
					{
						//We're ready to recruit people, so start a squad if I don't have one.
						if ( !m_pSquad )
						{
							pSquad = g_AI_SquadManager.FindCreateSquad( this, pev->netname );
						}

						pRecruit->pev->netname = pev->netname;

						pSquad->AddToSquad( pRecruit );
						squadCount++;
					}
				}
			}
		}
	}

	// no single member squads
	if ( pSquad && squadCount == 1 )
	{
		pSquad->RemoveFromSquad( this );
		g_AI_SquadManager.DeleteSquad( pSquad );
	}

	if ( squadCount > 1 )
	{
		g_iSquadIndex++;
	}

	return squadCount;
}

//-----------------------------------------------------------------------------

void CBaseMonster::SetSquad( CAI_Squad *pSquad )	
{ 
	if ( m_pSquad == pSquad )
	{
		return;
	}

	if ( m_pSquad && m_iMySquadSlot != SQUAD_SLOT_NONE)
	{
		VacateStrategySlot();
	}

	m_pSquad = pSquad; 	
}
