//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef AI_SAVERESTORE_H
#define AI_SAVERESTORE_H

#if defined( _WIN32 )
#pragma once
#endif

class CAI_SaveRestoreHandler
{
public:
	
	// serialization
	void Save( CSave &pSave );
	void Restore( CRestore &pRestore );
};

//-----------------------------------------------------------------------------

extern CAI_SaveRestoreHandler g_AI_SaveRestoreHandler;

#endif // AI_SAVERESTORE_H