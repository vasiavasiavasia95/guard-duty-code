//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:	Squad classes
//
//=============================================================================//

#include "cbase.h"
#include "ai_squad.h"
#include "ai_squadslot.h"
#include "basemonster.h"
#include "saverestore_bitstring.h"
#include "saverestore_utlvector.h"


//-----------------------------------------------------------------------------

CAI_SquadManager g_AI_SquadManager;

//-----------------------------------------------------------------------------
// CAI_SquadManager
//
// Purpose: Manages all the squads in the system
//
//-----------------------------------------------------------------------------

CAI_Squad *CAI_SquadManager::FindSquad( string_t squadName )
{
	CAI_Squad* pSquad = m_pSquads;

	while (pSquad)
	{
		if (FStrEq(STRING(squadName),pSquad->GetName()))
		{
			return pSquad;
		}
		pSquad = pSquad->m_pNextSquad;
	}
	return NULL;
}

//-------------------------------------

CAI_Squad *CAI_SquadManager::CreateSquad(string_t squadName)
{
	CAI_Squad *pResult = new CAI_Squad(squadName);

	// ---------------------------------
	// Only named squads get added to the squad list
	if ( squadName != iStringNull )
	{
		pResult->m_pNextSquad = m_pSquads;
		m_pSquads = pResult;
	}
	else
		pResult->m_pNextSquad = NULL;
	return pResult;
}

//-------------------------------------

int CAI_SquadManager::NumSquads()
{
	int nSquads = 0;
	CAI_Squad* pSquad = m_pSquads;

	while (pSquad)
	{
		nSquads++;
		pSquad = pSquad->GetNext();
	}
	return nSquads;
}

//-------------------------------------

void CAI_SquadManager::DeleteSquad( CAI_Squad *pSquad )
{
	CAI_Squad *pCurSquad = m_pSquads;
	if (pCurSquad == pSquad)
	{
		g_AI_SquadManager.m_pSquads = pCurSquad->m_pNextSquad;
	}
	else
	{
		while (pCurSquad)
		{
			if (pCurSquad->m_pNextSquad == pSquad)
			{
				pCurSquad->m_pNextSquad = pCurSquad->m_pNextSquad->m_pNextSquad;
				break;
			}
			pCurSquad= pCurSquad->m_pNextSquad;
		}
	}
	delete pSquad;
}

//-------------------------------------
// Purpose: Delete all the squads (called between levels / loads)
//-------------------------------------

void CAI_SquadManager::DeleteAllSquads(void) 
{
	CAI_Squad *squad = CAI_SquadManager::m_pSquads;

	while (squad) 
	{
		CAI_Squad *temp = squad->m_pNextSquad;
		delete squad;
		squad = temp;
	}
	CAI_SquadManager::m_pSquads = NULL;
}

//-----------------------------------------------------------------------------
// CAI_Squad
//
// Purpose: Tracks enemies, squad slots, squad members
//
//-----------------------------------------------------------------------------

#ifdef PER_ENEMY_SQUADSLOTS
BEGIN_SIMPLE_DATADESC( AISquadEnemyInfo_t )

	DEFINE_FIELD( AISquadEnemyInfo_t,		hEnemy,	FIELD_EHANDLE ),
	DEFINE_BITSTRING( AISquadEnemyInfo_t,	slots),

END_DATADESC()
#endif

BEGIN_SIMPLE_DATADESC( CAI_Squad )

	// 							m_pNextSquad		(rebuilt)
	// 							m_Name				(rebuilt)
	// 							m_hSquadMember		(rebuilt)
  	// 			 				m_nNumMembers		(rebuilt)
 	DEFINE_FIELD( CAI_Squad,	m_flSquadSoundWaitTime,		FIELD_TIME ),
 	//							m_pLastFoundEnemyInfo  (think transient)

#ifdef PER_ENEMY_SQUADSLOTS
	DEFINE_UTLVECTOR(CAI_Squad,	m_EnemyInfos,				FIELD_EMBEDDED ),
	DEFINE_FIELD( CAI_Squad,	m_flEnemyInfoCleanupTime,	FIELD_TIME ),
#else
	DEFINE_EMBEDDED( CAI_Squad,	m_squadSlotsUsed ),
#endif

END_DATADESC()

//-------------------------------------

CAI_Squad::CAI_Squad(string_t newName) 
#ifndef PER_ENEMY_SQUADSLOTS
 :	m_squadSlotsUsed(MAX_SQUADSLOTS)
#endif
{
	Init( newName );
}

//-------------------------------------

CAI_Squad::CAI_Squad() 
#ifndef PER_ENEMY_SQUADSLOTS
 :	m_squadSlotsUsed(MAX_SQUADSLOTS)
#endif
{
	Init( iStringNull );
}

//-------------------------------------

void CAI_Squad::Init(string_t newName) 
{
	m_Name = newName;
	m_pNextSquad = NULL;
	m_flSquadSoundWaitTime = 0;
	m_nNumMembers = 0;

	for (int member= 0; member < MAX_SQUAD_MEMBERS; member++)
	{
		m_hSquadMember[member] = NULL;
	}
	m_flSquadSoundWaitTime	= 0;

#ifdef PER_ENEMY_SQUADSLOTS
	m_flEnemyInfoCleanupTime = 0;
	m_pLastFoundEnemyInfo = NULL;
#endif

}

//-------------------------------------

CAI_Squad::~CAI_Squad(void)
{
}

//-------------------------------------
// Purpose: Removes a monster from a squad
//-------------------------------------
void CAI_Squad::RemoveFromSquad( CBaseMonster *pMonster )
{
	if ( !pMonster )
		return;

	// Find the index of this squad member
	int member;
	int myIndex = -1;
	for ( member = 0; member < m_nNumMembers; member++)
	{
		if ( m_hSquadMember[member] == pMonster )
		{
			myIndex = member;
			break;
		}
	}
	if (myIndex == -1)
	{
		ALERT( at_error, "Attempting to remove non-existing squad member!\n");
		return;
	}
	// Now down-shift members to get rid of this one
	for (member = myIndex; member < m_nNumMembers-1; member++)
	{
		m_hSquadMember[member] = m_hSquadMember[member+1];
	}
	m_nNumMembers--;
	if (m_nNumMembers < 0)
	{
		m_nNumMembers = 0;
	}

	// Notify squad members of death 
	for (member = 0; member < m_nNumMembers; member++)
	{
		CBaseMonster* pSquadMem = (CBaseMonster *)((CBaseEntity *)m_hSquadMember[member]);
		if (pSquadMem)
		{
			pSquadMem->NotifyDeadFriend(pMonster);
		}
	}

	pMonster->SetSquad( NULL );
	pMonster->SetSquadName( iStringNull );
}

//-------------------------------------
// Purpose: Addes the given Monster to the squad
//-------------------------------------
void CAI_Squad::AddToSquad( CBaseMonster *pMonster )
{
	if ( !pMonster || !pMonster->IsAlive() )
	{
		Assert(0);
		return;
	}

	if ( pMonster->GetSquad() == this )
		return;

	if ( pMonster->GetSquad() )
	{
		pMonster->GetSquad()->RemoveFromSquad(pMonster);
	}

	if ( m_nNumMembers == MAX_SQUAD_MEMBERS )
	{
		ALERT( at_console, "Error!! Squad %s is too big!!! Replacing last member\n", STRING( m_Name ) );
		m_nNumMembers--;
	}
	m_hSquadMember[m_nNumMembers] = pMonster;
	pMonster->SetSquad( this );
	pMonster->SetSquadName( m_Name );
	m_nNumMembers++;
}

//-------------------------------------

CBaseMonster *CAI_Squad::SquadMemberInRange( const Vector &vecLocation, float flDist )
{
	for (int i = 0; i < m_nNumMembers; i++)
	{
		if ( m_hSquadMember[i] != NULL && (vecLocation - m_hSquadMember[i]->pev->origin ).Length2D() <= flDist)
			return m_hSquadMember[i]->MyMonsterPointer();
	}
	return false;
}

//-------------------------------------
// Purpose: Returns the nearest squad member to the given squad member
//-------------------------------------

CBaseMonster *CAI_Squad::NearestSquadMember( CBaseMonster *pMember )
{
	float			fBestDist	= 4096;
	CBaseMonster		*fNearestEnt = NULL;
	Vector			fStartLoc = pMember->pev->origin;
	for (int i = 0; i < m_nNumMembers; i++)
	{
		if (m_hSquadMember[i] != NULL)
		{
			float fDist = (fStartLoc - m_hSquadMember[i]->pev->origin).Length();
			if (m_hSquadMember[i]	!=	pMember	&&
				fDist				<	fBestDist	)
			{
				fBestDist	= fDist;
				fNearestEnt	= (CBaseMonster *)((CBaseEntity *)m_hSquadMember[i]);
			}
		}
	}
	return fNearestEnt;
}

//-------------------------------------
// Purpose: Return the number of squad members visible to the specified member
//-------------------------------------
int	CAI_Squad::GetVisibleSquadMembers( CBaseMonster *pMember )
{
	int iCount = 0;

	for (int i = 0; i < m_nNumMembers; i++)
	{
		// Make sure it's not the specified member
		if ( m_hSquadMember[i] != NULL && pMember != m_hSquadMember[i] )
		{
			if ( pMember->FVisible( m_hSquadMember[i] ) )
			{
				iCount++;
			}
		}
	}

	return iCount;
}

//-------------------------------------
// Purpose: Returns true if given entity is in the squad
//-------------------------------------
bool CAI_Squad::SquadIsMember( CBaseEntity *pMember )
{
	CBaseMonster *pMonster = pMember->MyMonsterPointer();
	if ( pMonster && pMonster->GetSquad() == this )
		return true;
	
	return false;
}

//-------------------------------------

bool CAI_Squad::IsLeader( CBaseMonster *pLeader )
{
	if ( !pLeader )
		return false;

	if ( GetLeader() == pLeader )
		return true;

	return false;
}

//-------------------------------------

CBaseMonster * CAI_Squad::GetLeader( void )
{
	CBaseMonster *pLeader = NULL;
	for ( int i = 0; i < m_nNumMembers; i++ )
	{
		if ( !pLeader )
			pLeader = m_hSquadMember[i]->MyMonsterPointer();
	}
	return ( m_nNumMembers > 1) ? pLeader : NULL;
}

//-------------------------------------
// Purpose: Alert everyone in the squad to the presence of a new enmey
//-------------------------------------

int	CAI_Squad::NumMembers( void )
{
	return m_nNumMembers;
}

//-------------------------------------
// Purpose: Alert everyone in the squad to the presence of a new enmey
//-------------------------------------

void CAI_Squad::SquadNewEnemy( CBaseEntity *pEnemy )
{
	if ( !pEnemy )
	{
		ALERT( at_error, "SquadNewEnemy() - pEnemy is NULL!\n" );
		return;
	}

	for (int i = 0; i < m_nNumMembers; i++)
	{
		CBaseMonster *pMember = (CBaseMonster *)((CBaseEntity *)m_hSquadMember[i]);
		if (pMember)
		{
			// reset members who aren't activly engaged in fighting (only do this if the monster's using the squad memory, or it'll fail)
			if ( !pMember->GetEnemy() || 
				 ( pMember->GetEnemy() != pEnemy && 
				   !pMember->HasCondition( COND_SEE_ENEMY) &&
				   gpGlobals->time - pMember->GetEnemyLastTimeSeen() > 3.0 ) )
			{
				// give them a new enemy
				pMember->SetEnemy( pEnemy );
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: Try to get one of a contiguous range of slots
// Input  : slotIDStart - start of slot range
//			slotIDEnd - end of slot range
//			hEnemy - enemy this slot is for
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CAI_Squad::OccupyStrategySlotRange( CBaseEntity *pEnemy, int slotIDStart, int slotIDEnd, int *pSlot )
{
#ifndef PER_ENEMY_SQUADSLOTS
	// FIXME: combat slots need to be per enemy, not per squad.  
	// As it is, once a squad is occupied it stops making even simple attacks to other things nearby.
	// This code may make soldiers too aggressive
	if (pEnemy != GetLeader()->GetEnemy())
	{
		*pSlot = SQUAD_SLOT_NONE;
		return true;
	}
#endif

	// If I'm already occupying this slot
	if ( *pSlot >= slotIDStart && *pSlot <= slotIDEnd)
		return true;

	for ( int i = slotIDStart; i <= slotIDEnd; i++ )
	{
		// Check enemy to see if slot already occupied
		if (!IsSlotOccupied(pEnemy, i))
		{
			// Clear any previous spot;
			if (*pSlot != SQUAD_SLOT_NONE)
			{
				// As a debug measure check to see if slot was filled
				if (!IsSlotOccupied(pEnemy, *pSlot))
				{
					ALERT( at_console, "ERROR! Vacating an empty slot!\n" );
				}

				// Free the slot
				VacateSlot(pEnemy, *pSlot);
			}

			// Fill the slot
			OccupySlot(pEnemy, i);
			*pSlot = i;
			return true;
		}
	}
	return false;
}

//------------------------------------------------------------------------------

void CAI_Squad::VacateStrategySlot( CBaseEntity *pEnemy, int slot)
{
	// If I wasn't taking up a squad slot I'm done
	if (slot == SQUAD_SLOT_NONE)
		return;

	// As a debug measure check to see if slot was filled
	if (!IsSlotOccupied(pEnemy, slot))
	{
		ALERT( at_console, "ERROR! Vacating an empty slot!\n" );
	}

	// Free the slot
	VacateSlot(pEnemy, slot);
}

//------------------------------------------------------------------------------

void CAI_Squad::UpdateEnemyMemory( CBaseMonster *pUpdater, CBaseEntity *pEnemy, const Vector &position )
{
	//Broadcast to all members of the squad
	for ( int i = 0; i < m_nNumMembers; i++ )
	{
		if ( m_hSquadMember[i] != pUpdater )
		{
			((CBaseMonster *)(CBaseEntity *)m_hSquadMember[i])->UpdateEnemyMemory( pEnemy, position, false );
		}
	}
}

//------------------------------------------------------------------------------

#ifdef PER_ENEMY_SQUADSLOTS

AISquadEnemyInfo_t *CAI_Squad::FindEnemyInfo( CBaseEntity *pEnemy )
{
	int i;
	if ( gpGlobals->time > m_flEnemyInfoCleanupTime )
	{
		if ( m_EnemyInfos.Count() )
		{
			m_pLastFoundEnemyInfo = NULL;
			CUtlRBTree<CBaseEntity *> activeEnemies;
			SetDefLessFunc( activeEnemies );

			// Gather up the set of active enemies
			for ( i = 0; i < m_nNumMembers; i++ )
			{
				CBaseEntity *pMemberEnemy = ((CBaseMonster *)(CBaseEntity *)m_hSquadMember[i])->GetEnemy();
				if ( pMemberEnemy && activeEnemies.Find( pMemberEnemy ) == activeEnemies.InvalidIndex() )
				{
					activeEnemies.Insert( pMemberEnemy );
				}
			}
			
			// Remove the records for deleted or unused enemies
			for ( i = m_EnemyInfos.Count() - 1; i >= 0; --i )
			{
				if ( m_EnemyInfos[i].hEnemy == NULL || activeEnemies.Find( m_EnemyInfos[i].hEnemy ) == activeEnemies.InvalidIndex() )
				{
					m_EnemyInfos.FastRemove( i );
				}
			}
		}
		
		m_flEnemyInfoCleanupTime = gpGlobals->time + 30;
	}

	if ( m_pLastFoundEnemyInfo && m_pLastFoundEnemyInfo->hEnemy == pEnemy )
		return m_pLastFoundEnemyInfo;

	for ( i = 0; i < m_EnemyInfos.Count(); i++ )
	{
		if ( m_EnemyInfos[i].hEnemy == pEnemy )
		{
			m_pLastFoundEnemyInfo = &m_EnemyInfos[i];
			return &m_EnemyInfos[i];
		}
	}

	m_pLastFoundEnemyInfo = NULL;
	i = m_EnemyInfos.AddToTail();
	m_EnemyInfos[i].hEnemy = pEnemy;

	m_pLastFoundEnemyInfo = &m_EnemyInfos[i];
	return &m_EnemyInfos[i];
}

#endif
	
//------------------------------------------------------------------------------

void CAI_Squad::OccupySlot( CBaseEntity *pEnemy, int i )			
{ 
#ifdef PER_ENEMY_SQUADSLOTS
	AISquadEnemyInfo_t *pInfo = FindEnemyInfo( pEnemy );
	pInfo->slots.Set(i);
#else
	m_squadSlotsUsed.Set(i); 
#endif
}

//------------------------------------------------------------------------------

void CAI_Squad::VacateSlot( CBaseEntity *pEnemy, int i )			
{ 
#ifdef PER_ENEMY_SQUADSLOTS
	AISquadEnemyInfo_t *pInfo = FindEnemyInfo( pEnemy );
	pInfo->slots.Clear(i);
#else
	m_squadSlotsUsed.Clear(i); 
#endif
}

//------------------------------------------------------------------------------

bool CAI_Squad::IsSlotOccupied( CBaseEntity *pEnemy, int i ) const	
{ 
#ifdef PER_ENEMY_SQUADSLOTS
	const AISquadEnemyInfo_t *pInfo = FindEnemyInfo( pEnemy );
	return pInfo->slots.IsBitSet(i);
#else
	return m_squadSlotsUsed.IsBitSet(i);
#endif
}

//=============================================================================
