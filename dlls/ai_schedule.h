/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// Scheduling 
//=========================================================

#include "bitvec.h"

#ifndef	SCHEDULE_H
#define	SCHEDULE_H
#ifdef _WIN32
#pragma once
#endif

class   CAI_ClassScheduleIdSpace;
class	CBaseMonster;

struct	Task_t;

#ifndef MAX_CONDITIONS
#define	MAX_CONDITIONS 32*3
#endif
typedef CBitVec<MAX_CONDITIONS> CAI_ScheduleBits;

// These go in the flData member of the TASK_WALK_TO_TARGET, TASK_RUN_TO_TARGET
enum 
{
	TARGET_MOVE_NORMAL = 0,
	TARGET_MOVE_SCRIPTED = 1,
};

// A goal should be used for a task that requires several schedules to complete.  
// The goal index should indicate which schedule (ordinally) the monster is running.  
// That way, when tasks fail, the AI can make decisions based on the context of the 
// current goal and sequence rather than just the current schedule.
enum
{
	GOAL_ATTACK_ENEMY,
	GOAL_MOVE,
	GOAL_TAKE_COVER,
	GOAL_MOVE_TARGET,
	GOAL_EAT,
};

//=============================================================================
// >> CAI_Schedule
//=============================================================================

class CAI_Schedule;

class CAI_SchedulesManager
{
public:
	CAI_SchedulesManager()
	{
		allSchedules = NULL;
		m_CurLoadSig = 0;		// Note when schedules reset
	}

	int				GetScheduleLoadSignature() { return m_CurLoadSig; }
	CAI_Schedule*	GetScheduleFromID( int schedID );	// Function to return schedule from linked list 
	CAI_Schedule*	GetScheduleByName( const char *name );

	bool LoadAllSchedules(void);

	bool LoadSchedules( const char* prefix, CAI_ClassScheduleIdSpace *pIdSpace  );
	bool LoadSchedulesFromBuffer( const char *prefix, char *pfile, CAI_ClassScheduleIdSpace *pIdSpace );

private:
	friend class CAI_SystemHook;
	
	int				m_CurLoadSig;					// Note when schedules reset
	CAI_Schedule*	allSchedules;						// A linked list of all schedules

	CAI_Schedule *	CreateSchedule(char *name, int schedule_id);

	void CreateStringRegistries( void );
	void DestroyStringRegistries( void );
	void DeleteAllSchedules(void);

	//static bool	LoadSchedules( char* prefix,	int taskIDOffset,	int taskENOffset,
	//											int schedIDOffset,  int schedENOffset,
	//											int condIDOffset,	int condENOffset);

	// parsing helpers
	int	GetStateID(const char *state_name);
	int	GetMemoryID(const char *memory_name);
	int	GetActivityID(const char* actName);

};

extern CAI_SchedulesManager g_AI_SchedulesManager;

class CAI_Schedule
{
// ---------
//	Static
// ---------
// ---------
public:
	int GetId() const
	{
		return m_iScheduleID;
	}
	
	const Task_t *GetTaskList() const
	{
		return m_pTaskList;
	}
	
	int NumTasks() const
	{
		return m_iNumTasks;
	}
	
	void GetInterruptMask( CAI_ScheduleBits *pBits ) const
	{
		m_InterruptMask.CopyTo( pBits );
	}

	bool HasInterrupt( int condition ) const
	{
		return m_InterruptMask.IsBitSet( condition );
	}
	
	const char *GetName() const
	{
		return m_pName;
	}
	
private:
	friend class CAI_SchedulesManager;

	int			m_iScheduleID;				// The id number of this schedule

	Task_t		*m_pTaskList;
	int			m_iNumTasks;	 

	CAI_ScheduleBits m_InterruptMask;			// a bit mask of conditions that can interrupt this schedule 
	char		*m_pName;

	CAI_Schedule *nextSchedule;				// The next schedule in the list of schedules

	CAI_Schedule(char *name,int schedule_id, CAI_Schedule *pNext);
	~CAI_Schedule( void );
};

//-----------------------------------------------------------------------------
//
// In-memory schedules
//

#define AI_DEFINE_SCHEDULE( name, text ) \
	const char * g_psz##name = \
		"\n	Schedule" \
		"\n		" #name \
		text \
		"\n"


#define AI_LOAD_SCHEDULE( classname, name ) \
	do \
	{ \
		extern const char * g_psz##name; \
		if ( classname::gm_SchedLoadStatus.fValid ) \
		{ \
			classname::gm_SchedLoadStatus.fValid = g_AI_SchedulesManager.LoadSchedulesFromBuffer( #classname,(char *)g_psz##name,&classname::gm_ClassScheduleIdSpace ); \
		} \
	} while (false)


// For loading default schedules in memory  (see ai_default.cpp)
#define AI_LOAD_DEF_SCHEDULE( classname, name ) \
	do \
	{ \
		extern const char * g_psz##name; \
		if (!g_AI_SchedulesManager.LoadSchedulesFromBuffer( #classname,(char *)g_psz##name,&classname::gm_ClassScheduleIdSpace )) \
			return false; \
	} while (false)


//-----------------------------------------------------------------------------

// an array of waypoints makes up the monster's route. 
// !!!LATER- this declaration doesn't belong in this file.
struct WayPoint_t
{
	Vector	vecLocation;
	int		iType;
};

// these MoveFlag values are assigned to a WayPoint's TYPE in order to demonstrate the 
// type of movement the monster should use to get there.
#define bits_MF_TO_TARGETENT		( 1 << 0 ) // local move to targetent.
#define bits_MF_TO_ENEMY			( 1 << 1 ) // local move to enemy
#define bits_MF_TO_COVER			( 1 << 2 ) // local move to a hiding place
#define bits_MF_TO_DETOUR			( 1 << 3 ) // local move to detour point.
#define bits_MF_TO_PATHCORNER		( 1 << 4 ) // local move to a path corner
#define bits_MF_TO_NODE				( 1 << 5 ) // local move to a node
#define bits_MF_TO_LOCATION			( 1 << 6 ) // local move to an arbitrary point
#define bits_MF_IS_GOAL				( 1 << 7 ) // this waypoint is the goal of the whole move.
#define bits_MF_DONT_SIMPLIFY		( 1 << 8 ) // Don't let the route code simplify this waypoint

// If you define any flags that aren't _TO_ flags, add them here so we can mask
// them off when doing compares.
#define bits_MF_NOT_TO_MASK (bits_MF_IS_GOAL | bits_MF_DONT_SIMPLIFY)

#define MOVEGOAL_NONE				(0)
#define MOVEGOAL_TARGETENT			(bits_MF_TO_TARGETENT)
#define MOVEGOAL_ENEMY				(bits_MF_TO_ENEMY)
#define MOVEGOAL_PATHCORNER			(bits_MF_TO_PATHCORNER)
#define MOVEGOAL_LOCATION			(bits_MF_TO_LOCATION)
#define MOVEGOAL_NODE				(bits_MF_TO_NODE)

#endif	// SCHEDULE_H
