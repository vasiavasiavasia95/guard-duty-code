//========= Copyright (c) 1996-2002, Valve LLC, All rights reserved. ==========
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================

#ifndef AI_COMPONENT_H
#define AI_COMPONENT_H

#if defined( _WIN32 )
#pragma once
#endif

class CBaseMonster;
struct Task_t;

//-----------------------------------------------------------------------------
// CAI_Component
//
// Purpose: Shared functionality of all classes that assume some of the 
//			responsibilities of an owner AI. 
//-----------------------------------------------------------------------------

class CAI_Component
{
protected:
	CAI_Component( CBaseMonster *pOuter = NULL )
	 : m_pOuter(pOuter)
	{
	}

	virtual ~CAI_Component() {}

public:
	virtual void SetOuter( CBaseMonster *pOuter )	{ m_pOuter = pOuter; }

	CBaseMonster *		GetOuter() 			{ return m_pOuter; }
	const CBaseMonster *	GetOuter() const 	{ return m_pOuter; }

protected:
	//
	// Common services provided by CBaseMonster, Convenience methods to simplify derived code
	//

	const Vector &		GetOrigin() const;
	void 				SetOrigin( const Vector &origin );

	void				SetAngles( const Vector &angles );
	const Vector &		GetAngles( void ) const;
	
	float				GetGravity() const;
	void				SetGravity( float );

	CBaseEntity*		GetEnemy();
	const Vector &		GetEnemyLKP() const;
	
	CBaseEntity*		GetTarget();
	void				SetTarget( CBaseEntity *pTarget );
	
	const Task_t*		GetCurTask( void );
	virtual void		TaskFail( void );
	virtual void		TaskComplete( bool fIgnoreSetFailedCondition = false );
	int					TaskIsRunning();
	inline int			TaskIsComplete();

	Activity			GetActivity();
	void				SetActivity( Activity NewActivity );
	int					GetSequence();

	int					GetEntFlags() const;
	void				AddEntFlag( int flags );
	void				RemoveEntFlag( int flagsToRemove );
	void				ToggleEntFlag( int flagToToggle );

	CBaseEntity*		GetGoalEnt();
	void				SetGoalEnt( CBaseEntity *pGoalEnt );
	
	void				Remember( int iMemory );
	void				Forget( int iMemory );
	bool				HasMemory( int iMemory );

	const char * 		GetEntClassname();
	
	int					CapabilitiesGet();
private:
	CBaseMonster *m_pOuter;
};

//-----------------------------------------------------------------------------

template <class MONSTER_CLASS, class BASE_COMPONENT = CAI_Component>
class CAI_ComponentWithOuter : public BASE_COMPONENT
{
protected:
	CAI_ComponentWithOuter(MONSTER_CLASS *pOuter = NULL)
	 : BASE_COMPONENT(pOuter)
	{
	}

public:
	// Hides base version
	void SetOuter( MONSTER_CLASS *pOuter )		{ BASE_COMPONENT::SetOuter((CBaseMonster *)pOuter); }
	MONSTER_CLASS * 		GetOuter() 			{ return (MONSTER_CLASS *)(BASE_COMPONENT::GetOuter()); }
	const MONSTER_CLASS *	GetOuter() const 	{ return (MONSTER_CLASS *)(BASE_COMPONENT::GetOuter()); }
};

//-----------------------------------------------------------------------------

#define DEFINE_AI_COMPONENT_OUTER( MONSTER_CLASS ) \
	void SetOuter( MONSTER_CLASS *pOuter )		{ CAI_Component::SetOuter((CBaseMonster *)pOuter); } \
	MONSTER_CLASS * 		GetOuter() 			{ return (MONSTER_CLASS *)(CAI_Component::GetOuter()); } \
	const MONSTER_CLASS *	GetOuter() const 	{ return (MONSTER_CLASS *)(CAI_Component::GetOuter()); }

//-----------------------------------------------------------------------------

#endif // AI_COMPONENT_H
