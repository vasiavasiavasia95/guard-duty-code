//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Houndeye - a spooky sonic dog.
//
// $NoKeywords: $
//=============================================================================//

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_squad.h"
#include	"ai_squadslot.h"
#include	"ai_schedule.h"
#include	"animation.h"
#include	"nodes.h"
#include	"soundent.h"
#include	"game.h"

extern cvar_t sk_houndeye_health;
extern cvar_t sk_houndeye_dmg_blast;

extern CGraph WorldGraph;

// houndeye does 20 points of damage spread over a sphere 384 units in diameter, and each additional 
// squad member increases the BASE damage by 110%, per the spec.
#define HOUNDEYE_MAX_SQUAD_SIZE			4
#define	HOUNDEYE_MAX_ATTACK_RADIUS		384
#define	HOUNDEYE_SQUAD_BONUS			(float)1.1

#define HOUNDEYE_EYE_FRAMES 4 // how many different switchable maps for the eye

#define HOUNDEYE_SOUND_STARTLE_VOLUME	128 // how loud a sound has to be to badly scare a sleeping houndeye

//=========================================================
// monster-specific tasks
//=========================================================
enum
{
	TASK_HOUND_CLOSE_EYE = LAST_SHARED_TASK,
	TASK_HOUND_OPEN_EYE,
	TASK_HOUND_THREAT_DISPLAY,
	TASK_HOUND_FALL_ASLEEP,
	TASK_HOUND_WAKE_UP,
	TASK_HOUND_HOP_BACK
};

//=========================================================
// monster-specific schedule types
//=========================================================
enum
{
	SCHED_HOUND_GUARD_PACK = LAST_SHARED_SCHEDULE,
	SCHED_HOUND_RANGE_ATTACK1,
	SCHED_HOUND_RANGE_ATTACK2,
	SCHED_HOUND_SLEEP,
	SCHED_HOUND_WAKE_LAZY,
	SCHED_HOUND_WAKE_URGENT,
	SCHED_HOUND_SPECIAL_ATTACK,
	SCHED_HOUND_AGITATED,
	SCHED_HOUND_HOP_RETREAT,
	SCHED_HOUND_COMBAT_FAIL_PVS,
	SCHED_HOUND_COMBAT_FAIL_NOPVS,
};

//=========================================================
// monster-specific squad slots
//=========================================================
enum HoundEyeSquadSlots
{	
	SQUAD_SLOTS_HOUND_ATTACK1 = LAST_SHARED_SQUADSLOT,
	SQUAD_SLOTS_HOUND_ATTACK2,
	SQUAD_SLOTS_HOUND_ATTACK3,
};

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		HOUND_AE_WARN			1
#define		HOUND_AE_STARTATTACK	2
#define		HOUND_AE_THUMP			3
#define		HOUND_AE_ANGERSOUND1	4
#define		HOUND_AE_ANGERSOUND2	5
#define		HOUND_AE_HOPBACK		6
#define		HOUND_AE_CLOSE_EYE		7

class CHoundeye : public CBaseMonster
{
	DECLARE_CLASS( CHoundeye, CBaseMonster );

public:

	void			Spawn( void );
	void			Precache( void );
	Class_T			Classify ( void );
	void			HandleAnimEvent( animevent_t *pEvent );
	void			SetYawSpeed( void );

	void			WarmUpSound( void );
	void			AlertSound( void );
	void			DeathSound( void );
	void			WarnSound( void );
	void			PainSound( void );
	void			IdleSound( void );

	void			StartTask( const Task_t *pTask );
	void			RunTask ( const Task_t *pTask );

	void			SonicAttack( void );
	void			PrescheduleThink( void );
	int				TranslateSequence( Activity &idealActivity );
	void			WriteBeamColor ( void );
	bool			CheckRangeAttack1( float flDot, float flDist );
	bool			FValidateHintType ( short sHint );
	bool			FCanActiveIdle ( void );
	bool			FCanLeaderLook( void );

	int				TranslateSchedule( int scheduleType );
	int				SelectSchedule( void );

	DEFINE_CUSTOM_AI;
	DECLARE_DATADESC();

private:

	int		m_iSpriteTexture;
	bool	m_fAsleep;			// some houndeyes sleep in idle mode if this is set, the houndeye is lying down
	bool	m_fDontBlink;		// don't try to open/close eye if this bit is set!
	Vector	m_vecPackCenter;	// the center of the pack. The leader maintains this by averaging the origins of all pack members.
	bool	m_fStopImplosion;	// Hack. If this is TRUE implosion effect won't get updated anymore
};

LINK_ENTITY_TO_CLASS( monster_houndeye, CHoundeye );

BEGIN_DATADESC( CHoundeye )

	DEFINE_FIELD( CHoundeye, m_fAsleep, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHoundeye, m_fDontBlink, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHoundeye, m_fStopImplosion, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHoundeye, m_vecPackCenter, FIELD_POSITION_VECTOR ),

END_DATADESC()

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CHoundeye::Classify( void )
{
	return CLASS_ALIEN_MONSTER;
}

//=========================================================
//  FValidateHintType 
//=========================================================
bool CHoundeye::FValidateHintType( short sHint )
{
	int i;

	static short sHoundHints[] =
	{
		HINT_WORLD_MACHINERY,
		HINT_WORLD_BLINKING_LIGHT,
		HINT_WORLD_HUMAN_BLOOD,
		HINT_WORLD_ALIEN_BLOOD,
	};

	for ( i = 0 ; i < ARRAYSIZE ( sHoundHints ) ; i++ )
	{
		if ( sHoundHints[ i ] == sHint )
		{
			return true;
		}
	}

	ALERT ( at_aiconsole, "Couldn't validate hint type" );
	return false;
}


//=========================================================
// FCanActiveIdle
//=========================================================
bool CHoundeye::FCanActiveIdle( void )
{
	if ( m_pSquad )
	{
		AISquadIter_t iter;
		for ( CBaseMonster *pMember = m_pSquad->GetFirstMember( &iter ); pMember; pMember = m_pSquad->GetNextMember( &iter ) )
		{
			if ( pMember != this && pMember->m_iHintNode != NO_NODE )
			{
				// someone else in the group is active idling right now!
				return false;
			}
		}

		return true;
	}

	return true;
}

//=========================================================
// FCanActiveIdle
//=========================================================
bool CHoundeye::FCanLeaderLook( void )
{
	if ( m_pSquad && m_pSquad->IsLeader( this ) )
	{
		AISquadIter_t iter;
		for ( CBaseMonster *pMember = m_pSquad->GetFirstMember( &iter ); pMember; pMember = m_pSquad->GetNextMember( &iter ) )
		{
			if ( pMember != this && ((CHoundeye *)pMember)->m_fAsleep )
			{
				// this squad is sleeping. Perfor them leaderlook.
				return true;
			}
		}
	}

	return false;
}


//=========================================================
// CheckRangeAttack1 - overridden for houndeyes so that they
// try to get within half of their max attack radius before
// attacking, so as to increase their chances of doing damage.
//=========================================================
bool CHoundeye::CheckRangeAttack1( float flDot, float flDist )
{
	if ( flDist <= ( HOUNDEYE_MAX_ATTACK_RADIUS * 0.5 ) && flDot >= 0.3 )
	{
		return true;
	}
	return false;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CHoundeye :: SetYawSpeed ( void )
{
	int ys;

	ys = 90;

	switch ( m_Activity )
	{
	case ACT_CROUCHIDLE://sleeping!
		ys = 0;
		break;
	case ACT_IDLE:	
		ys = 60;
		break;
	case ACT_WALK:
		ys = 90;
		break;
	case ACT_RUN:	
		ys = 90;
		break;
	case ACT_TURN_LEFT:
	case ACT_TURN_RIGHT:
		ys = 90;
		break;
	}

	pev->yaw_speed = ys;
}

//=========================================================
// SetActivity 
//=========================================================
int CHoundeye::TranslateSequence( Activity &idealActivity )
{
	if ( m_MonsterState == MONSTERSTATE_COMBAT && idealActivity == ACT_IDLE && RANDOM_LONG(0,1) )
	{
		// play pissed idle.
		return LookupSequence( "madidle" );
	}
	
	return BaseClass::TranslateSequence( idealActivity );
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CHoundeye :: HandleAnimEvent( animevent_t *pEvent )
{
	switch ( pEvent->event )
	{
		case HOUND_AE_WARN:
			// do stuff for this event.
			WarnSound();
			break;

		case HOUND_AE_STARTATTACK:
			WarmUpSound();
			break;

		case HOUND_AE_HOPBACK:
			{
				float flGravity = g_psv_gravity->value;

				pev->flags &= ~FL_ONGROUND;

				Vector forward;
				AngleVectors( pev->angles, &forward );
				pev->velocity = forward * -200;
				pev->velocity.z += (0.6 * flGravity) * 0.5;

				break;
			}

		case HOUND_AE_THUMP:
			// emit the shockwaves
			SonicAttack();
			break;

		case HOUND_AE_ANGERSOUND1:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "houndeye/he_pain3.wav", 1, ATTN_NORM);	
			break;

		case HOUND_AE_ANGERSOUND2:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "houndeye/he_pain1.wav", 1, ATTN_NORM);	
			break;

		case HOUND_AE_CLOSE_EYE:
			if ( !m_fDontBlink )
			{
				pev->skin = HOUNDEYE_EYE_FRAMES - 1;
			}
			break;

		default:
			BaseClass::HandleAnimEvent( pEvent );
			break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CHoundeye :: Spawn()
{
	Precache( );

	SET_MODEL(ENT(pev), "models/houndeye.mdl");
	UTIL_SetSize(pev, Vector ( -16, -16, 0 ), Vector ( 16, 16, 36 ) );

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_YELLOW;
	pev->effects		= 0;
	pev->health			= sk_houndeye_health.value;
	pev->yaw_speed		= 5;//!!! should we put this in the monster's changeanim function since turn rates may vary with state/anim?
	m_flFieldOfView		= 0.5;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;
	m_fAsleep			= FALSE; // everyone spawns awake
	m_fDontBlink		= FALSE;
	
	CapabilitiesClear();
	CapabilitiesAdd( bits_CAP_SQUAD );

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CHoundeye :: Precache()
{
	PRECACHE_MODEL("models/houndeye.mdl");

	PRECACHE_SOUND("houndeye/he_alert1.wav");
	PRECACHE_SOUND("houndeye/he_alert2.wav");
	PRECACHE_SOUND("houndeye/he_alert3.wav");

	PRECACHE_SOUND("houndeye/he_die1.wav");
	PRECACHE_SOUND("houndeye/he_die2.wav");
	PRECACHE_SOUND("houndeye/he_die3.wav");

	PRECACHE_SOUND("houndeye/he_idle1.wav");
	PRECACHE_SOUND("houndeye/he_idle2.wav");
	PRECACHE_SOUND("houndeye/he_idle3.wav");

	PRECACHE_SOUND("houndeye/he_hunt1.wav");
	PRECACHE_SOUND("houndeye/he_hunt2.wav");
	PRECACHE_SOUND("houndeye/he_hunt3.wav");

	PRECACHE_SOUND("houndeye/he_pain1.wav");
	PRECACHE_SOUND("houndeye/he_pain3.wav");
	PRECACHE_SOUND("houndeye/he_pain4.wav");
	PRECACHE_SOUND("houndeye/he_pain5.wav");

	PRECACHE_SOUND("houndeye/he_attack1.wav");
	PRECACHE_SOUND("houndeye/he_attack3.wav");

	PRECACHE_SOUND("houndeye/he_blast1.wav");
	PRECACHE_SOUND("houndeye/he_blast2.wav");
	PRECACHE_SOUND("houndeye/he_blast3.wav");

	m_iSpriteTexture = PRECACHE_MODEL( "sprites/shockwave.spr" );
}	

//=========================================================
// IdleSound
//=========================================================
void CHoundeye :: IdleSound ( void )
{
	switch ( RANDOM_LONG(0,2) )
	{
	case 0:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_idle1.wav", 1, ATTN_NORM );	
		break;
	case 1:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_idle2.wav", 1, ATTN_NORM );	
		break;
	case 2:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_idle3.wav", 1, ATTN_NORM );	
		break;
	}
}

//=========================================================
// IdleSound
//=========================================================
void CHoundeye :: WarmUpSound ( void )
{
	switch ( RANDOM_LONG(0,1) )
	{
	case 0:
		EMIT_SOUND( ENT(pev), CHAN_WEAPON, "houndeye/he_attack1.wav", 0.7, ATTN_NORM );	
		break;
	case 1:
		EMIT_SOUND( ENT(pev), CHAN_WEAPON, "houndeye/he_attack3.wav", 0.7, ATTN_NORM );	
		break;
	}
}

//=========================================================
// WarnSound 
//=========================================================
void CHoundeye :: WarnSound ( void )
{
	switch ( RANDOM_LONG(0,2) )
	{
	case 0:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_hunt1.wav", 1, ATTN_NORM );	
		break;
	case 1:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_hunt2.wav", 1, ATTN_NORM );	
		break;
	case 2:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_hunt3.wav", 1, ATTN_NORM );	
		break;
	}
}

//=========================================================
// AlertSound 
//=========================================================
void CHoundeye :: AlertSound ( void )
{
	if ( m_pSquad && !m_pSquad->IsLeader( this ) )
	{
		return; // only leader makes ALERT sound.
	}

	switch ( RANDOM_LONG(0,2) )
	{
	case 0:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_alert1.wav", 1, ATTN_NORM );	
		break;
	case 1:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_alert2.wav", 1, ATTN_NORM );	
		break;
	case 2:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_alert3.wav", 1, ATTN_NORM );	
		break;
	}
}

//=========================================================
// DeathSound 
//=========================================================
void CHoundeye :: DeathSound ( void )
{
	switch ( RANDOM_LONG(0,2) )
	{
	case 0:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_die1.wav", 1, ATTN_NORM );	
		break;
	case 1:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_die2.wav", 1, ATTN_NORM );	
		break;
	case 2:
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_die3.wav", 1, ATTN_NORM );	
		break;
	}
}

//=========================================================
// PainSound 
//=========================================================
void CHoundeye :: PainSound ( void )
{
	switch ( RANDOM_LONG(0,2) )
	{
	case 0:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_pain3.wav", 1, ATTN_NORM );	
		break;
	case 1:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_pain4.wav", 1, ATTN_NORM );	
		break;
	case 2:	
		EMIT_SOUND( ENT(pev), CHAN_VOICE, "houndeye/he_pain5.wav", 1, ATTN_NORM );	
		break;
	}
}

//=========================================================
// WriteBeamColor - writes a color vector to the network 
// based on the size of the group. 
//=========================================================
void CHoundeye :: WriteBeamColor ( void )
{
	BYTE	bRed, bGreen, bBlue;

	if ( m_pSquad )
	{
		switch ( m_pSquad->NumMembers() )
		{
		case 2:
			// no case for 0 or 1, cause those are impossible for monsters in Squads.
			bRed	= 101;
			bGreen	= 133;
			bBlue	= 221;
			break;
		case 3:
			bRed	= 67;
			bGreen	= 85;
			bBlue	= 255;
			break;
		case 4:
			bRed	= 62;
			bGreen	= 33;
			bBlue	= 211;
			break;
		default:
			ALERT ( at_aiconsole, "Unsupported Houndeye SquadSize!\n" );
			bRed	= 188;
			bGreen	= 220;
			bBlue	= 255;
			break;
		}
	}
	else
	{
		// solo houndeye - weakest beam
		bRed	= 188;
		bGreen	= 220;
		bBlue	= 255;
	}
	
	WRITE_BYTE( bRed   );
	WRITE_BYTE( bGreen );
	WRITE_BYTE( bBlue  );
}
		

//=========================================================
// SonicAttack
//=========================================================
void CHoundeye :: SonicAttack ( void )
{
	float		flAdjustedDamage;
	float		flDist;

	m_fStopImplosion = TRUE;

	switch ( RANDOM_LONG( 0, 2 ) )
	{
	case 0:	EMIT_SOUND(ENT(pev), CHAN_WEAPON, "houndeye/he_blast1.wav", 1, ATTN_NORM);	break;
	case 1:	EMIT_SOUND(ENT(pev), CHAN_WEAPON, "houndeye/he_blast2.wav", 1, ATTN_NORM);	break;
	case 2:	EMIT_SOUND(ENT(pev), CHAN_WEAPON, "houndeye/he_blast3.wav", 1, ATTN_NORM);	break;
	}

	// blast circles
	MESSAGE_BEGIN( MSG_PAS, SVC_TEMPENTITY, pev->origin );
		WRITE_BYTE( TE_BEAMCYLINDER );
		WRITE_COORD( pev->origin.x);
		WRITE_COORD( pev->origin.y);
		WRITE_COORD( pev->origin.z + 16);
		WRITE_COORD( pev->origin.x);
		WRITE_COORD( pev->origin.y);
		WRITE_COORD( pev->origin.z + 16 + HOUNDEYE_MAX_ATTACK_RADIUS / .2); // reach damage radius over .3 seconds
		WRITE_SHORT( m_iSpriteTexture );
		WRITE_BYTE( 0 ); // startframe
		WRITE_BYTE( 0 ); // framerate
		WRITE_BYTE( 2 ); // life
		WRITE_BYTE( 16 );  // width
		WRITE_BYTE( 0 );   // noise

		WriteBeamColor();

		WRITE_BYTE( 255 ); //brightness
		WRITE_BYTE( 0 );		// speed
	MESSAGE_END();

	MESSAGE_BEGIN( MSG_PAS, SVC_TEMPENTITY, pev->origin );
		WRITE_BYTE( TE_BEAMCYLINDER );
		WRITE_COORD( pev->origin.x);
		WRITE_COORD( pev->origin.y);
		WRITE_COORD( pev->origin.z + 16);
		WRITE_COORD( pev->origin.x);
		WRITE_COORD( pev->origin.y);
		WRITE_COORD( pev->origin.z + 16 + ( HOUNDEYE_MAX_ATTACK_RADIUS / 2 ) / .2); // reach damage radius over .3 seconds
		WRITE_SHORT( m_iSpriteTexture );
		WRITE_BYTE( 0 ); // startframe
		WRITE_BYTE( 0 ); // framerate
		WRITE_BYTE( 2 ); // life
		WRITE_BYTE( 16 );  // width
		WRITE_BYTE( 0 );   // noise

		WriteBeamColor();
		
		WRITE_BYTE( 255 ); //brightness
		WRITE_BYTE( 0 );		// speed
	MESSAGE_END();

	CBaseEntity *pEntity = NULL;
	// iterate on all entities in the vicinity.
	while ((pEntity = UTIL_FindEntityInSphere( pEntity, pev->origin, HOUNDEYE_MAX_ATTACK_RADIUS )) != NULL)
	{
		if ( pEntity->pev->takedamage != DAMAGE_NO )
		{
			if ( !FClassnameIs(pEntity->pev, "monster_houndeye") )
			{// houndeyes don't hurt other houndeyes with their attack

				// houndeyes do FULL damage if the ent in question is visible. Half damage otherwise.
				// This means that you must get out of the houndeye's attack range entirely to avoid damage.
				// Calculate full damage first

				if ( m_pSquad && m_pSquad->NumMembers() > 1 )
				{
					// squad gets attack bonus.
					flAdjustedDamage = sk_houndeye_dmg_blast.value + sk_houndeye_dmg_blast.value * ( HOUNDEYE_SQUAD_BONUS * ( m_pSquad->NumMembers() - 1 ) );
				}
				else
				{
					// solo
					flAdjustedDamage = sk_houndeye_dmg_blast.value;
				}

				flDist = (pEntity->Center() - pev->origin).Length();

				flAdjustedDamage -= ( flDist / HOUNDEYE_MAX_ATTACK_RADIUS ) * flAdjustedDamage;

				if ( !FVisible( pEntity ) )
				{
					if ( pEntity->IsPlayer() )
					{
						// if this entity is a client, and is not in full view, inflict half damage. We do this so that players still 
						// take the residual damage if they don't totally leave the houndeye's effective radius. We restrict it to clients
						// so that monsters in other parts of the level don't take the damage and get pissed.
						flAdjustedDamage *= 0.5;
					}
					else if ( !FClassnameIs( pEntity->pev, "func_breakable" ) && !FClassnameIs( pEntity->pev, "func_pushable" ) ) 
					{
						// do not hurt nonclients through walls, but allow damage to be done to breakables
						flAdjustedDamage = 0;
					}
				}

				//ALERT ( at_aiconsole, "Damage: %f\n", flAdjustedDamage );

				if (flAdjustedDamage > 0 )
				{
					CTakeDamageInfo info( this, this, flAdjustedDamage, DMG_SONIC | DMG_ALWAYSGIB );
					pEntity->TakeDamage( info );
				}
			}
		}
	}
}
		
//=========================================================
// start task
//=========================================================
void CHoundeye :: StartTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_HOUND_FALL_ASLEEP:
		{
			m_fAsleep = TRUE; // signal that hound is lying down (must stand again before doing anything else!)
			TaskComplete();
			break;
		}
	case TASK_HOUND_WAKE_UP:
		{
			m_fAsleep = FALSE; // signal that hound is standing again
			TaskComplete();
			break;
		}
	case TASK_HOUND_OPEN_EYE:
		{
			m_fDontBlink = FALSE; // turn blinking back on and that code will automatically open the eye
			TaskComplete();
			break;
		}
	case TASK_HOUND_CLOSE_EYE:
		{
			pev->skin = 0;
			m_fDontBlink = TRUE; // tell blink code to leave the eye alone.
			break;
		}
	case TASK_HOUND_THREAT_DISPLAY:
		{
			m_IdealActivity = ACT_IDLE_ANGRY;
			break;
		}
	case TASK_HOUND_HOP_BACK:
		{
			m_IdealActivity = ACT_LEAP;
			break;
		}
	case TASK_RANGE_ATTACK1:
		{
			ResetIdealActivity( ACT_RANGE_ATTACK1 );

			m_fStopImplosion = FALSE; // ATTACK has started
/*
			if ( InSquad() )
			{
				// see if there is a battery to connect to. 
				CSquadMonster *pSquad = m_pSquadLeader;

				while ( pSquad )
				{
					if ( pSquad->m_iMySlot == bits_SLOT_HOUND_BATTERY )
					{
						// draw a beam.
						MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
							WRITE_BYTE( TE_BEAMENTS );
							WRITE_SHORT( ENTINDEX( this->edict() ) );
							WRITE_SHORT( ENTINDEX( pSquad->edict() ) );
							WRITE_SHORT( m_iSpriteTexture );
							WRITE_BYTE( 0 ); // framestart
							WRITE_BYTE( 0 ); // framerate
							WRITE_BYTE( 10 ); // life
							WRITE_BYTE( 40 );  // width
							WRITE_BYTE( 10 );   // noise
							WRITE_BYTE( 0  );   // r, g, b
							WRITE_BYTE( 50 );   // r, g, b
							WRITE_BYTE( 250);   // r, g, b
							WRITE_BYTE( 255 );	// brightness
							WRITE_BYTE( 30 );		// speed
						MESSAGE_END();
						break;
					}

					pSquad = pSquad->m_pSquadNext;
				}
			}
*/

			break;
		}
	case TASK_SPECIAL_ATTACK1:
		{
			m_IdealActivity = ACT_SPECIAL_ATTACK1;
			break;
		}
	case TASK_GUARD:
		{
			m_IdealActivity = ACT_GUARD;
			break;
		}
	default: 
		{
			BaseClass::StartTask(pTask);
			break;
		}
	}
}

//=========================================================
// RunTask 
//=========================================================
void CHoundeye :: RunTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_HOUND_THREAT_DISPLAY:
		{
			MakeIdealYaw ( GetEnemyLKP() );
			ChangeYaw ( pev->yaw_speed );

			if ( m_fSequenceFinished )
			{
				TaskComplete();
			}
			
			break;
		}
	case TASK_HOUND_CLOSE_EYE:
		{
			if ( pev->skin < HOUNDEYE_EYE_FRAMES - 1 )
			{
				pev->skin++;
			}
			break;
		}
	case TASK_HOUND_HOP_BACK:
		{
			if ( m_fSequenceFinished )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_RANGE_ATTACK1:
		{
			MakeIdealYaw ( GetEnemyLKP() );
			ChangeYaw ( pev->yaw_speed );
			
			float life;
			
			if ( !m_fStopImplosion )
			{ 
				pev->skin = RANDOM_LONG(0, HOUNDEYE_EYE_FRAMES - 1);

				life = ((255 - pev->frame) / (pev->framerate * m_flFrameRate));
				if (life < 0.1) life = 0.1;

				MESSAGE_BEGIN( MSG_PAS, SVC_TEMPENTITY, pev->origin );
					WRITE_BYTE(  TE_IMPLOSION );
					WRITE_COORD( pev->origin.x);
					WRITE_COORD( pev->origin.y);
					WRITE_COORD( pev->origin.z + 16);
					WRITE_BYTE( 50 * life + 100 );  // radius
					WRITE_BYTE( pev->frame / 25.0 ); // count
					WRITE_BYTE( ( life * 10 ) / 3 ); // life
				MESSAGE_END();
			}

			if ( m_fSequenceFinished )
			{
				//SonicAttack(); 
				TaskComplete();
			}

			break;
		}
	case TASK_GUARD:
		{
			if ( m_fSequenceFinished )
			{
				TaskComplete();
			}
			break;
		}
	default:
		{
			BaseClass::RunTask(pTask);
			break;
		}
	}
}

//=========================================================
// PrescheduleThink
//=========================================================
void CHoundeye::PrescheduleThink ( void )
{
	// if the hound is mad and is running, make hunt noises.
	if ( m_MonsterState == MONSTERSTATE_COMBAT && m_Activity == ACT_RUN && RANDOM_FLOAT( 0, 1 ) < 0.2 )
	{
		WarnSound();
	}

	// at random, initiate a blink if not already blinking or sleeping
	if ( !m_fDontBlink )
	{
		if ( ( pev->skin == 0 ) && RANDOM_LONG(0,0x7F) == 0 )
		{// start blinking!
			pev->skin = HOUNDEYE_EYE_FRAMES - 1;
		}
		else if ( pev->skin != 0 )
		{// already blinking
			pev->skin--;
		}
	}

	// if you are the leader, average the origins of each pack member to get an approximate center.
	if ( m_pSquad && m_pSquad->IsLeader( this ) )
	{
		int iSquadCount = 0;

		AISquadIter_t iter;
		for ( CBaseMonster *pSquadMember = m_pSquad->GetFirstMember( &iter ); pSquadMember; pSquadMember = m_pSquad->GetNextMember( &iter ) )
		{
			iSquadCount++;
			m_vecPackCenter = m_vecPackCenter + pSquadMember->pev->origin;
		}

		m_vecPackCenter = m_vecPackCenter / iSquadCount;
	}
}

//=========================================================
// TranslateSchedule
//=========================================================
int CHoundeye::TranslateSchedule( int scheduleType )
{
	if ( m_fAsleep )
	{
		// if the hound is sleeping, must wake and stand!
		if ( HasCondition( COND_HEAR_DANGER ) || HasCondition( COND_HEAR_COMBAT ) 
		|| HasCondition( COND_HEAR_WORLD ) || HasCondition( COND_HEAR_PLAYER ) )
		{
			CSound *pWakeSound;

			pWakeSound = GetBestSound();
			ASSERT( pWakeSound != NULL );
			if ( pWakeSound )
			{
				MakeIdealYaw ( pWakeSound->m_vecOrigin );

				if ( FLSoundVolume ( pWakeSound ) >= HOUNDEYE_SOUND_STARTLE_VOLUME )
				{
					// awakened by a loud sound
					return SCHED_HOUND_WAKE_URGENT;
				}
			}
			// sound was not loud enough to scare the bejesus out of houndeye
			return SCHED_HOUND_WAKE_LAZY;
		}
		else if ( HasCondition( COND_NEW_ENEMY ) )
		{
			// get up fast, to fight.
			return SCHED_HOUND_WAKE_URGENT;
		}

		else
		{
			// hound is waking up on its own
			return SCHED_HOUND_WAKE_LAZY;
		}
	}
	switch	( scheduleType )
	{
	case SCHED_IDLE_STAND:
		{
			// we may want to sleep instead of stand!
			if ( m_pSquad && !m_pSquad->IsLeader( this ) && !m_fAsleep && RANDOM_LONG(0,29) < 1 )
			{
				return SCHED_HOUND_SLEEP;
			}
			else if ( FCanLeaderLook() && RANDOM_LONG( 0, 5) < 1 )
			{
				return SCHED_HOUND_GUARD_PACK;
			}
			else
			{
				return BaseClass::TranslateSchedule( scheduleType );
			}
		}
	case SCHED_RANGE_ATTACK1:
		{
			return SCHED_HOUND_RANGE_ATTACK1;

/*			if ( InSquad()  )
			{
				if ( RANDOM_LONG( 0, 1 ) == 0 )
				{
					return SCHED_HOUND_RANGE_ATTACK2;
				}
				else
				{
					return SCHED_HOUND_RANGE_ATTACK1;
				}
			
			}

			return SCHED_HOUND_RANGE_ATTACK2;*/
		}
	case SCHED_SPECIAL_ATTACK1:
		{
			return SCHED_HOUND_SPECIAL_ATTACK;
		}
	case SCHED_FAIL:
		{
			if ( m_MonsterState == MONSTERSTATE_COMBAT )
			{
				if ( !FNullEnt( FIND_CLIENT_IN_PVS( edict() ) ) )
				{
					// client in PVS
					return SCHED_HOUND_COMBAT_FAIL_PVS;
				}
				else
				{
					// client has taken off! 
					return SCHED_HOUND_COMBAT_FAIL_NOPVS;
				}
			}
			else
			{
				return BaseClass::TranslateSchedule( scheduleType );
			}
		}
	default:
		{
			return BaseClass::TranslateSchedule( scheduleType );
		}
	}
}

//=========================================================
// GetSchedule 
//=========================================================
int CHoundeye :: SelectSchedule( void )
{
	switch	( m_MonsterState )
	{
	case MONSTERSTATE_COMBAT:
		{
// dead enemy
			if ( HasCondition( COND_ENEMY_DEAD ) )
			{
				// call base class, all code to handle dead enemies is centralized there.
				return BaseClass::SelectSchedule();
			}

			if ( HasCondition( COND_LIGHT_DAMAGE ) || HasCondition( COND_HEAVY_DAMAGE ) )
			{
				if ( RANDOM_FLOAT( 0 , 1 ) <= 0.4 )
				{
					TraceResult tr;
					Vector forward;
					AngleVectors( pev->angles, &forward );
					UTIL_TraceEntity( this, pev->origin, pev->origin + forward * -128, dont_ignore_monsters, ENT( pev ), &tr );

					if ( tr.flFraction == 1.0 )
					{
						// it's clear behind, so the hound will jump
						return SCHED_HOUND_HOP_RETREAT;
					}
				}

				return SCHED_TAKE_COVER_FROM_ENEMY;
			}

			if ( HasCondition( COND_CAN_RANGE_ATTACK1 ) )
			{
				if ( OccupyStrategySlotRange( SQUAD_SLOTS_HOUND_ATTACK1, SQUAD_SLOTS_HOUND_ATTACK3 ) )
				{
					return SCHED_RANGE_ATTACK1;
				}

				return SCHED_HOUND_AGITATED;
			}
			break;
		}
	}

	return BaseClass::SelectSchedule();
}

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

AI_BEGIN_CUSTOM_NPC( monster_houndeye, CHoundeye )

	DECLARE_TASK ( TASK_HOUND_CLOSE_EYE )
	DECLARE_TASK ( TASK_HOUND_OPEN_EYE )
	DECLARE_TASK ( TASK_HOUND_THREAT_DISPLAY )
	DECLARE_TASK ( TASK_HOUND_FALL_ASLEEP )
	DECLARE_TASK ( TASK_HOUND_WAKE_UP )
	DECLARE_TASK ( TASK_HOUND_HOP_BACK )

	DECLARE_SQUADSLOT( SQUAD_SLOTS_HOUND_ATTACK1 )
	DECLARE_SQUADSLOT( SQUAD_SLOTS_HOUND_ATTACK2 )
	DECLARE_SQUADSLOT( SQUAD_SLOTS_HOUND_ATTACK3 )

	//=========================================================
	// > SCHED_HOUND_GUARD_PACK
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_GUARD_PACK,
	
		"	Tasks"
		"		TASK_STOP_MOVING	0"
		"		TASK_GUARD			0"
		""
		"	Interrupts"
		"			COND_SEE_HATE"
		"			COND_LIGHT_DAMAGE"
		"			COND_HEAVY_DAMAGE"
		"			COND_PROVOKED"
		"			COND_HEAR_DANGER"
		
	)

	//=========================================================
	// > SCHED_HOUND_RANGE_ATTACK1 - primary range attack
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_RANGE_ATTACK1,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_FACE_IDEAL				0"
		"		TASK_RANGE_ATTACK1			0"
		"		TASK_SET_SCHEDULE			SCHEDULE:SCHED_HOUND_AGITATED"
		""
		"	Interrupts"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
	)

	//=========================================================
	// > SCHED_HOUND_RANGE_ATTACK2
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_RANGE_ATTACK2,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_FACE_IDEAL				0"
		"		TASK_RANGE_ATTACK1			0"
		""
		"	Interrupts"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
	)

	//=========================================================
	// > SCHED_HOUND_SLEEP - lie down and fall asleep
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_SLEEP,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
		"		TASK_WAIT_RANDOM			5"
		"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_CROUCH"
		"		TASK_SET_ACTIVITY			ACTIVITY:ACT_CROUCHIDLE"
		"		TASK_HOUND_FALL_ASLEEP		0"
		"		TASK_WAIT_RANDOM			25"
		"		TASK_HOUND_CLOSE_EYE		0"
		//"		TASK_WAIT					10"
		//"		TASK_WAIT_RANDOM			10"
		""
		"	Interrupts"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
		"		COND_NEW_ENEMY"
		"		COND_HEAR_COMBAT"
		"		COND_HEAR_PLAYER"
		"		COND_HEAR_WORLD"
	)

	//=========================================================
	// > SCHED_HOUND_WAKE_LAZY - wake and stand up lazily
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_WAKE_LAZY,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_HOUND_OPEN_EYE			0"
		"		TASK_WAIT_RANDOM			2.5"
		"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_STAND"
		"		TASK_HOUND_WAKE_UP			0"
		"	"
		"	Interrupts"
	)

	//=========================================================
	// > SCHED_HOUND_WAKE_URGENT - wake and stand up with great urgency!
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_WAKE_URGENT,

		"	Tasks"
		"		TASK_HOUND_OPEN_EYE			0"
		"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_HOP"
		"		TASK_FACE_IDEAL				0"
		"		TASK_HOUND_WAKE_UP			0"
		"	"
		"	Interrupts"
	)

	//=========================================================
	// > SCHED_HOUND_SPECIAL_ATTACK
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_SPECIAL_ATTACK,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_FACE_IDEAL				0"
		"		TASK_SPECIAL_ATTACK1		0"
		"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_IDLE_ANGRY"
		"	"
		"	Interrupts"
		"		"
		"		COND_NEW_ENEMY"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
		"		COND_ENEMY_OCCLUDED"
	)

	//=========================================================
	// > SCHED_HOUND_AGITATED
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_AGITATED,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_HOUND_THREAT_DISPLAY	0"
		""
		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
	)

	//=========================================================
	// > SCHED_HOUND_HOP_RETREAT
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_HOP_RETREAT,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_HOUND_HOP_BACK			0"
		"		TASK_SET_SCHEDULE			SCHEDULE:SCHED_TAKE_COVER_FROM_ENEMY"
		""
		"	Interrupts"
	)

	//=========================================================
	// > SCHED_HOUND_COMBAT_FAIL_PVS - 
	// hound fails in combat with client in the PVS
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_COMBAT_FAIL_PVS,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_HOUND_THREAT_DISPLAY	0"
		"		TASK_WAIT_FACE_ENEMY		1"
		"	"
		"	Interrupts"
		"		"
		"		COND_NEW_ENEMY"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
	)

	//=========================================================
	// > SCHED_HOUND_COMBAT_FAIL_NOPVS -
	// hound fails in combat with no client in the PVS. Don't keep peeping!
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HOUND_COMBAT_FAIL_NOPVS,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_HOUND_THREAT_DISPLAY	0"
		"		TASK_WAIT_FACE_ENEMY		2"
		"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
		"		TASK_WAIT_PVS				0"
		"	"
		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
	)
	
AI_END_CUSTOM_NPC()
