/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
#ifndef SCRIPTED_H
#define SCRIPTED_H

#ifndef SCRIPTEVENT_H
#include "scriptevent.h"
#endif

#define SF_SCRIPT_WAITTILLSEEN		1
#define SF_SCRIPT_EXITAGITATED		2
#define SF_SCRIPT_REPEATABLE		4
#define SF_SCRIPT_LEAVECORPSE		8
//#define SF_SCRIPT_INTERPOLATE		16 // don't use, old bug
#define SF_SCRIPT_NOINTERRUPT		32
#define SF_SCRIPT_OVERRIDESTATE		64
#define SF_SCRIPT_NOSCRIPTMOVEMENT	128
#define SF_SCRIPT_NORESETENTITY		512		// OP4 FEATURE: Don't reset the entity's state after completing the script 
											// ( For chaining scripts without sequence changes )
	
enum CINE_MOVETO
{
	CINE_MOVETO_WAIT = 0,
	CINE_MOVETO_WALK = 1,
	CINE_MOVETO_RUN = 2,
	CINE_MOVETO_TELEPORT = 4,
	CINE_MOVETO_WAIT_FACING = 5,
};

//
// Interrupt levels for grabbing monsters to act out scripted events. These indicate
// how important it is to get a specific monster, and can affect how they respond.
//
enum SS_INTERRUPT
{
	SS_INTERRUPT_IDLE = 0,
	SS_INTERRUPT_BY_NAME,
	SS_INTERRUPT_AI,
};

// when a monster finishes an AI scripted sequence, we can choose
// a schedule to place them in. These defines are the aliases to
// resolve worldcraft input to real schedules (sjb)
#define SCRIPT_FINISHSCHED_DEFAULT	0
#define SCRIPT_FINISHSCHED_AMBUSH	1

class CScriptedSequence : public CBaseToggle
{
	DECLARE_CLASS( CScriptedSequence, CBaseToggle );
public:
	void Spawn( void );
	virtual void KeyValue( KeyValueData *pkvd );
	virtual void Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	virtual void Blocked( CBaseEntity *pOther );
	virtual void Touch( CBaseEntity *pOther );
	virtual int	 ObjectCaps( void ) { return (BaseClass::ObjectCaps() & ~FCAP_ACROSS_TRANSITION); }
	virtual void Activate( void );
	virtual void UpdateOnRemove( void );

	DECLARE_DATADESC();

	// void EXPORT CineSpawnThink( void );
	void EXPORT CineThink( void );
	void Pain( void );
	void Die( void );
	void DelayStart( int state );
	bool FindEntity( void );
	virtual void PossessEntity( void );

	void ReleaseEntity( CBaseMonster *pEntity );
	void CancelScript( void );
	virtual bool StartSequence( CBaseMonster *pTarget, int iszSeq, bool completeOnEmpty );
	virtual bool FCanOverrideState ( void );
	void SequenceDone ( CBaseMonster *pMonster );
	virtual void FixScriptMonsterSchedule( CBaseMonster *pMonster );
	bool	CanInterrupt( void );
	void	AllowInterrupt( bool fAllow );
	void	RemoveIgnoredConditions( void );

	string_t m_iszIdle;			// string index for idle animation
	string_t m_iszPlay;			// string index for scripted animation
	string_t m_iszEntity;	// entity that is wanted for this script
	

	int m_fMoveTo;
	int m_iFinishSchedule;

	float m_flRadius;		// range to search for a monster to possess.
	float m_flRepeat;		// repeat rate

	int m_iDelay;			// A counter indicating how many scripts are NOT ready to start.
	float m_startTime;

	int	m_saved_movetype;
	int	m_saved_solid;
	int m_saved_effects;

	bool m_interruptable;
	bool m_SequenceStarted;

	EHANDLE	m_hTargetEnt;

	static void ScriptEntityCancel( CBaseEntity *pentCine );

private:
	friend class CBaseMonster;	// should probably try to eliminate this relationship
};

class CCineAI : public CScriptedSequence
{
	bool StartSequence( CBaseMonster *pTarget, int iszSeq, bool completeOnEmpty );
	void PossessEntity( void );
	bool FCanOverrideState ( void );
	virtual void FixScriptMonsterSchedule( CBaseMonster *pMonster );
};


#endif		//SCRIPTED_H
