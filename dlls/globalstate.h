//========= Copyright (c) 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef GLOBALSTATE_H
#define GLOBALSTATE_H
#ifdef _WIN32
#pragma once
#endif

typedef enum { GLOBAL_OFF = 0, GLOBAL_ON = 1, GLOBAL_DEAD = 2 } GLOBALESTATE;

typedef struct globalentity_s globalentity_t;

struct globalentity_s
{
	char			name[64];
	char			levelName[32];
	GLOBALESTATE	state;
	globalentity_t	*pNext;
};

class CGlobalState
{
public:
					CGlobalState();
	void			Reset( void );
	void			ClearStates( void );
	void			EntityAdd( string_t globalname, string_t mapName, GLOBALESTATE state );
	void			EntitySetState( string_t globalname, GLOBALESTATE state );
	void			EntityUpdate( string_t globalname, string_t mapname );
	const globalentity_t	*EntityFromTable( string_t globalname );
	GLOBALESTATE	EntityGetState( string_t globalname );
	int				EntityInTable( string_t globalname ) { return (Find( globalname ) != NULL) ? 1 : 0; }
	int				Save( CSave &save );
	int				Restore( CRestore &restore );
	static typedescription_t m_SaveData[];

//#ifdef _DEBUG
	void			DumpGlobals( void );
//#endif

private:
	globalentity_t	*Find( string_t globalname );
	globalentity_t	*m_pList;
	int				m_listCount;
};

extern CGlobalState gGlobalState;

extern void SaveGlobalState( SAVERESTOREDATA *pSaveData );
extern void RestoreGlobalState( SAVERESTOREDATA *pSaveData );
extern void ResetGlobalState( void );

#endif // GLOBALSTATE_H