//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Default schedules.
//
//=============================================================================//

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"
#include	"ai_squad.h"
#include	"ai_default.h"
#include	"soundent.h"
#include	"nodes.h"
#include	"scripted.h"
#include	"igamesystem.h"

//-----------------------------------------------------------------------------
//
// Schedules
//
//-----------------------------------------------------------------------------

//=========================================================
// > Fail
// This schedule itself can fail because the monster may
// be unable to finish the stop moving. If so, fall back
// to a fail schedule that has no stop moving in it.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_FAIL,

	"	Tasks"
	"		TASK_SET_FAIL_SCHEDULE	SCHEDULE:SCHED_FAIL_NOSTOP"
	"		TASK_STOP_MOVING		0"
	"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
	"		TASK_WAIT				1"
	"		TASK_WAIT_PVS			0"
	""
	"	Interrupts"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
	"		COND_CAN_MELEE_ATTACK1"
	"		COND_CAN_MELEE_ATTACK2"
);

//=========================================================
// > Fail without stop moving, which can fail.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_FAIL_NOSTOP,

	"	Tasks"
	"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
	"		TASK_WAIT				1"
	"		TASK_WAIT_PVS			0"
	""
	"	Interrupts"
	"		COND_CAN_RANGE_ATTACK1 "
	"		COND_CAN_RANGE_ATTACK2 "
	"		COND_CAN_MELEE_ATTACK1 "
	"		COND_CAN_MELEE_ATTACK2"
 );

//===============================================
//	> Idle_Stand
//===============================================
AI_DEFINE_SCHEDULE
(
	SCHED_IDLE_STAND,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
	"		TASK_WAIT				5"	// repick IDLESTAND every five seconds. gives us a chance to pick an active idle, fidget, etc.
	"		TASK_WAIT_PVS			0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_SEE_FEAR"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_SMELL"
	"		COND_SMELL_FOOD"
	"		COND_PROVOKED"
	"		COND_HEAR_PLAYER"
	"		COND_HEAR_DANGER"
	"		COND_HEAR_COMBAT"
	"		COND_HEAR_WORLD"
);

//===============================================
//	> Wait_For_Script 
//===============================================
AI_DEFINE_SCHEDULE
(
	SCHED_WAIT_TRIGGER,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
	"		TASK_WAIT_INDEFINITE	0"
	""
	"	Interrupts"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
);

//===============================================
//	> IdleWalk
//===============================================
AI_DEFINE_SCHEDULE
(
	SCHED_IDLE_WALK,

	"	Tasks"
	"		TASK_WALK_PATH			9999"
	"		TASK_WAIT_FOR_MOVEMENT	0"
	"		TASK_WAIT_PVS			0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_SMELL"
	"		COND_PROVOKED"
	"		COND_HEAR_COMBAT"
);

//=========================================================
// Ambush - monster stands in place and waits for a new 
// enemy, or chance to attack an existing enemy.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_AMBUSH,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_WAIT_INDEFINITE		0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_PROVOKED"
);

//=========================================================
// IdleActive schedule - !!!BUGBUG - if this schedule doesn't
// complete on its own, the monster's HintNode will not be 
// cleared, and the rest of the monster's group will avoid
// that node because they think the group member that was 
// previously interrupted is still using that node to active
// idle.
///=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_IDLE_ACTIVE,

	"	Tasks"
	"		TASK_FIND_HINTNODE				0"
	"		TASK_GET_PATH_TO_HINTNODE		0"
	"		TASK_STORE_LASTPOSITION			0"
	"		TASK_WALK_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	"		TASK_FACE_HINTNODE				0"
	"		TASK_PLAY_HINT_ACTIVITY			0"
	"		TASK_GET_PATH_TO_LASTPOSITION	0"
	"		TASK_WALK_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	"		TASK_CLEAR_LASTPOSITION			0"
	"		TASK_CLEAR_HINTNODE				0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_PROVOKED"
	"		COND_HEAR_COMBAT"		// sound flags
	"		COND_HEAR_WORLD"
	"		COND_HEAR_PLAYER"
	"		COND_HEAR_DANGER"
);

//=========================================================
//	Wake Schedules
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_WAKE_ANGRY,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE "
	"		TASK_SOUND_WAKE			0"
	"		TASK_FACE_IDEAL			0"
	""
	"	Interrupts"
);

//=========================================================
//  > AlertFace
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_ALERT_FACE,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_FACE_IDEAL				0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_SEE_FEAR"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_PROVOKED"
);

//=========================================================
// AlertSmallFlinch Schedule - shot, but didn't see attacker,
// flinch then face
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_ALERT_SMALL_FLINCH,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_REMEMBER				MEMORY:FLINCHED "
	"		TASK_SMALL_FLINCH			0"
	"		TASK_SET_SCHEDULE			SCHEDULE:SCHED_ALERT_FACE"
	""
	"	Interrupts"
);

//=========================================================
//  > AlertStand
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_ALERT_STAND,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_WAIT					20"
	"		TASK_SUGGEST_STATE			STATE:IDLE"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_SEE_ENEMY"
	"		COND_SEE_FEAR"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_PROVOKED"
	"		COND_SMELL"
	"		COND_HEAR_COMBAT"		// sound flags
	"		COND_HEAR_WORLD"
	"		COND_HEAR_PLAYER"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > InvestigateSound
//
//	sends a monster to the location of the
//	sound that was just heard to check things out.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_INVESTIGATE_SOUND,

	"	Tasks"
	"		TASK_STOP_MOVING				0"
	"		TASK_STORE_LASTPOSITION			0"
	"		TASK_GET_PATH_TO_BESTSOUND		0"
	"		TASK_FACE_IDEAL					0"
	"		TASK_WALK_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	"		TASK_PLAY_SEQUENCE				ACTIVITY:ACT_IDLE"
	"		TASK_WAIT						10"
	"		TASK_GET_PATH_TO_LASTPOSITION	0"
	"		TASK_WALK_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	"		TASK_CLEAR_LASTPOSITION			0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_SEE_FEAR"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > CombatStand
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_COMBAT_STAND,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_WAIT_INDEFINITE		0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
	"		COND_CAN_MELEE_ATTACK1"
	"		COND_CAN_MELEE_ATTACK2"
);

//=========================================================
// > CombatFace
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_COMBAT_FACE,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_SET_ACTIVITY		ACTIVITY:ACT_IDLE"
	"		TASK_FACE_ENEMY			0"
	""
	"	Interrupts"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
	"		COND_CAN_MELEE_ATTACK1"
	"		COND_CAN_MELEE_ATTACK2"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
);

//=========================================================
// > Standoff
//
// Used in combat when a monster is
// hiding in cover or the enemy has moved out of sight.
// Should we look around in this schedule?
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_STANDOFF,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_WAIT_FACE_ENEMY		2"
	""
	"	Interrupts"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
	"		COND_ENEMY_DEAD"
	"		COND_NEW_ENEMY"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > Arm weapon (draw gun)
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_ARM_WEAPON,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_ARM"
	""
	"	Interrupts"
);

//=========================================================
// > Reload
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_RELOAD,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_RELOAD				0"
	""
	"	Interrupts"
	"		COND_HEAVY_DAMAGE"
);

//===============================================
//	> RangeAttack1
//===============================================
AI_DEFINE_SCHEDULE
(
	SCHED_RANGE_ATTACK1,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_FACE_ENEMY			0"
	"		TASK_RANGE_ATTACK1		0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_ENEMY_OCCLUDED"
	"		COND_NO_AMMO_LOADED"
	"		COND_HEAR_DANGER"
);

//===============================================
//	> RangeAttack2
//===============================================
AI_DEFINE_SCHEDULE
(
	SCHED_RANGE_ATTACK2,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_FACE_ENEMY				0"
	"		TASK_RANGE_ATTACK2			0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_ENEMY_OCCLUDED"
	"		COND_NO_AMMO_LOADED"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > Melee_Attack1
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_MELEE_ATTACK1,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_FACE_ENEMY			0"
	"		TASK_MELEE_ATTACK1		0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_ENEMY_OCCLUDED"
);

//=========================================================
// > Melee_Attack2
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_MELEE_ATTACK2,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_FACE_ENEMY			0"
	"		TASK_MELEE_ATTACK2		0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_ENEMY_OCCLUDED"
);

//=========================================================
// > SpecialAttack1
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SPECIAL_ATTACK1,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_FACE_ENEMY				0"
	"		TASK_SPECIAL_ATTACK1		0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_ENEMY_OCCLUDED"
	"		COND_NO_AMMO_LOADED"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > SpecialAttack2
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SPECIAL_ATTACK2,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_FACE_ENEMY			0"
	"		TASK_SPECIAL_ATTACK2	0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_ENEMY_OCCLUDED"
	"		COND_NO_AMMO_LOADED"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > ChaseEnemy
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_CHASE_ENEMY,

	"	Tasks"
	"		TASK_STOP_MOVING				0"
	"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_CHASE_ENEMY_FAILED"
	"		TASK_GET_PATH_TO_ENEMY			0"
	"		TASK_RUN_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_ENEMY_DEAD"
	"		COND_ENEMY_UNREACHABLE"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_MELEE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
	"		COND_CAN_MELEE_ATTACK2"
	"		COND_TASK_FAILED"
	"		COND_LOST_ENEMY"
	"		COND_HEAR_DANGER"
);

//=========================================================
// > ChaseEnemyFailed
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_CHASE_ENEMY_FAILED,

	"	Tasks"
	"		 TASK_STOP_MOVING					0"
	"		 TASK_WAIT							0.2"
	"		 TASK_SET_FAIL_SCHEDULE				SCHEDULE:SCHED_STANDOFF"
	"		 TASK_FIND_COVER_FROM_ENEMY			0"
	"		 TASK_RUN_PATH						0"
	"		 TASK_WAIT_FOR_MOVEMENT				0"
	"		 TASK_REMEMBER						MEMORY:INCOVER"
//	"		 TASK_TURN_LEFT						0"
	"		 TASK_FACE_ENEMY					0"
	"		 TASK_SET_ACTIVITY					ACTIVITY:ACT_IDLE"
	"		 TASK_WAIT							1"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_MELEE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
	"		COND_CAN_MELEE_ATTACK2"
	"		COND_HEAR_DANGER"
);

//=========================================================
// small flinch, played when minor damage is taken.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SMALL_FLINCH,

	"	Tasks"
	"		 TASK_REMEMBER				MEMORY:FLINCHED"
	"		 TASK_STOP_MOVING			0"
	"		 TASK_SMALL_FLINCH			0"
	""
	"	Interrupts"
);

//=========================================================
// Big Flinch, played when heavy damage is taken for the first time in a while
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_BIG_FLINCH,

	"	Tasks"
	"		 TASK_REMEMBER				MEMORY:FLINCHED"
	"		 TASK_STOP_MOVING			0"
	"		 TASK_BIG_FLINCH			0"
	""
	"	Interrupts"
);

//=========================================================
// Die!
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_DIE,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_SOUND_DIE			0"
	"		TASK_DIE				0"
	""
	"	Interrupts"
	"		COND_NO_CUSTOM_INTERRUPTS"
);

//=========================================================
// > VictoryDance
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_VICTORY_DANCE,

	"	Tasks"
	"		TASK_STOP_MOVING		0"
	"		TASK_PLAY_SEQUENCE		ACTIVITY:ACT_VICTORY_DANCE"
	"		TASK_WAIT				0"
	""
	"	Interrupts"
);

//=========================================================
// BarnacleVictimGrab - barnacle tongue just hit the monster,
// so play a hit animation, then play a cycling pull animation
// as the creature is hoisting the monster.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_BARNACLE_VICTIM_GRAB,

	"	Tasks"
	"		TASK_STOP_MOVING					0"
	"		TASK_PLAY_SEQUENCE					ACTIVITY:ACT_BARNACLE_HIT"
	"		TASK_SET_ACTIVITY					ACTIVITY:ACT_BARNACLE_PULL"
	"		TASK_WAIT_INDEFINITE				0"	// just cycle barnacle pull anim while barnacle hoists. 
	""
	"	Interrupts"
);

//=========================================================
// BarnacleVictimChomp - barnacle has pulled the prey to its
// mouth. Victim should play the BARNCLE_CHOMP animation 
// once, then loop the BARNACLE_CHEW animation indefinitely
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_BARNACLE_VICTIM_CHOMP,

	"	Tasks"
	"		TASK_STOP_MOVING					0"
	"		TASK_PLAY_SEQUENCE					ACTIVITY:ACT_BARNACLE_CHOMP"
	"		TASK_SET_ACTIVITY					ACTIVITY:ACT_BARNACLE_CHEW"
	"		TASK_WAIT_INDEFINITE				0"	// just cycle barnacle pull anim while barnacle hoists. 
	""
	"	Interrupts"
);

//	Universal Error Schedule
/*Task_t	tlError[] =
{
	{ TASK_STOP_MOVING,			0				},
	{ TASK_WAIT_INDEFINITE,				(float)0 },
};

Schedule_t	slError[] =
{
	{ 
		tlError,
		ARRAYSIZE ( tlError ), 
		0,
		0,
		"Error"
	},
};*/

//=========================================================
// > ScriptedWalk
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SCRIPTED_WALK,

	"	Tasks"
	"		 TASK_WALK_TO_TARGET				1"
	"		 TASK_WAIT_FOR_MOVEMENT				0"
	"		 TASK_PLANT_ON_SCRIPT				0"
	"		 TASK_FACE_SCRIPT					0"
	"		 TASK_ENABLE_SCRIPT					0"
	"		 TASK_WAIT_FOR_SCRIPT				0"
	"		 TASK_PLAY_SCRIPT					0"
	""
	"	Interrupts"
	"		COND_LIGHT_DAMAGE "
	"		COND_HEAVY_DAMAGE"
);

//=========================================================
// > ScriptedRun
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SCRIPTED_RUN,

	"	Tasks"
	"		 TASK_RUN_TO_TARGET					1"
	"		 TASK_WAIT_FOR_MOVEMENT				0"
	"		 TASK_PLANT_ON_SCRIPT				0"
	"		 TASK_FACE_SCRIPT					0"
	"		 TASK_ENABLE_SCRIPT					0"
	"		 TASK_WAIT_FOR_SCRIPT				0"
	"		 TASK_PLAY_SCRIPT					0"
	""
	"	Interrupts"
	"		COND_LIGHT_DAMAGE "
	"		COND_HEAVY_DAMAGE"
);

//=========================================================
// > ScriptedWait
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SCRIPTED_WAIT,

	"	Tasks"
	"		 TASK_STOP_MOVING				0"
	"		 TASK_WAIT_FOR_SCRIPT			0"
	"		 TASK_PLAY_SCRIPT				0"
	""
	"	Interrupts"
	"		COND_LIGHT_DAMAGE "
	"		COND_HEAVY_DAMAGE"
);

//=========================================================
// > ScriptedFace
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_SCRIPTED_FACE,

	"	Tasks"
	"		 TASK_STOP_MOVING				0"
	"		 TASK_FACE_SCRIPT				0"
	"		 TASK_WAIT_FOR_SCRIPT			0"
	"		 TASK_PLAY_SCRIPT				0"
	""
	"	Interrupts"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
);

//=========================================================
// > Cower
//
//		This is what is usually done when attempts
//		to escape danger fail.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_COWER,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_COWER"
	""
	"	Interrupts"
);

//=========================================================
// > TakeCoverFromOrigin
//
//			move away from where you're currently standing.
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_TAKE_COVER_FROM_ORIGIN,

	"	Tasks"
	"		 TASK_STOP_MOVING					0"
	"		 TASK_FIND_COVER_FROM_ORIGIN		0"
	"		 TASK_RUN_PATH						0"
	"		 TASK_WAIT_FOR_MOVEMENT				0"
	"		 TASK_REMEMBER						MEMORY:INCOVER"
	"		 TASK_TURN_LEFT						179"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
);

//=========================================================
// > TakeCoverFromBestSound
//
//			hide from the loudest sound source
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_TAKE_COVER_FROM_BEST_SOUND,

	"	Tasks"
	"		 TASK_STOP_MOVING					0"
	"		 TASK_FIND_COVER_FROM_BEST_SOUND	0"
	"		 TASK_RUN_PATH						0"
	"		 TASK_WAIT_FOR_MOVEMENT				0"
	"		 TASK_REMEMBER						MEMORY:INCOVER"
	"		 TASK_TURN_LEFT						179"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
);

//=========================================================
// Take cover from enemy! Tries lateral cover before node 
// cover! 
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_TAKE_COVER_FROM_ENEMY,

	"	Tasks"
	"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_FAIL_TAKE_COVER"
	"		TASK_STOP_MOVING				0"
	"		TASK_WAIT						0.2"
	"		TASK_FIND_COVER_FROM_ENEMY		0"
	"		TASK_RUN_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	"		TASK_REMEMBER					MEMORY:INCOVER"
	"		TASK_FACE_ENEMY					0"
	"		TASK_WAIT						1"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
);

//=========================================================
// FAIL_TAKE_COVER
//
//  Default case.  Overwritten by subclasses for behavior
//=========================================================
AI_DEFINE_SCHEDULE
(
	SCHED_FAIL_TAKE_COVER,

	"	Tasks"
	"		TASK_SET_ACTIVITY				ACTIVITY:ACT_IDLE"
	""
	"	Interrupts"
	"		COND_NEW_ENEMY"
);

//-----------------------------------------------------------------------------
// Purpose: Given and schedule name, return the schedule ID
//-----------------------------------------------------------------------------
int CBaseMonster::GetScheduleID( const char *schedName )
{
	return GetSchedulingSymbols()->ScheduleSymbolToId(schedName);
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CBaseMonster::InitDefaultScheduleSR(void)
{
	#define ADD_DEF_SCHEDULE( name, localId ) idSpace.AddSchedule(name, localId, "CBaseMonster" )

	CAI_ClassScheduleIdSpace &idSpace = CBaseMonster::AccessClassScheduleIdSpaceDirect();

	ADD_DEF_SCHEDULE( "SCHED_NONE",							SCHED_NONE);
	ADD_DEF_SCHEDULE( "SCHED_IDLE_STAND",					SCHED_IDLE_STAND);
	ADD_DEF_SCHEDULE( "SCHED_IDLE_WALK",					SCHED_IDLE_WALK);
	ADD_DEF_SCHEDULE( "SCHED_IDLE_ACTIVE",					SCHED_IDLE_ACTIVE);
	ADD_DEF_SCHEDULE( "SCHED_WAKE_ANGRY",					SCHED_WAKE_ANGRY);
	ADD_DEF_SCHEDULE( "SCHED_ALERT_FACE",					SCHED_ALERT_FACE);
	ADD_DEF_SCHEDULE( "SCHED_ALERT_SMALL_FLINCH",			SCHED_ALERT_SMALL_FLINCH);
	ADD_DEF_SCHEDULE( "SCHED_ALERT_STAND",					SCHED_ALERT_STAND);
	ADD_DEF_SCHEDULE( "SCHED_INVESTIGATE_SOUND",			SCHED_INVESTIGATE_SOUND);
	ADD_DEF_SCHEDULE( "SCHED_COMBAT_FACE",					SCHED_COMBAT_FACE);
	ADD_DEF_SCHEDULE( "SCHED_COMBAT_STAND",					SCHED_COMBAT_STAND);
	ADD_DEF_SCHEDULE( "SCHED_CHASE_ENEMY",					SCHED_CHASE_ENEMY);
	ADD_DEF_SCHEDULE( "SCHED_CHASE_ENEMY_FAILED",			SCHED_CHASE_ENEMY_FAILED);
	ADD_DEF_SCHEDULE( "SCHED_VICTORY_DANCE",				SCHED_VICTORY_DANCE);
	ADD_DEF_SCHEDULE( "SCHED_BARNACLE_VICTIM_GRAB",			SCHED_BARNACLE_VICTIM_GRAB);
	ADD_DEF_SCHEDULE( "SCHED_BARNACLE_VICTIM_CHOMP",		SCHED_BARNACLE_VICTIM_CHOMP);
	ADD_DEF_SCHEDULE( "SCHED_TARGET_FACE",					SCHED_TARGET_FACE);
	ADD_DEF_SCHEDULE( "SCHED_TARGET_CHASE",					SCHED_TARGET_CHASE);
	ADD_DEF_SCHEDULE( "SCHED_SMALL_FLINCH",					SCHED_SMALL_FLINCH);
	ADD_DEF_SCHEDULE( "SCHED_BIG_FLINCH",					SCHED_BIG_FLINCH);
	ADD_DEF_SCHEDULE( "SCHED_TAKE_COVER_FROM_ENEMY",		SCHED_TAKE_COVER_FROM_ENEMY);
	ADD_DEF_SCHEDULE( "SCHED_TAKE_COVER_FROM_BEST_SOUND",	SCHED_TAKE_COVER_FROM_BEST_SOUND);
	ADD_DEF_SCHEDULE( "SCHED_TAKE_COVER_FROM_ORIGIN",		SCHED_TAKE_COVER_FROM_ORIGIN);
	ADD_DEF_SCHEDULE( "SCHED_FAIL_TAKE_COVER",				SCHED_FAIL_TAKE_COVER);
	ADD_DEF_SCHEDULE( "SCHED_COWER",						SCHED_COWER);
	ADD_DEF_SCHEDULE( "SCHED_MELEE_ATTACK1",				SCHED_MELEE_ATTACK1);
	ADD_DEF_SCHEDULE( "SCHED_MELEE_ATTACK2",				SCHED_MELEE_ATTACK2);
	ADD_DEF_SCHEDULE( "SCHED_RANGE_ATTACK1",				SCHED_RANGE_ATTACK1);
	ADD_DEF_SCHEDULE( "SCHED_RANGE_ATTACK2",				SCHED_RANGE_ATTACK2);
	ADD_DEF_SCHEDULE( "SCHED_SPECIAL_ATTACK1",				SCHED_SPECIAL_ATTACK1);
	ADD_DEF_SCHEDULE( "SCHED_SPECIAL_ATTACK2",				SCHED_SPECIAL_ATTACK2);
	ADD_DEF_SCHEDULE( "SCHED_STANDOFF",						SCHED_STANDOFF);
	ADD_DEF_SCHEDULE( "SCHED_ARM_WEAPON",					SCHED_ARM_WEAPON);
	ADD_DEF_SCHEDULE( "SCHED_RELOAD",						SCHED_RELOAD);
	ADD_DEF_SCHEDULE( "SCHED_AMBUSH",						SCHED_AMBUSH);
	ADD_DEF_SCHEDULE( "SCHED_DIE",							SCHED_DIE);
	ADD_DEF_SCHEDULE( "SCHED_WAIT_TRIGGER",					SCHED_WAIT_TRIGGER);
	ADD_DEF_SCHEDULE( "SCHED_AISCRIPT",						SCHED_AISCRIPT);
	ADD_DEF_SCHEDULE( "SCHED_SCRIPTED_WALK",				SCHED_SCRIPTED_WALK);
	ADD_DEF_SCHEDULE( "SCHED_SCRIPTED_RUN",					SCHED_SCRIPTED_RUN);
	ADD_DEF_SCHEDULE( "SCHED_SCRIPTED_WAIT",				SCHED_SCRIPTED_WAIT);
	ADD_DEF_SCHEDULE( "SCHED_SCRIPTED_FACE",				SCHED_SCRIPTED_FACE);
	ADD_DEF_SCHEDULE( "SCHED_FAIL",							SCHED_FAIL);
	ADD_DEF_SCHEDULE( "SCHED_FAIL_NOSTOP",					SCHED_FAIL_NOSTOP);
}

bool CBaseMonster::LoadDefaultSchedules(void)
{
//	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_NONE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_IDLE_STAND);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_IDLE_WALK);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_IDLE_ACTIVE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_WAKE_ANGRY);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_ALERT_FACE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_ALERT_SMALL_FLINCH);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_ALERT_STAND);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_INVESTIGATE_SOUND);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_COMBAT_FACE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_COMBAT_STAND);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_CHASE_ENEMY);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_CHASE_ENEMY_FAILED);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_VICTORY_DANCE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_BARNACLE_VICTIM_GRAB);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_BARNACLE_VICTIM_CHOMP);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SMALL_FLINCH);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_BIG_FLINCH);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_TAKE_COVER_FROM_ENEMY);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_TAKE_COVER_FROM_BEST_SOUND);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_TAKE_COVER_FROM_ORIGIN);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_FAIL_TAKE_COVER);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_COWER);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_MELEE_ATTACK1);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_MELEE_ATTACK2);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_RANGE_ATTACK1);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_RANGE_ATTACK2);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SPECIAL_ATTACK1);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SPECIAL_ATTACK2);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_STANDOFF);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_ARM_WEAPON);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_RELOAD);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_AMBUSH);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_DIE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_WAIT_TRIGGER);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SCRIPTED_WALK);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SCRIPTED_RUN);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SCRIPTED_WAIT);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_SCRIPTED_FACE);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_FAIL);
	AI_LOAD_DEF_SCHEDULE( CBaseMonster,					SCHED_FAIL_NOSTOP);

	return true;
}

int CBaseMonster::TranslateSchedule( int scheduleType )
{
	// FIXME: Where should this go now?
#if 0
	if (scheduleType >= LAST_SHARED_SCHEDULE)
	{
		char errMsg[256];
		Q_snprintf(errMsg,sizeof(errMsg),"ERROR: Subclass Schedule (%s) Hitting Base Class!\n",ScheduleName(scheduleType));
		Msg( errMsg );
		AddTimedOverlay( errMsg, 5);
		return SCHED_FAIL;
	}
#endif

	switch( scheduleType )
	{
	// Hande some special cases
	case SCHED_AISCRIPT:
		{
			Assert( m_pCine != NULL );
			if ( !m_pCine )
			{
				ALERT( at_aiconsole, "Script failed for %s\n", STRING(pev->classname) );
				CineCleanup();
				return SCHED_IDLE_STAND;
			}
//			else
//				ALERT( at_aiconsole, "Starting script %s for %s\n", STRING( m_pCine->m_iszPlay ), STRING(pev->classname) );

			switch ( m_pCine->m_fMoveTo )
			{
				case CINE_MOVETO_WAIT:
				case CINE_MOVETO_TELEPORT:
				{
					return SCHED_SCRIPTED_WAIT;
				}

				case CINE_MOVETO_WALK:
				{
					return SCHED_SCRIPTED_WALK;
				}

				case CINE_MOVETO_RUN:
				{
					return SCHED_SCRIPTED_RUN;
				}

				case CINE_MOVETO_WAIT_FACING:
				{
					return SCHED_SCRIPTED_FACE;
				}
			}
		}
		break;

	case SCHED_IDLE_STAND:
		{
			if ( RANDOM_LONG(0,14) == 0 && FCanActiveIdle() )
			{
				return SCHED_IDLE_ACTIVE;
			}

			return SCHED_IDLE_STAND;
		}	
		break;

	// Vasia: This sure gave me a headache! Apparently only SquadMonster actually uses SCHED_CHASE_ENEMY_FAILED
	// and the base code always translated that schedule to SCHED_FAIL
	// @NOTE (Vasia 11-07-22): The new case I did won't make it work as it used to when squad behavior was in a separate class
	// I need to revisit this later.
	case SCHED_CHASE_ENEMY_FAILED:
		{
			if ( !m_pSquad )
			{
				return SCHED_FAIL;
			}

			return SCHED_CHASE_ENEMY_FAILED;
		}
		break;

	}

	return scheduleType;
}

//=========================================================
// GetScheduleOfType - returns a pointer to one of the 
// monster's available schedules of the indicated type.
//=========================================================
CAI_Schedule *CBaseMonster::GetScheduleOfType( int scheduleType )
{
	// allow the derived classes to pick an appropriate version of this schedule or override
	// base schedule types.
	scheduleType = TranslateSchedule( scheduleType );

	// Get a pointer to that schedule
	CAI_Schedule *schedule = GetSchedule(scheduleType);

	if (!schedule)
	{
		ALERT( at_aiconsole, "GetScheduleOfType(): No CASE for Schedule Type %d!\n", scheduleType );
		return GetSchedule(SCHED_IDLE_STAND);
	}
	return schedule;
}

CAI_Schedule *CBaseMonster::GetSchedule(int schedule)
{
	if (!GetClassScheduleIdSpace()->IsGlobalBaseSet())
	{
		ALERT( at_error," %s missing schedule!\n", GetSchedulingErrorName());
		return g_AI_SchedulesManager.GetScheduleFromID(SCHED_IDLE_STAND);
	}
	if ( AI_IdIsLocal( schedule ) )
	{
		schedule = GetClassScheduleIdSpace()->ScheduleLocalToGlobal(schedule);
	}

	return g_AI_SchedulesManager.GetScheduleFromID( schedule );
}

const char *CBaseMonster::ConditionName(int conditionID)
{
	if ( AI_IdIsLocal( conditionID ) )
		conditionID = GetClassScheduleIdSpace()->ConditionLocalToGlobal(conditionID);
	return GetSchedulingSymbols()->ConditionIdToSymbol(conditionID);
}

const char *CBaseMonster::TaskName(int taskID)
{
	if ( AI_IdIsLocal( taskID ) )
		taskID = GetClassScheduleIdSpace()->TaskLocalToGlobal(taskID);
	return GetSchedulingSymbols()->TaskIdToSymbol( taskID );
}

// This hooks the main game systems callbacks to allow the AI system to manage memory
class CAI_SystemHook : public CAutoGameSystem
{
public:

	// UNDONE: Schedule / strings stuff should probably happen once each GAME, not each level
	void LevelInitPreEntity()
	{
		extern float g_AINextDisabledMessageTime;
		g_AINextDisabledMessageTime = 0;

		g_AI_SchedulesManager.CreateStringRegistries();
		g_AI_SchedulesManager.LoadAllSchedules();
	}

	void LevelShutdownPreEntity()
	{
		CBaseCombatCharacter::ResetVisibilityCache();
	}
	
	void LevelShutdownPostEntity( void )
	{
		g_AI_SchedulesManager.DeleteAllSchedules();
		g_AI_SquadManager.DeleteAllSquads();
		g_AI_SchedulesManager.DestroyStringRegistries();
	}
};


static CAI_SystemHook g_AISystemHook;