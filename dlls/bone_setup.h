//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#if !defined ( STUDIOMODELBLENDING_H )
#define STUDIOMODELBLENDING_H
#if defined( _WIN32 )
#pragma once
#endif

/*
====================
CStudioModelBlending

====================
*/
class CStudioModelBlending
{
public:

	// Construction/Destruction
	CStudioModelBlending( void );
	virtual ~CStudioModelBlending( void );

	// Initialization
	virtual void Init( float (*pRotationMatrix)[3][4], float (*pBoneTransform)[MAXSTUDIOBONES][3][4] );

public:  

	// Public Interfaces
	virtual void StudioSetupBones ( struct model_s *pModel, float frame, int sequence, const float *angles, const float *origin,
	const byte *pcontroller, const byte *pblending, int iBone, const edict_t *pEdict);

public:

	// Local interfaces
	//

	// Look up animation data for sequence
	virtual mstudioanim_t *StudioGetAnim ( model_t *m_pSubModel, mstudioseqdesc_t *pseqdesc );

	// Compute bone adjustments ( bone controllers )
	virtual void StudioCalcBoneAdj( float *adj, const byte *pcontroller );

	// Get bone quaternions
	virtual void StudioCalcBoneQuaterion( int frame, float s, mstudiobone_t *pbone, mstudioanim_t *panim, float *adj, float *q);

	// Get bone positions
	virtual void StudioCalcBonePosition ( int frame, float s, mstudiobone_t *pbone, mstudioanim_t *panim, float *adj, float *pos );

	// Spherical interpolation of bones
	virtual void StudioSlerpBones ( vec4_t q1[], float pos1[][3], vec4_t q2[], float pos2[][3], float s );

	virtual void StudioCalcRotations( int boneused[], int numbones, const byte * pcontroller, float pos[][3], vec4_t * q, mstudioseqdesc_t * pseqdesc, mstudioanim_t * panim, float f);

public:

	// Pointer to header block for studio model data
	studiohdr_t		*m_pStudioHeader;
	
	// Matrices
	// Model to world transformation
	float			(*m_pRotationMatrix)[ 3 ][ 4 ];	
	
	// Concatenated bone transforms
	float			(*m_pBoneTransform) [ MAXSTUDIOBONES ][ 3 ][ 4 ];
};

#endif // STUDIOMODELBLENDING_H