//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Helper classes and functions for the save/restore system.
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include <limits.h>
#include "saverestore.h"

typedescription_t	gEntvarsDescription[] = 
{
	DEFINE_ENTITY_FIELD( classname, FIELD_STRING ),
	DEFINE_ENTITY_GLOBAL_FIELD( globalname, FIELD_STRING ),
	
	DEFINE_ENTITY_FIELD( origin, FIELD_POSITION_VECTOR ),
	DEFINE_ENTITY_FIELD( oldorigin, FIELD_POSITION_VECTOR ),
	DEFINE_ENTITY_FIELD( velocity, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( basevelocity, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( movedir, FIELD_VECTOR ),

	DEFINE_ENTITY_FIELD( angles, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( avelocity, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( punchangle, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( v_angle, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( fixangle, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( idealpitch, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( pitch_speed, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( ideal_yaw, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( yaw_speed, FIELD_FLOAT ),

	DEFINE_ENTITY_FIELD( modelindex, FIELD_INTEGER ),
	DEFINE_ENTITY_GLOBAL_FIELD( model, FIELD_MODELNAME ),

	DEFINE_ENTITY_FIELD( viewmodel, FIELD_MODELNAME ),
	DEFINE_ENTITY_FIELD( weaponmodel, FIELD_MODELNAME ),

	DEFINE_ENTITY_FIELD( absmin, FIELD_POSITION_VECTOR ),
	DEFINE_ENTITY_FIELD( absmax, FIELD_POSITION_VECTOR ),
	DEFINE_ENTITY_GLOBAL_FIELD( mins, FIELD_VECTOR ),
	DEFINE_ENTITY_GLOBAL_FIELD( maxs, FIELD_VECTOR ),
	DEFINE_ENTITY_GLOBAL_FIELD( size, FIELD_VECTOR ),

	DEFINE_ENTITY_FIELD( ltime, FIELD_TIME ),
	DEFINE_ENTITY_FIELD( nextthink, FIELD_TIME ),

	DEFINE_ENTITY_FIELD( solid, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( movetype, FIELD_INTEGER ),

	DEFINE_ENTITY_FIELD( skin, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( body, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( effects, FIELD_INTEGER ),

	DEFINE_ENTITY_FIELD( gravity, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( friction, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( light_level, FIELD_FLOAT ),

	DEFINE_ENTITY_FIELD( frame, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( scale, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( sequence, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( animtime, FIELD_TIME ),
	DEFINE_ENTITY_FIELD( framerate, FIELD_FLOAT ),
	DEFINE_ENTITY_ARRAY( controller, FIELD_CHARACTER ),
	DEFINE_ENTITY_ARRAY( blending, FIELD_CHARACTER ),

	DEFINE_ENTITY_FIELD( rendermode, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( renderamt, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( rendercolor, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( renderfx, FIELD_INTEGER ),

	DEFINE_ENTITY_FIELD( health, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( frags, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( weapons, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( takedamage, FIELD_FLOAT ),

	DEFINE_ENTITY_FIELD( deadflag, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( view_ofs, FIELD_VECTOR ),
	DEFINE_ENTITY_FIELD( button, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( impulse, FIELD_INTEGER ),

	DEFINE_ENTITY_FIELD( chain, FIELD_EDICT ),
	DEFINE_ENTITY_FIELD( dmg_inflictor, FIELD_EDICT ),
	DEFINE_ENTITY_FIELD( enemy, FIELD_EDICT ),
	DEFINE_ENTITY_FIELD( aiment, FIELD_EDICT ),
	DEFINE_ENTITY_FIELD( owner, FIELD_EDICT ),
	DEFINE_ENTITY_FIELD( groundentity, FIELD_EDICT ),

	DEFINE_ENTITY_FIELD( spawnflags, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( flags, FIELD_FLOAT ),

	DEFINE_ENTITY_FIELD( colormap, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( team, FIELD_INTEGER ),

	DEFINE_ENTITY_FIELD( max_health, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( teleport_time, FIELD_TIME ),
	DEFINE_ENTITY_FIELD( armortype, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( armorvalue, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( waterlevel, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( watertype, FIELD_INTEGER ),

	// Having these fields be local to the individual levels makes it easier to test those levels individually.
	DEFINE_ENTITY_GLOBAL_FIELD( target, FIELD_STRING ),
	DEFINE_ENTITY_GLOBAL_FIELD( targetname, FIELD_STRING ),
	DEFINE_ENTITY_FIELD( netname, FIELD_STRING ),
	DEFINE_ENTITY_FIELD( message, FIELD_STRING ),

	DEFINE_ENTITY_FIELD( dmg_take, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( dmg_save, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( dmg, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( dmgtime, FIELD_TIME ),

	DEFINE_ENTITY_FIELD( noise, FIELD_SOUNDNAME ),
	DEFINE_ENTITY_FIELD( noise1, FIELD_SOUNDNAME ),
	DEFINE_ENTITY_FIELD( noise2, FIELD_SOUNDNAME ),
	DEFINE_ENTITY_FIELD( noise3, FIELD_SOUNDNAME ),
	DEFINE_ENTITY_FIELD( speed, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( air_finished, FIELD_TIME ),
	DEFINE_ENTITY_FIELD( pain_finished, FIELD_TIME ),
	DEFINE_ENTITY_FIELD( radsuit_finished, FIELD_TIME ),

	DEFINE_ENTITY_FIELD( oldbuttons, FIELD_INTEGER ),

	// this was saved as a part of player class before the new physics code
	DEFINE_ENTITY_FIELD( flDuckTime, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( flSwimTime, FIELD_FLOAT ),
	DEFINE_ENTITY_FIELD( flTimeStepSound, FIELD_FLOAT ), 

	// This is saved as a part of new "viewmodel animations save/restore"
	DEFINE_ENTITY_FIELD( weaponanim, FIELD_INTEGER ),
	DEFINE_ENTITY_FIELD( fuser1, FIELD_TIME ),

	DEFINE_ENTITY_FIELD( bInDuck, FIELD_INTEGER ),
};

#define ENTVARS_COUNT		ARRAYSIZE( gEntvarsDescription )

static int gSizes[FIELD_TYPECOUNT] = 
{
	FIELD_SIZE( FIELD_FLOAT ),
	FIELD_SIZE( FIELD_STRING ),
	FIELD_SIZE( FIELD_ENTITY ),
	FIELD_SIZE( FIELD_CLASSPTR ),
	FIELD_SIZE( FIELD_EHANDLE ),
	FIELD_SIZE( FIELD_EVARS ),
	FIELD_SIZE( FIELD_EDICT ),
	FIELD_SIZE( FIELD_VECTOR ),
	FIELD_SIZE( FIELD_POSITION_VECTOR ),
	FIELD_SIZE( FIELD_POINTER ),
	FIELD_SIZE( FIELD_INTEGER ),
	FIELD_SIZE( FIELD_FUNCTION ),
	FIELD_SIZE( FIELD_BOOLEAN ),
	FIELD_SIZE( FIELD_SHORT ),
	FIELD_SIZE( FIELD_CHARACTER ),
	FIELD_SIZE( FIELD_TIME ),
	FIELD_SIZE( FIELD_MODELNAME ),
	FIELD_SIZE( FIELD_SOUNDNAME ),
	FIELD_SIZE( FIELD_VOID ),
	FIELD_SIZE( FIELD_CUSTOM ),
	FIELD_SIZE( FIELD_EMBEDDED ),
};

// HACK for the gameinterface.cpp that I need to properly convert engine's data descriptions
int UTIL_GetFieldSize( int iFieldType )
{
	return gSizes[iFieldType];
}

// Base class includes common SAVERESTOREDATA pointer, and manages the entity table
CSaveRestoreBuffer :: CSaveRestoreBuffer( void )
{
	m_pdata = NULL;
}


CSaveRestoreBuffer :: CSaveRestoreBuffer( SAVERESTOREDATA *pdata )
{
	m_pdata = pdata;
}


CSaveRestoreBuffer :: ~CSaveRestoreBuffer( void )
{
}

int	CSaveRestoreBuffer :: EntityIndex( CBaseEntity *pEntity )
{
	if ( pEntity == NULL )
		return -1;
	return EntityIndex( pEntity->pev );
}


int	CSaveRestoreBuffer :: EntityIndex( entvars_t *pevLookup )
{
	if ( pevLookup == NULL )
		return -1;
	return EntityIndex( ENT( pevLookup ) );
}

int	CSaveRestoreBuffer :: EntityIndex( EOFFSET eoLookup )
{
	return EntityIndex( ENT( eoLookup ) );
}


int	CSaveRestoreBuffer :: EntityIndex( edict_t *pentLookup )
{
	if ( !m_pdata || pentLookup == NULL )
		return -1;

	int i;
	ENTITYTABLE *pTable;

	for ( i = 0; i < m_pdata->tableCount; i++ )
	{
		pTable = m_pdata->pTable + i;
		if ( pTable->pent == pentLookup )
			return i;
	}
	return -1;
}


edict_t *CSaveRestoreBuffer :: EntityFromIndex( int entityIndex )
{
	if ( !m_pdata || entityIndex < 0 )
		return NULL;

	int i;
	ENTITYTABLE *pTable;

	for ( i = 0; i < m_pdata->tableCount; i++ )
	{
		pTable = m_pdata->pTable + i;
		if ( pTable->id == entityIndex )
			return pTable->pent;
	}
	return NULL;
}


int	CSaveRestoreBuffer :: EntityFlagsSet( int entityIndex, int flags )
{
	if ( !m_pdata || entityIndex < 0 )
		return 0;
	if ( entityIndex > m_pdata->tableCount )
		return 0;

	m_pdata->pTable[ entityIndex ].flags |= flags;

	return m_pdata->pTable[ entityIndex ].flags;
}


void CSaveRestoreBuffer :: BufferRewind( int size )
{
	if ( !m_pdata )
		return;

	if ( m_pdata->size < size )
		size = m_pdata->size;

	m_pdata->pCurrentData -= size;
	m_pdata->size -= size;
}

#ifndef _WIN32
extern "C" {
unsigned _rotr ( unsigned val, int shift)
{
        register unsigned lobit;        /* non-zero means lo bit set */
        register unsigned num = val;    /* number to rotate */

        shift &= 0x1f;                  /* modulo 32 -- this will also make
                                           negative shifts work */

        while (shift--) {
                lobit = num & 1;        /* get high bit */
                num >>= 1;              /* shift right one bit */
                if (lobit)
                        num |= 0x80000000;  /* set hi bit if lo bit was set */
        }

        return num;
}
}
#endif

unsigned int CSaveRestoreBuffer :: HashString( const char *pszToken )
{
	unsigned int	hash = 0;

	while ( *pszToken )
		hash = _rotr( hash, 4 ) ^ *pszToken++;

	return hash;
}

unsigned short CSaveRestoreBuffer::TokenHash( const char *pszToken )
{
	unsigned short	hash = (unsigned short)(HashString( pszToken ) % (unsigned)m_pdata->tokenCount );
	
#if _DEBUG
	static int tokensparsed = 0;
	tokensparsed++;
	if ( !m_pdata->tokenCount || !m_pdata->pTokens )
		ALERT( at_error, "No token table array in TokenHash()!" );
#endif

	for ( int i=0; i<m_pdata->tokenCount; i++ )
	{
#if _DEBUG
		static bool beentheredonethat = false;
		if ( i > 50 && !beentheredonethat )
		{
			beentheredonethat = true;
			ALERT( at_error, "CSaveRestoreBuffer::TokenHash() is getting too full!" );
		}
#endif

		int	index = hash + i;
		if ( index >= m_pdata->tokenCount )
			index -= m_pdata->tokenCount;

		if ( !m_pdata->pTokens[index] || strcmp( pszToken, m_pdata->pTokens[index] ) == 0 )
		{
			m_pdata->pTokens[index] = (char *)pszToken;
			return index;
		}
	}
		
	// Token hash table full!!! 
	// [Consider doing overflow table(s) after the main table & limiting linear hash table search]
	ALERT( at_error, "CSaveRestoreBuffer::TokenHash() is COMPLETELY FULL!" );
	return 0;
}

bool CSaveRestoreBuffer::BufferSeek( int absPosition )
{
	if ( absPosition < 0 || absPosition >= m_pdata->bufferSize )
		return false;
	
	m_pdata->size = absPosition;
	m_pdata->pCurrentData = m_pdata->pBaseData + m_pdata->size;
	return true;
}

const char *CSaveRestoreBuffer::StringFromToken( int token )
{
	if ( token >= 0 && token < m_pdata->tokenCount )
		return m_pdata->pTokens[token];
	Assert( 0 );
	return "<<illegal>>";
}

//-----------------------------------------------------------------------------
//
// CSave
//
//-----------------------------------------------------------------------------

CSave::CSave( SAVERESTOREDATA *pdata )
:	CSaveRestoreBuffer( pdata )
{
	m_BlockStartStack.EnsureCapacity( 32 );
}

//-------------------------------------

void CSave::WriteShort( const short *value, int count )
{
	BufferData( (const char *)value, sizeof(short) * count );
}

//-------------------------------------

void CSave::WriteInt( const int *value, int count )
{
	BufferData( (const char *)value, sizeof(int) * count );
}

//-------------------------------------

void CSave::WriteBool( const bool *value, int count )
{
	COMPILE_TIME_ASSERT( sizeof(bool) == sizeof(char) );
	BufferData( (const char *)value, sizeof(bool) * count );
}

//-------------------------------------

void CSave::WriteString( const char *pstring )
{
	BufferData( pstring, strlen(pstring) + 1 );
}

void CSave :: WriteData( const char *pname, int size, const char *pdata )
{
	BufferField( pname, size, pdata );
}


void CSave :: WriteShort( const char *pname, const short *data, int count )
{
	BufferField( pname, sizeof(short) * count, (const char *)data );
}


void CSave :: WriteInt( const char *pname, const int *data, int count )
{
	BufferField( pname, sizeof(int) * count, (const char *)data );
}

//-------------------------------------

void CSave::WriteBool( const char *pname, const bool *data, int count )
{
	COMPILE_TIME_ASSERT( sizeof(bool) == sizeof(char) );
	BufferField( pname, sizeof(bool) * count, (const char *)data );
}

//-------------------------------------

void CSave :: WriteFloat( const char *pname, const float *data, int count )
{
	BufferField( pname, sizeof(float) * count, (const char *)data );
}


void CSave :: WriteTime( const char *pname, const float *data, int count )
{
	int i;
	Vector tmp, input;

	BufferHeader( pname, sizeof(float) * count );
	for ( i = 0; i < count; i++ )
	{
		float tmp = data[0];

		// Always encode time as a delta from the current time so it can be re-based if loaded in a new level
		// Times of 0 are never written to the file, so they will be restored as 0, not a relative time
		if ( m_pdata )
			tmp -= m_pdata->time;

		BufferData( (const char *)&tmp, sizeof(float) );
		data ++;
	}
}


void CSave :: WriteString( const char *pname, const char *pdata )
{
#ifdef TOKENIZE
	short	token = (short)TokenHash( pdata );
	WriteShort( pname, &token, 1 );
#else
	BufferField( pname, strlen(pdata) + 1, pdata );
#endif
}


void CSave :: WriteString( const char *pname, const int *stringId, int count )
{
	int i, size;

#ifdef TOKENIZE
	short	token = (short)TokenHash( STRING( *stringId ) );
	WriteShort( pname, &token, 1 );
#else
#if 0
	if ( count != 1 )
		ALERT( at_error, "No string arrays!\n" );
	WriteString( pname, (char *)STRING(*stringId) );
#endif

	size = 0;
	for ( i = 0; i < count; i++ )
		size += strlen( STRING( stringId[i] ) ) + 1;

	BufferHeader( pname, size );
	for ( i = 0; i < count; i++ )
	{
		const char *pString = STRING(stringId[i]);
		BufferData( pString, strlen(pString)+1 );
	}
#endif
}


void CSave :: WriteVector( const char *pname, const Vector &value )
{
	WriteVector( pname, &value.x, 1 );
}


void CSave :: WriteVector( const char *pname, const float *value, int count )
{
	BufferHeader( pname, sizeof(float) * 3 * count );
	BufferData( (const char *)value, sizeof(float) * 3 * count );
}



void CSave :: WritePositionVector( const char *pname, const Vector &value )
{

	if ( m_pdata && m_pdata->fUseLandmark )
	{
		Vector tmp = value - m_pdata->vecLandmarkOffset;
		WriteVector( pname, tmp );
	}

	WriteVector( pname, value );
}


void CSave :: WritePositionVector( const char *pname, const float *value, int count )
{
	int i;
	Vector tmp, input;

	BufferHeader( pname, sizeof(float) * 3 * count );
	for ( i = 0; i < count; i++ )
	{
		Vector tmp( value[0], value[1], value[2] );

		if ( m_pdata && m_pdata->fUseLandmark )
			tmp = tmp - m_pdata->vecLandmarkOffset;

		BufferData( (const char *)&tmp.x, sizeof(float) * 3 );
		value += 3;
	}
}


void CSave::WriteFunction( const char *pname, void **data, int count )
{
	AssertMsg( count == 1, "Arrays of functions not presently supported" );
	const char *functionName = NAME_FOR_FUNCTION( (uint32)*data );

	if ( functionName )
		BufferField( pname, strlen(functionName) + 1, functionName );
	else
		ALERT( at_error, "Invalid function pointer in entity!" );
}

//-------------------------------------

void CSave::WriteEntityPtr( const char *pname, CBaseEntity **ppEntity, int count )
{
	Assert( count <= MAX_ENTITYARRAY ); 
	int entityArray[MAX_ENTITYARRAY];
	for ( int i = 0; i < count; i++ )
	{
		entityArray[i] = EntityIndex( ppEntity[i] );
	}
	WriteInt( pname, entityArray, count );
}

//-------------------------------------

void CSave::WriteEntityPtr( CBaseEntity **ppEntity, int count )
{
	Assert( count <= MAX_ENTITYARRAY ); 
	int entityArray[MAX_ENTITYARRAY];
	for ( int i = 0; i < count; i++ )
	{
		entityArray[i] = EntityIndex( ppEntity[i] );
	}
	WriteInt( entityArray, count );
}

void EntvarsKeyvalue( entvars_t *pev, KeyValueData *pkvd )
{
	int i;
	typedescription_t		*pField;

	for ( i = 0; i < ENTVARS_COUNT; i++ )
	{
		pField = &gEntvarsDescription[i];

		if ( !stricmp( pField->fieldName, pkvd->szKeyName ) )
		{
			switch( pField->fieldType )
			{
			case FIELD_MODELNAME:
			case FIELD_SOUNDNAME:
			case FIELD_STRING:
				(*(int *)((char *)pev + pField->fieldOffset)) = ALLOC_STRING( pkvd->szValue );
				break;

			case FIELD_TIME:
			case FIELD_FLOAT:
				(*(float *)((char *)pev + pField->fieldOffset)) = atof( pkvd->szValue );
				break;

			case FIELD_INTEGER:
				(*(int *)((char *)pev + pField->fieldOffset)) = atoi( pkvd->szValue );
				break;

			case FIELD_POSITION_VECTOR:
			case FIELD_VECTOR:
				UTIL_StringToVector( (float *)((char *)pev + pField->fieldOffset), pkvd->szValue );
				break;

			default:
			case FIELD_EVARS:
			case FIELD_CLASSPTR:
			case FIELD_EDICT:
			case FIELD_ENTITY:
			case FIELD_POINTER:
				ALERT( at_error, "Bad field in entity!!\n" );
				break;
			}
			pkvd->fHandled = TRUE;
			return;
		}
	}
}



int CSave :: WriteEntVars( const char *pname, entvars_t *pev )
{
	return WriteFields( pname, pev, gEntvarsDescription, ENTVARS_COUNT );
}

//-------------------------------------

bool CSave::ShouldSaveField( const void *pData, typedescription_t *pField )
{
	if ( !(pField->flags & FTYPEDESC_SAVE) || pField->fieldType == FIELD_VOID )
		return false;

	switch ( pField->fieldType )
	{
	case FIELD_EMBEDDED:
		{
			if ( pField->flags & FTYPEDESC_PTR )
			{
				AssertMsg( pField->fieldSize == 1, "Arrays of embedded pointer types presently unsupported by save/restore" );
				if ( pField->fieldSize != 1 )
					return false;
			}

			AssertMsg( pField->td != NULL, "Embedded type appears to have not had type description implemented" );
			if ( pField->td == NULL )
				return false;

			int nFieldCount = pField->fieldSize;
			char *pTestData = (char *)( ( !(pField->flags & FTYPEDESC_PTR) ) ? pData : *((void **)pData) );
			while ( --nFieldCount >= 0 )
			{
				typedescription_t *pTestField = pField->td->dataDesc;
				typedescription_t *pLimit	  = pField->td->dataDesc + pField->td->dataNumFields;
			
				for ( ; pTestField < pLimit; ++pTestField )
				{
					if ( ShouldSaveField( pTestData + pTestField->fieldOffset, pTestField ) )
						return true;
				}

				pTestData += pField->fieldSizeInBytes;
			}
			return false;
		}

	case FIELD_CUSTOM:
		{
			// ask the data if it's empty
			SaveRestoreFieldInfo_t fieldInfo =
			{
				const_cast<void *>(pData),
				((char *)pData) - pField->fieldOffset,
				pField
			};
			if ( pField->pSaveRestoreOps->IsEmpty( fieldInfo ) )
				return false;
		}
		return true;

	default:
		{
			if ( (pField->fieldSizeInBytes != pField->fieldSize * gSizes[pField->fieldType]) )
			{
				ALERT( at_warning, "Field %s is using the wrong FIELD_ type!\nFix this or you'll see a crash.\n", pField->fieldName );
				Assert( 0 );
			}

			// old byte-by-byte null check
			if ( DataEmpty( (const char *)pData, pField->fieldSize * gSizes[pField->fieldType] ) )
				return false;
		}
		return true;
	}
}

int CSave::CountFieldsToSave( const void *pBaseData, typedescription_t *pFields, int fieldCount )
{
	int result = 0;
	for ( int i = 0; i < fieldCount; i++ )
	{
		if ( ShouldSaveField( (char *)pBaseData + pFields[i].fieldOffset, &pFields[i] ) )
			result++;
	}
	return result;
}

int CSave::WriteFields( const char *pname, const void *pBaseData, typedescription_t *pFields, int fieldCount )
{
	int				i, j, actualCount;
	typedescription_t	*pTest;
	int				entityArray[MAX_ENTITYARRAY];

	// Empty fields will not be written, write out the actual number of fields to be written
	actualCount = CountFieldsToSave( pBaseData, pFields, fieldCount );
	WriteInt( pname, &actualCount, 1 );

	for ( i = 0; i < fieldCount; i++ )
	{
		pTest = &pFields[ i ];

		void *pOutputData = ( (char *)pBaseData + pTest->fieldOffset);

		// UNDONE: Must we do this twice?
		if ( !ShouldSaveField( pOutputData, pTest ) )
			continue;

		switch( pTest->fieldType )
		{
		case FIELD_FLOAT:
			WriteFloat( pTest->fieldName, (float *)pOutputData, pTest->fieldSize );
		break;
		case FIELD_TIME:
			WriteTime( pTest->fieldName, (float *)pOutputData, pTest->fieldSize );
		break;
		case FIELD_MODELNAME:
		case FIELD_SOUNDNAME:
		case FIELD_STRING:
			WriteString( pTest->fieldName, (int *)pOutputData, pTest->fieldSize );
		break;
		case FIELD_CLASSPTR:
		case FIELD_EVARS:
		case FIELD_EDICT:
		case FIELD_ENTITY:
		case FIELD_EHANDLE:
			if ( pTest->fieldSize > MAX_ENTITYARRAY )
				ALERT( at_error, "Can't save more than %d entities in an array!!!\n", MAX_ENTITYARRAY );
			for ( j = 0; j < pTest->fieldSize; j++ )
			{
				switch( pTest->fieldType )
				{
					case FIELD_EVARS:
						entityArray[j] = EntityIndex( ((entvars_t **)pOutputData)[j] );
					break;
					case FIELD_CLASSPTR:
						entityArray[j] = EntityIndex( ((CBaseEntity **)pOutputData)[j] );
					break;
					case FIELD_EDICT:
						entityArray[j] = EntityIndex( ((edict_t **)pOutputData)[j] );
					break;
					case FIELD_ENTITY:
						entityArray[j] = EntityIndex( ((EOFFSET *)pOutputData)[j] );
					break;
					case FIELD_EHANDLE:
						entityArray[j] = EntityIndex( (CBaseEntity *)(((EHANDLE *)pOutputData)[j]) );
					break;
				}
			}
			WriteInt( pTest->fieldName, entityArray, pTest->fieldSize );
		break;
		case FIELD_POSITION_VECTOR:
			WritePositionVector( pTest->fieldName, (float *)pOutputData, pTest->fieldSize );
		break;
		case FIELD_VECTOR:
			WriteVector( pTest->fieldName, (float *)pOutputData, pTest->fieldSize );
		break;

		case FIELD_INTEGER:
			WriteInt( pTest->fieldName, (int *)pOutputData, pTest->fieldSize );
		break;

		case FIELD_BOOLEAN:
			WriteBool( pTest->fieldName, (bool *)pOutputData, pTest->fieldSize );
		break;

		case FIELD_SHORT:
			WriteData( pTest->fieldName, 2 * pTest->fieldSize, ((char *)pOutputData) );
		break;

		case FIELD_CHARACTER:
			WriteData( pTest->fieldName, pTest->fieldSize, ((char *)pOutputData) );
		break;

		// For now, just write the address out, we're not going to change memory while doing this yet!
		case FIELD_POINTER:
			WriteInt( pTest->fieldName, (int *)(char *)pOutputData, pTest->fieldSize );
		break;

		case FIELD_FUNCTION:
			WriteFunction( pTest->fieldName, (void **)pOutputData, pTest->fieldSize );
		break;

		case FIELD_CUSTOM:
		{
			// Note it is up to the custom type implementor to handle arrays
			StartBlock( pTest->fieldName );

			SaveRestoreFieldInfo_t fieldInfo =
			{
				pOutputData,
				((char *)pOutputData) - pTest->fieldOffset,
				pTest
			};
			pTest->pSaveRestoreOps->Save( fieldInfo, this );
			
			EndBlock();
			break;
		}

		case FIELD_EMBEDDED:
		{
			AssertMsg( ( (pTest->flags & FTYPEDESC_PTR) == 0 ) || (pTest->fieldSize == 1), "Arrays of embedded pointer types presently unsupported by save/restore" );
			int nFieldCount = pTest->fieldSize;
			char *pFieldData = (char *)( ( !(pTest->flags & FTYPEDESC_PTR) ) ? pOutputData : *((void **)pOutputData) );

			StartBlock( pTest->fieldName );

			while ( --nFieldCount >= 0 )
			{
				WriteAll( pFieldData, pTest->td );
				pFieldData += pTest->fieldSizeInBytes;
			}

			EndBlock();
			break;
		}

		default:
			ALERT( at_error, "Bad field type\n" );
			Assert(0);
		}
	}

	return 1;
}

//-------------------------------------
// Purpose: Recursively saves all the classes in an object, in reverse order (top down)
// Output : int 0 on failure, 1 on success

int CSave::DoWriteAll( const void *pLeafObject, datamap_t *pLeafMap, datamap_t *pCurMap )
{
	// save base classes first
	if ( pCurMap->baseMap )
	{
		int status = DoWriteAll( pLeafObject, pLeafMap, pCurMap->baseMap );
		if ( !status )
			return status;
	}

	return WriteFields( pCurMap->dataClassName, pLeafObject, pCurMap->dataDesc, pCurMap->dataNumFields );
}

//-------------------------------------

void CSave::StartBlock( const char *pszBlockName )
{
	BufferHeader( pszBlockName, 0 ); // placeholder
	m_BlockStartStack.AddToTail( m_pdata->size );
}

//-------------------------------------

void CSave::StartBlock()
{
	StartBlock( "" );
}

//-------------------------------------

void CSave::EndBlock()
{
	int endPos = m_pdata->size;
	int startPos = m_BlockStartStack[ m_BlockStartStack.Count() - 1 ];
	short sizeBlock = endPos - startPos;
	
	m_BlockStartStack.Remove( m_BlockStartStack.Count() - 1 );
	
	// Move to the the location where the size of the block was written & rewrite the size
	BufferSeek( startPos - sizeof( HEADER ) );
	BufferData( (const char *)&sizeBlock, sizeof(short) );
	
	BufferSeek( endPos );
}

void CSave :: BufferString( char *pdata, int len )
{
	char c = 0;

	BufferData( pdata, len );		// Write the string
	BufferData( &c, 1 );			// Write a null terminator
}


int CSave :: DataEmpty( const char *pdata, int size )
{
	for ( int i = 0; i < size; i++ )
	{
		if ( pdata[i] )
			return 0;
	}
	return 1;
}


void CSave :: BufferField( const char *pname, int size, const char *pdata )
{
	BufferHeader( pname, size );
	BufferData( pdata, size );
}


void CSave::BufferHeader( const char *pname, int size )
{
	short shortSize = size;
	short	hashvalue = TokenHash( pname );
	if ( size > SHRT_MAX || size < 0 )
	{
		ALERT( at_error, "CSave::BufferHeader() size parameter exceeds 'short'!" );
	}

	BufferData( (const char *)&shortSize, sizeof(short) );
	BufferData( (const char *)&hashvalue, sizeof(short) );
}


void CSave :: BufferData( const char *pdata, int size )
{
	if ( !m_pdata )
		return;

	if ( m_pdata->size + size > m_pdata->bufferSize )
	{
		ALERT( at_error, "Save/Restore overflow!" );
		m_pdata->size = m_pdata->bufferSize;
		return;
	}

	memcpy( m_pdata->pCurrentData, pdata, size );
	m_pdata->pCurrentData += size;
	m_pdata->size += size;
}


// --------------------------------------------------------------
//
// CRestore
//
// --------------------------------------------------------------

CRestore::CRestore( SAVERESTOREDATA *pdata )
:	CSaveRestoreBuffer( pdata ),
	m_global( 0 ),
	m_precache( true )
{
	m_BlockEndStack.EnsureCapacity( 32 );
}

//-------------------------------------

const char *CRestore::StringFromHeaderToken( int token )
{
	const char *pszResult = StringFromToken( token );
	return ( pszResult ) ? pszResult : "";
}

//-------------------------------------

void CRestore::ReadField( const HEADER &header, void *pDest, typedescription_t *pField )
{
	switch( pField->fieldType )
	{
		case FIELD_TIME:
		{
			ReadTime( (float *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_FLOAT:
		{
			ReadFloat( (float *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_MODELNAME:
		case FIELD_SOUNDNAME:
		{
			string_t *pStringDest = (string_t *)pDest;
			int nRead = ReadString( pStringDest, pField->fieldSize, header.size );
			if ( m_precache )
			{
				for ( int i = 0; i < nRead; i++ )
				{
					if ( !FStringNull( pStringDest[i] ) )
					{
						if ( pField->fieldType == FIELD_MODELNAME )
							PRECACHE_MODEL( STRING( pStringDest[i] ) );
						else if ( pField->fieldType == FIELD_SOUNDNAME )
							PRECACHE_SOUND( STRING( pStringDest[i] ) );
					}
				}
			}
			break;
		}

		case FIELD_STRING:
		{
			ReadString( (string_t *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_EVARS:
		{
			ReadEntityVars( (entvars_t **)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_CLASSPTR:
			ReadEntityPtr( (CBaseEntity **)pDest, pField->fieldSize, header.size );
			break;
			
		case FIELD_EDICT:
			ReadEdictPtr( (edict_t **)pDest, pField->fieldSize, header.size );
			break;
		case FIELD_EHANDLE:
			ReadEHandle( (EHANDLE *)pDest, pField->fieldSize, header.size );
			break;

		case FIELD_VECTOR:
		{
			ReadVector( ( Vector *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_POSITION_VECTOR:
		{
			ReadPositionVector( ( Vector *)pDest, pField->fieldSize, header.size );
			break;
		}
		
		case FIELD_INTEGER:
		{
			ReadInt( (int *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_BOOLEAN:
		{
			ReadBool( (bool *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_SHORT:
		{
			ReadShort( (short *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_CHARACTER:
		{
			ReadData( (char *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_POINTER:
		{
			ReadInt( (int *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_FUNCTION:
		{
			ReadFunction( (int *)pDest, pField->fieldSize, header.size );
			break;
		}

		case FIELD_CUSTOM:
		{
			// No corresponding "block" (see write) as it was used as the header of the field
			int posNextField = m_pdata->size + header.size;

			SaveRestoreFieldInfo_t fieldInfo =
			{
				pDest,
				((char *)pDest) - pField->fieldOffset,
				pField
			};
			
			pField->pSaveRestoreOps->Restore( fieldInfo, this );
			
			Assert( posNextField >= m_pdata->size );
			BufferSeek( posNextField );
			break;
		}

		case FIELD_EMBEDDED:
		{
			AssertMsg( (( pField->flags & FTYPEDESC_PTR ) == 0) || (pField->fieldSize == 1), "Arrays of embedded pointer types presently unsupported by save/restore" );
#ifdef _DEBUG
			int startPos = m_pdata->size;
#endif
			int nFieldCount = pField->fieldSize;
			char *pFieldData = (char *)( ( !(pField->flags & FTYPEDESC_PTR) ) ? pDest : *((void **)pDest) );
			while ( --nFieldCount >= 0 )
			{
				// No corresponding "block" (see write) as it was used as the header of the field
				ReadAll( pFieldData, pField->td );
				pFieldData += pField->fieldSizeInBytes;
			}
			Assert( m_pdata->size - startPos == header.size );
			break;
			
		}

		default:
			ALERT( at_error, "Bad field type\n" );
			Assert(0);
	}
}

//-------------------------------------

bool CRestore::ShouldReadField( typedescription_t *pField )
{
	if ( (pField->flags & FTYPEDESC_SAVE) == 0 )
		return false;

	if ( m_global && (pField->flags & FTYPEDESC_GLOBAL) )
		return false;

	return true;
}

//-------------------------------------

typedescription_t *CRestore::FindField( const char *pszFieldName, typedescription_t *pFields, int fieldCount, int *pCookie )
{
	int &fieldNumber = *pCookie;
	if ( pszFieldName )
	{
		typedescription_t *pTest;
		
		for ( int i = 0; i < fieldCount; i++ )
		{
			pTest = &pFields[fieldNumber];
			
			++fieldNumber;
			if ( fieldNumber == fieldCount )
				fieldNumber = 0;
			
			if ( stricmp( pTest->fieldName, pszFieldName ) == 0 )
				return pTest;
		}
	}

	fieldNumber = 0;
	return NULL;
}

//-------------------------------------

bool CRestore::ShouldEmptyField( typedescription_t *pField )
{
	// don't clear out fields that don't get saved, or that are handled specially
	if ( !( pField->flags & FTYPEDESC_SAVE ) )
		return false;

	// Don't clear global fields
	if ( m_global && (pField->flags & FTYPEDESC_GLOBAL) )
		return false;

	return true;
}

//-------------------------------------

void CRestore::EmptyFields( void *pBaseData, typedescription_t *pFields, int fieldCount )
{
	int i;
	for ( i = 0; i < fieldCount; i++ )
	{
		typedescription_t *pField = &pFields[i];
		if ( !ShouldEmptyField( pField ) )
			continue;

		void *pFieldData = (char *)pBaseData + pField->fieldOffset;
		switch( pField->fieldType )
		{
		case FIELD_CUSTOM:
			{
				SaveRestoreFieldInfo_t fieldInfo =
				{
					pFieldData,
					pBaseData,
					pField
				};
				pField->pSaveRestoreOps->MakeEmpty( fieldInfo );
			}
			break;

		case FIELD_EMBEDDED:
			{
				int nFieldCount = pField->fieldSize;
				char *pFieldMemory = (char *)( ( !(pField->flags & FTYPEDESC_PTR) ) ? pFieldData : *((void **)pFieldData) );
				while ( --nFieldCount >= 0 )
				{
					EmptyFields( pFieldMemory, pField->td->dataDesc, pField->td->dataNumFields );
					pFieldMemory += pField->fieldSizeInBytes;
				}
			}
			break;

		default:
			// NOTE: If you hit this assertion, you've got a bug where you're using 
			// the wrong field type for your field
			if ( pField->fieldSizeInBytes != pField->fieldSize * gSizes[pField->fieldType] )
			{
				ALERT( at_console, "Field %s is using the wrong FIELD_ type!\nFix this or you'll see a crash.\n", pField->fieldName );
				Assert( 0 );
			}
			memset( pFieldData, 0, pField->fieldSize * gSizes[pField->fieldType] );
			break;
		}
	}
}

//-------------------------------------

void CRestore::StartBlock( HEADER *pHeader )
{
	ReadHeader( pHeader );
	m_BlockEndStack.AddToTail( m_pdata->size + pHeader->size );	
}

//-------------------------------------

void CRestore::StartBlock( char szBlockName[] )
{
	HEADER header;
	StartBlock( &header );
	Q_strncpy( szBlockName, StringFromHeaderToken( header.token ), SIZE_BLOCK_NAME_BUF );
}

//-------------------------------------

void CRestore::StartBlock()
{
	char szBlockName[SIZE_BLOCK_NAME_BUF];
	StartBlock( szBlockName );
}

//-------------------------------------

void CRestore::EndBlock()
{
	int endPos = m_BlockEndStack[ m_BlockEndStack.Count() - 1 ];
	m_BlockEndStack.Remove( m_BlockEndStack.Count() - 1 );
	BufferSeek( endPos );
}
	
//-------------------------------------

int CRestore::ReadEntVars( const char *pname, entvars_t *pev )
{
	return ReadFields( pname, pev, gEntvarsDescription, ENTVARS_COUNT );
}

int CRestore::ReadFields( const char *pname, void *pBaseData, typedescription_t *pFields, int fieldCount )
{
	static int lastName = -1;
	Verify( ReadShort() == sizeof(int) );			// First entry should be an int
	int symName = TokenHash(pname);

	// Check the struct name
	if ( ReadShort() != symName )			// Field Set marker
	{
		const char *pLastName = StringFromHeaderToken( lastName );
		ALERT( at_error, "Expected %s found %s! (prev: %s)\n", pname, BufferPointer(), pLastName );
		BufferRewind( 2*sizeof(short) );
		return 0;
	}
	lastName = symName;

	// Clear out base data
	EmptyFields( pBaseData, pFields, fieldCount );

	// Skip over the struct name
	int i;
	int nFieldsSaved = ReadInt();						// Read field count
	int searchCookie = 0;								// Make searches faster, most data is read/written in the same order
	HEADER header;

	for ( i = 0; i < nFieldsSaved; i++ )
	{
		ReadHeader( &header );

		typedescription_t *pField = FindField( StringFromHeaderToken( header.token ), pFields, fieldCount, &searchCookie);
		if ( pField && ShouldReadField( pField ) )
		{
			ReadField( header, ((char *)pBaseData + pField->fieldOffset), pField );
		}
		else
		{
			BufferSkipBytes( header.size );			// Advance to next field
		}
	}
	
	return 1;
}

void CRestore::ReadHeader( HEADER *pheader )
{
	if ( pheader != NULL )
	{
		Assert(pheader != NULL);
		pheader->size = ReadShort();				// Read field size
		pheader->token = ReadShort();				// Read field name token
	}
	else
	{
		BufferSkipBytes( sizeof(short) * 2 );
	}
}

//-------------------------------------

short CRestore::ReadShort( void )
{
	short tmp = 0;

	BufferReadBytes( (char *)&tmp, sizeof(short) );

	return tmp;
}

//-------------------------------------

int	CRestore::ReadInt( void )
{
	int tmp = 0;

	BufferReadBytes( (char *)&tmp, sizeof(int) );

	return tmp;
}

//-------------------------------------
// Purpose: Recursively restores all the classes in an object, in reverse order (top down)
// Output : int 0 on failure, 1 on success

int CRestore::DoReadAll( void *pLeafObject, datamap_t *pLeafMap, datamap_t *pCurMap )
{
	// restore base classes first
	if ( pCurMap->baseMap )
	{
		int status = DoReadAll( pLeafObject, pLeafMap, pCurMap->baseMap );
		if ( !status )
			return status;
	}

	return ReadFields( pCurMap->dataClassName, pLeafObject, pCurMap->dataDesc, pCurMap->dataNumFields );
}

//-------------------------------------

char *CRestore::BufferPointer( void )
{
	if ( !m_pdata )
		return NULL;

	return m_pdata->pCurrentData;
}

void CRestore::BufferReadBytes( char *pOutput, int size )
{
	Assert( m_pdata !=NULL );

	if ( !m_pdata || Empty() )
		return;

	if ( (m_pdata->size + size) > m_pdata->bufferSize )
	{
		ALERT( at_error, "Restore overflow!" );
		m_pdata->size = m_pdata->bufferSize;
		return;
	}

	if ( pOutput )
		memcpy( pOutput, m_pdata->pCurrentData, size );
	m_pdata->pCurrentData += size;
	m_pdata->size += size;
}

void CRestore::BufferSkipBytes( int bytes )
{
	BufferReadBytes( NULL, bytes );
}

//-------------------------------------

int CRestore::ReadShort( short *pValue, int nElems, int nBytesAvailable )
{
	return ReadSimple( pValue, nElems, nBytesAvailable );
}

//-------------------------------------

int CRestore::ReadInt( int *pValue, int nElems, int nBytesAvailable )
{
	return ReadSimple( pValue, nElems, nBytesAvailable );
}

//-------------------------------------

int CRestore::ReadBool( bool *pValue, int nElems, int nBytesAvailable )
{
	COMPILE_TIME_ASSERT( sizeof(bool) == sizeof(char) );
	return ReadSimple( pValue, nElems, nBytesAvailable );
}

//-------------------------------------

int CRestore::ReadFloat( float *pValue, int nElems, int nBytesAvailable )
{
	return ReadSimple( pValue, nElems, nBytesAvailable );
}

//-------------------------------------

int CRestore::ReadData( char *pData, int size, int nBytesAvailable )
{
	return ReadSimple( pData, size, nBytesAvailable );
}


void CRestore::ReadString( char *pDest, int nSizeDest, int nBytesAvailable )
{
	const char *pString = BufferPointer();
	if ( !nBytesAvailable )
		nBytesAvailable = strlen( pString ) + 1;
	BufferSkipBytes( nBytesAvailable );

	strncpy(pDest, pString, nSizeDest );
}
	
//-------------------------------------

int CRestore::ReadString( string_t *pValue, int nElems, int nBytesAvailable )
{
	AssertMsg( nBytesAvailable > 0, "CRestore::ReadString() implementation does not currently support unspecified bytes available");
	
	int i;
	char *pString = BufferPointer();
	char *pLimit = pString + nBytesAvailable;
	for ( i = 0; i < nElems && pString < pLimit; i++ )
	{
		if ( *((char *)pString) == 0 )
			pValue[i] = iStringNull;
		else
			pValue[i] = ALLOC_STRING( (char *)pString );
		
		while (*pString)
			pString++;
		pString++;
	}

	BufferSkipBytes( nBytesAvailable );

	return i;
}

//-------------------------------------

int CRestore::ReadVector( Vector *pValue )
{
	BufferReadBytes( (char *)pValue, sizeof(float) * 3 );
	return 1;
}

//-------------------------------------

int CRestore::ReadVector( Vector *pValue, int nElems, int nBytesAvailable )
{
	return ReadSimple( pValue, nElems, nBytesAvailable );
}

//-------------------------------------

int CRestore::ReadTime( float *pValue, int count, int nBytesAvailable )
{
	float baseTime = m_pdata->time;
	int nRead = ReadFloat( pValue, count, nBytesAvailable );
	
	for ( int i = nRead - 1; i >= 0; i-- )
	{
		pValue[i] += baseTime;
	}
	
	return nRead;
}

int CRestore::ReadPositionVector( Vector *pValue )
{
	return ReadPositionVector( pValue, 1, sizeof(float) * 3 );
}

//-------------------------------------

int CRestore::ReadPositionVector( Vector *pValue, int count, int nBytesAvailable )
{
	Vector basePosition = m_pdata->vecLandmarkOffset;
	int nRead = ReadVector( pValue, count, nBytesAvailable );
	
	for ( int i = nRead - 1; i >= 0; i-- )
	{
		pValue[i] = pValue[i] + basePosition;
	}
	
	return nRead;
}

//-------------------------------------

int CRestore::ReadEntityVars( entvars_t **ppEntVars, int count, int nBytesAvailable )
{
	Assert( count <= MAX_ENTITYARRAY ); 
	int entityArray[MAX_ENTITYARRAY];
	edict_t *pEdict;
	
	int nRead = ReadInt( entityArray, count, nBytesAvailable );
	
	for ( int i = 0; i < nRead; i++ ) // nRead is never greater than count
	{
		pEdict = EntityFromIndex( entityArray[i] );
		ppEntVars[i] = (pEdict) ? VARS( pEdict ) : NULL;
	}
	
	if ( nRead < count)
	{
		memset( &ppEntVars[nRead], 0, ( count - nRead ) * sizeof(ppEntVars[0]) );
	}
	
	return nRead;
}

//-------------------------------------

int CRestore::ReadEntityPtr( CBaseEntity **ppEntity, int count, int nBytesAvailable )
{
	Assert( count <= MAX_ENTITYARRAY ); 
	int entityArray[MAX_ENTITYARRAY];
	edict_t *pEdict;
	
	int nRead = ReadInt( entityArray, count, nBytesAvailable );
	
	for ( int i = 0; i < nRead; i++ ) // nRead is never greater than count
	{
		pEdict = EntityFromIndex( entityArray[i] );
		ppEntity[i] = (pEdict) ? CBaseEntity::Instance( pEdict ) : NULL;
	}
	
	if ( nRead < count)
	{
		memset( &ppEntity[nRead], 0, ( count - nRead ) * sizeof(ppEntity[0]) );
	}
	
	return nRead;
}

//-------------------------------------
int CRestore::ReadEdictPtr( edict_t **ppEdict, int count, int nBytesAvailable )
{
	Assert( count <= MAX_ENTITYARRAY ); 
	int entityArray[MAX_ENTITYARRAY];
	
	int nRead = ReadInt( entityArray, count, nBytesAvailable );
	
	for ( int i = 0; i < nRead; i++ ) // nRead is never greater than count
	{
		ppEdict[i] = EntityFromIndex( entityArray[i] );
	}
	
	if ( nRead < count)
	{
		memset( &ppEdict[nRead], 0, ( count - nRead ) * sizeof(ppEdict[0]) );
	}
	
	return nRead;
}


//-------------------------------------

int CRestore::ReadEHandle( EHANDLE *pEHandle, int count, int nBytesAvailable )
{
	Assert( count <= MAX_ENTITYARRAY ); 
	int entityArray[MAX_ENTITYARRAY];
	edict_t *pEdict;

	int nRead = ReadInt( entityArray, count, nBytesAvailable );
	
	for ( int i = 0; i < nRead; i++ ) // nRead is never greater than count
	{
		pEdict = EntityFromIndex( entityArray[i] );
		pEHandle[i] = (pEdict) ? CBaseEntity::Instance( pEdict ) : NULL;
	}
	
	if ( nRead < count)
	{
		memset( &pEHandle[nRead], 0, ( count - nRead ) * sizeof(pEHandle[0]) );
	}
	
	return nRead;
}

//-------------------------------------

int CRestore::ReadFunction( int *pValue, int count, int nBytesAvailable )
{
	AssertMsg( nBytesAvailable > 0, "CRestore::ReadFunction() implementation does not currently support unspecified bytes available");
	
	char *pszFunctionName = BufferPointer();
	BufferSkipBytes( nBytesAvailable );
	
	AssertMsg( count == 1, "Arrays of functions not presently supported" );
	
	if ( *pszFunctionName == 0 )
		*pValue = NULL;
	else
		*pValue = FUNCTION_FROM_NAME( pszFunctionName );

	return 0;
}