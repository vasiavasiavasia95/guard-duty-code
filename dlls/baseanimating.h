//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef BASEANIMATING_H
#define BASEANIMATING_H
#ifdef _WIN32
#pragma once
#endif

#include "baseentity.h"

class CBaseAnimating : public CBaseDelay
{
public:
	DECLARE_CLASS( CBaseAnimating, CBaseDelay );

	DECLARE_DATADESC();

	// Basic Monster Animation functions

	studiohdr_t *GetModelPtr( void );

	float GetAnimTimeInterval( void );

	void StudioFrameAdvance( void ); // accumulate animation frame time from last time called until now
	int	 GetSequenceFlags( int iSequence );
	inline bool IsSequenceFinished( void )	{ return m_fSequenceFinished; }
	inline bool SequenceLoops( void )		{ return m_fSequenceLoops; }
	int  LookupActivity ( int activity );
	int  LookupActivityHeaviest ( int activity );
	int  LookupSequence ( const char *label );
	const char *GetSequenceName( int iSequence );
	void ResetSequenceInfo ( );
	// This will stop animation until you call ResetSequenceInfo() at some point in the future
	inline void StopAnimation( void ) { pev->framerate = 0; }
	void DispatchAnimEvents ( void ); // Handle events that have happend since last time called up until X seconds into the future
	virtual void HandleAnimEvent( animevent_t *pEvent ) { return; };
	float SetBoneController ( int iController, float flValue );
	void InitBoneControllers ( void );
	float SetBlending ( int iBlender, float flValue );
	int	LookupBone( const char *szName );
	void GetBonePosition ( int iBone, Vector &origin, Vector &angles );
	void GetAutomovement( Vector &origin, Vector &angles, float flInterval = 0.1 );
	int  FindTransition( int iEndingSequence, int iGoalSequence, int *piDir );
	void GetAttachment ( int iAttachment, Vector &origin, Vector &angles );
	void SetBodygroup( int iGroup, int iValue );
	int GetBodygroup( int iGroup );
	int ExtractBbox( int sequence, Vector& mins, Vector& maxs );
	void SetSequenceBox( void );

	// animation needs
	float				m_flFrameRate;		// computed FPS for current sequence
	float				m_flGroundSpeed;	// computed linear movement rate for current sequence
	float				m_flLastEventCheck;	// last time the event list was checked
	bool				m_fSequenceFinished;// flag set when StudioAdvanceFrame moves across a frame boundry
	bool				m_fSequenceLoops;	// true if the sequence loops
	float				m_flPrevAnimTime;
};

#endif // BASEANIMATING_H