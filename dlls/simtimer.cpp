//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "simtimer.h"

//-----------------------------------------------------------------------------

BEGIN_SIMPLE_DATADESC( CSimpleSimTimer )
	DEFINE_FIELD( CSimpleSimTimer,	m_next,			FIELD_TIME	),
END_DATADESC()

BEGIN_SIMPLE_DATADESC_( CSimTimer, CSimpleSimTimer )
	DEFINE_FIELD( CSimTimer,		m_interval,		FIELD_FLOAT	),
END_DATADESC()

BEGIN_SIMPLE_DATADESC_( CRandSimTimer, CSimpleSimTimer )
	DEFINE_FIELD( CRandSimTimer,		m_minInterval,		FIELD_FLOAT	),
	DEFINE_FIELD( CRandSimTimer,		m_maxInterval,		FIELD_FLOAT	),
END_DATADESC()

BEGIN_SIMPLE_DATADESC_( CStopwatchBase, CSimpleSimTimer )
	DEFINE_FIELD( CStopwatchBase, m_fIsRunning,	FIELD_BOOLEAN	),
END_DATADESC()

BEGIN_SIMPLE_DATADESC_( CStopwatch, CStopwatchBase )
	DEFINE_FIELD( CStopwatch, m_interval,		FIELD_FLOAT	),
END_DATADESC()

BEGIN_SIMPLE_DATADESC_( CRandStopwatch, CStopwatchBase )
	DEFINE_FIELD( CRandStopwatch,		m_minInterval,		FIELD_FLOAT	),
	DEFINE_FIELD( CRandStopwatch,		m_maxInterval,		FIELD_FLOAT	),
END_DATADESC()

//-----------------------------------------------------------------------------
