/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
/*

  h_ai.cpp - halflife specific ai code

*/



#include	"cbase.h"
#include	"game.h"
	
#define		NUM_LATERAL_CHECKS		13  // how many checks are made on each side of a monster looking for lateral cover
#define		NUM_LATERAL_LOS_CHECKS		6  // how many checks are made on each side of a monster looking for lateral cover

//float flRandom = RANDOM_FLOAT(0,1);

DLL_GLOBAL	bool	g_fDrawLines = FALSE;

//=========================================================
// 
// AI UTILITY FUNCTIONS
//
// !!!UNDONE - move CBaseMonster functions to monsters.cpp
//=========================================================

//=========================================================
// FBoxVisible - a more accurate ( and slower ) version
// of FVisible. 
//
// !!!UNDONE - make this CBaseMonster?
//=========================================================
bool FBoxVisible ( CBaseEntity *pLooker, CBaseEntity *pTarget, Vector &vecTargetOrigin, float flSize )
{
	// don't look through water
	if ((pLooker->pev->waterlevel != 3 && pTarget->pev->waterlevel == 3)
		|| (pLooker->pev->waterlevel == 3 && pTarget->pev->waterlevel == 0))
		return false;

	TraceResult tr;
	Vector	vecLookerOrigin = pLooker->EyePosition();//look through the monster's 'eyes'
	for (int i = 0; i < 5; i++)
	{
		Vector vecTarget = pTarget->pev->origin;
		vecTarget.x += RANDOM_FLOAT( pTarget->pev->mins.x + flSize, pTarget->pev->maxs.x - flSize);
		vecTarget.y += RANDOM_FLOAT( pTarget->pev->mins.y + flSize, pTarget->pev->maxs.y - flSize);
		vecTarget.z += RANDOM_FLOAT( pTarget->pev->mins.z + flSize, pTarget->pev->maxs.z - flSize);

		UTIL_TraceLine(vecLookerOrigin, vecTarget, ignore_monsters, ignore_glass, pLooker->edict(), &tr);
		
		if (tr.flFraction == 1.0)
		{
			vecTargetOrigin = vecTarget;
			return true;// line of sight is valid.
		}
	}
	return false;// Line of sight is not established
}

//-----------------------------------------------------------------------------
// Purpose: Returns the correct toss velocity to throw a given object at a point.
// Input  : pEntity - The entity that is throwing the object.
//			vecSpot1 - The point from which the object is being thrown.
//			vecSpot2 - The point TO which the object is being thrown.
//			flHeightMaxRatio - A scale factor indicating the maximum ratio of height
//				to total throw distance, measured from the higher of the two endpoints to
//				the apex. -1 indicates that there is no maximum.
//			flGravityAdj - Scale factor for gravity - should match the gravity scale
//				that the object will use in midair.
//			bRandomize - when true, introduces a little fudge to the throw
// Output : Velocity to throw the object with.
//-----------------------------------------------------------------------------
Vector VecCheckToss ( CBaseEntity *pEntity, const Vector &vecSpot1, Vector vecSpot2, float flHeightMaxRatio, float flGravityAdj, bool bRandomize )
{
	TraceResult		tr;
	Vector			vecMidPoint;// halfway point between Spot1 and Spot2
	Vector			vecApex;// highest point 
	Vector			vecScale;
	Vector			vecTossVel;
	Vector			vecTemp;
	float			flGravity = g_psv_gravity->value * flGravityAdj;

	if (vecSpot2.z - vecSpot1.z > 500)
	{
		// to high, fail
		return vec3_origin;
	}

	Vector forward, right;
	AngleVectors( pEntity->pev->angles, &forward, &right, NULL );

	if (bRandomize)
	{
		// toss a little bit to the left or right, not right down on the enemy's bean (head). 
		vecSpot2 = vecSpot2 + right * ( RANDOM_FLOAT(-8,8) + RANDOM_FLOAT(-16,16) );
		vecSpot2 = vecSpot2 + forward * ( RANDOM_FLOAT(-8,8) + RANDOM_FLOAT(-16,16) );
	}
	
	// calculate the midpoint and apex of the 'triangle'
	// UNDONE: normalize any Z position differences between spot1 and spot2 so that triangle is always RIGHT
	// get a rough idea of how high it can be thrown
	vecMidPoint = vecSpot1 + (vecSpot2 - vecSpot1) * 0.5;
	UTIL_TraceLine(vecMidPoint, vecMidPoint + Vector(0,0,300), ignore_monsters, pEntity->edict(), &tr);
	vecMidPoint = tr.vecEndPos;

	if ( tr.flFraction != 1.0 )
	{
		// (subtract 15 so the object doesn't hit the ceiling)
		vecMidPoint.z -= 15;
	}

	if (flHeightMaxRatio != -1)
	{
		// But don't throw so high that it looks silly. Maximize the height of the
		// throw above the highest of the two endpoints to a ratio of the throw length.
		float flHeightMax = flHeightMaxRatio * (vecSpot2 - vecSpot1).Length();
		float flHighestEndZ = V_max(vecSpot1.z, vecSpot2.z);
		if ((vecMidPoint.z - flHighestEndZ) > flHeightMax)
		{
			vecMidPoint.z = flHighestEndZ + flHeightMax;
		}
	}

	if (vecMidPoint.z < vecSpot1.z || vecMidPoint.z < vecSpot2.z)
	{
		// to not enough space, fail
		return vec3_origin;
	}

	// How high should the grenade travel to reach the apex
	float distance1 = (vecMidPoint.z - vecSpot1.z);
	float distance2 = (vecMidPoint.z - vecSpot2.z);

	// How long will it take for the grenade to travel this distance
	float time1 = sqrt( distance1 / (0.5 * flGravity) );
	float time2 = sqrt( distance2 / (0.5 * flGravity) );

	if (time1 < 0.1)
	{
		// too close
		return vec3_origin;
	}

	// how hard to throw sideways to get there in time.
	vecTossVel = (vecSpot2 - vecSpot1) / (time1 + time2);

	// how hard upwards to reach the apex at the right time.
	vecTossVel.z = flGravity * time1;

	// find the apex
	vecApex  = vecSpot1 + vecTossVel * time1;
	vecApex.z = vecMidPoint.z;

	UTIL_TraceLine(vecSpot1, vecApex, dont_ignore_monsters, pEntity->edict(), &tr);
	if (tr.flFraction != 1.0)
	{
		// fail!
		return vec3_origin;
	}

	// UNDONE: either ignore monsters or change it to not care if we hit our enemy
	UTIL_TraceLine(vecSpot2, vecApex, ignore_monsters, pEntity->edict(), &tr);
	if (tr.flFraction != 1.0)
	{
		// fail!
		return vec3_origin;
	}
	
	return vecTossVel;
}


//
// VecCheckThrow - returns the velocity vector at which an object should be thrown from vecspot1 to hit vecspot2.
// returns vec3_origin if throw is not feasible.
// 
Vector VecCheckThrow ( CBaseEntity *pEntity, const Vector &vecSpot1, Vector vecSpot2, float flSpeed, float flGravityAdj )
{
	float			flGravity = g_psv_gravity->value * flGravityAdj;

	Vector vecGrenadeVel = (vecSpot2 - vecSpot1);

	// throw at a constant time
	float time = vecGrenadeVel.Length( ) / flSpeed;
	vecGrenadeVel = vecGrenadeVel * (1.0 / time);

	// adjust upward toss to compensate for gravity loss
	vecGrenadeVel.z += flGravity * time * 0.5;

	Vector vecApex = vecSpot1 + (vecSpot2 - vecSpot1) * 0.5;
	vecApex.z += 0.5 * flGravity * (time * 0.5) * (time * 0.5);
	
	TraceResult tr;
	UTIL_TraceLine(vecSpot1, vecApex, dont_ignore_monsters, pEntity->edict(), &tr);
	if (tr.flFraction != 1.0)
	{
		// fail!
		return vec3_origin;
	}

	UTIL_TraceLine(vecSpot2, vecApex, ignore_monsters, pEntity->edict(), &tr);
	if (tr.flFraction != 1.0)
	{
		// fail!
		return vec3_origin;
	}

	return vecGrenadeVel;
}


