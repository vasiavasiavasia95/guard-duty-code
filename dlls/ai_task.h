//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Defines the tasks for default AI.
//
// $NoKeywords: $
//=============================================================================//

#ifndef AI_TASK_H
#define AI_TASK_H
#ifdef _WIN32
#pragma once
#endif

enum TaskStatus_e 
{
	TASKSTATUS_NEW =			 	0,			// Just started
	TASKSTATUS_RUN_MOVE_AND_TASK =	1,			// Running task & movement
	TASKSTATUS_RUN_MOVE	=			2,			// Just running movement
	TASKSTATUS_RUN_TASK	=			3,			// Just running task
	TASKSTATUS_COMPLETE	=			4,			// Completed, get next task
};

// an array of tasks is a task list
// an array of schedules is a schedule list
struct Task_t
{
	int		iTask;
	float	flData;
};

//=========================================================
// These are the shared tasks
//=========================================================
enum sharedtasks_e
{
		TASK_INVALID = 0,

		// Waits for the specified number of seconds.
		TASK_WAIT,

		// Waits for the specified number of seconds. Will constantly turn to 
		// face the enemy while waiting. 
		TASK_WAIT_FACE_ENEMY,

		// Wait until the player enters the same PVS as this character.
		TASK_WAIT_PVS,

		TASK_SUGGEST_STATE,

		// Walk to m_hTargetEnt's location
		TASK_WALK_TO_TARGET,

		// Run to m_hTargetEnt's location
		TASK_RUN_TO_TARGET,

		// Move to within specified range of m_hTargetEnt
		TASK_MOVE_TO_TARGET_RANGE,

		// Path to the enemy's location. Even if the enemy is unseen!
		TASK_GET_PATH_TO_ENEMY,

		// Path to the last place this character saw the enemy
		TASK_GET_PATH_TO_ENEMY_LKP,

		// Path to the dead enemy's carcass.
		TASK_GET_PATH_TO_ENEMY_CORPSE,

		TASK_GET_PATH_TO_LEADER,

		TASK_GET_PATH_TO_SPOT,

		// Build a path to m_hTargetEnt
		TASK_GET_PATH_TO_TARGET,

		TASK_GET_PATH_TO_HINTNODE,

		TASK_GET_PATH_TO_LASTPOSITION,

		// Path to source of loudest heard sound that I care about
		TASK_GET_PATH_TO_BESTSOUND,

		// Path to source of the strongest scend that I care about
		TASK_GET_PATH_TO_BESTSCENT,

		// Run the current path
		TASK_RUN_PATH,	

		// Walk the current path
		TASK_WALK_PATH,	

		// Walk the current path sideways (must be supported by animation)
		TASK_STRAFE_PATH,

		// Clear m_flMoveWaitFinished (timer that inhibits movement)
		TASK_CLEAR_MOVE_WAIT,

		// Store current position for later reference
		TASK_STORE_LASTPOSITION,

		// Clear stored position
		TASK_CLEAR_LASTPOSITION,

		// Play activity associate with the current hint
		TASK_PLAY_HINT_ACTIVITY,

		TASK_FIND_HINTNODE,
		TASK_CLEAR_HINTNODE,
		TASK_SMALL_FLINCH,
		TASK_BIG_FLINCH,
		TASK_FACE_IDEAL,
		TASK_FACE_ROUTE,
		TASK_FACE_ENEMY,
		TASK_FACE_HINTNODE,
		TASK_FACE_TARGET,
		TASK_FACE_LASTPOSITION,

		// Attack the enemy (should be facing the enemy)
		TASK_RANGE_ATTACK1,
		TASK_RANGE_ATTACK2,		
		TASK_MELEE_ATTACK1,		
		TASK_MELEE_ATTACK2,	

		// Reload weapon
		TASK_RELOAD,

		// Attack the enemy, but do not make yaw adjustments during the attack
		TASK_RANGE_ATTACK1_NOTURN,
		TASK_RANGE_ATTACK2_NOTURN,		
		TASK_MELEE_ATTACK1_NOTURN,		
		TASK_MELEE_ATTACK2_NOTURN,		

		// Reload weapon, do not make yaw adjustments
		TASK_RELOAD_NOTURN,

		// Execute special attack (user-defined)
		TASK_SPECIAL_ATTACK1,
		TASK_SPECIAL_ATTACK2,

		TASK_CROUCH,
		TASK_STAND,
		TASK_GUARD,
		TASK_STEP_LEFT,
		TASK_STEP_RIGHT,
		TASK_STEP_FORWARD,
		TASK_STEP_BACK,
		TASK_DODGE_LEFT,
		TASK_DODGE_RIGHT,
		TASK_SOUND_ANGRY,

		// Set current animation activity to the specified activity
		TASK_SET_ACTIVITY,

		// Immediately change to a schedule of the specified type
		TASK_SET_SCHEDULE,

		// Set the specified schedule to execute if the current schedule fails.
		TASK_SET_FAIL_SCHEDULE,

		// Return to use of default fail schedule
		TASK_CLEAR_FAIL_SCHEDULE,

		TASK_PLAY_SEQUENCE,
		TASK_PLAY_SEQUENCE_FACE_ENEMY,
		TASK_PLAY_SEQUENCE_FACE_TARGET,
		TASK_SOUND_IDLE,
		TASK_SOUND_WAKE,
		TASK_SOUND_PAIN,
		TASK_SOUND_DIE,

		// tries lateral cover first, then node cover
		TASK_FIND_COVER_FROM_BEST_SOUND,

		// tries lateral cover first, then node cover
		TASK_FIND_COVER_FROM_ENEMY,
			
		// Find a place to hide from the enemy, somewhere on either side of me
		TASK_FIND_LATERAL_COVER_FROM_ENEMY,

		TASK_FIND_NODE_COVER_FROM_ENEMY,
		TASK_FIND_NEAR_NODE_COVER_FROM_ENEMY,// data for this one is the MAXIMUM acceptable distance to the cover.
		TASK_FIND_FAR_NODE_COVER_FROM_ENEMY,// data for this one is there MINIMUM aceptable distance to the cover.
		TASK_FIND_COVER_FROM_ORIGIN,
		TASK_EAT,

		// Unhook from the AI system.
		TASK_DIE,

		// Wait until scripted sequence plays
		TASK_WAIT_FOR_SCRIPT,

		// Play scripted sequence animation
		TASK_PLAY_SCRIPT,
		TASK_ENABLE_SCRIPT,
		TASK_PLANT_ON_SCRIPT,
		TASK_FACE_SCRIPT,
		
		// Wait for 0 to specified number of seconds
		TASK_WAIT_RANDOM,

		// Wait forever (until this schedule is interrupted)
		TASK_WAIT_INDEFINITE,

		TASK_STOP_MOVING,

		// Turn left the specified number of degrees
		TASK_TURN_LEFT,

		// Turn right the specified number of degrees
		TASK_TURN_RIGHT,

		// Remember the specified piece of data
		TASK_REMEMBER,

		// Forget the specified piece of data
		TASK_FORGET,

		// Wait until current movement is complete. 
		TASK_WAIT_FOR_MOVEMENT,

		// Wander for a specfied amound of time
		TASK_WANDER,

		// Require an enemy be seen after the task is run to be considered a candidate enemy
		TASK_IGNORE_OLD_ENEMIES,

		// ======================================
		// IMPORTANT: This must be the last enum
		// ======================================
		LAST_SHARED_TASK

};

#endif // AI_TASK_H
