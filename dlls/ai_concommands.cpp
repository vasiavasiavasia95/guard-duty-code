//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Console commands for debugging and manipulating monsters.
//
//===========================================================================//

#include "cbase.h"
#include "basemonster.h"
#include "player.h"
#include "game.h"

//------------------------------------------------------------------------------
// Purpose: Disables all monsters
//------------------------------------------------------------------------------
void CC_AI_Disable( void )
{
	if (CBaseMonster::m_nDebugBits & bits_debugDisableAI)
	{
		CBaseMonster::m_nDebugBits &= ~bits_debugDisableAI;
		ALERT( at_console, "AI Enabled.\n" );
	}
	else
	{
		CBaseMonster::m_nDebugBits |= bits_debugDisableAI;
		ALERT(at_console, "AI Disabled.\n" );
	}
}

//------------------------------------------------------------------------------
// Purpose: monster step trough AI
//------------------------------------------------------------------------------
void CC_AI_Step( void )
{
	ALERT(at_console, "AI Stepping...\n" );

	// Start monster's stepping through tasks
	CBaseMonster::m_nDebugBits |= bits_debugStepAI;
	CBaseMonster::m_nDebugPauseIndex++;
}

//------------------------------------------------------------------------------
// Purpose: Resume normal AI processing after stepping
//------------------------------------------------------------------------------
void CC_AI_Resume( void )
{
	ALERT(at_console, "AI Resume...\n" );

	// End monster's stepping through tasks
	CBaseMonster::m_nDebugBits &= ~bits_debugStepAI;
}