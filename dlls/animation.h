/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef ANIMATION_H
#define ANIMATION_H

#ifndef MONSTEREVENT_H
#include "monsterevent.h"
#endif

struct studiohdr_t;
struct mstudioseqdesc_t;

extern int IsSoundEvent( int eventNumber );

int LookupActivity( studiohdr_t *pstudiohdr, int activity );
int LookupActivityHeaviest( studiohdr_t *pstudiohdr, int activity );
int LookupSequence( studiohdr_t *pstudiohdr, const char *label );
void GetSequenceInfo( studiohdr_t *pstudiohdr, int sequence, float *pflFrameRate, float *pflGroundSpeed );
const char *GetSequenceName( studiohdr_t *pstudiohdr, int iSequence );
int GetSequenceFlags( studiohdr_t *pstudiohdr, int sequence );
float SetController( studiohdr_t *pstudiohdr, entvars_t *pev, int iController, float flValue );
float SetBlending( studiohdr_t *pstudiohdr, entvars_t *pev, int iBlender, float flValue );
void GetEyePosition( studiohdr_t *pstudiohdr, Vector &vecEyePosition );
void SequencePrecache( studiohdr_t *pstudiohdr, const char *pSequenceName );
int FindTransition( studiohdr_t *pstudiohdr, int iEndingAnim, int iGoalAnim, int *piDir );
void SetBodygroup( studiohdr_t *pstudiohdr, int& body, int iGroup, int iValue );
int GetBodygroup( studiohdr_t *pstudiohdr, int body, int iGroup );

int GetAnimationEvent( studiohdr_t *pstudiohdr, int sequence, animevent_t *pMonsterEvent, float flStart, float flEnd, int index );
int ExtractBbox( studiohdr_t *pstudiohdr, int sequence, Vector& mins, Vector& maxs );

int FindBoneIndexByName( studiohdr_t *pstudiohdr, const char *pName );

// From /engine/studio.h
#define STUDIO_LOOPING		0x0001


#endif	//ANIMATION_H
