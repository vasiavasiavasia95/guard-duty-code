/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include	"cbase.h"
#include	"com_model.h"
#include	"studio.h"
#include	"r_studioint.h"
#include	"bone_setup.h"
#include	"server.h"

// Vasia: valve put this into animation.cpp
// but I made it it's own class for convenience similarly to studio render code

// The object, created on the stack.
CStudioModelBlending g_StudioBlending;

#define SV_IsValidEdict( e )	( e && !e->free )

void StudioGetAttachment( const edict_t *pEdict, int iAttachment, float *rgflOrigin, float *rgflAngles );

/*
====================
Init

====================
*/
void CStudioModelBlending::Init( float (*pRotationMatrix)[3][4], float (*pBoneTransform)[MAXSTUDIOBONES][3][4] )
{
	// Get pointers to engine data structures
	m_pRotationMatrix	= (float(*)[3][4])pRotationMatrix;
	m_pBoneTransform	= (float(*)[MAXSTUDIOBONES][3][4])pBoneTransform;
}

/*
====================
CStudioModelBlending

====================
*/
CStudioModelBlending::CStudioModelBlending( void )
{
	m_pRotationMatrix	= NULL;
	m_pBoneTransform	= NULL;
	m_pStudioHeader		= NULL;
}

/*
====================
~CStudioModelBlending

====================
*/
CStudioModelBlending::~CStudioModelBlending( void )
{
}

mstudioanim_t *CStudioModelBlending::StudioGetAnim( model_t *m_pSubModel, mstudioseqdesc_t *pseqdesc )
{
	mstudioseqgroup_t	*pseqgroup;
	cache_user_t *paSequences;

	pseqgroup = (mstudioseqgroup_t *)((byte *)m_pStudioHeader + m_pStudioHeader->seqgroupindex) + pseqdesc->seqgroup;

	if ( pseqdesc->seqgroup == 0 )
	{
		return (mstudioanim_t *)((byte *)m_pStudioHeader + pseqdesc->animindex);
	}

	paSequences = (cache_user_t *)m_pSubModel->submodels;

	if (paSequences == NULL)
	{
		paSequences = (cache_user_t *)IEngineStudio.Mem_Calloc( MAXSTUDIOGROUPS, sizeof( cache_user_t ) ); // UNDONE: leak!
		m_pSubModel->submodels = (dmodel_t *)paSequences;
	}

	if (!IEngineStudio.Cache_Check( (struct cache_user_s *)&(paSequences[pseqdesc->seqgroup])))
	{
		//gEngfuncs.Con_DPrintf("loading %s\n", pseqgroup->name );
		IEngineStudio.LoadCacheFile( pseqgroup->name, (struct cache_user_s *)&paSequences[pseqdesc->seqgroup] );
	}

	return (mstudioanim_t *)((byte *)paSequences[pseqdesc->seqgroup].data + pseqdesc->animindex);
}

/*
====================
StudioCalcBoneAdj

====================
*/
void CStudioModelBlending::StudioCalcBoneAdj( float *adj, const byte *pcontroller )
{
	int					i, j;
	float				value;
	mstudiobonecontroller_t *pbonecontroller;
	
	pbonecontroller = (mstudiobonecontroller_t *)((byte *)m_pStudioHeader + m_pStudioHeader->bonecontrollerindex);

	for (j = 0; j < m_pStudioHeader->numbonecontrollers; j++)
	{
		i = pbonecontroller[j].index;

		if ( i == STUDIO_MOUTH )
			continue; // ignore mouth

		if ( i <= MAXSTUDIOCONTROLLERS )
		{
			// check for 360% wrapping
			if( pbonecontroller[j].type & STUDIO_RLOOP )
			{
				value = pcontroller[i] * (360.0 / 256.0) + pbonecontroller[j].start;
			}
			else 
			{
				value = pcontroller[i] / 255.0;
				if (value < 0) value = 0;
				if (value > 1.0) value = 1.0;
				value = (1.0f - value) * pbonecontroller[j].start + value * pbonecontroller[j].end;
			}
		}

		switch( pbonecontroller[j].type & STUDIO_TYPES )
		{
		case STUDIO_XR:
		case STUDIO_YR:
		case STUDIO_ZR:
			adj[j] = value * (M_PI / 180.0);
			break;
		case STUDIO_X:
		case STUDIO_Y:
		case STUDIO_Z:
			adj[j] = value;
			break;
		}
	}
}

void CStudioModelBlending::StudioCalcBoneQuaterion(int frame, float s, mstudiobone_t *pbone, mstudioanim_t *panim, float *adj, float *q)
{
	vec4_t q1, q2;
	Vector angle1, angle2;

	for (int i = 0; i < 3; i++)
	{
		if (panim->offset[i + 3])
		{
			mstudioanimvalue_t *panimvalue = (mstudioanimvalue_t *)((byte *)panim + panim->offset[i + 3]);
			int j = (panimvalue->num.total < panimvalue->num.valid) ? 0 : frame;

			while (panimvalue->num.total <= j)
			{
				j -= panimvalue->num.total;
				panimvalue += panimvalue->num.valid + 1;

				if (panimvalue->num.total < panimvalue->num.valid)
					j = 0;
			}

			if (panimvalue->num.valid > j)
			{
				angle1[i] = panimvalue[j + 1].value;

				if (panimvalue->num.valid > j + 1)
					angle2[i] = panimvalue[j + 2].value;
				else if (panimvalue->num.total > j + 1)
					angle2[i] = angle1[i];
				else
					angle2[i] = panimvalue[panimvalue->num.valid + 2].value;
			}
			else
			{
				angle1[i] = panimvalue[panimvalue->num.valid].value;

				if (panimvalue->num.total > j + 1)
					angle2[i] = angle1[i];
				else
					angle2[i] = panimvalue[panimvalue->num.valid + 2].value;
			}

			angle1[i] = pbone->value[i + 3] + angle1[i] * pbone->scale[i + 3];
			angle2[i] = pbone->value[i + 3] + angle2[i] * pbone->scale[i + 3];
		}
		else
			angle2[i] = angle1[i] = pbone->value[i + 3];

		if (pbone->bonecontroller[i + 3] != -1)
		{
			angle1[i] += adj[pbone->bonecontroller[i + 3]];
			angle2[i] += adj[pbone->bonecontroller[i + 3]];
		}
	}

	if (!VectorCompare(angle1, angle2))
	{
		AngleQuaternion(angle1, q1);
		AngleQuaternion(angle2, q2);
		QuaternionSlerp(q1, q2, s, q);
	}
	else
		AngleQuaternion(angle1, q);
}


void CStudioModelBlending::StudioCalcBonePosition(int frame, float s, mstudiobone_t *pbone, mstudioanim_t *panim, float *adj, float *pos)
{
	for (int i = 0; i < 3; i++)
	{
		pos[i] = pbone->value[i];

		if (panim->offset[i] != 0)
		{
			mstudioanimvalue_t *panimvalue = (mstudioanimvalue_t *)((byte *)panim + panim->offset[i]);
			int j = (panimvalue->num.total < panimvalue->num.valid) ? 0 : frame;

			while (panimvalue->num.total <= j)
			{
				j -= panimvalue->num.total;
				panimvalue += panimvalue->num.valid + 1;

				if (panimvalue->num.total < panimvalue->num.valid)
					j = 0;
			}

			if (panimvalue->num.valid > j)
			{
				if (panimvalue->num.valid > j + 1)
					pos[i] += (panimvalue[j + 1].value * (1 - s) + s * panimvalue[j + 2].value) * pbone->scale[i];
				else
					pos[i] += panimvalue[j + 1].value * pbone->scale[i];
			}
			else
			{
				if (panimvalue->num.total <= j + 1)
					pos[i] += (panimvalue[panimvalue->num.valid].value * (1 - s) + s * panimvalue[panimvalue->num.valid + 2].value) * pbone->scale[i];
				else
					pos[i] += panimvalue[panimvalue->num.valid].value * pbone->scale[i];
			}
		}

		if (pbone->bonecontroller[i] != -1 && adj)
			pos[i] += adj[pbone->bonecontroller[i]];
	}
}

/*
====================
StudioSlerpBones

====================
*/
void CStudioModelBlending::StudioSlerpBones( vec4_t q1[], float pos1[][3], vec4_t q2[], float pos2[][3], float s )
{
	int			i;
	vec4_t		q3;
	float		s1;

	if (s < 0) s = 0;
	else if (s > 1.0) s = 1.0;

	s1 = 1.0 - s;

	for (i = 0; i < m_pStudioHeader->numbones; i++)
	{
		QuaternionSlerp( q1[i], q2[i], s, q3 );
		q1[i][0] = q3[0];
		q1[i][1] = q3[1];
		q1[i][2] = q3[2];
		q1[i][3] = q3[3];
		pos1[i][0] = pos1[i][0] * s1 + pos2[i][0] * s;
		pos1[i][1] = pos1[i][1] * s1 + pos2[i][1] * s;
		pos1[i][2] = pos1[i][2] * s1 + pos2[i][2] * s;
	}
}

/*
====================
StudioCalcRotations

====================
*/
void CStudioModelBlending::StudioCalcRotations( int boneused[], int numbones, const byte *pcontroller, float pos[][3], vec4_t *q, mstudioseqdesc_t *pseqdesc, mstudioanim_t *panim, float f )
{
	int		i, j, frame;
	mstudiobone_t	*pbone;
	float		adj[MAXSTUDIOCONTROLLERS];
	float		s;

	// bah, fix this bug with changing sequences too fast
	if( f > pseqdesc->numframes - 1 )
	{
		f = 0.0f;
	}
	else if( f < -0.01f )
	{
		// BUG ( somewhere else ) but this code should validate this data.
		// This could cause a crash if the frame # is negative, so we'll go ahead
		// and clamp it here
		f = -0.01f;
	}

	frame = (int)f;
	s = (f - frame);

	// add in programtic controllers
	pbone = (mstudiobone_t *)((byte *)m_pStudioHeader + m_pStudioHeader->boneindex);

	StudioCalcBoneAdj( adj, pcontroller );

	for( j = numbones - 1; j >= 0; j-- )
	{
		i = boneused[j];
		StudioCalcBoneQuaterion( frame, s, &pbone[i], &panim[i], adj, q[i] );
		StudioCalcBonePosition( frame, s, &pbone[i], &panim[i], adj, pos[i] );
	}

	if( pseqdesc->motiontype & STUDIO_X ) pos[pseqdesc->motionbone][0] = 0.0;
	if( pseqdesc->motiontype & STUDIO_Y ) pos[pseqdesc->motionbone][1] = 0.0;
	if( pseqdesc->motiontype & STUDIO_Z ) pos[pseqdesc->motionbone][2] = 0.0;
}

/*
====================
StudioSetupBones

====================
*/
void CStudioModelBlending::StudioSetupBones( struct model_s *pModel, float frame, int sequence, const float *angles, const float *origin,
const byte *pcontroller, const byte *pblending, int iBone, const edict_t *pEdict )
{
	int					i, j, numbones, boneused[MAXSTUDIOBONES];
	float				f, angles2[3];

	float				bonematrix[3][4];
	mstudiobone_t		*pbones;
	mstudioseqdesc_t	*pseqdesc;
	mstudioanim_t		*panim;

	static float		pos[MAXSTUDIOBONES][3];
	static vec4_t		q[MAXSTUDIOBONES];
	static float		pos2[MAXSTUDIOBONES][3];
	static vec4_t		q2[MAXSTUDIOBONES];

	angles2[0]	= -angles[0];
	angles2[1]	= angles[1];
	angles2[2]	= angles[2];

	m_pStudioHeader = (studiohdr_t *)IEngineStudio.Mod_Extradata( pModel );
	//if ( !m_pStudioHeader ) return;

	numbones = 0;
	if( sequence < 0 || sequence >= m_pStudioHeader->numseq )
	{
		ALERT( at_console, "SV_StudioSetupBones: sequence %i/%i out of range for model %s\n", sequence, m_pStudioHeader->numseq, m_pStudioHeader->name );
		sequence = 0;
	}

	pseqdesc = (mstudioseqdesc_t *)((byte *)m_pStudioHeader + m_pStudioHeader->seqindex) + sequence;
	pbones = (mstudiobone_t *)((byte *)m_pStudioHeader + m_pStudioHeader->boneindex);
	panim = StudioGetAnim( pModel, pseqdesc );

	if( iBone < -1 || iBone >= m_pStudioHeader->numbones )
		iBone = 0;

	if( iBone == -1 )
	{
		numbones = m_pStudioHeader->numbones;

		for ( i = 0; i < numbones; i++)
			boneused[(numbones - i) - 1] = i;
	}
	else
	{
		// only the parent bones
		for (i = iBone; i != -1; i = pbones[i].parent)
			boneused[numbones++] = i;
	}

	if( pseqdesc->numframes > 1 )
	{ 
		f = ( frame * ( pseqdesc->numframes - 1 )) / 256.0;
	}
	else
	{
		f = 0;
	}

	StudioCalcRotations( boneused, numbones, pcontroller, pos, q, pseqdesc, panim, f );

	if( pseqdesc->numblends > 1 )
	{
		float	s;

		//panim = R_StudioGetAnim( pModel, pseqdesc);
		panim += m_pStudioHeader->numbones;

		StudioCalcRotations( boneused, numbones, pcontroller, pos2, q2, pseqdesc, panim, f );

		s = (float)pblending[0] / 255.0;

		StudioSlerpBones( q, pos, q2, pos2, s );
	}

	AngleMatrix ( angles2, (*m_pRotationMatrix));

	MatrixSetColumn( origin, 3, (*m_pRotationMatrix) );
	
	for ( j = numbones - 1; j >= 0; j--)
	{
		i = boneused[j];
		QuaternionMatrix( q[i], bonematrix);

		bonematrix[0][3] = pos[i][0];
		bonematrix[1][3] = pos[i][1];
		bonematrix[2][3] = pos[i][2];

		if (pbones[i].parent == -1)
			ConcatTransforms((*m_pRotationMatrix), bonematrix, (*m_pBoneTransform)[i]);
		else
			ConcatTransforms((*m_pBoneTransform)[pbones[i].parent], bonematrix, (*m_pBoneTransform)[i]);
	}
}

/*
================
SV_ModelHandle
get model by handle
================
*/
model_t *SV_ModelHandle( int modelindex )
{
	if( modelindex < 0 || modelindex >= MAX_MODELS )
		return NULL;

	server_t *sv = SV_GetServer();

	return sv->models[modelindex];
}


/*
================
StudioGetAttachment

Gets the attachment origin and angles.

Ported to the game code to fix the fact engine doesn't set attachment angles
And to make the function safer by preventing illegal access errors
================
*/
void StudioGetAttachment( const edict_t *pEdict, int iAttachment, float *rgflOrigin, float *rgflAngles )
{
	if( !SV_IsValidEdict( pEdict ) )
		return;

	studiohdr_t *pstudiohdr;
	mstudioattachment_t	 *pAtt;
	Vector	angles;
	model_t	*mod;

	mod = SV_ModelHandle( pEdict->v.modelindex );
	pstudiohdr = (studiohdr_t *)IEngineStudio.Mod_Extradata( mod );
	if ( !pstudiohdr )
		return;

	// calculate attachment origin and angles
	pAtt = (mstudioattachment_t *)((byte *)pstudiohdr + pstudiohdr->attachmentindex) + iAttachment;

	VectorCopy( pEdict->v.angles, angles );

	// We have to do the stupid quake bug here because our StudioSetupBones is made to account for it when dealing with engine's hitbox stuff
	angles[PITCH] = -angles[PITCH];

	// Pass the the values to our setupbones interface
	g_StudioBlending.StudioSetupBones( mod, pEdict->v.frame, pEdict->v.sequence, angles, pEdict->v.origin, pEdict->v.controller, pEdict->v.blending, pAtt->bone, pEdict );

	if ( rgflOrigin )
		VectorTransform( pAtt->org, (*g_StudioBlending.m_pBoneTransform)[pAtt->bone], rgflOrigin );

	if ( rgflAngles )
		VectorCopy( pEdict->v.angles, rgflAngles );
}

void SV_StudioSetupBones( struct model_s *pModel, float frame, int sequence, const float *angles, const float *origin, 
const byte *pcontroller, const byte *pblending, int iBone, const edict_t *pEdict )
{
	g_StudioBlending.StudioSetupBones( pModel, frame, sequence, angles, origin, pcontroller, pblending, iBone, pEdict );
}

// The simple interface we'll pass back to the engine
static sv_blending_interface_t studio_blending =
{
	SV_BLENDING_INTERFACE_VERSION,
	SV_StudioSetupBones,
};

/*
====================
Server_GetBlendingInterface

Export this function for the engine to use the blending code to blend models
====================
*/
DLL_EXPORT int Server_GetBlendingInterface( int version, sv_blending_interface_t **ppInterface, server_studio_api_t *pStudio, float (*pRotationMatrix)[3][4], float (*pBoneTransform)[MAXSTUDIOBONES][3][4] )
{
	if( version != SV_BLENDING_INTERFACE_VERSION )
		return 0;

	// Point the engine to our callbacks
	*ppInterface = &studio_blending;

	// Copy in engine helper functions
	memcpy( &IEngineStudio, pStudio, sizeof( IEngineStudio ) );

	// Initialize local variables, etc.
	g_StudioBlending.Init( pRotationMatrix, pBoneTransform );

	// Success
	return 1;
}
