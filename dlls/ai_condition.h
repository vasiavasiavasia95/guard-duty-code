//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: The default shared conditions
//
//=============================================================================//

#ifndef	CONDITION_H
#define	CONDITION_H
#ifdef _WIN32
#pragma once
#endif

#ifndef MAX_CONDITIONS
#define	MAX_CONDITIONS 32*3
#endif

//=========================================================
// these bits represent conditions that may befall the monster, of which some are allowed 
// to interrupt certain schedules. 
//=========================================================
enum SCOND_t 
{
	COND_NONE,				// A way for a function to return no condition to get
	COND_NO_AMMO_LOADED,	// weapon needs to be reloaded!
	COND_SEE_HATE,			// see something that you hate
	COND_SEE_FEAR,			// see something that you are afraid of
	COND_SEE_DISLIKE,		// see something that you dislike
	COND_SEE_ENEMY,			// enemy is in full view.
	COND_LOST_ENEMY,
	COND_ENEMY_OCCLUDED,	// Can't see m_hEnemy
	COND_SMELL_FOOD,
	COND_ENEMY_TOO_FAR,
	COND_LIGHT_DAMAGE,		// hurt a little 
	COND_HEAVY_DAMAGE,		// hurt a lot
	COND_CAN_RANGE_ATTACK1,
	COND_CAN_RANGE_ATTACK2,
	COND_CAN_MELEE_ATTACK1,
	COND_CAN_MELEE_ATTACK2,
	COND_PROVOKED,
	COND_NEW_ENEMY,
	COND_HEAR_DANGER,
	COND_HEAR_COMBAT,
	COND_HEAR_WORLD,
	COND_HEAR_PLAYER,
	COND_SMELL,				// there is an interesting scent
	COND_ENEMY_FACING_ME,	// enemy is facing me
	COND_ENEMY_DEAD,		// enemy was killed. If you get this in combat, try to find another enemy. If you get it in alert, victory dance.
	COND_ENEMY_UNREACHABLE,	// Not connected to me via node graph
	COND_SEE_PLAYER,
	COND_SEE_NEMESIS,

	COND_TASK_FAILED,
	COND_SCHEDULE_DONE,

	COND_NO_CUSTOM_INTERRUPTS,		// Don't call BuildScheduleTestBits for this schedule. Used for schedules that must strictly control their interruptibility.

	// ======================================
	// IMPORTANT: This must be the last enum
	// ======================================
	LAST_SHARED_CONDITION	
};

#endif	//CONDITION_H