//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: encapsulates and implements all the accessing of the game dll from external
//			sources (only the engine at the time of writing)
//			This files ONLY contains functions and data necessary to build an interface
//			to external modules
//===========================================================================//

#include	"cbase.h"
#include	"saverestore.h"
#include	"client.h"
#include	"decals.h"
#include	"gamerules.h"
#include	"game.h"
#include	"pm_shared.h"
#include	"r_studioint.h"
#include	"gameinterface.h"
#include	"interface.h"
#include	"FileSystem.h"
#include	"igamesystem.h"
#include	"globalstate.h"

extern DLL_GLOBAL bool g_ServerLoadGame;

// Engine interfaces.
enginefuncs_t *engine = NULL;
server_studio_api_t IEngineStudio;
IFileSystem	*g_pFileSystem = NULL;

// Holds global variables shared between engine and game.
globalvars_t  *gpGlobals;

int UTIL_GetFieldSize( int iFieldType );

void GIVEFNPTRS_DLLEXPORT GiveFnptrsToDll( enginefuncs_t *pengfuncsFromEngine, globalvars_t *pGlobals )
{
	// copy the engine interface
	engine = pengfuncsFromEngine;
	gpGlobals = pGlobals;
}

static DLL_FUNCTIONS gFunctionTable = 
{
	GameDLLInit,				//pfnGameInit
	DispatchSpawn,				//pfnSpawn
	DispatchThink,				//pfnThink
	DispatchUse,				//pfnUse
	DispatchTouch,				//pfnTouch
	DispatchBlocked,			//pfnBlocked
	DispatchKeyValue,			//pfnKeyValue
	DispatchSave,				//pfnSave
	DispatchRestore,			//pfnRestore
	DispatchObjectCollsionBox,	//pfnAbsBox

	SaveWriteFields,			//pfnSaveWriteFields
	SaveReadFields,				//pfnSaveReadFields

	SaveGlobalState,			//pfnSaveGlobalState
	RestoreGlobalState,			//pfnRestoreGlobalState
	ResetGlobalState,			//pfnResetGlobalState

	ClientConnect,				//pfnClientConnect
	ClientDisconnect,			//pfnClientDisconnect
	ClientKill,					//pfnClientKill
	ClientPutInServer,			//pfnClientPutInServer
	ClientCommand,				//pfnClientCommand
	ClientUserInfoChanged,		//pfnClientUserInfoChanged
	ServerActivate,				//pfnServerActivate
	ServerDeactivate,			//pfnServerDeactivate

	PlayerPreThink,				//pfnPlayerPreThink
	PlayerPostThink,			//pfnPlayerPostThink

	StartFrame,					//pfnStartFrame
	ParmsNewLevel,				//pfnParmsNewLevel
	BuildAdjacentMapList,		//pfnParmsChangeLevel

	GetGameDescription,         //pfnGetGameDescription    Returns string describing current .dll game.
	PlayerCustomization,        //pfnPlayerCustomization   Notifies .dll of new customization for player.

	SpectatorConnect,			//pfnSpectatorConnect      Called when spectator joins server
	SpectatorDisconnect,        //pfnSpectatorDisconnect   Called when spectator leaves the server
	SpectatorThink,				//pfnSpectatorThink        Called when spectator sends a command packet (usercmd_t)

	Sys_Error,					//pfnSys_Error				Called when engine has encountered an error

	PM_Move,					//pfnPM_Move
	PM_Init,					//pfnPM_Init				Server version of player movement initialization
	PM_FindTextureType,			//pfnPM_FindTextureType
	
	SetupVisibility,			//pfnSetupVisibility        Set up PVS and PAS for networking for this client
	UpdateClientData,			//pfnUpdateClientData       Set up data sent only to specific client
	AddToFullPack,				//pfnAddToFullPack
	CreateBaseline,				//pfnCreateBaseline			Tweak entity baseline for network encoding, allows setup of player baselines, too.
	RegisterEncoders,			//pfnRegisterEncoders		Callbacks for network encoding
	GetWeaponData,				//pfnGetWeaponData
	CmdStart,					//pfnCmdStart
	CmdEnd,						//pfnCmdEnd
	ConnectionlessPacket,		//pfnConnectionlessPacket
	PM_GetHullBounds,			//pfnGetHullBounds
	CreateInstancedBaselines,   //pfnCreateInstancedBaselines
	InconsistentFile,			//pfnInconsistentFile
	AllowLagCompensation,		//pfnAllowLagCompensation
};

NEW_DLL_FUNCTIONS gNewDLLFunctions =
{
	OnFreeContainingEntity,		//pfnOnFreeEntPrivateData
	GameDLLShutdown,			//pfnGameShutdown
	DispatchCollide,			//pfnShouldCollide
};

/************************ ENGINE INTERFACE INITIALIZATION ************************/

DLL_EXPORT int GetEntityAPI( DLL_FUNCTIONS *pFunctionTable, int interfaceVersion )
{
	if ( !pFunctionTable || interfaceVersion != INTERFACE_VERSION )
	{
		return false;
	}
	
	memcpy( pFunctionTable, &gFunctionTable, sizeof( DLL_FUNCTIONS ) );
	return true;
}

DLL_EXPORT int GetEntityAPI2( DLL_FUNCTIONS *pFunctionTable, int *interfaceVersion )
{
	if ( !pFunctionTable || *interfaceVersion != INTERFACE_VERSION )
	{
		// Tell engine what version we had, so it can figure out who is out of date.
		*interfaceVersion = INTERFACE_VERSION;
		return false;
	}
	
	memcpy( pFunctionTable, &gFunctionTable, sizeof( DLL_FUNCTIONS ) );
	return true;
}

DLL_EXPORT int GetNewDLLFunctions( NEW_DLL_FUNCTIONS *pFunctionTable, int *interfaceVersion )
{
	if( !pFunctionTable || *interfaceVersion != NEW_DLL_FUNCTIONS_VERSION )
	{
		*interfaceVersion = NEW_DLL_FUNCTIONS_VERSION;
		return false;
	}

	memcpy( pFunctionTable, &gNewDLLFunctions, sizeof(gNewDLLFunctions) );
	return true;
}

/************************ ENGINE INTERFACE CALLBACKS IMPLEMENTATION ************************/

void GameDLLInit( void )
{
	if ( !LoadFileSystem() )
	{
		// Shut the game down as soon as possible.
		SERVER_COMMAND("quit\n");
		return;
	}

	// init the cvar list first in case inits want to reference them
	InitializeCvars();

	if ( !IGameSystem::InitAllSystems() )
	{
		// Shut the game down as soon as possible.
		SERVER_COMMAND("quit\n");
		return;
	}
}

void GameDLLShutdown( void )
{
	FreeFileSystem();

	IGameSystem::ShutdownAllSystems();
}

/*
================
Sys_Error

Engine is going to shut down, allows setting a breakpoint in game .dll to catch that occasion
================
*/
void Sys_Error( const char *error_string )
{
	// Default case, do nothing.  MOD AUTHORS:  Add code ( e.g., _asm { int 3 }; here to cause a breakpoint for debugging your game .dlls
}

/*
====================================================
SaveWriteFields

Saves data from a struct into a saverestore object, to be saved to disk
	pSaveData - the saverestore object
	char *pname - the name of the data to write
	*pBaseData - the struct into which the data is to be read
	*pFields - pointer to an array of data field descriptions
	fieldCount - the size of the array (number of field descriptions)
====================================================
*/
void SaveWriteFields( SAVERESTOREDATA *pSaveData, const char *pname, void *pBaseData, TYPEDESCRIPTION *pFields, int fieldCount )
{
	typedescription_t	pRemapFields[64];
	typedescription_t	*pInFields, *pField;

	pInFields = pField = pRemapFields;

	Assert( fieldCount <= 64 );

	// convert legacy typedescriptions since engine can't recognise our changes
	for( int i = 0; i < fieldCount; i++, pField++ )
	{
		pField->fieldType		= pFields[i].fieldType;
		pField->fieldName		= pFields[i].fieldName;
		pField->fieldOffset		= pFields[i].fieldOffset;
		pField->fieldSize		= pFields[i].fieldSize;
		pField->flags			= pFields[i].flags | FTYPEDESC_SAVE;
		pField->fieldSizeInBytes = pFields[i].fieldSize * UTIL_GetFieldSize( pFields[i].fieldType );
		pField->pSaveRestoreOps = NULL;
		pField->td = NULL;
	}

	CSave saveHelper( pSaveData );
	saveHelper.WriteFields( pname, pBaseData, pInFields, fieldCount );
}

/*
==================================================
SaveReadFields

Reads data from a save/restore block into a structure
	*pSaveData - the saverestore object
	char *pname - the name of the data to extract from
	*pBaseData - the struct into which the data is to be restored
	*pFields - pointer to an array of data field descriptions
	fieldCount - the size of the array (number of field descriptions)
==================================================
*/
void SaveReadFields( SAVERESTOREDATA *pSaveData, const char *pname, void *pBaseData, TYPEDESCRIPTION *pFields, int fieldCount )
{
	typedescription_t	pRemapFields[64];
	typedescription_t	*pInFields, *pField;

	pInFields = pField = pRemapFields;

	Assert( fieldCount <= 64 );

	// convert legacy typedescriptions since engine can't recognise our changes
	for( int i = 0; i < fieldCount; i++, pField++ )
	{
		pField->fieldType		= pFields[i].fieldType;
		pField->fieldName		= pFields[i].fieldName;
		pField->fieldOffset		= pFields[i].fieldOffset;
		pField->fieldSize		= pFields[i].fieldSize;
		pField->flags			= pFields[i].flags | FTYPEDESC_SAVE;
		pField->fieldSizeInBytes = pFields[i].fieldSize * UTIL_GetFieldSize( pFields[i].fieldType );
		pField->pSaveRestoreOps = NULL;
		pField->td = NULL;
	}

	CRestore restoreHelper( pSaveData );
	restoreHelper.ReadFields( pname, pBaseData, pInFields, fieldCount );
}

// This little hack lets me marry BSP names to messages in titles.txt
typedef struct
{
	char	*pBSPName;
	char	*pTitleName;
} TITLECOMMENT;

// LATER: Make a new worldspawn variable to set the title from Hammer ( SDK feature maybe???)
// For now, read titles from the hardcoded table below.
static TITLECOMMENT gTitleComments[] =
{
#ifdef OLD_CHAPTERNAMES
	{ "ba_tram",		"#BA_TRAMTITLE" },
	{ "ba_security",	"#BA_SECURITYTITLE" },
	{ "ba_main",		"#BA_SECURITYTITLE" },
	{ "ba_elevator",	"#BA_SECURITYTITLE" },
	{ "ba_canal",		"#BA_CANALSTITLE" },
	{ "ba_yard",		"#BA_YARDTITLE" },
	{ "ba_xen",			"#BA_XENTITLE" },
	{ "ba_hazard",		"#BA_HAZARD" },
	{ "ba_power",		"#BA_POWERTITLE" },
	{ "ba_teleport1",	"#BA_YARDTITLE" },
	{ "ba_teleport",	"#BA_TELEPORTTITLE" },
	{ "ba_outro",		"#BA_OUTRO" },
	{ "ba_office",		"#BA_OFFICETITLE" }
#else
	{ "ba_office",		"#barney_Chapter1_Title" },
	{ "ba_tram",		"#barney_Chapter1_Title" },
	{ "ba_security",	"#barney_Chapter2_Title" },
	{ "ba_main",		"#barney_Chapter2_Title" },
	{ "ba_elevator",	"#barney_Chapter2_Title" },
	{ "ba_canal",		"#barney_Chapter3_Title" },
	{ "ba_yard",		"#barney_Chapter4_Title" },
	{ "ba_xen",			"#barney_Chapter5_Title" },
	{ "ba_hazard",		"#BA_HAZARD" },
	{ "ba_power",		"#barney_Chapter6_Title" },
	{ "ba_teleport1",	"#barney_Chapter4_Title" },
	{ "ba_teleport",	"#barney_Chapter7_Title" },
	{ "ba_outro",		"#barney_Chapter8_Title" }
#endif // OLD_CHAPTERNAMES
};

/*
================================
SV_SaveGameComment

Engine calls this everytime a player saves the game, to read save titles provided by game .dll.
================================
*/
DLL_EXPORT void SV_SaveGameComment( char *pszBuffer, int iSizeBuffer )
{
	const char	*pTitleName;
	int		i;

	const char *mapname = STRING(gpGlobals->mapname); // Initialize save title as a map's name

	pTitleName = NULL;

	// Try to find a matching title comment for this mapname
	for ( i = 0; i < ARRAYSIZE(gTitleComments) && !pTitleName; i++)
	{
		// Vasia: It is important to use strnicmp because it doesn't have discrimination between uppercase and lowercase letters.
		if ( !strnicmp( mapname, gTitleComments[i].pBSPName, strlen( gTitleComments[i].pBSPName )) )
		{
			pTitleName = gTitleComments[i].pTitleName;
		}
	}

	// If we didn't get one, use the designer's map name, or the BSP name itself
	if ( !pTitleName )
	{
		pTitleName = mapname;
	}

	// Copy our title to pszBuffer
	strncpy(pszBuffer, pTitleName, iSizeBuffer - 1);
	pszBuffer[iSizeBuffer - 1] = '\0';
}

/*
================
BuildAdjacentMapList

Called during a transition or save, to build a map adjacency list
================
*/
void BuildAdjacentMapList( void )
{
	// retrieve the pointer to the save data
	SAVERESTOREDATA *pSaveData = (SAVERESTOREDATA *)gpGlobals->pSaveData;

	if ( pSaveData )
		pSaveData->connectionCount = BuildChangeList( pSaveData->levelList, MAX_LEVEL_CONNECTIONS );

	// Vasia: This is a part of our Map load type hack. 
	// We have to do this because by the time engine calls into CWorld's Precache sv->loadgame is already false
	// Since we need to reliably check for the moment engine loads the save we update this pretty global here and let worldspawn do the magic.
	g_ServerLoadGame = TRUE;
}

/*
================================
UpdateChapterRestrictions

Updates which chapters are unlocked

Added for our new chapter selection system
================================
*/
void UpdateChapterRestrictions( const char *mapname )
{
	if ( !g_psv_unlockedchapters )
		return;

	// look at the chapter for this map
	char chapterTitle[64];
	chapterTitle[0] = 0;
	for ( int i = 0; i < ARRAYSIZE(gTitleComments); i++ )
	{
		if ( !strnicmp( mapname, gTitleComments[i].pBSPName, strlen(gTitleComments[i].pBSPName) ) )
		{
			// found
			strncpy( chapterTitle, gTitleComments[i].pTitleName, sizeof( chapterTitle ) );
			break;
		}
	}

	if ( !chapterTitle[0] )
		return;

	// make sure the specified chapter title is unlocked
	_strlwr( chapterTitle );
	
	// Get our active mod directory name
	char modDir[MAX_PATH];

	GET_GAME_DIR(modDir);

	if ( !modDir[0] )
		return;

	char chapterNumberPrefix[64];
	snprintf(chapterNumberPrefix, sizeof(chapterNumberPrefix), "#%s_chapter", modDir);

	const char *newChapterNumber = strstr( chapterTitle, chapterNumberPrefix );
	if ( newChapterNumber )
	{
		// cut off the front
		newChapterNumber += strlen( chapterNumberPrefix );
		char newChapter[32];
		strncpy( newChapter, newChapterNumber, sizeof(newChapter) );

		// cut off the end
		char *end = strstr( newChapter, "_title" );
		if ( end )
		{
			*end = 0;
		}

		int nNewChapter = atoi( newChapter );

		// ok we have the string, see if it's newer
		const char *unlockedChapter = g_psv_unlockedchapters->string;
		int nUnlockedChapter = atoi( unlockedChapter );

		if ( nUnlockedChapter < nNewChapter )
		{
			// ok we're at a higher chapter, unlock
			engine->pfnCvar_DirectSet( g_psv_unlockedchapters, newChapter);
		}
	}
}

/************************ EXTRA ENGINE INTERFACE INITIALIZATION HELPERS ************************/

CSysModule *g_pFileSystemModule = NULL;

bool LoadFileSystem( void )
{
	if ( g_pFileSystem )
	{
		//Already loaded.
		return true;
	}

	// Determine which filesystem to use.
#if defined ( _WIN32 )
	char *szFsModule = "filesystem_stdio.dll";
#elif defined(OSX)
	char *szFsModule = "filesystem_stdio.dylib";	
#elif defined(LINUX)
	char *szFsModule = "filesystem_stdio.so";
#else
#error
#endif

	// Get filesystem interface.
	// The library is located next to the game exe, so there is no need to resolve the path first.
	g_pFileSystemModule = Sys_LoadModule(szFsModule);
	assert( g_pFileSystemModule );
	if( !g_pFileSystemModule )
	{
		return false;
	}

	CreateInterfaceFn fileSystemFactory = Sys_GetFactory( g_pFileSystemModule );
	if( !fileSystemFactory )
	{
		return false;
	}

	g_pFileSystem = ( IFileSystem * )fileSystemFactory( FILESYSTEM_INTERFACE_VERSION, NULL );
	assert( g_pFileSystem );
	if( !g_pFileSystem )
	{
		return false;
	}

	return true;
}

void FreeFileSystem( void )
{
	if ( g_pFileSystem )
	{
		g_pFileSystem = NULL;
	}

	if ( g_pFileSystemModule )
	{
		Sys_UnloadModule( g_pFileSystemModule );
	}
}