/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
#ifndef DEFAULTAI_H
#define DEFAULTAI_H

//=========================================================
// These are the schedule types
//=========================================================
typedef enum 
{
	SCHED_NONE = 0,
	SCHED_IDLE_STAND,
	SCHED_IDLE_WALK,
	SCHED_IDLE_ACTIVE,
	SCHED_WAKE_ANGRY,
	SCHED_WAKE_CALLED,
	SCHED_ALERT_FACE,
	SCHED_ALERT_SMALL_FLINCH,
	SCHED_ALERT_BIG_FLINCH,
	SCHED_ALERT_STAND,
	SCHED_INVESTIGATE_SOUND,
	SCHED_COMBAT_FACE,
	SCHED_COMBAT_STAND,
	SCHED_CHASE_ENEMY,
	SCHED_CHASE_ENEMY_FAILED,
	SCHED_VICTORY_DANCE,
	SCHED_TARGET_FACE,
	SCHED_TARGET_CHASE,
	SCHED_SMALL_FLINCH,
	SCHED_BIG_FLINCH,
	SCHED_TAKE_COVER_FROM_ENEMY,
	SCHED_TAKE_COVER_FROM_BEST_SOUND,
	SCHED_TAKE_COVER_FROM_ORIGIN,
	SCHED_FAIL_TAKE_COVER,
	SCHED_COWER, // usually a last resort!
	SCHED_MELEE_ATTACK1,
	SCHED_MELEE_ATTACK2,
	SCHED_RANGE_ATTACK1,
	SCHED_RANGE_ATTACK2,
	SCHED_SPECIAL_ATTACK1,
	SCHED_SPECIAL_ATTACK2,
	SCHED_STANDOFF,
	SCHED_ARM_WEAPON,
	SCHED_RELOAD,
	SCHED_GUARD,
	SCHED_AMBUSH,
	SCHED_DIE,
	SCHED_WAIT_TRIGGER,
	SCHED_FOLLOW,
	SCHED_SLEEP,
	SCHED_WAKE,
	SCHED_BARNACLE_VICTIM_GRAB,
	SCHED_BARNACLE_VICTIM_CHOMP,
	SCHED_AISCRIPT,
	SCHED_SCRIPTED_WALK,
	SCHED_SCRIPTED_RUN,
	SCHED_SCRIPTED_WAIT,
	SCHED_SCRIPTED_FACE,
	SCHED_FAIL,
	SCHED_FAIL_NOSTOP,

	// ======================================
	// IMPORTANT: This must be the last enum
	// ======================================
	LAST_SHARED_SCHEDULE

} SCHEDULE_TYPE;

#endif		// DEFAULTAI_H
