//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef	SQUADSLOT_H
#define	SQUADSLOT_H

#define	MAX_SQUADSLOTS 32

//=========================================================
// These are the default shared squad slots
//=========================================================
enum SQUAD_SLOT_t {

	// Currently there are no shared squad slots
	SQUAD_SLOT_NONE = -1,

	SQUAD_SLOT_ATTACK1 = 0,		// reserve 2 attack slots for most squads
	SQUAD_SLOT_ATTACK2,

	// ======================================
	// IMPORTANT: This must be the last enum
	// ======================================
	LAST_SHARED_SQUADSLOT,		
};

#endif	//SQUADSLOT_H
