/***
*
*	Copyright (c) 1999, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "cbase.h"
#include "weapons.h"
#include "player.h"
#include "gamerules.h"
#include "skill.h"

#define	BATON_RANGE				32.0f
#define	BATON_REFIRE_MISS		0.5f
#define	BATON_REFIRE_HIT		0.25f

#define	BATON_HIT_VOLUME 128

static const Vector g_TraceCheckMins( -16, -16, -18 );
static const Vector g_TraceCheckMaxs( 16, 16, 18 );

extern cvar_t sk_plr_crowbar;

class CWeaponBaton : public CBasePlayerWeapon
{
	DECLARE_CLASS( CWeaponBaton, CBasePlayerWeapon );
public:

	void			Spawn( void );
	void			Precache( void );
	bool			Deploy( void );
	void			Holster( void );
	virtual void	ItemPostFrame( void );
	void			PrimaryAttack( void );
	void			WeaponIdle( void );

	int		iItemSlot( void ) { return 1; }
	int		GetItemInfo(ItemInfo *p);
	
private:
	virtual void		Swing( void );
	virtual	void		Hit( void );

	TraceResult		m_traceHit;
	int				m_iSwing;
};

LINK_ENTITY_TO_CLASS( weapon_baton, CWeaponBaton );

PRECACHE_WEAPON_REGISTER( weapon_baton );


enum crowbar_e {
	CROWBAR_IDLE = 0,
	CROWBAR_IDLE1,
	CROWBAR_ATTACK1MISS,
	CROWBAR_ATTACK2MISS,
	CROWBAR_HOLSTER,
	CROWBAR_DRAW,
	CROWBAR_ATTACK1HIT,
	CROWBAR_ATTACK2HIT,
};


void CWeaponBaton::Spawn( )
{
	Precache( );
	m_iId = WEAPON_CROWBAR;
	SET_MODEL(ENT(pev), "models/w_baton.mdl");
	m_iClip = -1;

	FallInit();// get ready to fall down.
}


void CWeaponBaton::Precache( void )
{
	PRECACHE_MODEL("models/v_baton.mdl");
	PRECACHE_MODEL("models/w_baton.mdl");
	PRECACHE_MODEL("models/p_crowbar.mdl");
	PRECACHE_SOUND("weapons/cbar_hit1.wav");
	PRECACHE_SOUND("weapons/cbar_hit2.wav");
	PRECACHE_SOUND("weapons/cbar_hitbod1.wav");
	PRECACHE_SOUND("weapons/cbar_hitbod2.wav");
	PRECACHE_SOUND("weapons/cbar_hitbod3.wav");
	PRECACHE_SOUND("weapons/cbar_miss1.wav");
}

int CWeaponBaton::GetItemInfo(ItemInfo *p)
{
	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = NULL;
	p->iMaxAmmo1 = -1;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip = WEAPON_NOCLIP;
	p->iSlot = 0;
	p->iPosition = 0;
	p->iId = WEAPON_CROWBAR;
	p->iWeight = CROWBAR_WEIGHT;
	return 1;
}



bool CWeaponBaton::Deploy( )
{
	return DefaultDeploy( "models/v_baton.mdl", "models/p_crowbar.mdl", CROWBAR_DRAW, "crowbar" );
}

void CWeaponBaton::Holster( )
{
	m_pPlayer->SetNextAttack( gpGlobals->time + 0.5 );
	SendWeaponAnim( CROWBAR_HOLSTER );
}


void FindHullIntersection( const Vector &vecSrc, TraceResult &tr, const Vector &mins, const Vector &maxs, edict_t *pEntity )
{
	int			i, j, k;
	float		distance;
	const float		*minmaxs[2] = {mins, maxs};
	TraceResult tmpTrace;
	Vector		vecHullEnd = tr.vecEndPos;
	Vector		vecEnd;

	distance = 1e6f;

	vecHullEnd = vecSrc + ((vecHullEnd - vecSrc)*2);
	UTIL_TraceLine( vecSrc, vecHullEnd, dont_ignore_monsters, pEntity, &tmpTrace );
	if ( tmpTrace.flFraction == 1.0 )
	{
		for ( i = 0; i < 2; i++ )
		{
			for ( j = 0; j < 2; j++ )
			{
				for ( k = 0; k < 2; k++ )
				{
					vecEnd.x = vecHullEnd.x + minmaxs[i][0];
					vecEnd.y = vecHullEnd.y + minmaxs[j][1];
					vecEnd.z = vecHullEnd.z + minmaxs[k][2];

					UTIL_TraceLine( vecSrc, vecEnd, dont_ignore_monsters, pEntity, &tmpTrace );
					if ( tmpTrace.flFraction < 1.0 )
					{
						float thisDistance = (tmpTrace.vecEndPos - vecSrc).Length();
						if ( thisDistance < distance )
						{
							tr = tmpTrace;
							distance = thisDistance;
						}
					}
				}
			}
		}
	}
	else
	{
		tr = tmpTrace;
	}
}


void CWeaponBaton::PrimaryAttack()
{
	Swing();
}

//------------------------------------------------------------------------------
// Purpose: Implement impact function
//------------------------------------------------------------------------------
void CWeaponBaton::Hit( void )
{
	CBaseEntity *pHitEntity = CBaseEntity::Instance(m_traceHit.pHit);

	//Apply damage to a hit target
	if ( pHitEntity != NULL )
	{
		Vector hitDirection;
		m_pPlayer->EyeVectors( &hitDirection, NULL, NULL );
		VectorNormalize( hitDirection );

		ClearMultiDamage( );
		CTakeDamageInfo info( m_pPlayer, m_pPlayer, sk_plr_crowbar.value, DMG_CLUB );
		
		if ( (m_flNextPrimaryAttack + 1 < gpGlobals->time) || g_pGameRules->IsMultiplayer() )
		{
			// first swing does full damage
			info.ScaleDamage( 1.0 );
		}
		else
		{
			// subsequent swings do half
			info.ScaleDamage( 0.5 );
		}	

		pHitEntity->TraceAttack( info, hitDirection, &m_traceHit);
		ApplyMultiDamage();

		switch( (m_iSwing++) % 2 )
		{
		case 0:
			SendWeaponAnim( CROWBAR_ATTACK1HIT ); break;
		case 1:
			SendWeaponAnim( CROWBAR_ATTACK2HIT ); break;
		}

		// play hit sound
		switch( RANDOM_LONG(0, 1) )
		{
		case 0:
			EMIT_SOUND(ENT(m_pPlayer->pev), CHAN_WEAPON, "weapons/bullet_hit1.wav", 1, ATTN_NORM); break;
		case 1:
			EMIT_SOUND(ENT(m_pPlayer->pev), CHAN_WEAPON, "weapons/bullet_hit2.wav", 1, ATTN_NORM); break;
		}

		m_pPlayer->m_iWeaponVolume = BATON_HIT_VOLUME;
	}
}

//------------------------------------------------------------------------------
// Purpose : Starts the swing of the weapon and determines the animation
//------------------------------------------------------------------------------
void CWeaponBaton::Swing( void )
{
	// Try a ray
	Vector swingStart = m_pPlayer->GetGunPosition();
	Vector forward;

	m_pPlayer->EyeVectors( &forward, NULL, NULL );

	UTIL_TraceLine( swingStart, swingStart + forward * BATON_RANGE, dont_ignore_monsters, ENT( m_pPlayer->pev ), &m_traceHit );

	if ( m_traceHit.flFraction == 1.0 )
	{
		UTIL_TraceHull( swingStart, swingStart + forward * BATON_RANGE, dont_ignore_monsters, head_hull, ENT( m_pPlayer->pev ), &m_traceHit );
		if ( m_traceHit.flFraction < 1.0 )
		{
			// Calculate the point of intersection of the line (or hull) and the object we hit
			// This is and approximation of the "best" intersection
			FindHullIntersection( swingStart, m_traceHit, g_TraceCheckMins, g_TraceCheckMaxs, m_pPlayer->edict() );
		}
	}

	if ( m_traceHit.flFraction == 1.0 )
	{
		// miss
		switch( (m_iSwing++) % 2 )
		{
		case 0:
			SendWeaponAnim( CROWBAR_ATTACK1MISS ); break;
		case 1:
			SendWeaponAnim( CROWBAR_ATTACK2MISS ); break;
		}

		m_flNextPrimaryAttack = gpGlobals->time + BATON_REFIRE_MISS;

		// play wiff or swish sound
		EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_WEAPON, "weapons/cbar_miss1.wav", 1, ATTN_NORM, 0, 94 + RANDOM_LONG(0,0xF));
	}
	else
	{
		Hit();

		m_flNextPrimaryAttack = gpGlobals->time + BATON_REFIRE_HIT;
	}

	// player "shoot" animation
	m_pPlayer->SetAnimation( PLAYER_ATTACK1 );

	m_flTimeWeaponIdle = gpGlobals->time + RANDOM_FLOAT( 10, 15 );
}

void CWeaponBaton::ItemPostFrame( void )
{
	if ( (m_pPlayer->pev->button & IN_ATTACK) && ( m_flNextPrimaryAttack <= gpGlobals->time ) )
	{
		PrimaryAttack();
	} 
	else 
	{
		WeaponIdle();
		return;
	}
}

void CWeaponBaton::WeaponIdle( void )
{
	if (m_flTimeWeaponIdle > gpGlobals->time)
		return;

	int iAnim;

	float flRand = RANDOM_FLOAT( 0, 1 );

	if (flRand <= 0.9)
	{
		iAnim = CROWBAR_IDLE1;
		m_flTimeWeaponIdle = gpGlobals->time + (46.0 / 18.0);
	}
	else
	{
		iAnim = CROWBAR_IDLE;
		m_flTimeWeaponIdle = gpGlobals->time + (40.0 / 18.0);
	}

	SendWeaponAnim( iAnim );
}

