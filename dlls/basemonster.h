//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef BASEMONSTER_H
#define BASEMONSTER_H

#ifdef _WIN32
#pragma once
#endif

#include "simtimer.h" 	
#include "basecombatcharacter.h"
#include "ai_default.h"
#include "ai_schedule.h"
#include "ai_condition.h"
#include "ai_component.h"
#include "ai_task.h"
#include "ai_namespaces.h"
#include "monsterstate.h"
#include "monsters.h"
#include "bitvec.h"

class CAI_Schedule;
class CAI_Senses;
class CAI_Enemies;
class CAI_Squad;
class CScriptedSequence;

typedef CBitVec<MAX_CONDITIONS> CAI_ScheduleBits;

//=============================================================================
//
// Constants & enumerations
//
//=============================================================================

// these bits represent the monster's memory
#define MEMORY_CLEAR					0
#define bits_MEMORY_PROVOKED			( 1 << 0 )// right now only used for houndeyes.
#define bits_MEMORY_INCOVER				( 1 << 1 )// monster knows it is in a covered position.
#define bits_MEMORY_SUSPICIOUS			( 1 << 2 )// Ally is suspicious of the player, and will move to provoked more easily
#define bits_MEMORY_PATH_FINISHED		( 1 << 3 )// Finished monster path (just used by big momma for now)
#define bits_MEMORY_ON_PATH				( 1 << 4 )// Moving on a path
#define bits_MEMORY_MOVE_FAILED			( 1 << 5 )// Movement has already failed
#define bits_MEMORY_FLINCHED			( 1 << 6 )// Has already flinched

#define bits_MEMORY_HAD_ENEMY			( 1 << 7 )// Had an enemy
#define bits_MEMORY_HAD_PLAYER			( 1 << 8 )// Had player

#define bits_MEMORY_CUSTOM4				( 1 << 28 )	// Monster-specific memory
#define bits_MEMORY_CUSTOM3				( 1 << 29 )	// Monster-specific memory
#define bits_MEMORY_CUSTOM2				( 1 << 30 )	// Monster-specific memory
#define bits_MEMORY_CUSTOM1				( 1 << 31 )	// Monster-specific memory

//-------------------------------------
// Spawn flags
//-------------------------------------

#define	SF_MONSTER_WAIT_TILL_SEEN		( 1 << 0  )	// spawnflag that makes npcs wait until player can see them before attacking.
#define	SF_MONSTER_GAG					( 1 << 1  ) // no idle noises from this monster
#define SF_MONSTER_HITMONSTERCLIP		( 1 << 2  )
//										( 1 << 3  )
#define SF_MONSTER_PRISONER				( 1 << 4  ) // monster won't attack anyone, no one will attack him.
#define	SF_MONSTER_SQUADLEADER			( 1 << 5  )
//										( 1 << 6  )
#define	SF_MONSTER_WAIT_FOR_SCRIPT		( 1 << 7  ) // spawnflag that makes monsters wait to check for attacking until the script is done or they've been attacked
#define SF_MONSTER_PREDISASTER			( 1 << 8  )	//this is a predisaster scientist or barney. Influences how they speak.
#define SF_MONSTER_FADECORPSE			( 1 << 9  ) // Fade out corpse after death
#define SF_MONSTER_FALL_TO_GROUND		( 1 << 10 )

// specialty spawnflags
#define SF_MONSTER_TURRET_AUTOACTIVATE	( 1 << 5  )
#define SF_MONSTER_TURRET_STARTINACTIVE	( 1 << 6  )
#define SF_MONSTER_WAIT_UNTIL_PROVOKED	( 1 << 6  ) // don't attack the player unless provoked

//-------------------------------------
//
// Debug bits
//
//-------------------------------------

enum DebugBaseMonsterBits_e
{
	bits_debugDisableAI = 0x00000001,		// disable AI
	bits_debugStepAI	= 0x00000002,		// step AI

};

//=============================================================================
//
// Types used by CBaseMonster
//
//=============================================================================

struct AIScheduleState_t
{
	int					 iCurTask;
	TaskStatus_e		 fTaskStatus;
	float				 timeStarted;
	float				 timeCurTaskStarted;
	bool				 bScheduleWasInterrupted;

	DECLARE_SIMPLE_DATADESC();
};

// -----------------------------------------
//	An entity that this monster can't reach
// -----------------------------------------

struct UnreachableEnt_t
{
	EHANDLE	hUnreachableEnt;	// Entity that's unreachable
	float	fExpireTime;		// Time to forget this information
	Vector	vLocationWhenUnreachable;
	
	DECLARE_SIMPLE_DATADESC();
};

//=============================================================================
//
// Utility functions
//
//=============================================================================

Vector VecCheckToss ( CBaseEntity *pEntity, const Vector &vecSpot1, Vector vecSpot2, float flHeightMaxRatio, float flGravityAdj, bool bRandomize );
Vector VecCheckThrow ( CBaseEntity *pEntity, const Vector &vecSpot1, Vector vecSpot2, float flSpeed, float flGravityAdj = 1.0 );

extern DLL_GLOBAL Vector g_vecAttackDir;

bool FBoxVisible ( CBaseEntity *pLooker, CBaseEntity *pTarget );
bool FBoxVisible ( CBaseEntity *pLooker, CBaseEntity *pTarget, Vector &vecTargetOrigin, float flSize = 0.0 );

//=============================================================================
//
// class CAI_Manager
//
// Central location for components of the AI to operate across all AIs without
// iterating over the global list of entities.
//
//=============================================================================

class CAI_Manager
{
public:
	CAI_Manager();
	
	CBaseMonster **	AccessAIs();
	int				NumAIs();
	
	void AddAI( CBaseMonster *pAI );
	void RemoveAI( CBaseMonster *pAI );
	
private:
	enum
	{
		MAX_AIS = 256
	};
	
	typedef CUtlVector<CBaseMonster *> CAIArray;
	
	CAIArray m_AIs;

};

//-------------------------------------

extern CAI_Manager g_AI_Manager;

//
// Base Monster with AI
//
class CBaseMonster : public CBaseCombatCharacter
{
	DECLARE_CLASS( CBaseMonster, CBaseCombatCharacter );

public:
	//-----------------------------------------------------
	//
	// Initialization, cleanup, serialization, identity
	//

	CBaseMonster();
	~CBaseMonster();

	//---------------------------------

	DECLARE_DATADESC();

	virtual int			Save( CSave &save ); 
	virtual int			Restore( CRestore &restore );
	virtual void		OnRestore();
	void				SaveConditions( CSave &save, const CAI_ScheduleBits &conditions );
	void				RestoreConditions( CRestore &restore, CAI_ScheduleBits *pConditions );
	
	//---------------------------------

	void				KeyValue( KeyValueData *pkvd );
	virtual void		Precache( void ); // derived calls at start of Spawn()
	virtual void		MonsterInit ( void ); // derived calls after Spawn()
	void EXPORT			MonsterInitThink ( void );
	virtual void		StartMonster ( void );

	virtual void		UpdateOnRemove( void );

	//---------------------------------
	// Component creation factories
	// 

	// The master call, override if you introduce new component types. Call base first
	virtual bool 			CreateComponents();

	// Components defined by the base AI class
	virtual CAI_Senses *	CreateSenses();

	//---------------------------------

	virtual CBaseMonster *MyMonsterPointer( void ) { return this; }

public:
	//-----------------------------------------------------
	//
	// AI processing - thinking, schedule selection and task running
	//
	//-----------------------------------------------------
	void EXPORT			CallMonsterThink( void ) { this->MonsterThink(); }

	// Thinking, including core thinking, movement, animation
	virtual void		MonsterThink( void );

	// Core thinking (schedules & tasks)
	virtual void		RunAI ( void );// core ai function!

	// Called to gather up all relevant conditons
	virtual void		GatherConditions( void );

	// Called immediately prior to schedule processing
	virtual void		PrescheduleThink( void ) { return; };

	// Called immediately after schedule processing
	virtual void		PostscheduleThink( void ) { return; };

	// Notification that the current schedule, if any, is ending and a new one is being selected
	virtual void		OnScheduleChange( void );

	// Notification that a new schedule is about to run its first task
	virtual void		OnScheduleStart( int scheduleType ) {};

	// This function implements a decision tree for the monster.  It is responsible for choosing the next behavior (schedule)
	// based on the current conditions and state.
	virtual int			SelectSchedule( void );
	virtual int			SelectFailSchedule( int failedSchedule, int failedTask );

	// After the schedule has been selected, it will be processed by this function so child monster classes can 
	// remap base schedules into child-specific behaviors
	virtual int			TranslateSchedule( int scheduleType );

	virtual void		StartTask ( const Task_t *pTask );
	virtual void		RunTask ( const Task_t *pTask );

	void				ClearTransientConditions();

	virtual void		HandleAnimEvent( animevent_t *pEvent );

	virtual bool		ShouldAlwaysThink();
	void				ForceGatherConditions()	{ m_bForceConditionsGather = true; }	// Force a monster out of PVS to call GatherConditions on next think

	enum
	{
		NEXT_SCHEDULE 	= LAST_SHARED_SCHEDULE,
		NEXT_TASK		= LAST_SHARED_TASK,
		NEXT_CONDITION 	= LAST_SHARED_CONDITION,
	};

protected:
	// Used by derived classes to chain a task to a task that might not be the 
	// one they are currently handling:
	void				ChainStartTask( int task, float taskData = 0 ) { Task_t tempTask = { task, taskData }; StartTask( (const Task_t *)&tempTask ); }
	void				ChainRunTask( int task, float taskData = 0 ) { Task_t tempTask = { task, taskData }; RunTask( (const Task_t *)&tempTask ); }

private:
	bool				PreThink( void );
	void				PerformSensing();
	void				MaintainSchedule( void );
	void				RunAnimation( void );

public:
	//-----------------------------------------------------
	//
	// Schedules & tasks
	//
	//-----------------------------------------------------

	void				SetSchedule ( CAI_Schedule *pNewSchedule );
	void				SetSchedule( int localScheduleID ) 			{ SetSchedule( GetScheduleOfType( localScheduleID ) ); }

	void				SetDefaultFailSchedule( int failSchedule )	{ m_failSchedule = failSchedule; }

	void				ClearSchedule( void );

	CAI_Schedule *		GetCurSchedule()							{ return m_pSchedule; }
	bool				IsCurSchedule( int schedId, bool fTranslate = true )	{ return ( m_pSchedule && 
																						   ( ( fTranslate && m_pSchedule == GetScheduleOfType( schedId ) ) ||
																						   	 ( !fTranslate && GetClassScheduleIdSpace()->ScheduleGlobalToLocal( m_pSchedule->GetId() ) == schedId) ) ); }

	virtual CAI_Schedule *GetSchedule(int localScheduleID);
	virtual int			GetLocalScheduleId( int globalScheduleID )	{ return GetClassScheduleIdSpace()->ScheduleGlobalToLocal( globalScheduleID ); }

	float				GetTimeScheduleStarted() const				{ return m_ScheduleState.timeStarted; }

	//---------------------------------

	const Task_t*		GetTask( void );
	int					TaskIsRunning( void );

	virtual void		TaskFail( void );
	void				TaskComplete( bool fIgnoreSetFailedCondition = false );
		
	void				TaskMovementComplete( void );
	inline int			TaskIsComplete( void ) 						{ return (GetTaskStatus() == TASKSTATUS_COMPLETE); }

	virtual const char *TaskName(int taskID);

	float				GetTimeTaskStarted() const					{ return m_ScheduleState.timeCurTaskStarted; }
	virtual int			GetLocalTaskId( int globalTaskId)			{ return GetClassScheduleIdSpace()->TaskGlobalToLocal( globalTaskId ); }

	virtual const char *GetSchedulingErrorName()					{ return "CBaseMonster"; }

protected:
	static bool			LoadSchedules( void );
	virtual bool		LoadedSchedules( void );
	virtual void		BuildScheduleTestBits( void );

private:
	// This function maps the type through TranslateSchedule() and then retrieves the pointer
	// to the actual CAI_Schedule from the database of schedules available to this class.
	CAI_Schedule *		GetScheduleOfType( int scheduleType );

	// This is the main call to select/translate a schedule
	CAI_Schedule *		GetNewSchedule( void );
	CAI_Schedule *		GetFailSchedule( void );

	bool				FHaveSchedule( void );
	bool				FScheduleDone ( void );

	int 				GetScheduleCurTaskIndex() const			{ return m_ScheduleState.iCurTask;		}
	int 				IncScheduleCurTaskIndex()				{ return ++m_ScheduleState.iCurTask; 		}
	void 				ResetScheduleCurTaskIndex()				{ m_ScheduleState.iCurTask = 0;			}
	void				NextScheduledTask ( void );
	bool				FScheduleValid ( void );
	bool				ShouldSelectIdealState( void );

	void				OnStartTask( void ) 					{ SetTaskStatus( TASKSTATUS_RUN_MOVE_AND_TASK ); }
	void 				SetTaskStatus( TaskStatus_e status )	{ m_ScheduleState.fTaskStatus = status; 	}
	TaskStatus_e 		GetTaskStatus() const					{ return m_ScheduleState.fTaskStatus; 	}

	void				DiscardScheduleState();

	//---------------------------------

	CAI_Schedule		*m_pSchedule;
	AIScheduleState_t	m_ScheduleState;
	int					m_failSchedule;				// Schedule type to choose if current schedule fails
	bool				m_bDoPostRestoreRefindPath;

public:
	//-----------------------------------------------------
	//
	// Conditions
	//
	//-----------------------------------------------------

	virtual const char *ConditionName( int conditionID );

	virtual void		RemoveIgnoredConditions ( void );
	void				SetCondition( int iCondition /*, bool state = true*/ );
	bool				HasCondition( int iCondition );
	bool				HasCondition( int iCondition, bool bUseIgnoreConditions );
	bool				HasInterruptCondition( int iCondition );
	bool				HasConditionsToInterruptSchedule( int nLocalScheduleID );

	void				ClearCondition( int iCondition );
	void				ClearConditions( int *pConditions, int nConditions );
	void				SetIgnoreConditions( int *pConditions, int nConditions );
	void				ClearIgnoreConditions( int *pConditions, int nConditions );
	bool				ConditionInterruptsCurSchedule( int iCondition );
	bool				ConditionInterruptsSchedule( int schedule, int iCondition );

	void				SetCustomInterruptCondition( int nCondition );
	bool				IsCustomInterruptConditionSet( int nCondition );
	void				ClearCustomInterruptCondition( int nCondition );
	void				ClearCustomInterruptConditions( void );

	bool				ConditionsGathered() const		{ return m_bConditionsGathered; }
	const CAI_ScheduleBits &AccessConditionBits() const { return m_Conditions; }
	CAI_ScheduleBits &	AccessConditionBits()			{ return m_Conditions; }

	bool				DidChooseEnemy() const			{ return !m_bSkippedChooseEnemy; }

private:
	CAI_ScheduleBits	m_Conditions;
	CAI_ScheduleBits	m_CustomInterruptConditions;	//Bit string assembled by the schedule running, then 
														//modified by leaf classes to suit their needs

	CAI_ScheduleBits	m_ConditionsPreIgnore;
	CAI_ScheduleBits	m_InverseIgnoreConditions;

	bool				m_bForceConditionsGather;
	bool				m_bConditionsGathered;
	bool				m_bSkippedChooseEnemy;

public:
	//-----------------------------------------------------
	//
	// Monster State
	//
	//-----------------------------------------------------

	virtual MONSTERSTATE SelectIdealState ( void );

	void				SetState ( MONSTERSTATE State );
	virtual	void 		OnStateChange( MONSTERSTATE OldState, MONSTERSTATE NewState ) {/*Base class doesn't care*/};

	MONSTERSTATE		GetState( void ) { return m_MonsterState; }

	//---------------------------------

	MONSTERSTATE		m_MonsterState;			// monster's current state
	MONSTERSTATE		m_IdealMonsterState;	// monster should change to this state

public:
	//-----------------------------------------------------
	//
	//	Activities
	// 
	//-----------------------------------------------------

	virtual int			TranslateSequence( Activity &idealActivity ) { return LookupActivity( idealActivity ); }
	Activity			GetActivity( void ) { return m_Activity; }
	virtual void		SetActivity ( Activity NewActivity );
	Activity			GetIdealActivity( void ) { return m_IdealActivity; }
	void				ResetIdealActivity( Activity newIdealActivity );
	void				SetSequenceByName ( const char *szSequence );
	virtual Activity	GetStoppedActivity( void ) { return ACT_IDLE; }
	virtual bool		IsActivityMovementPhased( Activity activity );
	virtual void		OnChangeActivity( Activity eNewActivity ) {}

public: // TODO: Change to private:

	void				MaintainActivity(void);

	Activity			m_Activity;				// Current animation state
	Activity			m_IdealActivity;		// Desired animation state

public:
	//-----------------------------------------------------
	//
	// Senses
	//
	//-----------------------------------------------------

	CAI_Senses *		GetSenses()			{ return m_pSenses; }
	const CAI_Senses *	GetSenses() const	{ return m_pSenses; }

	void				SetDistLook( float flDistLook );

	virtual bool		QueryHearSound( CSound *pSound ) { return true; }
	virtual bool		QuerySeeEntity( CBaseEntity *pEntity ) { return true; }

	virtual void		OnLooked();
	virtual void		OnListened();

	virtual void		OnSeeEntity( CBaseEntity *pEntity ) {}

	virtual int			GetSoundInterests( void );

	bool				FShouldEat( void );				// see if a monster is 'hungry'

	CSound *			GetLoudestSoundOfType( int iType );
	virtual CSound*		GetBestSound ( void );
	virtual CSound*		GetBestScent ( void );
	virtual float		HearingSensitivity( void ) { return 1.0; };
	float				FLSoundVolume ( CSound *pSound );

	virtual void		Eat ( float flFullDuration );	// make the monster 'full' for a while.

protected:
	virtual void		ClearSenseConditions( void );

private:
	CAI_Senses *		m_pSenses;

public:
	//-----------------------------------------------------
	//
	// Enemy and target
	//
	//-----------------------------------------------------

	CBaseEntity*		GetEnemy()							{ return m_hEnemy; }
	CBaseEntity*		GetEnemy() const					{ return m_hEnemy; }
	void				SetEnemy( CBaseEntity *pEnemy, bool bSetCondNewEnemy = true );

	const Vector &		GetEnemyLKP() const;
	float				GetEnemyLastTimeSeen() const;
	void				MarkEnemyAsEluded();
	void				ClearEnemyMemory();
	bool				EnemyHasEludedMe() const;

	virtual CBaseEntity *BestEnemy();		// returns best enemy in memory list
	virtual	bool		IsValidEnemy( CBaseEntity *pEnemy );

	void				ForceChooseNewEnemy()	{ m_EnemiesSerialNumber = -1; }

	bool				ChooseEnemy();
	virtual bool		ShouldChooseNewEnemy();
	virtual void		CheckEnemy( CBaseEntity *pEnemy );
	float				EnemyDistance( CBaseEntity *pEnemy );
	CBaseCombatCharacter *GetEnemyCombatCharacterPointer();
	void SetEnemyOccluder(CBaseEntity *pBlocker);
	CBaseEntity *GetEnemyOccluder(void);

//private:
	
	//---------------------------------

	EHANDLE				m_hEnemy;		 // the entity that the monster is fighting.
	EHANDLE				m_hTargetEnt;	 // the entity that the monster is trying to reach

	CRandStopwatch		m_GiveUpOnDeadEnemyTimer;
	int					m_EnemiesSerialNumber;

	float				m_flAcceptableTimeSeenEnemy;

public:
	//-----------------------------------------------------
	//
	//  Sounds
	// 
	//-----------------------------------------------------
	virtual int			CanPlaySequence( bool fDisregardState, int interruptLevel );

	virtual int			CanPlaySentence( bool fDisregardState ) { return IsAlive(); }
	virtual void		PlaySentence( const char *pszSentence, float duration, float volume, float attenuation );
	virtual void		PlayScriptedSentence( const char *pszSentence, float duration, float volume, float attenuation, bool bConcurrent, CBaseEntity *pListener );
	void				SentenceStop( void );

	virtual bool		FOkToMakeSound( void );
	virtual void		JustMadeSound( void );

	virtual void		DeathSound( void )					{ return; };
	virtual void		AlertSound( void )					{ return; };
	virtual void		IdleSound( void )					{ return; };
	virtual void		PainSound( void )					{ return; };
	virtual void		LostEnemySound( void ) 				{ return; };
	virtual void		FoundEnemySound( void ) 			{ return; };
	
	virtual bool		ShouldPlayIdleSound( void );

protected:
	float				m_flSoundWaitTime;	// Time when I'm allowed to make another sound

public:
	//-----------------------------------------------------
	//
	// Capabilities report
	//
	//-----------------------------------------------------
	virtual int			CapabilitiesGet( void );

	// local capabilities access
	int					CapabilitiesAdd( int capabilities );
	int					CapabilitiesRemove( int capabilities );
	void				CapabilitiesClear( void );

private:
	int					m_afCapability;			// tells us what a monster can/can't do.

public:
	//-----------------------------------------------------
	//
	// Pathfinding, navigation & movement
	//
	//-----------------------------------------------------

	int					IsMoving( void ) { return m_movementGoal != MOVEGOAL_NONE; }

	// Turns a directional vector into a yaw value that points down that vector.
	float				VecToYaw( const Vector &vecDir );
	virtual void		SetYawSpeed ( void ) { return; };// allows different yaw_speeds for each activity

	//  Turning
	bool				FacingIdeal( void );

	// ------------
	// Methods used by motor to query properties/preferences/move-related state
	// ------------
	bool 				ShouldMoveWait();

	//---------------------------------
	
	virtual bool		OverrideMove( float flInterval );				// Override to take total control of movement (return true if done so)

	//---------------------------------
	
	void				RememberUnreachable( CBaseEntity* pEntity );	// Remember that entity is unreachable
	virtual bool		IsUnreachable( CBaseEntity* pEntity );			// Is entity is unreachable?

	//---------------------------------
	
	float				m_flMoveWaitFinished;

private:

	//  Unreachable Entities
	CUtlVector<UnreachableEnt_t> m_UnreachableEnts;								// Array of unreachable entities

public:
	//-----------------------------------------------------
	//
	// Eye position, view offset, head direction, eye direction
	//
	//-----------------------------------------------------

	void				SetEyePosition ( void );


public:
	//-----------------------------------------------------
	//
	// Scripting
	//
	//-----------------------------------------------------

	// Scripted sequence Info
	enum SCRIPTSTATE
	{
		SCRIPT_PLAYING = 0,			// Playing the sequence
		SCRIPT_WAIT,				// Waiting on everyone in the script to be ready
		SCRIPT_CLEANUP,				// Cancelling the script / cleaning up
		SCRIPT_WALK_TO_MARK,		// Walking to the scripted sequence position.
		SCRIPT_RUN_TO_MARK,			// Running to the scripted sequence position.
	};

	bool				ExitScriptedSequence( );
	bool				CineCleanup( );

	// Scripted sequence Info
	SCRIPTSTATE			m_scriptState;		// internal cinematic state
	CScriptedSequence	*m_pCine;

public:
	//-----------------------------------------------------
	//
	// Memory
	//
	//-----------------------------------------------------

	inline void			Remember( int iMemory ) 		{ m_afMemory |= iMemory; }
	inline void			Forget( int iMemory ) 			{ m_afMemory &= ~iMemory; }
	inline bool			HasMemory( int iMemory ) 		{ if ( m_afMemory & iMemory ) return true; return false; }
	inline bool			HasAllMemories( int iMemory ) 	{ if ( (m_afMemory & iMemory) == iMemory ) return true; return false; }

	virtual CAI_Enemies *GetEnemies( void );
	virtual void		RemoveMemory( void );

	virtual bool		UpdateEnemyMemory( CBaseEntity *pEnemy, const Vector &position, bool firstHand = true );

protected:
	CAI_Enemies *		m_pEnemies;	// Holds information about enemies / danger positions / shared between sqaud members
	int					m_afMemory;
	EHANDLE				m_hEnemyOccluder;	// The entity my enemy is hiding behind.

public:
	//-----------------------------------------------------
	//
	// Squads & tactics
	//
	//-----------------------------------------------------

	virtual bool		InitSquad( void );
	int					SquadRecruit( int searchRadius );

	virtual const char*	SquadSlotName(int slotID)		{ return gm_SquadSlotNamespace.IdToSymbol(slotID);					}
	bool				OccupyStrategySlot( int squadSlotID );
	bool				OccupyStrategySlotRange( int slotIDStart, int slotIDEnd );
	bool				HasStrategySlot( int squadSlotID );
	bool				HasStrategySlotRange( int slotIDStart, int slotIDEnd );
	void				VacateStrategySlot( void );
	
	CAI_Squad *			GetSquad()						{ return m_pSquad; 		}
	void				SetSquad( CAI_Squad *pSquad );
	void				SetSquadName( string_t name )	{ pev->netname = name; 	}
	bool				IsInSquad() const				{ return m_pSquad != NULL; }

	//---------------------------------
	//  Cover

	// Checks lateral cover
	virtual bool		TestLateralCover( const Vector &vecCheckStart, const Vector &vecCheckEnd );
	virtual bool		FindLateralCover( const Vector &vecThreat );

	virtual bool		FindCover ( Vector vecThreat, Vector vecViewOffset, float flMinDist, float flMaxDist );
	virtual bool		FValidateCover ( const Vector &vecCoverLocation );
	virtual float		CoverRadius( void ) { return 784; } // Default cover radius

protected:

	CAI_Squad *			m_pSquad;		// The squad that I'm on

	int					m_iMySquadSlot;	// this is the behaviour slot that the monster currently holds in the squad. 

public:
	//-----------------------------------------------------
	//
	// Base schedule & task support; Miscellaneous
	//
	//-----------------------------------------------------

	void EXPORT			MonsterUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );

	virtual bool		ShouldFadeOnDeath( void );

	virtual void		MonsterInitDead( void );	// Call after animation/pose is set up
	void EXPORT			CorpseFallThink( void );

	virtual bool		FCheckAITrigger( void );	// checks and, if necessary, fires the monster's trigger target. 

	// these functions will survey conditions and set appropriate conditions bits for attack types.
	virtual bool		CheckRangeAttack1( float flDot, float flDist );
	virtual bool		CheckRangeAttack2( float flDot, float flDist );
	virtual bool		CheckMeleeAttack1( float flDot, float flDist );
	virtual bool		CheckMeleeAttack2( float flDot, float flDist );

	//---------------------------------
	//  States
	//---------------------------------

	virtual void		ClearAttackConditions( void );
	void				GatherAttackConditions( CBaseEntity *pTarget, float flDist );

	virtual bool		FCanCheckAttacks ( void );
	virtual void		CheckAmmo( void ) { return; };

	virtual bool		FValidateHintType( short sHint );
	int					FindHintNode ( void );
	virtual bool		FCanActiveIdle ( void );

	void				SetTurnActivity ( void );

	// Returns the time when the door will be open
	float				OpenDoorAndWait( entvars_t *pevDoor );

	bool				BBoxFlat( void );

	//---------------------------------

	void				MakeDamageBloodDecal ( int cCount, float flNoise, TraceResult *ptr, const Vector &vecDir );
	void				TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, TraceResult *ptr );

	//---------------------------------
	// combat functions
	//---------------------------------

	Activity			GetFlinchActivity( bool bHeavyDamage );

	virtual bool		CorpseGib( const CTakeDamageInfo &info );
	virtual bool		Event_Gibbed( const CTakeDamageInfo &info );
	virtual void		Event_Killed( const CTakeDamageInfo &info );

	virtual Vector		GetShootEnemyDir( const Vector &shootOrigin );
	virtual Vector		BodyTarget( const Vector &posSrc, bool bNoisy = true );

	virtual bool		FBecomeProne ( void );
	virtual void		BarnacleVictimBitten( CBaseEntity *pBarnacle );
	virtual void		BarnacleVictimReleased( void );

	virtual bool		NoFriendlyFire( void );

	//---------------------------------
	//  Damage
	//---------------------------------
	virtual int			TakeDamage_Alive( const CTakeDamageInfo &info );
	virtual int			TakeDamage_Dead( const CTakeDamageInfo &info );
	virtual bool		IsLightDamage( float flDamage, int bitsDamageType );
	virtual bool		IsHeavyDamage( float flDamage, int bitsDamageType );

	void				DoRadiusDamage( const CTakeDamageInfo &info, int iClassIgnore );
	void				DoRadiusDamage( const CTakeDamageInfo &info, const Vector &vecSrc, int iClassIgnore );

	//---------------------------------

	CBaseEntity			*DropItem ( const char *pszItemName, const Vector &vecPos, const Vector &vecAng );// drop an item.

	//---------------------------------
	
	virtual void		NotifyDeadFriend( CBaseEntity *pFriend ) { return; };

	//---------------------------------

	float				m_flWaitFinished;			// if we're told to wait, this is the time that the wait will be over.

	Vector				m_vecLastPosition;			// monster sometimes wants to return to where it started after an operation.
	Vector				m_vSavePosition;			// position stored by code that called this schedules
	int					m_iHintNode;				// this is the hint node that the monster is moving towards or performing active idle on.

	int					m_cAmmoLoaded;				// how much ammo is in the weapon (used to trigger reload anim sequences)
	float				m_flDistTooFar;				// if enemy farther away than this, bits_COND_ENEMY_TOOFAR set in CheckEnemy
	CBaseEntity			*m_pGoalEnt;				// path corner we are heading towards
	
	WayPoint_t			m_Route[ ROUTE_SIZE ];		// Positions of movement
	int					m_movementGoal;				// Goal that defines route
	int					m_iRouteIndex;				// index into m_Route[]
	float				m_moveWaitTime;				// How long I should wait for something to move

	Vector				m_vecMoveGoal;				// kept around for node graph moves, so we know our ultimate goal
	Activity			m_movementActivity;			// When moving, set this activity

	float				m_flHungryTime;				// set this is a future time to stop the monster from eating for a while. 

	int					m_iTriggerCondition;		// for scripted AI, this is the condition that will cause the activation of the monster's TriggerTarget
	string_t			m_iszTriggerTarget;			// name of target that should be fired. 

// overrideable Monster member functions
	
	
	virtual bool	IsAlive( void ) { return (pev->deadflag == DEAD_NO); }
	
// Basic Monster AI functions
	virtual void ChangeYaw ( int speed );
	
	float FlYawDiff ( void ); 

		virtual int CheckLocalMove ( const Vector &vecStart, const Vector &vecEnd, CBaseEntity *pTarget, float *pflDist );// check validity of a straight move through space
		virtual void Move( float flInterval = 0.1 );
		virtual void MoveExecute( CBaseEntity *pTargetEnt, const Vector &vecDir, float flInterval );
		virtual bool ShouldAdvanceRoute( float flWaypointDist );

		
		virtual void Stop( void ) { m_IdealActivity = GetStoppedActivity(); }

		

		bool FGetNodeRoute ( Vector vecDest );
		
		
		inline int MovementIsComplete( void ) { return (m_movementGoal == MOVEGOAL_NONE); }

		bool FRefreshRoute( void );
		bool FRouteClear ( void );
		void RouteSimplify( CBaseEntity *pTargetEnt );
		void AdvanceRoute ( float distance );
		virtual bool FTriangulate ( const Vector &vecStart , const Vector &vecEnd, float flDist, CBaseEntity *pTargetEnt, Vector *pApex );
		void MakeIdealYaw( Vector vecTarget );
		
		bool BuildRoute ( const Vector &vecGoal, int iMoveFlag, CBaseEntity *pTarget );
		virtual bool BuildNearestRoute ( Vector vecThreat, Vector vecViewOffset, float flMinDist, float flMaxDist );
		bool BuildRandomRoute( void );
		int RouteClassify( int iMoveFlag );
		void InsertWaypoint ( Vector vecLocation, int afMoveFlags );
		
		bool MoveToNode( Activity movementAct, float waitTime, const Vector &goal );
		bool MoveToTarget( Activity movementAct, float waitTime );
		bool MoveToLocation( Activity movementAct, float waitTime, const Vector &goal );
		bool MoveToEnemy( Activity movementAct, float waitTime );
		bool MoveToWanderGoal( float minRadius, float maxRadius );

		

	void RouteClear( void );
	void RouteNew( void );

	inline int UsableMonsterObjectCaps( int baseCaps )
	{
		if ( IsAlive() )
			baseCaps |= FCAP_IMPULSE_USE;
		return baseCaps;
	}

public:
	//-----------------------------------------------------
	//
	// Core mapped data structures 
	//
	// String Registries for default AI Shared by all CBaseMonsters
	//	These are used only during initialization and in debug
	//-----------------------------------------------------

	static void InitSchedulingTables();

	static CAI_GlobalScheduleNamespace *GetSchedulingSymbols()		{ return &gm_SchedulingSymbols; }
	static CAI_ClassScheduleIdSpace &AccessClassScheduleIdSpaceDirect() { return gm_ClassScheduleIdSpace; }
	virtual CAI_ClassScheduleIdSpace *	GetClassScheduleIdSpace()	{ return &gm_ClassScheduleIdSpace; }

	static int			GetScheduleID	(const char* schedName);

protected:
	static CAI_GlobalNamespace gm_SquadSlotNamespace;
	static CAI_LocalIdSpace    gm_SquadSlotIdSpace;
	
private:
	friend class CAI_SchedulesManager;
	
	static bool			LoadDefaultSchedules(void);

	static void			InitDefaultScheduleSR(void);
	static void			InitDefaultTaskSR(void);
	static void			InitDefaultConditionSR(void);
	static void			InitDefaultSquadSlotSR(void);
	
	static int			GetConditionID	(const char *condName);
	static int			GetTaskID		(const char *taskName);
	static int			GetSquadSlotID	(const char* slotName);

	static CAI_GlobalScheduleNamespace	gm_SchedulingSymbols;
	static CAI_ClassScheduleIdSpace		gm_ClassScheduleIdSpace;
	
public:
	//----------------------------------------------------
	// Debugging tools
	//

	// -----------------------------
	//  Debuging Fields and Methods
	// -----------------------------
	int					m_nDebugCurIndex;			// Index used for stepping through AI
	virtual void		ReportAIState( void );

	static void			ClearAllSchedules( void );

	static int			m_nDebugBits;

	static CBaseMonster	*m_pDebugMonster;
	static int			m_nDebugPauseIndex;		// Current step
	static inline void	SetDebugMonster( CBaseMonster *pMonster )  { m_pDebugMonster = pMonster; }
	static inline bool	IsDebugMonster( CBaseMonster *pMonster) { return(pMonster == m_pDebugMonster ); }

};

// ============================================================================
//	Macros for introducing new schedules in sub-classes
//
// Strings registries and schedules use unique ID's for each item, but 
// sub-class enumerations are non-unique, so we translate between the 
// enumerations and unique ID's
// ============================================================================

#define AI_BEGIN_CUSTOM_SCHEDULE_PROVIDER( derivedClass ) \
	IMPLEMENT_CUSTOM_SCHEDULE_PROVIDER(derivedClass ) \
	void derivedClass::InitCustomSchedules( void ) \
	{ \
		typedef derivedClass CNpc; \
		const char *pszClassName = #derivedClass; \
		\
		CUtlVector<char *> schedulesToLoad; \
		CUtlVector<AIScheduleLoadFunc_t> reqiredOthers; \
		CAI_NamespaceInfos scheduleIds; \
		CAI_NamespaceInfos taskIds; \
		CAI_NamespaceInfos conditionIds;
		

//-----------------

#define AI_BEGIN_CUSTOM_NPC( className, derivedClass ) \
	IMPLEMENT_CUSTOM_AI(className, derivedClass ) \
	void derivedClass::InitCustomSchedules( void ) \
	{ \
		typedef derivedClass CNpc; \
		const char *pszClassName = #derivedClass; \
		\
		CUtlVector<char *> schedulesToLoad; \
		CUtlVector<AIScheduleLoadFunc_t> reqiredOthers; \
		CAI_NamespaceInfos scheduleIds; \
		CAI_NamespaceInfos taskIds; \
		CAI_NamespaceInfos conditionIds; \
		CAI_NamespaceInfos squadSlotIds;
		
//-----------------

#define EXTERN_SCHEDULE( id ) \
	scheduleIds.PushBack( #id, id ); \
	extern const char * g_psz##id; \
	schedulesToLoad.AddToTail( (char *)g_psz##id );

//-----------------

#define DEFINE_SCHEDULE( id, text ) \
	scheduleIds.PushBack( #id, id ); \
	char * g_psz##id = \
		"\n	Schedule" \
		"\n		" #id \
		text \
		"\n"; \
	schedulesToLoad.AddToTail( (char *)g_psz##id );
	
//-----------------

#define DECLARE_CONDITION( id ) \
	conditionIds.PushBack( #id, id );

//-----------------

#define DECLARE_TASK( id ) \
	taskIds.PushBack( #id, id );

//-----------------

#define DECLARE_SQUADSLOT( id ) \
	squadSlotIds.PushBack( #id, id );

//-----------------

#define DECLARE_USES_SCHEDULE_PROVIDER( classname )	reqiredOthers.AddToTail( ScheduleLoadHelper(classname) );

//-----------------

// IDs are stored and then added in order due to constraints in the namespace implementation
#define AI_END_CUSTOM_SCHEDULE_PROVIDER() \
		\
		int i; \
		\
		CNpc::AccessClassScheduleIdSpaceDirect().Init( BaseClass::GetSchedulingSymbols(), &BaseClass::AccessClassScheduleIdSpaceDirect() ); \
		\
		scheduleIds.Sort(); \
		taskIds.Sort(); \
		conditionIds.Sort(); \
		\
		for ( i = 0; i < scheduleIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_SCHEDULE_NAMED( CNpc, scheduleIds[i].pszName, scheduleIds[i].localId );  \
		} \
		\
		for ( i = 0; i < taskIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_TASK_NAMED( CNpc, taskIds[i].pszName, taskIds[i].localId );  \
		} \
		\
		for ( i = 0; i < conditionIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_CONDITION_NAMED( CNpc, conditionIds[i].pszName, conditionIds[i].localId );  \
		} \
		\
		for ( i = 0; i < reqiredOthers.Count(); i++ ) \
		{ \
			(*reqiredOthers[i])();  \
		} \
		\
		for ( i = 0; i < schedulesToLoad.Count(); i++ ) \
		{ \
			if ( CNpc::gm_SchedLoadStatus.fValid ) \
			{ \
				CNpc::gm_SchedLoadStatus.fValid = g_AI_SchedulesManager.LoadSchedulesFromBuffer( pszClassName, schedulesToLoad[i], &AccessClassScheduleIdSpaceDirect() ); \
			} \
			else \
				break; \
		} \
	}

//-------------------------------------

// IDs are stored and then added in order due to constraints in the namespace implementation
#define AI_END_CUSTOM_NPC() \
		\
		int i; \
		\
		CNpc::AccessClassScheduleIdSpaceDirect().Init( BaseClass::GetSchedulingSymbols(), &BaseClass::AccessClassScheduleIdSpaceDirect() ); \
		CNpc::gm_SquadSlotIdSpace.Init( &BaseClass::gm_SquadSlotNamespace, &BaseClass::gm_SquadSlotIdSpace); \
		\
		scheduleIds.Sort(); \
		taskIds.Sort(); \
		conditionIds.Sort(); \
		squadSlotIds.Sort(); \
		\
		for ( i = 0; i < scheduleIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_SCHEDULE_NAMED( CNpc, scheduleIds[i].pszName, scheduleIds[i].localId );  \
		} \
		\
		for ( i = 0; i < taskIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_TASK_NAMED( CNpc, taskIds[i].pszName, taskIds[i].localId );  \
		} \
		\
		for ( i = 0; i < conditionIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_CONDITION_NAMED( CNpc, conditionIds[i].pszName, conditionIds[i].localId );  \
		} \
		\
		for ( i = 0; i < squadSlotIds.Count(); i++ ) \
		{ \
			ADD_CUSTOM_SQUADSLOT_NAMED( CNpc, squadSlotIds[i].pszName, squadSlotIds[i].localId );  \
		} \
		\
		for ( i = 0; i < reqiredOthers.Count(); i++ ) \
		{ \
			(*reqiredOthers[i])();  \
		} \
		\
		for ( i = 0; i < schedulesToLoad.Count(); i++ ) \
		{ \
			if ( CNpc::gm_SchedLoadStatus.fValid ) \
			{ \
				CNpc::gm_SchedLoadStatus.fValid = g_AI_SchedulesManager.LoadSchedulesFromBuffer( pszClassName, schedulesToLoad[i], &AccessClassScheduleIdSpaceDirect() ); \
			} \
			else \
				break; \
		} \
	}

//-------------------------------------

struct AI_NamespaceAddInfo_t
{
	AI_NamespaceAddInfo_t( const char *pszName, int localId )
	 :	pszName( pszName ),
		localId( localId )
	{
	}
	
	const char *pszName;
	int			localId;
};

class CAI_NamespaceInfos : public CUtlVector<AI_NamespaceAddInfo_t>
{
public:
	void PushBack(  const char *pszName, int localId )
	{
		AddToTail( AI_NamespaceAddInfo_t( pszName, localId ) );
	}

	void Sort()
	{
		if ( Count() )
			qsort( Base(), Count(), sizeof(AI_NamespaceAddInfo_t), Compare );
	}
	
private:
	static int Compare(const void *pLeft, const void *pRight )
	{
		return ((AI_NamespaceAddInfo_t *)pLeft)->localId - ((AI_NamespaceAddInfo_t *)pRight)->localId;
	}
	
};

//-------------------------------------

// Declares the static variables that hold the string registry offset for the new subclass
// as well as the initialization in schedule load functions

struct AI_SchedLoadStatus_t
{
	bool fValid;
	int  signature;
};

// Load schedules pulled out to support stepping through with debugger
inline bool AI_DoLoadSchedules( bool (*pfnBaseLoad)(), void (*pfnInitCustomSchedules)(),
								AI_SchedLoadStatus_t *pLoadStatus )
{
	(*pfnBaseLoad)();
	
	if (pLoadStatus->signature != g_AI_SchedulesManager.GetScheduleLoadSignature())
	{
		(*pfnInitCustomSchedules)();
		pLoadStatus->fValid	   = true;
		pLoadStatus->signature = g_AI_SchedulesManager.GetScheduleLoadSignature();
	}
	return pLoadStatus->fValid;
}

//-------------------------------------

typedef bool (*AIScheduleLoadFunc_t)();

// @Note (toml 02-16-03): The following class exists to allow us to establish an anonymous friendship
// in DEFINE_CUSTOM_SCHEDULE_PROVIDER. The particulars of this implementation is almost entirely
// defined by bugs in MSVC 6.0
class ScheduleLoadHelperImpl
{
public:
	template <typename T> 
	static AIScheduleLoadFunc_t AccessScheduleLoadFunc(T *)
	{
		return (&T::LoadSchedules);
	}
};

#define ScheduleLoadHelper( type ) (ScheduleLoadHelperImpl::AccessScheduleLoadFunc((type *)0))

//-------------------------------------

#define DEFINE_CUSTOM_SCHEDULE_PROVIDER\
	static AI_SchedLoadStatus_t 		gm_SchedLoadStatus; \
	static CAI_ClassScheduleIdSpace 	gm_ClassScheduleIdSpace; \
	static const char *					gm_pszErrorClassName;\
	\
	static CAI_ClassScheduleIdSpace &	AccessClassScheduleIdSpaceDirect() 	{ return gm_ClassScheduleIdSpace; } \
	virtual CAI_ClassScheduleIdSpace *	GetClassScheduleIdSpace()			{ return &gm_ClassScheduleIdSpace; } \
	virtual const char *				GetSchedulingErrorName()			{ return gm_pszErrorClassName; } \
	\
	static void							InitCustomSchedules(void);\
	\
	static bool							LoadSchedules(void);\
	virtual bool						LoadedSchedules(void); \
	\
	friend class ScheduleLoadHelperImpl;	\
	\
	class CScheduleLoader \
	{ \
	public: \
		CScheduleLoader(); \
	} m_ScheduleLoader; \
	\
	friend class CScheduleLoader;

//-------------------------------------

#define DEFINE_CUSTOM_AI\
	DEFINE_CUSTOM_SCHEDULE_PROVIDER \
	\
	static CAI_LocalIdSpace gm_SquadSlotIdSpace; \
	\
	const char*				SquadSlotName	(int squadSlotID);

//-------------------------------------

#define IMPLEMENT_CUSTOM_SCHEDULE_PROVIDER(derivedClass)\
	AI_SchedLoadStatus_t		derivedClass::gm_SchedLoadStatus = { true, -1 }; \
	CAI_ClassScheduleIdSpace 	derivedClass::gm_ClassScheduleIdSpace; \
	const char *				derivedClass::gm_pszErrorClassName = #derivedClass; \
	\
	derivedClass::CScheduleLoader::CScheduleLoader()\
	{ \
		derivedClass::LoadSchedules(); \
	} \
	\
	/* --------------------------------------------- */ \
	/* Load schedules for this type of monster       */ \
	/* --------------------------------------------- */ \
	bool derivedClass::LoadSchedules(void)\
	{\
		return AI_DoLoadSchedules( derivedClass::BaseClass::LoadSchedules, \
								   derivedClass::InitCustomSchedules, \
								   &derivedClass::gm_SchedLoadStatus ); \
	}\
	\
	bool derivedClass::LoadedSchedules(void) \
	{ \
		return derivedClass::gm_SchedLoadStatus.fValid;\
	} 


//-------------------------------------

// Initialize offsets and implement methods for loading and getting squad info for the subclass
#define IMPLEMENT_CUSTOM_AI(className, derivedClass)\
	IMPLEMENT_CUSTOM_SCHEDULE_PROVIDER(derivedClass)\
	\
	CAI_LocalIdSpace 	derivedClass::gm_SquadSlotIdSpace; \
	\
	/* -------------------------------------------------- */ \
	/* Given squadSlot enumeration return squadSlot name */ \
	/* -------------------------------------------------- */ \
	const char* derivedClass::SquadSlotName(int slotEN)\
	{\
		return gm_SquadSlotNamespace.IdToSymbol( derivedClass::gm_SquadSlotIdSpace.LocalToGlobal(slotEN) );\
	}
	
//-------------------------------------

#define ADD_CUSTOM_SCHEDULE_NAMED(derivedClass,schedName,schedEN)\
	if ( !derivedClass::AccessClassScheduleIdSpaceDirect().AddSchedule( schedName, schedEN, derivedClass::gm_pszErrorClassName ) ) return;

#define ADD_CUSTOM_SCHEDULE(derivedClass,schedEN) ADD_CUSTOM_SCHEDULE_NAMED(derivedClass,#schedEN,schedEN)

#define ADD_CUSTOM_TASK_NAMED(derivedClass,taskName,taskEN)\
	if ( !derivedClass::AccessClassScheduleIdSpaceDirect().AddTask( taskName, taskEN, derivedClass::gm_pszErrorClassName ) ) return;

#define ADD_CUSTOM_TASK(derivedClass,taskEN) ADD_CUSTOM_TASK_NAMED(derivedClass,#taskEN,taskEN)

#define ADD_CUSTOM_CONDITION_NAMED(derivedClass,condName,condEN)\
	if ( !derivedClass::AccessClassScheduleIdSpaceDirect().AddCondition( condName, condEN, derivedClass::gm_pszErrorClassName ) ) return;

#define ADD_CUSTOM_CONDITION(derivedClass,condEN) ADD_CUSTOM_CONDITION_NAMED(derivedClass,#condEN,condEN)

//-------------------------------------

#define INIT_CUSTOM_AI(derivedClass)\
	derivedClass::AccessClassScheduleIdSpaceDirect().Init( BaseClass::GetSchedulingSymbols(), &BaseClass::AccessClassScheduleIdSpaceDirect() ); \
	derivedClass::gm_SquadSlotIdSpace.Init( &CBaseMonster::gm_SquadSlotNamespace, &BaseClass::gm_SquadSlotIdSpace);

#define ADD_CUSTOM_SQUADSLOT_NAMED(derivedClass,squadSlotName,squadSlotEN)\
	if ( !derivedClass::gm_SquadSlotIdSpace.AddSymbol( squadSlotName, squadSlotEN, "squadslot", derivedClass::gm_pszErrorClassName ) ) return;

#define ADD_CUSTOM_SQUADSLOT(derivedClass,squadSlotEN) ADD_CUSTOM_SQUADSLOT_NAMED(derivedClass,#squadSlotEN,squadSlotEN)

//=============================================================================
// class CAI_Component
//=============================================================================

inline const Vector &CAI_Component::GetOrigin() const
{
	return GetOuter()->pev->origin;
}

//-----------------------------------------------------------------------------

inline void CAI_Component::SetOrigin(const Vector &origin)
{
	GetOuter()->pev->origin = origin;
}

//-----------------------------------------------------------------------------

inline float CAI_Component::GetGravity() const
{
	return GetOuter()->pev->gravity;
}

//-----------------------------------------------------------------------------

inline void CAI_Component::SetGravity( float flGravity )
{
	GetOuter()->pev->gravity = flGravity;
}

//-----------------------------------------------------------------------------

inline CBaseEntity *CAI_Component::GetEnemy()
{
	return GetOuter()->GetEnemy();
}

//-----------------------------------------------------------------------------

inline const Vector &CAI_Component::GetEnemyLKP() const
{
	return GetOuter()->GetEnemyLKP();
}

//-----------------------------------------------------------------------------

inline CBaseEntity *CAI_Component::GetTarget()
{
	return GetOuter()->m_hTargetEnt;
}
//-----------------------------------------------------------------------------

inline void CAI_Component::SetTarget( CBaseEntity *pTarget )
{
	GetOuter()->m_hTargetEnt = pTarget;
}

//-----------------------------------------------------------------------------

inline const Task_t *CAI_Component::GetCurTask()
{
	return GetOuter()->GetTask();
}

//-----------------------------------------------------------------------------

inline void CAI_Component::TaskFail( void )
{
	GetOuter()->TaskFail();
}
//-----------------------------------------------------------------------------

inline void CAI_Component::TaskComplete( bool fIgnoreSetFailedCondition )
{
	GetOuter()->TaskComplete( fIgnoreSetFailedCondition );
}
//-----------------------------------------------------------------------------

inline int CAI_Component::TaskIsRunning()
{
	return GetOuter()->TaskIsRunning();
}
//-----------------------------------------------------------------------------

inline int CAI_Component::TaskIsComplete()
{
	return GetOuter()->TaskIsComplete();
}

//-----------------------------------------------------------------------------

inline Activity CAI_Component::GetActivity()
{
	return GetOuter()->GetActivity();
}
//-----------------------------------------------------------------------------

inline void CAI_Component::SetActivity( Activity NewActivity )
{
	GetOuter()->SetActivity( NewActivity );
}
//-----------------------------------------------------------------------------

inline int CAI_Component::GetSequence()
{
	return GetOuter()->pev->sequence;
}

//-----------------------------------------------------------------------------

inline int CAI_Component::GetEntFlags() const
{
	return GetOuter()->pev->flags;
}

//-----------------------------------------------------------------------------

inline void CAI_Component::AddEntFlag( int flags )
{
	GetOuter()->pev->flags |= flags;
}

//-----------------------------------------------------------------------------

inline void CAI_Component::RemoveEntFlag( int flagsToRemove )
{
	GetOuter()->pev->flags &= ~flagsToRemove;
}

//-----------------------------------------------------------------------------

inline void CAI_Component::ToggleEntFlag( int flagToToggle )
{
	GetOuter()->pev->flags ^= flagToToggle;
}

//-----------------------------------------------------------------------------

inline CBaseEntity* CAI_Component::GetGoalEnt()
{
	return GetOuter()->m_pGoalEnt;
}
//-----------------------------------------------------------------------------

inline void CAI_Component::SetGoalEnt( CBaseEntity *pGoalEnt )
{
	GetOuter()->m_pGoalEnt = pGoalEnt;
}

//-----------------------------------------------------------------------------

inline void CAI_Component::Remember( int iMemory )
{
	GetOuter()->Remember( iMemory );
}
//-----------------------------------------------------------------------------

inline void CAI_Component::Forget( int iMemory )
{
	GetOuter()->Forget( iMemory );
}
//-----------------------------------------------------------------------------

inline bool CAI_Component::HasMemory( int iMemory )
{
	return GetOuter()->HasMemory( iMemory );
}

//-----------------------------------------------------------------------------

inline const char *CAI_Component::GetEntClassname()
{
	return STRING( GetOuter()->pev->classname );
}

//-----------------------------------------------------------------------------

inline int CAI_Component::CapabilitiesGet()
{
	return GetOuter()->CapabilitiesGet();
}

//-----------------------------------------------------------------------------

inline void CAI_Component::SetAngles( const Vector &angles )
{
	GetOuter()->pev->angles = angles;
}

//-----------------------------------------------------------------------------

inline const Vector &CAI_Component::GetAngles( void ) const
{
	return GetOuter()->pev->angles;
}

// ============================================================================

#endif // BASEMONSTER_H
