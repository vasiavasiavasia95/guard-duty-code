/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// CSquadMonster - all the extra data for monsters that
// form squads.
//=========================================================
#ifndef SQUADMONSTER_H
#define SQUADMONSTER_H
#ifdef _WIN32
#pragma once
#endif

#if 0

#include "basemonster.h"


//=========================================================
// CSquadMonster - for any monster that forms squads.
//=========================================================
class CSquadMonster : public CBaseMonster 
{
public:
	DECLARE_CLASS( CSquadMonster, CBaseMonster );

	// squad leader info
	EHANDLE	m_hSquadLeader;		// who is my leader
	EHANDLE	m_hSquadMember[MAX_SQUAD_MEMBERS-1];	// valid only for leader
	int		m_afSquadSlots;
	float	m_flLastEnemySightTime; // last time anyone in the squad saw the enemy
	bool	m_fEnemyEluded;

	// squad member info
	int		m_iMySlot;// this is the behaviour slot that the monster currently holds in the squad. 

	bool UpdateEnemyMemory( CBaseEntity *pEnemy, const Vector &position, bool firstHand = true );
	void StartMonster ( void );
	void VacateSlot( void );
	void OnScheduleChange( void );
	void Event_Killed( const CTakeDamageInfo &info );
	bool OccupySlot( int iDesiredSlot );
	bool NoFriendlyFire( void );

	// squad functions still left in base class
	CSquadMonster *MySquadLeader( ) 
	{ 
		CSquadMonster *pSquadLeader = (CSquadMonster *)((CBaseEntity *)m_hSquadLeader); 
		if (pSquadLeader != NULL)
			return pSquadLeader;
		return this;
	}
	CSquadMonster *MySquadMember( int i ) 
	{ 
		if (i >= MAX_SQUAD_MEMBERS-1)
			return this;
		else
			return (CSquadMonster *)((CBaseEntity *)m_hSquadMember[i]); 
	}
	int	InSquad ( void ) { return m_hSquadLeader != NULL; }
	int IsLeader ( void ) { return m_hSquadLeader == this; }
	int SquadJoin ( int searchRadius );
	int SquadRecruit ( int searchRadius, int maxMembers );
	int	SquadCount( void );
	void SquadRemove( CSquadMonster *pRemove );
	void SquadUnlink( void );
	bool SquadAdd( CSquadMonster *pAdd );
	void SquadDisband( void );
	void SquadAddConditions ( int iConditions );
	void SquadMakeEnemy ( CBaseEntity *pEnemy );
	void SquadPasteEnemyInfo ( void );
	void SquadCopyEnemyInfo ( void );
	bool SquadEnemySplit ( void );
	bool SquadMemberInRange( const Vector &vecLocation, float flDist );

	virtual CSquadMonster *MySquadMonsterPointer( void ) { return this; }

	DECLARE_DATADESC();

	bool FValidateCover ( const Vector &vecCoverLocation );

	MONSTERSTATE SelectIdealState ( void );
	int	TranslateSchedule( int scheduleType );
};


#endif		//SQUADMONSTER_H

#endif