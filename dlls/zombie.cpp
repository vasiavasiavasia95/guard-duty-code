//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: A slow-moving, once-human headcrab victim with only melee attacks.
//
// UNDONE: Make head take 100% damage, body take 30% damage.
// UNDONE: Don't flinch every time you get hit.
//
//=============================================================================//

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"

extern cvar_t sk_zombie_health;
extern cvar_t sk_zombie_dmg_one_slash;
extern cvar_t sk_zombie_dmg_both_slash;

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define	ZOMBIE_AE_ATTACK_RIGHT		0x01
#define	ZOMBIE_AE_ATTACK_LEFT		0x02
#define	ZOMBIE_AE_ATTACK_BOTH		0x03

#define ZOMBIE_FLINCH_DELAY			2		// at most one flinch every n secs

#define ZOMBIE_MELEE_REACH			55

#define ZOMBIE_BULLET_DAMAGE_SCALE	0.3f

class CZombie : public CBaseMonster
{
public:
	DECLARE_CLASS( CZombie, CBaseMonster );

	void Spawn( void );
	void Precache( void );
	void SetYawSpeed( void );
	Class_T Classify( void );
	void HandleAnimEvent( animevent_t *pEvent );
	
	// No range attacks
	bool CheckRangeAttack1( float flDot, float flDist ) { return false; }
	bool CheckRangeAttack2( float flDot, float flDist ) { return false; }

	int TakeDamage_Alive( const CTakeDamageInfo &info );

	virtual void BuildScheduleTestBits( void );
	virtual void OnScheduleChange( void );

	virtual CBaseEntity *ClawAttack( float flDist, int iDamage, Vector &vecViewPunch, Vector &vecVelocityPunch );

	void PainSound( void );
	void AlertSound( void );
	void IdleSound( void );
	void AttackSound( void );
	void AttackHitSound( void );
	void AttackMissSound( void );

private:

	float	m_flNextFlinch;

	static const char *pAttackSounds[];
	static const char *pIdleSounds[];
	static const char *pAlertSounds[];
	static const char *pPainSounds[];
	static const char *pAttackHitSounds[];
	static const char *pAttackMissSounds[];

};

LINK_ENTITY_TO_CLASS( monster_zombie, CZombie );

//---------------------------------------------------------
//---------------------------------------------------------
const char *CZombie::pAttackHitSounds[] = 
{
	"zombie/claw_strike1.wav",
	"zombie/claw_strike2.wav",
	"zombie/claw_strike3.wav",
};

const char *CZombie::pAttackMissSounds[] = 
{
	"zombie/claw_miss1.wav",
	"zombie/claw_miss2.wav",
};

const char *CZombie::pAttackSounds[] = 
{
	"zombie/zo_attack1.wav",
	"zombie/zo_attack2.wav",
};

const char *CZombie::pIdleSounds[] = 
{
	"zombie/zo_idle1.wav",
	"zombie/zo_idle2.wav",
	"zombie/zo_idle3.wav",
	"zombie/zo_idle4.wav",
};

const char *CZombie::pAlertSounds[] = 
{
	"zombie/zo_alert10.wav",
	"zombie/zo_alert20.wav",
	"zombie/zo_alert30.wav",
};

const char *CZombie::pPainSounds[] = 
{
	"zombie/zo_pain1.wav",
	"zombie/zo_pain2.wav",
};

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CZombie::Classify( void )
{
	return CLASS_ALIEN_MONSTER;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CZombie :: SetYawSpeed ( void )
{
	int ys;

	ys = 120;

#if 0
	switch ( m_Activity )
	{
	}
#endif

	pev->yaw_speed = ys;
}

int CZombie::TakeDamage_Alive( const CTakeDamageInfo &inputInfo)
{
	CTakeDamageInfo info = inputInfo;

	// Take 30% damage from bullets
	if ( info.GetDamageType() & DMG_BULLET && LastHitGroup() != HITGROUP_HEAD )
	{
		Vector vecDir = pev->origin - info.GetInflictor()->Center();
		vecDir = vecDir.Normalize();
		float flForce = DamageForce( pev->size, info.GetDamage() );
		pev->velocity = pev->velocity + vecDir * flForce;
		info.ScaleDamage( ZOMBIE_BULLET_DAMAGE_SCALE );
	}

	return BaseClass::TakeDamage_Alive( info );
}

//-----------------------------------------------------------------------------
// Purpose: Play a random attack hit sound
//-----------------------------------------------------------------------------
void CZombie::AttackHitSound( void )
{
	int pitch = 100 + RANDOM_LONG( -5, 5 );

	EMIT_SOUND_DYN ( ENT(pev), CHAN_WEAPON, pAttackHitSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackHitSounds)-1) ], VOL_NORM, ATTN_NORM, 0, pitch );
}

//-----------------------------------------------------------------------------
// Purpose: Play a random attack miss sound
//-----------------------------------------------------------------------------
void CZombie::AttackMissSound( void )
{
	int pitch = 100 + RANDOM_LONG( -5, 5 );

	EMIT_SOUND_DYN ( ENT(pev), CHAN_WEAPON, pAttackMissSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackMissSounds)-1) ], VOL_NORM, ATTN_NORM, 0, pitch );
}

void CZombie :: PainSound( void )
{
	int pitch = 95 + RANDOM_LONG(0,9);

	if (RANDOM_LONG(0,5) < 2)
		EMIT_SOUND_DYN ( ENT(pev), CHAN_VOICE, pPainSounds[ RANDOM_LONG(0,ARRAYSIZE(pPainSounds)-1) ], VOL_NORM, ATTN_NORM, 0, pitch );
}

void CZombie :: AlertSound( void )
{
	int pitch = 95 + RANDOM_LONG(0,9);

	EMIT_SOUND_DYN ( ENT(pev), CHAN_VOICE, pAlertSounds[ RANDOM_LONG(0,ARRAYSIZE(pAlertSounds)-1) ], VOL_NORM, ATTN_NORM, 0, pitch );
}

void CZombie :: IdleSound( void )
{
	int pitch = 100 + RANDOM_LONG(-5,5);

	// Play a random idle sound
	EMIT_SOUND_DYN ( ENT(pev), CHAN_VOICE, pIdleSounds[ RANDOM_LONG(0,ARRAYSIZE(pIdleSounds)-1) ], VOL_NORM, ATTN_NORM, 0, pitch );
}

void CZombie :: AttackSound( void )
{
	int pitch = 100 + RANDOM_LONG(-5,5);

	// Play a random attack sound
	EMIT_SOUND_DYN ( ENT(pev), CHAN_VOICE, pAttackSounds[ RANDOM_LONG(0,ARRAYSIZE(pAttackSounds)-1) ], VOL_NORM, ATTN_NORM, 0, pitch );
}

//-----------------------------------------------------------------------------
// Purpose: Look in front and see if the claw hit anything.
//
// Input  :	flDist				distance to trace		
//			iDamage				damage to do if attack hits
//			vecViewPunch		camera punch (if attack hits player)
//			vecVelocityPunch	velocity punch (if attack hits player)
//
// Output : The entity hit by claws. NULL if nothing.
//-----------------------------------------------------------------------------
CBaseEntity *CZombie::ClawAttack( float flDist, int iDamage, Vector &vecViewPunch, Vector &vecVelocityPunch )
{
	//
	// Trace out a cubic section of our hull and see what we hit.
	//
	CBaseEntity *pHurt = CheckTraceHullAttack( flDist, iDamage, DMG_SLASH );

	if ( pHurt )
	{
		AttackHitSound();

		if ( pHurt->pev->flags & (FL_MONSTER|FL_CLIENT) )
		{
			pHurt->pev->punchangle = vecViewPunch;
			pHurt->pev->velocity = pHurt->pev->velocity + vecVelocityPunch;
		}
	}
	else 
	{
		AttackMissSound();
	}

	if ( RANDOM_LONG(0,1) )
		AttackSound();

	return pHurt;
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CZombie :: HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
		case ZOMBIE_AE_ATTACK_RIGHT:
		{
			// do stuff for this event.
	//		ALERT( at_console, "Slash right!\n" );
			Vector right;
			AngleVectors( pev->angles, NULL, &right, NULL );

			right = right * -100;

			ClawAttack( ZOMBIE_MELEE_REACH, sk_zombie_dmg_one_slash.value, Vector( 5, 0, -18 ), right );
		}
		break;

		case ZOMBIE_AE_ATTACK_LEFT:
		{
			// do stuff for this event.
	//		ALERT( at_console, "Slash left!\n" );
			Vector right;
			AngleVectors( pev->angles, NULL, &right, NULL );

			right = right * 100;

			ClawAttack( ZOMBIE_MELEE_REACH, sk_zombie_dmg_one_slash.value, Vector( 5, 0, 18 ), right );
		}
		break;

		case ZOMBIE_AE_ATTACK_BOTH:
		{
			// do stuff for this event.
			Vector forward;
			AngleVectors( pev->angles, &forward );

			forward = forward * 100;

			ClawAttack( ZOMBIE_MELEE_REACH, sk_zombie_dmg_both_slash.value, Vector( 5, 0, 0 ), forward );
		}
		break;

		default:
			BaseClass::HandleAnimEvent( pEvent );
			break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CZombie :: Spawn()
{
	Precache( );

	SET_MODEL(ENT(pev), "models/zombie.mdl");
	UTIL_SetSize( pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX );

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_GREEN;
	pev->health			= sk_zombie_health.value;
	pev->view_ofs		= VEC_VIEW;// position of the eyes relative to monster's origin.
	m_flFieldOfView		= 0.5;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;

	CapabilitiesClear();
	CapabilitiesAdd( bits_CAP_DOORS_GROUP );

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CZombie :: Precache()
{
	PRECACHE_MODEL("models/zombie.mdl");

	PRECACHE_SOUND_ARRAY( pAttackHitSounds );
	PRECACHE_SOUND_ARRAY( pAttackMissSounds );
	PRECACHE_SOUND_ARRAY( pAttackSounds );
	PRECACHE_SOUND_ARRAY( pIdleSounds );
	PRECACHE_SOUND_ARRAY( pAlertSounds );
	PRECACHE_SOUND_ARRAY( pPainSounds );
}	

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

/*void CZombie::RemoveIgnoredConditions ( void )
{
	if (( GetActivity() == ACT_MELEE_ATTACK1 ) || ( GetActivity() == ACT_MELEE_ATTACK2 ))
	{
#if 0
		if ( pev->health < 20 )
		{ 
			ClearCondition( COND_LIGHT_DAMAGE );
			ClearCondition( COND_HEAVY_DAMAGE );
		}
		else
#endif			
		if ( m_flNextFlinch >= gpGlobals->time)
		{
			 ClearCondition( COND_LIGHT_DAMAGE );
			 ClearCondition( COND_HEAVY_DAMAGE );
		}
	}

	if (( GetActivity() == ACT_SMALL_FLINCH ) || ( GetActivity() == ACT_BIG_FLINCH ))
	{
		if (m_flNextFlinch < gpGlobals->time)
			m_flNextFlinch = gpGlobals->time + ZOMBIE_FLINCH_DELAY;
	}

	BaseClass::RemoveIgnoredConditions();
}*/

//-----------------------------------------------------------------------------
// Purpose: Allows for modification of the interrupt mask for the current schedule.
//			In the most cases the base implementation should be called first.
//-----------------------------------------------------------------------------
void CZombie::BuildScheduleTestBits( void )
{
	// Ignore damage if we were recently damaged and we're attacking.
	if ( ( GetActivity() == ACT_MELEE_ATTACK1 ) || ( GetActivity() == ACT_MELEE_ATTACK2 ) )
	{
		if ( m_flNextFlinch >= gpGlobals->time )
		{
			ClearCustomInterruptCondition( COND_LIGHT_DAMAGE );
			ClearCustomInterruptCondition( COND_HEAVY_DAMAGE );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: Called when we change schedules.
//-----------------------------------------------------------------------------
void CZombie::OnScheduleChange( void )
{
	//
	// If we took damage and changed schedules, ignore further damage for a few seconds.
	//
	if ( HasCondition( COND_LIGHT_DAMAGE ) || HasCondition( COND_HEAVY_DAMAGE ))
	{
		m_flNextFlinch = gpGlobals->time + ZOMBIE_FLINCH_DELAY;
	} 
}