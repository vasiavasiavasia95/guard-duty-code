//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: We have made the start.
// When, at what date and time, and the proletarians of which nation will complete this process is not important. 
// The important thing is that the ice has been broken; the road is open, the way has been shown.
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "filesystem.h"
#include "basemonster.h"
#include "ai_schedule.h"
#include "ai_default.h"
#include "ai_condition.h"
#include "stringregistry.h"

//-----------------------------------------------------------------------------
// Init static variables
//-----------------------------------------------------------------------------
CAI_SchedulesManager g_AI_SchedulesManager;

//-----------------------------------------------------------------------------
// Purpose:	Delete all the string registries
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CAI_SchedulesManager::DestroyStringRegistries(void)
{
	CBaseMonster::GetSchedulingSymbols()->Clear();
	CBaseMonster::gm_SquadSlotNamespace.Clear();
}

void CAI_SchedulesManager::CreateStringRegistries( void )
{
	CBaseMonster::GetSchedulingSymbols()->Clear();
	CBaseMonster::gm_SquadSlotNamespace.Clear();
}

//-----------------------------------------------------------------------------
// Purpose: Load all the schedules
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CBaseMonster::InitSchedulingTables()
{
	CBaseMonster::gm_ClassScheduleIdSpace.Init( CBaseMonster::GetSchedulingSymbols() );
	CBaseMonster::InitDefaultScheduleSR();
	CBaseMonster::InitDefaultConditionSR();
	CBaseMonster::InitDefaultTaskSR();
	CBaseMonster::InitDefaultSquadSlotSR();
}
bool CAI_SchedulesManager::LoadAllSchedules(void)
{
	// If I haven't loaded schedules yet
	if (!CAI_SchedulesManager::allSchedules)
	{
		// Init defaults
		CBaseMonster::InitSchedulingTables();
		if (!CBaseMonster::LoadDefaultSchedules())
		{
			CBaseMonster::m_nDebugBits |= bits_debugDisableAI;
			ALERT( at_error, "Mistake in default schedule definitions, AI Disabled.\n");
		}

// UNDONE: enable this after the schedules are all loaded (right now some load in monster spawns)
#if 0
		// If not in developer mode, free the string memory.  Otherwise
		// keep it around for debugging information
		if (!g_pDeveloper->GetInt())
		{
			ClearStringRegistries();
		}
#endif

	}
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Creates and returns schedule of the given name
//			This should eventually be replaced when we convert to
//			non-hard coded schedules
// Input  :
// Output :
//-----------------------------------------------------------------------------
CAI_Schedule *CAI_SchedulesManager::CreateSchedule(char *name, int schedule_id)
{
	// Allocate schedule
	CAI_Schedule *pSched = new CAI_Schedule(name,schedule_id,CAI_SchedulesManager::allSchedules);
	CAI_SchedulesManager::allSchedules = pSched;

	// Return schedule
	return pSched;
}

//-----------------------------------------------------------------------------
// Purpose: Given text name of a NPC state returns its ID number
// Input  :
// Output :
//-----------------------------------------------------------------------------
int CAI_SchedulesManager::GetStateID(const char *state_name)
{
	if		(!stricmp(state_name,"NONE"))		{	return MONSTERSTATE_NONE;	}
	else if (!stricmp(state_name,"IDLE"))		{	return MONSTERSTATE_IDLE;	}
	else if (!stricmp(state_name,"COMBAT"))		{	return MONSTERSTATE_COMBAT;	}
	else if (!stricmp(state_name,"PRONE"))		{	return MONSTERSTATE_PRONE;	}
	else if (!stricmp(state_name,"ALERT"))		{	return MONSTERSTATE_ALERT;	}
	else if (!stricmp(state_name,"SCRIPT"))		{	return MONSTERSTATE_SCRIPT;	}
	else if (!stricmp(state_name,"PLAYDEAD"))	{	return MONSTERSTATE_PLAYDEAD;	}
	else if (!stricmp(state_name,"DEAD"))		{	return MONSTERSTATE_DEAD;	}
	else											return -1;
}

//-----------------------------------------------------------------------------
// Purpose: Given text name of a memory bit returns its ID number
// Input  :
// Output :
//-----------------------------------------------------------------------------
int CAI_SchedulesManager::GetMemoryID(const char *state_name)
{
	if		(!stricmp(state_name,"PROVOKED"))		{	return bits_MEMORY_PROVOKED;		}
	else if (!stricmp(state_name,"INCOVER"))		{	return bits_MEMORY_INCOVER;			}
	else if (!stricmp(state_name,"SUSPICIOUS"))		{	return bits_MEMORY_SUSPICIOUS;		}
	else if (!stricmp(state_name,"MOVE_FAILED"))	{	return bits_MEMORY_MOVE_FAILED;		}
	else if (!stricmp(state_name,"FLINCHED"))		{	return bits_MEMORY_FLINCHED;		}
	else if (!stricmp(state_name,"CUSTOM4"))		{	return bits_MEMORY_CUSTOM4;			}
	else if (!stricmp(state_name,"CUSTOM3"))		{	return bits_MEMORY_CUSTOM3;			}
	else if (!stricmp(state_name,"CUSTOM2"))		{	return bits_MEMORY_CUSTOM2;			}
	else if (!stricmp(state_name,"CUSTOM1"))		{	return bits_MEMORY_CUSTOM1;			}
	else												return -1;
}

//-----------------------------------------------------------------------------
// Purpose: Given the activity name, return the activity ID
//-----------------------------------------------------------------------------
int CAI_SchedulesManager::GetActivityID(const char *actName)
{
	// Vasia: due the way Goldsrc studio models handle activities
	// the original code Source uses wouldn't be working so just use data
	// from the activity map that valve so conveniently already made for us.
	for ( int i = 0; activity_map[i].name; i++)
	{
		if ( stricmp( actName, activity_map[i].name ) == 0)
			return activity_map[i].type;
	}

	return -1;
}

//-----------------------------------------------------------------------------
// Purpose: Read data on schedules
//			As I'm parsing a human generated file, give a lot of error output
// Output:  true  - if data successfully read
//			false - if data load fails
//-----------------------------------------------------------------------------

bool CAI_SchedulesManager::LoadSchedulesFromBuffer( const char *prefix, char *pfile, CAI_ClassScheduleIdSpace *pIdSpace )
{
	char token[1024];
	char save_token[1024];
	pfile = g_pFileSystem->ParseFile( pfile, token, false );

	while (!stricmp("Schedule",token))
	{
		pfile = g_pFileSystem->ParseFile( pfile, token, false );

		// -----------------------------
		// Check for duplicate schedule
		// -----------------------------
		if (GetScheduleByName(token))
		{
			ALERT( at_error, "file contains a schedule (%s) that has already been defined!\n",token);
			ALERT( at_console,"       Aborting schedule load.\n");
			Assert(0);
			return false;
		}

		int scheduleID = CBaseMonster::GetScheduleID(token);
		if (scheduleID == -1)
		{
			ALERT( at_error, "LoadSchd (%s): Unknown schedule type (%s)\n", prefix, token);
			// FIXME: .sch's not being in code/perforce makes it hard to share branches between developers
			// for now, just stop processing this entities schedules if one is found that isn't in the schedule registry
			break;
			// return false;
		}

		CAI_Schedule *new_schedule = CreateSchedule(token,scheduleID);

		pfile = g_pFileSystem->ParseFile( pfile, token, false );
		if (stricmp(token,"Tasks"))
		{
			ALERT( at_error, "LoadSchd (%s): (%s) Malformed AI Schedule.  Expecting 'Tasks' keyword.\n",prefix,new_schedule->GetName());
			Assert(0);
			return false;
		}

		// ==========================
		// Now read in the tasks
		// ==========================
		// Store in temp array until number of tasks is known
		Task_t tempTask[50];
		int	   taskNum = 0;

		pfile = g_pFileSystem->ParseFile( pfile, token, false );
		while ((token[0]!=NULL) && (stricmp("Interrupts",token)))
		{
			// Convert generic ID to sub-class specific enum
			int taskID = CBaseMonster::GetTaskID(token);
			tempTask[taskNum].iTask = (pIdSpace) ? pIdSpace->TaskGlobalToLocal(taskID) : AI_RemapFromGlobal( taskID );
			
			// If not a valid condition, send a warning message
			if (tempTask[taskNum].iTask == -1)
			{
				ALERT( at_error, "LoadSchd (%s): (%s) Unknown task %s!\n", prefix,new_schedule->GetName(), token);
				Assert(0);
				return false;
			}

			Assert( AI_IdIsLocal( tempTask[taskNum].iTask ) );

			// Read in the task argument
			pfile = g_pFileSystem->ParseFile( pfile, token, false );

			if (!stricmp("Activity",token))
			{
				// Skip the ";", but make sure it's present
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				if (stricmp(token,":"))
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Malformed AI Schedule.  Expecting ':' after type 'ACTIVITY.\n",prefix,new_schedule->GetName());
					Assert(0);
					return false;
				}

				// Load the activity and make sure its valid
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				tempTask[taskNum].flData = CAI_SchedulesManager::GetActivityID(token);
				if (tempTask[taskNum].flData == -1)
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Unknown activity %s!\n", prefix,new_schedule->GetName(), token);
					Assert(0);
					return false;
				}
			}
			else if (!stricmp("Task",token))
			{
				// Skip the ";", but make sure it's present
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				if (stricmp(token,":"))
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Malformed AI Schedule.  Expecting ':' after type 'ACTIVITY.\n",prefix,new_schedule->GetName());
					Assert(0);
					return false;
				}

				// Load the activity and make sure its valid
				pfile = g_pFileSystem->ParseFile( pfile, token, false );

				// Convert generic ID to sub-class specific enum
				int taskID = CBaseMonster::GetTaskID(token);
				tempTask[taskNum].flData = (pIdSpace) ? pIdSpace->TaskGlobalToLocal(taskID) : AI_RemapFromGlobal( taskID );

				if (tempTask[taskNum].flData == -1)
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Unknown task %s!\n", prefix,new_schedule->GetName(), token);
					Assert(0);
					return false;
				}
			}
			else if (!stricmp("Schedule",token))
			{
				// Skip the ";", but make sure it's present
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				if (stricmp(token,":"))
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Malformed AI Schedule.  Expecting ':' after type 'ACTIVITY.\n",prefix,new_schedule->GetName());
					Assert(0);
					return false;
				}

				// Load the schedule and make sure its valid
				pfile = g_pFileSystem->ParseFile( pfile, token, false );

				// Convert generic ID to sub-class specific enum
				int schedID = CBaseMonster::GetScheduleID(token);
				tempTask[taskNum].flData = (pIdSpace) ? pIdSpace->ScheduleGlobalToLocal(schedID) : AI_RemapFromGlobal( schedID );

				if (tempTask[taskNum].flData == -1)
				{
					ALERT( at_error, "LoadSchd %d (%s): (%s) Unknown shedule %s!\n", __LINE__, prefix,new_schedule->GetName(), token);
					Assert(0);
					return false;
				}
			}
			else if (!stricmp("State",token))
			{
				// Skip the ";", but make sure it's present
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				if (stricmp(token,":"))
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Malformed AI Schedule.  Expecting ':' after type 'STATE.\n",prefix,new_schedule->GetName());
					Assert(0);
					return false;
				}

				// Load the activity and make sure its valid
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				tempTask[taskNum].flData = CAI_SchedulesManager::GetStateID(token);
				if (tempTask[taskNum].flData == -1)
				{
					ALERT( at_error, "LoadSchd %d (%s): (%s) Unknown shedule %s!\n", __LINE__, prefix,new_schedule->GetName(), token);
					Assert(0);
					return false;
				}
			}
			else if (!stricmp("Memory",token))
			{

				// Skip the ";", but make sure it's present
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				if (stricmp(token,":"))
				{
					ALERT( at_error, "LoadSchd (%s): (%s) Malformed AI Schedule.  Expecting ':' after type 'STATE.\n",prefix,new_schedule->GetName());
					Assert(0);
					return false;
				}

				// Load the activity and make sure its valid
				pfile = g_pFileSystem->ParseFile( pfile, token, false );
				tempTask[taskNum].flData = CAI_SchedulesManager::GetMemoryID(token);
				if (tempTask[taskNum].flData == -1)
				{
					ALERT( at_error, "LoadSchd %d (%s): (%s) Unknown shedule %s!\n", __LINE__, prefix,new_schedule->GetName(), token);
					Assert(0);
					return false;
				}
			}
			else if (!stricmp("Interrupts",token) || !strnicmp("TASK_",token,5) )
			{
				// a parse error.  Interrupts is the next section, TASK_ is probably the next task, missing task argument?
				ALERT( at_error, "LoadSchd (%s): (%s) Bad syntax at task #%d (wasn't expecting %s)\n", prefix, new_schedule->GetName(), taskNum, token);
				Assert(0);
				return false;
			}
			else
			{
				tempTask[taskNum].flData = atof(token);
			}
			taskNum++;

			// Read the next token
			Q_strncpy(save_token,token,sizeof(save_token));
			pfile = g_pFileSystem->ParseFile( pfile, token, false );

			// Check for malformed task argument type
			if (!stricmp(token,":"))
			{
				ALERT( at_error, "LoadSchd (%s): Schedule (%s),\n        Task (%s), has a malformed AI Task Argument = (%s)\n",
						prefix,new_schedule->GetName(),taskID,save_token);
				Assert(0);
				return false;
			}
		}

		// Now copy the tasks into the new schedule
		new_schedule->m_iNumTasks = taskNum;
		new_schedule->m_pTaskList = new Task_t[taskNum];
		for (int i=0;i<taskNum;i++)
		{
			new_schedule->m_pTaskList[i].iTask		= tempTask[i].iTask;
			new_schedule->m_pTaskList[i].flData = tempTask[i].flData;

			Assert( AI_IdIsLocal( new_schedule->m_pTaskList[i].iTask ) );
		}

		// ==========================
		// Now read in the interrupts
		// ==========================
		pfile = g_pFileSystem->ParseFile( pfile, token, false );
		while ((token[0]!=NULL) && (stricmp("Schedule",token)))
		{
			// Convert generic ID to sub-class specific enum
			int condID = CBaseMonster::GetConditionID(token);

			// If not a valid condition, send a warning message
			if (condID == -1)
			{
				ALERT( at_error, "LoadSchd (%s): Schedule (%s), Unknown condition %s!\n", prefix,new_schedule->GetName(),token);
				Assert(0);
			}

			// Otherwise, add to this schedules list of conditions
			else
			{
				int interrupt = AI_RemapFromGlobal(condID);
				Assert( AI_IdIsGlobal( condID ) && interrupt >= 0 && interrupt < MAX_CONDITIONS );
				new_schedule->m_InterruptMask.Set(interrupt);
			}

			// Read the next token
			pfile = g_pFileSystem->ParseFile( pfile, token, false );
		}
	}
	return true;
}

bool CAI_SchedulesManager::LoadSchedules( const char *prefix, CAI_ClassScheduleIdSpace *pIdSpace  )
{
	char sz[128];

	// Open the weapon's data file and read the weaponry details
	Q_snprintf(sz,sizeof(sz), "scripts/%s.sch",prefix);
	char *pfile = (char*)LOAD_FILE_FOR_ME( sz, NULL );

	if (!pfile)
	{
		ALERT( at_aiconsole, "Unable to open AI Schedule data file for: %s\n", sz);
		return false;
	}
	if (!LoadSchedulesFromBuffer( prefix, pfile, pIdSpace))
	{
		ALERT( at_aiconsole, "       Schedule file: %s\n", sz );
		FREE_FILE( pfile );
		return false;
	}
	FREE_FILE( pfile );
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Given a schedule ID, returns a schedule of the given type
//-----------------------------------------------------------------------------
CAI_Schedule *CAI_SchedulesManager::GetScheduleFromID( int schedID )
{
	for ( CAI_Schedule *schedule = CAI_SchedulesManager::allSchedules; schedule != NULL; schedule = schedule->nextSchedule )
	{
		if (schedule->m_iScheduleID == schedID)
			return schedule;
	}

	ALERT( at_aiconsole, "Couldn't find schedule (%s)\n", CBaseMonster::GetSchedulingSymbols()->ScheduleIdToSymbol(schedID) );

	return NULL;
}

//-----------------------------------------------------------------------------
// Purpose: Given a schedule name, returns a schedule of the given type
//-----------------------------------------------------------------------------
CAI_Schedule *CAI_SchedulesManager::GetScheduleByName( const char *name )
{
	for ( CAI_Schedule *schedule = CAI_SchedulesManager::allSchedules; schedule != NULL; schedule = schedule->nextSchedule )
	{
		if (FStrEq(schedule->GetName(),name))
			return schedule;
	}

	return NULL;
}

//-----------------------------------------------------------------------------
// Purpose: Delete all the schedules
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CAI_SchedulesManager::DeleteAllSchedules(void)
{
	m_CurLoadSig++;

	if ( m_CurLoadSig < 0 )
		m_CurLoadSig = 0;

	CAI_Schedule *schedule = CAI_SchedulesManager::allSchedules;
	CAI_Schedule *next;

	while (schedule)
	{
		next = schedule->nextSchedule;
		delete schedule;
		schedule = next;
	}
	CAI_SchedulesManager::allSchedules = NULL;
}


//-----------------------------------------------------------------------------
// Purpose:
// Input  :
// Output :
//-----------------------------------------------------------------------------

CAI_Schedule::CAI_Schedule(char *name, int schedule_id, CAI_Schedule *pNext)
{
	m_iScheduleID = schedule_id;

	int len = strlen(name);
	m_pName = new char[len+1];
	Q_strncpy(m_pName,name,len+1);

	m_pTaskList = NULL;
	m_iNumTasks = 0;

	// ---------------------------------
	//  Add to linked list of schedules
	// ---------------------------------
	nextSchedule = pNext;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
CAI_Schedule::~CAI_Schedule( void )
{
	delete[] m_pName;
	delete[] m_pTaskList;
}