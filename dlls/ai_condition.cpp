//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================

#include "cbase.h"
#include "stringregistry.h"
#include "basemonster.h"
#include "ai_condition.h"

//-----------------------------------------------------------------------------
// Purpose: Given and condition name, return the condition ID
//-----------------------------------------------------------------------------
int CBaseMonster::GetConditionID(const char* condName)
{
	return GetSchedulingSymbols()->ConditionSymbolToId(condName);
}

//-----------------------------------------------------------------------------
// Purpose: Register the default conditions
// Input  :
// Output :
//-----------------------------------------------------------------------------

#define ADD_CONDITION_TO_SR( _n ) idSpace.AddCondition( #_n, _n, "CBaseMonster" )

void CBaseMonster::InitDefaultConditionSR(void)
{
	CAI_ClassScheduleIdSpace &idSpace = CBaseMonster::AccessClassScheduleIdSpaceDirect();

	ADD_CONDITION_TO_SR( COND_NONE );
	ADD_CONDITION_TO_SR( COND_NO_AMMO_LOADED );
	ADD_CONDITION_TO_SR( COND_SEE_HATE );
	ADD_CONDITION_TO_SR( COND_SEE_FEAR );
	ADD_CONDITION_TO_SR( COND_SEE_DISLIKE );
	ADD_CONDITION_TO_SR( COND_SEE_ENEMY );
	ADD_CONDITION_TO_SR( COND_LOST_ENEMY );
	ADD_CONDITION_TO_SR( COND_SMELL_FOOD );
	ADD_CONDITION_TO_SR( COND_ENEMY_OCCLUDED );
	ADD_CONDITION_TO_SR( COND_ENEMY_TOO_FAR );
	ADD_CONDITION_TO_SR( COND_LIGHT_DAMAGE );
	ADD_CONDITION_TO_SR( COND_HEAVY_DAMAGE );
	ADD_CONDITION_TO_SR( COND_CAN_RANGE_ATTACK1 );
	ADD_CONDITION_TO_SR( COND_CAN_RANGE_ATTACK2 );
	ADD_CONDITION_TO_SR( COND_CAN_MELEE_ATTACK1 );
	ADD_CONDITION_TO_SR( COND_CAN_MELEE_ATTACK2 );
	ADD_CONDITION_TO_SR( COND_PROVOKED );
	ADD_CONDITION_TO_SR( COND_NEW_ENEMY );
	ADD_CONDITION_TO_SR( COND_HEAR_DANGER );
	ADD_CONDITION_TO_SR( COND_HEAR_COMBAT );
	ADD_CONDITION_TO_SR( COND_HEAR_WORLD );
	ADD_CONDITION_TO_SR( COND_HEAR_PLAYER );
	ADD_CONDITION_TO_SR( COND_SMELL );
	ADD_CONDITION_TO_SR( COND_ENEMY_FACING_ME );
	ADD_CONDITION_TO_SR( COND_ENEMY_DEAD );
	ADD_CONDITION_TO_SR( COND_ENEMY_UNREACHABLE );
	ADD_CONDITION_TO_SR( COND_SEE_PLAYER );
	ADD_CONDITION_TO_SR( COND_SEE_NEMESIS );
	ADD_CONDITION_TO_SR( COND_TASK_FAILED );
	ADD_CONDITION_TO_SR( COND_SCHEDULE_DONE );
	ADD_CONDITION_TO_SR( COND_NO_CUSTOM_INTERRUPTS );
}
