/***
*
*	Copyright (c) 1999, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/


#include "cbase.h"
#include "weapons.h"
#include "player.h"
#include "gamerules.h"

// special deathmatch shotgun spreads
#define VECTOR_CONE_DM_SHOTGUN	Vector( 0.08716, 0.04362, 0.00  )// 10 degrees by 5 degrees
#define VECTOR_CONE_DM_DOUBLESHOTGUN Vector( 0.17365, 0.04362, 0.00 ) // 20 degrees by 5 degrees

enum shotgun_e {
	SHOTGUN_IDLE = 0,
	SHOTGUN_FIRE,
	SHOTGUN_FIRE2,
	SHOTGUN_RELOAD,
	SHOTGUN_PUMP,
	SHOTGUN_START_RELOAD,
	SHOTGUN_DRAW,
	SHOTGUN_HOLSTER,
	SHOTGUN_IDLE4,
	SHOTGUN_IDLE_DEEP
};

class CShotgun : public CBasePlayerWeapon
{
	DECLARE_CLASS( CShotgun, CBasePlayerWeapon );
public:
	
	DECLARE_DATADESC();

	void Spawn(void);
	void Precache(void);
	int iItemSlot() { return 3; }
	int GetItemInfo(ItemInfo *p);
	int AddToPlayer(CBasePlayer *pPlayer);

	void PrimaryAttack(void);
	void SecondaryAttack(void);
	bool Deploy();
	void Reload(void);
	void WeaponIdle(void);

	void ItemPostFrame( void );
	void FillClip( void );
	void FinishReload( void );
	bool StartReload( void );

private:

	int m_iShell;
	float m_flPumpTime;

	bool	m_bNeedPump;		// When emptied completely
	bool	m_bDelayedFire1;	// Fire primary when finished reloading
	bool	m_bDelayedFire2;	// Fire secondary when finished reloading
};

LINK_ENTITY_TO_CLASS(weapon_shotgun, CShotgun);

PRECACHE_WEAPON_REGISTER( weapon_shotgun );

BEGIN_DATADESC(	CShotgun )

	DEFINE_FIELD( CShotgun, m_flPumpTime, FIELD_TIME),

	DEFINE_FIELD( CShotgun, m_bNeedPump, FIELD_BOOLEAN ),
	DEFINE_FIELD( CShotgun, m_bDelayedFire1, FIELD_BOOLEAN ),
	DEFINE_FIELD( CShotgun, m_bDelayedFire2, FIELD_BOOLEAN ),

END_DATADESC()

void CShotgun::Spawn()
{
	Precache();
	m_iId = WEAPON_SHOTGUN;
	SET_MODEL(ENT(pev), "models/w_shotgun.mdl");

	m_iDefaultAmmo = SHOTGUN_DEFAULT_GIVE;

	FallInit();// get ready to fall
}


void CShotgun::Precache(void)
{
	PRECACHE_MODEL("models/v_shotgun.mdl");
	PRECACHE_MODEL("models/w_shotgun.mdl");
	PRECACHE_MODEL("models/p_shotgun.mdl");

	m_iShell = PRECACHE_MODEL("models/shotgunshell.mdl");// shotgun shell

	PRECACHE_SOUND("items/9mmclip1.wav");

	PRECACHE_SOUND("weapons/dbarrel1.wav");//shotgun
	PRECACHE_SOUND("weapons/sbarrel1.wav");//shotgun

	PRECACHE_SOUND("weapons/reload1.wav");	// shotgun reload
	PRECACHE_SOUND("weapons/reload3.wav");	// shotgun reload

//	PRECACHE_SOUND ("weapons/sshell1.wav");	// shotgun reload - played on client
//	PRECACHE_SOUND ("weapons/sshell3.wav");	// shotgun reload - played on client

	PRECACHE_SOUND("weapons/357_cock1.wav"); // gun empty sound
	PRECACHE_SOUND("weapons/scock1.wav");	// cock gun
}

int CShotgun::AddToPlayer(CBasePlayer *pPlayer)
{
	if (CBasePlayerWeapon::AddToPlayer(pPlayer))
	{
		MESSAGE_BEGIN(MSG_ONE, gmsgWeapPickup, NULL, pPlayer->pev);
		WRITE_BYTE(m_iId);
		MESSAGE_END();
		return TRUE;
	}
	return FALSE;
}


int CShotgun::GetItemInfo(ItemInfo *p)
{
	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "buckshot";
	p->iMaxAmmo1 = BUCKSHOT_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip = SHOTGUN_MAX_CLIP;
	p->iSlot = 2;
	p->iPosition = 1;
	p->iFlags = 0;
	p->iId = m_iId = WEAPON_SHOTGUN;
	p->iWeight = SHOTGUN_WEIGHT;

	return 1;
}

bool CShotgun::Deploy()
{
	return DefaultDeploy("models/v_shotgun.mdl", "models/p_shotgun.mdl", SHOTGUN_DRAW, "shotgun");
}

bool CShotgun::StartReload( void )
{
	if (m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType] <= 0)
		return false;

	if (m_iClip >= iMaxClip())
		return false;

	int j = V_min(1, m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType]);

	if (j <= 0)
		return false;

	SendWeaponAnim( SHOTGUN_START_RELOAD );

	m_pPlayer->SetNextAttack( gpGlobals->time );
	m_flNextPrimaryAttack = gpGlobals->time + 1.0;

	m_fInReload = true;
	return true;
}

//=========================================================
// Reload - Override so only reload one shell at a time
//=========================================================
void CShotgun::Reload( void )
{
	// Check that StartReload was called first
	if (!m_fInReload)
	{
		ALERT( at_error, "Shotgun Reload called incorrectly!\n" );
	}

	if ( m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType] <= 0 )
		return;

	if (m_iClip >= iMaxClip())
		return;

	int j = V_min(1, m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType]);

	if (j <= 0)
		return;

	FillClip();

	// Play reload on different channel as otherwise steals channel away from fire sound
	if (RANDOM_LONG(0, 1))
		EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_ITEM, "weapons/reload1.wav", 1, ATTN_NORM, 0, 85 + RANDOM_LONG(0, 0x1f));
	else
		EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_ITEM, "weapons/reload3.wav", 1, ATTN_NORM, 0, 85 + RANDOM_LONG(0, 0x1f));

	SendWeaponAnim(SHOTGUN_RELOAD);

	m_pPlayer->SetNextAttack( gpGlobals->time );
	m_flNextPrimaryAttack = gpGlobals->time + 0.6;
}

//=========================================================
// FinishReload - Play finish reload anim and fill clip
//=========================================================
void CShotgun::FinishReload( void )
{
	m_fInReload = false;

	// Finish reload animation
	SendWeaponAnim( SHOTGUN_PUMP );

	// play cocking sound
	EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_ITEM, "weapons/scock1.wav", 1, ATTN_NORM, 0, 95 + RANDOM_LONG(0,0x1f));

	m_pPlayer->SetNextAttack( gpGlobals->time );
	m_flNextPrimaryAttack = gpGlobals->time + 0.7;
	m_flTimeWeaponIdle = gpGlobals->time + 1.5;
}

//=========================================================
// FillClip - Play finish reload anim and fill clip
//=========================================================
void CShotgun::FillClip( void )
{
	if ( m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType] <= 0 )
		return;

	if (m_iClip >= iMaxClip())
		return;

	// Add them to the clip
	m_iClip += 1;
	m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType] -= 1;
}

void CShotgun::PrimaryAttack()
{
	m_pPlayer->m_iWeaponVolume = LOUD_GUN_VOLUME;
	m_pPlayer->m_iWeaponFlash = NORMAL_GUN_FLASH;

	m_iClip--;
	m_pPlayer->pev->effects = (int)(m_pPlayer->pev->effects) | EF_MUZZLEFLASH;

	SendWeaponAnim(SHOTGUN_FIRE);

	// player "shoot" animation
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_WEAPON, "weapons/sbarrel1.wav", RANDOM_FLOAT(0.95, 1.0), ATTN_NORM, 0, 93 + RANDOM_LONG(0, 0x1f));

	Vector vecSrc = m_pPlayer->GetGunPosition();
	Vector vecAiming = m_pPlayer->GetAutoaimVector(AUTOAIM_5DEGREES);

	if (g_pGameRules->IsDeathmatch())
	{
		// altered deathmatch spread
		m_pPlayer->FireBullets(4, vecSrc, vecAiming, VECTOR_CONE_DM_SHOTGUN, 2048, BULLET_PLAYER_BUCKSHOT, 0);
	}
	else
	{
		// regular old, untouched spread. 
		m_pPlayer->FireBullets(6, vecSrc, vecAiming, VECTOR_CONE_10DEGREES, 2048, BULLET_PLAYER_BUCKSHOT, 0);
	}

	if (m_iClip != 0)
		m_flPumpTime = gpGlobals->time + 0.5;

	m_flNextPrimaryAttack = gpGlobals->time + 0.75;
	
	if (m_iClip != 0)
		m_flTimeWeaponIdle = gpGlobals->time + 5.0;
	else
		m_flTimeWeaponIdle = gpGlobals->time + 0.75;
	m_fInReload = 0;

	m_pPlayer->pev->punchangle.x -= 5;
}


void CShotgun::SecondaryAttack(void)
{
	m_iClip -= 2;

	m_pPlayer->pev->effects = (int)(m_pPlayer->pev->effects) | EF_MUZZLEFLASH;

	SendWeaponAnim(SHOTGUN_FIRE2);

	// player "shoot" animation
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_WEAPON, "weapons/dbarrel1.wav", RANDOM_FLOAT(0.98, 1.0), ATTN_NORM, 0, 85 + RANDOM_LONG(0, 0x1f));

	Vector vecSrc = m_pPlayer->GetGunPosition();
	Vector vecAiming = m_pPlayer->GetAutoaimVector(AUTOAIM_5DEGREES);

	if (g_pGameRules->IsDeathmatch())
	{
		// tuned for deathmatch
		m_pPlayer->FireBullets(8, vecSrc, vecAiming, VECTOR_CONE_DM_DOUBLESHOTGUN, 2048, BULLET_PLAYER_BUCKSHOT, 0);
	}
	else
	{
		// untouched default single player
		m_pPlayer->FireBullets(12, vecSrc, vecAiming, VECTOR_CONE_10DEGREES, 2048, BULLET_PLAYER_BUCKSHOT, 0);
	}


	if (m_iClip != 0)
		m_flPumpTime = gpGlobals->time + 0.95;

	m_flNextPrimaryAttack = gpGlobals->time + 1.5;
	
	if (m_iClip != 0)
		m_flTimeWeaponIdle = gpGlobals->time + 6.0;
	else
		m_flTimeWeaponIdle = gpGlobals->time + 1.5;

	m_fInReload = 0;

	m_pPlayer->pev->punchangle.x -= 10;
}


//-----------------------------------------------------------------------------
// Purpose: Override so shotgun can do mulitple reloads in a row
//-----------------------------------------------------------------------------
void CShotgun::ItemPostFrame( void )
{
	if (m_fInReload)
	{
		// If I'm primary firing and have one round stop reloading and fire
		if ((m_pPlayer->pev->button & IN_ATTACK ) && ( m_iClip >= 1))
		{
			m_fInReload		= FALSE;
			m_bDelayedFire1 = TRUE;
			m_flTimeWeaponIdle = gpGlobals->time + 1.5;
		}
		// If I'm secondary firing and have two rounds stop reloading and fire
		else if ((m_pPlayer->pev->button & IN_ATTACK2 ) && (m_iClip >= 2))
		{
			m_fInReload		= false;
			m_bDelayedFire2 = true;
			m_flTimeWeaponIdle = gpGlobals->time + 1.5;
		}
		else if ( m_flNextPrimaryAttack <= gpGlobals->time )
		{
			// If out of ammo end reload
			if ( m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType] <= 0)
			{
				FinishReload();
				return;
			}
			// If clip not full reload again
			if (m_iClip < iMaxClip())
			{
				Reload();
				return;
			}
			// Clip full, stop reloading
			else
			{
				FinishReload();
				return;
			}
		}
	}

	if (m_flPumpTime && m_flPumpTime < gpGlobals->time)
	{
		// play pumping sound
		EMIT_SOUND_DYN(ENT(m_pPlayer->pev), CHAN_ITEM, "weapons/scock1.wav", 1, ATTN_NORM, 0, 95 + RANDOM_LONG(0, 0x1f));
		m_flPumpTime = 0;
	}
	
	// Shotgun uses same timing and ammo for secondary attack
	if ((m_bDelayedFire2 || m_pPlayer->pev->button & IN_ATTACK2 ) && ( m_flNextPrimaryAttack <= gpGlobals->time))
	{
		m_bDelayedFire2 = false;
		if ( m_iClip <= 1 )
		{
			if (!m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType])
			{
				PlayEmptySound();
				m_flNextPrimaryAttack	= gpGlobals->time + 0.75;
				m_flTimeWeaponIdle = gpGlobals->time + 1.0;
			}
			else
			{
				StartReload();
			}
		}

		// don't fire underwater
		else if ( m_pPlayer->pev->waterlevel == 3)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = gpGlobals->time + 0.15;
			return;
		}
		else
		{
			// If the firing button was just pressed, reset the firing time
			if ( m_pPlayer->pev->button & IN_ATTACK )
			{
				 m_flNextPrimaryAttack = gpGlobals->time;
			}
			SecondaryAttack();
		}
	}
	else if ( (m_bDelayedFire1 || m_pPlayer->pev->button & IN_ATTACK) && m_flNextPrimaryAttack <= gpGlobals->time)
	{
		m_bDelayedFire1 = false;
		if ( ( m_iClip <= 0 ) )
		{
			if (!m_pPlayer->m_rgAmmo[m_iPrimaryAmmoType])
			{
				PlayEmptySound();
				m_flNextPrimaryAttack = gpGlobals->time + 0.75;
				m_flTimeWeaponIdle = gpGlobals->time + 1.0;
			}
			else
			{
				StartReload();
			}
		}
		// don't fire underwater
		else if ( m_pPlayer->pev->waterlevel == 3 )
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = gpGlobals->time + 0.15;
			return;
		}
		else
		{
			// If the firing button was just pressed, reset the firing time
			if ( m_pPlayer->m_afButtonPressed & IN_ATTACK )
			{
				 m_flNextPrimaryAttack = gpGlobals->time;
			}
			PrimaryAttack();
		}
	}

	if ( m_pPlayer->pev->button & IN_RELOAD && !m_fInReload ) 
	{
		// reload when reload is pressed, or if no buttons are down and weapon is empty.
		StartReload();
	}
	else 
	{
		// no fire buttons down
		m_fFireOnEmpty = FALSE;

		if ( !IsUseable() && m_flNextPrimaryAttack < gpGlobals->time )
		{
			// weapon isn't useable, switch.
			if ( !(iFlags() & ITEM_FLAG_NOAUTOSWITCHEMPTY) && g_pGameRules->GetNextBestWeapon(m_pPlayer, this) )
			{
				m_flNextPrimaryAttack = gpGlobals->time + 0.3;
				return;
			}
		}
		else
		{
			// weapon is useable. Reload if empty and weapon has waited as long as it has to after firing
			if ( m_iClip == 0 && !(iFlags() & ITEM_FLAG_NOAUTORELOAD) && m_flNextPrimaryAttack < gpGlobals->time )
			{
				if (StartReload())
				{
					// if we've successfully started to reload, we're done
					return;
				}
			}
		}

		WeaponIdle( );
		return;
	}

}


void CShotgun::WeaponIdle(void)
{
	ResetEmptySound();

	m_pPlayer->GetAutoaimVector(AUTOAIM_5DEGREES);

	if ( m_flTimeWeaponIdle > gpGlobals->time || m_fInReload )
		return;

	int iAnim;
			
	float flRand = RANDOM_FLOAT(0, 1);

	if (flRand <= 0.95)
	{
		iAnim = SHOTGUN_IDLE;
		m_flTimeWeaponIdle = gpGlobals->time + (20.0 / 9.0);
	}
	else
	{
		iAnim = SHOTGUN_IDLE4;
		m_flTimeWeaponIdle = gpGlobals->time + (60.0 / 15.0);
	}

	SendWeaponAnim(iAnim);
}





