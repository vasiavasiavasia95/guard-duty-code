/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// monster template
//=========================================================


#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"

//=========================================================
// Monster's Anim Events Go Here
//=========================================================

class CPanther : public CBaseMonster
{
	DECLARE_CLASS( CPanther, CBaseMonster );

public:
	void Spawn( void );
	void Precache( void );
	void SetYawSpeed( void );
	Class_T Classify ( void );
	void HandleAnimEvent( animevent_t *pEvent );

	void IdleSound( void );

	static const char *pIdleSounds[];
};

LINK_ENTITY_TO_CLASS( monster_panther, CPanther );

const char *CPanther::pIdleSounds[] =
{
	"panthereye/pa_idle1.wav",
	"panthereye/pa_idle2.wav",
	"panthereye/pa_idle3.wav",
	"panthereye/pa_idle4.wav",
};

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CPanther::Classify( void )
{
	return CLASS_ALIEN_PREDATOR;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CPanther::SetYawSpeed ( void )
{
	int ys;

	switch ( m_Activity )
	{
	case ACT_IDLE:
	default:
		ys = 90;
	}

	pev->yaw_speed = ys;
}

void CPanther::IdleSound( void )
{
	// Play a random idle sound
	EMIT_SOUND( edict(), CHAN_VOICE, pIdleSounds[ RANDOM_LONG(0,ARRAYSIZE(pIdleSounds)-1) ], VOL_NORM, ATTN_NORM );
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CPanther::HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case 0:
	default:
		BaseClass::HandleAnimEvent( pEvent );
		break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CPanther::Spawn()
{
	Precache( );

	SET_MODEL( edict(), "models/panther.mdl" );
	UTIL_SetSize( pev, Vector( -32, -32, 0 ), Vector( 32, 32, 64 ) ); // from the Alpha

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_YELLOW;
	pev->health			= 50; // from the Alpha
	pev->view_ofs		= vec3_origin;// position of the eyes relative to monster's origin.
	m_flFieldOfView		= 0.5;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CPanther::Precache()
{
	PRECACHE_MODEL("models/panther.mdl");

	PRECACHE_SOUND_ARRAY(pIdleSounds);
}	

//=========================================================
// AI Schedules Specific to this monster
//=========================================================