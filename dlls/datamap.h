//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef DATAMAP_H
#define DATAMAP_H
#ifdef _WIN32
#pragma once
#endif

#ifndef VECTOR_H
#include "vector.h"
#endif

// SINGLE_INHERITANCE restricts the size of CBaseEntity pointers-to-member-functions to 4 bytes
class SINGLE_INHERITANCE CBaseEntity;

//-----------------------------------------------------------------------------
// Field sizes... 
//-----------------------------------------------------------------------------
template <int FIELD_TYPE>
class CDatamapFieldSizeDeducer
{
public:
	enum
	{
		SIZE = 0
	};

	static int FieldSize( )
	{
		return 0;
	}
};

#define DECLARE_FIELD_SIZE( _fieldType, _fieldSize )	\
	template< > class CDatamapFieldSizeDeducer<_fieldType> { public: enum { SIZE = _fieldSize }; static int FieldSize() { return _fieldSize; } };
#define FIELD_SIZE( _fieldType )	CDatamapFieldSizeDeducer<_fieldType>::SIZE

DECLARE_FIELD_SIZE( FIELD_FLOAT,		sizeof(float))
DECLARE_FIELD_SIZE( FIELD_STRING,		sizeof(int))
DECLARE_FIELD_SIZE( FIELD_ENTITY,		sizeof(int))
DECLARE_FIELD_SIZE( FIELD_CLASSPTR,		sizeof(int))
DECLARE_FIELD_SIZE( FIELD_EHANDLE,		2 * sizeof(int)) // Vasia: Assert keeps screaming at me if this not 8 bytes. TODO: Figure out why
DECLARE_FIELD_SIZE( FIELD_EVARS,		sizeof(int))
DECLARE_FIELD_SIZE( FIELD_EDICT,		sizeof(int))
DECLARE_FIELD_SIZE( FIELD_VECTOR,		3 * sizeof(float))
DECLARE_FIELD_SIZE( FIELD_POSITION_VECTOR, 	3 * sizeof(float))
DECLARE_FIELD_SIZE( FIELD_POINTER,		sizeof(int *))
DECLARE_FIELD_SIZE( FIELD_INTEGER,		sizeof(int))
#ifdef POSIX
// pointer to members under gnuc are 8bytes if you have a virtual func
DECLARE_FIELD_SIZE( FIELD_FUNCTION,		sizeof(int *)*2)
#else
DECLARE_FIELD_SIZE( FIELD_FUNCTION,		sizeof(int *))
#endif
DECLARE_FIELD_SIZE( FIELD_BOOLEAN,		sizeof(char))
DECLARE_FIELD_SIZE( FIELD_SHORT,		sizeof(short))
DECLARE_FIELD_SIZE( FIELD_CHARACTER,	sizeof(char))
DECLARE_FIELD_SIZE( FIELD_TIME,			sizeof(float))
DECLARE_FIELD_SIZE( FIELD_MODELNAME,	sizeof(int))
DECLARE_FIELD_SIZE( FIELD_SOUNDNAME,	sizeof(int))

#define _FIELD(type,name,fieldtype,count,flags)		{ fieldtype, #name, offsetof(type, name), count, flags, NULL, NULL, sizeof( ((type *)0)->name ) }
#define DEFINE_FIELD(type,name,fieldtype)			_FIELD(type, name, fieldtype, 1, FTYPEDESC_SAVE )
#define DEFINE_AUTO_ARRAY(type,name,fieldtype)		_FIELD(type, name, fieldtype, ARRAYSIZE(((type *)0)->name), FTYPEDESC_SAVE )
#define DEFINE_ARRAY(type,name,fieldtype,count)		_FIELD(type, name, fieldtype, count, FTYPEDESC_SAVE )
#define DEFINE_ENTITY_FIELD(name,fieldtype)			_FIELD(entvars_t, name, fieldtype, 1, FTYPEDESC_SAVE )
#define DEFINE_ENTITY_ARRAY(name,fieldtype)			_FIELD(entvars_t, name, fieldtype, ARRAYSIZE(((entvars_t *)0)->name), FTYPEDESC_SAVE )
#define DEFINE_ENTITY_GLOBAL_FIELD(name,fieldtype)	_FIELD(entvars_t, name, fieldtype, 1, FTYPEDESC_SAVE | FTYPEDESC_GLOBAL )
#define DEFINE_GLOBAL_FIELD(type,name,fieldtype)	_FIELD(type, name, fieldtype, 1, FTYPEDESC_SAVE | FTYPEDESC_GLOBAL )
#define DEFINE_CUSTOM_FIELD(type,name,datafuncs)	{ FIELD_CUSTOM, #name, offsetof(type,name), 1, FTYPEDESC_SAVE, datafuncs, NULL }

#define DEFINE_EMBEDDED( type, name )						\
	{ FIELD_EMBEDDED, #name, offsetof(type, name), 1, FTYPEDESC_SAVE, NULL, &(((type *)0)->name.m_DataMap), sizeof( ((type *)0)->name ) }

#define DEFINE_EMBEDDEDBYREF( type, name )					\
	{ FIELD_EMBEDDED, #name, offsetof(type, name), 1, FTYPEDESC_SAVE | FTYPEDESC_PTR, NULL, &(((type *)0)->name->m_DataMap), sizeof( *(((type *)0)->name) ) }

// Vasia: FTYPEDESC_GLOBAL is recognised by the engine and thus must always be first!!!
#define FTYPEDESC_GLOBAL			0x0001		// This field is masked for global entity save/restore
#define FTYPEDESC_SAVE				0x0002		// This field is saved to disk
#define FTYPEDESC_PTR				0x0004		// This field is a pointer, not an embedded object

class ISaveRestoreOps;

struct datamap_t;

struct typedescription_t
{
	FIELDTYPE			fieldType;
	const char			*fieldName;
	int					fieldOffset;
	short				fieldSize;
	short				flags;

	// pointer to the function set for save/restoring of custom data types
	ISaveRestoreOps		*pSaveRestoreOps; 		

	// For embedding additional datatables inside this one
	datamap_t			*td;

	// Stores the actual member variable size in bytes
	int					fieldSizeInBytes;
};

//-----------------------------------------------------------------------------
// Purpose: stores the list of objects in the hierarchy
//			used to iterate through an object's data descriptions
//-----------------------------------------------------------------------------
struct datamap_t
{
	typedescription_t	*dataDesc;
	int					dataNumFields;
	char const			*dataClassName;
	datamap_t			*baseMap;
};


//-----------------------------------------------------------------------------
//
// Macros used to implement datadescs
//

#define DECLARE_SIMPLE_DATADESC() \
	static datamap_t m_DataMap; \
	template <typename T> friend datamap_t *DataMapInit(T *);

#define	DECLARE_DATADESC() \
	DECLARE_SIMPLE_DATADESC() \
	virtual datamap_t *GetDataDescMap( void );

#define BEGIN_DATADESC( className ) \
	datamap_t className::m_DataMap = { 0, 0, #className, &BaseClass::m_DataMap }; \
	datamap_t *className::GetDataDescMap( void ) { return &m_DataMap; } \
	BEGIN_DATADESC_GUTS( className )

#define BEGIN_DATADESC_NO_BASE( className ) \
	datamap_t className::m_DataMap = { 0, 0, #className, NULL }; \
	datamap_t *className::GetDataDescMap( void ) { return &m_DataMap; } \
	BEGIN_DATADESC_GUTS( className )

#define BEGIN_SIMPLE_DATADESC( className ) \
	datamap_t className::m_DataMap = { 0, 0, #className, NULL }; \
	BEGIN_DATADESC_GUTS( className )

#define BEGIN_SIMPLE_DATADESC_( className, BaseClass ) \
	datamap_t className::m_DataMap = { 0, 0, #className, &BaseClass::m_DataMap }; \
	BEGIN_DATADESC_GUTS( className )

#define BEGIN_DATADESC_GUTS( className ) \
	template <typename T> datamap_t *DataMapInit(T *); \
	template <> datamap_t *DataMapInit<className>( className * ); \
	namespace className##_DataDescInit \
	{ \
		datamap_t *g_DataMapHolder = DataMapInit( (className *)NULL ); /* This can/will be used for some clean up duties later */ \
	} \
	\
	template <> datamap_t *DataMapInit<className>( className * ) \
	{ \
		typedef className classNameTypedef; \
		static typedescription_t dataDesc[] = \
		{ \
		{ FIELD_VOID, 0,0,0,0,0,0,0}, /* so you can define "empty" tables */

#define END_DATADESC() \
		}; \
		\
		if ( sizeof( dataDesc ) > sizeof( dataDesc[0] ) ) \
		{ \
			classNameTypedef::m_DataMap.dataNumFields = ARRAYSIZE( dataDesc ) - 1; \
			classNameTypedef::m_DataMap.dataDesc 	  = &dataDesc[1]; \
		} \
		else \
		{ \
			classNameTypedef::m_DataMap.dataNumFields = 1; \
			classNameTypedef::m_DataMap.dataDesc 	  = dataDesc; \
		} \
		return &classNameTypedef::m_DataMap; \
	}

// used for when there is no data description
#define IMPLEMENT_NULL_SIMPLE_DATADESC( derivedClass ) \
	BEGIN_SIMPLE_DATADESC( derivedClass ) \
	END_DATADESC()

#define IMPLEMENT_NULL_SIMPLE_DATADESC_( derivedClass, baseClass ) \
	BEGIN_SIMPLE_DATADESC_( derivedClass, baseClass ) \
	END_DATADESC()

#define IMPLEMENT_NULL_DATADESC( derivedClass ) \
	BEGIN_DATADESC( derivedClass ) \
	END_DATADESC()

//-----------------------------------------------------------------------------

#endif // DATAMAP_H
