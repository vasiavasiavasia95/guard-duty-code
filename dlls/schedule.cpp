//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Functions and data pertaining to the monsters' AI scheduling system.
//			Implements default monster tasks and schedules.
//
//=============================================================================//

#include "cbase.h"
#include "basemonster.h"
#include "animation.h"
#include "scripted.h"
#include "ai_task.h"
#include "nodes.h"
#include "ai_default.h"
#include "soundent.h"
#include "ai_memory.h"

extern CGraph WorldGraph;

#define MAX_TASKS_RUN 10

//=========================================================
// FHaveSchedule - Returns true if monster's m_pSchedule
// is anything other than NULL.
//=========================================================
bool CBaseMonster :: FHaveSchedule( void )
{
	if ( GetCurSchedule() == NULL )
	{
		return false;
	}

	return true;
}

//=========================================================
// ClearSchedule - blanks out the caller's schedule pointer
// and index.
//=========================================================
void CBaseMonster :: ClearSchedule( void )
{
	m_ScheduleState.timeCurTaskStarted = m_ScheduleState.timeStarted = 0;
	m_ScheduleState.bScheduleWasInterrupted = true;
	SetTaskStatus( TASKSTATUS_NEW );
	m_pSchedule = NULL;
	ResetScheduleCurTaskIndex();
	m_InverseIgnoreConditions.SetAll();
}

//=========================================================
// FScheduleDone - Returns TRUE if the caller is on the
// last task in the schedule
//=========================================================
bool CBaseMonster :: FScheduleDone ( void )
{
	Assert( GetCurSchedule() != NULL );
	
	if ( GetScheduleCurTaskIndex() == GetCurSchedule()->NumTasks() )
	{
		return true;
	}

	return false;
}

//=========================================================
// SetSchedule - replaces the monster's schedule pointer
// with the passed pointer, and sets the ScheduleIndex back
// to 0
//=========================================================
void CBaseMonster::SetSchedule ( CAI_Schedule *pNewSchedule )
{
	Assert( pNewSchedule != NULL );

	m_ScheduleState.timeCurTaskStarted = m_ScheduleState.timeStarted = gpGlobals->time;
	m_ScheduleState.bScheduleWasInterrupted = false;

	m_pSchedule			= pNewSchedule;
	ResetScheduleCurTaskIndex();
	SetTaskStatus( TASKSTATUS_NEW );
	m_Conditions.ClearAll();// clear all of the conditions
	m_failSchedule		= SCHED_NONE;
	m_bConditionsGathered = false;
	RouteClear();
	m_InverseIgnoreConditions.SetAll();

// this is very useful code if you can isolate a test case in a level with a single monster. It will notify
// you of every schedule selection the monster makes.
#if 0
	if ( FClassnameIs( pev, "monster_human_grunt" ) )
	{
		const Task_t *pTask = GetTask();
		
		if ( pTask )
		{
			const char *pName = NULL;

			if ( GetCurSchedule() )
			{
				pName = GetCurSchedule()->GetName();
			}
			else
			{
				pName = "No Schedule";
			}
			
			if ( !pName )
			{
				pName = "Unknown";
			}

			ALERT( at_aiconsole, "%s: picked schedule %s\n", STRING( pev->classname ), pName );
		}
	}
#endif// 0

}

//=========================================================
// NextScheduledTask - increments the ScheduleIndex
//=========================================================
void CBaseMonster :: NextScheduledTask ( void )
{
	Assert( GetCurSchedule() != NULL );

	SetTaskStatus( TASKSTATUS_NEW );
	IncScheduleCurTaskIndex();

	if ( FScheduleDone() )
	{
		// just completed last task in schedule, so make it invalid by clearing it.
		SetCondition( COND_SCHEDULE_DONE );
	}
}

//-----------------------------------------------------------------------------
// Purpose: This function allows monsters to modify the interrupt mask for the
//			current schedule. This enables them to use base schedules but with
//			different interrupt conditions. Implement this function in your
//			derived class, and Set or Clear condition bits as you please.
//
//			NOTE: Always call the base class in your implementation, but be
//				  aware of the difference between changing the bits before vs.
//				  changing the bits after calling the base implementation.
//
// Input  : pBitString - Receives the updated interrupt mask.
//-----------------------------------------------------------------------------
void CBaseMonster::BuildScheduleTestBits( void )
{
	//NOTENOTE: Always defined in the leaf classes
}

//=========================================================
// FScheduleValid - returns true as long as the current
// schedule is still the proper schedule to be executing,
// taking into account all conditions
//=========================================================
bool CBaseMonster :: FScheduleValid ( void )
{
	if ( GetCurSchedule() == NULL || GetCurSchedule()->NumTasks() == 0 )
	{
		// schedule is empty, and therefore not valid.
		return false;
	}

	//Start out with the base schedule's set interrupt conditions
	GetCurSchedule()->GetInterruptMask( &m_CustomInterruptConditions );

	// Let the leaf class modify our interrupt test bits, but:
	// - Don't allow any modifications when scripted
	// - Don't modify interrupts for Schedules that set the COND_NO_CUSTOM_INTERRUPTS bit.
	if ( m_MonsterState != MONSTERSTATE_SCRIPT && !m_CustomInterruptConditions.IsBitSet( COND_NO_CUSTOM_INTERRUPTS ) )
	{
		BuildScheduleTestBits();
	}

	// This is like: m_CustomInterruptConditions &= m_Conditions;
	CAI_ScheduleBits testBits;
	m_CustomInterruptConditions.And( m_Conditions, &testBits  );

	if ( !testBits.IsAllClear() ) 
		return false;

	if ( HasCondition( COND_SCHEDULE_DONE ) || HasCondition( COND_TASK_FAILED ) )
	{
#ifdef DEBUG
		if ( HasCondition( COND_TASK_FAILED ) && m_failSchedule == SCHED_NONE )
		{
			// fail! Send a visual indicator.
			ALERT ( at_aiconsole, "Schedule: %s Failed\n", GetCurSchedule()->GetName() );

			Vector tmp = pev->origin;
			tmp.z = pev->absmax.z + 16;
			UTIL_Sparks( tmp );
		}
#endif // DEBUG

		// some condition has interrupted the schedule, or the schedule is done
		return false;
	}
	
	return true;
}


//=========================================================
// ShouldSelectIdealState - Determines whether or not SelectIdealState() 
// should be called before a monster selects a new schedule. 
//
//	NOTE: This logic was a source of pure, distilled trouble in Half-Life.
//	If you change this function, please supply good comments.
//
//=========================================================
bool CBaseMonster :: ShouldSelectIdealState ( void )
{
/*

	HERE's the old Half-Life code that used to control this.

	if ( m_IdealMonsterState != MONSTERSTATE_DEAD && 
		(m_IdealMonsterState != MONSTERSTATE_SCRIPT || m_IdealMonsterState == m_MonsterState) )
	{
		if (	(m_afConditions && !HasConditions(bits_COND_SCHEDULE_DONE)) ||
				(GetCurSchedule() && (GetCurSchedule()->iInterruptMask & bits_COND_SCHEDULE_DONE)) ||
				((m_MonsterState == MONSTERSTATE_COMBAT) && (m_hEnemy == NULL))	)
		{
			SelectIdealState();
		}
	}
*/
	
	if ( m_IdealMonsterState == MONSTERSTATE_DEAD )
	{
		// Don't get ideal state if you are supposed to be dead.
		return false;
	}

	if ( (m_IdealMonsterState == MONSTERSTATE_SCRIPT) && (m_MonsterState != MONSTERSTATE_SCRIPT) )
	{
		// If I'm supposed to be in scripted state, but i'm not yet, do not allow 
		// SelectIdealState() to be called, because it doesn't know how to determine 
		// that a monster should be in SCRIPT state and will stomp it with some other 
		// state. (Most likely ALERT)
		return false;
	}

	if ( !HasCondition( COND_SCHEDULE_DONE ) )
	{
		// If the NPC has any current conditions, and one of those conditions indicates
		// that the previous schedule completed successfully, then don't run SelectIdealState(). 
		// Paths between states only exist for interrupted schedules, or when a schedule 
		// contains a task that suggests that the NPC change state.
		return true;
	}

	if ( GetCurSchedule() && GetCurSchedule()->HasInterrupt( COND_SCHEDULE_DONE ) )
	{
		// This seems like some sort of hack...
		// Currently no schedule that I can see in the AI uses this feature, but if a schedule
		// interrupt mask contains bits_COND_SCHEDULE_DONE, then force a call to SelectIdealState().
		// If we want to keep this feature, I suggest we create a new condition with a name that
		// indicates exactly what it does. 
		return true;
	}

	if ( (m_MonsterState == MONSTERSTATE_COMBAT) && (GetEnemy() == NULL) )
	{
		// Don't call SelectIdealState if a NPC in combat state has a valid enemy handle. Otherwise,
		// we need to change state immediately because something unexpected happened to the enemy 
		// entity (it was blown apart by someone else, for example), and we need the NPC to change
		// state. THE REST OF OUR CODE should be robust enough that this can go away!!
		return true;
	}

	return false;
}

//=========================================================
// GetNewSchedule
//
// Returns a new schedule based on current condition bits.
//=========================================================
CAI_Schedule *CBaseMonster :: GetNewSchedule( void )
{
	int scheduleType;

	//
	// Schedule selection code here overrides all leaf schedule selection.
	//

	// I dunno how this trend got started, but we need to find the problem.
	// You may not be in combat state with no enemy!!! (sjb) 11/4/03
	if ( m_MonsterState == MONSTERSTATE_COMBAT && !GetEnemy() )
	{
		ALERT( at_error, "Combat State with no enemy! slamming to ALERT\n" );
		SetState( MONSTERSTATE_ALERT );
	}

	if ( m_MonsterState == MONSTERSTATE_SCRIPT || m_MonsterState == MONSTERSTATE_DEAD )
	{
		scheduleType = CBaseMonster::SelectSchedule();
	}
	else
	{
		scheduleType = SelectSchedule();
	}

	return GetScheduleOfType( scheduleType );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
CAI_Schedule *CBaseMonster :: GetFailSchedule( void )
{
	int prevSchedule;
	int failedTask;

	if ( GetCurSchedule() != NULL )
		prevSchedule = GetLocalScheduleId( GetCurSchedule()->GetId() );
	else
		prevSchedule = SCHED_NONE;
		
	const Task_t *pTask = GetTask();
	if ( pTask )
		failedTask = pTask->iTask;
	else
		failedTask = TASK_INVALID;

	Assert( AI_IdIsLocal( prevSchedule ) );
	Assert( AI_IdIsLocal( failedTask ) );

	int scheduleType = SelectFailSchedule( prevSchedule, failedTask );
	return GetScheduleOfType( scheduleType );
}

//=========================================================
// MaintainSchedule - does all the per-think schedule maintenance.
// ensures that the monster leaves this function with a valid
// schedule!
//=========================================================
void CBaseMonster::MaintainSchedule ( void )
{
	CAI_Schedule	*pNewSchedule;
	int			i;
	bool		runTask = true;

	// UNDONE: Tune/fix this MAX_TASKS_RUN... This is just here so infinite loops are impossible
	for ( i = 0; i < MAX_TASKS_RUN; i++ )
	{
		if ( GetCurSchedule() != NULL && TaskIsComplete() )
		{
			// Schedule is valid, so advance to the next task if the current is complete.
			NextScheduledTask();     

			// If we finished the current schedule, clear our ignored conditions so they
			// aren't applied to the next schedule selection.
			if ( HasCondition( COND_SCHEDULE_DONE ) )
			{
				// Put our conditions back the way they were after GatherConditions,
				// but add in COND_SCHEDULE_DONE.
				m_Conditions = m_ConditionsPreIgnore;
				SetCondition( COND_SCHEDULE_DONE );

				m_InverseIgnoreConditions.SetAll();
			}

			// --------------------------------------------------------
			//	If debug stepping advance when I complete a task
			// --------------------------------------------------------
			if (CBaseMonster::m_nDebugBits & bits_debugStepAI)
			{
				m_nDebugCurIndex++;
				return;
			}
		}

		// validate existing schedule 
		if ( !FScheduleValid() || m_MonsterState != m_IdealMonsterState )
		{
			// if we come into this block of code, the schedule is going to have to be changed.
			// if the previous schedule was interrupted by a condition, SelectIdealState will be 
			// called. Else, a schedule finished normally.

			// Notify the monster that his schedule is changing
			m_ScheduleState.bScheduleWasInterrupted = true;
			OnScheduleChange();

			if ( !m_bConditionsGathered || m_bSkippedChooseEnemy )
			{
				// occurs if a schedule is exhausted within one think
				GatherConditions();
			}

			if ( ShouldSelectIdealState() )
			{
				MONSTERSTATE eIdealState = SelectIdealState();
				m_IdealMonsterState = eIdealState;
			}
		
			if ( HasCondition( COND_TASK_FAILED ) && m_MonsterState == m_IdealMonsterState )
			{
				// Get a fail schedule if the previous schedule failed during execution and 
				// the monster is still in its ideal state. Otherwise, the monster would immediately
				// select the same schedule again and fail again.
				pNewSchedule = GetFailSchedule();
				// schedule was invalid because the current task failed to start or complete
				ALERT ( at_aiconsole, "Schedule (%s) Failed at %d!\n", GetCurSchedule() ? GetCurSchedule()->GetName() : "GetCurSchedule() == NULL", GetScheduleCurTaskIndex() );
				SetSchedule( pNewSchedule );
			}
			else
			{
				// If the monster is supposed to change state, it doesn't matter if the previous
				// schedule failed or completed. Changing state means selecting an entirely new schedule.
				SetState( m_IdealMonsterState );

				pNewSchedule = GetNewSchedule();

				SetSchedule( pNewSchedule );
			}
		}

		if (!GetCurSchedule())
		{
			pNewSchedule = GetNewSchedule();
			
			if (pNewSchedule)
			{
				SetSchedule( pNewSchedule );
			}
		}

		if ( !GetCurSchedule() || GetCurSchedule()->NumTasks() == 0 )
		{
			ALERT ( at_error, "Missing or invalid schedule!\n" );
			SetActivity ( ACT_IDLE );
			return;
		}

		if ( GetTaskStatus() == TASKSTATUS_NEW )
		{	
			if ( GetScheduleCurTaskIndex() == 0 )
			{
				OnScheduleStart( GetLocalScheduleId( GetCurSchedule()->GetId() ) );
			}

			const Task_t *pTask = GetTask();
			Assert( pTask != NULL );
			
			OnStartTask();

			m_ScheduleState.timeCurTaskStarted = gpGlobals->time;

			StartTask( pTask );
		}

		// UNDONE: Twice?!!!
		MaintainActivity();
		
		if ( !TaskIsComplete() && GetTaskStatus() != TASKSTATUS_NEW )
		{
			if ( TaskIsRunning() && !HasCondition( COND_TASK_FAILED ) && runTask )
			{
				const Task_t *pTask = GetTask();
				Assert( pTask != NULL );
				RunTask( pTask );

				// don't do this again this frame
				// FIXME: RunTask() should eat some of the clock, depending on what it has done
				// runTask = false;

				if ( !TaskIsComplete() )
					break;
			}
			else
			{
				break;
			}

		}
	}

	// UNDONE: We have to do this so that we have an animation set to blend to if RunTask changes the animation
	// RunTask() will always change animations at the end of a script!
	// Don't do this twice
	MaintainActivity();

	// --------------------------------------------------------
	//	If I'm stopping to debug step, don't animate unless
	//  I'm in motion
	// --------------------------------------------------------
	if (CBaseMonster::m_nDebugBits & bits_debugStepAI)
	{
		if ( FRouteClear() && m_nDebugCurIndex >= CBaseMonster::m_nDebugPauseIndex )
		{
			pev->framerate = 0;
		}
	}
}

//=========================================================
// RunTask 
//=========================================================
void CBaseMonster :: RunTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_TURN_RIGHT:
	case TASK_TURN_LEFT:
		{
			ChangeYaw( pev->yaw_speed );

			if ( FacingIdeal() )
			{
				TaskComplete();
			}
			break;
		}

	case TASK_PLAY_SEQUENCE_FACE_ENEMY:
	case TASK_PLAY_SEQUENCE_FACE_TARGET:
		{
			CBaseEntity *pTarget;

			if ( pTask->iTask == TASK_PLAY_SEQUENCE_FACE_TARGET )
				pTarget = m_hTargetEnt;
			else
				pTarget = m_hEnemy;
			if ( pTarget )
			{
				pev->ideal_yaw = UTIL_VecToYaw( pTarget->pev->origin - pev->origin );
				ChangeYaw( pev->yaw_speed );
			}
			if ( IsSequenceFinished() )
				TaskComplete();
		}
		break;

	case TASK_PLAY_SEQUENCE:
	case TASK_PLAY_HINT_ACTIVITY:
		{
			if ( IsSequenceFinished() )
			{
				TaskComplete();
			}
			break;
		}


	case TASK_FACE_ENEMY:
		{
			Vector vecEnemyLKP = GetEnemyLKP();
			MakeIdealYaw( vecEnemyLKP );

			ChangeYaw( pev->yaw_speed );

			if ( FacingIdeal() )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_FACE_HINTNODE:
	case TASK_FACE_LASTPOSITION:
	case TASK_FACE_TARGET:
	case TASK_FACE_IDEAL:
	case TASK_FACE_SCRIPT:
	case TASK_FACE_ROUTE:
		{
			ChangeYaw( pev->yaw_speed );

			if ( FacingIdeal() )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_WAIT_PVS:
		{
			if ( ShouldAlwaysThink() || !FNullEnt(FIND_CLIENT_IN_PVS(edict())) || 
				( GetState() == MONSTERSTATE_COMBAT && GetEnemy() && gpGlobals->time - GetEnemies()->LastTimeSeen( GetEnemy() ) < 15 ))
			{
				TaskComplete();
			}
			break;
		}
	case TASK_WAIT_INDEFINITE:
		{
			// don't do anything.
			break;
		}
	case TASK_WAIT:
	case TASK_WAIT_RANDOM:
		{
			if ( gpGlobals->time >= m_flWaitFinished )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_WAIT_FACE_ENEMY:
		{
			Vector vecEnemyLKP = GetEnemyLKP();
			if (!FInAimCone( vecEnemyLKP ))
			{
				MakeIdealYaw ( vecEnemyLKP );
				ChangeYaw( pev->yaw_speed ); 
			}
			
			if ( gpGlobals->time >= m_flWaitFinished )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_MOVE_TO_TARGET_RANGE:
		{
			float distance;

			if ( m_hTargetEnt == NULL )
				TaskFail();
			else
			{
				distance = ( m_vecMoveGoal - pev->origin ).Length2D();
				// Re-evaluate when you think your finished, or the target has moved too far
				if ( (distance < pTask->flData) || (m_vecMoveGoal - m_hTargetEnt->pev->origin).Length() > pTask->flData * 0.5 )
				{
					m_vecMoveGoal = m_hTargetEnt->pev->origin;
					distance = ( m_vecMoveGoal - pev->origin ).Length2D();
					FRefreshRoute();
				}

				// Set the appropriate activity based on an overlapping range
				// overlap the range to prevent oscillation
				if ( distance < pTask->flData )
				{
					TaskComplete();
					RouteClear();		// Stop moving
				}
				else if ( distance < 190 && m_movementActivity != ACT_WALK )
					m_movementActivity = ACT_WALK;
				else if ( distance >= 270 && m_movementActivity != ACT_RUN )
					m_movementActivity = ACT_RUN;
			}

			break;
		}
	case TASK_WAIT_FOR_MOVEMENT:
		{
			if (MovementIsComplete())
			{
				TaskComplete();
				RouteClear();		// Stop moving
			}
			break;
		}
	case TASK_DIE:
		{
			if ( IsSequenceFinished() && pev->frame >= 255 )
			{
				pev->deadflag = DEAD_DEAD;
				
				SetThink ( NULL );
				StopAnimation();

				if ( !BBoxFlat() )
				{
					// a bit of a hack. If a corpses' bbox is positioned such that being left solid so that it can be attacked will
					// block the player on a slope or stairs, the corpse is made nonsolid. 
//					pev->solid = SOLID_NOT;
					UTIL_SetSize ( pev, Vector ( -4, -4, 0 ), Vector ( 4, 4, 1 ) );
				}
				else // !!!HACKHACK - put monster in a thin, wide bounding box until we fix the solid type/bounding volume problem
					UTIL_SetSize ( pev, Vector ( pev->mins.x, pev->mins.y, pev->mins.z ), Vector ( pev->maxs.x, pev->maxs.y, pev->mins.z + 1 ) );

				if ( ShouldFadeOnDeath() )
				{
					// this monster was created by a monstermaker... fade the corpse out.
					SUB_StartFadeOut();
				}
				else
				{
					// body is gonna be around for a while, so have it stink for a bit.
					CSoundEnt::InsertSound ( bits_SOUND_CARCASS, pev->origin, 384, 30 );
				}
			}
			break;
		}
	case TASK_RANGE_ATTACK1_NOTURN:
	case TASK_MELEE_ATTACK1_NOTURN:
	case TASK_MELEE_ATTACK2_NOTURN:
	case TASK_RANGE_ATTACK2_NOTURN:
	case TASK_RELOAD_NOTURN:
		{
			if ( IsSequenceFinished() )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_RANGE_ATTACK1:
	case TASK_MELEE_ATTACK1:
	case TASK_MELEE_ATTACK2:
	case TASK_RANGE_ATTACK2:
	case TASK_SPECIAL_ATTACK1:
	case TASK_SPECIAL_ATTACK2:
	case TASK_RELOAD:
		{
			Vector vecEnemyLKP = GetEnemyLKP();

			// If our enemy was killed, but I'm not done animating, the last known position comes
			// back as the origin and makes the me face the world origin if my attack schedule
			// doesn't break when my enemy dies. (sjb)
			if ( vecEnemyLKP != vec3_origin )
			{
				MakeIdealYaw ( vecEnemyLKP );
				ChangeYaw ( pev->yaw_speed );
			}

			if ( IsSequenceFinished() )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_SMALL_FLINCH:
	case TASK_BIG_FLINCH:
		{
			if ( IsSequenceFinished() )
			{
				TaskComplete();
			}
			break;
		}
	case TASK_WAIT_FOR_SCRIPT:
		{
			if ( m_pCine->m_iDelay <= 0 && gpGlobals->time >= m_pCine->m_startTime )
			{
				TaskComplete();
				m_pCine->StartSequence( (CBaseMonster *)this, m_pCine->m_iszPlay, TRUE );
				if ( IsSequenceFinished() )
					ClearSchedule();
				pev->framerate = 1.0;
				//ALERT( at_aiconsole, "Script %s has begun for %s\n", STRING( m_pCine->m_iszPlay ), STRING(pev->classname) );
			}
			else if (!m_pCine)
			{
				ALERT( at_console, "Cine died!\n");
				TaskComplete();
			}
			break;
		}
	case TASK_PLAY_SCRIPT:
		{
			if ( IsSequenceFinished() )
			{
				m_pCine->SequenceDone( this );

				//TaskComplete();
			}
			break;
		}
	}
}

//=========================================================
// SetTurnActivity - measures the difference between the way
// the monster is facing and determines whether or not to
// select one of the 180 turn animations.
//=========================================================
void CBaseMonster::SetTurnActivity ( void )
{
	float flYD;
	flYD = FlYawDiff();

	if ( flYD <= -45 && LookupActivity ( ACT_TURN_RIGHT ) != ACTIVITY_NOT_AVAILABLE )
	{// big right turn
		m_IdealActivity = ACT_TURN_RIGHT;
		return;
	}
	if ( flYD > 45 && LookupActivity ( ACT_TURN_LEFT ) != ACTIVITY_NOT_AVAILABLE )
	{// big left turn
		m_IdealActivity = ACT_TURN_LEFT;
		return;
	}

	m_IdealActivity = ACT_IDLE;	// failure case
}

//=========================================================
// ResetIdealActivity - For non-looping animations 
// that may be replayed sequentially (like attacks)
// Set the activity to ACT_RESET if this is a replay, 
// otherwise just set ideal activity
//=========================================================
void CBaseMonster :: ResetIdealActivity ( Activity newIdealActivity )
{
	if ( m_Activity == newIdealActivity )
	{
		m_Activity = ACT_RESET;
	}

	m_IdealActivity = newIdealActivity;
}

//=========================================================
// Start task - selects the correct activity and performs
// any necessary calculations to start the next task on the
// schedule. 
//=========================================================
void CBaseMonster :: StartTask ( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_TURN_RIGHT:
		{
			float flCurrentYaw;
			
			flCurrentYaw = UTIL_AngleMod( pev->angles.y );
			pev->ideal_yaw = UTIL_AngleMod( flCurrentYaw - pTask->flData );
			SetTurnActivity();
			break;
		}
	case TASK_TURN_LEFT:
		{
			float flCurrentYaw;
			
			flCurrentYaw = UTIL_AngleMod( pev->angles.y );
			pev->ideal_yaw = UTIL_AngleMod( flCurrentYaw + pTask->flData );
			SetTurnActivity();
			break;
		}
	case TASK_REMEMBER:
		{
			Remember ( (int)pTask->flData );
			TaskComplete();
			break;
		}
	case TASK_FORGET:
		{
			Forget ( (int)pTask->flData );
			TaskComplete();
			break;
		}
	case TASK_FIND_HINTNODE:
		{
			m_iHintNode = FindHintNode();

			if ( m_iHintNode != NO_NODE )
			{
				TaskComplete();
			}
			else
			{
				TaskFail();
			}
			break;
		}
	case TASK_STORE_LASTPOSITION:
		{
			m_vecLastPosition = pev->origin;
			TaskComplete();
			break;
		}
	case TASK_CLEAR_LASTPOSITION:
		{
			m_vecLastPosition = vec3_origin;
			TaskComplete();
			break;
		}
	case TASK_CLEAR_HINTNODE:
		{
			m_iHintNode = NO_NODE;
			TaskComplete();
			break;
		}
	case TASK_STOP_MOVING:
		{
			if ( m_IdealActivity == m_movementActivity )
			{
				m_IdealActivity = GetStoppedActivity();
			}

			RouteClear();
			TaskComplete();
			break;
		}
	case TASK_PLAY_SEQUENCE_FACE_ENEMY:
	case TASK_PLAY_SEQUENCE_FACE_TARGET:
	case TASK_PLAY_SEQUENCE:
		{
			m_IdealActivity = ( Activity )( int )pTask->flData;
			break;
		}
	case TASK_PLAY_HINT_ACTIVITY:
		{
			// monsters verify that they have a sequence for the node's activity BEFORE
			// moving towards the node, so it's ok to just set the activity without checking here.
			m_IdealActivity = ( Activity )WorldGraph.m_pNodes[ m_iHintNode ].m_sHintActivity;
			break;
		}
	case TASK_SET_SCHEDULE:
		{
			CAI_Schedule *pNewSchedule;

			pNewSchedule = GetScheduleOfType( (int)pTask->flData );
			
			if ( pNewSchedule )
			{
				SetSchedule( pNewSchedule );
			}
			else
			{
				TaskFail();
			}

			break;
		}
	case TASK_FIND_NEAR_NODE_COVER_FROM_ENEMY:
		{
			if ( m_hEnemy == NULL )
			{
				TaskFail();
				return;
			}

			if ( FindCover( m_hEnemy->pev->origin, m_hEnemy->pev->view_ofs, 0, pTask->flData ) )
			{
				// try for cover farther than the FLData from the schedule.
				TaskComplete();
			}
			else
			{
				// no coverwhatsoever.
				TaskFail();
			}
			break;
		}
	case TASK_FIND_FAR_NODE_COVER_FROM_ENEMY:
		{
			if ( m_hEnemy == NULL )
			{
				TaskFail();
				return;
			}

			if ( FindCover( m_hEnemy->pev->origin, m_hEnemy->pev->view_ofs, pTask->flData, CoverRadius() ) )
			{
				// try for cover farther than the FLData from the schedule.
				TaskComplete();
			}
			else
			{
				// no coverwhatsoever.
				TaskFail();
			}
			break;
		}
	case TASK_FIND_NODE_COVER_FROM_ENEMY:
		{
			if ( m_hEnemy == NULL )
			{
				TaskFail();
				return;
			}

			if ( FindCover( m_hEnemy->pev->origin, m_hEnemy->pev->view_ofs, 0, CoverRadius() ) )
			{
				// try for cover farther than the FLData from the schedule.
				TaskComplete();
			}
			else
			{
				// no coverwhatsoever.
				TaskFail();
			}
			break;
		}
	case TASK_FIND_COVER_FROM_ENEMY:
		{
			CBaseEntity *pEntity = GetEnemy();

			if ( pEntity == NULL )
			{
				// Find cover from self if no enemy available
				pEntity = this;
			}

			if ( FindLateralCover( pEntity->EyePosition() ) )
			{
				// try lateral first
				m_flMoveWaitFinished = gpGlobals->time + pTask->flData;
				TaskComplete();
			}
			else if ( FindCover( pEntity->pev->origin, pEntity->pev->view_ofs, 0, CoverRadius() ) )
			{
				// then try for plain ole cover
				m_flMoveWaitFinished = gpGlobals->time + pTask->flData;
				TaskComplete();
			}
			else
			{
				// no coverwhatsoever.
				TaskFail();
			}
			break;
		}
	case TASK_FIND_LATERAL_COVER_FROM_ENEMY:
		{
			CBaseEntity *pEntity = GetEnemy();

			if ( pEntity == NULL )
			{
				// Find cover from self if no enemy available
				pEntity = this;
			}

			if ( FindLateralCover( pEntity->EyePosition() ) )
			{
				// try lateral first
				m_flMoveWaitFinished = gpGlobals->time + pTask->flData;
				TaskComplete();
			}
			else
			{
				// no coverwhatsoever.
				TaskFail();
			}
			break;
		}
	case TASK_FIND_COVER_FROM_ORIGIN:
		{
			if ( FindCover( pev->origin, pev->view_ofs, 0, CoverRadius() ) )
			{
				// then try for plain ole cover
				m_flMoveWaitFinished = gpGlobals->time + pTask->flData;
				TaskComplete();
			}
			else
			{
				// no cover!
				TaskFail();
			}
		}
		break;
	case TASK_FIND_COVER_FROM_BEST_SOUND:
		{
			CSound *pBestSound;

			pBestSound = GetBestSound();

			ASSERT( pBestSound != NULL );
			/*
			if ( pBestSound && FindLateralCover( pBestSound->m_vecOrigin, vec3_origin ) )
			{
				// try lateral first
				m_flMoveWaitFinished = gpGlobals->time + pTask->flData;
				TaskComplete();
			}
			*/

			if ( pBestSound && FindCover( pBestSound->m_vecOrigin, vec3_origin, pBestSound->m_iVolume, CoverRadius() ) )
			{
				// then try for plain ole cover
				m_flMoveWaitFinished = gpGlobals->time + pTask->flData;
				TaskComplete();
			}
			else
			{
				// no coverwhatsoever. or no sound in list
				TaskFail();
			}
			break;
		}
	case TASK_FACE_HINTNODE:
		{
			pev->ideal_yaw = WorldGraph.m_pNodes[ m_iHintNode ].m_flHintYaw;
			SetTurnActivity();
			break;
		}
	
	case TASK_FACE_LASTPOSITION:
		MakeIdealYaw ( m_vecLastPosition );
		SetTurnActivity(); 
		break;

	case TASK_FACE_TARGET:
		if ( m_hTargetEnt != NULL )
		{
			MakeIdealYaw ( m_hTargetEnt->pev->origin );
			SetTurnActivity(); 
		}
		else
			TaskFail();
		break;
	case TASK_FACE_ENEMY:
		{
			Vector vecEnemyLKP = GetEnemyLKP();
			if (!FInAimCone( vecEnemyLKP ))
			{
				MakeIdealYaw ( vecEnemyLKP );
				SetTurnActivity(); 
			}
			else
			{
				TaskComplete();
			}
			break;
		}
	case TASK_FACE_IDEAL:
		{
			SetTurnActivity();
			break;
		}
	case TASK_FACE_ROUTE:
		{
			if (FRouteClear())
			{
				ALERT(at_aiconsole, "No route to face!\n");
				TaskFail();
			}
			else
			{
				const float NPC_TRIVIAL_TURN = 15;	// (Degrees). Turns this small or smaller, don't bother with a transition.

				MakeIdealYaw(m_Route[m_iRouteIndex].vecLocation);

				if( fabs( FlYawDiff() ) <= NPC_TRIVIAL_TURN )
				{
					// This character is already facing the path well enough that 
					// moving will look fairly natural. Don't bother with a transitional
					// turn animation.
					TaskComplete();
					break;
				}

				SetTurnActivity();
			}
			break;
		}
	case TASK_WAIT_PVS:
	case TASK_WAIT_INDEFINITE:
		{
			// don't do anything.
			break;
		}
	case TASK_WAIT:
	case TASK_WAIT_FACE_ENEMY:
		{// set a future time that tells us when the wait is over.
			m_flWaitFinished = gpGlobals->time + pTask->flData;	
			break;
		}
	case TASK_WAIT_RANDOM:
		{// set a future time that tells us when the wait is over.
			m_flWaitFinished = gpGlobals->time + RANDOM_FLOAT( 0.1, pTask->flData );
			break;
		}
	case TASK_MOVE_TO_TARGET_RANGE:
		{
			if ( m_hTargetEnt == NULL)
			{
				TaskFail();
			}
			if ( (m_hTargetEnt->pev->origin - pev->origin).Length() < 1 )
			{
				TaskComplete();
			}
			else
			{
				m_vecMoveGoal = m_hTargetEnt->pev->origin;
				if ( !MoveToTarget( ACT_WALK, 2 ) )
					TaskFail();
			}
			break;
		}
	case TASK_RUN_TO_TARGET:
	case TASK_WALK_TO_TARGET:
		{
			Activity newActivity;

			if ( m_hTargetEnt == NULL )
			{
				TaskFail();
			}
			if ( (m_hTargetEnt->pev->origin - pev->origin).Length() < 1 )
			{
				TaskComplete();
			}
			else
			{
				//
				// Select the appropriate activity.
				//
				if ( pTask->iTask == TASK_WALK_TO_TARGET )
				{
					newActivity = ACT_WALK;
				}
				else
				{
					newActivity = ACT_RUN;
				}

				if ( LookupActivity( newActivity ) == ACT_INVALID )
				{
					Assert( 0 );
					// This monster can't do this!
					TaskComplete();
				}
				else
				{
					if ( m_hTargetEnt == NULL )
					{
						TaskFail();
					}
					else
					{
						if ( !MoveToTarget( newActivity, 2 ) )
						{
							TaskFail();
							ALERT( at_aiconsole, "%s Failed to reach target!!!\n", STRING(pev->classname) );
							RouteClear();
						}
					}
				}
			}

			TaskComplete();
			break;
		}
	case TASK_CLEAR_MOVE_WAIT:
		{
			m_flMoveWaitFinished = gpGlobals->time;
			TaskComplete();
			break;
		}
	case TASK_MELEE_ATTACK1_NOTURN:
	case TASK_MELEE_ATTACK1:
		{
			ResetIdealActivity( ACT_MELEE_ATTACK1 );
			break;
		}
	case TASK_MELEE_ATTACK2_NOTURN:
	case TASK_MELEE_ATTACK2:
		{
			ResetIdealActivity( ACT_MELEE_ATTACK2 );
			break;
		}
	case TASK_RANGE_ATTACK1_NOTURN:
	case TASK_RANGE_ATTACK1:
		{
			ResetIdealActivity( ACT_RANGE_ATTACK1 );
			break;
		}
	case TASK_RANGE_ATTACK2_NOTURN:
	case TASK_RANGE_ATTACK2:
		{
			ResetIdealActivity( ACT_RANGE_ATTACK2 );
			break;
		}
	case TASK_RELOAD_NOTURN:
	case TASK_RELOAD:
		{
			ResetIdealActivity( ACT_RELOAD );
			break;
		}
	case TASK_SPECIAL_ATTACK1:
		{
			ResetIdealActivity( ACT_SPECIAL_ATTACK1 );
			break;
		}
	case TASK_SPECIAL_ATTACK2:
		{
			ResetIdealActivity( ACT_SPECIAL_ATTACK2 );
			break;
		}
	case TASK_SET_ACTIVITY:
		{
			Activity goalActivity = (Activity)(int)pTask->flData;
			if (goalActivity != ACT_RESET)
			{
				m_IdealActivity = goalActivity;
			}
			else
			{
				m_Activity = ACT_RESET;
			}
			TaskComplete();
			break;
		}
	case TASK_GET_PATH_TO_ENEMY_LKP:
		{
			CBaseEntity *pEnemy = m_hEnemy;
			if ( !pEnemy || IsUnreachable(pEnemy) )
			{
				TaskFail();
				return;
			}

			Vector vecEnemyLKP = GetEnemyLKP();

			if ( BuildRoute ( vecEnemyLKP, bits_MF_TO_LOCATION, NULL ) )
			{
				TaskComplete();
			}
			else if (BuildNearestRoute( vecEnemyLKP, pev->view_ofs, 0, (vecEnemyLKP - pev->origin).Length() ))
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToEnemyLKP failed!!\n" );
				RememberUnreachable( pEnemy );
				TaskFail();
			}
			break;
		}
	case TASK_GET_PATH_TO_ENEMY:
		{
			CBaseEntity *pEnemy = m_hEnemy;

			if ( pEnemy == NULL )
			{
				TaskFail();
				return;
			}

			if ( IsUnreachable( pEnemy ) )
			{
				TaskFail();
				return;
			}

			if ( BuildRoute ( pEnemy->pev->origin, bits_MF_TO_ENEMY, pEnemy ) )
			{
				TaskComplete();
			}
			else if (BuildNearestRoute( pEnemy->pev->origin, pEnemy->pev->view_ofs, 0, (pEnemy->pev->origin - pev->origin).Length() ))
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToEnemy failed!!\n" );
				RememberUnreachable( pEnemy );
				TaskFail();
			}
			break;
		}
	case TASK_GET_PATH_TO_ENEMY_CORPSE:
		{
			Vector forward;
			AngleVectors( pev->angles, &forward );
			Vector vecEnemyLKP = GetEnemyLKP();

			if ( BuildRoute ( vecEnemyLKP - forward * 64, bits_MF_TO_LOCATION, NULL ) )
			{
				TaskComplete();
			}
			else
			{
				ALERT ( at_aiconsole, "GetPathToEnemyCorpse failed!!\n" );
				TaskFail();
			}
		}
		break;
	case TASK_GET_PATH_TO_SPOT:
		{
			CBaseEntity *pPlayer = CBaseEntity::Instance( FIND_ENTITY_BY_CLASSNAME( NULL, "player" ) );
			if ( BuildRoute ( m_vecMoveGoal, bits_MF_TO_LOCATION, pPlayer ) )
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToSpot failed!!\n" );
				TaskFail();
			}
			break;
		}

	case TASK_GET_PATH_TO_TARGET:
		{
			RouteClear();
			if ( m_hTargetEnt != NULL && MoveToTarget( m_movementActivity, 1 ) )
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToSpot failed!!\n" );
				TaskFail();
			}
			break;
		}
	case TASK_GET_PATH_TO_HINTNODE:// for active idles!
		{
			if ( MoveToLocation( m_movementActivity, 2, WorldGraph.m_pNodes[ m_iHintNode ].m_vecOrigin ) )
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToHintNode failed!!\n" );
				TaskFail();
			}
			break;
		}
	case TASK_GET_PATH_TO_LASTPOSITION:
		{
			m_vecMoveGoal = m_vecLastPosition;

			if ( MoveToLocation( m_movementActivity, 2, m_vecMoveGoal ) )
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToLastPosition failed!!\n" );
				TaskFail();
			}
			break;
		}
	case TASK_GET_PATH_TO_BESTSOUND:
		{
			CSound *pSound;

			pSound = GetBestSound();

			if ( pSound && MoveToLocation( m_movementActivity, 2, pSound->m_vecOrigin ) )
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToBestSound failed!!\n" );
				TaskFail();
			}
			break;
		}
case TASK_GET_PATH_TO_BESTSCENT:
		{
			CSound *pScent;

			pScent = GetBestScent();

			if ( pScent && MoveToLocation( m_movementActivity, 2, pScent->m_vecOrigin ) )
			{
				TaskComplete();
			}
			else
			{
				// no way to get there =(
				ALERT ( at_aiconsole, "GetPathToBestScent failed!!\n" );
				
				TaskFail();
			}
			break;
		}
	case TASK_RUN_PATH:
		{
			// UNDONE: This is in some default AI and some monsters can't run? -- walk instead?
			if ( LookupActivity( ACT_RUN ) != ACTIVITY_NOT_AVAILABLE )
			{
				m_movementActivity = ACT_RUN;
			}
			else
			{
				m_movementActivity = ACT_WALK;
			}
			// Cover is void once I move
			Forget( bits_MEMORY_INCOVER );
			TaskComplete();
			break;
		}
	case TASK_WALK_PATH:
		{
			if ( pev->movetype == MOVETYPE_FLY )
			{
				m_movementActivity = ACT_FLY;
			}
			if ( LookupActivity( ACT_WALK ) != ACTIVITY_NOT_AVAILABLE )
			{
				m_movementActivity = ACT_WALK;
			}
			else
			{
				m_movementActivity = ACT_RUN;
			}
			// Cover is void once I move
			Forget( bits_MEMORY_INCOVER );
			TaskComplete();
			break;
		}
	case TASK_STRAFE_PATH:
		{
			Vector2D	vec2DirToPoint; 
			Vector2D	vec2RightSide;

			// to start strafing, we have to first figure out if the target is on the left side or right side
			Vector right;
			AngleVectors( pev->angles, NULL, &right, NULL );

			vec2DirToPoint = ( m_Route[m_iRouteIndex].vecLocation - pev->origin ).Make2D().Normalize();
			vec2RightSide = right.Make2D().Normalize();

			if ( DotProduct ( vec2DirToPoint, vec2RightSide ) > 0 )
			{
				// strafe right
				m_movementActivity = ACT_STRAFE_RIGHT;
			}
			else
			{
				// strafe left
				m_movementActivity = ACT_STRAFE_LEFT;
			}

			// Cover is void once I move
			Forget( bits_MEMORY_INCOVER );
			TaskComplete();
			break;
		}


	case TASK_WAIT_FOR_MOVEMENT:
		{
			if (FRouteClear())
			{
				TaskComplete();
			}
			break;
		}

	case TASK_EAT:
		{
			Eat( pTask->flData );
			TaskComplete();
			break;
		}
	case TASK_SMALL_FLINCH:
		{
			m_IdealActivity = GetFlinchActivity( false );
			break;
		}
	case TASK_BIG_FLINCH:
		{
			m_IdealActivity = GetFlinchActivity( true );
			break;
		}
	case TASK_DIE:
		{
			RouteClear();	
			
			m_IdealActivity = GetDeathActivity();

			pev->deadflag = DEAD_DYING;
			break;
		}
	case TASK_SOUND_WAKE:
		{
			AlertSound();
			TaskComplete();
			break;
		}
	case TASK_SOUND_DIE:
		{
			DeathSound();
			TaskComplete();
			break;
		}
	case TASK_SOUND_IDLE:
		{
			IdleSound();
			TaskComplete();
			break;
		}
	case TASK_SOUND_PAIN:
		{
			PainSound();
			TaskComplete();
			break;
		}
	case TASK_SOUND_ANGRY:
		{
			// sounds are complete as soon as we get here, cause we've already played them.
			ALERT ( at_aiconsole, "SOUND\n" );			
			TaskComplete();
			break;
		}
	case TASK_WAIT_FOR_SCRIPT:
		{

			// Start waiting to play a script. Play the script's idle animation if one
			// is specified, otherwise just go to our default idle activity.
			if (m_pCine->m_iszIdle)
			{
				m_pCine->StartSequence( (CBaseMonster *)this, m_pCine->m_iszIdle, FALSE );
				if (FStrEq( STRING(m_pCine->m_iszIdle), STRING(m_pCine->m_iszPlay)))
				{
					pev->framerate = 0;
				}
			}
			else
				m_IdealActivity = ACT_IDLE;

			break;
		}
	case TASK_PLAY_SCRIPT:
		{
			pev->movetype = MOVETYPE_FLY;
			ClearBits(pev->flags, FL_ONGROUND);

			//
			// Start playing a scripted sequence.
			//
			m_scriptState = SCRIPT_PLAYING;
			break;
		}
	case TASK_ENABLE_SCRIPT:
		{
			m_pCine->DelayStart( 0 );
			TaskComplete();
			break;
		}
	case TASK_PLANT_ON_SCRIPT:
		{
			if ( m_hTargetEnt != NULL )
			{
				pev->origin = m_hTargetEnt->pev->origin;	// Plant on target
			}

			TaskComplete();
			break;
		}
	case TASK_FACE_SCRIPT:
		{
			if ( m_hTargetEnt != NULL )
			{
				pev->ideal_yaw = UTIL_AngleMod( m_hTargetEnt->pev->angles.y );
			}

			SetTurnActivity();

			//TaskComplete();
			//m_IdealActivity = ACT_IDLE;
			RouteClear();
			break;
		}

	case TASK_SUGGEST_STATE:
		{
			m_IdealMonsterState = (MONSTERSTATE)(int)pTask->flData;
			TaskComplete();
			break;
		}

	case TASK_SET_FAIL_SCHEDULE:
		m_failSchedule = (int)pTask->flData;
		TaskComplete();
		break;

	case TASK_CLEAR_FAIL_SCHEDULE:
		m_failSchedule = SCHED_NONE;
		TaskComplete();
		break;

	case TASK_WANDER:
		{
			// This task really uses 2 parameters, so we have to extract
			// them from a single integer. To send both parameters, the
			// formula is MIN_DIST * 10000 + MAX_DIST
			{
				int iMinDist, iMaxDist, iParameter;

				iParameter = pTask->flData;

				iMinDist = iParameter / 10000;
				iMaxDist = iParameter - (iMinDist * 10000);

				if ( MoveToWanderGoal( iMinDist, iMaxDist ) )
					TaskComplete();
				else
					TaskFail();
			}
		}
		break;

	case TASK_IGNORE_OLD_ENEMIES:
		m_flAcceptableTimeSeenEnemy = gpGlobals->time;
		if ( GetEnemy() && GetEnemyLastTimeSeen() < m_flAcceptableTimeSeenEnemy )
		{
			CBaseEntity *pNewEnemy = BestEnemy();

			Assert( pNewEnemy != GetEnemy() );

			if( pNewEnemy != NULL )
			{
				//New enemy! Clear the timers and set conditions.
				SetEnemy( pNewEnemy );
				SetState( MONSTERSTATE_COMBAT );
			}
			else
			{
				SetEnemy( NULL );
				ClearAttackConditions();
			}
		}
		TaskComplete();
		break;

	default:
		{
			ALERT ( at_aiconsole, "No StartTask entry for %s\n", TaskName( pTask->iTask ) );
			break;
		}
	}
}

//=========================================================
// GetTask - returns a pointer to the current 
// scheduled task. NULL if there's a problem.
//=========================================================
const Task_t *CBaseMonster::GetTask( void ) 
{
	int iScheduleIndex = GetScheduleCurTaskIndex();
	if ( !GetCurSchedule() || iScheduleIndex < 0 || iScheduleIndex >= GetCurSchedule()->NumTasks() )
		// iScheduleIndex is not within valid range for the monster's current schedule.
		return NULL;
	
	return &GetCurSchedule()->GetTaskList()[ iScheduleIndex ];
}

//=========================================================
// GetSchedule - Decides which type of schedule best suits
// the monster's current state and conditions. Then calls
// monster's member function to get a pointer to a schedule
// of the proper type.
//=========================================================
int CBaseMonster :: SelectSchedule ( void )
{
	switch	( m_MonsterState )
	{
	case MONSTERSTATE_PRONE:
		{
			return SCHED_BARNACLE_VICTIM_GRAB;
			break;
		}
	case MONSTERSTATE_NONE:
		{
			ALERT ( at_aiconsole, "MONSTERSTATE IS NONE!\n" );
			break;
		}
	case MONSTERSTATE_IDLE:
		{
			if ( HasCondition ( COND_HEAR_DANGER ) ||
				 HasCondition ( COND_HEAR_COMBAT ) ||
				 HasCondition ( COND_HEAR_WORLD  ) ||
				 HasCondition ( COND_HEAR_PLAYER ) )
			{
				return SCHED_ALERT_FACE;
			}
			else if ( FRouteClear() )
			{
				// no valid route!
				return SCHED_IDLE_STAND;
			}
			else
			{
				// valid route. Get moving
				return SCHED_IDLE_WALK;
			}
			break;
		}
	case MONSTERSTATE_ALERT:
		{
			if ( HasCondition( COND_ENEMY_DEAD ) && LookupActivity( ACT_VICTORY_DANCE ) != ACTIVITY_NOT_AVAILABLE )
			{
				return SCHED_VICTORY_DANCE;
			}

			if ( HasCondition(COND_LIGHT_DAMAGE) || HasCondition( COND_HEAVY_DAMAGE) )
			{
				if ( fabs( FlYawDiff() ) < (1.0 - m_flFieldOfView) * 60 ) // roughly in the correct direction
				{
					return SCHED_TAKE_COVER_FROM_ORIGIN;
				}
				else
				{
					return SCHED_ALERT_SMALL_FLINCH;
				}
			}

			else if ( HasCondition ( COND_HEAR_DANGER ) ||
					  HasCondition ( COND_HEAR_PLAYER ) ||
					  HasCondition ( COND_HEAR_WORLD  ) ||
					  HasCondition ( COND_HEAR_COMBAT ) )
			{
				return SCHED_ALERT_FACE;
			}
			else
			{
				return SCHED_ALERT_STAND;
			}
			break;
		}
	case MONSTERSTATE_COMBAT:
		{
			if ( HasCondition( COND_NEW_ENEMY ) )
			{
				return SCHED_WAKE_ANGRY;
			}
			else if ( HasCondition( COND_ENEMY_DEAD ) )
			{
				// clear the current (dead) enemy and try to find another.
				SetEnemy( NULL );

				if ( ChooseEnemy() )
				{
					ClearCondition( COND_ENEMY_DEAD );
					return SelectSchedule();
				}
				
				SetState( MONSTERSTATE_ALERT );
				return SelectSchedule();
			}
			else if ( HasCondition( COND_HEAVY_DAMAGE ) ) 
			{
				//MODDD - taking heavy damage is more drastic now with its own check.
				//It won't happen often enough to need memory for blocking.
				return SCHED_BIG_FLINCH;
			}
			else if (HasCondition( COND_LIGHT_DAMAGE ) && !HasMemory( bits_MEMORY_FLINCHED) )
			{
				return SCHED_SMALL_FLINCH;
			}
			else if ( !HasCondition( COND_SEE_ENEMY ) )
			{
				// we can't see the enemy
				if ( !HasCondition( COND_ENEMY_OCCLUDED ) )
				{
					// enemy is unseen, but not occluded!
					// turn to face enemy
					return SCHED_COMBAT_FACE;
				}
				else
				{
					// chase!
					return SCHED_CHASE_ENEMY;
				}
			}
			else  
			{
				// we can see the enemy
				if ( HasCondition( COND_CAN_RANGE_ATTACK1 ) )
				{
					return SCHED_RANGE_ATTACK1;
				}
				if ( HasCondition( COND_CAN_RANGE_ATTACK2 ) )
				{
					return SCHED_RANGE_ATTACK2;
				}
				if ( HasCondition( COND_CAN_MELEE_ATTACK1 ) )
				{
					return SCHED_MELEE_ATTACK1;
				}
				if ( HasCondition( COND_CAN_MELEE_ATTACK2 ) )
				{
					return SCHED_MELEE_ATTACK2;
				}
				if ( !FacingIdeal() )
				{
					//turn
					return SCHED_COMBAT_FACE;
				}
				else if ( !HasCondition( COND_CAN_RANGE_ATTACK1 ) && !HasCondition( COND_CAN_MELEE_ATTACK1 ) )
				{
					// if we can see enemy but can't use either attack type, we must need to get closer to enemy
					return SCHED_CHASE_ENEMY;
				}
				else
				{
					ALERT ( at_aiconsole, "No suitable combat schedule!\n" );
				}
			}
			break;
		}
	case MONSTERSTATE_DEAD:
		{
			return SCHED_DIE;
			break;
		}
	case MONSTERSTATE_SCRIPT:
		{
			ASSERT( m_pCine != NULL );
			if ( !m_pCine )
			{
				ALERT( at_aiconsole, "Script failed for %s\n", STRING(pev->classname) );
				CineCleanup();
				return SCHED_IDLE_STAND;
			}

			return SCHED_AISCRIPT;
		}
	default:
		{
			ALERT ( at_aiconsole, "Invalid State for SelectSchedule!\n" );
			break;
		}
	}

	return SCHED_FAIL;
}

//-----------------------------------------------------------------------------

int CBaseMonster::SelectFailSchedule( int failedSchedule, int failedTask )
{
	return ( m_failSchedule != SCHED_NONE ) ? m_failSchedule : SCHED_FAIL;
}