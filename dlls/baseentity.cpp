//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: The base class from which all game entities are derived.
//
//===========================================================================//

#include "cbase.h"
#include "basemonster.h"
#include "globalstate.h"
#include "saverestore.h"
#include "decals.h"
#include "baseentity.h"
#include "world.h"
#include "nodes.h"

extern CGraph WorldGraph;

void SetObjectCollisionBox( entvars_t *pev );

extern Vector VecBModelOrigin( entvars_t* pevBModel );
extern DLL_GLOBAL Vector		g_vecAttackDir;
extern DLL_GLOBAL int			g_iSkillLevel;

//------------------------------------------------------------------------------
// Purpose : If name exists returns name, otherwise returns classname
// Input   :
// Output  :
//------------------------------------------------------------------------------
const char *CBaseEntity::GetDebugName(void)
{
	if ( pev->targetname ) 
	{
		return STRING( pev->targetname );
	}
	else
	{
		return STRING( pev->classname );
	}
}

// give health
int CBaseEntity::TakeHealth( float flHealth, int bitsDamageType )
{
	if (!pev->takedamage)
		return 0;

// heal
	if ( pev->health >= pev->max_health )
		return 0;

	pev->health += flHealth;

	if (pev->health > pev->max_health)
		pev->health = pev->max_health;

	return 1;
}

// inflict damage on this entity.  bitsDamageType indicates type of damage inflicted, ie: DMG_CRUSH

int CBaseEntity::TakeDamage( const CTakeDamageInfo &info )
{
	Vector			vecTemp;

	if (!pev->takedamage)
		return 0;

	// UNDONE: some entity types may be immune or resistant to some bitsDamageType

	vecTemp = info.GetInflictor()->pev->origin - ( VecBModelOrigin(pev) );
	
// this global is still used for glass and other non-monster killables, along with decals.
	g_vecAttackDir = vecTemp.Normalize();
		
// save damage based on the target's armor level

// figure momentum add (don't let hurt brushes or other triggers move player)
	if ( info.GetInflictor() && (pev->movetype == MOVETYPE_WALK || pev->movetype == MOVETYPE_STEP) && (info.GetAttacker()->pev->solid != SOLID_TRIGGER) )
	{
		Vector vecDir = pev->origin - info.GetInflictor()->Center();
		vecDir = vecDir.Normalize();

		float flForce = info.GetDamage() * ((32 * 32 * 72.0) / (pev->size.x * pev->size.y * pev->size.z)) * 5;
		
		if (flForce > 1000.0) 
			flForce = 1000.0;
		pev->velocity = pev->velocity + vecDir * flForce;
	}

// do the damage
	pev->health -= info.GetDamage();
	if (pev->health <= 0)
	{
		Event_Killed( info );
		return 0;
	}

	return 1;
}


void CBaseEntity::Event_Killed( const CTakeDamageInfo &info )
{
	pev->takedamage = DAMAGE_NO;
	pev->deadflag = DEAD_DEAD;
	UTIL_Remove( this );
}


CBaseEntity *CBaseEntity::GetNextTarget( void )
{
	if ( FStringNull( pev->target ) )
		return NULL;
	edict_t *pTarget = FIND_ENTITY_BY_TARGETNAME ( NULL, STRING(pev->target) );
	if ( FNullEnt(pTarget) )
		return NULL;

	return Instance( pTarget );
}

// Global Savedata for Delay
BEGIN_DATADESC_NO_BASE( CBaseEntity )

	DEFINE_FIELD( CBaseEntity, m_pfnThink,		FIELD_FUNCTION ),		// UNDONE: Build table of these!!!
	DEFINE_FIELD( CBaseEntity, m_pfnTouch,		FIELD_FUNCTION ),
	DEFINE_FIELD( CBaseEntity, m_pfnUse,		FIELD_FUNCTION ),
	DEFINE_FIELD( CBaseEntity, m_pfnBlocked,	FIELD_FUNCTION ),

END_DATADESC()

// This updates global tables that need to know about entities being removed
void CBaseEntity::UpdateOnRemove( void )
{
	int	i;

	if ( FBitSet( pev->flags, FL_GRAPHED ) )
	{
	// this entity was a LinkEnt in the world node graph, so we must remove it from
	// the graph since we are removing it from the world.
		for ( i = 0 ; i < WorldGraph.m_cLinks ; i++ )
		{
			if ( WorldGraph.m_pLinkPool [ i ].m_pLinkEnt == pev )
			{
				// if this link has a link ent which is the same ent that is removing itself, remove it!
				WorldGraph.m_pLinkPool [ i ].m_pLinkEnt = NULL;
			}
		}
	}

	if ( pev->globalname )
	{ 
		// NOTE: During level shutdown the global list will suppress this
		// it assumes your changing levels or the game will end
		// causing the whole list to be flushed
		gGlobalState.EntitySetState( pev->globalname, GLOBAL_DEAD );
	}
}

// Initialize absmin & absmax to the appropriate box
void SetObjectCollisionBox( entvars_t *pev )
{
	if ( (pev->solid == SOLID_BSP) && 
		 (pev->angles.x || pev->angles.y|| pev->angles.z) )
	{	// expand for rotation
		float		max, v;
		int			i;

		max = 0;
		for (i=0 ; i<3 ; i++)
		{
			v = fabs( ((float *)pev->mins)[i]);
			if (v > max)
				max = v;
			v = fabs( ((float *)pev->maxs)[i]);
			if (v > max)
				max = v;
		}
		for (i=0 ; i<3 ; i++)
		{
			((float *)pev->absmin)[i] = ((float *)pev->origin)[i] - max;
			((float *)pev->absmax)[i] = ((float *)pev->origin)[i] + max;
		}
	}
	else
	{
		pev->absmin = pev->origin + pev->mins;
		pev->absmax = pev->origin + pev->maxs;
	}

	pev->absmin.x -= 1;
	pev->absmin.y -= 1;
	pev->absmin.z -= 1;
	pev->absmax.x += 1;
	pev->absmax.y += 1;
	pev->absmax.z += 1;
}


void CBaseEntity::SetObjectCollisionBox( void )
{
	::SetObjectCollisionBox( pev );
}


int	CBaseEntity::Intersects( CBaseEntity *pOther )
{
	if ( pOther->pev->absmin.x > pev->absmax.x ||
		 pOther->pev->absmin.y > pev->absmax.y ||
		 pOther->pev->absmin.z > pev->absmax.z ||
		 pOther->pev->absmax.x < pev->absmin.x ||
		 pOther->pev->absmax.y < pev->absmin.y ||
		 pOther->pev->absmax.z < pev->absmin.z )
		 return 0;
	return 1;
}

void CBaseEntity::MakeDormant( void )
{
	SetBits( pev->flags, FL_DORMANT );

	// Don't touch
	pev->solid = SOLID_NOT;
	// Don't move
	pev->movetype = MOVETYPE_NONE;
	// Don't draw
	SetBits( pev->effects, EF_NODRAW );
	// Don't think
	pev->nextthink = 0;
	// Relink
	UTIL_SetOrigin( pev, pev->origin );
}

int CBaseEntity::IsDormant( void )
{
	return FBitSet( pev->flags, FL_DORMANT );
}

bool CBaseEntity::IsInWorld( void ) const
{
	// position 
	if (pev->origin.x >= 4096) return false;
	if (pev->origin.y >= 4096) return false;
	if (pev->origin.z >= 4096) return false;
	if (pev->origin.x <= -4096) return false;
	if (pev->origin.y <= -4096) return false;
	if (pev->origin.z <= -4096) return false;
	// speed
	if (pev->velocity.x >= 2000) return false;
	if (pev->velocity.y >= 2000) return false;
	if (pev->velocity.z >= 2000) return false;
	if (pev->velocity.x <= -2000) return false;
	if (pev->velocity.y <= -2000) return false;
	if (pev->velocity.z <= -2000) return false;

	return true;
}

int CBaseEntity::ShouldToggle( USE_TYPE useType, int currentState )
{
	if ( useType != USE_TOGGLE && useType != USE_SET )
	{
		if ( (currentState && useType == USE_ON) || (!currentState && useType == USE_OFF) )
			return 0;
	}
	return 1;
}

int	CBaseEntity::DamageDecal( int bitsDamageType )
{
	if ( pev->rendermode == kRenderTransAlpha )
		return -1;

	if ( pev->rendermode != kRenderNormal )
		return DECAL_BPROOF1;

	return DECAL_GUNSHOT1 + RANDOM_LONG(0,4);
}

// NOTE: szName must be a pointer to constant memory, e.g. "monster_class" because the entity
// will keep a pointer to it after this call.
CBaseEntity * CBaseEntity::Create( const char *szName, const Vector &vecOrigin, const Vector &vecAngles, edict_t *pentOwner )
{
	edict_t	*pent;
	CBaseEntity *pEntity;

	pent = CREATE_NAMED_ENTITY( MAKE_STRING( szName ));
	if ( FNullEnt( pent ) )
	{
		ALERT ( at_console, "NULL Ent in Create!\n" );
		return NULL;
	}
	pEntity = Instance( pent );
	pEntity->pev->owner = pentOwner;
	pEntity->pev->origin = vecOrigin;
	pEntity->pev->angles = vecAngles;
	DispatchSpawn( pEntity->edict() );
	return pEntity;
}

//-----------------------------------------------------------------------------
// Purpose: Saves the current object out to disk, by iterating through the objects
//			data description hierarchy
// Input  : &save - save buffer which the class data is written to
// Output : int	- 0 if the save failed, 1 on success
//-----------------------------------------------------------------------------
int CBaseEntity::Save( CSave &save )
{
	int status = save.WriteEntVars( "ENTVARS", pev );

	// loop through the data description list, saving each data desc block
	if ( status )
		status = SaveDataDescBlock( save, GetDataDescMap() );

	return status;
}

//-----------------------------------------------------------------------------
// Purpose: Restores the current object from disk, by iterating through the objects
//			data description hierarchy
// Input  : &restore - restore buffer which the class data is read from
// Output : int	- 0 if the restore failed, 1 on success
//-----------------------------------------------------------------------------
int CBaseEntity::Restore( CRestore &restore )
{
	int status = restore.ReadEntVars( "ENTVARS", pev );

	// loops through the data description list, restoring each data desc block in order
	if ( status )
		status = RestoreDataDescBlock( restore, GetDataDescMap() );

    if ( pev->modelindex != 0 && !FStringNull(pev->model) )
	{
		Vector mins, maxs;
		mins = pev->mins;	// Set model is about to destroy these
		maxs = pev->maxs;

		PRECACHE_MODEL( (char *)STRING(pev->model) );
		SET_MODEL(ENT(pev), STRING(pev->model));
		UTIL_SetSize(pev, mins, maxs);	// Reset them
	}

	return status;
}

//-----------------------------------------------------------------------------
// handler to do stuff before you are saved
//-----------------------------------------------------------------------------
void CBaseEntity::OnSave()
{
}


//-----------------------------------------------------------------------------
// handler to do stuff after you are restored
//-----------------------------------------------------------------------------
void CBaseEntity::OnRestore()
{
}

//-----------------------------------------------------------------------------
// Purpose: Recursively saves all the classes in an object, in reverse order (top down)
// Output : int 0 on failure, 1 on success
//-----------------------------------------------------------------------------
int CBaseEntity::SaveDataDescBlock( CSave &save, datamap_t *dmap )
{
	return save.WriteAll( this, dmap );
}

//-----------------------------------------------------------------------------
// Purpose: Recursively restores all the classes in an object, in reverse order (top down)
// Output : int 0 on failure, 1 on success
//-----------------------------------------------------------------------------
int CBaseEntity::RestoreDataDescBlock( CRestore &restore, datamap_t *dmap )
{
	return restore.ReadAll( this, dmap );
}

//-----------------------------------------------------------------------------
// CBaseEntity new/delete
// allocates and frees memory for itself from the engine->
// All fields in the object are all initialized to 0.
//-----------------------------------------------------------------------------
void *CBaseEntity::operator new( size_t stAllocateBlock, entvars_t *pev )
{
	// call into engine to get memory
	return (void *)ALLOC_PRIVATE(ENT(pev), stAllocateBlock);
};


void CBaseEntity::operator delete( void *pMem, entvars_t *pev )
{
	// get the engine to free the memory
	FREE_PRIVATE( ENT(pev) );
};

#ifdef _DEBUG
void CBaseEntity::FunctionCheck( void *pFunction, char *name )
{ 
	if ( pFunction && !NAME_FOR_FUNCTION((uint32)pFunction) )
	{
		ALERT( at_error, "No EXPORT: %s:%s (%08lx)\n", STRING(pev->classname), name, (uint32)pFunction );
	}
}
#endif

bool CBaseEntity::IsTransparent() const
{
	return pev->rendermode != kRenderNormal;
}

//=========================================================
// FVisible - returns true if a line can be traced from
// the caller's eyes to the target
//=========================================================
bool CBaseEntity::FVisible( CBaseEntity *pEntity, CBaseEntity **ppBlocker )
{
	TraceResult tr;
	Vector		vecLookerOrigin;
	Vector		vecTargetOrigin;
	
	if (FBitSet( pEntity->pev->flags, FL_NOTARGET ))
		return false;

	// don't look through water
	if ((pev->waterlevel != 3 && pEntity->pev->waterlevel == 3) 
		|| (pev->waterlevel == 3 && pEntity->pev->waterlevel == 0))
		return false;

	vecLookerOrigin = EyePosition();//look through the caller's 'eyes'
	vecTargetOrigin = pEntity->EyePosition();

	UTIL_TraceLine( vecLookerOrigin, vecTargetOrigin, ignore_monsters, ignore_glass, ENT(pev)/*pentIgnore*/, &tr );
	
	if (tr.flFraction != 1.0)
	{
		if ( ppBlocker )
		{
			*ppBlocker = CBaseEntity::Instance( tr.pHit );
		}
		return false;// Line of sight is not established
	}
	else
	{
		return true;// line of sight is valid.
	}
}

//=========================================================
// FVisible - returns true if a line can be traced from
// the caller's eyes to the target vector
//=========================================================
bool CBaseEntity::FVisible( const Vector &vecTarget, CBaseEntity **ppBlocker )
{
	TraceResult tr;
	Vector		vecLookerOrigin;
	
	vecLookerOrigin = EyePosition();//look through the caller's 'eyes'

	UTIL_TraceLine( vecLookerOrigin, vecTarget, ignore_monsters, ignore_glass, ENT(pev), &tr );
	
	if (tr.flFraction != 1.0)
	{
		if ( ppBlocker )
		{
			*ppBlocker = CBaseEntity::Instance( tr.pHit );
		}
		return false;// Line of sight is not established
	}
	else
	{
		return true;// line of sight is valid.
	}
}

//-----------------------------------------------------------------------------
// Selects a random point in the bounds given the normalized 0-1 bounds 
//-----------------------------------------------------------------------------
void CBaseEntity::RandomPointInBounds( const Vector &vecNormalizedMins, const Vector &vecNormalizedMaxs, Vector *pPoint) const
{
	float x = RANDOM_FLOAT( vecNormalizedMins.x, vecNormalizedMaxs.x );
	float y = RANDOM_FLOAT( vecNormalizedMins.y, vecNormalizedMaxs.y );
	float z = RANDOM_FLOAT( vecNormalizedMins.z, vecNormalizedMaxs.z );

	pPoint->x = pev->size.x * x + pev->mins.x;
	pPoint->y = pev->size.y * y + pev->mins.y;
	pPoint->z = pev->size.z * z + pev->mins.z;
	*pPoint = *pPoint + pev->origin;
}