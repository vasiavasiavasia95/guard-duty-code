/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// monster template
//=========================================================

#include	"cbase.h"
#include	"basemonster.h"
#include	"ai_schedule.h"

//=========================================================
// Monster's Anim Events Go Here
//=========================================================

class CHAssault : public CBaseMonster
{
	DECLARE_CLASS( CHAssault, CBaseMonster );

public:
	void Spawn( void );
	void Precache( void );
	void SetYawSpeed( void );
	Class_T Classify ( void );
	void HandleAnimEvent( animevent_t *pEvent );

	void AlertSound( void );
};

LINK_ENTITY_TO_CLASS( monster_human_assault, CHAssault );

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CHAssault::Classify( void )
{
	return CLASS_HUMAN_MILITARY;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CHAssault::SetYawSpeed ( void )
{
	int ys;

	switch ( m_Activity )
	{
	case ACT_IDLE:
	default:
		ys = 90;
	}

	pev->yaw_speed = ys;
}

//=========================================================
// AlertSound
//=========================================================
void CHAssault::AlertSound ( void )
{
	EMIT_SOUND ( edict(), CHAN_VOICE, "hassault/hw_alert.wav", VOL_NORM, ATTN_NORM );
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CHAssault::HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case 0:
	default:
		BaseClass::HandleAnimEvent( pEvent );
		break;
	}
}

//=========================================================
// Spawn
//=========================================================
void CHAssault::Spawn()
{
	Precache( );

	SET_MODEL( edict(), "models/hassault.mdl" );
	UTIL_SetSize( pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX );

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_RED;
	pev->health			= 8;
	pev->view_ofs		= VEC_VIEW;// position of the eyes relative to monster's origin.
	m_flFieldOfView		= 0.5;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;

	MonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CHAssault::Precache()
{
	PRECACHE_MODEL("models/hassault.mdl");

	PRECACHE_SOUND("hassault/hw_alert.wav");
}	

//=========================================================
// AI Schedules Specific to this monster
//=========================================================
