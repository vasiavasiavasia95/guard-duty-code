/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef SAVERESTORE_H
#define SAVERESTORE_H
#if defined( _WIN32 )
#pragma once
#endif

class SINGLE_INHERITANCE CBaseEntity;
class EHANDLE;

#define MAX_ENTITYARRAY 64

class CSaveRestoreBuffer
{
public:
	CSaveRestoreBuffer( void );
	CSaveRestoreBuffer( SAVERESTOREDATA *pdata );
	~CSaveRestoreBuffer( void );

	int			EntityIndex( entvars_t *pevLookup );
	int			EntityIndex( edict_t *pentLookup );
	int			EntityIndex( EOFFSET eoLookup );
	int			EntityIndex( CBaseEntity *pEntity );

	int			EntityFlags( int entityIndex, int flags ) { return EntityFlagsSet( entityIndex, 0 ); }
	int			EntityFlagsSet( int entityIndex, int flags );

	edict_t		*EntityFromIndex( int entityIndex );

	unsigned short	TokenHash( const char *pszToken );
	const char *StringFromToken( int token );

protected:

	// Stream data
	SAVERESTOREDATA		*m_pdata;
	void		BufferRewind( int size );
	unsigned int	HashString( const char *pszToken );
	bool BufferSeek( int absPosition );
};

//-----------------------------------------------------------------------------
//
// CSave
//
//-----------------------------------------------------------------------------

class CSave : public CSaveRestoreBuffer
{
public:
	CSave( SAVERESTOREDATA *pdata );

	//---------------------------------
	// Datamap based writing
	//
	
	int		WriteAll( const void *pLeafObject, datamap_t *pLeafMap )	{ return DoWriteAll( pLeafObject, pLeafMap, pLeafMap ); }

	template <typename T>
	int WriteAll( const T *pLeafObject )
	{
		return WriteAll( pLeafObject, &pLeafObject->m_DataMap );
	}
	
	//---------------------------------
	// Block support
	//
	
	virtual void	StartBlock( const char *pszBlockName );
	virtual void	StartBlock();
	virtual void	EndBlock();

	//---------------------------------
	// Primitive types
	//

	void	WriteShort( const short *value, int count = 1 );
	void	WriteInt( const int *value, int count = 1 );		                           // Save an int
	inline void		WriteInt( const unsigned *value, int count = 1 ) { WriteInt( (int *)value, count );	}
	void	WriteBool( const bool *value, int count = 1 );		                           // Save a bool
	void	WriteString( const char *pstring );			                                   // Save a null-terminated string
	void	WriteEntityPtr( CBaseEntity **ppEntity, int count = 1 );

	// Note: All of the following will write out both a header and the data. On restore,
	// this needs to be cracked
	void	WriteShort( const char *pname, const short *value, int count );
	void	WriteInt( const char *pname, const int *value, int count );		// Save an int
	void	WriteBool( const char *pname, const bool *value, int count = 1 ); // Save a bool
	void	WriteFloat( const char *pname, const float *value, int count );	// Save a float
	void	WriteTime( const char *pname, const float *value, int count );	// Save a float (timevalue)
	void	WriteData( const char *pname, int size, const char *pdata );		// Save a binary data block
	void	WriteString( const char *pname, const char *pstring );			// Save a null-terminated string
	void	WriteString( const char *pname, const int *stringId, int count );	// Save a null-terminated string (engine string)
	void	WriteVector( const char *pname, const Vector &value );				// Save a vector
	void	WriteVector( const char *pname, const float *value, int count );	// Save a vector
	void	WritePositionVector( const char *pname, const Vector &value );		// Offset for landmark if necessary
	void	WritePositionVector( const char *pname, const float *value, int count );	// array of pos vectors
	void	WriteFunction( const char *pname, void **value, int count );		// Save a function pointer
	void	WriteEntityPtr( const char *pname, CBaseEntity **ppEntity, int count = 1 );
	int		WriteEntVars( const char *pname, entvars_t *pev );		// Save entvars_t (entvars_t)
	int		WriteFields( const char *pname, const void *pBaseData, typedescription_t *pFields, int fieldCount );

private:

	int		DoWriteAll( const void *pLeafObject, datamap_t *pLeafMap, datamap_t *pCurMap );

	int		DataEmpty( const char *pdata, int size );
	void	BufferField( const char *pname, int size, const char *pdata );
	void	BufferString( char *pdata, int len );
	void	BufferData( const char *pdata, int size );
	void	BufferHeader( const char *pname, int size );

	int		CountFieldsToSave( const void *pBaseData, typedescription_t *pFields, int fieldCount );
	bool	ShouldSaveField( const void *pData, typedescription_t *pField );

	//---------------------------------
	
	CUtlVector<int> m_BlockStartStack;
};

//-----------------------------------------------------------------------------
//
// CRestore
//
//-----------------------------------------------------------------------------

typedef struct 
{
	unsigned short		size;
	unsigned short		token;
} HEADER;

const int MAX_BLOCK_NAME_LEN = 31;
const int SIZE_BLOCK_NAME_BUF = 31 + 1;

class CRestore : public CSaveRestoreBuffer
{
public:
	CRestore( SAVERESTOREDATA *pdata );

	// Datamap based reading
	//
	
	int		ReadAll( void *pLeafObject, datamap_t *pLeafMap )	{ return DoReadAll( pLeafObject, pLeafMap, pLeafMap ); }

	template <typename T>
	int ReadAll( T *pLeafObject )
	{
		return ReadAll( pLeafObject, &pLeafObject->m_DataMap );
	}

	//---------------------------------

	int		ReadEntVars( const char *pname, entvars_t *pev );		// entvars_t
	int		ReadFields( const char *pname, void *pBaseData, typedescription_t *pFields, int fieldCount );
	void 	EmptyFields( void *pBaseData, typedescription_t *pFields, int fieldCount );

	//---------------------------------
	// Block support
	//
	
	virtual void	StartBlock( HEADER *pHeader );
	virtual void	StartBlock( char szBlockName[SIZE_BLOCK_NAME_BUF] );
	virtual void	StartBlock();
	virtual void	EndBlock();

	//---------------------------------
	// Field header cracking
	//

	void			ReadHeader( HEADER *pheader );
	int				SkipHeader()	{ HEADER header; ReadHeader( &header ); return header.size; }
	const char *	StringFromHeaderToken( int token );

	//---------------------------------
	// Primitive types
	//

	short			ReadShort( void );
	int				ReadShort( short *pValue, int count = 1, int nBytesAvailable = 0);
	int				ReadInt( int *pValue, int count = 1, int nBytesAvailable = 0);
	inline  int		ReadInt( unsigned *pValue, int count = 1, int nBytesAvailable = 0 ) { return ReadInt( (int *)pValue, count, nBytesAvailable ); }
	int				ReadInt( void );
	int 			ReadBool( bool *pValue, int count = 1, int nBytesAvailable = 0);
	int				ReadFloat( float *pValue, int count = 1, int nBytesAvailable = 0);
	int				ReadData( char *pData, int size, int nBytesAvailable );
	void			ReadString( char *pDest, int nSizeDest, int nBytesAvailable );			// A null-terminated string
	int				ReadString( string_t *pString, int count = 1, int nBytesAvailable = 0);
	int				ReadVector( Vector *pValue );
	int				ReadVector( Vector *pValue, int count = 1, int nBytesAvailable = 0);

	int				ReadTime( float *pValue, int count = 1, int nBytesAvailable = 0);
	int				ReadPositionVector( Vector *pValue );
	int				ReadPositionVector( Vector *pValue, int count = 1, int nBytesAvailable = 0);
	int				ReadFunction( int *pValue, int count = 1, int nBytesAvailable = 0);

	int 			ReadEntityVars( entvars_t **ppEntVars, int count = 1, int nBytesAvailable = 0 );
	int 			ReadEntityPtr( CBaseEntity **ppEntity, int count = 1, int nBytesAvailable = 0 );
	int				ReadEdictPtr( edict_t **ppEdict, int count = 1, int nBytesAvailable = 0 );
	int				ReadEHandle( EHANDLE *pEHandle, int count = 1, int nBytesAvailable = 0 );

	int		Empty( void ) { return (m_pdata == NULL) || ((m_pdata->pCurrentData-m_pdata->pBaseData)>=m_pdata->bufferSize); }

	//---------------------------------

	void	SetGlobalMode( int global ) { m_global = global; }
	void	PrecacheMode( bool mode ) { m_precache = mode; }
	bool	GetPrecacheMode( void ) { return m_precache; }

private:
	//---------------------------------
	// Read primitives
	//

	char *			BufferPointer( void );
	void			BufferSkipBytes( int bytes );

	int				DoReadAll( void *pLeafObject, datamap_t *pLeafMap, datamap_t *pCurMap );

	typedescription_t *FindField( const char *pszFieldName, typedescription_t *pFields, int fieldCount, int *pIterator );
	void			ReadField( const HEADER &header, void *pDest, typedescription_t *pField );

	void			BufferReadBytes( char *pOutput, int size );

	template <typename T>
	int ReadSimple( T *pValue, int nElems, int nBytesAvailable ) // must be inline in class to keep MSVS happy
	{
		int desired = nElems * sizeof(T);
		int actual;
		
		if ( nBytesAvailable == 0 )
			actual = desired;
		else
		{
			Assert( nBytesAvailable % sizeof(T) == 0 );
			actual = min( desired, nBytesAvailable );
		}

		BufferReadBytes( (char *)pValue, actual );
		
		if ( actual < nBytesAvailable )
			BufferSkipBytes( nBytesAvailable - actual );
		
		return ( actual / sizeof(T) );
	}

	bool			ShouldReadField( typedescription_t *pField );
	bool 			ShouldEmptyField( typedescription_t *pField );

	//---------------------------------
	
	CUtlVector<int> m_BlockEndStack;

	int		m_global;		// Restoring a global entity?
	bool	m_precache;
};

//-----------------------------------------------------------------------------
// Purpose: The operations necessary to save and restore custom types (FIELD_CUSTOM)
//
//

struct SaveRestoreFieldInfo_t
{
	void *			   pField;

	// Note that it is legal for the following two fields to be NULL,
	// though it may be disallowed by implementors of CSaveRestoreOps
	void *			   pOwner;
	typedescription_t *pTypeDesc;
};

class ISaveRestoreOps
{
public:
	// save data type interface
	virtual void Save( const SaveRestoreFieldInfo_t &fieldInfo, CSave *pSave ) = 0;
	virtual void Restore( const SaveRestoreFieldInfo_t &fieldInfo, CRestore *pRestore ) = 0;

	virtual bool IsEmpty( const SaveRestoreFieldInfo_t &fieldInfo ) = 0;
	virtual void MakeEmpty( const SaveRestoreFieldInfo_t &fieldInfo ) = 0;
	virtual bool Parse( const SaveRestoreFieldInfo_t &fieldInfo, char const* szValue ) = 0;

	//---------------------------------

	void Save( void *pField, CSave *pSave )				{ SaveRestoreFieldInfo_t fieldInfo = { pField, NULL, NULL }; Save( fieldInfo, pSave ); }
	void Restore( void *pField, CRestore *pRestore )	{ SaveRestoreFieldInfo_t fieldInfo = { pField, NULL, NULL }; Restore( fieldInfo, pRestore ); }

	bool IsEmpty( void *pField)							{ SaveRestoreFieldInfo_t fieldInfo = { pField, NULL, NULL }; return IsEmpty( fieldInfo ); }
	void MakeEmpty( void *pField)						{ SaveRestoreFieldInfo_t fieldInfo = { pField, NULL, NULL }; MakeEmpty( fieldInfo ); }
	bool Parse( void *pField, char const *pszValue )	{ SaveRestoreFieldInfo_t fieldInfo = { pField, NULL, NULL }; return Parse( fieldInfo, pszValue ); }
};

//-------------------------------------

class CDefSaveRestoreOps : public ISaveRestoreOps
{
public:
	// save data type interface
	virtual void Save( const SaveRestoreFieldInfo_t &fieldInfo, CSave *pSave ) {}
	virtual void Restore( const SaveRestoreFieldInfo_t &fieldInfo, CRestore *pRestore ) {}

	virtual bool IsEmpty( const SaveRestoreFieldInfo_t &fieldInfo ) { return false; }
	virtual void MakeEmpty( const SaveRestoreFieldInfo_t &fieldInfo ) {}
	virtual bool Parse( const SaveRestoreFieldInfo_t &fieldInfo, char const* szValue ) { return false; }
};


//-----------------------------------------------------------------------------
// Used by ops that deal with pointers
//-----------------------------------------------------------------------------
class CClassPtrSaveRestoreOps : public CDefSaveRestoreOps
{
public:
	virtual bool IsEmpty( const SaveRestoreFieldInfo_t &fieldInfo )
	{
		void **ppClassPtr = (void **)fieldInfo.pField;
		int nObjects = fieldInfo.pTypeDesc->fieldSize;
		for ( int i = 0; i < nObjects; i++ )
		{
			if ( ppClassPtr[i] != NULL )
				return false;
		}
		return true;
	}

	virtual void MakeEmpty( const SaveRestoreFieldInfo_t &fieldInfo )
	{
		memset( fieldInfo.pField, 0, fieldInfo.pTypeDesc->fieldSize * sizeof( void * ) );
	}
};

#endif		//SAVERESTORE_H
