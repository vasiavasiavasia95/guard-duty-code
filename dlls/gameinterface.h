//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Expose things from GameInterface.cpp. Mostly the engine interfaces.
//
//=============================================================================//

#ifndef GAMEINTERFACE_H
#define GAMEINTERFACE_H

#ifdef _WIN32
#pragma once
#endif

#ifdef _WIN32
//The one function in Half-Life 1 that uses stdcall. Be aware of this. - Solokiller
#define GIVEFNPTRS_DLLEXPORT STDCALL
#else
#define GIVEFNPTRS_DLLEXPORT DLL_CLASS_EXPORT
#endif

// interface initialization callbacks

extern "C" void GIVEFNPTRS_DLLEXPORT GiveFnptrsToDll( enginefuncs_t *pengfuncsFromEngine, globalvars_t *pGlobals );

// Most of this is implemented in gameinterface.cpp, but some of it is in per-mod in files like client.cpp, etc.

extern void GameDLLInit( void );
extern void GameDLLShutdown( void );

// Dummy functions for callbacks engine never actually calls or that we don't use
// but have to provide in order for the interface to load
void SpectatorConnect( edict_t *pEntity )		{ return; };
void SpectatorDisconnect( edict_t *pEntity )	{ return; };
void SpectatorThink( edict_t *pEntity )			{ return; };
void ParmsNewLevel( void )						{ return; };

extern void Sys_Error( const char *error_string );

extern void SaveWriteFields( SAVERESTOREDATA *pSaveData, const char *pname, void *pBaseData, TYPEDESCRIPTION *pFields, int fieldCount );
extern void SaveReadFields( SAVERESTOREDATA *pSaveData, const char *pname, void *pBaseData, TYPEDESCRIPTION *pFields, int fieldCount );

extern void BuildAdjacentMapList( void );

// Helper functions to implement extra game functionality

bool LoadFileSystem( void );
void FreeFileSystem( void );

#endif // GAMEINTERFACE_H