//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:	A gib is a chunk of a body, or a piece of wood/metal/rocks/etc.
//
//===========================================================================//
#ifndef GIB_H
#define GIB_H
#pragma once 

#define GERMAN_GIB_COUNT		4
#define	HUMAN_GIB_COUNT			6
#define ALIEN_GIB_COUNT			4

enum GibType_e
{
	GIB_HUMAN,
	GIB_ALIEN,
};

class CGib : public CBaseEntity
{
public:
	DECLARE_CLASS( CGib, CBaseEntity );

	void Spawn( const char *szGibModel );
	void InitGib( CBaseEntity *pVictim, float fMaxVelocity, float fMinVelocity );
	void EXPORT BounceGibTouch ( CBaseEntity *pOther );
	void EXPORT StickyGibTouch ( CBaseEntity *pOther );
	void EXPORT WaitTillLand( void );
	void		LimitVelocity( void );

	virtual int	ObjectCaps( void ) { return (BaseClass::ObjectCaps() & ~FCAP_ACROSS_TRANSITION) | FCAP_DONT_SAVE; }
	static	void SpawnHeadGib( CBaseEntity *pVictim );
	static	void SpawnRandomGibs( CBaseEntity *pVictim, int cGibs, GibType_e eGibType );
	static  void SpawnStickyGibs( CBaseEntity *pVictim, Vector vecOrigin, int cGibs );
	static	void SpawnSpecificGibs( CBaseEntity *pVictim, int nNumGibs, float fMaxVelocity, float fMinVelocity, const char* cModelName, float flLifetime = 25);

public:
	void SetBloodColor( int nBloodColor );

	int		m_cBloodDecals;
	int		m_material;
	float	m_lifeTime;

private:
	// A little piece of duplicated code
	void AdjustVelocityBasedOnHealth( int nHealth, Vector &vecVelocity );
	int		m_bloodColor;
};

#endif	//GIB_H
