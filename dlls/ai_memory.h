//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:		A monsters's memory of potential enemies 
//
//=============================================================================//

#include "mempool.h"
#include "utlmap.h"

#ifndef AI_MEMORY_H
#define AI_MEMORY_H
#pragma once

DECLARE_POINTER_HANDLE(AIEnemiesIter_t);

//-----------------------------------------------------------------------------
// AI_EnemyInfo_t
//
// Purpose: Stores relevant tactical information about an enemy
//
//-----------------------------------------------------------------------------
struct AI_EnemyInfo_t
{
	AI_EnemyInfo_t();
	
	EHANDLE			hEnemy;				// Pointer to the enemy

	Vector			vLastKnownLocation;
	float			flLastTimeSeen;		// Last time enemy was seen
	bool			bDangerMemory;		// Memory of danger position w/o Enemy pointer
	bool			bEludedMe;			// True if enemy not at last known location 

	DECLARE_SIMPLE_DATADESC();
	DECLARE_FIXEDSIZE_ALLOCATOR(AI_EnemyInfo_t);
};

//-----------------------------------------------------------------------------
// CAI_Enemies
//
// Purpose: Stores a set of AI_EnemyInfo_t's
//
//-----------------------------------------------------------------------------
class CAI_Enemies
{
public:
	CAI_Enemies(void);
	~CAI_Enemies();
	
	AI_EnemyInfo_t *GetFirst( AIEnemiesIter_t *pIter );
	AI_EnemyInfo_t *GetNext( AIEnemiesIter_t *pIter );
	AI_EnemyInfo_t *Find( CBaseEntity *pEntity, bool bTryDangerMemory = false );

	int				NumEnemies() const		{ return m_Map.Count(); }
	int				GetSerialNumber() const	{ return m_serial;		}

	void			RefreshMemories(void);
	bool			UpdateMemory( CBaseEntity *enemy, const Vector &vPosition, bool firstHand = true );

	bool			HasMemory( CBaseEntity *enemy );
	void			ClearMemory( CBaseEntity *enemy );

	const Vector &	LastKnownPosition( CBaseEntity *pEnemy );
	float			LastTimeSeen( CBaseEntity *pEnemy );
	
	void			MarkAsEluded( CBaseEntity *enemy );						// Don't know where he is (whole squad)
	bool			HasEludedMe( CBaseEntity *pEnemy );
	
	void			SetFreeKnowledgeDuration( float flDuration ) { m_flFreeKnowledgeDuration = flDuration; }

	DECLARE_SIMPLE_DATADESC();

	typedef CUtlMap<CBaseEntity *, AI_EnemyInfo_t*, unsigned char> CMemMap;

private:
	bool ShouldDiscardMemory( AI_EnemyInfo_t *pMemory );

	CMemMap m_Map;
	float	m_flFreeKnowledgeDuration;
	Vector	m_vecDefaultLKP;
	int		m_serial;
};

//-----------------------------------------------------------------------------

#endif // AI_MEMORY_H
