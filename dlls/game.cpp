/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#include "cbase.h"
#include "game.h"

cvar_t	displaysoundlist = {"displaysoundlist","0"};

cvar_t	*g_psv_unlockedchapters = NULL;

// multiplayer server rules
cvar_t	fragsleft	= {"mp_fragsleft","0", FCVAR_SERVER | FCVAR_UNLOGGED };	  // Don't spam console/log files/users with this changing
cvar_t	timeleft	= {"mp_timeleft","0" , FCVAR_SERVER | FCVAR_UNLOGGED };	  // "      "

// multiplayer server rules
cvar_t	teamplay	= {"mp_teamplay","0", FCVAR_SERVER };
cvar_t	fraglimit	= {"mp_fraglimit","0", FCVAR_SERVER };
cvar_t	timelimit	= {"mp_timelimit","0", FCVAR_SERVER };
cvar_t	friendlyfire= {"mp_friendlyfire","0", FCVAR_SERVER };
cvar_t	falldamage	= {"mp_falldamage","0", FCVAR_SERVER };
cvar_t	weaponstay	= {"mp_weaponstay","0", FCVAR_SERVER };
cvar_t	forcerespawn= {"mp_forcerespawn","1", FCVAR_SERVER };
cvar_t	flashlight	= {"mp_flashlight","0", FCVAR_SERVER };
cvar_t	aimcrosshair= {"mp_autocrosshair","1", FCVAR_SERVER };
cvar_t	decalfrequency = {"decalfrequency","30", FCVAR_SERVER };
cvar_t	teamlist = {"mp_teamlist","hgrunt;scientist", FCVAR_SERVER };
cvar_t	teamoverride = {"mp_teamoverride","1" };
cvar_t	defaultteam = {"mp_defaultteam","0" };
cvar_t	allowmonsters={"mp_allowmonsters","0", FCVAR_SERVER };

cvar_t  allow_spectators = { "allow_spectators", "0.0", FCVAR_SERVER };		// 0 prevents players from being spectators

cvar_t  mp_chattime = {"mp_chattime","10", FCVAR_SERVER };

// Engine Cvars
cvar_t 	*g_psv_gravity = NULL;
cvar_t	*g_psv_aim = NULL;
cvar_t	*g_footsteps = NULL;

//CVARS FOR SKILL LEVEL SETTINGS

// Agrunt
cvar_t	sk_agrunt_health	= {	"sk_agrunt_health",		"0" };
cvar_t	sk_agrunt_dmg_punch = { "sk_agrunt_dmg_punch",	"0" };

// Apache
cvar_t	sk_apache_health	= {	"sk_apache_health",	"0"	};

// Barney
cvar_t	sk_barney_health	= {	"sk_barney_health",	"0"	};

// Bullsquid
cvar_t	sk_bullsquid_health		= {	"sk_bullsquid_health",	 "0" };
cvar_t	sk_bullsquid_dmg_bite	= {	"sk_bullsquid_dmg_bite", "0" };
cvar_t	sk_bullsquid_dmg_whip	= {	"sk_bullsquid_dmg_whip", "0" };
cvar_t	sk_bullsquid_dmg_spit	= {	"sk_bullsquid_dmg_spit", "0" };

// Big Momma
cvar_t	sk_bigmomma_health_factor	= {	"sk_bigmomma_health_factor", "1.0" };
cvar_t	sk_bigmomma_dmg_slash		= {	"sk_bigmomma_dmg_slash", "50" };
cvar_t	sk_bigmomma_dmg_blast		= {	"sk_bigmomma_dmg_blast", "100"	};
cvar_t	sk_bigmomma_radius_blast	= {	"sk_bigmomma_radius_blast", "250" };

// Gargantua
cvar_t	sk_gargantua_health		= {	"sk_gargantua_health",		"0"	};
cvar_t	sk_gargantua_dmg_slash	= {	"sk_gargantua_dmg_slash",	"0"	};
cvar_t	sk_gargantua_dmg_fire	= {	"sk_gargantua_dmg_fire",	"0"	};
cvar_t	sk_gargantua_dmg_stomp	= {	"sk_gargantua_dmg_stomp",	"0"	};

// Hassassin
cvar_t	sk_hassassin_health = {"sk_hassassin_health","0"};

// Headcrab
cvar_t	sk_headcrab_health		= {	"sk_headcrab_health",	"0"	};
cvar_t	sk_headcrab_dmg_bite	= {	"sk_headcrab_dmg_bite",	"0"	};

// Hgrunt 
cvar_t	sk_hgrunt_health	= {	"sk_hgrunt_health",		"0"	};
cvar_t	sk_hgrunt_kick		= {	"sk_hgrunt_kick",		"0"	};
cvar_t	sk_hgrunt_pellets	= {	"sk_hgrunt_pellets",	"0"	};
cvar_t	sk_hgrunt_gspeed	= {	"sk_hgrunt_gspeed",		"0"	};

// Houndeye
cvar_t	sk_houndeye_health		= {	"sk_houndeye_health",		"0"	};
cvar_t	sk_houndeye_dmg_blast	= {	"sk_houndeye_dmg_blast",	"0"	};

// ISlave
cvar_t	sk_islave_health		= {	"sk_islave_health",			"0"	};
cvar_t	sk_islave_dmg_claw		= {	"sk_islave_dmg_claw",		"0"	};
cvar_t	sk_islave_dmg_clawrake	= {	"sk_islave_dmg_clawrake",	"0"	};
cvar_t	sk_islave_dmg_zap		= {	"sk_islave_dmg_zap",		"0"	};

// Icthyosaur
cvar_t	sk_ichthyosaur_health	= {"sk_ichthyosaur_health","0"};
cvar_t	sk_ichthyosaur_shake	= {"sk_ichthyosaur_shake","0"};

// Leech
cvar_t	sk_leech_health		= {	"sk_leech_health",		"0"	};
cvar_t	sk_leech_dmg_bite	= {	"sk_leech_dmg_bite",	"0"	};

// Controller
cvar_t	sk_controller_health	= {	"sk_controller_health",		"0"	};
cvar_t	sk_controller_dmgzap	= {	"sk_controller_dmgzap",		"0"	};
cvar_t	sk_controller_speedball = {	"sk_controller_speedball",	"0"	};
cvar_t	sk_controller_dmgball	= {	"sk_controller_dmgball",	"0"	};

// Nihilanth
cvar_t	sk_nihilanth_health	= {	"sk_nihilanth_health",	"0"	};
cvar_t	sk_nihilanth_zap	= {	"sk_nihilanth_zap",	"0"	};

// Scientist
cvar_t	sk_scientist_health	= {	"sk_scientist_health",	"0"	};
cvar_t	sk_scientist_heal	= {	"sk_scientist_heal",	"0"	};	

// Snark
cvar_t	sk_snark_health		= {	"sk_snark_health",		"0"	};
cvar_t	sk_snark_dmg_bite	= {	"sk_snark_dmg_bite",	"0"	};
cvar_t	sk_snark_dmg_pop	= {	"sk_snark_dmg_pop",		"0"	};

// Zombie
cvar_t	sk_zombie_health			= {	"sk_zombie_health",			"0"	};
cvar_t	sk_zombie_dmg_one_slash		= {	"sk_zombie_dmg_one_slash",	"0"	};
cvar_t	sk_zombie_dmg_both_slash	= {	"sk_zombie_dmg_both_slash",	"0"	};

//Turret
cvar_t	sk_turret_health = {"sk_turret_health","0"};

// MiniTurret
cvar_t	sk_miniturret_health = {"sk_miniturret_health","0"};

// Sentry Turret
cvar_t	sk_sentry_health = {"sk_sentry_health","0"};

// PLAYER WEAPONS

// Crowbar whack
cvar_t	sk_plr_crowbar = {"sk_plr_crowbar","0"};

// Glock Round
cvar_t	sk_plr_9mm_bullet = {"sk_plr_9mm_bullet","0"};

// 357 Round
cvar_t	sk_plr_357_bullet = { "sk_plr_357_bullet", "0" };

// MP5 Round
cvar_t	sk_plr_9mmAR_bullet = {	"sk_plr_9mmAR_bullet", "0" };

// M203 grenade
cvar_t	sk_plr_9mmAR_grenade = { "sk_plr_9mmAR_grenade", "0" };

// Shotgun buckshot
cvar_t	sk_plr_buckshot	= {	"sk_plr_buckshot",	"0"	};

// Crossbow
cvar_t	sk_plr_xbow_bolt_client		= {	"sk_plr_xbow_bolt_client",	"0"	};
cvar_t	sk_plr_xbow_bolt_monster	= {	"sk_plr_xbow_bolt_monster",	"0"	};

// RPG
cvar_t	sk_plr_rpg = { "sk_plr_rpg", "0" };

// Tau Cannon
cvar_t	sk_plr_gauss = { "sk_plr_gauss", "0" };

// Egon Gun
cvar_t	sk_plr_egon_narrow	= {	"sk_plr_egon_narrow",	"0"	};
cvar_t	sk_plr_egon_wide	= {	"sk_plr_egon_wide",		"0"	};

// Hand Grendade
cvar_t	sk_plr_hand_grenade = { "sk_plr_hand_grenade", "0" };

// Satchel Charge
cvar_t	sk_plr_satchel	= { "sk_plr_satchel", "0" };

// Tripmine
cvar_t	sk_plr_tripmine = { "sk_plr_tripmine", "0" };

// WORLD WEAPONS
cvar_t	sk_12mm_bullet	= {	"sk_12mm_bullet",	"0"	};
cvar_t	sk_9mmAR_bullet = {	"sk_9mmAR_bullet",	"0"	};
cvar_t	sk_9mm_bullet	= {	"sk_9mm_bullet",	"0"	};

// HORNET
cvar_t	sk_hornet_dmg = { "sk_hornet_dmg", "0" };

// HEALTH/CHARGE
cvar_t	sk_suitcharger		= { "sk_suitcharger",	"0" };	
cvar_t	sk_battery			= { "sk_battery",		"0" };
cvar_t	sk_healthcharger	= { "sk_healthcharger",	"0" };	
cvar_t	sk_healthkit		= { "sk_healthkit",		"0" };

// monster damage adjusters
cvar_t	sk_monster_head		= { "sk_monster_head",		"2" };
cvar_t	sk_monster_chest	= { "sk_monster_chest",		"1" };
cvar_t	sk_monster_stomach	= { "sk_monster_stomach",	"1" };
cvar_t	sk_monster_arm		= { "sk_monster_arm",		"1" };
cvar_t	sk_monster_leg		= { "sk_monster_leg",		"1" };

// player damage adjusters
cvar_t	sk_player_head		= { "sk_player_head",		"2" };
cvar_t	sk_player_chest		= { "sk_player_chest",		"1" };
cvar_t	sk_player_stomach	= { "sk_player_stomach",	"1" };
cvar_t	sk_player_arm		= { "sk_player_arm",		"1" };
cvar_t	sk_player_leg		= { "sk_player_leg",		"1" };

// END Cvars for Skill Level settings

// Register your console variables here
// This gets called one time when the game is initialied
void InitializeCvars( void )
{
	// Register commands here:

	engine->pfnAddServerCommand( "ai_disable", CC_AI_Disable );
	engine->pfnAddServerCommand( "ai_step", CC_AI_Step );
	engine->pfnAddServerCommand( "ai_resume", CC_AI_Resume );

	// Register cvars here:

	g_psv_gravity = CVAR_GET_POINTER( "sv_gravity" );
	g_psv_aim = CVAR_GET_POINTER( "sv_aim" );
	g_footsteps = CVAR_GET_POINTER( "mp_footsteps" );

	// Grab the pointer for the cvar that checks selected chapters
	// The cvar isn't created serverside because we need it to exist as soon as the engine fully loads
	g_psv_unlockedchapters = CVAR_GET_POINTER( "sv_unlockedchapters" );

	CVAR_REGISTER (&displaysoundlist);
	CVAR_REGISTER( &allow_spectators );

	CVAR_REGISTER (&teamplay);
	CVAR_REGISTER (&fraglimit);
	CVAR_REGISTER (&timelimit);

	CVAR_REGISTER (&fragsleft);
	CVAR_REGISTER (&timeleft);

	CVAR_REGISTER (&friendlyfire);
	CVAR_REGISTER (&falldamage);
	CVAR_REGISTER (&weaponstay);
	CVAR_REGISTER (&forcerespawn);
	CVAR_REGISTER (&flashlight);
	CVAR_REGISTER (&aimcrosshair);
	CVAR_REGISTER (&decalfrequency);
	CVAR_REGISTER (&teamlist);
	CVAR_REGISTER (&teamoverride);
	CVAR_REGISTER (&defaultteam);
	CVAR_REGISTER (&allowmonsters);

	CVAR_REGISTER (&mp_chattime);

// REGISTER CVARS FOR SKILL LEVEL STUFF

	// Agrunt
	CVAR_REGISTER ( &sk_agrunt_health );
	CVAR_REGISTER ( &sk_agrunt_dmg_punch );

	// Apache
	CVAR_REGISTER ( &sk_apache_health );

	// Barney
	CVAR_REGISTER ( &sk_barney_health );

	// Bullsquid
	CVAR_REGISTER ( &sk_bullsquid_health );
	CVAR_REGISTER ( &sk_bullsquid_dmg_bite );
	CVAR_REGISTER ( &sk_bullsquid_dmg_whip );
	CVAR_REGISTER ( &sk_bullsquid_dmg_spit );

	CVAR_REGISTER ( &sk_bigmomma_health_factor );
	CVAR_REGISTER ( &sk_bigmomma_dmg_slash );
	CVAR_REGISTER ( &sk_bigmomma_dmg_blast );
	CVAR_REGISTER ( &sk_bigmomma_radius_blast );

	// Gargantua
	CVAR_REGISTER ( &sk_gargantua_health );
	CVAR_REGISTER ( &sk_gargantua_dmg_slash );
	CVAR_REGISTER ( &sk_gargantua_dmg_fire );
	CVAR_REGISTER ( &sk_gargantua_dmg_stomp );

	// Hassassin
	CVAR_REGISTER ( &sk_hassassin_health );

	// Headcrab
	CVAR_REGISTER ( &sk_headcrab_health );
	CVAR_REGISTER ( &sk_headcrab_dmg_bite );

	// Hgrunt 
	CVAR_REGISTER ( &sk_hgrunt_health );
	CVAR_REGISTER ( &sk_hgrunt_kick );
	CVAR_REGISTER ( &sk_hgrunt_pellets );
	CVAR_REGISTER ( &sk_hgrunt_gspeed );

	// Houndeye
	CVAR_REGISTER ( &sk_houndeye_health );
	CVAR_REGISTER ( &sk_houndeye_dmg_blast );

	// ISlave
	CVAR_REGISTER ( &sk_islave_health );
	CVAR_REGISTER ( &sk_islave_dmg_claw );
	CVAR_REGISTER ( &sk_islave_dmg_clawrake );
	CVAR_REGISTER ( &sk_islave_dmg_zap );

	// Icthyosaur
	CVAR_REGISTER ( &sk_ichthyosaur_health	);
	CVAR_REGISTER ( &sk_ichthyosaur_shake	);

	// Leech
	CVAR_REGISTER ( &sk_leech_health );
	CVAR_REGISTER ( &sk_leech_dmg_bite );

	// Controller
	CVAR_REGISTER ( &sk_controller_health );
	CVAR_REGISTER ( &sk_controller_dmgzap );
	CVAR_REGISTER ( &sk_controller_speedball );
	CVAR_REGISTER ( &sk_controller_dmgball );

	// Nihilanth
	CVAR_REGISTER ( &sk_nihilanth_health );
	CVAR_REGISTER ( &sk_nihilanth_zap );

	// Scientist
	CVAR_REGISTER ( &sk_scientist_health );
	CVAR_REGISTER ( &sk_scientist_heal );

	// Snark
	CVAR_REGISTER ( &sk_snark_health );
	CVAR_REGISTER ( &sk_snark_dmg_bite );
	CVAR_REGISTER ( &sk_snark_dmg_pop );

	// Zombie
	CVAR_REGISTER ( &sk_zombie_health );
	CVAR_REGISTER ( &sk_zombie_dmg_one_slash );
	CVAR_REGISTER ( &sk_zombie_dmg_both_slash );

	//Turret
	CVAR_REGISTER ( &sk_turret_health );

	// MiniTurret
	CVAR_REGISTER ( &sk_miniturret_health );

	// Sentry Turret
	CVAR_REGISTER ( &sk_sentry_health );

	// PLAYER WEAPONS

	// Crowbar whack
	CVAR_REGISTER ( &sk_plr_crowbar );

	// Glock Round
	CVAR_REGISTER ( &sk_plr_9mm_bullet );

	// 357 Round
	CVAR_REGISTER ( &sk_plr_357_bullet );

	// MP5 Round
	CVAR_REGISTER ( &sk_plr_9mmAR_bullet );

	// M203 grenade
	CVAR_REGISTER ( &sk_plr_9mmAR_grenade );

	// Shotgun buckshot
	CVAR_REGISTER ( &sk_plr_buckshot );

	// Crossbow
	CVAR_REGISTER ( &sk_plr_xbow_bolt_monster );
	CVAR_REGISTER ( &sk_plr_xbow_bolt_client );

	// RPG
	CVAR_REGISTER ( &sk_plr_rpg );

	// Gauss Gun
	CVAR_REGISTER ( &sk_plr_gauss );

	// Egon Gun
	CVAR_REGISTER ( &sk_plr_egon_narrow );

	CVAR_REGISTER ( &sk_plr_egon_wide );

	// Hand Grendade
	CVAR_REGISTER ( &sk_plr_hand_grenade );

	// Satchel Charge
	CVAR_REGISTER ( &sk_plr_satchel );

	// Tripmine
	CVAR_REGISTER ( &sk_plr_tripmine );

	// WORLD WEAPONS
	CVAR_REGISTER ( &sk_12mm_bullet );
	CVAR_REGISTER ( &sk_9mmAR_bullet );
	CVAR_REGISTER ( &sk_9mm_bullet );

	// HORNET
	CVAR_REGISTER ( &sk_hornet_dmg );

	// HEALTH/SUIT CHARGE DISTRIBUTION
	CVAR_REGISTER ( &sk_suitcharger );
	CVAR_REGISTER ( &sk_battery );
	CVAR_REGISTER ( &sk_healthcharger );
	CVAR_REGISTER ( &sk_healthkit );

	// monster damage adjusters
	CVAR_REGISTER ( &sk_monster_head );
	CVAR_REGISTER ( &sk_monster_chest );
	CVAR_REGISTER ( &sk_monster_stomach );
	CVAR_REGISTER ( &sk_monster_arm );
	CVAR_REGISTER ( &sk_monster_leg );

	// player damage adjusters
	CVAR_REGISTER ( &sk_player_head );
	CVAR_REGISTER ( &sk_player_chest );
	CVAR_REGISTER ( &sk_player_stomach );
	CVAR_REGISTER ( &sk_player_arm );
	CVAR_REGISTER ( &sk_player_leg );

// END REGISTER CVARS FOR SKILL LEVEL STUFF
}

