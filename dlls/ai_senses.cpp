//========= Copyright (c) 1996-2002, Valve LLC, All rights reserved. ==========
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================

#include "cbase.h"

#include "ai_senses.h"

#include "soundent.h"
#include "basemonster.h"
#include "player.h"
#include "saverestore_utlvector.h"

// Use this to disable caching and other optimizations in senses
//#define AI_SENSES_HOMOGENOUS_TREATMENT 1
//#define DEBUG_SENSES 1

#ifdef DEBUG_SENSES
#define AI_PROFILE_SENSES(tag) AI_PROFILE_SCOPE(tag)
#else
#define AI_PROFILE_SENSES(tag) ((void)0)
#endif

const float AI_NPC_SEARCH_TIME = .25;
const float AI_HIGH_PRIORITY_SEARCH_TIME = 0.15;

//-----------------------------------------------------------------------------

#pragma pack(push)
#pragma pack(1)

struct AISightIterVal_t
{
	short array;
	short iNext;
};

#pragma pack(pop)

//=============================================================================
//
// CAI_Senses
//
//=============================================================================

BEGIN_SIMPLE_DATADESC( CAI_Senses )

	DEFINE_FIELD( CAI_Senses, m_LookDist, FIELD_FLOAT ),
	DEFINE_FIELD( CAI_Senses, m_LastLookDist, FIELD_FLOAT ),
	DEFINE_FIELD( CAI_Senses, m_TimeLastLook, FIELD_TIME ),
	
	DEFINE_UTLVECTOR( CAI_Senses, m_SeenHighPriority, FIELD_EHANDLE ),
	DEFINE_UTLVECTOR( CAI_Senses, m_SeenMonsters, FIELD_EHANDLE ),

	DEFINE_FIELD( CAI_Senses, m_TimeLastLookHighPriority, FIELD_TIME ),
	DEFINE_FIELD( CAI_Senses, m_TimeLastLookMonsters, FIELD_TIME ),

END_DATADESC()

//-----------------------------------------------------------------------------

bool CAI_Senses::CanHearSound( CSound *pSound )
{
	if( ( pSound->m_vecOrigin - GetOuter()->EarPosition() ).Length() <= pSound->m_iVolume * GetOuter()->HearingSensitivity() )
	{
		return GetOuter()->QueryHearSound( pSound );
	}

	return false;
}

//-----------------------------------------------------------------------------
// Listen - monsters dig through the active sound list for
// any sounds that may interest them. (smells, too!)

void CAI_Senses::Listen( void )
{
	m_iAudibleList = SOUNDLIST_EMPTY; 

	int iSoundMask = GetOuter()->GetSoundInterests();
	
	if ( iSoundMask != bits_SOUND_NONE )
	{
		int	iSound = CSoundEnt::ActiveList();
		
		while ( iSound != SOUNDLIST_EMPTY )
		{
			CSound *pCurrentSound = CSoundEnt::SoundPointerForIndex( iSound );

			if ( pCurrentSound && ( iSoundMask & pCurrentSound->m_iType ) && CanHearSound( pCurrentSound ) )
			{
				// the monster cares about this sound, and it's close enough to hear.
				pCurrentSound->m_iNextAudible = m_iAudibleList;
				m_iAudibleList = iSound;
			}
			iSound = pCurrentSound->m_iNext;
		}
	}
	
	GetOuter()->OnListened();
}

//-----------------------------------------------------------------------------

bool CAI_Senses::ShouldSeeEntity( CBaseEntity *pSightEnt )
{
	if ( pSightEnt == GetOuter() || pSightEnt->pev->health <= 0 )
		return false;

	// don't see prisoners
	if ( pSightEnt->HasSpawnFlags( SF_MONSTER_PRISONER ) )
		return false;

	if ( pSightEnt->IsPlayer() && ( pSightEnt->pev->flags & FL_NOTARGET ) )
		return false;

	// don't notice anyone waiting to be seen by the player
	// NOTE: HL1 didn't have this so I decided to comment it out in case it breaks anything
	/*if ( pSightEnt->HasSpawnFlags( SF_MONSTER_WAIT_TILL_SEEN ) )
		return false;*/

	if ( !pSightEnt->CanBeSeen() )
		return false;
	
	if ( !GetOuter()->QuerySeeEntity( pSightEnt ) )
		return false;

	return true;
}

//-----------------------------------------------------------------------------

bool CAI_Senses::CanSeeEntity( CBaseEntity *pSightEnt )
{
	return ( GetOuter()->FInViewCone( pSightEnt ) && 
			 GetOuter()->FVisible( pSightEnt ) );
}

//-----------------------------------------------------------------------------

bool CAI_Senses::DidSeeEntity( CBaseEntity *pSightEnt ) const
{
	AISightIter_t iter;
	CBaseEntity *pTestEnt;

	pTestEnt = GetFirstSeenEntity( &iter );

	while( pTestEnt )
	{
		if ( pSightEnt == pTestEnt )
			return true;
		pTestEnt = GetNextSeenEntity( &iter );
	}
	return false;
}

//-----------------------------------------------------------------------------

void CAI_Senses::NoteSeenEntity( CBaseEntity *pSightEnt )
{
	pSightEnt->m_pLink = GetOuter()->m_pLink;
	GetOuter()->m_pLink = pSightEnt;
}

//-----------------------------------------------------------------------------

bool CAI_Senses::WaitingUntilSeen( CBaseEntity *pSightEnt )
{
	if ( GetOuter()->HasSpawnFlags( SF_MONSTER_WAIT_TILL_SEEN ) )
	{
		if ( pSightEnt->IsPlayer() )
		{
			CBasePlayer *pPlayer = ToBasePlayer( pSightEnt );

			Vector zero = vec3_origin;
			// don't link this client in the list if the monster is wait till seen and the player isn't facing the monster
			if ( pPlayer && FBoxVisible( pSightEnt, GetOuter(), zero ) && pPlayer->FInViewCone( GetOuter() ) )
			{
				// player sees us, become normal now.
				GetOuter()->pev->spawnflags &= ~SF_MONSTER_WAIT_TILL_SEEN;
				return false;
			}
			// Vasia: IMPORTANT!!! In the original HL2 code this bit would return true even if pSightEnt is not a client.
			// I fixed to replicate the original HL1 behavior
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

bool CAI_Senses::SeeEntity( CBaseEntity *pSightEnt )
{
	GetOuter()->OnSeeEntity( pSightEnt );

	// insert at the head of my sight list
	NoteSeenEntity( pSightEnt );

	return true;
}

//-----------------------------------------------------------------------------

CBaseEntity *CAI_Senses::GetFirstSeenEntity( AISightIter_t *pIter ) const
{ 
	COMPILE_TIME_ASSERT( sizeof( AISightIter_t ) == sizeof( AISightIterVal_t ) );
	
	AISightIterVal_t *pIterVal = (AISightIterVal_t *)pIter;
	
	for ( int i = 0; i < ARRAYSIZE( m_SeenArrays ); i++ )
	{
		if ( m_SeenArrays[i]->Count() != 0 )
		{
			pIterVal->array = i;
			pIterVal->iNext = 1;
			return (*m_SeenArrays[i])[0];
		}
	}
	
	(*pIter) = (AISightIter_t)(-1); 
	return NULL;
}

//-----------------------------------------------------------------------------

CBaseEntity *CAI_Senses::GetNextSeenEntity( AISightIter_t *pIter ) const	
{ 
	if ( ((int)*pIter) != -1 )
	{
		AISightIterVal_t *pIterVal = (AISightIterVal_t *)pIter;
		
		for ( int i = pIterVal->array;  i < ARRAYSIZE( m_SeenArrays ); i++ )
		{
			for ( int j = pIterVal->iNext; j < m_SeenArrays[i]->Count(); j++ )
			{
				if ( (*m_SeenArrays[i])[j].Get() != NULL )
				{
					pIterVal->array = i;
					pIterVal->iNext = j+1;
					return (*m_SeenArrays[i])[j];
				}
			}
			pIterVal->iNext = 0;
		}
		(*pIter) = (AISightIter_t)(-1); 
	}
	return NULL;
}

//-----------------------------------------------------------------------------

void CAI_Senses::BeginGather()
{
	// clear my sight list
	GetOuter()->m_pLink = NULL;
}

//-----------------------------------------------------------------------------

void CAI_Senses::EndGather( int nSeen, CUtlVector<EHANDLE> *pResult )
{
	pResult->SetCount( nSeen );
	if ( nSeen )
	{
		CBaseEntity *pCurrent = GetOuter()->m_pLink;
		for (int i = 0; i < nSeen; i++ )
		{
			Assert( pCurrent );
			(*pResult)[i].Set( pCurrent->edict() );
			pCurrent = pCurrent->m_pLink;
		}
		GetOuter()->m_pLink = NULL;
	}
}

//-----------------------------------------------------------------------------
// Look - Base class monster function to find enemies or 
// food by sight. iDistance is distance ( in units ) that the 
// monster can see.
//
// Sets the sight bits of the m_Conditions mask to indicate
// which types of entities were sighted.
// Function also sets the Looker's m_pLink 
// to the head of a link list that contains all visible ents.
// (linked via each ent's m_pLink field)
//

void CAI_Senses::Look( int iDistance )
{
	// See no evil if prisoner is set
	if ( ( m_TimeLastLook != gpGlobals->time || m_LastLookDist != iDistance ) && !GetOuter()->HasSpawnFlags( SF_MONSTER_PRISONER ) )
	{
		//-----------------------------
		
		LookForHighPriorityEntities( iDistance );
		LookForMonsters( iDistance);
		
		//-----------------------------
		
		m_LastLookDist = iDistance;
		m_TimeLastLook = gpGlobals->time;
	}
	
	GetOuter()->OnLooked();
}

//-----------------------------------------------------------------------------

bool CAI_Senses::Look( CBaseEntity *pSightEnt )
{
	if ( WaitingUntilSeen( pSightEnt ) )
		return false;
	
	if ( ShouldSeeEntity( pSightEnt ) && CanSeeEntity( pSightEnt ) )
	{
		return SeeEntity( pSightEnt );
	}
	return false;
}


//-----------------------------------------------------------------------------

int CAI_Senses::LookForHighPriorityEntities( int iDistance )
{
	int nSeen = 0;
	if ( gpGlobals->time - m_TimeLastLookHighPriority > AI_HIGH_PRIORITY_SEARCH_TIME )
	{
		AI_PROFILE_SENSES(CAI_Senses_LookForHighPriorityEntities);
		m_TimeLastLookHighPriority = gpGlobals->time;
		
		BeginGather();
	
		float distSq = ( iDistance * iDistance );
		const Vector &origin = GetOrigin();
		
		// Players
		for ( int i = 1; i <= gpGlobals->maxClients; i++ )
		{
			CBaseEntity *pPlayer = UTIL_PlayerByIndex( i );

			if ( pPlayer )
			{
				if ( origin.DistToSqr( pPlayer->pev->origin ) < distSq && Look( pPlayer ) )
				{
					nSeen++;
				}
			}
		}
	
		EndGather( nSeen, &m_SeenHighPriority );
    }
    else
    {
		for ( int i = m_SeenHighPriority.Count() - 1; i >= 0; --i )
    	{
    		if ( m_SeenHighPriority[i].Get() == NULL )
    			m_SeenHighPriority.FastRemove( i );    			
    	}
    	nSeen = m_SeenHighPriority.Count();
    }
	
	return nSeen;
}

//-----------------------------------------------------------------------------

int CAI_Senses::LookForMonsters( int iDistance )
{
	int nSeen = 0;
	if ( gpGlobals->time - m_TimeLastLookMonsters > AI_NPC_SEARCH_TIME )
	{
		m_TimeLastLookMonsters = gpGlobals->time;

		BeginGather();

		float distSq = ( iDistance * iDistance );
		const Vector &origin = GetOrigin();

		CBaseMonster **ppAIs = g_AI_Manager.AccessAIs();
		
		for ( int i = 0; i < g_AI_Manager.NumAIs(); i++ )
		{
			if ( ppAIs[i] != GetOuter() && origin.DistToSqr( ppAIs[i]->pev->origin ) < distSq )
			{
				if ( Look( ppAIs[i] ) )
				{
					nSeen++;
				}
			}
		}

		EndGather( nSeen, &m_SeenMonsters );
	}
    else
    {
		for ( int i = m_SeenMonsters.Count() - 1; i >= 0; --i )
    	{
    		if ( m_SeenMonsters[i].Get() == NULL )
				m_SeenMonsters.FastRemove( i );
    	}
    	nSeen = m_SeenMonsters.Count();
    }
	
	return nSeen;
}

//-----------------------------------------------------------------------------

float CAI_Senses::GetTimeLastUpdate( CBaseEntity *pEntity )
{
	if ( !pEntity )
		return 0.0;

	if ( pEntity->IsPlayer() )
		return m_TimeLastLookHighPriority;

	if ( pEntity->pev->flags & FL_MONSTER )
		return m_TimeLastLookMonsters;

	return 0.0;
}

//-----------------------------------------------------------------------------

CSound* CAI_Senses::GetFirstHeardSound( AISoundIter_t *pIter )
{
	int iFirst = GetAudibleList(); 

	if ( iFirst == SOUNDLIST_EMPTY )
	{
		*pIter = NULL;
		return NULL;
	}
	
	*pIter = (AISoundIter_t)iFirst;
	return CSoundEnt::SoundPointerForIndex( iFirst );
}

//-----------------------------------------------------------------------------

CSound* CAI_Senses::GetNextHeardSound( AISoundIter_t *pIter )
{
	if ( !*pIter )
		return NULL;

	int iCurrent = (int)*pIter;
	
	Assert( iCurrent != SOUNDLIST_EMPTY );
	if ( iCurrent == SOUNDLIST_EMPTY )
	{
		*pIter = NULL;
		return NULL;
	}
	
	iCurrent = CSoundEnt::SoundPointerForIndex( iCurrent )->m_iNextAudible;
	if ( iCurrent == SOUNDLIST_EMPTY )
	{
		*pIter = NULL;
		return NULL;
	}
	
	*pIter = (AISoundIter_t)iCurrent;
	return CSoundEnt::SoundPointerForIndex( iCurrent );
}

//-----------------------------------------------------------------------------

CSound *CAI_Senses::GetClosestSound( bool fScent )
{
	float flBestDist = 8192 * 8192;// so first nearby sound will become best so far.
	float flDist;
	
	AISoundIter_t iter;
	
	CSound *pResult = NULL;
	CSound *pCurrent = GetFirstHeardSound( &iter );

	Vector earPosition = GetOuter()->EarPosition();
	
	while ( pCurrent )
	{
		if ( ( !fScent && pCurrent->FIsSound() ) || 
			 ( fScent && pCurrent->FIsScent() ) )
		{
			flDist = ( pCurrent->m_vecOrigin - earPosition ).LengthSqr();

			if ( flDist < flBestDist )
			{
				pResult = pCurrent;
				flBestDist = flDist;
			}
		}
		
		pCurrent = GetNextHeardSound( &iter );
	}
	
	return pResult;
}

//-----------------------------------------------------------------------------

void CAI_Senses::PerformSensing( void )
{
	// -----------------
	//  Look	
	// -----------------
	Look( m_LookDist );

	// ------------------
	//  Listen
	// ------------------
	Listen();
}

//=============================================================================
