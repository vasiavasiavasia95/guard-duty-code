//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Base class for all animating characters and objects.
//
//=============================================================================//

#include "cbase.h"
#include "baseanimating.h"
#include "animation.h"
#include "studio.h"
#include "saverestore.h"

#define MAX_ANIMTIME_INTERVAL 0.2

#define clamp( val, min, max ) ( ((val) > (max)) ? (max) : ( ((val) < (min)) ? (min) : (val) ) )

extern void StudioGetAttachment( const edict_t *pEdict, int iAttachment, float *rgflOrigin, float *rgflAngles );

BEGIN_DATADESC( CBaseAnimating )

	DEFINE_FIELD( CBaseAnimating, m_flFrameRate, FIELD_FLOAT ),
	DEFINE_FIELD( CBaseAnimating, m_flGroundSpeed, FIELD_FLOAT ),
	DEFINE_FIELD( CBaseAnimating, m_flLastEventCheck, FIELD_FLOAT ),
	DEFINE_FIELD( CBaseAnimating, m_fSequenceFinished, FIELD_BOOLEAN ),
	DEFINE_FIELD( CBaseAnimating, m_fSequenceLoops, FIELD_BOOLEAN ),

	DEFINE_FIELD( CBaseAnimating, m_flPrevAnimTime, FIELD_TIME ),

END_DATADESC()


float CBaseAnimating::GetAnimTimeInterval( void )
{
	float flInterval;
	if ( pev->animtime < gpGlobals->time)
	{
		// estimate what it'll be this frame
		flInterval = clamp( gpGlobals->time - pev->animtime, 0, MAX_ANIMTIME_INTERVAL );
	}
	else
	{ 
		// report actual
		flInterval = clamp( pev->animtime - m_flPrevAnimTime, 0, MAX_ANIMTIME_INTERVAL );
	}
	return flInterval;
}

//=========================================================
// StudioFrameAdvance - advance the animation frame up to the current time
// if an flInterval is passed in, only advance animation that number of seconds
//=========================================================
void CBaseAnimating::StudioFrameAdvance( void )
{
	if ( !m_flPrevAnimTime )
	{
		m_flPrevAnimTime = pev->animtime;
	}

	// Time since last animation
	float flInterval = gpGlobals->time - pev->animtime;
	flInterval = clamp( flInterval, 0, MAX_ANIMTIME_INTERVAL );

	//ALERT(at_console, "%i %s interval %f\n", entindex(), STRING(pev->classname), flInterval );

	if (flInterval <= 0.001)
	{
		//ALERT(at_console, "%s : %s : %5.3f (skip)\n", STRING(pev->classname), GetSequenceName( pev->sequence ), pev->frame );
		return;
	}

	// Latch prev
	m_flPrevAnimTime = pev->animtime;

	// Set current
	pev->animtime = gpGlobals->time;

	// Drive frame
	pev->frame += flInterval * m_flFrameRate * pev->framerate;

	if (pev->frame < 0.0 || pev->frame >= 256.0) 
	{
		if (m_fSequenceLoops)
			pev->frame -= (int)(pev->frame / 256.0) * 256.0;
		else
			pev->frame = (pev->frame < 0.0) ? 0 : 256.0;

		m_fSequenceFinished = true;	// just in case it wasn't caught in GetEvents
	}

}

//=========================================================
// LookupActivity
//=========================================================
int CBaseAnimating::LookupActivity( int activity )
{
	Assert( activity != 0 );
	return ::LookupActivity( GetModelPtr(), activity );
}

//=========================================================
// LookupActivityHeaviest
//
// Get activity with highest 'weight'
//
//=========================================================
int CBaseAnimating::LookupActivityHeaviest( int activity )
{
	return ::LookupActivityHeaviest( GetModelPtr(), activity );
}

//=========================================================
//=========================================================
int CBaseAnimating::LookupSequence( const char *label )
{
	return ::LookupSequence( GetModelPtr(), label );
}

const char *CBaseAnimating::GetSequenceName( int iSequence )
{
	if( iSequence == -1 )
	{
		return "Not Found!";
	}

	if ( !GetModelPtr() )
		return "No model!";

	return ::GetSequenceName( GetModelPtr(), iSequence );
}

//=========================================================
//=========================================================
void CBaseAnimating::ResetSequenceInfo( )
{
	if ( pev->sequence == -1 )
	{
		// This shouldn't happen.	Setting pev->sequence blindly is a horrible coding practice.
		pev->sequence = 0;
	}

	GetSequenceInfo( GetModelPtr(), pev->sequence, &m_flFrameRate, &m_flGroundSpeed );
	m_fSequenceLoops = ((GetSequenceFlags( pev->sequence ) & STUDIO_LOOPING) != 0);
	// Vasia: Source has this commented out which causes small snaps on animated objects.
	// TODO: Check if this causes animation issues
	//pev->animtime = m_flPrevAnimTime = gpGlobals->time; 
	pev->framerate = 1.0;
	m_fSequenceFinished = false;
	m_flLastEventCheck = 0;
}

//=========================================================
//=========================================================
int CBaseAnimating::GetSequenceFlags( int iSequence )
{
	return ::GetSequenceFlags( GetModelPtr(), iSequence );
}

//=========================================================
// DispatchAnimEvents
//=========================================================
void CBaseAnimating::DispatchAnimEvents( void )
{
	animevent_t	event;

	studiohdr_t *pstudiohdr = GetModelPtr( );

	if ( !pstudiohdr )
	{
		ALERT( at_aiconsole, "Gibbed monster is thinking!\n" );
		return;
	}

	// don't fire events if the framerate is 0, and skip this altogether if there are no events
	if ( pev->framerate == 0.0 || pstudiohdr->pSeqdesc( pev->sequence )->numevents == 0 )
	{
		return;
	}

	// look from when it last checked to some short time in the future	
	float flCycleRate = m_flFrameRate * pev->framerate;
	float flStart = m_flLastEventCheck;
	float flEnd = pev->frame;

	if (!m_fSequenceLoops && m_fSequenceFinished )
	{
		// HACK!!! This magic number here is necessary to fix
		// events on last frame getting skipped
		// Valve sets flEnd to 1.01 here 
		// so we do the same but in Goldsrc's anim cycle range
		flEnd = 258.5f;
	}
	m_flLastEventCheck = flEnd;

	// FIXME: does not handle negative framerates!
	int index = 0;
	while ( (index = GetAnimationEvent( pstudiohdr, pev->sequence, &event, flStart, flEnd, index ) ) != 0 )
	{
		HandleAnimEvent( &event );
	}
}

studiohdr_t *CBaseAnimating::GetModelPtr( void ) 
{ 
	void *pmodel = GET_MODEL_PTR( ENT(pev) );
	if ( !pmodel )
		return NULL;

	// NOTE: The engine actually never checks if the extracted data is a studio model
	return static_cast< studiohdr_t * >( pmodel ); 
}

//=========================================================
//=========================================================
float CBaseAnimating::SetBoneController( int iController, float flValue )
{
	return SetController( GetModelPtr(), pev, iController, flValue );
}

//=========================================================
//=========================================================
void CBaseAnimating :: InitBoneControllers ( void )
{
	studiohdr_t *pmodel = GetModelPtr();

	SetController( pmodel, pev, 0, 0.0 );
	SetController( pmodel, pev, 1, 0.0 );
	SetController( pmodel, pev, 2, 0.0 );
	SetController( pmodel, pev, 3, 0.0 );
}

//=========================================================
//=========================================================
float CBaseAnimating::SetBlending( int iBlender, float flValue )
{
	return ::SetBlending( GetModelPtr(), pev, iBlender, flValue );
}

//-----------------------------------------------------------------------------
// Purpose: Returns index number of a given named bone
// Input  : name of a bone
// Output :	Bone index number or -1 if bone not found
//-----------------------------------------------------------------------------
int CBaseAnimating::LookupBone( const char *szName )
{
	return FindBoneIndexByName( GetModelPtr(), szName );
}

//=========================================================
//=========================================================
void CBaseAnimating::GetBonePosition( int iBone, Vector &origin, Vector &angles )
{
	GET_BONE_POSITION( ENT(pev), iBone, origin, angles );
}

//=========================================================
//=========================================================
void CBaseAnimating::GetAttachment( int iAttachment, Vector &origin, Vector &angles )
{
	StudioGetAttachment( ENT(pev), iAttachment, origin, angles );
}

//=========================================================
//=========================================================
int CBaseAnimating::FindTransition( int iEndingSequence, int iGoalSequence, int *piDir )
{
	if (piDir == NULL)
	{
		int iDir;
		int sequence = ::FindTransition( GetModelPtr(), iEndingSequence, iGoalSequence, &iDir );
		if (iDir != 1)
			return -1;
		else
			return sequence;
	}

	return ::FindTransition( GetModelPtr(), iEndingSequence, iGoalSequence, piDir );
}

//=========================================================
//=========================================================
void CBaseAnimating::GetAutomovement( Vector &origin, Vector &angles, float flInterval )
{

}

void CBaseAnimating::SetBodygroup( int iGroup, int iValue )
{
	int newBody = pev->body;
	::SetBodygroup( GetModelPtr(), newBody, iGroup, iValue );
	pev->body = newBody;
}

int CBaseAnimating::GetBodygroup( int iGroup )
{
	return ::GetBodygroup( GetModelPtr(), pev->body, iGroup );
}


int CBaseAnimating::ExtractBbox( int sequence, Vector& mins, Vector& maxs )
{
	return ::ExtractBbox( GetModelPtr( ), sequence, mins, maxs );
}

//=========================================================
//=========================================================

void CBaseAnimating::SetSequenceBox( void )
{
	Vector mins, maxs;

	// Get sequence bbox
	if ( ExtractBbox( pev->sequence, mins, maxs ) )
	{
		// expand box for rotation
		// find min / max for rotations
		float yaw = pev->angles.y * (M_PI / 180.0);
		
		Vector xvector, yvector;
		xvector.x = cos(yaw);
		xvector.y = sin(yaw);
		yvector.x = -sin(yaw);
		yvector.y = cos(yaw);
		Vector bounds[2];

		bounds[0] = mins;
		bounds[1] = maxs;
		
		Vector rmin( 9999, 9999, 9999 );
		Vector rmax( -9999, -9999, -9999 );
		Vector base, transformed;

		for (int i = 0; i <= 1; i++ )
		{
			base.x = bounds[i].x;
			for ( int j = 0; j <= 1; j++ )
			{
				base.y = bounds[j].y;
				for ( int k = 0; k <= 1; k++ )
				{
					base.z = bounds[k].z;
					
				// transform the point
					transformed.x = xvector.x*base.x + yvector.x*base.y;
					transformed.y = xvector.y*base.x + yvector.y*base.y;
					transformed.z = base.z;
					
					if (transformed.x < rmin.x)
						rmin.x = transformed.x;
					if (transformed.x > rmax.x)
						rmax.x = transformed.x;
					if (transformed.y < rmin.y)
						rmin.y = transformed.y;
					if (transformed.y > rmax.y)
						rmax.y = transformed.y;
					if (transformed.z < rmin.z)
						rmin.z = transformed.z;
					if (transformed.z > rmax.z)
						rmax.z = transformed.z;
				}
			}
		}
		rmin.z = 0;
		rmax.z = rmin.z + 1;
		UTIL_SetSize( pev, rmin, rmax );
	}
}

