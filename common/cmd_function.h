/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#ifndef CMD_FUNCTION_H
#define CMD_FUNCTION_H
#if defined( _WIN32 )
#pragma once
#endif

typedef void (*xcommand_t) (void);

#define	FCMD_HUD_COMMAND		(1<<0)	// This is a HUD (client library) command
#define	FCMD_GAME_COMMAND		(1<<1)	// This is a game (server library) command
#define	FCMD_WRAPPER_COMMAND	(1<<2)	// This is a wrapper (GameUI library) command
#define FCMD_PRIVILEGED_COMMAND	(1<<3)	// This command is only available in privileged mode
#define FCMD_FILTERABLE_COMMAND (1<<4)	// This command is filtered in unprivileged mode if cl_filterstuffcmd is 1

// Represents a command function that can be executed.
typedef struct cmd_function_s
{
	struct cmd_function_s	*next;		// Next function in the list.
	const char				*name;
	xcommand_t				function;	// Function to execute
	int						flags;

} cmd_function_t;

#endif //CMD_FUNCTION_H
