#if !defined( INPUT_H )
#define INPUT_H
#ifdef _WIN32
#pragma once
#endif

#include "kbutton.h"
#include "usercmd.h"

// Obsolete
/*typedef struct kblist_s
{
	struct kblist_s *next;
	kbutton_t *pkey;
	char name[32];
} kblist_t;*/

class CKeyboardKey
{
public:
	// Name for key
	char				name[ 32 ];
	// Pointer to the underlying structure
	struct kbutton_s	*pkey;
	// Next key in key list.
	CKeyboardKey		*next;
};

typedef struct
{
	unsigned int AxisFlags;
	unsigned int AxisMap;
	unsigned int ControlMap;
	int			pRawValue;
} joy_axis_t;

enum _ControlList
{
	AxisNada = 0,
	AxisForward,
	AxisLook,
	AxisSide,
	AxisTurn
};

// joystick defines and variables
// where should defines be moved?
#define JOY_ABSOLUTE_AXIS	0x00000000		// control like a joystick
#define JOY_RELATIVE_AXIS	0x00000010		// control like a mouse, spinner, trackball
#define	JOY_MAX_AXES		6				// X, Y, Z, R, U, V
#define JOY_AXIS_X			0
#define JOY_AXIS_Y			1
#define JOY_AXIS_Z			2
#define JOY_AXIS_R			3
#define JOY_AXIS_U			4
#define JOY_AXIS_V			5

#define MOUSE_BUTTON_COUNT 5

class CInput
{
// Interface
public:

	// Construction/Destruction
	CInput( void );
	~CInput( void );

public:

	// Initialization/shutdown of the subsystem
	virtual	void	Init_All( void );
	virtual void	Shutdown_All( void );

	virtual void	Init_Keyboard( void );
	virtual void	Shutdown_Keyboard( void );

	virtual void	Init_Mouse( void );
	virtual void	Shutdown_Mouse( void );
	virtual void	StartupMouse( void );

	virtual void	Init_Joystick( void );
	virtual void	StartupJoystick( void );

	virtual void	Init_Camera( void );

	// Latching button states
	//virtual int	GetButtonBits( int );

	// Create movement command
	virtual void	CreateMove ( float frametime, struct usercmd_s *cmd, int active );

	// Issue key event
	virtual int		KeyEvent( int down, int keynum, const char *pszCurrentBinding );

	// Look for key
	virtual struct kbutton_s *FindKey( const char *name );

	// Add a named key to the list queryable by the engine
	virtual void	AddKeyButton( const char *name, kbutton_t *pkb );

	// Accumulate mouse delta
	virtual	void	AccumulateMouse( void );

	// Notify about mouse event
	virtual	void	MouseEvent( int mstate );

	// Activate/deactivate mouse
	virtual	void	ActivateMouse( void );
	virtual	void	DeactivateMouse( void );

	// Clear mouse state data
	virtual	void	ClearStates( void );

	virtual	void	ResetMouse( void );

	// Apply custom mouse scaling/acceleration
	virtual	void	ScaleMouse( int *x, int *y );

	virtual void	RunInputMove ( float frametime, usercmd_t *cmd );

	virtual void	MouseMove ( usercmd_t *cmd );

	virtual void	GetAccumulatedMouse( int *mx, int *my );

	virtual void	GetMouseDelta( int mx, int my, int *oldx, int *oldy, int *x, int *y );

	virtual int		RawValuePointer( int axis );

	virtual	void	ControllerCommands( void );
	virtual	void	Joystick_Advanced( void );

	virtual void	JoyStickMove ( float frametime, usercmd_t *cmd );

	virtual int		ReadJoystick( void );

	// Third Person camera
	virtual void	CAM_Think( void );
	virtual int		CAM_IsThirdPerson( void );
	virtual void	CAM_GetCameraOffset( float *ofs );
	virtual void	CAM_ToThirdPerson(void);
	virtual void	CAM_ToFirstPerson(void);
	virtual void	CAM_StartMouseMove(void);
	virtual void	CAM_EndMouseMove(void);
	virtual void	CAM_StartDistance(void);
	virtual void	CAM_EndDistance(void);
	virtual int		CAM_InterceptingMouse( void );

	// Mouse/keyboard movement input helpers
	virtual void	ScaleMovements( usercmd_t *cmd );
	virtual void	ComputeForwardMove( usercmd_t *cmd );
	virtual void	ComputeUpwardMove( usercmd_t *cmd );
	virtual void	ComputeSideMove( usercmd_t *cmd );
	virtual	void	AdjustAngles ( float frametime );
	virtual void	ClampAngles(  Vector &viewangles );
	virtual void	AdjustPitch( float speed, Vector &viewangles );
	virtual void	AdjustYaw( float speed, Vector &viewangles );
	virtual	float	DetermineKeySpeed( float frametime );

public:

	// Has the mouse been initialized?
	int			m_nMouseInitialized;

	// Is the mouse active?
	int			m_nMouseActive;

	// Is there a joystick?
	int			m_fJoystickAvailable;

	// Has the joystick advanced initialization been run?
	int			m_fJoystickAdvancedInit;

	// Does the joystick have a POV control?
	int			m_fJoystickHasPOVControl;

	// Number of buttons on joystick
	int			m_nJoystickButtons;

	// Which joystick are we using?
	int			m_nJoystickID;

	// Old Joystick button states
	unsigned int m_nJoystickOldButtons;
	unsigned int m_nJoystickOldPOVState;

	// Number of mouse buttons
	int			m_nMouseButtons;

	// Old button states
	int			m_nMouseOldButtons;

	// Accumulated mouse deltas
	int			m_nXAccum;
	int			m_nYAccum;

	// Flag to restore systemparameters when exiting
	int			m_fRestoreSPI;

	// Original mouse parameters
	int			m_rgOrigMouseParms[ 3 ];

	// Current mouse parameters.
	int			m_rgNewMouseParms[ 3 ];

	// Are the parameters valid
	int			m_fMouseParmsValid;

	bool		m_bRawInput;

	// Joystick Axis data
	joy_axis_t m_rgAxes[ JOY_MAX_AXES ];

	// List of queryable keys
	CKeyboardKey *m_pKeys;

	// Is the 3rd person camera using the mouse?
	int			m_fCameraInterceptingMouse;

	// Are we in 3rd person view?
	int			m_fCameraInThirdPerson;

	// Should we move view along with mouse?
	int			m_fCameraMovingWithMouse;

	// What is the current camera offset from the view origin?
	Vector		m_vecCameraOffset;

	// Is the camera in distance moving mode?
	int			m_fCameraDistanceMove;

	// Old and current mouse position readings.
	int			m_nCameraOldX;
	int			m_nCameraOldY;
	int			m_nCameraX;
	int			m_nCameraY;

	Vector		m_vecPreviousViewAngles;
};

#endif // INPUT_H
