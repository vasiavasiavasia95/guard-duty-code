/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
// hud_items.cpp
//
// implementation of CHudItems class
//

#include "cl_dll.h"
#include "hud.h"
#include "hud_macros.h"

#include <string.h>
#include <stdio.h>

//MODDD - new stuff
DECLARE_MESSAGE( m_Items, AntidoteP)
DECLARE_MESSAGE( m_Items, RadiationP)

int CHudItems::Init(void)
{
	gHUD.AddHudElem(this);

	//MODDD - CUSTOM MESSAGE
	HOOK_MESSAGE(AntidoteP);
	HOOK_MESSAGE(RadiationP);

	Reset();

	m_iFlags |= HUD_ACTIVE; //!!!

	return 1;
};

void CHudItems::Reset(void)
{
	m_antidotes = 0;
	m_radiations = 1;
}

int CHudItems::VidInit(void)
{
	m_antidoteindex = gHUD.GetSpriteIndex("antidote");
	m_radiationindex = gHUD.GetSpriteIndex("radiation");
	return 1;
}

//MODDD - needless to say, the next 3 methods are new.
int CHudItems:: MsgFunc_AntidoteP(const char *pszName,  int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );
	int x = READ_SHORT();

	m_antidotes = x;

	/*if (x != m_iBat)
	{
		m_fFade = FADE_TIME;
		
	}
	m_iBat = 100;
	gHUD.m_Health.m_iHealth = 100;
	*/

	return 1;
}

int CHudItems:: MsgFunc_RadiationP(const char *pszName,  int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );
	int x = READ_SHORT();

	m_radiations = x;

	return 1;
}


int CHudItems::Draw(float flTime)
{
	int x, y, r, g, b;

	if (!(gHUD.m_iWeaponBits & (1<<(WEAPON_SUIT)) ))
		return 1;
	
	if ( !gpActiveSel )
		return 0;
	
	//each is the same size.
	int inventoryIconWidth = gHUD.GetSpriteRect(m_antidoteindex).right - gHUD.GetSpriteRect(m_antidoteindex).left;
	int inventoryIconHeight = gHUD.GetSpriteRect(m_antidoteindex).bottom - gHUD.GetSpriteRect(m_antidoteindex).top;

	float flIconGap = inventoryIconHeight / 6;

	//UnpackRGB( r, g, b, RGB_YELLOWISH);

	//RADIATION
	//////////////////////////////////////////////////////////////////////
	x = gHUD.m_iHudScaleWidth - inventoryIconWidth - 20;
	y = flIconGap * 2 + inventoryIconHeight;

	if ( m_radiations > 0 )
	{
		UnpackRGB( r, g, b, RGB_YELLOWISH );
	}
	else
	{
		UnpackRGB( r, g, b, RGB_REDISH );
		ScaleColors(r, g, b, 128);
	}

	SPR_Set(gHUD.GetSprite(m_radiationindex), r, g, b);
	SPR_DrawAdditive(0, x, y, &gHUD.GetSpriteRect(m_radiationindex));

	x += 4;
	y += 38;

	// Draw the radiation number (adjust these coords if needed)
	SPR_Set( gHUD.GetSprite(gHUD.m_HUD_bucket_0 + m_radiations), r, g, b );
	SPR_DrawAdditive( 0, x, y, &gHUD.GetSpriteRect(gHUD.m_HUD_bucket_0 + m_radiations) );
	//////////////////////////////////////////////////////////////////////


	//ANTIDOTE
	//////////////////////////////////////////////////////////////////////
	x = gHUD.m_iHudScaleWidth - inventoryIconWidth - 20;
	y = flIconGap * 9 + inventoryIconHeight;

	if ( m_antidotes > 0 )
	{
		UnpackRGB( r, g, b, RGB_YELLOWISH );
	}
	else
	{
		UnpackRGB( r, g, b, RGB_REDISH );
		ScaleColors(r, g, b, 128);
	}

	SPR_Set(gHUD.GetSprite(m_antidoteindex), r, g, b);
	SPR_DrawAdditive(0, x, y, &gHUD.GetSpriteRect(m_antidoteindex));

	x += 4;
	y += 38;

	//Draw the antidote number (adjust these coords if needed)
	SPR_Set( gHUD.GetSprite(gHUD.m_HUD_bucket_0 + m_antidotes), r, g, b );
	SPR_DrawAdditive( 0, x, y, &gHUD.GetSpriteRect(gHUD.m_HUD_bucket_0 + m_antidotes) );
	//////////////////////////////////////////////////////////////////////

	return 1;
}