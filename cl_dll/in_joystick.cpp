//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Joystick handling function
//
// $NoKeywords: $
//===========================================================================//

#include "port.h"

#include "cl_dll.h"
#include "kbutton.h"
#include "cvardef.h"
#include "usercmd.h"
#include "in_defs.h"
#include "../public/keydefs.h"
#include "view.h"
#include "Exports.h"
#include "input.h"

#include <SDL2/SDL_gamecontroller.h>

SDL_GameController *s_pJoystick = NULL;

// none of these cvars are saved over a session
// this means that advanced controller configuration needs to be executed
// each time.  this avoids any problems with getting back to a default usage
// or when changing from one controller to another.  this way at least something
// works.
cvar_t	*in_joystick;
cvar_t	*joy_name;
cvar_t	*joy_advanced;
cvar_t	*joy_advaxisx;
cvar_t	*joy_advaxisy;
cvar_t	*joy_advaxisz;
cvar_t	*joy_advaxisr;
cvar_t	*joy_advaxisu;
cvar_t	*joy_advaxisv;
cvar_t	*joy_forwardthreshold;
cvar_t	*joy_sidethreshold;
cvar_t	*joy_pitchthreshold;
cvar_t	*joy_yawthreshold;
cvar_t	*joy_forwardsensitivity;
cvar_t	*joy_sidesensitivity;
cvar_t	*joy_pitchsensitivity;
cvar_t	*joy_yawsensitivity;
cvar_t	*joy_wwhack1;
cvar_t	*joy_wwhack2;

extern cvar_t	*m_pitch;

extern cvar_t *lookstrafe;
extern cvar_t *lookspring;
extern cvar_t *cl_pitchdown;
extern cvar_t *cl_pitchup;
extern cvar_t *cl_yawspeed;
extern cvar_t *cl_sidespeed;
extern cvar_t *cl_forwardspeed;
extern cvar_t *cl_pitchspeed;
extern cvar_t *cl_movespeedkey;

extern kbutton_t in_strafe;
extern kbutton_t in_mlook;
extern kbutton_t in_speed;
extern kbutton_t in_jlook;

/* 
=============== 
IN_StartupJoystick 
=============== 
*/  
void CInput::StartupJoystick (void)
{ 
	// abort startup if user requests no joystick
	if ( engine->CheckParm ("-nojoy", NULL ) ) 
		return; 
 
 	// assume no joystick
	m_fJoystickAvailable = 0;

	int nJoysticks = SDL_NumJoysticks();
	if ( nJoysticks > 0 )
	{
		for ( int i = 0; i < nJoysticks; i++ )
		{
			if ( SDL_IsGameController( i ) )
			{
				s_pJoystick = SDL_GameControllerOpen( i );
				if ( s_pJoystick )
				{
					//save the joystick's number of buttons and POV status
					m_nJoystickButtons = SDL_CONTROLLER_BUTTON_MAX;
					m_fJoystickHasPOVControl = 0;
					
					// old button and POV states default to no buttons pressed
					m_nJoystickOldButtons = 0;
					m_nJoystickOldPOVState = 0;
					
					// mark the joystick as available and advanced initialization not completed
					// this is needed as cvars are not available during initialization
					engine->Con_Printf ("joystick found\n\n", SDL_GameControllerName(s_pJoystick)); 
					m_fJoystickAvailable = 1;
					m_fJoystickAdvancedInit = 0;
					break;
				}

			}
		}
	}
	else
	{
		engine->Con_DPrintf ("joystick not found -- driver not present\n\n");
	}
	
}

int CInput::RawValuePointer ( int axis )
{
	switch (axis)
	{
		default:
		case JOY_AXIS_X:
			return SDL_GameControllerGetAxis( s_pJoystick, SDL_CONTROLLER_AXIS_LEFTX );
		case JOY_AXIS_Y:
			return SDL_GameControllerGetAxis( s_pJoystick, SDL_CONTROLLER_AXIS_LEFTY );
		case JOY_AXIS_Z:
			return SDL_GameControllerGetAxis( s_pJoystick, SDL_CONTROLLER_AXIS_RIGHTX );
		case JOY_AXIS_R:
			return SDL_GameControllerGetAxis( s_pJoystick, SDL_CONTROLLER_AXIS_RIGHTY );
		
	}
}


void CInput::Joystick_Advanced( void )
{

	// called once by IN_ReadJoystick and by user whenever an update is needed
	// cvars are now available
	int	i;
	DWORD dwTemp;

	// initialize all the maps
	for (i = 0; i < JOY_MAX_AXES; i++)
	{
		m_rgAxes[i].AxisMap = AxisNada;
		m_rgAxes[i].ControlMap = JOY_ABSOLUTE_AXIS;
		m_rgAxes[i].pRawValue = RawValuePointer(i);
	}

	if( joy_advanced->value == 0.0)
	{
		// default joystick initialization
		// 2 axes only with joystick control
		m_rgAxes[JOY_AXIS_X].AxisMap = AxisTurn;
		m_rgAxes[JOY_AXIS_Y].AxisMap = AxisForward;
	}
	else
	{
		if ( strcmp ( joy_name->string, "joystick") != 0 )
		{
			// notify user of advanced controller
			engine->Con_Printf ("\n%s configured\n\n", joy_name->string);
		}

		// advanced initialization here
		// data supplied by user via joy_axisn cvars
		dwTemp = (DWORD) joy_advaxisx->value;
		m_rgAxes[JOY_AXIS_X].AxisMap = dwTemp & 0x0000000f;
		m_rgAxes[JOY_AXIS_X].ControlMap = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD) joy_advaxisy->value;
		m_rgAxes[JOY_AXIS_Y].AxisMap = dwTemp & 0x0000000f;
		m_rgAxes[JOY_AXIS_Y].ControlMap = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD) joy_advaxisz->value;
		m_rgAxes[JOY_AXIS_Z].AxisMap = dwTemp & 0x0000000f;
		m_rgAxes[JOY_AXIS_Z].ControlMap = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD) joy_advaxisr->value;
		m_rgAxes[JOY_AXIS_R].AxisMap = dwTemp & 0x0000000f;
		m_rgAxes[JOY_AXIS_R].ControlMap = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD) joy_advaxisu->value;
		m_rgAxes[JOY_AXIS_U].AxisMap = dwTemp & 0x0000000f;
		m_rgAxes[JOY_AXIS_U].ControlMap = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD) joy_advaxisv->value;
		m_rgAxes[JOY_AXIS_V].AxisMap = dwTemp & 0x0000000f;
		m_rgAxes[JOY_AXIS_V].ControlMap = dwTemp & JOY_RELATIVE_AXIS;
	}
}

/*
===========
ControllerCommands
===========
*/
void CInput::ControllerCommands( void )
{
	int		i, key_index;

	if (!m_fJoystickAvailable)
	{
		return;
	}

	DWORD	buttonstate, povstate;
	
	// loop through the joystick buttons
	// key a joystick event or auxillary event for higher number buttons for each state change
	buttonstate = 0;
	for ( i = 0; i < SDL_CONTROLLER_BUTTON_MAX; i++ )
	{
		if ( SDL_GameControllerGetButton( s_pJoystick, (SDL_GameControllerButton)i ) )
		{
			buttonstate |= 1<<i;
		}
	}
	
	for (i = 0; i < JOY_MAX_AXES; i++)
	{
		m_rgAxes[i].pRawValue = RawValuePointer(i);
	}

	for (i=0 ; i < m_nJoystickButtons; i++)
	{
		if ( (buttonstate & (1<<i)) && !(m_nJoystickOldButtons & (1<<i)) )
		{
			key_index = (i < 4) ? K_JOY1 : K_AUX1;
			engine->Key_Event (key_index + i, 1);
		}

		if ( !(buttonstate & (1<<i)) && (m_nJoystickOldButtons & (1<<i)) )
		{
			key_index = (i < 4) ? K_JOY1 : K_AUX1;
			engine->Key_Event (key_index + i, 0);
		}
	}
	m_nJoystickOldButtons = (unsigned int)buttonstate;

	if (m_fJoystickHasPOVControl)
	{
		// convert POV information into 4 bits of state information
		// this avoids any potential problems related to moving from one
		// direction to another without going through the center position
		povstate = 0;
		// determine which bits have changed and key an auxillary event for each change
		for (i=0 ; i < 4 ; i++)
		{
			if ( (povstate & (1<<i)) && !(m_nJoystickOldPOVState & (1<<i)) )
			{
				engine->Key_Event (K_AUX29 + i, 1);
			}

			if ( !(povstate & (1<<i)) && (m_nJoystickOldPOVState & (1<<i)) )
			{
				engine->Key_Event (K_AUX29 + i, 0);
			}
		}
		m_nJoystickOldPOVState = (unsigned int)povstate;
	}
}

/* 
=============== 
IN_ReadJoystick
=============== 
*/  
int CInput::ReadJoystick (void)
{
	SDL_JoystickUpdate();
	return 1;
}


/*
===========
IN_JoyMove
===========
*/
void CInput::JoyStickMove( float frametime, usercmd_t *cmd )
{
	float	speed, aspeed;
	float	fAxisValue, fTemp;
	int		i;
	Vector viewangles;

	engine->GetViewAngles( (float *)viewangles );

	// complete initialization if first time in
	// this is needed as cvars are not available at initialization time
	if( m_fJoystickAdvancedInit != 1 )
	{
		Joystick_Advanced();
		m_fJoystickAdvancedInit = 1;
	}

	// verify joystick is available and that the user wants to use it
	if (!m_fJoystickAvailable || !in_joystick->value)
	{
		return; 
	}
 
	// collect the joystick data, if possible
	if ( ReadJoystick () != 1)
	{
		return;
	}

	if (in_speed.state & 1)
		speed = cl_movespeedkey->value;
	else
		speed = 1;

	aspeed = speed * frametime;

	// loop through the axes
	for (i = 0; i < JOY_MAX_AXES; i++)
	{
		// get the floating point zero-centered, potentially-inverted data for the current axis
		fAxisValue = (float)m_rgAxes[i].pRawValue;

		if (joy_wwhack2->value != 0.0)
		{
			if (m_rgAxes[i].AxisMap == AxisTurn)
			{
				// this is a special formula for the Logitech WingMan Warrior
				// y=ax^b; where a = 300 and b = 1.3
				// also x values are in increments of 800 (so this is factored out)
				// then bounds check result to level out excessively high spin rates
				fTemp = 300.0 * pow( fabs(fAxisValue) / 800.0, 1.3);
				if (fTemp > 14000.0)
					fTemp = 14000.0;
				// restore direction information
				fAxisValue = (fAxisValue > 0.0) ? fTemp : -fTemp;
			}
		}

		// convert range from -32768..32767 to -1..1 
		fAxisValue /= 32768.0;

		switch (m_rgAxes[i].AxisMap)
		{
		case AxisForward:
			if ((joy_advanced->value == 0.0) && (in_jlook.state & 1))
			{
				// user wants forward control to become look control
				if (fabs(fAxisValue) > joy_pitchthreshold->value)
				{		
					// if mouse invert is on, invert the joystick pitch value
					// only absolute control support here (joy_advanced is 0)
					if (m_pitch->value < 0.0)
					{
						viewangles[PITCH] -= (fAxisValue * joy_pitchsensitivity->value) * aspeed * cl_pitchspeed->value;
					}
					else
					{
						viewangles[PITCH] += (fAxisValue * joy_pitchsensitivity->value) * aspeed * cl_pitchspeed->value;
					}
					V_StopPitchDrift();
				}
				else
				{
					// no pitch movement
					// disable pitch return-to-center unless requested by user
					// *** this code can be removed when the lookspring bug is fixed
					// *** the bug always has the lookspring feature on
					if(lookspring->value == 0.0)
					{
						V_StopPitchDrift();
					}
				}
			}
			else
			{
				// user wants forward control to be forward control
				if (fabs(fAxisValue) > joy_forwardthreshold->value)
				{
					cmd->forwardmove += (fAxisValue * joy_forwardsensitivity->value) * speed * cl_forwardspeed->value;
				}
			}
			break;

		case AxisSide:
			if (fabs(fAxisValue) > joy_sidethreshold->value)
			{
				cmd->sidemove += (fAxisValue * joy_sidesensitivity->value) * speed * cl_sidespeed->value;
			}
			break;

		case AxisTurn:
			if ((in_strafe.state & 1) || (lookstrafe->value && (in_jlook.state & 1)))
			{
				// user wants turn control to become side control
				if (fabs(fAxisValue) > joy_sidethreshold->value)
				{
					cmd->sidemove -= (fAxisValue * joy_sidesensitivity->value) * speed * cl_sidespeed->value;
				}
			}
			else
			{
				// user wants turn control to be turn control
				if (fabs(fAxisValue) > joy_yawthreshold->value)
				{
					if(m_rgAxes[i].ControlMap == JOY_ABSOLUTE_AXIS)
					{
						viewangles[YAW] += (fAxisValue * joy_yawsensitivity->value) * aspeed * cl_yawspeed->value;
					}
					else
					{
						viewangles[YAW] += (fAxisValue * joy_yawsensitivity->value) * speed * 180.0;
					}

				}
			}
			break;

		case AxisLook:
			if (in_jlook.state & 1)
			{
				if (fabs(fAxisValue) > joy_pitchthreshold->value)
				{
					// pitch movement detected and pitch movement desired by user
					if(m_rgAxes[i].ControlMap == JOY_ABSOLUTE_AXIS)
					{
						viewangles[PITCH] += (fAxisValue * joy_pitchsensitivity->value) * aspeed * cl_pitchspeed->value;
					}
					else
					{
						viewangles[PITCH] += (fAxisValue * joy_pitchsensitivity->value) * speed * 180.0;
					}
					V_StopPitchDrift();
				}
				else
				{
					// no pitch movement
					// disable pitch return-to-center unless requested by user
					// *** this code can be removed when the lookspring bug is fixed
					// *** the bug always has the lookspring feature on
					if( lookspring->value == 0.0 )
					{
						V_StopPitchDrift();
					}
				}
			}
			break;

		default:
			break;
		}
	}

	// bounds check pitch
	if (viewangles[PITCH] > cl_pitchdown->value)
		viewangles[PITCH] = cl_pitchdown->value;
	if (viewangles[PITCH] < -cl_pitchup->value)
		viewangles[PITCH] = -cl_pitchup->value;

	engine->SetViewAngles( (float *)viewangles );
}


/*
===========
Init_Joystick
===========
*/
void CInput::Init_Joystick (void)
{
	in_joystick				= engine->pfnRegisterVariable ( "joystick","0", FCVAR_ARCHIVE );
	joy_name				= engine->pfnRegisterVariable ( "joyname", "joystick", 0 );
	joy_advanced			= engine->pfnRegisterVariable ( "joyadvanced", "0", 0 );
	joy_advaxisx			= engine->pfnRegisterVariable ( "joyadvaxisx", "0", 0 );
	joy_advaxisy			= engine->pfnRegisterVariable ( "joyadvaxisy", "0", 0 );
	joy_advaxisz			= engine->pfnRegisterVariable ( "joyadvaxisz", "0", 0 );
	joy_advaxisr			= engine->pfnRegisterVariable ( "joyadvaxisr", "0", 0 );
	joy_advaxisu			= engine->pfnRegisterVariable ( "joyadvaxisu", "0", 0 );
	joy_advaxisv			= engine->pfnRegisterVariable ( "joyadvaxisv", "0", 0 );
	joy_forwardthreshold	= engine->pfnRegisterVariable ( "joyforwardthreshold", "0.15", 0 );
	joy_sidethreshold		= engine->pfnRegisterVariable ( "joysidethreshold", "0.15", 0 );
	joy_pitchthreshold		= engine->pfnRegisterVariable ( "joypitchthreshold", "0.15", 0 );
	joy_yawthreshold		= engine->pfnRegisterVariable ( "joyyawthreshold", "0.15", 0 );
	joy_forwardsensitivity	= engine->pfnRegisterVariable ( "joyforwardsensitivity", "-1.0", 0 );
	joy_sidesensitivity		= engine->pfnRegisterVariable ( "joysidesensitivity", "-1.0", 0 );
	joy_pitchsensitivity	= engine->pfnRegisterVariable ( "joypitchsensitivity", "1.0", 0 );
	joy_yawsensitivity		= engine->pfnRegisterVariable ( "joyyawsensitivity", "-1.0", 0 );
	joy_wwhack1				= engine->pfnRegisterVariable ( "joywwhack1", "0.0", 0 );
	joy_wwhack2				= engine->pfnRegisterVariable ( "joywwhack2", "0.0", 0 );

	StartupJoystick();
}