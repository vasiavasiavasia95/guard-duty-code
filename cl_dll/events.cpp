//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Client side events handling
//
// $NoKeywords: $
//=============================================================================//

#include "cl_dll.h"
#include "event_api.h"
#include "event_args.h"
#include "const.h"
#include "entity_state.h"
#include "cl_entity.h"
#include "entity_types.h"
#include "usercmd.h"
#include "pm_defs.h"

#include "eventscripts.h"

#include "r_efx.h"

#include <string.h>

#include "com_model.h"

#include "r_studioint.h"

extern cvar_t *cl_ejectbrass;

struct model_s	*m_pShells[2];

extern Vector v_sim_vel, v_angles;

extern "C"
{
	void EV_Sparks( struct event_args_s *args  );
	void EV_GlockSilencer1( struct event_args_s *args  );
	void EV_GlockSilencer2( struct event_args_s *args  );
}

/*
===================
EV_HookEvents

Associate script file name with callback functions.  Callback's must be extern "C" so
 the engine doesn't get confused about name mangling stuff.  Note that the format is
 always the same.  Of course, a clever mod team could actually embed parameters, behavior
 into the actual .sc files and create a .sc file parser and hook their functionality through
 that.. i.e., a scripting system.

That was what we were going to do, but we ran out of time...oh well.
===================
*/
void EV_HookEvents( void )
{
	engine->pfnHookEvent("events/sparks.sc", EV_Sparks);
	engine->pfnHookEvent("events/silencer1.sc", EV_GlockSilencer1);
	engine->pfnHookEvent("events/silencer2.sc", EV_GlockSilencer2);
}

//=======================
// SPECIAL EFFECTS START
//=======================

#define VectorNormalize2( v, dest ) {float ilength = (float)sqrt(DotProduct(v,v));if (ilength) ilength = 1.0f / ilength;dest[0] = v[0] * ilength;dest[1] = v[1] * ilength;dest[2] = v[2] * ilength; }

/*void EV_Sparks(event_args_t *args)
{
	TEMPENTITY *pTemp;
	int i;
	int m_iBalls;
	Vector vecDelta, vecDir;
	float flAmplitude = 50;
	float flSpeed = 90;

	m_iBalls  = engine->pEventAPI->EV_FindModelIndex("sprites/hotglow.spr");

	VectorSubtract( args->origin, args->origin, vecDelta );
	VectorNormalize2( vecDelta, vecDir );

	flAmplitude /= 256.0f;

	for ( i = 0; i < 5; i++) 
	{
		Vector	vecPos, vecVel;

		// Be careful of divide by 0 when using 'count' here...
		if (i == 0)
		{
			VectorCopy(args->origin, vecPos);
		}
		else
		{
			VectorMA(args->origin, (i / (5 - 1.0f)), vecDelta, vecPos);
		}

		float randomStrength = 56;

		float randx = engine->pfnRandomFloat(0, randomStrength);
		float randy = engine->pfnRandomFloat(0, randomStrength);
		float randz = engine->pfnRandomFloat(35, randomStrength + 80);

		if (engine->pfnRandomLong(0, 1) == 0) {
			randx *= -1;
		}
		if (engine->pfnRandomLong(0, 1) == 0) {
			randy *= -1;
		}

		if (engine->pfnRandomLong(0, 1) == 0) {
			randz *= -1;
		}

		Vector rot = Vector(randx, randy, randz);

		pTemp = engine->pEfxAPI->R_TempSprite( args->origin, rot, 0.12, m_iBalls, kRenderGlow, kRenderFxNoDissipation, /*250.0 / 255.0*/ //0.98, 0.22, FTENT_GRAVITY | FTENT_COLLIDEWORLD | FTENT_FADEOUT);

	/*	if (pTemp != NULL)
		{
			pTemp->fadeSpeed = 2;
		}

	}

}*/

/*void R_AlphaSparks(int *a1)
{
  int v1; // esi
  GLfloat *v2; // edx
  int v3; // ebx
  int *v4; // edi
  int v5; // eax
  int v6; // eax
  char v7; // al
  bool v8; // zf
  double v9; // st7
  int v10; // [esp+Ch] [ebp-Ch]
  float v11; // [esp+10h] [ebp-8h]
  int v12; // [esp+14h] [ebp-4h]
  float v13; // [esp+14h] [ebp-4h]
  float v14; // [esp+14h] [ebp-4h]

  v10 = 0;
  v1 = dword_D38734;
  do
  {
    if ( !v1 )
      break;
    v2 = v;
    v3 = v1;
    v4 = a1;
    v5 = *(_DWORD *)(v1 + 16);
    v = (GLfloat *)v1;
    dword_D38734 = v5;
    *(_DWORD *)(v1 + 16) = v2;
    v12 = 3;
    do
    {
      v6 = *v4++;
      *(_DWORD *)v3 = v6;
      v7 = rand();
      v3 += 4;
      v8 = v12-- == 1;
      v11 = (float)((v7 & 0x1F) - 16);
      *(float *)(v3 + 16) = v11;
    }
    while ( !v8 );
    v13 = (float)(rand() & 0x3F);
    *(float *)(v1 + 28) = v13;
    v9 = dbl_849460 + 3.0;
    *(_DWORD *)(v1 + 32) = 0;
    *(_DWORD *)(v1 + 12) = 1132331008;
    *(_DWORD *)(v1 + 40) = 7;
    v14 = v9;
    ++v10;
    *(float *)(v1 + 36) = v14;
    v1 = dword_D38734;
  }
  while ( v10 < 15 );
}*/

static int gSparkRamp[9] = { 0xfe, 0xfd, 0xfc, 0x6f, 0x6e, 0x6d, 0x6c, 0x67, 0x60 };

/*
===============
R_Blob2ParticlesCallback

A callback that imitates Alpha's engine pt_blob2 behavior
===============
*/
void R_Blob2ParticlesCallback(struct particle_s *particle, float frametime)
{
	qboolean finishedspark = false;

	float flGravity = frametime * engine->hudGetServerGravityValue() * 0.05;
	float time2 = frametime * 10.0;
	float dvel = frametime * 4.0;

	// update position.
	VectorMA( particle->org, frametime, particle->vel, particle->org);

	particle->ramp += time2;

	if( particle->ramp < 4.0 ) 
		particle->color = gSparkRamp[(int)particle->ramp];
		
	engine->pEfxAPI->R_GetPackedColor( &particle->packedColor, particle->color );

	// Move particles down
	particle->vel[2] -= flGravity * 4.0;
}

/*
===============
R_ParticleSparks

A spark effect using qparticles as seen in the Alpha's engine

NOTE: Original effect had a fadeout but It's not possible on steam
and I couldn't even find the code for it in alpha
===============
*/
void R_ParticleSparks( Vector pos )
{
	int			i, j;
	particle_t	*p;

	int test = -1431655765;

	float f = *(float*)&test;

	// Test to fix decompiled floats
	engine->Con_Printf("test hack is %f", f);

	for ( i = 0 ; i < 15; i++)
	{
		p = engine->pEfxAPI->R_AllocParticle( R_Blob2ParticlesCallback );
		if ( !p ) return;

		for( j = 0; j < 3; j++ )
		{
			p->org[j] = pos[j];
			//p->vel[j] = (rand() & 0x1F) - 16;
			p->vel[j] = engine->pfnRandomFloat(-16.0, 15.0);
		}

		p->vel[2] = engine->pfnRandomFloat(0.0, 63.0);

		p->color = 254;

		// This is for software to setup color correctly
		engine->pEfxAPI->R_GetPackedColor( &p->packedColor, p->color );

		p->ramp = 0.0;
		p->type = pt_clientcustom;
		p->die = engine->GetClientTime() + 3.0; // die in 3 seconds
	}
}

void EV_Sparks( event_args_t *args )
{
	TEMPENTITY	*pTemp;
	Vector		org, vecVel;
	int		i;
	int modelIndex;
	int nCount = 5;
	float flSize = 0.1;
	float flAmplitude = 75;
	float flSpeed = 150;

	VectorCopy( args->origin, org );

	// Vasia: HACK??? Don't want to deal with all this ricochet sprite alloc bullshit
	//so lets just use spark effect with 0 sparks
	engine->pEfxAPI->R_SparkEffect( org, 0, 0, 0 );

	//R_ParticleSparks( org );

	modelIndex = engine->pEventAPI->EV_FindModelIndex( "sprites/hotglow.spr" );
	
	for( i = 0; i < nCount; i++ )
	{
		vecVel[0] = engine->pfnRandomFloat( -50.0, 50.0 );
		vecVel[1] = engine->pfnRandomFloat( -50.0, 50.0 );
		vecVel[2] = engine->pfnRandomFloat( 0.0, 63.0 );

		pTemp = engine->pEfxAPI->R_TempSprite( org, vecVel, 0.08, modelIndex, kRenderGlow, kRenderFxNoDissipation, 1.0, 0.35, FTENT_COLLIDEWORLD | FTENT_SLOWGRAVITY | FTENT_FADEOUT );
		if( !pTemp ) return;

		pTemp->fadeSpeed = 2.0;
	}
}

void EV_GlockSilencer1( event_args_t *args )
{
	int	iBody, bInvert;

	iBody = args->iparam1;

	bInvert = args->bparam1;

	cl_entity_t	*view = engine->GetViewModel();

	if ( !view )
		return;

	if ( bInvert )
	{
		view->curstate.body = (iBody == 0) ? 1 : 0;
	}
	else
	{
		view->curstate.body = iBody;
	}
}

void EV_GlockSilencer2( event_args_t *args )
{
	int	bFramerateInvert;

	bFramerateInvert = args->bparam2;

	cl_entity_t	*view = engine->GetViewModel();

	if ( !view )
		return;

	// change framerate
	view->curstate.iuser1 = bFramerateInvert ? 1 : 0;
}

/*
=================
EV_GetDefaultShellInfo

Determine where to eject shells from
=================
*/
void EV_GetDefaultShellInfo( float *Angles, int IsLocal, float *ShellVelocity, float *ShellOrientation )
{
	int i;
	float fR, fU;
	Vector vecViewAngles, vecVel, vecForward, vecRight, vecUp;

	// HACK:: Only add velocity for the local player
	vecVel = IsLocal ? v_sim_vel : vec3_origin;

	vecViewAngles = IsLocal ? v_angles : Angles;

	AngleVectors( vecViewAngles, vecForward, vecRight, vecUp );

	fR = engine->pfnRandomFloat( 50, 70 );
	fU = engine->pfnRandomFloat( 100, 150 );

	for ( i = 0; i < 3; i++ )
	{
		ShellVelocity[i] = vecVel[i] + vecRight[i] * fR + vecUp[i] * fU + vecForward[i] * 25;
	}
}

//
// EjectBrass - tosses a brass shell from passed origin at passed velocity
//
void EjectBrass( Vector vecOrigin, const Vector &angles, const Vector &gunAngles, int type, int IsLocal )
{
	if ( !cl_ejectbrass->value )
		return;

	if ( !m_pShells[0] )
	{
		m_pShells[0] = IEngineStudio->Mod_ForName( "models/shell.mdl", true );

		m_pShells[0]->needload = 3;

		m_pShells[1] = IEngineStudio->Mod_ForName( "models/shotgunshell.mdl", true );

		m_pShells[1]->needload = 3;
	}

	const model_t *pModel = m_pShells[type];
	
	if ( pModel == NULL )
		return;

	TEMPENTITY	*pTemp = engine->pEfxAPI->CL_TempEntAlloc( vecOrigin, ( model_s * ) pModel );

	if ( pTemp == NULL )
		return;

	//Keep track of shell type
	if ( type == 1 )
	{
		pTemp->hitSound = BOUNCE_SHOTSHELL;
	}
	else
	{
		pTemp->hitSound = BOUNCE_SHELL;
	}

	pTemp->entity.curstate.body	= 0;

	pTemp->flags |= ( FTENT_COLLIDEWORLD | FTENT_FADEOUT | FTENT_GRAVITY | FTENT_ROTATE );

	pTemp->entity.baseline.angles[0] = engine->pfnRandomFloat(-256,255);
	pTemp->entity.baseline.angles[1] = engine->pfnRandomFloat(-256,255);
	pTemp->entity.baseline.angles[2] = engine->pfnRandomFloat(-256,255);

	pTemp->entity.curstate.rendermode = kRenderNormal;
	pTemp->entity.baseline.renderamt = 255;		// Set this for fadeout

	Vector	vecShellVelocity, vecShellOrientation;

	EV_GetDefaultShellInfo( Vector( angles.x, angles.y, angles.z ), IsLocal, vecShellVelocity, vecShellOrientation );

	// Face forward
	pTemp->entity.angles = gunAngles;

	pTemp->entity.baseline.origin = vecShellVelocity;

	pTemp->die = engine->GetClientTime() + 2.5;
}

//=======================
// SPECIAL EFFECTS END
//=======================
