/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
//  cl_dll.h
//

// 4-23-98  JOHN

//
//  This DLL is linked by the client when they first initialize.
// This DLL is responsible for the following tasks:
//		- Loading the HUD graphics upon initialization
//		- Drawing the HUD graphics every frame
//		- Handling the custum HUD-update packets
//

#ifndef CBASE_H
#define CBASE_H
#ifdef _WIN32
#pragma once
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "Platform.h"

typedef unsigned char byte;
typedef unsigned short word;

#include "mathlib.h"

#include "const.h"

#include "wrect.h"
#include "../engine/cdll_int.h"
#include "cl_util.h"

typedef struct engine_studio_api_s engine_studio_api_t;

extern cl_enginefunc_t *engine;
extern engine_studio_api_t *IEngineStudio;

#endif // CBASE_H
