/***
*
*	Copyright (c) 1999, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
//  cdll_int.c
//
// this implementation handles the linking of the engine to the DLL
//

#include "cl_dll.h"
#include "hud.h"
#include "input.h"
#include "tri.h"
#include "pm_shared.h"
#include "vgui2/IViewport.h"
#include "interface.h"
#include "Exports.h"
#include "GameUI/IGameUI.h"
#include "GameUI/IGameConsole.h"

// IF YOU ADD AN INTERFACE, EXTERN IT IN THE HEADER FILE.
cl_enginefunc_t *engine = NULL;
IGameUI *g_pGameUI = NULL;
IGameConsole *g_pGameConsole = NULL;

// Acquire pointers to the GameUI interfaces
CSysModule *g_hGameUIModule = NULL;
void CL_LoadGameUI( void );
void CL_UnloadGameUI( void );

extern CInput g_Input;

CHud gHUD;

double g_HostFrameTime;

void EV_HookEvents( void );

/*
========================== 
    Initialize

Called when the DLL is first loaded.
==========================
*/
int DLLEXPORT Initialize( cl_enginefunc_t *pEnginefuncs, int iVersion )
{
	// copy the engine interface
	engine = pEnginefuncs;

	//!!! mwh UNDONE We need to think about our versioning strategy. Do we want to try to be compatible
	// with previous versions, especially when we're only 'bonus' functionality? Should it be the engine
	// that decides if the DLL is compliant?

	if (iVersion != CLDLL_INTERFACE_VERSION)
		return false;

	EV_HookEvents();

	// Load the GameUI module
	// It doesn't contain any important functiononality ( at least as of now ) so don't quit if couldn't import
	CL_LoadGameUI();

	return true;
}

/*
==========================
	HUD_Shutdown

Called when the client .dll is being dismissed
==========================
*/
void DLLEXPORT HUD_Shutdown( void )
{
//	RecClShutdown();

	g_Input.Shutdown_All();

	CL_UnloadGameUI();
}

/*
==========================
	HUD_VidInit

Called when the game initializes
and whenever the vid_mode is changed
so the HUD can reinitialize itself.
==========================
*/

int DLLEXPORT HUD_VidInit( void )
{
//	RecClHudVidInit();
	gHUD.VidInit();

	return 1;
}

/*
==========================
	HUD_Init

Called whenever the client connects
to a server.  Reinitializes all 
the hud variables.
==========================
*/

void DLLEXPORT HUD_Init( void )
{
//	RecClHudInit();
	g_Input.Init_All();
	gHUD.Init();
}

/*
================================
HUD_GetHullBounds

  Engine calls this to enumerate player collision hulls, for prediction.  Return 0 if the hullnumber doesn't exist.
================================
*/
int DLLEXPORT HUD_GetHullBounds( int hullnumber, float *mins, float *maxs )
{
//	RecClGetHullBounds(hullnumber, mins, maxs);

	return PM_GetHullBounds( hullnumber, mins, maxs );
}

/*
=====================
HUD_PostRunCmd

Vasia: Since we don't use client weapons anymore
All this function does is set client's fov to the previous stored value ( g_lastFOV )
=====================
*/
void DLLEXPORT HUD_PostRunCmd(struct local_state_s *from, struct local_state_s *to, struct usercmd_s *cmd, int runfuncs, double time, unsigned int random_seed)
{
	//	RecClPostRunCmd(from, to, cmd, runfuncs, time, random_seed);

	to->client.fov = g_lastFOV;

	// All games can use FOV state
	g_lastFOV = to->client.fov;
}


void DLLEXPORT HUD_PlayerMoveInit( struct playermove_s *ppmove )
{
//	RecClClientMoveInit(ppmove);

	PM_Init( ppmove );
}

char DLLEXPORT HUD_PlayerMoveTexture( char *name )
{
//	RecClClientTextureType(name);

	return PM_FindTextureType( name );
}

void DLLEXPORT HUD_PlayerMove( struct playermove_s *ppmove, int server )
{
//	RecClClientMove(ppmove, server);

	PM_Move( ppmove, server );
}

/*
==========================
	HUD_Redraw

called every screen frame to
redraw the HUD.
===========================
*/

int DLLEXPORT HUD_Redraw( float time, int intermission )
{
//	RecClHudRedraw(time, intermission);

	gHUD.Redraw( time, intermission );

	return 1;
}


/*
==========================
	HUD_UpdateClientData

called every time shared client
dll/engine data gets changed,
and gives the cdll a chance
to modify the data.

returns 1 if anything has been changed, 0 otherwise.
==========================
*/

int DLLEXPORT HUD_UpdateClientData(client_data_t *pcldata, float flTime )
{
//	RecClHudUpdateClientData(pcldata, flTime);

	g_Input.ControllerCommands();

	return gHUD.UpdateClientData(pcldata, flTime );
}

/*
==========================
	HUD_Reset

Called at start and end of demos to restore to "non"HUD state.
==========================
*/

void DLLEXPORT HUD_Reset( void )
{
//	RecClHudReset();

	gHUD.VidInit();
}

/*
================================
HUD_ConnectionlessPacket

 Return 1 if the packet is valid.  Set response_buffer_size if you want to send a response packet.  Incoming, it holds the max
  size of the response_buffer, so you must zero it out if you choose not to respond.
================================
*/
int	DLLEXPORT HUD_ConnectionlessPacket( const struct netadr_s *net_from, const char *args, char *response_buffer, int *response_buffer_size )
{
//	RecClConnectionlessPacket(net_from, args, response_buffer, response_buffer_size);

	// Parse stuff from args
	int max_buffer_size = *response_buffer_size;

	// Zero it out since we aren't going to respond.
	// If we wanted to response, we'd write data into response_buffer
	*response_buffer_size = 0;

	// Since we don't listen for anything here, just respond that it's a bogus message
	// If we didn't reject the message, we'd return 1 for success instead.
	return 0;
}

/*
==========================
	IN_ActivateMouse

Called when mouse input is activated.
==========================
*/
void DLLEXPORT IN_ActivateMouse (void)
{
	g_Input.ActivateMouse();
}


/*
==========================
	IN_DeactivateMouse

Called when mouse input is deactivated.
==========================
*/
void DLLEXPORT IN_DeactivateMouse (void)
{
	g_Input.DeactivateMouse();
}

/*
==========================
	IN_MouseEvent

Called when a mouse event has occurred.
==========================
*/
void DLLEXPORT IN_MouseEvent (int mstate)
{
	g_Input.MouseEvent( mstate );
}

/*
=====================
	IN_Accumulate

This is only called during extra sound updates and just accumulates mouse x, y offets and recenters the mouse.
This call is used to try to prevent the mouse from appearing out of the side of a windowed version of the engine if
rendering or other processing is taking too long
=====================
*/
void DLLEXPORT IN_Accumulate (void)
{
	g_Input.AccumulateMouse();
}

/*
===================
	IN_ClearStates

Clears all mouse button states.
===================
*/
void DLLEXPORT IN_ClearStates (void)
{
	g_Input.ClearStates();
}

/*
============
KB_Find

Allows the engine to get a kbutton_t directly ( so it can check +mlook state, etc ) for saving out to .cfg files
============
*/
struct kbutton_s DLLEXPORT *KB_Find( const char *name )
{
	return g_Input.FindKey( name );
}

/*
============
HUD_Key_Event

Return 1 to allow engine to process the key, otherwise, act on it as needed
============
*/
int DLLEXPORT HUD_Key_Event( int down, int keynum, const char *pszCurrentBinding )
{
	return g_Input.KeyEvent( down, keynum, pszCurrentBinding );
}

/*
================
CL_CreateMove

Fills in usercmd_s structure based on current view angles and key/controller inputs
	frametime - timestamp for last frame
	*cmd - the command to fill in
	active - whether the user is fully connected to a server
================
*/
void DLLEXPORT CL_CreateMove ( float frametime, struct usercmd_s *cmd, int active )
{	
	g_Input.CreateMove( frametime, cmd, active );
}

void DLLEXPORT CAM_Think( void )
{
	g_Input.CAM_Think();
}

int DLLEXPORT CL_IsThirdPerson( void )
{
	return g_Input.CAM_IsThirdPerson();
}

void DLLEXPORT CL_CameraOffset( float *ofs )
{
	g_Input.CAM_GetCameraOffset( ofs );
}

/*
==========================
HUD_Frame

Called by engine every frame that client .dll is loaded
==========================
*/

void DLLEXPORT HUD_Frame( double time )
{
//	RecClHudFrame(time);

	g_HostFrameTime = time;

	if ( gViewPortInterface )
		gViewPortInterface->RunFrame();
}


/*
==========================
HUD_VoiceStatus

Called when a player starts or stops talking.
==========================
*/

void DLLEXPORT HUD_VoiceStatus(int entindex, qboolean bTalking)
{
////	RecClVoiceStatus(entindex, bTalking);
}

/*
==========================
HUD_DirectorMessage

Called when a director event message was received
==========================
*/

void DLLEXPORT HUD_DirectorMessage( int iSize, void *pbuf )
{
}

/*
==========================
HUD_ChatInputPosition

Sets the location of the input for chat text
==========================
*/

void DLLEXPORT HUD_ChatInputPosition( int *x, int *y )
{
//	RecClChatInputPosition( x, y );
}

/*
==========================
HUD_GetPlayerTeam

This function is never called and does nothing
==========================
*/

int DLLEXPORT HUD_GetPlayerTeam( int iplayer )
{
	int result = 0;

  // Vasia: CS code
  //if ( (unsigned int)iplayer <= 0x40 )
   // return g_PlayerExtraInfo[iplayer].team_id;
 
	return result;
}

/*
==========================
ClientFactory

This function is never called, but it has to exist in order for the engine to load stuff from the client. - Solokiller
Vasia: CS seems to return Sys_GetFactoryThis pointer so we'll do the same to look cool
==========================
*/
extern "C" DLLEXPORT void *ClientFactory()
{
	return ( void *)Sys_GetFactoryThis();
}

extern "C" void DLLEXPORT F(void *pv)
{
	cldll_func_t *pcldll_func = (cldll_func_t *)pv;

	cldll_func_t cldll_func = 
	{
	Initialize,
	HUD_Init,
	HUD_VidInit,
	HUD_Redraw,
	HUD_UpdateClientData,
	HUD_Reset,
	HUD_PlayerMove,
	HUD_PlayerMoveInit,
	HUD_PlayerMoveTexture,
	IN_ActivateMouse,
	IN_DeactivateMouse,
	IN_MouseEvent,
	IN_ClearStates,
	IN_Accumulate,
	CL_CreateMove,
	CL_IsThirdPerson,
	CL_CameraOffset,
	KB_Find,
	CAM_Think,
	V_CalcRefdef,
	HUD_AddEntity,
	HUD_CreateEntities,
	HUD_DrawNormalTriangles,
	HUD_DrawTransparentTriangles,
	HUD_StudioEvent,
	HUD_PostRunCmd,
	HUD_Shutdown,
	HUD_TxferLocalOverrides,
	HUD_ProcessPlayerState,
	HUD_TxferPredictionData,
	Demo_ReadBuffer,
	HUD_ConnectionlessPacket,
	HUD_GetHullBounds,
	HUD_Frame,
	HUD_Key_Event,
	HUD_TempEntUpdate,
	HUD_GetUserEntity,
	HUD_VoiceStatus,
	HUD_DirectorMessage,
	HUD_GetStudioModelInterface,
	HUD_ChatInputPosition,
	HUD_GetPlayerTeam,
	ClientFactory,	//NOTE: Needed for the engine to query CreateInterface for interface instantiation. - Solokiller
	};

	*pcldll_func = cldll_func;
}

void CL_UnloadGameUI( void )
{
	Sys_UnloadModule( g_hGameUIModule );

	g_pGameUI = NULL;
	g_pGameConsole = NULL;
	g_hGameUIModule = NULL;
}

void CL_LoadGameUI( void )
{

#if defined ( _WIN32 )
	char *szFsModule = "cl_dlls/gameui.dll";
#elif defined(OSX)
	char *szFsModule = "cl_dlls/gameui.dylib";	
#elif defined(LINUX)
	char *szFsModule = "cl_dlls/gameui.so";
#else
#error
#endif

	char szPDir[512];

	if ( engine->COM_ExpandFilename( szFsModule, szPDir, sizeof( szPDir ) ) == false )
	{
		g_pGameUI = NULL;
		g_pGameConsole = NULL;
		g_hGameUIModule = NULL;
		return;
	}

	g_hGameUIModule = Sys_LoadModule( szPDir );
	CreateInterfaceFn GameUIFactory = Sys_GetFactory( g_hGameUIModule );

	if ( GameUIFactory == NULL )
	{
		g_pGameUI = NULL;
		g_pGameConsole = NULL;
		g_hGameUIModule = NULL;
		return;
	}

	g_pGameUI = (IGameUI *)GameUIFactory( GAMEUI_INTERFACE_VERSION, NULL);
	g_pGameConsole = (IGameConsole *)GameUIFactory( GAMECONSOLE_INTERFACE_VERSION, NULL);
}
