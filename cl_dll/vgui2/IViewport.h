//======== (C) Copyright 1999, 2000 Valve, L.L.C. All rights reserved. ========
//
// The copyright to the contents herein is the property of Valve, L.L.C.
// The contents may be used and/or copied only with the written permission of
// Valve, L.L.C., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
// Purpose: 
//
// $Workfile:     $
// $Date:         $
//
//-----------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//=============================================================================
#if !defined( IVIEWPORT_H )
#define IVIEWPORT_H
#ifdef _WIN32
#pragma once
#endif

//-----------------------------------------------------------------------------
// Purpose: viewport interface for the rest of the dll
//	This header must never depend on VGUI, so don't even try doing it!!!
//-----------------------------------------------------------------------------
class IViewPort
{
public:

	// A workaround for the lack of RunFrame function in IClientVGUI
	virtual void RunFrame() = 0;
	
	virtual int KeyInput( int down, int keynum, const char *pszCurrentBinding ) = 0;

	virtual bool IsInLevel() = 0;

};

extern IViewPort *gViewPortInterface;

#endif // IVIEWPORT_H
