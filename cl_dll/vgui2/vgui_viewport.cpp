//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Client DLL VGUI2 Viewport
//
// $NoKeywords: $
//=============================================================================//

// Vasia: This file originally was still called "vgui_TeamFortressViewport" and viewport class was called "TeamFortressViewport"
// But I'm not restoring that because I want it to be generic and clean
// NEW: Actually VGUI2 in leak and current CS use TF viewport as a wrapper to the IClientVGUI interface from which
// CS then derrives to it's CounterStrikeViewport.
// CS has a few cool features that I want to port over ( mostly graphics related )

// client dll/engine defines
#include "cl_dll.h"

// VGUI panel includes
#include <vgui_controls/Panel.h>
#include <vgui/ISurface.h>
#include <KeyValues.h>
#include <vgui/IScheme.h>
#include <vgui/IVGui.h>
#include <vgui/ILocalize.h>
#include <vgui/IInputInternal.h>

// Engine API
#include "IEngineVGUI.h"
#include "BaseUI/IBaseUI.h"
#include "IGameUIFuncs.h"

// sub dialogs
//#include "NewGameDialog.h"

// our definition
#include "KeyValuesCompat.h"
#include "vgui_viewport.h"

IViewPort *gViewPortInterface = NULL;

cvar_t *sv_unlockedchapters;

IGameUIFuncs *g_pGameUIFuncs = NULL;
IBaseUI *g_pBaseUI = NULL;
IEngineVGui *g_pEngineVGUI = NULL;

static CBaseViewport g_ClientViewport;

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: singleton accessor
//-----------------------------------------------------------------------------
CBaseViewport &ClientUI()
{
	return g_ClientViewport;
}

EXPOSE_SINGLE_INTERFACE_GLOBALVAR( CBaseViewport, IClientVGUI, VCLIENTVGUI_VERSION, g_ClientViewport );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CBaseViewport::CBaseViewport()
{
	gViewPortInterface = this;
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CBaseViewport::~CBaseViewport()
{
	m_bInitialized = false;
}

void CBaseViewport::Initialize( CreateInterfaceFn *factories, int count )
{
	/*
	*	Factories in the given array:
	*	engine
	*	vgui2
	*	filesystem
	*	chrome HTML
	*	GameUI
	*	client (this library)
	*/

	CreateInterfaceFn fileSystemFactory = factories[ 2 ];
	CreateInterfaceFn vguiFactory = factories[ 1 ];
	CreateInterfaceFn engineFactory = factories[ 0 ];
	CreateInterfaceFn clientFactory = factories[ 3 ];

	m_FactoryList[ 0 ] = Sys_GetFactoryThis();
	m_FactoryList[ 1 ] = engineFactory;
	m_FactoryList[ 2 ] = vguiFactory;
	m_FactoryList[ 3 ] = fileSystemFactory;
	m_FactoryList[ 4 ] = clientFactory;

	if( !VGuiControls_Init( "ClientUI", m_FactoryList, MAX_NUM_FACTORIES ) )
	{
		Msg( "Failed to initialize VGUI2\n" );
		return;
	}

	if( !KV_InitKeyValuesSystem( m_FactoryList, MAX_NUM_FACTORIES ) )
	{
		Msg( "Failed to initialize IKeyValues\n" );
		return;
	}

	g_pGameUIFuncs = ( IGameUIFuncs *)engineFactory( VENGINE_GAMEUIFUNCS_VERSION, NULL );
	g_pBaseUI = ( IBaseUI *)engineFactory( BASEUI_INTERFACE_VERSION, NULL );
	g_pEngineVGUI = (IEngineVGui *)engineFactory(VENGINE_VGUI_VERSION, NULL);

	// Set up some local variables
}

//-----------------------------------------------------------------------------
// Purpose: called when the VGUI subsystem starts up
//			Creates the sub panels and initialises them
//-----------------------------------------------------------------------------
void CBaseViewport::Start()
{
	//m_hNewGameDialog = new CCustomNewGameDialog();
	//m_hNewGameDialog->Activate(); // TEST

	// Vasia: This is a nice way to debug what panel we got pointer to.
	// NOTE: The command won't display if called here yet
	//engine->Con_Printf("ROOT parent panel name is : %s\n", vgui2::ipanel()->GetName(GameUiDll));

	// Vasia: Get the pointer to the root GameUI panel
	//vgui::VPANEL pGameUIPanel = g_pEngineVGUI->GetPanel(PANEL_GAMEUIDLL);

	// Get the pointer to the TaskBar GameUI panel
	//m_pTaskBar = FindPanelChild( pGameUIPanel, "TaskBar", true);

	// Vasia: CS does this here to
	m_bInitialized = true;
}

//-----------------------------------------------------------------------------
// Purpose: Sets the parent for each panel to use
//-----------------------------------------------------------------------------
void CBaseViewport::SetParent( vgui::VPANEL parent )
{
}

void CBaseViewport::HideScoreBoard()
{
}

//-----------------------------------------------------------------------------
// Purpose: Hides all the VGUI menus at once (except the scoreboard, as it is used in intermissions)
//-----------------------------------------------------------------------------
void CBaseViewport::HideAllVGUIMenu()
{
}

//-----------------------------------------------------------------------------
// Purpose: called when the engine shows the base client VGUI panel (i.e when entering a new level or exiting GameUI )
//-----------------------------------------------------------------------------
void CBaseViewport::ActivateClientUI()
{
}

//-----------------------------------------------------------------------------
// Purpose: called when the engine hides the base client VGUI panel (i.e when the GameUI is comming up ) 
//-----------------------------------------------------------------------------
void CBaseViewport::HideClientUI()
{
	// Vasia: This is not needed anymore because ROOT will disable it's children itself
	// UPDATE: Actually it seems we still need this because game ui panel doesn't make our panel invis
	// so we have make it not visible ourselves in order to make sure our mouse won't get stuck
	// UPDATE 2: This was caused by me stupidly replacing everything with hl2's leak controls code which unlike
	// Goldsrc and Source can't handle connections between panels linked from different modules
	//if ( m_hNewGameDialog.Get() && !m_hNewGameDialog->IsVisible() && IsInLevel() )
		//m_hNewGameDialog->SetVisible( true );
}

//-----------------------------------------------------------------------------
// Purpose: Direct Key Input
//-----------------------------------------------------------------------------
int	CBaseViewport::KeyInput( int down, int keynum, const char *pszCurrentBinding )
{
	return 1;
}

void CBaseViewport::Shutdown()
{
}

void CBaseViewport::RunFrame()
{
}

//-----------------------------------------------------------------------------
// Purpose: moves the game menu button to the right place on the taskbar
//-----------------------------------------------------------------------------
void CBaseViewport::PositionDialog( vgui::PHandle dlg )
{
	if (!dlg.Get())
		return;

	int x, y, ww, wt, wide, tall;
	vgui::surface()->GetWorkspaceBounds( x, y, ww, wt );
	dlg->GetSize(wide, tall);

	// Center it, keeping requested size
	dlg->SetPos(x + ((ww - wide) / 2), y + ((wt - tall) / 2));
}


//-----------------------------------------------------------------------------
// Purpose: returns true if we're currently playing the game
//-----------------------------------------------------------------------------
bool CBaseViewport::IsInLevel()
{
	const char *levelName = engine->pfnGetLevelName();
	if ( levelName && levelName[0] )
	{
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: returns the pointer to the GameUI TaskBar panel 
//-----------------------------------------------------------------------------
vgui::VPANEL CBaseViewport::GetTaskBarPanel( void )
{
	return m_pTaskBar;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
vgui::VPANEL CBaseViewport::FindPanelChild( vgui::VPANEL pParent, const char *childName, bool recurseDown )
{
	for (int i = 0; i < vgui::ipanel()->GetChildCount( pParent ); i++)
	{
		vgui::VPANEL pChild = vgui::ipanel()->GetChild( pParent, i);
		if (!pChild)
			continue;

		if (!stricmp(vgui::ipanel()->GetName( pChild ), childName))
		{
			return pChild;
		}

		if (recurseDown)
		{
			vgui::VPANEL panel = FindPanelChild( pChild, childName, recurseDown);
			if ( panel )
			{
				return panel;
			}
		}
	}

	return NULL;
}
