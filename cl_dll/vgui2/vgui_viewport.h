//========= Copyright (c) 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef CBASEVIEWPORT_H
#define CBASEVIEWPORT_H

// viewport interface for the rest of the dll
#include "IViewport.h"

// Vasia: Interesting. 
// This interface is one of a few things that were completely removed from leak sourcecode
// Thanks Solokiller!!!
#include <IClientVGUI.h>

#include "UtlQueue.h" // a vector based queue template to manage our VGUI menu queue

#include <vgui_controls/Frame.h>
#include "vgui_controls/EditablePanel.h"

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Base class for VGUI2 viewports.
//-----------------------------------------------------------------------------
class CBaseViewport : public IViewPort, public IClientVGUI
{
public:
	CBaseViewport();
	~CBaseViewport();

	// IClientVGUI interface
	virtual void Initialize( CreateInterfaceFn *factories, int count );
	virtual void Start( void );
	virtual void SetParent( vgui::VPANEL parent );
	virtual bool UseVGUI1( void ) { return false; }
	virtual void HideScoreBoard( void );
	virtual void HideAllVGUIMenu( void );   // these two are also in IViewPort, as IViewPort has no VGUI2 dependencies in it.
	virtual void ActivateClientUI( void );
	virtual void HideClientUI( void );
	virtual void Shutdown( void );

	// IViewPort interface
	virtual void RunFrame( void );
	virtual int	 KeyInput( int down, int keynum, const char *pszCurrentBinding );
	virtual bool IsInLevel( void );

	vgui::VPANEL FindPanelChild( vgui::VPANEL pParent, const char *childName, bool recurseDown);

	// custom GameUI related stuff 
	vgui::VPANEL GetTaskBarPanel( void );

	// position the dialog on screen
	virtual void PositionDialog( vgui::PHandle dlg );

private:

	bool	m_bInitialized;

	enum { MAX_NUM_FACTORIES = 5 };
	CreateInterfaceFn m_FactoryList[MAX_NUM_FACTORIES];

	// Panels
	vgui::VPANEL m_pTaskBar;
};

// Purpose: singleton accessor
extern CBaseViewport &ClientUI();


#endif //CBASEVIEWPORT_H
