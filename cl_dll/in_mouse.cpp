//========= Copyright (c) 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: Mouse input routines
//
// $NoKeywords: $
//=============================================================================

// in_win.c -- windows 95 mouse and joystick code
// 02/21/97 JCB Added extended DirectInput code to support external controllers.

#include "port.h"

#include "cl_dll.h"
#include "hud.h"
#include "kbutton.h"
#include "cvardef.h"
#include "usercmd.h"
#include "in_defs.h"
#include "../public/keydefs.h"
#include "view.h"
#include "Exports.h"
#include "input.h"

#include <SDL2/SDL_mouse.h>

#include "vgui/ISurface.h"
#include "vgui_controls/Controls.h"
#include "vgui/Cursor.h"
#include "vgui/IInputInternal.h"

// Vasia: VGUI2 SUPPORT 
// Added checks for active vgui2 cursor ( vgui2::surface()->IsCursorVisible() ) referenced from CS and HL2 code 

// Set this to 1 to show mouse cursor.  Experimental
int	g_iVisibleMouse = 0;

extern kbutton_t	in_strafe;
extern kbutton_t	in_mlook;
extern kbutton_t	in_speed;
extern kbutton_t	in_jlook;

extern cvar_t	*m_pitch;
extern cvar_t	*m_yaw;
extern cvar_t	*m_forward;
extern cvar_t	*m_side;

extern cvar_t *lookstrafe;
extern cvar_t *lookspring;
extern cvar_t *cl_pitchdown;
extern cvar_t *cl_pitchup;
extern cvar_t *cl_yawspeed;
extern cvar_t *cl_sidespeed;
extern cvar_t *cl_forwardspeed;
extern cvar_t *cl_pitchspeed;
extern cvar_t *cl_movespeedkey;

static double s_flRawInputUpdateTime = 0.0f;

// mouse variables
cvar_t		*m_filter;
cvar_t		*sensitivity;

static int	mouseshowtoggle = 1;

/*
===========
IN_ActivateMouse
===========
*/
void CInput::ActivateMouse (void)
{
	if ( m_nMouseActive == 1 )
		return;

	if (m_nMouseInitialized)
	{
#ifdef _WIN32
		if (m_fMouseParmsValid)
		{
			m_fRestoreSPI = SystemParametersInfo (SPI_SETMOUSE, 0, m_rgNewMouseParms, 0);
		}
#endif
		m_nMouseActive = 1;

		ResetMouse();

		// Clear accumulated error, too
		m_nXAccum = 0;
		m_nYAccum = 0;
	}
}


/*
===========
IN_DeactivateMouse
===========
*/
void CInput::DeactivateMouse (void)
{
	// This gets called whenever the mouse should be inactive. We only respond to it if we had 
	// previously activated the mouse.
	if ( m_nMouseActive == 0 )
		return;

	if (m_nMouseInitialized)
	{
#ifdef _WIN32
		if (m_fRestoreSPI)
		{
			SystemParametersInfo (SPI_SETMOUSE, 0, m_rgOrigMouseParms, 0);
		}
#endif

		m_nMouseActive = 0;
	}
}

/*
===========
IN_StartupMouse
===========
*/
void CInput::StartupMouse (void)
{
	if ( engine->CheckParm ("-nomouse", NULL ) ) 
		return; 

	m_nMouseInitialized = 1;
#ifdef _WIN32
	m_fMouseParmsValid = SystemParametersInfo (SPI_GETMOUSE, 0, m_rgOrigMouseParms, 0);

	if (m_fMouseParmsValid)
	{
		if ( engine->CheckParm ("-noforcemspd", NULL ) ) 
			m_rgNewMouseParms[2] = m_rgOrigMouseParms[2];

		if ( engine->CheckParm ("-noforcemaccel", NULL ) ) 
		{
			m_rgNewMouseParms[0] = m_rgOrigMouseParms[0];
			m_rgNewMouseParms[1] = m_rgOrigMouseParms[1];
		}

		if ( engine->CheckParm ("-noforcemparms", NULL ) ) 
		{
			m_rgNewMouseParms[0] = m_rgOrigMouseParms[0];
			m_rgNewMouseParms[1] = m_rgOrigMouseParms[1];
			m_rgNewMouseParms[2] = m_rgOrigMouseParms[2];
		}
	}
#endif
	
	m_nMouseButtons = MOUSE_BUTTON_COUNT;
}

/*
===========
IN_Shutdown
===========
*/
void CInput::Shutdown_Mouse (void)
{
	DeactivateMouse ();
}

/*
===========
IN_GetMousePos

Ask for mouse position from engine
===========
*/
void IN_GetMousePos( int *mx, int *my )
{
	engine->GetMousePosition( mx, my );
}

/*
===========
IN_ResetMouse

===========
*/
void CInput::ResetMouse( void )
{
	// no work to do in SDL
#ifdef _WIN32
	if ( !m_bRawInput && m_nMouseActive && engine->GetWindowCenterX && engine->GetWindowCenterY )
		SetCursorPos ( engine->GetWindowCenterX(), engine->GetWindowCenterY() );

	if ( engine->GetClientTime() - s_flRawInputUpdateTime > 1.0f )
	{
		s_flRawInputUpdateTime = engine->GetClientTime();
		m_bRawInput = CVAR_GET_FLOAT( "m_rawinput" ) != 0;
	}
#endif
}

/*
===========
IN_MouseEvent
===========
*/
void CInput::MouseEvent (int mstate)
{
	int		i;

	if ( m_fCameraInterceptingMouse || g_iVisibleMouse || vgui::surface()->IsCursorVisible() )
		return;

	// perform button actions
	for (i=0 ; i<m_nMouseButtons ; i++)
	{
		if ( (mstate & (1<<i)) &&
			!(m_nMouseOldButtons & (1<<i)) )
		{
			engine->Key_Event (K_MOUSE1 + i, 1);
		}

		if ( !(mstate & (1<<i)) &&
			(m_nMouseOldButtons & (1<<i)) )
		{
			engine->Key_Event (K_MOUSE1 + i, 0);
		}
	}	
	
	m_nMouseOldButtons = mstate;
}

/*
==============================
GetAccumulatedMouse

==============================
*/
void CInput::GetAccumulatedMouse( int *mx, int *my )
{
	*mx = m_nXAccum;
	*my = m_nYAccum;

	m_nXAccum = 0;
	m_nYAccum = 0;
}

/*
==============================
GetMouseDelta

==============================
*/
void CInput::GetMouseDelta( int mx, int my, int *oldx, int *oldy, int *x, int *y )
{
	if (m_filter && m_filter->value)
	{
		*x = ( mx + *oldx ) * 0.5;
		*y = ( my + *oldy ) * 0.5;
	}
	else
	{
		*x = mx;
		*y = my;
	}

	*oldx = mx;
	*oldy = my;

}

//-----------------------------------------------------------------------------
// Purpose: Allows modulation of mouse scaling/senstivity value and application
//  of custom algorithms.
// Input  : *x - 
//			*y - 
//-----------------------------------------------------------------------------
void CInput::ScaleMouse( int *x, int *y )
{
	if ( gHUD.GetSensitivity() != 0 )
	{
		*x *= gHUD.GetSensitivity();
		*y *= gHUD.GetSensitivity();
	}
	else
	{
		*x *= sensitivity->value;
		*y *= sensitivity->value;
	}
}

/*
===========
IN_MouseMove
===========
*/
void CInput::MouseMove ( usercmd_t *cmd)
{
	int		mouse_x, mouse_y;
	int		mx, my;
	Vector viewangles;

	static int old_mouse_x, old_mouse_y;

	// Get view angles from engine
	engine->GetViewAngles( (float *)viewangles );

	// Don't dript pitch at all if mouselooking.
	if ( in_mlook.state & 1)
	{
		V_StopPitchDrift ();
	}

	//jjb - this disbles normal mouse control if the user is trying to 
	//      move the camera, or if the mouse cursor is visible or if we're in intermission
	if ( !m_fCameraInterceptingMouse && !gHUD.m_iIntermission && !vgui::surface()->IsCursorVisible() )
	{
		// Sample mouse one more time
		AccumulateMouse();
		
		// Latch accumulated mouse movements
		GetAccumulatedMouse( &mx, &my );

		// Filter, etc. the delta values and place into mouse_x and mouse_y
		GetMouseDelta( mx, my, &old_mouse_x, &old_mouse_y, &mouse_x, &mouse_y );

		// Apply custom mouse scaling/acceleration
		ScaleMouse( &mouse_x, &mouse_y );

		// add mouse X/Y movement to cmd
		if ( (in_strafe.state & 1) || (lookstrafe->value && (in_mlook.state & 1) ))
			cmd->sidemove += m_side->value * mouse_x;
		else
			viewangles[YAW] -= m_yaw->value * mouse_x;

		if ( (in_mlook.state & 1) && !(in_strafe.state & 1))
		{
			viewangles[PITCH] += m_pitch->value * mouse_y;
			if (viewangles[PITCH] > cl_pitchdown->value)
				viewangles[PITCH] = cl_pitchdown->value;
			if (viewangles[PITCH] < -cl_pitchup->value)
				viewangles[PITCH] = -cl_pitchup->value;
		}
		else
		{
			if ((in_strafe.state & 1) && engine->IsNoClipping() )
			{
				cmd->upmove -= m_forward->value * mouse_y;
			}
			else
			{
				cmd->forwardmove -= m_forward->value * mouse_y;
			}
		}

		// if the mouse has moved, force it to the center, so there's room to move
		ResetMouse();
	}

	// Store out the new viewangles.
	engine->SetViewAngles( (float *)viewangles );
}

/*
===========
IN_Accumulate
===========
*/
void CInput::AccumulateMouse (void)
{
	//only accumulate mouse if we are not moving the camera with the mouse
	if ( !m_fCameraInterceptingMouse && !vgui::surface()->IsCursorVisible() )
	{
#ifdef _WIN32
		if ( !m_bRawInput )
		{
			POINT current_pos;
			GetCursorPos (&current_pos);
					
			m_nXAccum += current_pos.x - engine->GetWindowCenterX();
			m_nYAccum += current_pos.y - engine->GetWindowCenterY();
		}
		else
#endif
		{
			int deltaX, deltaY;
			SDL_GetRelativeMouseState( &deltaX, &deltaY );
			m_nXAccum += deltaX;
			m_nXAccum += deltaY;
		}

		// force the mouse to the center, so there's room to move
		ResetMouse();
	}
}

/*
===================
IN_ClearStates
===================
*/
void CInput::ClearStates (void)
{
	if ( !m_nMouseActive )
		return;

	m_nXAccum = 0;
	m_nYAccum = 0;
	m_nMouseOldButtons = 0;
}

/*
===========
IN_Init
===========
*/
void CInput::Init_Mouse (void)
{
	m_filter				= engine->pfnRegisterVariable ( "m_filter","0", FCVAR_ARCHIVE );
	sensitivity				= engine->pfnRegisterVariable ( "sensitivity","3", FCVAR_ARCHIVE ); // user mouse sensitivity setting.

	StartupMouse();
}


