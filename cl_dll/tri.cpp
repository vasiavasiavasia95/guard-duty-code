//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Triangle rendering, if any
//
// $NoKeywords: $
//=============================================================================//
 
// Triangle rendering apis are in engine->pTriAPI

#include "cl_dll.h"
#include "hud.h"
#include "tri.h"
#include "com_model.h"
#include "entity_state.h"
#include "cl_entity.h"
#include "triangleapi.h"
#include "Exports.h"

model_s *TRI_pModel;

#define TRI_HUD

#if defined( TRI_HUD )

/*
================
R_GetSpriteFrame

assume pModel is valid
================
*/
mspriteframe_t *R_GetSpriteFrame( model_t *pModel, int frame, float yaw )
{
	msprite_t		*psprite;
	mspritegroup_t	*pspritegroup;
	mspriteframe_t	*pspriteframe = NULL;
	float		*pintervals, fullinterval;
	int		i, numframes;
	float		targettime;

	psprite = (msprite_t*)pModel->cache.data;

	if ( !psprite )
	{
		engine->Con_Printf("Sprite:  no pSprite!!!\n");
		return 0;
	}

	if( frame < 0 )
	{
		frame = 0;
	}
	else if( frame >= psprite->numframes )
	{
		if( frame > psprite->numframes )
			engine->Con_Printf( "R_GetSpriteFrame: no such frame %d (%s)\n", frame, pModel->name );
		frame = psprite->numframes - 1;
	}

	if( psprite->frames[frame].type == SPR_SINGLE )
	{
		pspriteframe = psprite->frames[frame].frameptr;
	}
	else if( psprite->frames[frame].type == SPR_GROUP ) 
	{
		pspritegroup = (mspritegroup_t *)psprite->frames[frame].frameptr;
		pintervals = pspritegroup->intervals;
		numframes = pspritegroup->numframes;
		fullinterval = pintervals[numframes-1];

		// when loading in Mod_LoadSpriteGroup, we guaranteed all interval values
		// are positive, so we don't have to worry about division by zero
		targettime = engine->GetClientTime() - ((int)( engine->GetClientTime() / fullinterval )) * fullinterval;

		for( i = 0; i < (numframes - 1); i++ )
		{
			if( pintervals[i] > targettime )
				break;
		}
		pspriteframe = pspritegroup->frames[i];
	}
	
	return pspriteframe;
}

void TRI_GetSpriteParms( model_t *pSprite, int *frameWidth, int *frameHeight, int *numFrames, int currentFrame )
{
	mspriteframe_t	*pFrame;

	if ( !pSprite || pSprite->type != mod_sprite ) 
		return;

	pFrame = R_GetSpriteFrame( pSprite, currentFrame, 0.0f);

	if( frameWidth ) *frameWidth = pFrame->width;
	if( frameHeight ) *frameHeight = pFrame->height;
	if( numFrames ) *numFrames = pSprite->numframes;
}

void TRI_SprAdjustSize( float *x, float *y, float *w, float *h )
{
	float xscale, yscale;

	if ( !x && !y && !w && !h ) 
	  return;

	// scale for screen sizes
	xscale = ScreenWidth / (float)gHUD.m_iHudScaleWidth;
	yscale = ScreenHeight / (float)gHUD.m_iHudScaleHeight;
	
	if( x ) *x *= xscale;
	if( y ) *y *= yscale;
	if( w ) *w *= xscale;
	if( h ) *h *= yscale;
}

void TRI_SprDrawStretchPic( model_t* pModel, int frame, float x, float y, float w, float h, float s1, float t1, float s2, float t2 )
{
	engine->pTriAPI->SpriteTexture( pModel, frame );

	engine->pTriAPI->Begin(TRI_QUADS);

	engine->pTriAPI->TexCoord2f(s1, t1);
	engine->pTriAPI->Vertex3f(x, y, 0);

	engine->pTriAPI->TexCoord2f(s2, t1);
	engine->pTriAPI->Vertex3f(x + w, y, 0);

	engine->pTriAPI->TexCoord2f(s2, t2);
	engine->pTriAPI->Vertex3f(x + w, y + h, 0);

	engine->pTriAPI->TexCoord2f(s1, t2);
	engine->pTriAPI->Vertex3f(x, y + h, 0);

	engine->pTriAPI->End();
}

void TRI_SprDrawGeneric( int frame, float x, float y, wrect_t* prc)
{
	float	s1, s2, t1, t2;
	float width, height;
	int	w, h;

	// assume we get sizes from image
	TRI_GetSpriteParms( TRI_pModel, &w, &h, NULL, frame );

	width = w;
	height = h;

	if (prc)
	{
		wrect_t	rc = *prc;

		if( rc.left <= 0 || rc.left >= width ) rc.left = 0;
		if( rc.top <= 0 || rc.top >= height ) rc.top = 0;
		if( rc.right <= 0 || rc.right > width ) rc.right = width;
		if( rc.bottom <= 0 || rc.bottom > height ) rc.bottom = height;

		if ( gHUD.m_pCvarScale->value ) //magic nipples - slightly clip the boundaries on the rects so you don't see clipping from scaling.
		{
			s1 = ((float)rc.left + 0.33) / width;
			t1 = ((float)rc.top + 0.33) / height;
			s2 = ((float)rc.right - 0.33) / width;
			t2 = ((float)rc.bottom - 0.33) / height;
		}
		else
		{
			s1 = (float)rc.left / width;
			t1 = (float)rc.top / height;
			s2 = (float)rc.right / width;
			t2 = (float)rc.bottom / height;
		}

		width = rc.right - rc.left;
		height = rc.bottom - rc.top;
	}
	else
	{
		s1 = t1 = 0.0;
		s2 = t2 = 1.0;
	}

	TRI_SprAdjustSize( &x, &y, &width, &height);

	TRI_SprDrawStretchPic( TRI_pModel, frame, x, y, width, height, s1, t1, s2, t2);
}

void TRI_SprDrawAdditive( int frame, int x, int y, wrect_t *prc)
{
	engine->pTriAPI->RenderMode(kRenderTransAdd);

	TRI_SprDrawGeneric(frame, x, y, prc);

	engine->pTriAPI->RenderMode(kRenderNormal);
}

void TRI_SprSet( HSPRITE spr, int r, int g, int b)
{
	TRI_pModel = (model_s*)engine->GetSpritePointer(spr);

	engine->pTriAPI->Color4ub(r, g, b, 255);
}

void TRI_FillRGBA( float x, float y, float width, float height, int r, int g, int b, int a)
{
  TRI_SprAdjustSize( &x, &y, &width, &height);

  engine->pfnFillRGBA(x, y, width, height, r, g, b, a);

}

void TRI_SprDrawHoles( int frame, int x, int y, wrect_t *prc)
{
	engine->pTriAPI->RenderMode( kRenderTransAlpha );

	TRI_SprDrawGeneric(frame, x, y, prc);

	engine->pTriAPI->RenderMode( kRenderNormal );
}


#endif

/*
=================
HUD_DrawNormalTriangles

Non-transparent triangles-- add them here
=================
*/
void DLLEXPORT HUD_DrawNormalTriangles( void )
{
//	RecClDrawNormalTriangles();

}

/*
=================
HUD_DrawTransparentTriangles

Render any triangles with transparent rendermode needs here
=================
*/
void DLLEXPORT HUD_DrawTransparentTriangles( void )
{
//	RecClDrawTransparentTriangles();

}
