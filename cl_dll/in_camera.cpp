//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "cl_dll.h"
#include "hud.h"
#include "kbutton.h"
#include "usercmd.h"
#include "in_defs.h"
#include "Exports.h"
#include "input.h"

#include "port.h"

#include "eventscripts.h"
#include "event_api.h"
#include "pm_defs.h"

float CL_KeyState (kbutton_t *key);

extern Vector v_sim_org, v_origin;

//-------------------------------------------------- Constants

#define CAM_DIST_DELTA 1.0
#define CAM_ANGLE_DELTA 2.5
#define CAM_ANGLE_SPEED 2.5
#define CAM_MIN_DIST 30.0
#define CAM_ANGLE_MOVE .5
#define MAX_ANGLE_DIFF 10.0
#define PITCH_MAX 90.0
#define PITCH_MIN 0
#define YAW_MAX  135.0
#define YAW_MIN	 -135.0

enum ECAM_Command
{
	CAM_COMMAND_NONE = 0,
	CAM_COMMAND_TOTHIRDPERSON = 1,
	CAM_COMMAND_TOFIRSTPERSON = 2
};

//-------------------------------------------------- Global Variables

cvar_t	*cam_command;
cvar_t	*cam_snapto;
cvar_t	*cam_idealyaw;
cvar_t	*cam_idealpitch;
cvar_t	*cam_idealdist;
cvar_t	*cam_contain;

cvar_t	*c_maxpitch;
cvar_t	*c_minpitch;
cvar_t	*c_maxyaw;
cvar_t	*c_minyaw;
cvar_t	*c_maxdistance;
cvar_t	*c_mindistance;

extern CInput g_Input;

//-------------------------------------------------- Local Variables

static kbutton_t cam_pitchup, cam_pitchdown, cam_yawleft, cam_yawright;
static kbutton_t cam_in, cam_out, cam_move;

//-------------------------------------------------- Prototypes

void CAM_ToThirdPerson(void);
void CAM_ToFirstPerson(void);
void CAM_StartDistance(void);
void CAM_EndDistance(void);

void SDL_GetCursorPos( POINT *p )
{
	//engine->GetMousePosition( (int *)&p->x, (int *)&p->y );
	engine->pfnGetMousePos( p );
//	SDL_GetMouseState( (int *)&p->x, (int *)&p->y );
}

void SDL_SetCursorPos( const int x, const int y )
{
	engine->pfnSetMousePos( x, y );
}

// Found this in the engine 
// was also mentioned in the SDK camera code as engine->GetClientOrigin
/*void GetClientOrigin( Vector &origin )
{
	cl_entity_t *pPlayer;

	pPlayer = engine->GetEntityByIndex( );
	if ( EV_IsLocal( pPlayer->index - 1 ) )
	{
		VectorCopy( v_sim_org, origin );
	}
	else
	{
		VectorCopy( pPlayer->origin, origin );
	}
}*/

//-------------------------------------------------- Local Functions

float MoveToward( float cur, float goal, float maxspeed )
{
	if( cur != goal )
	{
		if( fabs( cur - goal ) > 180.0 )
		{
			if( cur < goal )
				cur += 360.0;
			else
				cur -= 360.0;
		}

		if( cur < goal )
		{
			if( cur < goal - 1.0 )
				cur += ( goal - cur ) / 4.0;
			else
				cur = goal;
		}
		else
		{
			if( cur > goal + 1.0 )
				cur -= ( cur - goal ) / 4.0;
			else
				cur = goal;
		}
	}


	// bring cur back into range
	if( cur < 0 )
		cur += 360.0;
	else if( cur >= 360 )
		cur -= 360;

	return cur;
}


//-------------------------------------------------- Global Functions

void CAM_ToThirdPerson(void)
{ 
	g_Input.CAM_ToThirdPerson();
}

void CAM_ToFirstPerson(void) 
{ 
	g_Input.CAM_ToFirstPerson();
}

void CAM_StartMouseMove(void)
{
	g_Input.CAM_StartMouseMove();
}

void CAM_EndMouseMove(void)
{
   g_Input.CAM_EndMouseMove();
}

void CAM_StartDistance(void)
{
	g_Input.CAM_StartDistance();
}

void CAM_EndDistance(void)
{
	g_Input.CAM_EndDistance();
}

void CAM_ToggleSnapto( void )
{ 
	cam_snapto->value = !cam_snapto->value;
}

void CInput::CAM_Think( void )
{
	Vector idealAngles;
	Vector camForward, camRight, camUp;
	Vector camAngles;
	float flSensitivity;
	Vector viewangles;

	switch( (int) cam_command->value )
	{
		case CAM_COMMAND_TOTHIRDPERSON:
			CAM_ToThirdPerson();
			break;

		case CAM_COMMAND_TOFIRSTPERSON:
			CAM_ToFirstPerson();
			break;

		case CAM_COMMAND_NONE:
		default:
			break;
	}

	if( !m_fCameraInThirdPerson )
		return;
	
	idealAngles[ PITCH ] = cam_idealpitch->value;
	idealAngles[ YAW ] = cam_idealyaw->value;
	idealAngles[ 2 ] = cam_idealdist->value;
	//
	//movement of the camera with the mouse
	//
	if (m_fCameraMovingWithMouse)
	{
		int cpx, cpy;

	    //get windows cursor position
		//SDL_GetCursorPos (&cam_mouse);
		engine->GetMousePosition( &cpx, &cpy ); // same as SDL_GetCursorPos in the engine but uses ints

		m_nCameraX = cpx;
		m_nCameraY = cpy;

		//check for X delta values and adjust accordingly
		//eventually adjust YAW based on amount of movement
		//don't do any movement of the cam using YAW/PITCH if we are zooming in/out the camera	
		if (!m_fCameraDistanceMove)
		{
			//keep the camera within certain limits around the player (ie avoid certain bad viewing angles)  
			if (m_nCameraX>engine->GetWindowCenterX())
			{
				//if ((camAngles[YAW]>=225.0)||(camAngles[YAW]<135.0))
				if (idealAngles[YAW]<c_maxyaw->value)
				{
					idealAngles[ YAW ] += (CAM_ANGLE_MOVE)*((m_nCameraX-engine->GetWindowCenterX())/2);
				}
				if (idealAngles[YAW]>c_maxyaw->value)
				{
				
					idealAngles[YAW]=c_maxyaw->value;
				}
		}
		else if (m_nCameraX <engine->GetWindowCenterX())
		{
			//if ((camAngles[YAW]<=135.0)||(camAngles[YAW]>225.0))
			if (idealAngles[YAW]>c_minyaw->value)
			{
				idealAngles[ YAW ] -= (CAM_ANGLE_MOVE)* ((engine->GetWindowCenterX()- m_nCameraX)/2);
			   	
			}
			if (idealAngles[YAW]<c_minyaw->value)
			{
				idealAngles[YAW]=c_minyaw->value;
				
			}
		}

		//check for y delta values and adjust accordingly
		//eventually adjust PITCH based on amount of movement
		//also make sure camera is within bounds
		if (m_nCameraY >engine->GetWindowCenterY())
		{
			if(idealAngles[PITCH]<c_maxpitch->value)
			{
				idealAngles[PITCH] +=(CAM_ANGLE_MOVE)* ((m_nCameraY-engine->GetWindowCenterY())/2);
			}
			if (idealAngles[PITCH]>c_maxpitch->value)
			{
				idealAngles[PITCH]=c_maxpitch->value;
			}
		}
		else if (m_nCameraY<engine->GetWindowCenterY())
		{
			if (idealAngles[PITCH]>c_minpitch->value)
			{
				idealAngles[PITCH] -= (CAM_ANGLE_MOVE)*((engine->GetWindowCenterY()-m_nCameraY)/2);
			}
			if (idealAngles[PITCH]<c_minpitch->value)
			{
				idealAngles[PITCH]=c_minpitch->value;
			}
		}

		//set old mouse coordinates to current mouse coordinates
		//since we are done with the mouse

		if ( ( flSensitivity = gHUD.GetSensitivity() ) != 0 )
		{
			m_nCameraOldX = m_nCameraX * flSensitivity;
			m_nCameraOldY = m_nCameraY * flSensitivity;
		}
		else
		{
			m_nCameraOldX = m_nCameraX;
			m_nCameraOldY = m_nCameraY;
		}
		
		engine->pfnSetMousePos(engine->GetWindowCenterX(), engine->GetWindowCenterY());
	  }
	}

	//Nathan code here
	if( CL_KeyState( &cam_pitchup ) )
		idealAngles[ PITCH ] += CAM_ANGLE_DELTA;
	else if( CL_KeyState( &cam_pitchdown ) )
		idealAngles[ PITCH ] -= CAM_ANGLE_DELTA;

	if( CL_KeyState( &cam_yawleft ) )
		idealAngles[ YAW ] -= CAM_ANGLE_DELTA;
	else if( CL_KeyState( &cam_yawright ) )
		idealAngles[ YAW ] += CAM_ANGLE_DELTA;

	if( CL_KeyState( &cam_in ) )
	{
		idealAngles[ 2 ] -= CAM_DIST_DELTA;
		if( idealAngles[ 2 ] < CAM_MIN_DIST )
		{
			// If we go back into first person, reset the angle
			idealAngles[ PITCH ] = 0;
			idealAngles[ YAW ] = 0;
			idealAngles[ 2 ] = CAM_MIN_DIST;
		}

	}
	else if( CL_KeyState( &cam_out ) )
		idealAngles[ 2 ] += CAM_DIST_DELTA;

	if (m_fCameraDistanceMove)
	{
		if (m_nCameraY >engine->GetWindowCenterY())
		{
			if(idealAngles[ 2 ]<c_maxdistance->value)
			{
			    idealAngles[ 2 ] +=CAM_DIST_DELTA * ((m_nCameraY-engine->GetWindowCenterY())/2);
			}
			if (idealAngles[ 2 ]>c_maxdistance->value)
			{
				idealAngles[ 2 ]=c_maxdistance->value;
			}
		}
		else if (m_nCameraY<engine->GetWindowCenterY())
		{
			if (idealAngles[ 2 ]>c_mindistance->value)
			{
			   idealAngles[ 2 ] -= (CAM_DIST_DELTA)*((engine->GetWindowCenterY()-m_nCameraY)/2);
			}
			if (idealAngles[ 2 ]<c_mindistance->value)
			{
				idealAngles[ 2 ]=c_mindistance->value;
			}
		}
		//set old mouse coordinates to current mouse coordinates
		//since we are done with the mouse
		m_nCameraOldX = m_nCameraX * gHUD.GetSensitivity();
		m_nCameraOldY = m_nCameraY * gHUD.GetSensitivity();
		engine->pfnSetMousePos(engine->GetWindowCenterX(), engine->GetWindowCenterY());
	}

	// Move towards ideal
	VectorCopy( m_vecCameraOffset, camAngles );

	engine->GetViewAngles( (float *)viewangles );

	if( cam_snapto->value )
	{
		camAngles[ YAW ] = cam_idealyaw->value + viewangles[ YAW ];
		camAngles[ PITCH ] = cam_idealpitch->value + viewangles[ PITCH ];
		camAngles[ 2 ] = cam_idealdist->value;
	}
	else
	{
		if( camAngles[ YAW ] - viewangles[ YAW ] != cam_idealyaw->value )
			camAngles[ YAW ] = MoveToward( camAngles[ YAW ], cam_idealyaw->value + viewangles[ YAW ], CAM_ANGLE_SPEED );

		if( camAngles[ PITCH ] - viewangles[ PITCH ] != cam_idealpitch->value )
			camAngles[ PITCH ] = MoveToward( camAngles[ PITCH ], cam_idealpitch->value + viewangles[ PITCH ], CAM_ANGLE_SPEED );

		if( fabs( camAngles[ 2 ] - cam_idealdist->value ) < 2.0 )
			camAngles[ 2 ] = cam_idealdist->value;
		else
			camAngles[ 2 ] += ( cam_idealdist->value - camAngles[ 2 ] ) / 4.0;
	}

	if( cam_contain->value )
	{
		Vector camForward, viewoffset;
		pmtrace_t trace;

		// find our player's origin, and from there, the eye position
		Vector origin = v_sim_org;

		engine->pEventAPI->EV_LocalPlayerViewheight( viewoffset );

		VectorAdd( origin, viewoffset, origin)
		
		// get the forward vector
		AngleVectors( Vector(camAngles[ PITCH ], camAngles[ YAW ], 0), camForward, NULL, NULL );

		engine->pEventAPI->EV_SetTraceHull( 3 );

		engine->pEventAPI->EV_PlayerTrace( origin, origin - (camForward * camAngles[ 2 ]), PM_STUDIO_IGNORE, -1, &trace);

		//engine->pEfxAPI->R_RunParticleEffect( trace.endpos, vec3_origin, 0, 10 );

		// move the camera closer if it hit something
		if( trace.fraction < 1.0 )
		{
			camAngles[ 2 ] *= trace.fraction;
		}

		// For now, I'd rather see the insade of a player model than punch the camera through a wall
		// might try the fade out trick at some point
		//if( camOffset[ DIST ] < CAM_MIN_DIST )
		//    camOffset[ DIST ] = CAM_MIN_DIST; // clamp up to minimum
	}

	m_vecCameraOffset[ 0 ] = camAngles[ 0 ];
	m_vecCameraOffset[ 1 ] = camAngles[ 1 ];
	m_vecCameraOffset[ 2 ] = camAngles[ 2 ];
}

extern void KeyDown (kbutton_t *b);	// HACK
extern void KeyUp (kbutton_t *b);	// HACK

void CAM_PitchUpDown(void) { KeyDown( &cam_pitchup ); }
void CAM_PitchUpUp(void) { KeyUp( &cam_pitchup ); }
void CAM_PitchDownDown(void) { KeyDown( &cam_pitchdown ); }
void CAM_PitchDownUp(void) { KeyUp( &cam_pitchdown ); }
void CAM_YawLeftDown(void) { KeyDown( &cam_yawleft ); }
void CAM_YawLeftUp(void) { KeyUp( &cam_yawleft ); }
void CAM_YawRightDown(void) { KeyDown( &cam_yawright ); }
void CAM_YawRightUp(void) { KeyUp( &cam_yawright ); }
void CAM_InDown(void) { KeyDown( &cam_in ); }
void CAM_InUp(void) { KeyUp( &cam_in ); }
void CAM_OutDown(void) { KeyDown( &cam_out ); }
void CAM_OutUp(void) { KeyUp( &cam_out ); }

/*
==============================
CAM_ToThirdPerson

==============================
*/
void CInput::CAM_ToThirdPerson(void)
{ 
	Vector viewangles;

#if !defined( _DEBUG )
	if ( engine->GetMaxClients() > 1 )
	{
		// no thirdperson in multiplayer.
		return;
	}
#endif

	engine->GetViewAngles( (float *)viewangles );

	if( !m_fCameraInThirdPerson )
	{
		m_fCameraInThirdPerson = 1; 
		
		m_vecCameraOffset[ YAW ] = viewangles[ YAW ]; 
		m_vecCameraOffset[ PITCH ] = viewangles[ PITCH ]; 
		m_vecCameraOffset[ 2 ] = CAM_MIN_DIST; 
	}

	engine->Cvar_SetValue( "cam_command", 0 );
}

/*
==============================
CAM_ToFirstPerson

==============================
*/
void CInput::CAM_ToFirstPerson(void)
{
	m_fCameraInThirdPerson = 0;

	engine->Cvar_SetValue("cam_command", 0);
}

/*
==============================
CAM_StartMouseMove

==============================
*/
void CInput::CAM_StartMouseMove(void)
{
	float flSensitivity;
		
	//only move the cam with mouse if we are in third person.
	if (m_fCameraInThirdPerson)
	{
		//set appropriate flags and initialize the old mouse position
		//variables for mouse camera movement
		if (!m_fCameraMovingWithMouse)
		{
			int cpx, cpy;

			m_fCameraMovingWithMouse=1;
			m_fCameraInterceptingMouse=1;
			
			engine->GetMousePosition( &cpx, &cpy);

			m_nCameraX = cpx;
			m_nCameraY = cpy;

			if ( ( flSensitivity = gHUD.GetSensitivity() ) != 0 )
			{
				m_nCameraOldX=m_nCameraX*flSensitivity;
				m_nCameraOldY=m_nCameraY*flSensitivity;
			}
			else
			{
				m_nCameraOldX=m_nCameraX;
				m_nCameraOldY=m_nCameraY;
			}
		}
	}
	//we are not in 3rd person view..therefore do not allow camera movement
	else
	{   
		m_fCameraMovingWithMouse=0;
		m_fCameraInterceptingMouse=0;
	}
}

/*
==============================
CAM_EndMouseMove

the key has been released for camera movement
tell the engine that mouse camera movement is off
==============================
*/
void CInput::CAM_EndMouseMove(void)
{
   m_fCameraMovingWithMouse=0;
   m_fCameraInterceptingMouse=0;
}

/*
==============================
CAM_StartDistance

routines to start the process of moving the cam in or out 
using the mouse
==============================
*/
void CInput::CAM_StartDistance(void)
{
	//only move the cam with mouse if we are in third person.
	if (m_fCameraInThirdPerson)
	{
	  //set appropriate flags and initialize the old mouse position
	  //variables for mouse camera movement
	  if (!m_fCameraDistanceMove)
	  {
		  int cpx, cpy;

		  m_fCameraDistanceMove=1;
		  m_fCameraMovingWithMouse=1;
		  m_fCameraInterceptingMouse=1;
		  engine->GetMousePosition( &cpx, &cpy );

		  m_nCameraX = cpx;
		  m_nCameraY = cpy;

		  m_nCameraOldX=m_nCameraX*gHUD.GetSensitivity();
		  m_nCameraOldY=m_nCameraY*gHUD.GetSensitivity();
	  }
	}
	//we are not in 3rd person view..therefore do not allow camera movement
	else
	{   
		m_fCameraDistanceMove=0;
		m_fCameraMovingWithMouse=0;
		m_fCameraInterceptingMouse=0;
	}
}

/*
==============================
CAM_EndDistance

the key has been released for camera movement
tell the engine that mouse camera movement is off
==============================
*/
void CInput::CAM_EndDistance(void)
{
   m_fCameraDistanceMove=0;
   m_fCameraMovingWithMouse=0;
   m_fCameraInterceptingMouse=0;
}

/*
==============================
CAM_IsThirdPerson

==============================
*/
int CInput::CAM_IsThirdPerson( void )
{
	return m_fCameraInThirdPerson;
}

/*
==============================
CAM_GetCameraOffset

==============================
*/
void CInput::CAM_GetCameraOffset( float *ofs )
{
	VectorCopy( m_vecCameraOffset, ofs );
}

/*
==============================
CAM_InterceptingMouse

==============================
*/
int CInput::CAM_InterceptingMouse( void )
{
	return m_fCameraInterceptingMouse;
}

void CInput::Init_Camera( void )
{
	engine->pfnAddCommand( "+campitchup", CAM_PitchUpDown );
	engine->pfnAddCommand( "-campitchup", CAM_PitchUpUp );
	engine->pfnAddCommand( "+campitchdown", CAM_PitchDownDown );
	engine->pfnAddCommand( "-campitchdown", CAM_PitchDownUp );
	engine->pfnAddCommand( "+camyawleft", CAM_YawLeftDown );
	engine->pfnAddCommand( "-camyawleft", CAM_YawLeftUp );
	engine->pfnAddCommand( "+camyawright", CAM_YawRightDown );
	engine->pfnAddCommand( "-camyawright", CAM_YawRightUp );
	engine->pfnAddCommand( "+camin", CAM_InDown );
	engine->pfnAddCommand( "-camin", CAM_InUp );
	engine->pfnAddCommand( "+camout", CAM_OutDown );
	engine->pfnAddCommand( "-camout", CAM_OutUp );
	engine->pfnAddCommand( "thirdperson", ::CAM_ToThirdPerson );
	engine->pfnAddCommand( "firstperson", ::CAM_ToFirstPerson );
	engine->pfnAddCommand( "+cammousemove",::CAM_StartMouseMove);
	engine->pfnAddCommand( "-cammousemove",::CAM_EndMouseMove);
	engine->pfnAddCommand( "+camdistance", ::CAM_StartDistance );
	engine->pfnAddCommand( "-camdistance", ::CAM_EndDistance );
	engine->pfnAddCommand( "snapto", ::CAM_ToggleSnapto );

	cam_command				= engine->pfnRegisterVariable ( "cam_command", "0", 0 );	 // tells camera to go to thirdperson
	cam_snapto				= engine->pfnRegisterVariable ( "cam_snapto", "0", 0 );	 // snap to thirdperson view
	cam_idealyaw			= engine->pfnRegisterVariable ( "cam_idealyaw", "0", 0 );	 // thirdperson yaw
	cam_idealpitch			= engine->pfnRegisterVariable ( "cam_idealpitch", "0", 0 );	 // thirperson pitch
	cam_idealdist			= engine->pfnRegisterVariable ( "cam_idealdist", "64", 0 );	 // thirdperson distance
	cam_contain				= engine->pfnRegisterVariable ( "cam_contain", "1", 0 );	// contain camera to world

	c_maxpitch				= engine->pfnRegisterVariable ( "c_maxpitch", "90.0", 0 );
	c_minpitch				= engine->pfnRegisterVariable ( "c_minpitch", "0.0", 0 );
	c_maxyaw				= engine->pfnRegisterVariable ( "c_maxyaw",   "135.0", 0 );
	c_minyaw				= engine->pfnRegisterVariable ( "c_minyaw",   "-135.0", 0 );
	c_maxdistance			= engine->pfnRegisterVariable ( "c_maxdistance",   "200.0", 0 );
	c_mindistance			= engine->pfnRegisterVariable ( "c_mindistance",   "30.0", 0 );
}

/*void CAM_ClearStates( void )
{
	Vector viewangles;

	engine->GetViewAngles( (float *)viewangles );

	cam_pitchup.state = 0;
	cam_pitchdown.state = 0;
	cam_yawleft.state = 0;
	cam_yawright.state = 0;
	cam_in.state = 0;
	cam_out.state = 0;

	cam_thirdperson = 0;
	cam_command->value = 0;
	cam_mousemove=0;

	cam_snapto->value = 0;
	cam_distancemove = 0;

	cam_ofs[ 0 ] = 0.0;
	cam_ofs[ 1 ] = 0.0;
	cam_ofs[ 2 ] = CAM_MIN_DIST;

	cam_idealpitch->value = viewangles[ PITCH ];
	cam_idealyaw->value = viewangles[ YAW ];
	cam_idealdist->value = CAM_MIN_DIST;
}*/







